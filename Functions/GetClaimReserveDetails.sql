USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[GetClaimReserveDetails]    Script Date: 1/28/2019 9:59:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[GetClaimReserveDetails]
(
  @endDate DATETIME = NULL
)
RETURNS @reserveTable TABLE 
(
  [claimID] INTEGER,
  [refCategory] VARCHAR(100),
  [asOfDate] DATETIME,
  [transDate] DATETIME,
  [pendingAmount] DECIMAL(18,2),
  [approvedAmount] DECIMAL(18,2),
  [stat] VARCHAR(20)
)
AS
BEGIN

  IF (@endDate IS NULL)
  BEGIN
    SET @endDate = GETDATE()
  END
  
  -- requested
  INSERT INTO @reserveTable ([claimID],[refCategory],[asOfDate],[transDate],[pendingAmount],[approvedAmount],[stat])
  SELECT  [claimID],
          [refCategory],
          @endDate,
          [dateRequested],
          [requestedAmount],
          0,
          'O'
  FROM    [claimadjreq]
  WHERE   [dateRequested] <= @endDate

  -- requested but not approved
  INSERT INTO @reserveTable ([claimID],[refCategory],[asOfDate],[transDate],[pendingAmount],[approvedAmount],[stat])
  SELECT  [claimID],
          [refCategory],
          @endDate,
          [dateRequested],
          [requestedAmount],
          0,
          'O'
  FROM    [claimadjtrx]
  WHERE   [dateRequested] <= @endDate
  AND     [transDate] > @endDate

  -- approved
  INSERT INTO @reserveTable ([claimID],[refCategory],[asOfDate],[transDate],[pendingAmount],[approvedAmount],[stat])
  SELECT  [claimID],
          [refCategory],
          @endDate,
          [dateRequested],
          0,
          [transAmount],
          'A'
  FROM    [claimadjtrx]
  WHERE   [transDate] <= @endDate

  RETURN
END