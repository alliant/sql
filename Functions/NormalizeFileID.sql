USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[NormalizeFileID]    Script Date: 3/19/2020 4:33:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[NormalizeFileID]
(
	-- Add the parameters for the function here
	@fileNumber VARCHAR(1000)
)
RETURNS VARCHAR(1000)
AS
BEGIN
	-- Declare the return variable here
  DECLARE @validChars VARCHAR(1000) = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
          @invalidChars VARCHAR(1000),
          @i INTEGER = 1

	-- Add the T-SQL statements to compute the return value here
  SET @invalidChars = @fileNumber
  WHILE @i <= LEN(@validChars)
  BEGIN
	  SELECT @invalidChars = REPLACE(@invalidChars, SUBSTRING(@validChars, @i, 1), '')
    SET @i = @i + 1
  END
  SET @invalidChars = @invalidChars + '-' /* Appended '-' because LEN neglects trailing space */
  
  SET @i = 1
  WHILE @i <= LEN(@invalidChars)
  BEGIN
	  SELECT @fileNumber = REPLACE(@fileNumber, SUBSTRING(@invalidChars, @i, 1), '')
    SET @i = @i + 1
  END

	-- Return the result of the function
	RETURN UPPER(@fileNumber)

END
