USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[GetAlertGreaterOrLessThan]    Script Date: 2/12/2018 8:43:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[GetAlertGreaterOrLessThan]
(
	-- Add the parameters for the function here
	@processCode VARCHAR(30)
)
RETURNS BIT
AS
BEGIN
  DECLARE @return BIT = 0

  SELECT  @return = CONVERT(BIT,[dbo].[GetEntry] (1, [objDesc], ','))
  FROM    [dbo].[sysprop]
  WHERE   [appCode] = 'AMD'
  AND     [objAction] = 'Alert'
  AND     [objProperty] = 'Threshold' 
  AND     [objID] = @processCode
	-- Return the result of the function
	RETURN @return

END
