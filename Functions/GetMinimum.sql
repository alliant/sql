USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[GetMinimum]    Script Date: 5/7/2018 10:00:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[GetMinimum] 
(
	@number1 DECIMAL(18,2) = 0,
  @number2 DECIMAL(18,2) = 0
)
RETURNS DECIMAL(18,2)
AS
BEGIN
	DECLARE @minimum DECIMAL(18,2)
  SELECT @minimum=CASE WHEN @number1 < @number2 THEN @number1 ELSE @number2 END
  RETURN @minimum

END