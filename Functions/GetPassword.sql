USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[GetPassword]    Script Date: 12/21/2015 10:25:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		  John Oliver
-- Create date: 12/21/2015
-- Description:	Get the user's password
-- =============================================
ALTER FUNCTION [dbo].[GetPassword] (@name varchar(200))
RETURNS varchar(200)
AS
BEGIN
  DECLARE @uid varchar(32),
          @pass varchar(32) = '',
          @char char,
          @newchar char,
          @index int = 1
  
  -- set up the convertion table
  DECLARE @convertTable table (chr CHAR, digit CHAR)
  INSERT INTO @convertTable (chr,digit) VALUES ('a', '@')
  INSERT INTO @convertTable (chr,digit) VALUES ('e', '3')
  INSERT INTO @convertTable (chr,digit) VALUES ('i', '!')
  INSERT INTO @convertTable (chr,digit) VALUES ('o', '0')
  INSERT INTO @convertTable (chr,digit) VALUES ('u', '*')

  SET @uid = dbo.GetUID(@name)

  WHILE @index < LEN(@uid) + 1
  BEGIN
    SET @char = SUBSTRING(@uid,@index,1)
    SET @newchar = NULL
    SELECT @newchar=[digit] FROM @convertTable WHERE [chr]=LOWER(@char)
    IF (@newchar IS NULL)
    BEGIN
      SET @newchar = @char
    END
    SET @pass = @pass + @newchar
    SET @index = @index + 1
  END

  SET @pass = @pass + '!'
  SET @pass = UPPER(SUBSTRING(@pass,1,1)) + SUBSTRING(@pass,2,LEN(@pass)-1)

  RETURN @pass
END
