USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[GetAlertThresholdRange]    Script Date: 2/16/2018 1:27:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[GetAlertThresholdRange] 
(
	-- Add the parameters for the function here
  @processCode VARCHAR(100) = '',
  @score DECIMAL(18,2) = 0
)
RETURNS VARCHAR(100)
AS
BEGIN
	DECLARE @threshold VARCHAR(100) = '',
          @threshold0Range VARCHAR(30) = '',
          @threshold1 DECIMAL(18,2) = 0,
          @threshold1Range VARCHAR(30) = '',
          @threshold2 DECIMAL(18,2) = 0,
          @threshold2Range VARCHAR(30) = '',
          @greaterThan BIT = 0,
          @return VARCHAR(100) = ''
          
  SELECT @threshold=[objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'AMD' AND [objAction] = 'Alert' AND [objProperty] = 'Threshold' AND [objID] = @processCode
  
  IF (@threshold <> 'User Defined')
  BEGIN
    SET @threshold2 = CONVERT(DECIMAL(18,2),[dbo].[GetEntry] (1, @threshold, DEFAULT))
    SET @threshold1 = CONVERT(DECIMAL(18,2),[dbo].[GetEntry] (2, @threshold, DEFAULT))
    
    SET @greaterThan = [dbo].[GetAlertGreaterOrLessThan] (@processCode)
    IF (@greaterThan = 1)
    BEGIN
      --Get the threshold in character format
      SELECT  @threshold0Range =  CASE @threshold1
                                   WHEN 1 THEN
                                    CASE [type]
                                      WHEN 'money' THEN '$ 0.00'
                                      WHEN 'percent' THEN '0%'
                                      WHEN 'integer' THEN '0'
                                      WHEN 'decimal' THEN '0.00'
                                    END
                                   ELSE
                                    CASE [type]
                                      WHEN 'money' THEN '$ 0.00 to $ ' + REPLACE(CONVERT(VARCHAR, CONVERT(MONEY, FLOOR(@threshold1 - 0.01)), 1), '.00', '')
                                      WHEN 'percent' THEN '0% to ' + CONVERT(VARCHAR, CONVERT(INTEGER, (@threshold1 - 0.01) * 100)) + '%'
                                      WHEN 'integer' THEN '0 to ' + CONVERT(VARCHAR, CONVERT(INTEGER, @threshold1 - 1))
                                      WHEN 'decimal' THEN '0.00 to ' + CONVERT(VARCHAR, @threshold1 - 0.01)
                                    END
                                  END,
              @threshold1Range =  CASE @threshold1 + 1
                                   WHEN @threshold2 THEN
                                    CASE [type]
                                      WHEN 'money' THEN '$ ' + REPLACE(CONVERT(VARCHAR, CONVERT(MONEY, FLOOR(@threshold1)), 1) , '.00', '')
                                      WHEN 'percent' THEN CONVERT(VARCHAR, CONVERT(INTEGER, (@threshold1) * 100)) + '%'
                                      WHEN 'integer' THEN CONVERT(VARCHAR, CONVERT(INTEGER, @threshold1))
                                      WHEN 'decimal' THEN CONVERT(VARCHAR, @threshold1)
                                    END
                                   ELSE
                                    CASE [type]
                                      WHEN 'money' THEN '$ ' + REPLACE(CONVERT(VARCHAR, CONVERT(MONEY, FLOOR(@threshold1)), 1), '.00', '') + ' to $ ' + REPLACE(CONVERT(VARCHAR, CONVERT(MONEY, FLOOR(@threshold2 - 0.01)), 1), '.00', '')
                                      WHEN 'percent' THEN CONVERT(VARCHAR, CONVERT(INTEGER, (@threshold1) * 100)) + '% to ' + CONVERT(VARCHAR, CONVERT(INTEGER, (@threshold2 - 0.01) * 100)) + '%'
                                      WHEN 'integer' THEN CONVERT(VARCHAR, CONVERT(INTEGER, @threshold1)) + ' to ' + CONVERT(VARCHAR, CONVERT(INTEGER, @threshold2) - 1)
                                      WHEN 'decimal' THEN CONVERT(VARCHAR, @threshold1) + ' to ' + CONVERT(VARCHAR, @threshold2 - 0.01)
                                    END
                                  END,
              @threshold2Range =  CASE [type]
                                    WHEN 'money' THEN '$ ' + REPLACE(CONVERT(VARCHAR, CONVERT(MONEY, FLOOR(@threshold2)), 1), '.00', '') + '+'
                                    WHEN 'percent' THEN CONVERT(VARCHAR, CONVERT(INTEGER, (@threshold2) * 100)) + '%+'
                                    WHEN 'integer' THEN CONVERT(VARCHAR, CONVERT(INTEGER, @threshold2)) + '+'
                                    WHEN 'decimal' THEN CONVERT(VARCHAR, @threshold2) + '+'
                                  END
      FROM    [dbo].[syscode]
      WHERE   [codeType] = 'Alert'
      AND     [code] = @processCode

      -- If score is below threshold 1, severity is 0
      SET @return = @threshold0Range
      -- greater than threshold 1, severity is 1
      IF (@score >= @threshold1)
        SET @return = @threshold1Range
      -- greater than threshold 2, severity is 2
      IF (@score >= @threshold2)
        SET @return = @threshold2Range
    END
    ELSE
    BEGIN
      --Get the threshold in character format
      SELECT  @threshold0Range =  CASE [type]
                                    WHEN 'money' THEN '$ ' + CONVERT(VARCHAR, CONVERT(MONEY, @threshold2 + 0.01), 1) + '+'
                                    WHEN 'percent' THEN CONVERT(VARCHAR, CONVERT(INTEGER, (@threshold2 + 0.01) * 100)) + '%+'
                                    WHEN 'integer' THEN CONVERT(VARCHAR, CONVERT(INTEGER, @threshold2) + 1) + '+'
                                    WHEN 'decimal' THEN CONVERT(VARCHAR, @threshold2 + 0.01) + '+'
                                  END,
              @threshold1Range =  CASE @threshold1 + 1
                                   WHEN @threshold2 THEN
                                    CASE [type]
                                      WHEN 'money' THEN '$ ' + CONVERT(VARCHAR, CONVERT(MONEY, @threshold2), 1) 
                                      WHEN 'percent' THEN CONVERT(VARCHAR, CONVERT(INTEGER, (@threshold2) * 100)) + '%'
                                      WHEN 'integer' THEN CONVERT(VARCHAR, CONVERT(INTEGER, @threshold2))
                                      WHEN 'decimal' THEN CONVERT(VARCHAR, @threshold2)
                                    END
                                   ELSE
                                    CASE [type]
                                      WHEN 'money' THEN '$ ' + CONVERT(VARCHAR, CONVERT(MONEY, @threshold2), 1) + ' to $ ' + CONVERT(VARCHAR, CONVERT(MONEY, @threshold1 + 0.01) , 1)
                                      WHEN 'percent' THEN CONVERT(VARCHAR, CONVERT(INTEGER, (@threshold2) * 100)) + '% to ' + CONVERT(VARCHAR, CONVERT(INTEGER, (@threshold1 + 0.01) * 100)) + '%'
                                      WHEN 'integer' THEN CONVERT(VARCHAR, CONVERT(INTEGER, @threshold2)) + ' to ' + CONVERT(VARCHAR, CONVERT(INTEGER, @threshold1) + 1)
                                      WHEN 'decimal' THEN CONVERT(VARCHAR, @threshold2) + ' to ' + CONVERT(VARCHAR, @threshold1 + 0.01)
                                    END
                                  END,
              @threshold2Range =  CASE @threshold1
                                   WHEN 1 THEN
                                    CASE [type]
                                      WHEN 'money' THEN '$ 0.00'
                                      WHEN 'percent' THEN '0%'
                                      WHEN 'integer' THEN '0'
                                      WHEN 'decimal' THEN '0.00'
                                    END
                                   ELSE
                                    CASE [type]
                                      WHEN 'money' THEN '$ 0.00 to $ ' + CONVERT(VARCHAR, CONVERT(MONEY, @threshold1), 1)
                                      WHEN 'percent' THEN '0% to ' + CONVERT(VARCHAR, CONVERT(INTEGER, (@threshold1) * 100)) + '%'
                                      WHEN 'integer' THEN '0 to ' + CONVERT(VARCHAR, CONVERT(INTEGER, @threshold1))
                                      WHEN 'decimal' THEN '0.00 to ' + CONVERT(VARCHAR, @threshold1)
                                    END
                                  END
              
      FROM    [dbo].[syscode]
      WHERE   [codeType] = 'Alert'
      AND     [code] = @processCode

      -- If score is above threshold 2, severity is 0
      SET @return = @threshold0Range
      -- less than threshold 2, severity is 1
      IF (@score <= @threshold2)
        SET @return = @threshold1Range
      -- less than threshold 1, severity is 2
      IF (@score <= @threshold1)
        SET @return = @threshold2Range
    END
  END
  ELSE SET @return = @threshold

	-- Return the result of the function
	RETURN @return

END
