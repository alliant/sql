/****** Object:  UserDefinedFunction [dbo].[CanAccessState]    Script Date: 4/4/2023 12:33:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author     : VJ
-- Create date: 13-08-2020
-- Description:	This function check whether a userId 
--              have access to particular agent or not
-- =============================================
ALTER FUNCTION [dbo].[CanAccessState]
(
  -- Add the parameters for the function here
  @UID VARCHAR(100)    = '',
  @StateID VARCHAR(20) = ''
)
RETURNS BIT
AS
BEGIN
  DECLARE @flag               INTEGER = 0,
          @objManagerValue    VARCHAR(100) = '',          
		  @objStateValue      VARCHAR(500) = '',
		  @roleValue          VARCHAR(500) = ''

  SELECT @roleValue = sysuser.role FROM sysuser WHERE sysuser.uid = @UID
  IF @roleValue != '' AND @roleValue LIKE '%' + 'Administrator' + '%'
   BEGIN
     SET @flag = 1
	 RETURN @flag
   END

  SELECT @objStateValue = sysprop.objValue FROM sysprop WHERE sysprop.appcode = 'SYS'AND sysprop.objAction   = 'AgentFilter' 
		  	    																	 AND sysprop.objProperty = 'State'
																					 AND sysprop.objId       = @UID
  IF @objStateValue != ''  
   BEGIN
	IF @objStateValue = '*'
	 BEGIN
	   SET @flag = 1
	   RETURN @flag
	 END 
	ELSE IF ([dbo].[GetCanDo](@objStateValue, @StateID) = 1)
	 BEGIN
	   SET @flag = 1
	   RETURN @flag
	 END
   END

  SELECT @objManagerValue = sysprop.objValue FROM sysprop WHERE sysprop.appcode = 'SYS' AND sysprop.objAction   = 'AgentFilter' 
																						AND sysprop.objProperty = 'Manager' 
																						AND sysprop.objId       = @UID 
  IF @objManagerValue != '' AND @objManagerValue = @UID
   BEGIN
    SET @flag = 1
	RETURN @flag
   END

  RETURN @flag
END
