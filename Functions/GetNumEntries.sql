USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[GetNumEntries]    Script Date: 1/3/2018 4:06:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[GetNumEntries]
(
	@string VARCHAR(MAX) = '',
  @delimiter VARCHAR(1) = ','
)
RETURNS INTEGER
AS
BEGIN
  DECLARE @substring VARCHAR(MAX),
          @numEntries INTEGER = 0,
          @pos INTEGER = 0

  SET @string = @string + @delimiter
  SET @pos = CHARINDEX(@delimiter, @string)
  WHILE (@pos <> 0)
	BEGIN
    SET @numEntries = @numEntries + 1
		SET @string = SUBSTRING(@string, @pos + 1, LEN(@string))
		SET @pos = CHARINDEX(@delimiter, @string)
	END
	-- Return the result of the function
	RETURN @numEntries

END
