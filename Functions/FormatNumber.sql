USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[FormatNumber]    Script Date: 9/19/2019 10:50:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[FormatNumber]
(
	-- Add the parameters for the function here
  @number DECIMAL(18,10),
	@precision INTEGER
)
RETURNS VARCHAR(30)
AS
BEGIN
  DECLARE @offset INTEGER = 0
  IF (@precision = 1)
    SET @offset = 1
  IF (@precision <= 0)
    SET @offset = 3

  RETURN SUBSTRING(CONVERT(VARCHAR,CONVERT(money,@number), 1), 1, LEN(CONVERT(VARCHAR,CONVERT(money,@number), 1)) - @offset)
END
