USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[PrintForExcel]    Script Date: 10/12/2018 9:31:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[PrintForExcel]
(
	-- Add the parameters for the function here
	@word VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	RETURN '="' + @word + '"'
END
