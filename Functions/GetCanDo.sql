/****** Object:  UserDefinedFunction [dbo].[GetCanDo]    Script Date: 6/19/2022 11:54:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[GetCanDo]
(
	-- Add the parameters for the function here
	@valueList VARCHAR(MAX) = '',
	@value     VARCHAR(MAX) = ''
)
RETURNS INTEGER
AS
BEGIN
	DECLARE @numEntries INTEGER = 0,
			@string     VARCHAR(100) = '',
			@index      INTEGER = 1,
			@pos        INTEGER = 0

	IF @valueList IS NULL
		SET @valueList = ''

	IF @value IS NULL
		SET @value = ''

	IF @valueList = '' AND @value = '' 
		RETURN 1

	SET @numEntries = [dbo].[GetNumEntries] (@ValueList, ',')

	WHILE @index <= @numentries
	BEGIN	
		SET @string = [dbo].[GetEntry](@index,@valueList,',')
		SET @index = @index + 1	

		IF @string = ''
			CONTINUE
					
		/* (user-ID) = This user has access.  */
		IF CHARINDEX('!', @string) = 0 AND CHARINDEX('*', @string) = 0 AND @string = @value
			RETURN 1

		/* (string*) = Users whose IDs begin with string have access. */		
		IF CHARINDEX('*' ,@string) = LEN(@string) AND @value LIKE (SUBSTRING(@string,1,(LEN(@string)-1)) + '%') 
			RETURN 1

		/* (!user) = This user does not have access. */
		IF CHARINDEX('!', @string) = 1 AND CHARINDEX('*', @string) = 0 AND SUBSTRING(@string, 2, LEN(@string)) = @value
			RETURN 0

		/* (!user*) = This user does not have access. */
		IF CHARINDEX('!' ,@string) = 1 AND CHARINDEX('*', @string) = LEN(@string) AND @value LIKE (SUBSTRING(@string,2,(LEN(@string)-2)) + '%') 
			RETURN 0

		/* (*) = All values have access.  */	
		IF @string = '*'
			RETURN 1

		/* (!*) = All values does not have access */
		IF @string = '!*'
			RETURN 0
			
		
	END
	RETURN 0
END