USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[GetInvoiceDetails]    Script Date: 11/8/2017 11:07:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[GetInvoiceDetails]
(
  @endDate DATETIME = NULL
)
RETURNS @invoiceTable TABLE 
(
  [apinvID] INTEGER,
  [refID] VARCHAR(100),
  [refCategory] VARCHAR(100),
  [asOfDate] DATETIME,
  [transDate] DATETIME,
  [pendingAmount] DECIMAL(18,2),
  [approvedAmount] DECIMAL(18,2),
  [completedAmount] DECIMAL(18,2),
  [stat] VARCHAR(20)
)
AS
BEGIN
  IF (@endDate IS NULL)
  BEGIN
    SET @endDate = GETDATE()
  END

  -- Insert all the invoices
  INSERT INTO @invoiceTable ([apinvID],[refID],[refCategory],[asOfDate],[transDate],[pendingAmount],[approvedAmount],[completedAmount],[stat])
  SELECT  [apinvID],
          [refID],
          [refCategory],
          @endDate,
          [invoiceDate],
          0,
          0,
          0,
          ''
  FROM    [apinv]
  WHERE   [dateReceived] <= @endDate

  -- update the invoices that are approved
  MERGE INTO @invoiceTable inv
       USING (
             SELECT [apinvID],
                    MAX([dateActed]) AS [transDate],
                    SUM([amount]) AS [amount]
             FROM   [apinva]
             WHERE  [stat] = 'A'
             AND    [dateActed] <= @endDate
             GROUP BY [apinvID]
             ) app
          ON app.[apinvID] = inv.[apinvID]
  WHEN MATCHED THEN
      UPDATE 
         SET [pendingAmount] = 0,
             [approvedAmount] = app.[amount],
             [completedAmount] = 0,
             [transDate] = app.[transDate],
             [stat] = 'A';
             
  -- update the invoices that are completed before end date
  MERGE INTO @invoiceTable inv
       USING (
             SELECT [apinvID],
                    MAX([transDate]) AS [transDate],
                    SUM([transAmount]) AS [amount]
             FROM   [aptrx]
             WHERE  [transDate] <= @endDate
             GROUP BY [apinvID]
             ) trx
          ON trx.[apinvID] = inv.[apinvID]
  WHEN MATCHED THEN
      UPDATE 
         SET [pendingAmount] = 0,
             [approvedAmount] = 0,
             [completedAmount] = trx.[amount],
             [transDate] = trx.[transDate],
             [stat] = 'C';

  -- any invoice that doesn't have a stat now must have been open at the time of the @endDate
  MERGE INTO @invoiceTable inv
       USING (
             SELECT [apinvID],
                    [invoiceDate] AS [invoiceDate],
                    [amount] AS [amount]
             FROM   [apinv]
             ) opn
          ON inv.[apinvID] = opn.[apinvID]
         AND inv.[stat] = ''
  WHEN MATCHED THEN
      UPDATE 
         SET [pendingAmount] = opn.[amount],
             [approvedAmount] = 0,
             [completedAmount] = 0,
             [stat] = 'O';

  RETURN
END
