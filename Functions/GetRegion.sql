/****** Object:  UserDefinedFunction [dbo].[GetRegion]    Script Date: 7/6/2022 2:00:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER FUNCTION [dbo].[GetRegion]
(	
  @stateID VARCHAR(2)
)
RETURNS VARCHAR(20)
AS
BEGIN
  DECLARE @return VARCHAR(20)

  select @return = state.region from state where state.stateID = @stateID
  RETURN @return
END
