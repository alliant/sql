USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[GetAlert]    Script Date: 9/29/2017 2:33:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER FUNCTION [dbo].[GetAlert]
(	
	@appCode VARCHAR(20),
  @objID VARCHAR(100)
)
RETURNS TABLE 
AS
RETURN 
(
  SELECT  [objValue]
  FROM    [dbo].[sysprop]
  WHERE   [appCode] = @appCode
  AND     [objAction] = 'Alert'
  AND     [objProperty] = 'Threshold'
  AND     [objID] = @objID
)
