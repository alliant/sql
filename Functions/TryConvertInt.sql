USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[TryConvertInt]    Script Date: 4/6/2018 12:52:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[TryConvertInt]
(
	-- Add the parameters for the function here
	@Value VARCHAR(100)
)
RETURNS INTEGER
AS
BEGIN
  SET @Value = REPLACE(@Value, ',', '')
  IF ISNUMERIC(@Value + 'e0') = 0 RETURN NULL
  IF ( CHARINDEX('.', @Value) > 0 AND CONVERT(bigint, PARSENAME(@Value, 1)) <> 0 ) RETURN NULL
  DECLARE @I BIGINT =
      CASE
      WHEN CHARINDEX('.', @Value) > 0 THEN CONVERT(bigint, PARSENAME(@Value, 2))
      ELSE CONVERT(bigint, @Value)
      END
  IF ABS(@I) > 2147483647 RETURN NULL
  RETURN @I
END
