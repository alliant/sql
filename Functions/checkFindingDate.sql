USE [COMPASS_ALFA]
GO
/****** Object:  UserDefinedFunction [dbo].[checkFindingDate]    Script Date: 7/27/2022 10:07:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[checkFindingDate]
(
	-- Add the parameters for the function here
	@year INTEGER = 0,
	@month INTEGER = 0,
	@createDate DATETIME, 
	@closedDate DATETIME,
	@stat VARCHAR(10) = ''
)
RETURNS BIT
AS
BEGIN
    
	-- Add the T-SQL statements to compute the return value here

	IF @stat = 'C' 
	BEGIN 
	    IF( @year > 0 AND @year <> YEAR(@closedDate) AND ( @month > 0 AND @month <> MONTH(@closedDate) ))
	    RETURN 0
     END
	ELSE
	BEGIN
	    IF( @year > 0 AND @year <> YEAR(@createDate) AND ( @month > 0 AND @month <> MONTH(@createDate) ))
	   RETURN 0	     
     END

	

	-- Return the result of the function
	RETURN 1

END
