USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[GetEmail]    Script Date: 12/21/2015 10:22:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		  John Oliver
-- Create date: 12/21/2015
-- Description:	Get the user's email address
-- =============================================
ALTER FUNCTION [dbo].[GetEmail] (@name varchar(200))
RETURNS varchar(200)
AS
BEGIN
  RETURN dbo.GetUID(@name) + '@alliantnational.com'
END
