USE [COMPASS]
GO
/****** Object:  Function [dbo].[hasAllAffiliates]    Script Date: 22-09-2020 13:59:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sachin Chaturvedi
-- Create date: 22-09-2020
-- Description:	Checks if agent has all tags as in the input tag list
-- Modification: 
-- ============================================= 

CREATE FUNCTION dbo.hasAllAffiliates(
    @tAffPeopleList VARCHAR(MAX) = '',
    @agentID        VARCHAR(MAX) = '',
	@paramState     VARCHAR(MAX) = '',  /* state passed as parameter */
	@agentState     VARCHAR(MAX) = ''   /* current agent state       */
)
RETURNS BIT
AS 
BEGIN
    DECLARE @index       INTEGER      = 0,
			@affPersonID VARCHAR(MAX) = '',
			@flag        BIT          = 0 

	WHILE @index < [dbo].[GetNumEntries] (@tAffPeopleList, DEFAULT)
    BEGIN
      SET @index = @index + 1
      SET @affPersonID = [dbo].[GetEntry] (@index, @tAffPeopleList, DEFAULT)

      IF NOT EXISTS(SELECT  af.[affiliationID] 
				     FROM [dbo].[affiliation] af
				     WHERE af.[partnerBType] = 'P'          AND
				           af.[partnerB] IN (@affPersonID)  AND
					       af.[partnerA] IN(
										SELECT ogr.[orgID]         FROM 
											   [dbo].[orgrole] ogr WHERE
											   ogr.[stateID]  = CASE WHEN @paramState != '' THEN 
																   @paramState 
																 ELSE
																   @agentState
																 END              AND
												ogr.[source]   = 'Agent'          AND
												ogr.[sourceID] = @agentID
										)		
			  )
	   RETURN 0
    END

	SET @flag = 1
    RETURN @flag
END;