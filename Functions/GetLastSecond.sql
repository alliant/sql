USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[GetLastSecond]    Script Date: 11/6/2019 2:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[GetLastSecond] 
(
	-- Add the parameters for the function here
	@date AS DATETIME
)
RETURNS DATETIME
AS
BEGIN
  DECLARE @return DATETIME = NULL

  IF (@date IS NULL)
    SET @date = GETDATE()

  SET @date = CONVERT(DATE,@date)

  SET @return = DATEADD(SECOND,-1,DATEADD(d,1,@date))

	-- Return the result of the function
	RETURN @return

END
