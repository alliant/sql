GO
/****** Object:  UserDefinedFunction [dbo].[Maximum]    Script Date: 6/4/2021 11:27:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[Maximum]
(
	-- Add the parameters for the function here
	@val1 integer,
  @val2 integer
)
RETURNS integer
AS
BEGIN
	if @val1 > @val2
    return @val1
  return isnull(@val2,@val1)

END
