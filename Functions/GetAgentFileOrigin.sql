USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[GetAgentFileOrigin]    Script Date: 2/8/2018 3:23:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[GetAgentFileOrigin]
(
	-- Add the parameters for the function here
	@cAgentID VARCHAR(50) = '',
    @cFileID VARCHAR(50) = '',
	@dtfirstPolicyIssueDt datetime,
	@dtfirstCPLIssueDt datetime
)
RETURNS VARCHAR(50)
AS
BEGIN
  DECLARE @fOrigin VARCHAR(50) = '',
		  @cSourceType VARCHAR(50) = '',
		  @dtfirstBatchPost datetime,
		  @dtfirstArtran datetime,
          @dtLeastDt datetime		  

  SELECT @dtfirstBatchPost = min(b.postDate) from batch b inner join 
							      batchform bf on b.batchid = bf.batchid 
								  WHERE b.agentID = @cAgentID and
								  bf.fileID = @cFileID
								  
   SELECT @dtfirstArtran = min(ar.trandate) from artran ar 
								  WHERE ar.entityID = @cAgentID and
								  ar.fileID = @cFileID 

  SELECT @cSourceType = max(at.SourceType)
   FROM artran at
    INNER JOIN(select  min(ar.trandate) as td from artran ar 
			    WHERE ar.entityID = @cAgentID and
				ar.fileID = @cFileID)b 
	on at.trandate = b.td
	 where at.entityID = @cAgentID and
		   at.fileID = @cFileID
  
  SELECT
  @dtLeastDt = (SELECT MIN(x) FROM (VALUES (@dtfirstPolicyIssueDt),(@dtfirstCPLIssueDt),(@dtfirstBatchPost),(@dtfirstArtran)) AS value(x))

   							  
		  
  SET @fOrigin = (case when @dtLeastDt IS NOT NULL 
                   then 
				    case when @dtLeastDt = @dtfirstPolicyIssueDt then 'Policy'
					     when @dtLeastDt = @dtfirstCPLIssueDt    then 'CPL'
						 when @dtLeastDt = @dtfirstBatchPost     then 'Batch'
						 when @dtLeastDt = @dtfirstArtran        
						  then 
						   case when @cSourceType = 'C' then 'Credit'
						        when (@cSourceType = 'P') or (@cSourceType = 'D') then 'Payment' 
						   end						  
					end
				   else ''	
			      end
				 )
				 
  RETURN @fOrigin
END
