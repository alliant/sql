USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[GetMaximum]    Script Date: 5/7/2018 10:00:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[GetMaximum] 
(
	@number1 DECIMAL(18,2) = 0,
  @number2 DECIMAL(18,2) = 0
)
RETURNS DECIMAL(18,2)
AS
BEGIN
	DECLARE @maximum DECIMAL(18,2)
  SELECT @maximum=CASE WHEN @number1 > @number2 THEN @number1 ELSE @number2 END
  RETURN @maximum

END