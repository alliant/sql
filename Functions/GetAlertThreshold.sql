USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[GetAlertThreshold]    Script Date: 2/8/2018 3:23:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[GetAlertThreshold]
(
	-- Add the parameters for the function here
	@processCode VARCHAR(100) = '',
  @score DECIMAL(18,2) = 0
)
RETURNS DECIMAL(18,2)
AS
BEGIN
	DECLARE @threshold VARCHAR(100) = 0,
          @threshold1 DECIMAL(18,2) = 0,
          @threshold2 DECIMAL(18,2) = 0,
          @greaterThan BIT = 0,
          @return DECIMAL(18,2) = 0
          
  SELECT @threshold=[objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'AMD' AND [objAction] = 'Alert' AND [objProperty] = 'Threshold' AND [objID] = @processCode
    
  SET @return = 0
  IF (@threshold <> 'User Defined')
  BEGIN
    SET @threshold2 = CONVERT(DECIMAL(18,2),[dbo].[GetEntry] (1, @threshold, DEFAULT))
    SET @threshold1 = CONVERT(DECIMAL(18,2),[dbo].[GetEntry] (2, @threshold, DEFAULT))
    SET @greaterThan = [dbo].[GetAlertGreaterOrLessThan] (@processCode)
    IF (@greaterThan = 1)
    BEGIN
      IF (@score >= @threshold1)
        SET @return = @threshold1
      IF (@score >= @threshold2)
        SET @return = @threshold2
    END
    ELSE
    BEGIN
      IF (@score <= @threshold2)
        SET @return = @threshold2
      IF (@score <= @threshold1)
        SET @return = @threshold1
    END
  END

  RETURN @return
END
