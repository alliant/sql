USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[GetInitials]    Script Date: 12/21/2015 10:20:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		  John Oliver
-- Create date: 12/21/2015
-- Description:	Get the user's initials
-- =============================================
ALTER FUNCTION [dbo].[GetInitials] (@name varchar(200))
RETURNS varchar(200)
AS
BEGIN
  DECLARE @first VARCHAR(100),
          @last VARCHAR(100),
          @space int

  SET @space = CHARINDEX(' ',@name,1)
  SET @first = SUBSTRING(@name,1,@space)
  SET @last = SUBSTRING(@name,@space + 1,LEN(@name) - @space)
  RETURN UPPER(SUBSTRING(@first,1,1)) + UPPER(SUBSTRING(@last,1,1))
END
