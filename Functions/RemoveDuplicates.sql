-- ================================================
-- Template generated from Template Explorer using:
-- Create Scalar Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION RemoveDuplicates 
(
	-- Add the parameters for the function here
	@string VARCHAR(MAX),
  @delimiter VARCHAR(100)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
  IF (@string IS NULL)
    RETURN ''

	DECLARE @pos INT = 0,
          @temp VARCHAR(100) = '',
          @new VARCHAR(MAX) = ''

  SET @string = @string + @delimiter
  SET @Pos = CHARINDEX(@delimiter, @string)
  WHILE (@Pos > 0)
  BEGIN
	  SET @temp = SUBSTRING(@string, 1, @pos - 1)
    IF (PATINDEX('%' + @temp + '%', @new) = 0)
      SET @new = @new + @temp + @delimiter
	  SET @string = TRIM(SUBSTRING(@string, @pos + 1, LEN(@string)))
	  SET @pos = CHARINDEX(@delimiter, @string)
  END
  SET @new = SUBSTRING(@new, 1, LEN(@new) - LEN(@delimiter))
  RETURN @new
END
GO

