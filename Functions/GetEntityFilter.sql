USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[GetEntityFilter]    Script Date: 3/19/2018 10:39:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER FUNCTION [dbo].[GetEntityFilter]
(	
	-- Add the parameters for the function here
	@entity VARCHAR(30) = '',
  @entityList VARCHAR(MAX) = ''
)
RETURNS @entityTable TABLE ([field] VARCHAR(100))
AS
BEGIN
  DECLARE @NextString NVARCHAR(100),
          @Pos INTEGER = 0,
          @Str NVARCHAR(MAX),
          @Delimiter NCHAR(1) = ','

	-- Add the SELECT statement with parameter references here
	IF (@entityList != 'ALL')
  BEGIN
    SET @Str = @entityList + @Delimiter
    SET @Pos = CHARINDEX(@Delimiter, @Str)
    WHILE (@Pos <> 0)
	  BEGIN
		  SET @NextString = SUBSTRING(@Str,1,@Pos-1)
      INSERT INTO @entityTable ([field])
      SELECT @NextString
		  SET @Str = SUBSTRING(@Str,@Pos+1,LEN(@Str))
		  SET @Pos = CHARINDEX(@Delimiter, @Str)
	  END
  END
  ELSE
  BEGIN
    IF (@entity = 'Agent')
    BEGIN
      INSERT INTO @entityTable ([field])
      SELECT [agentID] FROM [dbo].[agent]
    END
    IF (@entity = 'Attorney')
    BEGIN
      INSERT INTO @entityTable ([field])
      SELECT [attorneyID] FROM [dbo].[attorney]
    END
    IF (@entity = 'State')
    BEGIN
      INSERT INTO @entityTable ([field])
      SELECT [stateID] FROM [dbo].[state]
    END
    IF (@entity = 'AgentManager')
    BEGIN
      INSERT INTO @entityTable ([field])
      SELECT [objID] FROM [dbo].[sysprop] WHERE [appCode] = 'AMD' AND [objAction] = 'AgentManager' AND [objProperty] = ''
    END
    IF (@entity = 'ActionStatus')
    BEGIN
      INSERT INTO @entityTable ([field])
      SELECT [objID] FROM [dbo].[sysprop] WHERE [appCode] = 'CAM' AND [objAction] = 'Action' AND [objProperty] = 'Status'
    END
  END
  RETURN
END
