USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[GetAlertAutoClose]    Script Date: 8/21/2018 3:49:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[GetAlertAutoClose]
(
	-- Add the parameters for the function here
	@processCode VARCHAR(30)
)
RETURNS BIT
AS
BEGIN
  DECLARE @return BIT = 0

  SELECT  @return = CONVERT(BIT,[dbo].[GetEntry] (2, [objDesc], ','))
  FROM    [dbo].[sysprop]
  WHERE   [appCode] = 'AMD'
  AND     [objAction] = 'Alert'
  AND     [objProperty] = 'Threshold' 
  AND     [objID] = @processCode
	-- Return the result of the function
	RETURN @return

END
