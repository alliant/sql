USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[GetActivityTable]    Script Date: 9/9/2019 4:47:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER FUNCTION [dbo].[GetActivityTable]
(
  @category VARCHAR(50),
  @year INTEGER
)
RETURNS @activityTable TABLE 
(
	[activityID] [int] NOT NULL,
	[name] [varchar](200) NULL,
	[agentID] [varchar](50) NULL,
	[stateID] [varchar](50) NULL,
	[year] [int] NULL,
	[stat] [varchar](50) NULL,
	[category] [varchar](50) NULL,
	[type] [varchar](50) NULL,
  [corporationID] [varchar](200) NULL,
  [sortOrder] [int] NULL,
	[month1] [decimal](18, 2) NULL,
	[month2] [decimal](18, 2) NULL,
	[month3] [decimal](18, 2) NULL,
	[month4] [decimal](18, 2) NULL,
	[month5] [decimal](18, 2) NULL,
	[month6] [decimal](18, 2) NULL,
	[month7] [decimal](18, 2) NULL,
	[month8] [decimal](18, 2) NULL,
	[month9] [decimal](18, 2) NULL,
	[month10] [decimal](18, 2) NULL,
	[month11] [decimal](18, 2) NULL,
	[month12] [decimal](18, 2) NULL
)
AS
BEGIN
  IF (@category = '')
    SET @category = 'ALL'

  INSERT INTO @activityTable ([activityID],[agentID],[name],[stateID],[year],[type],[stat],[category],[corporationID],[sortOrder],[month1],[month2],[month3],[month4],[month5],[month6],[month7],[month8],[month9],[month10],[month11],[month12])
  SELECT  [activityID],
          aa.[agentID],
          a.[name],
          a.[stateID],
          aa.[year],
          aa.[type],
          aa.[stat],
          aa.[category],
          CASE WHEN a.[corporationID] = '' THEN a.[agentID] ELSE a.[corporationID] END AS [corporationID],
          p.[objRef] AS [sortOrder],
          aa.[month1],
          aa.[month2],
          aa.[month3],
          aa.[month4],
          aa.[month5],
          aa.[month6],
          aa.[month7],
          aa.[month8],
          aa.[month9],
          aa.[month10],
          aa.[month11],
          aa.[month12]
  FROM    [dbo].[agentactivity] aa INNER JOIN
          [dbo].[agent] a
  ON      aa.[agentID] = a.[agentID] INNER JOIN
          [dbo].[sysprop] p
  ON      aa.[category] = p.[objID]
  AND     p.[appCode] = 'AMD'
  AND     p.[objAction] = 'Activity'
  AND     p.[objProperty] = 'Category'
  WHERE   aa.[agentID] != ''
  
  -- Create a CPL Net category
  INSERT INTO @activityTable ([activityID],[agentID],[name],[stateID],[year],[type],[stat],[category],[corporationID],[sortOrder],[month1],[month2],[month3],[month4],[month5],[month6],[month7],[month8],[month9],[month10],[month11],[month12])
  SELECT  0,
          aa.[agentID],
          a.[name],
          a.[stateID],
          aa.[year],
          'A' AS [type],
          'C' AS [stat],
          p.[objID] AS [category],
          CASE WHEN a.[corporationID] = '' THEN a.[agentID] ELSE a.[corporationID] END AS [corporationID],
          p.[objRef] AS [sortOrder],
          aa.[month1],
          aa.[month2],
          aa.[month3],
          aa.[month4],
          aa.[month5],
          aa.[month6],
          aa.[month7],
          aa.[month8],
          aa.[month9],
          aa.[month10],
          aa.[month11],
          aa.[month12]
  FROM    (
          SELECT  i.[agentID],
                  i.[year],
                  'T' AS [category],
                  i.[month1]  - (v.[month1]  + r.[month1])  AS [month1],
                  i.[month2]  - (v.[month2]  + r.[month2])  AS [month2],
                  i.[month3]  - (v.[month3]  + r.[month3])  AS [month3],
                  i.[month4]  - (v.[month4]  + r.[month4])  AS [month4],
                  i.[month5]  - (v.[month5]  + r.[month5])  AS [month5],
                  i.[month6]  - (v.[month6]  + r.[month6])  AS [month6],
                  i.[month7]  - (v.[month7]  + r.[month7])  AS [month7],
                  i.[month8]  - (v.[month8]  + r.[month8])  AS [month8],
                  i.[month9]  - (v.[month9]  + r.[month9])  AS [month9],
                  i.[month10] - (v.[month10] + r.[month10]) AS [month10],
                  i.[month11] - (v.[month11] + r.[month11]) AS [month11],
                  i.[month12] - (v.[month12] + r.[month12]) AS [month12]
          FROM    [dbo].[agentactivity] i INNER JOIN
                  [dbo].[agentactivity] v
          ON      i.[agentID] = v.[agentID]
          AND     i.[year] = v.[year]
          AND     i.[type] = v.[type] INNER JOIN
                  [dbo].[agentactivity] r
          ON      i.[agentID] = r.[agentID]
          AND     i.[year] = r.[year]
          AND     i.[type] = r.[type]
          WHERE   i.[category] = 'I'
          AND     v.[category] = 'V'
          AND     r.[category] = 'R'
          ) aa INNER JOIN
          [dbo].[agent] a
  ON      aa.[agentID] = a.[agentID] INNER JOIN
          [dbo].[sysprop] p
  ON      aa.[category] = p.[objID]
  AND     p.[appCode] = 'AMD'
  AND     p.[objAction] = 'Activity'
  AND     p.[objProperty] = 'Category'
  WHERE   aa.[agentID] != ''

  IF (@category <> 'ALL')
    DELETE FROM @activityTable WHERE [category] <> @category

  IF (@year > 0)
    DELETE FROM @activityTable WHERE [year] <> @year

  RETURN
END
