USE [alliant]
GO
/****** Object:  UserDefinedFunction [dbo].[GetAPIVendorName]    Script Date: 11/29/2018 8:13:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[GetAPIVendorName] 
(
	-- Add the parameters for the function here
	@vendor INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN

  DECLARE @vendorName VARCHAR(MAX)
  DECLARE @vendorColumns AS VARCHAR(MAX)

  -- setup the vendor table
  DECLARE @vendorTable TABLE (id INT, name VARCHAR(MAX))
  INSERT INTO @vendorTable (id,name) VALUES (1,'Resware')
  INSERT INTO @vendorTable (id,name) VALUES (2,'RamQuest')
  INSERT INTO @vendorTable (id,name) VALUES (3,'SoftPro')
  INSERT INTO @vendorTable (id,name) VALUES (4,'LandTech')
  INSERT INTO @vendorTable (id,name) VALUES (5,'Driggs')
  INSERT INTO @vendorTable (id,name) VALUES (7,'E-Closing')
  INSERT INTO @vendorTable (id,name) VALUES (8,'ClosersChoice')
  INSERT INTO @vendorTable (id,name) VALUES (9,'Qualia')
 
  SELECT @vendorColumns = COALESCE(@vendorColumns + ',','') + '''' + ([name]) + '''' FROM (SELECT DISTINCT [name] FROM @vendorTable) AS a
  INSERT INTO @vendorTable (id,name) VALUES (0,@vendorColumns)

  SELECT @vendorName = [name] FROM @vendorTable WHERE [id] = @vendor

  IF (@vendor != 0)
  BEGIN
    SET @vendorName = '''' + @vendorName + ''''
  END

	-- Return the result of the function
	RETURN @vendorName

END
