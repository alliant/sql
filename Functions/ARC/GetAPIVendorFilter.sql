USE [alliant]
USE [alliant_test]
USE [dev_alliant]
GO
/****** Object:  UserDefinedFunction [dbo].[GetAPIVendorType]    Script Date: 7/15/2016 12:05:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[GetAPIVendorFilter] 
(
	-- Add the parameters for the function here
	@typeName VARCHAR(50),
  @year INT = 0,
  @month INT = 0
)
RETURNS NVARCHAR(500)
AS
BEGIN

  DECLARE @sql NVARCHAR(500) = ''

  -- add a clause to get the proper year
  IF (@year > 0)
  BEGIN
    SET @sql = @sql + '
    AND     YEAR([TimeStamp]) = ' + CONVERT(VARCHAR,@year)
  END
  -- add a clause to get the proper month
  IF (@month > 0)
  BEGIN
    SET @sql = @sql + '
    AND     MONTH([TimeStamp]) = ' + CONVERT(VARCHAR,@month)
  END
  --add a clause to get the spefic transactions
  IF (@typeName <> 'all')
  BEGIN
    SET @sql = @sql + '
    AND     [TransactionServiceType] LIKE ''' + @typeName + '%'''
  END

	-- Return the result of the function
	RETURN @sql

END
