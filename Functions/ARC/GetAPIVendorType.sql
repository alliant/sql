USE [alliant]
GO
/****** Object:  UserDefinedFunction [dbo].[GetAPIVendorType]    Script Date: 7/15/2016 12:05:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[GetAPIVendorType] 
(
	-- Add the parameters for the function here
	@type INT
)
RETURNS VARCHAR(50)
AS
BEGIN

  DECLARE @vendorType VARCHAR(50)

  -- setup the vendor table
  DECLARE @typeTable TABLE (id INT, name VARCHAR(50))
  INSERT INTO @typeTable (id,name) VALUES (0,'all')
  INSERT INTO @typeTable (id,name) VALUES (1,'cpl')
  INSERT INTO @typeTable (id,name) VALUES (2,'policy')

  SELECT @vendorType = [name] FROM @typeTable WHERE [id] = @type

	-- Return the result of the function
	RETURN @vendorType

END
