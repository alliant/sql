USE [alliant]
GO
/****** Object:  UserDefinedFunction [dbo].[GetAPIVendorType]    Script Date: 7/15/2016 12:05:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[GetAPIVendorStatus] 
(
	-- Add the parameters for the function here
	@status VARCHAR(50)
)
RETURNS VARCHAR(50)
AS
BEGIN

  DECLARE @newStatus VARCHAR(50)

  -- setup the vendor table
  SELECT  @newStatus = 
          CASE substring(@status,1,3)
            WHEN 'cpl' THEN 'CPL ' + substring(@status,4,50)
            WHEN 'pol' THEN 'Policy Jacket ' + substring(@status,7,50)
            ELSE @status
          END

	-- Return the result of the function
	RETURN @newStatus

END
