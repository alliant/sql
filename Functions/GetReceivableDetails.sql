USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[GetReceivableDetails]    Script Date: 9/29/2017 2:34:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[GetReceivableDetails]
(
  @endDate DATETIME = NULL
)
RETURNS @invoiceTable TABLE 
(
  [arinvID] INTEGER,
  [refID] VARCHAR(100),
  [asOfDate] DATETIME,
  [transDate] DATETIME,
  [originalAmount] DECIMAL(18,2),
  [pendingAmount] DECIMAL(18,2),
  [completedAmount] DECIMAL(18,2),
  [waivedAmount] DECIMAL(18,2),
  [stat] VARCHAR(20)
)
AS
BEGIN
  IF (@endDate IS NULL)
  BEGIN
    SET @endDate = GETDATE()
  END

  -- Insert all the invoices
  INSERT INTO @invoiceTable ([arinvID],[refID],[asOfDate],[transDate],[originalAmount],[pendingAmount],[completedAmount],[waivedAmount],[stat])
  SELECT  [arinvID],
          [refID],
          @endDate,
          [dateRequested],
          [requestedAmount],
          0,
          0,
          0,
          ''
  FROM    [arinv]
  WHERE   [dateRequested] <= @endDate
             
  -- update the invoices that are fully or partially completed at the time of @endDate
  MERGE INTO @invoiceTable inv
       USING (
             SELECT inv.[arinvID],
                    inv.[requestedAmount],
                    trx.[transDate],
                    trx.[amount] AS [paidAmount],
                    inv.[waivedAmount] AS [waivedAmt]
             FROM   [arinv] inv INNER JOIN
                    (
                    SELECT  [arinvID],
                            MAX([transDate]) AS [transDate],
                            SUM([transAmount]) AS [amount]
                    FROM    [artrx]
                    WHERE   [transDate] <= @endDate
                    GROUP BY [arinvID]
                    ) trx
             ON     inv.[arinvID] = trx.[arinvID]
             ) trx
          ON inv.[arinvID] = trx.[arinvID]
  WHEN MATCHED THEN
      UPDATE 
         SET [pendingAmount] = CASE WHEN [waivedAmt] = 0
                                THEN [requestedAmount] - [paidAmount]
                                ELSE 0
                               END,
             [completedAmount] = [paidAmount],
             [waivedAmount] = [waivedAmt],
             [transDate] = trx.[transDate],
             [stat] = CASE 
                       WHEN [requestedAmount] = [paidAmount] THEN 'C' 
                       ELSE 
                        CASE WHEN  [waivedAmt] = 0
                         THEN 'O'
                         ELSE 'W'
                        END
                      END;
                  
  -- update the invoices that are only open at the time of @endDate
  MERGE INTO @invoiceTable inv
       USING (
             SELECT inv.[arinvID],
                    inv.[requestedAmount]
             FROM   [arinv] inv
             ) opn
          ON inv.[arinvID] = opn.[arinvID]
         AND inv.[stat] = ''
  WHEN MATCHED THEN
      UPDATE 
         SET [pendingAmount] = [requestedAmount],
             [completedAmount] = 0,
             [waivedAmount] = 0,
             [stat] = 'O';

  RETURN
END
