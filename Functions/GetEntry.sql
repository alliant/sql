-- ================================================
-- Template generated from Template Explorer using:
-- Create Scalar Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION GetEntry
(
	@index INTEGER = 0,
  @string VARCHAR(MAX) = '',
  @delimiter VARCHAR(1) = ','
)
RETURNS VARCHAR(MAX)
AS
BEGIN
  DECLARE @tempString VARCHAR(MAX) = '',
          @word VARCHAR(MAX) = '',
          @pos INTEGER = 0,
          @iter INTEGER = 0

  SET @string = @string + @delimiter
  WHILE @iter < @index
  BEGIN
    SET @pos = CHARINDEX(@delimiter, @string)
    IF @pos = 0
      BREAK

		SET @word = SUBSTRING(@string,1,@pos-1)
		SET @string = SUBSTRING(@string,@pos+1,LEN(@string))
    SET @iter = @iter + 1
  END

  RETURN @word
END
GO

