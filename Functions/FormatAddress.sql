GO
/****** Object:  UserDefinedFunction [dbo].[FormatAddress]    Script Date: 5/16/2023 11:56:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:    <Author,,Name>
-- Create date: <Create Date, ,>
-- Description: <Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[FormatAddress]
(
  -- Add the parameters for the function here
  @add1 VARCHAR(200),
  @add2 VARCHAR(200) = '',
  @add3 VARCHAR(200) = '',
  @add4 VARCHAR(200) = '',
  @city VARCHAR(100),
  @state VARCHAR(50)
)
RETURNS VARCHAR(2000)
AS
BEGIN
  -- Declare the return variable here
  DECLARE @Formatted VARCHAR(2000)

  SET @Formatted = @add1
  
  IF (@add2 <> '' AND @add2 IS NOT NULL)
    SET @Formatted = @Formatted + ' ' + @add2
  
  IF (@add3 <> '' AND @add3 IS NOT NULL)
    SET @Formatted = @Formatted + ' ' + @add3
  
  IF (@add4 <> '' AND @add4 IS NOT NULL)
    SET @Formatted = @Formatted + ' ' + @add4

 SET @Formatted = @Formatted + ', ' + @city + ', ' + @state

  -- Return the result of the function
  RETURN @Formatted

END
