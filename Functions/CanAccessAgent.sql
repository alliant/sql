USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[CanAccessAgent]    Script Date: 17-08-2020 19:25:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author     : VJ
-- Create date: 13-08-2020
-- Description:	This function check whether a userId 
--              have access to particular agent or not
-- =============================================
ALTER FUNCTION [dbo].[CanAccessAgent]
(
  -- Add the parameters for the function here
  @UID VARCHAR(100)    = '',
  @AgentID VARCHAR(20) = ''
)
RETURNS BIT
AS
BEGIN
--variable declaration
DECLARE @objManagerProperty NVARCHAR(100),
        @objStateProperty NVARCHAR(100),
        @objManagerValue NVARCHAR(100),
		@objStateValue NVARCHAR(100),
		@role        NVARCHAR(100),
		@flag        BIT

	SET @flag = 0

    --If userId or agnetID is empty then return 0 
	IF @UID = '' OR @AgentID = ''
	 RETURN 0

	--IF role equals to 'Administrator' then access all agent 
	SELECT @role =  sysuser.role FROM sysuser WHERE sysuser.uid = @UID
	
	IF @role LIKE '%' + 'Administrator' + '%'
	 RETURN 1
   
	--Getting objectProperty like  manager and object value list according to state or manager 
	SELECT  @objManagerProperty = sysprop.objProperty , @objManagerValue = sysprop.objValue FROM sysprop WHERE sysprop.appcode = 'SYS' 
	                                                                                            AND sysprop.objAction = 'AgentFilter' 
																								AND sysprop.objProperty = 'Manager' 
																								AND sysprop.objId     = @UID

	--Getting objectProperty like state and object value list according to state or manager 	
    SELECT  @objStateProperty = sysprop.objProperty , @objStateValue = sysprop.objValue FROM sysprop WHERE sysprop.appcode = 'SYS' 
	                                                                                            AND sysprop.objAction = 'AgentFilter' 
																								AND sysprop.objProperty = 'State'
																								AND sysprop.objId     = @UID
																								
	-- IF objproperty or objvalue equals  to empty then
	IF (@objManagerProperty = '' AND  @objStateProperty = '') OR (@objManagerValue = '' AND @objStateValue = '')
	 RETURN 0

        --If sysprop.objProperty equals to manager
	IF @objManagerProperty = 'Manager'
	 BEGIN
	  IF EXISTS(SELECT * FROM agentmanager WHERE agentmanager.agentID = @AgentID  
	                                        AND agentmanager.stat = 'A'
                                            AND agentmanager.uid  = @objManagerValue) 
           SET @flag = 1
	 END
	
	--If sysprop.objProperty equals to state   
	IF @objStateProperty = 'State' AND @objStateValue  != '!*' 
	 BEGIN
	  IF EXISTS(SELECT agentID stateID FROM agent WHERE agent.agentID = @AgentID 
	                                                AND ((@objStateValue  LIKE '%' + agent.stateid + '%') 
													OR  ((@objStateValue  LIKE '%' + '*'  + '%' ) AND (@objStateValue  NOT LIKE '%' + '!' + '%')) ) )
            SET @flag = 1
	 END 

  RETURN @flag
END
