USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[GetAgentFileType]    Script Date: 2/8/2018 3:23:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[GetAgentFileType]
(
	-- Add the parameters for the function here
	@cAgentID VARCHAR(50) = '',
    @cFileID VARCHAR(50) = ''
)
RETURNS VARCHAR(50)
AS
BEGIN
  DECLARE 
      @totalLoanPolicy INTEGER,
	  @totalOwnerPolicy INTEGER,
	  @fType VARCHAR(MAX) = ''
	  
	/* counting the number of loan policies issued on a file with policy */   
  
   SET @totalLoanPolicy = (select count(*) from batch b inner join batchform bf 
                            on b.batchID = bf.batchID 
							inner join stateform sf 
							 on b.stateID = sf.stateID AND 
							    bf.formID = sf.formID 
							where b.agentID = @cAgentID 
							  and bf.fileID = @cFileID 
							  /* and b.stateID = #policyData.stateID */ 
							  and bf.formType = 'P' 
							  and sf.insuredType = 'L' 
							group by /* b.stateID,*/ b.agentID, bf.fileID)  

 

  /* counting the number of owner policies issued on a file with policy */ 
  
   SET @totalOwnerPolicy = (select count(*) from batch b inner join batchform bf
							on b.batchID = bf.batchID 
							inner join stateform sf 
							on b.stateID = sf.stateID AND 
							    bf.formID = sf.formID 
						    where b.agentID = @cAgentID 
							 and bf.fileID = @cFileID
							 /* and b.stateID = #policyData.stateID */
							 and bf.formType = 'P' 
							 and sf.insuredType = 'O' 
						  group by /* b.stateID,*/ b.agentID, bf.fileID)  
						  
	SET @fType = (case when @totalLoanPolicy > 0 and @totalOwnerPolicy > 0 then 'Purchase'
					when @totalLoanPolicy > 0 then 'Refinance'
					when @totalOwnerPolicy > 0 then 'Cash Purchase' end)				  

  RETURN @fType
END
