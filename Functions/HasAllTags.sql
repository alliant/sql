USE [COMPASS]
GO
/****** Object:  Function [dbo].[hasAllTags]    Script Date: 22-09-2020 13:09:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sachin Chaturvedi
-- Create date: 22-09-2020
-- Description:	Checks if agent has all tags as in the input tag list
-- Modification: 
-- ============================================= 
  
CREATE FUNCTION dbo.hasAllTags(
    @tList   VARCHAR(MAX) = '',
    @agentID VARCHAR(MAX) = ''	
)
RETURNS bit
AS 
BEGIN
    DECLARE @index  INTEGER      = 0,
			@tagVal VARCHAR(MAX) = '',
			@flag   BIT          = 0 

	WHILE @index < [dbo].[GetNumEntries] (@tList, DEFAULT)
    BEGIN
      SET @index = @index + 1
      SET @tagVal = [dbo].[GetEntry] (@index, @tList, DEFAULT)
      IF NOT EXISTS(SELECT  t.[tagID] 
			     	FROM [dbo].[tag] t
			  	    WHERE t.[name]   = @tagVal     AND
					      t.[entity] = 'A'         AND
					      t.[entityID] = @agentID)
	   RETURN 0
    END
	SET @flag = 1
    RETURN @flag
END; 

