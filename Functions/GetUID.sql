USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[GetUID]    Script Date: 12/21/2015 10:03:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		  John Oliver
-- Create date: 12/21/2015
-- Description:	Get the user's uid
-- =============================================
ALTER FUNCTION [dbo].[GetUID] (@name varchar(200))
RETURNS varchar(200)
AS
BEGIN
  DECLARE @first VARCHAR(100),
          @last VARCHAR(100),
          @space int

  SET @space = CHARINDEX(' ',@name,1)
  SET @first = SUBSTRING(@name,1,@space)
  SET @last = SUBSTRING(@name,@space + 1,LEN(@name) - @space)
  RETURN LOWER(SUBSTRING(@first,1,1)) + LOWER(@last)
END
