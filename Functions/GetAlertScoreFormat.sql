USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[GetAlertScoreFormat]    Script Date: 2/8/2018 3:13:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[GetAlertScoreFormat] 
(
	-- Add the parameters for the function here
  @processCode VARCHAR(100) = '',
  @score DECIMAL(18,2) = 0
)
RETURNS VARCHAR(100)
AS
BEGIN
	DECLARE @return VARCHAR(100) = ''
  
  IF (@return <> 'User Defined')
  BEGIN
    SELECT  @return = CASE [type]
                        WHEN 'money' THEN '$ ' + REPLACE(CONVERT(VARCHAR, CONVERT(MONEY, FLOOR(@score)), 1), '.00', '')
                        WHEN 'percent' THEN CONVERT(VARCHAR, CONVERT(INTEGER, @score * 100)) + '%'
                        WHEN 'integer' THEN CONVERT(VARCHAR, CONVERT(INTEGER, @score))
                        WHEN 'decimal' THEN CONVERT(VARCHAR, @score)
                      END
    FROM    [dbo].[syscode]
    WHERE   [codeType] = 'Alert'
    AND     [code] = @processCode
  END
      
	-- Return the result of the function
	RETURN @return

END
