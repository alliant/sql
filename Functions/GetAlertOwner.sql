USE [COMPASS]
GO
/****** Object:  UserDefinedFunction [dbo].[GetAlertOwner]    Script Date: 2/8/2018 3:37:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[GetAlertOwner]
(
	-- Add the parameters for the function here
	@processCode VARCHAR(100) = ''
)
RETURNS VARCHAR(100)
AS
BEGIN
	DECLARE @return VARCHAR(100) = ''

	-- Add the T-SQL statements to compute the return value here
	SELECT @return=[objName] FROM [dbo].[sysprop] WHERE [appCode] = 'AMD' AND [objAction] = 'Alert' AND [objProperty] = 'Threshold' AND [objID] = @processCode

	-- Return the result of the function
	RETURN @return

END
