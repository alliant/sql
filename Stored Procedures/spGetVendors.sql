GO
/****** Object:  StoredProcedure [dbo].[spGetBatchCounts]    Script Date: 8/9/2016 2:38:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		  John Oliver
-- Create date: 5/16/2016
-- Description:	Gets the vendors from GP
-- =============================================
ALTER PROCEDURE [dbo].[spGetVendors]
	-- Add the parameters for the stored procedure here
	@vendorNbr VARCHAR(30) = 'ALL',
  @module VARCHAR(30) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  CREATE TABLE #vendorTable
  (
    [vendorID] VARCHAR(30),
    [name] VARCHAR(200),
    [active] VARCHAR(30),
    [address] VARCHAR(200),
    [city] VARCHAR(50),
    [zipcode] VARCHAR(50),
    [acct] VARCHAR(50),
    [acctname] VARCHAR(100),
    [dept] VARCHAR(50),
    [deptname] VARCHAR(50)
  )

  -- Build the report
  SELECT  RTRIM(vm.[VENDORID]) AS [vendorID],
          RTRIM(vm.[VENDNAME]) AS [name],
          CASE WHEN vm.[VENDSTTS] = 1 THEN 'yes' ELSE 'no' END AS [active],
          RTRIM(vm.[ADDRESS1]) + RTRIM(vm.[ADDRESS2]) AS [address],
          RTRIM(vm.[CITY]) AS [city],
          RTRIM(vm.[STATE]) AS [state],
          RTRIM(vm.[ZIPCODE]) AS [zipcode],
          RTRIM(a.[ACTNUMST]) AS [acct],
          RTRIM(am.[ACTDESCR]) AS [acctname],
          RTRIM(d.[SGMNTID]) AS [dept],
          RTRIM(d.[DSCRIPTN]) AS [deptname]
  FROM    [ANTIC].[dbo].[PM00200] vm INNER JOIN
          [ANTIC].[dbo].[GL00100] am INNER JOIN
          [ANTIC].[dbo].[GL00105] a INNER JOIN
          [ANTIC].[dbo].[GL40200] d
  ON      d.[SGMNTID] = a.[ACTNUMBR_3]
  ON      a.[ACTINDX] = am.[ACTINDX]
  ON      am.ACTINDX = vm.[PMPRCHIX]
  WHERE   vm.[VENDORID] = CASE WHEN @vendorNbr = 'ALL' THEN vm.[VENDORID] ELSE @vendorNbr END
    
  /* John Oliver 05.10.2021 - Commenting out vendor filtering per Bryan Johnson's request. 
                              Refer to Claim Module Requests - 4.9.2021 Row 13 */
  --IF @module <> ''
  --BEGIN
  --  DELETE FROM #vendorTable
  --  WHERE   [vendorID] NOT IN
  --          (
  --          SELECT  [objValue]
  --          FROM    [dbo].[sysprop]
  --          WHERE   [appCode] = @module
  --          AND     [objAction] = 'Vendor'
  --          )
  --END

  SELECT  *
  FROM    #vendorTable
END