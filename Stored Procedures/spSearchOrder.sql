USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spSearchOrder]    Script Date: 8/21/2019 10:41:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spSearchOrder]
	-- Add the parameters for the stored procedure here
	@search VARCHAR(500) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  EXEC [dbo].[spGetOrder]
END
