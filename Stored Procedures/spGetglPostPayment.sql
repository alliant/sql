USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetglPostPayment]    Script Date: 1/31/2020 1:37:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetglPostPayment] 
	-- Add the parameters for the stored procedure here
	@arTranIDList     VARCHAR(MAX) = 'ALL'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    CREATE TABLE #arTranIDTable ([artranID] integer)
    INSERT INTO #arTranIDTable ([artranID])
    SELECT [field] FROM [dbo].[GetEntityFilter] ('Artran', @arTranIDList)

    -- Insert statements for procedure here
    CREATE TABLE #GLPostPmt (agentID          VARCHAR(80),
                             chknum           VARCHAR(50),
                             pmtID            VARCHAR(50),
                             postingDate      DATETIME,
				             arglref          VARCHAR(500),
		                     debit            [decimal](18, 2) NULL,
     				         credit           [decimal](18, 2) NULL,
							 depositRef       VARCHAR(50))
							 
    /*-------credit-----------*/			
    INSERT INTO #GLPostPmt ( agentid, chknum, pmtid, postingdate, arglref, credit)    
   
    select ar.entityid,ar.reference, ar.tranid,ar.trandate, a.arglref , ar.tranamt
	    from artran ar inner join agent a on ar.entityid = a.agentid 
	    where  ar.artranid in (Select * from #arTranIDTable)
		 and   ar.type = 'P'
	  
    /*--------Debit-------------*/				
    INSERT INTO #GLPostPmt ( agentid, chknum, pmtid, postingdate, arglref, debit)    
   
    select ar.entityid,ar.reference, ar.tranid,ar.trandate, a.arcashglref , ar.tranamt
	    from artran ar inner join agent a on ar.entityid = a.agentid 
	    where ar.artranid in (Select * from #arTranIDTable)
		  and ar.type = 'P'

    update #GLPostPmt
	  set depositRef  = ad.depositRef
	  from arpmt ap
	  inner join
	  ardeposit ad
	  on ad.depositID = ap.depositID
	  where pmtid = ap.arPmtID

		
    update #GLPostPmt set credit      = ISNULL(credit,0),
		     		      debit       = ISNULL(debit,0),		
						  depositRef  = ISNULL(depositRef,''),
						  chknum      = ISNULL(chknum,'')

    select * from #GLPostPmt
	

END
