-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
USE [COMPASS]
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportClaimReserveWorksheet]
	-- Add the parameters for the stored procedure here
	@agentID VARCHAR(MAX) = '',
  @stateID VARCHAR(10) = '',
  @status VARCHAR(5) = '',
  @assignedTo VARCHAR(50) = ''
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  
  CREATE TABLE #agentTable ([agentID] VARCHAR(30))
  INSERT INTO #agentTable ([agentID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)

  --Insert all claims into the temp table
  CREATE TABLE #claimTable ([claimID] INTEGER)
  INSERT INTO #claimTable
  SELECT [claimID] FROM [dbo].[claim]

  DECLARE @startQuarterDate DATETIME,
          @endQuarterDate DATETIME
  SELECT @startQuarterDate = CONVERT(DATETIME, DATEADD(q, DATEDIFF(q,0,GETDATE()) -1 ,0)),
         @endQuarterDate = CONVERT(DATETIME, DATEADD(s,-1,DATEADD(q, DATEDIFF(q,0,GETDATE()),0)))


  CREATE TABLE #balanceTable
  (
  claimID INTEGER,
  refCategory VARCHAR(10),
  asOfDate DATETIME,
  pendingInvoiceAmount DECIMAL(18,2),
  approvedInvoiceAmount DECIMAL(18,2),
  completedInvoiceAmount DECIMAL(18,2),
  pendingReserveAmount DECIMAL(18,2),
  approvedReserveAmount DECIMAL(18,2),
  reserveBalance DECIMAL(18,2)
  )

  SELECT * INTO #quarterTable FROM #balanceTable WHERE 1 = 0

  IF (@agentID <> '')
  BEGIN
    DELETE FROM #claimTable
    WHERE [claimID] IN (SELECT [claimID] FROM [dbo].[claim] WHERE [agentID] NOT IN (SELECT [agentID] FROM #agentTable))
  END

  IF (@stateID <> '')
  BEGIN
    DELETE FROM #claimTable
    WHERE [claimID] IN (SELECT [claimID] FROM [dbo].[claim] WHERE [stateID] <> @stateID)
  END

  IF (@status <> '')
  BEGIN
    DELETE FROM #claimTable
    WHERE [claimID] IN (SELECT [claimID] FROM [dbo].[claim] WHERE [stat] <> @status)
  END

  IF (@assignedTo <> '')
  BEGIN
    DELETE FROM #claimTable
    WHERE [claimID] IN (SELECT [claimID] FROM [dbo].[claim] WHERE [assignedTo] <> @assignedTo)
  END

  -- Get the claim balances
  INSERT INTO #balanceTable EXEC [dbo].[spReportClaimBalances]
  INSERT INTO #quarterTable EXEC [dbo].[spReportClaimBalances] @asOfDate = @startQuarterDate
  INSERT INTO #quarterTable EXEC [dbo].[spReportClaimBalances] @asOfDate = @endQuarterDate
  
  -- Insert statements for procedure here
	SELECT  claim.[claimID],
          claim.[stat],
          (SELECT [objValue] FROM [sysprop] WHERE [appCode] = 'CLM' AND [objAction] = 'ClaimDescription' AND [objProperty] = 'Status' AND [objID] = claim.[stat]) AS 'statDesc',
          claim.[description],
          claim.[stage],
          (SELECT [objValue] FROM [sysprop] WHERE [appCode] = 'CLM' AND [objAction] = 'ClaimDescription' AND [objProperty] = 'Stage' AND [objID] = claim.[stage]) AS 'stageDesc',
          claim.[type],
          (SELECT [objValue] FROM [sysprop] WHERE [appCode] = 'CLM' AND [objAction] = 'ClaimDescription' AND [objProperty] = 'Type' AND [objID] = claim.[type]) AS 'typeDesc',
          claim.[action],
          (SELECT [objValue] FROM [sysprop] WHERE [appCode] = 'CLM' AND [objAction] = 'ClaimDescription' AND [objProperty] = 'Action' AND [objID] = claim.[action]) AS 'actionDesc',
          claim.[difficulty],
          claim.[urgency],
          claim.[stateID],
          agent.[agentID],
          agent.[name] AS 'agentName',
          claim.[dateCreated] AS 'dateOpened',
          CONVERT(BIT,CASE WHEN claim.[agentError] = 'Y' THEN 1 ELSE 0 END) AS 'agentError',
          (SELECT [objValue] FROM [sysprop] WHERE [appCode] = 'CLM' AND [objAction] = 'ClaimDescription' AND [objProperty] = 'AgentError' AND [objID] = claim.[agentError]) AS 'agentErrorDesc',
          claim.[fileNumber],
          claim.[assignedTo],
          note.[lastNote],
          note.[noteCnt],
          CONVERT(VARCHAR(MAX),ISNULL(claimcode.[descriptionCode],'')) AS 'descriptionCode',
          CONVERT(VARCHAR(MAX),ISNULL(claimcode.[descriptionDesc],'')) AS 'descriptionDesc',
          CONVERT(VARCHAR(MAX),ISNULL(claimcode.[causeCode],'')) AS 'causeCode',
          CONVERT(VARCHAR(MAX),ISNULL(claimcode.[causeDesc],'')) AS 'causeDesc',
          claim.[altaResponsibility] AS 'altaResponsibilityCode',
          CONVERT(VARCHAR(MAX),ISNULL((SELECT [description] FROM [syscode] WHERE [code] = claim.[altaResponsibility] and [codeType] = 'ClaimAltaResp'),'')) AS 'altaResponsibilityDesc',
          claim.[altaRisk] AS 'altaRiskCode',
          CONVERT(VARCHAR(MAX),ISNULL((SELECT [description] FROM [syscode] WHERE [code] = claim.[altaRisk] and [codeType] = 'ClaimAltaRisk'),'')) AS 'altaRiskDesc',
          balance.[laeOpen],
          balance.[laeApproved],
          balance.[laeLTD],
          balance.[laeAdjOpen],
          balance.[laeAdjLTD],
          balance.[laeReserve],
          balance.[lossOpen],
          balance.[lossApproved],
          balance.[lossLTD],
          balance.[lossAdjOpen],
          balance.[lossAdjLTD],
          balance.[lossReserve],
          ISNULL(recoveries.[recoveries],0) AS 'recoveries',
          ISNULL(recoveries.[pendingRecoveries],0) AS 'pendingRecoveries',
          CONVERT(VARCHAR(MAX),ISNULL(claimcoverage.[policyID],'')) AS 'policyID',
          claimcoverage.[policyDate],
          CONVERT(VARCHAR(MAX),ISNULL(claimcoverage.[policyType],'')) AS 'policyType',
          CONVERT(VARCHAR(MAX),ISNULL(claimcoverage.[insuredName],'')) AS 'insuredName',
          CONVERT(VARCHAR(MAX),ISNULL(claimcoverage.[insuredAddr],'')) AS 'insuredAddr',
          @startQuarterDate AS 'beginQuarter',
          @endQuarterDate AS 'endQuarter',
          quarterDelta.[laeQuarterReserveDelta],
          quarterDelta.[lossQuarterReserveDelta]
  FROM    [dbo].[claim] INNER JOIN
          #claimTable claimTable
  ON      claimTable.[claimID] = claim.[claimID] INNER JOIN
          [dbo].[agent]
  ON      claim.[agentID] = agent.[agentID] LEFT OUTER JOIN
          (
          SELECT  [claimID],
                  MAX([noteDate]) AS 'lastNote',
                  COUNT([seq]) AS 'noteCnt'
          FROM    [dbo].[claimnote]
          GROUP BY [claimID]
          ) note
  ON      claim.[claimID] = note.[claimID] LEFT OUTER JOIN
          (
          SELECT  cc2.[claimID],
                  SUBSTRING(
                  (
                    SELECT  ', ' + cc.[code] AS [text()]
                    FROM    [claimcode] cc
                    WHERE   cc.[claimID] = cc2.[claimID]
                    AND     cc.[codeType] = 'ClaimDescription'
                    ORDER BY cc.[claimID]
                    FOR XML PATH('')
                  ), 3, 1000) AS 'descriptionCode',
                  SUBSTRING(
                  (
                    SELECT  ', ' + sc.[description] AS [text()]
                    FROM    [syscode] sc INNER JOIN
                            [claimcode] cc
                    ON      cc.[code] = sc.[code]
                    AND     cc.[codeType] = sc.[codeType]
                    WHERE   cc.[claimID] = cc2.[claimID]
                    AND     sc.[codeType] = 'ClaimDescription'
                    ORDER BY cc.[claimID]
                    FOR XML PATH('')
                  ), 3, 1000) AS 'descriptionDesc',
                  SUBSTRING(
                  (
                    SELECT  ', ' + cc.[code] AS [text()]
                    FROM    [claimcode] cc
                    WHERE   cc.[claimID] = cc2.[claimID]
                    AND     cc.[codeType] = 'ClaimCause'
                    ORDER BY cc.[claimID]
                    FOR XML PATH('')
                  ), 3, 1000) AS 'causeCode',
                  SUBSTRING(
                  (
                    SELECT  ', ' + sc.[description] AS [text()]
                    FROM    [syscode] sc INNER JOIN
                            [claimcode] cc
                    ON      cc.[code] = sc.[code]
                    AND     cc.[codeType] = sc.[codeType]
                    WHERE   cc.[claimID] = cc2.[claimID]
                    AND     sc.[codeType] = 'ClaimCause'
                    ORDER BY cc.[claimID]
                    FOR XML PATH('')
                  ), 3, 1000) AS 'causeDesc'
          FROM    [claimcode] cc2
          GROUP BY cc2.[claimID]
          ) claimcode
  ON      claim.[claimID] = claimcode.[claimID] LEFT OUTER JOIN
          (
          SELECT  cc.[claimID],
                  cc.[coverageID] AS 'policyID',
                  COALESCE(cc.[effDate],p.[effDate]) AS 'policyDate',
                  sf.[description] AS 'policyType',
                  cc.[insuredName],
                  '' AS 'insuredAddr'
          FROM    [dbo].[claimcoverage] cc INNER JOIN
                  [dbo].[policy] p
          ON      cc.[coverageID] = p.[policyID] INNER JOIN
                  [dbo].[stateform] sf
          ON      p.[formID] = sf.[formID]
          AND     p.[stateID] = sf.[stateID]
          WHERE   ISNUMERIC(cc.[coverageID]) = 1
          ) claimcoverage
  ON      claim.[claimID] = claimcoverage.[claimID] LEFT OUTER JOIN
          (
          SELECT  [claimID],
                  SUM([EPP]) AS 'laeOpen',
                  SUM([EAP]) AS 'laeApproved',
                  SUM([ECP]) AS 'laeLTD',
                  SUM([EPA]) AS 'laeAdjOpen',
                  SUM([EAA]) AS 'laeAdjLTD',
                  SUM([ER]) AS 'laeReserve',
                  SUM([LPP]) AS 'lossOpen',
                  SUM([LAP]) AS 'lossApproved',
                  SUM([LCP]) AS 'lossLTD',
                  SUM([LPA]) AS 'lossAdjOpen',
                  SUM([LAA]) AS 'lossAdjLTD',
                  SUM([LR]) AS 'lossReserve'
          FROM    (                  
                  SELECT  [claimID],
                          [refCategory] + 'PP' AS 'pendingPayment',
                          [refCategory] + 'AP' AS 'approvedPayment',
                          [refCategory] + 'CP' AS 'completePayment',
                          [refCategory] + 'PA' AS 'pendingAdjustments',
                          [refCategory] + 'AA' AS 'approvedAdjustments',
                          [refCategory] + 'R' AS 'reserve',
                          [pendingInvoiceAmount],
                          [approvedInvoiceAmount],
                          [completedInvoiceAmount],
                          [pendingReserveAmount],
                          [approvedReserveAmount],
                          [reserveBalance]
                  FROM    #balanceTable
                  ) AS src
                  PIVOT
                  (
                  SUM([pendingInvoiceAmount])
                  FOR [pendingPayment] IN (EPP,LPP)
                  ) AS p1
                  PIVOT
                  (
                  SUM([approvedInvoiceAmount])
                  FOR [approvedPayment] IN (EAP,LAP)
                  ) AS p2
                  PIVOT
                  (
                  SUM([completedInvoiceAmount])
                  FOR [completePayment] IN (ECP,LCP)
                  ) AS p3
                  PIVOT
                  (
                  SUM([pendingReserveAmount])
                  FOR [pendingAdjustments] IN (EPA,LPA)
                  ) AS p4
                  PIVOT
                  (
                  SUM([approvedReserveAmount])
                  FOR [approvedAdjustments] IN (EAA,LAA)
                  ) AS p5
                  PIVOT
                  (
                  SUM([reserveBalance])
                  FOR [reserve] IN (ER,LR)
                  ) AS p6
          GROUP BY [claimID]
          ) balance
  ON      claim.[claimID] = balance.[claimID] LEFT OUTER JOIN
          (
          SELECT  [claimID],
                  SUM([E]) AS 'laeQuarterReserveDelta',
                  SUM([L]) AS 'lossQuarterReserveDelta'
          FROM    (                  
                  SELECT  qEnd.[claimID],
                          qEnd.[refCategory],
                          qEnd.[approvedReserveAmount] - qBegin.[approvedReserveAmount] AS 'approvedReserveAmount'
                  FROM    #quarterTable qEnd INNER JOIN
                          #quarterTable qBegin
                  ON      qBegin.[refCategory] = qEnd.[refCategory]
                  AND     qBegin.[claimID] = qEnd.[claimID]
                  WHERE   qBegin.[asOfDate] = @startQuarterDate
                  AND     qEnd.[asOfDate] = @endQuarterDate
                  ) AS src
                  PIVOT
                  (
                  SUM([approvedReserveAmount])
                  FOR [refCategory] IN (E,L)
                  ) AS p
          GROUP BY [claimID]
          ) quarterDelta
  ON      claim.[claimID] = quarterDelta.[claimID] LEFT OUTER JOIN
          (
          SELECT  CONVERT(INTEGER,inv.[refID]) AS 'claimID',
                  SUM(CASE WHEN inv.[stat] = 'O' OR inv.[stat] = 'A' THEN [requestedAmount] ELSE 0 END) - 
                  SUM(CASE WHEN inv.[stat] = 'O' OR inv.[stat] = 'A' THEN [transAmount] ELSE 0 END) AS 'pendingRecoveries',
                  SUM([transAmount]) AS 'recoveries'
          FROM    [arinv] inv LEFT OUTER JOIN
                  [artrx] trx
          ON      inv.[arinvID] = trx.[arinvID]
          WHERE   inv.[dateRequested] BETWEEN @startQuarterDate AND @endQuarterDate
          GROUP BY inv.[refID]
          ) recoveries
  ON      claim.[claimID] = recoveries.[claimID]

  DROP TABLE #agentTable
  DROP TABLE #balanceTable
  DROP TABLE #claimTable
  DROP TABLE #quarterTable
END
GO
