-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	<Rahul Sharma>
-- Create date: <04-05-2019>
-- Description:	<Get syslog records by module>
-- =============================================
ALTER PROCEDURE [dbo].[spGetUsersByModule] 
    -- Add the parameters for the stored procedure here
    @pcModule    VARCHAR(50) = 'ALL',
    @pdStartDate DATE,
    @pdEndDate   DATE
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  -- Temporary table definition
  CREATE TABLE #templog (uid            VARCHAR(100),			            
                         applicationID  VARCHAR(50))

  CREATE TABLE #syslog (uid            VARCHAR(100),
                        totalcount     INTEGER,
                        applicationID  VARCHAR(50))
    
  -- Insert statements for procedure here
  IF @pcModule = 'ALL'
  BEGIN
    INSERT INTO #templog([uid],[applicationID]) 	     	
	  SELECT syslog.uid, syslog.applicationID
      FROM syslog
      WHERE CAST(syslog.createdate AS DATE) >= @pdStartDate
        AND CAST(syslog.createdate AS DATE) <= @pdEndDate
	
    UPDATE #templog 
      SET #templog.applicationID = (CASE WHEN #templog.applicationID NOT IN (SELECT syscode.code FROM syscode) 
	                                                                     OR (#templog.applicationID is NULL) THEN 'Unknown'
									ELSE #templog.applicationID END)	
    
	INSERT INTO #syslog([uid],[applicationID],[totalcount]) 	     	
      SELECT #templog.uid, #templog.applicationID, COUNT(#templog.uid)
      FROM #templog 
	  GROUP BY #templog.applicationID, #templog.uid
   
    SELECT #syslog.applicationID, #syslog.uid, sysuser.name, sysuser.role, #syslog.totalcount
      FROM sysuser 
	    INNER JOIN #syslog ON sysuser.uid = #syslog.uid
  END
  ELSE IF @pcModule = 'Unknown'
  BEGIN
    -- @pcModule = 'Unknown' means all those syslog records whose applicationID is other than syscode
    INSERT INTO #syslog([uid],[totalcount])       
      SELECT syslog.uid, COUNT(syslog.uid)
	  FROM syslog
      WHERE CAST(syslog.createdate AS DATE) >= @pdStartDate 
        AND CAST(syslog.createdate AS DATE) <= @pdEndDate
	    AND (syslog.applicationID NOT IN (SELECT syscode.code FROM syscode) 
	     OR (syslog.applicationID is NULL)) 
      GROUP BY syslog.uid

    UPDATE #syslog SET #syslog.applicationID='Unknown'

	SELECT #syslog.applicationID, #syslog.uid, sysuser.name, sysuser.role, #syslog.totalcount
      FROM sysuser 
	    INNER JOIN #syslog ON sysuser.uid = #syslog.uid    
  END
  ELSE 
  BEGIN
    INSERT INTO #syslog([uid],[applicationID],[totalcount])    
      SELECT syslog.uid, syslog.applicationID, COUNT(syslog.uid)
      FROM syslog
      WHERE CAST(syslog.createdate AS DATE) >= @pdStartDate
        AND CAST(syslog.createdate AS DATE) <= @pdEndDate 
        AND syslog.applicationID = @pcModule
      GROUP BY syslog.applicationID, syslog.uid
       
    SELECT #syslog.applicationID, #syslog.uid, sysuser.name, sysuser.role, #syslog.totalcount
      FROM sysuser 
	    INNER JOIN #syslog ON sysuser.uid = #syslog.uid
  END 
END
GO
