USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spAlertReceivables]    Script Date: 6/1/2017 6:40:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetAlertReceivables]
  @agentID VARCHAR(MAX) = 'ALL',
  @stateID VARCHAR(3) = 'ALL',
  @UID varchar(100)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SET @agentID = [dbo].[StandardizeAgentID](@agentID)

  -- Get the state
  CREATE TABLE #stateTable ([stateID] VARCHAR(2))
  INSERT INTO #stateTable ([stateID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('State', @stateID)

  DECLARE @NextString NVARCHAR(100),
          @Pos INTEGER = 0,
          @Str NVARCHAR(MAX),
          @Delimiter NCHAR(1) = ','

  SELECT  A.[agentID],
          A.[name] AS [agentName],
          A.[stateID],
          A.[stat],
          CONVERT(VARCHAR(50),ISNULL((SELECT [uid] FROM [dbo].[agentmanager] WHERE [agentID] = a.[agentID] AND [isPrimary] = 1 AND [stat] = 'A'),'')) AS [manager],
          SUM(B.[daysCurrent]) AS [daysCurrent],
          SUM(B.[days31to60]) AS [days31to60],
          SUM(B.[days61to90]) AS [days61to90],
          SUM(B.[daysOver90]) AS [daysOver90]
  FROM    [dbo].[agent] A INNER JOIN
          (
          SELECT  CM.[CUSTNMBR] AS [custID],
                  CASE
                    WHEN DATEDIFF(D, RM.[DOCDATE], GETDATE()) <= 30 AND RM.[RMDTYPAL] < 7 THEN RM.[CURTRXAM]
                    WHEN DATEDIFF(D, RM.[DOCDATE], GETDATE()) <= 30 AND RM.[RMDTYPAL] > 6 THEN RM.[CURTRXAM] * -1
                    ELSE 0
                  END AS [daysCurrent],
                  CASE
                    WHEN DATEDIFF(D, RM.[DOCDATE], GETDATE()) BETWEEN 31 AND 60 AND RM.[RMDTYPAL] < 7 THEN RM.[CURTRXAM]
                    WHEN DATEDIFF(D, RM.[DOCDATE], GETDATE()) BETWEEN 31 AND 60 AND RM.[RMDTYPAL] > 6 THEN RM.[CURTRXAM] * -1
                    ELSE 0
                  END AS [days31to60],
                  CASE
                    WHEN DATEDIFF(D, RM.[DOCDATE], GETDATE()) BETWEEN 61 AND 90 AND RM.[RMDTYPAL] < 7 THEN RM.[CURTRXAM]
                    WHEN DATEDIFF(D, RM.[DOCDATE], GETDATE()) BETWEEN 61 AND 90 AND RM.[RMDTYPAL] > 6 THEN RM.[CURTRXAM] * -1
                    ELSE 0
                  END AS [days61to90],
                  CASE
                    WHEN DATEDIFF(D, RM.[DOCDATE], GETDATE()) > 90 AND RM.[RMDTYPAL] < 7 THEN RM.[CURTRXAM]
                    WHEN DATEDIFF(D, RM.[DOCDATE], GETDATE()) > 90 AND RM.[RMDTYPAL] > 6 THEN RM.[CURTRXAM] * -1
                    ELSE 0
                  END AS [daysOver90]
          FROM    [ANTIC].[dbo].[RM20101] RM INNER JOIN 
                  [ANTIC].[dbo].[RM00101] CM
          ON      RM.[CUSTNMBR] = CM.[CUSTNMBR] LEFT OUTER JOIN
                  [ANTIC].[dbo].[RM00103] S
          ON      RM.[CUSTNMBR]= S.[CUSTNMBR]
          WHERE   RM.[VOIDSTTS] = 0
          AND     RM.[CURTRXAM] <> 0
          ) B
  ON      A.[agentID] = CONVERT(VARCHAR(6),B.[custID]) INNER JOIN
          #stateTable ST
  ON      A.[stateID] = ST.[stateID]
  WHERE (A.[agentID] = (CASE  WHEN @agentID != 'ALL' THEN @agentID ELSE A.[agentID] END)) 
  AND   (dbo.CanAccessAgent(@UID ,A.[agentID]) = 1) 
   
  GROUP BY a.[agentID],a.[name],a.[stateID],a.[stat]

  DROP TABLE #stateTable
END
