USE [COMPASS_DVLP]
GO
/****** Object:  StoredProcedure [dbo].[spCreateFileIDPolicy]    Script Date: 3/20/2020 8:16:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spCreateFileIDPolicy]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  DECLARE @agentID VARCHAR(20),
          @fileNumber VARCHAR(1000),
          @addr1 VARCHAR(100) = '',
          @addr2 VARCHAR(100) = '',
          @addr3 VARCHAR(100) = '',
          @addr4 VARCHAR(100) = '',
          @city VARCHAR(100) = '',
          @countyID VARCHAR(50) = '',
          @state VARCHAR(2) = '',
          @zip VARCHAR(20) = '',
          @stage VARCHAR(20),
          @transactionType VARCHAR(20) = '',
          @insuredType VARCHAR(20) = '',
          @reportingDate DATETIME = NULL,
          @liability DECIMAL(18,2) = 0,
          @notes VARCHAR(MAX),
          @agentFileID INTEGER

  -- Insert statements for procedure here
	CREATE TABLE #fileID
  (
    [agentID] VARCHAR(20),
    [policyID] INTEGER,
    [liability] DECIMAL(18,2),
    [fileNumber] VARCHAR(1000),
    [fileID] VARCHAR(1000)
  )

  INSERT INTO #fileID ([agentID],[policyID],[liability],[fileNumber],[fileID])
  SELECT  [agentID],
          [policyID],
          [liabilityAmount],
          [fileNumber],
          [dbo].[NormalizeFileID] ([fileNumber])
  FROM    [dbo].[policy]
  WHERE policy.fileid NOT IN(SELECT fileid FROM agentfile af WHERE af.agentid = policy.agentid)
	AND policy.fileid IS NOT NULL
	AND policy.fileid <> '' 
	AND policy.agentid IS NOT NULL
	AND policy.agentid <> ''

  --Insert into agentfile
  DECLARE cur CURSOR LOCAL FOR
  SELECT  [agentID],
          [fileNumber],
          [liability],
          'PolicyIssued' AS [stage],
          'DataFix:PolicyIssued,ARC,Processed Agent File when Policy ' + CONVERT(VARCHAR,[policyID]) + ' is issued' AS [notes]
  FROM    #fileID f

  OPEN cur
  FETCH NEXT FROM cur INTO @agentID, @fileNumber, @liability, @stage, @notes

  WHILE @@FETCH_STATUS = 0 
  BEGIN
    SET @agentFileID = 0
    EXEC [dbo].[spInsertAgentFile] @agentID, @fileNumber, @addr1, @addr2, @addr3, @addr4, @city, @countyID, @state, @zip, @stage, @transactionType, @insuredType, @reportingDate, @liability, @notes, @agentFileID OUTPUT 
	UPDATE agentfile 
	 SET agentfile.addr1 = c.addr1,
		 agentfile.addr2 = c.addr2,
		 agentfile.addr3 = c.addr3,
		 agentfile.addr4 = c.addr4,
         agentfile.city  = c.city,
		 agentfile.countyID = c.county,
		 agentfile.state = c.state,
		 agentfile.zip = c.zip 
		  FROM agentfile inner join cpl c ON 
           c.agentid = agentfile.agentid
       AND c.fileID = agentfile.fileID	
     WHERE agentfile.agentfileID = @agentFileID
           AND c.stat in ('I','R',NULL)	 

	UPDATE agentfile 
	 SET agentfile.transactionType = CASE WHEN bf.[residential] = 1 THEN 'Residential' ELSE 'Commercial' END,
		 agentfile.insuredType     = bf.[insuredType],
		 agentfile.reportingDate   = CASE WHEN (agentfile.reportingDate > b.[invoiceDate]) OR 
											   (agentfile.reportingDate IS NULL)
		                                   THEN b.[invoiceDate] 
										   ELSE agentfile.reportingDate END
		 FROM agentfile INNER JOIN
		  batchform bf
         ON bf.fileID = agentfile.fileID INNER JOIN 
		  batch b
		 ON  b.agentid = agentfile.agentid
		 AND b.batchid = bf.batchid 
		 where bf.formType = 'P'
	       AND agentfile.agentfileID = @agentFileID 		
		 
    FETCH NEXT FROM cur INTO @agentID, @fileNumber, @liability, @stage, @notes
  END
  CLOSE cur
  DEALLOCATE cur

  DROP TABLE #fileID 
END
