/****** Object:  StoredProcedure [dbo].[spGetSysQueue]    Script Date: 2/24/2020 2:09:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetSysQueue] 
	-- Add the parameters for the stored procedure here
	@queueID                  INT = 0,
	@actionID         VARCHAR(30) = 'ALL',
	@UID             varchar(100) = 'ALL',
	@appCode          VARCHAR(30) = 'ALL',
	@queuestatus      VARCHAR(30) = 'ALL',
	@queuestartDate  datetime     = NULL,
	@queueendDate    datetime     = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
  IF @actionID = ''
    SET @actionID = 'ALL'
  
  IF @UID = ''
    SET @UID = 'ALL'
	
  IF @appCode = ''
    SET @appCode = 'ALL'

  IF @queuestatus = ''
    SET @queuestatus = 'ALL'

  SELECT  q.[queueID],
          qi.[seq],
          q.[actionID],
          (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'SYS' AND [objAction] = 'Destination' AND [objProperty] = 'Action' AND [objID] = q.[actionID]) AS [actionDesc],
          q.[appCode],
          q.[queueDate],
	  q.[startDate],
	  q.[endDate],
          q.[notifyRequestor],
	  q.[UID],
          q.[stat] AS [queueStat],
          (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'SYS' AND [objAction] = 'Queue' AND [objProperty] = 'Status' AND [objID] = q.[stat]) AS [queueStatDesc],
          qi.[parameters],
          qi.[destinations],
          qi.[stat] AS [itemStat],
          (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'SYS' AND [objAction] = 'Queue' AND [objProperty] = 'Status' AND [objID] = qi.[stat]) AS [itemStatDesc],
          qi.[faultMsg] AS [itemFault]
  FROM    [dbo].[sysqueue] q INNER JOIN
          [dbo].[sysqueueitem] qi
  ON      q.[queueID] = qi.[queueID]
  WHERE   q.[queueID] = CASE WHEN @queueID = 0 THEN q.[queueID] ELSE @queueID END
  AND     q.[actionID] = CASE WHEN @actionID = 'ALL' THEN q.[actionID] ELSE @actionID END
  AND     q.[UID] = CASE WHEN   @UID = 'ALL'  THEN q.[UID] ELSE @UID END
  AND     q.[appCode] = CASE WHEN @appCode = 'ALL' THEN q.[appCode] ELSE @appCode END
  AND     q.[stat] = CASE WHEN @queuestatus = 'ALL' THEN q.[stat] ELSE @queuestatus END
  AND     q.[queueDate] = CASE  WHEN (@queuestartDate IS NULL OR @queueendDate IS NULL ) 
                                 THEN q.[queueDate] 
						         ELSE ( case when (q.[queueDate] >= @queuestartDate) AND (q.[queueDate] <= @queueendDate) 
								         then 
								          q.[queueDate]
										 else
										  '2001-01-01'
										end
								      ) 
						  END
  ORDER BY q.[startDate]
END

