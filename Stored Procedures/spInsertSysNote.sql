/****** Object:  StoredProcedure [dbo].[spInsertAgentFile]    Script Date: 3/20/2020 9:12:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spInsertSysNote]
	-- Add the parameters for the stored procedure here
	@entity VARCHAR(30),
	@entityID VARCHAR(30),
	@notes VARCHAR(MAX),
	@isError BIT = 0      OUTPUT,
	@errMsg  VARCHAR(500) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET ANSI_WARNINGS OFF;

	-- Insert statements for procedure here
	DECLARE @sysNoteID  INTEGER = 0,
			@seq        INTEGER = 1,
			@transCount BIT = 0

	SET @isError = 0
	SET @errMsg = ''


	BEGIN TRY
		IF(@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @transCount = 1
		END

		--Get the ID and seq
		EXEC [dbo].[spGetNextSequence] 'sysNote', @sysNoteID OUTPUT
		IF(@sysNoteID = -1)
			RAISERROR('Sequence ''sysNote'' is not defined. Please contact system administrator.', 16, 1)

		SELECT @seq = Max([seq]) + 1 FROM [dbo].[sysnote] WHERE [entity] = @entity AND [entityID] = @entityID

		IF @seq IS NULL 
		SET @seq = 1
  
		INSERT INTO [dbo].[sysnote] ([sysNoteID],[entity],[entityID],[seq],[uid],[notes],[dateCreated],[dateModified]) VALUES
		(
			@sysNoteID,
			@entity,
			@entityID,
			@seq,
			'compass@alliantnational.com',
			@notes,
			GETDATE(),
			GETDATE()
		)

		IF(@transCount = 1)
			COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		SET @isError = ERROR_STATE()
		SET @errMsg  = ERROR_MESSAGE()
		IF(@transCount = 1)
			ROLLBACK TRANSACTION
		RETURN
	END CATCH
END
