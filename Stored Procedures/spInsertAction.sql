-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		John Oliver (joliver)
-- Create date: 8.21.2015
-- Description:	Used to insert a new row into the sysaction table
-- =============================================
ALTER PROCEDURE dbo.spInsertAction 
	-- Add the parameters for the stored procedure here
  @action          varchar(32) = NULL, -- Action is the third parameter passed. EX: claimsGet. REQUIRED
  @description     varchar(80) = NULL, -- Description of the executed code. Not required but highly recommended
  @progExec        varchar(60) = NULL, -- The .p program to execute. EX: claim/getclaim.p. REQUIRED
  @isActive        bit = 1,            -- Always set to 1 (TRUE)
  @isAnonymous     bit = 0,            -- Always set to 0 (FALSE)
  @isSecure        bit = 1,            -- Always set to 1 (TRUE)
  @isLog           bit = 0,            -- Always set to 0 (FALSE)
  @isAudit         bit = 0,            -- Always set to 0 (FALSE)
  @roles           varchar(200) = '*', -- If isSecure, what roles have access to the program
  @userids         varchar(200) = '*', -- If isSecure, the users that have access to the program
  @addrs           varchar(200) = '*', -- Always set to *
  @emails          varchar(1) = 'N',   -- If the program emails out
  @emailSuccess    varchar(100) = '',  -- Success email message
  @emailFailure    varchar(100) = '',  -- Failure email message
  @isEventsEnabled bit = 0,            -- Sends an event to MQServer
  @comments        varchar(max) = ''   -- Additional notes
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  -- Insert statements for procedure here
  DECLARE @isError bit = 0
         ,@tempAction varchar(32) = ''

  IF (@action IS NULL)
  BEGIN
    RAISERROR(N'The action is missing',10,1)
    SET @isError = 1
  END
  IF (@description IS NULL)
  BEGIN
    RAISERROR(N'The description is missing. It is set to NULL',10,1)
  END
  IF (@progExec IS NULL)
  BEGIN
    RAISERROR(N'The program to execute is missing',10,1)
    SET @isError = 1
  END
  IF @isError = 1
  BEGIN
    RETURN(1)
  END

  SELECT @tempAction=[action] FROM [sysaction] WHERE [action]=@action

  IF (@tempAction = '')
  BEGIN
    INSERT INTO [sysaction]
    (
           [action]
          ,[description]
          ,[progExec]
          ,[isActive]
          ,[isAnonymous]
          ,[isSecure]
          ,[isLog]
          ,[isAudit]
          ,[roles]
          ,[userids]
          ,[addrs]
          ,[createDate]
          ,[emails]
          ,[emailSuccess]
          ,[emailFailure]
          ,[isEventsEnabled]
          ,[comments]
    )
    VALUES
    (
           @action
          ,@description
          ,@progExec
          ,@isActive
          ,@isAnonymous
          ,@isSecure
          ,@isLog
          ,@isAudit
          ,@roles
          ,@userids
          ,@addrs
          ,GETDATE()
          ,@emails
          ,@emailSuccess
          ,@emailFailure
          ,@isEventsEnabled
          ,@comments
    )
  END
  ELSE
  BEGIN
    UPDATE [sysaction]
    SET [description] = @description
       ,[progExec] = @progExec
       ,[isActive] = @isActive
       ,[isAnonymous] = @isAnonymous
       ,[isSecure] = @isSecure
       ,[isLog] = @isLog
       ,[isAudit] = @isAudit
       ,[roles] = @roles
       ,[userids] = @userids
       ,[addrs] = @addrs
       ,[createDate] = getdate()
       ,[emails] = @emails
       ,[emailSuccess] = @emailSuccess
       ,[emailFailure] = @emailFailure
       ,[isEventsEnabled] = @isEventsEnabled
       ,[comments] = @comments
    WHERE [action]=@action
  END
END
GO
