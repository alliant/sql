
/****** Object:  StoredProcedure [dbo].[spGetAgentApplication]    Script Date: 7/21/2022 11:57:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetAgentApplication]
	-- Add the parameters for the stored procedure here
	@agentID VARCHAR(MAX) = 'ALL',
  @stateID VARCHAR(100) = 'ALL',
  @UID varchar(100)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;
  SET @agentID = [dbo].[StandardizeAgentID](@agentID)

  CREATE TABLE #stateTable ([stateID] VARCHAR(2))
  INSERT INTO #stateTable ([stateID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('State', @stateID)

  SELECT  aa.[applicationID],
          aa.[agentID],
          aa.[agentName],
          aa.[manager],
          aa.[stateID],
          [dbo].[GetRegion] (aa.[stateID]) AS [regionID],
          aa.[dateCreated],
          aa.[dateStarted],
          aa.[dateSubmitted],
          aa.[dateUnderReview],
          aa.[dateSentReview],
          q.[schedStartDate] AS [dateOrderedERR],
          q.[auditStartDate] AS [dateStartedERR],
          q.[auditFinishDate] AS [dateReceviedERR],
          aa.[qarID],
          aa.[dateApproved],
          aa.[dateStopped],
          aa.[dateSentAgency],
          aa.[dateSigned],
          aa.[username],
          aa.[password],
          ISNULL(aa.[reasoncode],'') AS [reasoncode],
          aa.[type],
          (SELECT [objValue] FROM [sysprop] WHERE [appCode] = 'AMD' AND [objAction] = 'Application' AND [objProperty] = 'Type' AND [objID] = aa.[type]) AS [typeDesc],
          aa.[subtype],
          aa.[stat],
          '',
		      ISNULL(aa.[yearsActive],'') AS [yearsActive],
		      ISNULL(aa.[phone],'') AS [phone],
		      ISNULL(aa.[email],'') AS [email],
		      ISNULL(aa.[name],'') AS [name],
		      ISNULL(aa.[addr1],'') AS [addr1],
		      ISNULL(aa.[addr2],'') AS [addr2],
		      ISNULL(aa.[city],'') AS [city],
		      ISNULL(aa.[state],'') AS [state],
		      ISNULL(aa.[zip],'') AS [zip]
  FROM    (
          SELECT  a.[name] AS [agentName],
                  am.[uid] AS [manager],
				  aa.[applicationID]
           ,aa.[agentID]
           ,aa.[dateCreated]
           ,aa.[dateStarted]
           ,aa.[dateSubmitted]
           ,aa.[dateUnderReview]
           ,aa.[dateSentReview]
           ,aa.[dateApproved]
           ,aa.[dateStopped]
           ,aa.[dateSentAgency]
           ,aa.[dateSigned]
           ,aa.[username]
           ,aa.[password]
           ,aa.[reasoncode]
           ,aa.[stateID]
           ,aa.[stat]
           ,aa.[type]
           ,aa.[subtype]
           ,aa.[qarID]
           ,aa.[yearsActive]
           ,aa.[phone]
           ,aa.[email]
           ,aa.[name]
           ,aa.[addr1]
           ,aa.[addr2]
           ,aa.[city]
           ,aa.[state]
           ,aa.[zip]
          FROM    [dbo].[agent] a LEFT OUTER JOIN
                  [dbo].[agentmanager] am
          ON      a.[agentID] = am.[agentID]
          AND     am.[isPrimary] = 1
          AND     am.[stat] = 'A'  INNER JOIN
                  [dbo].[agentapplication] aa
          ON      a.[agentID] = aa.[agentID]
          AND     aa.[type] = 'G'
          UNION ALL
          SELECT  a.[name1] AS [agentName],
                  NULL,
				  aa.[applicationID]
           ,aa.[agentID]
           ,aa.[dateCreated]
           ,aa.[dateStarted]
           ,aa.[dateSubmitted]
           ,aa.[dateUnderReview]
           ,aa.[dateSentReview]
           ,aa.[dateApproved]
           ,aa.[dateStopped]
           ,aa.[dateSentAgency]
           ,aa.[dateSigned]
           ,aa.[username]
           ,aa.[password]
           ,aa.[reasoncode]
           ,aa.[stateID]
           ,aa.[stat]
           ,aa.[type]
           ,aa.[subtype]
           ,aa.[qarID]
           ,aa.[yearsActive]
           ,aa.[phone]
           ,aa.[email]
           ,aa.[name]
           ,aa.[addr1]
           ,aa.[addr2]
           ,aa.[city]
           ,aa.[state]
           ,aa.[zip]
          FROM    [dbo].[attorney] a INNER JOIN
                  [dbo].[agentapplication] aa
          ON      a.[attorneyID] = aa.[agentID]
          AND     aa.[type] = 'A'
          ) aa LEFT OUTER JOIN
          [dbo].[qar] q
  ON      aa.[qarID] = q.[qarID]
  WHERE   aa.[agentID] IN (SELECT [agentID] FROM [dbo].[agent] WHERE ([agentID] = (CASE  WHEN @agentID != 'ALL' THEN @agentID ELSE [agentID] END)) AND ([dbo].[CanAccessAgent](@UID , [agentID]) = 1))
  AND     aa.[stateID] IN (SELECT [stateID] FROM #stateTable)
  ORDER BY aa.[agentID]

  DROP TABLE #stateTable
END
