GO
/****** Object:  StoredProcedure [dbo].[spGetactiverequirements]    Script Date: 2/18/2025 12:07:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetactiverequirements]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
  SELECT  
        s.stateID,
		(select description from state  where state.stateID = s.stateID) AS [stateDesc],
		s.requirementID,
		s.description
  FROM    [dbo].[StateRequirement] s
  WHERE s.reqFor    = 'O' AND
        s.appliesTo = 'Agent' AND
  EXISTS(SELECT requirementid FROM statereqqual WHERE statereqqual.requirementid =  s.requirementid AND statereqqual.active = '1') 
END