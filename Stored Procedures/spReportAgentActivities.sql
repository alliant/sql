GO
/****** Object:  StoredProcedure [dbo].[spGetAgentActiviies]    Script Date: 2/14/2018 4:16:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportAgentActivities]
	-- Add the parameters for the stored procedure here
  @agentID VARCHAR(MAX) = 'ALL',
  @stateID VARCHAR(50) = 'ALL',
  @category VARCHAR(50) = 'ALL',
  @year INTEGER = 0,
  @UID varchar(100)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;
  SET @agentID = [dbo].[StandardizeAgentID](@agentID);

  -- Get the state
  CREATE TABLE #stateTable ([stateID] VARCHAR(2))
  INSERT INTO #stateTable ([stateID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('State', @stateID)

  SELECT  [activityID],
          aa.[agentID],
          a.[name],
          a.[stateID],
          aa.[year],
          aa.[type],
          aa.[stat],
          aa.[category],
          CASE WHEN a.[corporationID] = '' THEN a.[agentID] ELSE a.[corporationID] END AS [corporationID],
          aa.[month1],
          aa.[month2],
          aa.[month3],
          aa.[month4],
          aa.[month5],
          aa.[month6],
          aa.[month7],
          aa.[month8],
          aa.[month9],
          aa.[month10],
          aa.[month11],
          aa.[month12]
  FROM    [dbo].[GetActivityTable] (@category,@year) aa LEFT OUTER JOIN
          [dbo].[agent] a
  ON      aa.[agentID] = a.[agentID]
  WHERE   aa.[agentID] != ''
  AND     a.[stateID] IN (SELECT [stateID] FROM #stateTable)
  AND     (aa.[agentID] = (CASE  WHEN @agentID != 'ALL' THEN @agentID ELSE aa.[agentID] END)) 
  AND     (dbo.CanAccessAgent(@UID ,aa.[agentID]) = 1)   
  AND     aa.[category] = CASE WHEN @category = 'ALL' THEN aa.[category] ELSE @category END
  AND     aa.[year] = CASE WHEN @year = 0 THEN aa.[year] ELSE @year END
  ORDER BY aa.[agentID],aa.[year],aa.[sortOrder],aa.[type]

  DROP TABLE #stateTable
END
