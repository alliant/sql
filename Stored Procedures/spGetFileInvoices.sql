/****** Object:  StoredProcedure [dbo].[spGetfileInvoices]    Script Date: 2/7/2023 12:00:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetfileInvoices]
	-- Add the parameters for the stored procedure here
	@agentFileID            INTEGER           =  0,
	@posted                 VARCHAR(1)        = 'B',
	@pcAgentID              VARCHAR(50)       = 'ALL',
	@pInvoiceStartDate      DATETIME          = NULL,
	@pInvoiceEndDate        DATETIME          = NULL,
	/*---------------Post date range-----------------*/	
    @pFromPostDate          DATETIME          = NULL,
	@pToPostDate            DATETIME          = NULL
	
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	CREATE TABLE #fileInvoice
	(
      fileInvID       integer,
	  agentFileID     integer,
	  fileNumber      varchar(50),
	  agentID         varchar(20),
	  agentName       varchar(200),
	  invoiceNumber   varchar(50),
	  invoiceAmt      decimal(18,2) NULL,
	  invoiceDate     date,
	  posted          bit,
	  postedAmount    decimal(18,2) NULL,
	  postedDate      datetime,
	  suffix          integer,
	  seq             integer,
	  description     varchar(1000),
	  amount          decimal(18,2) NULL,
	  itemAmount      decimal(18,2) NULL
	)
	
    IF @posted  = 'P' /* get posted fileInvoice data*/
	BEGIN
      INSERT INTO #fileInvoice ([fileInvID],[agentFileID],[fileNumber],[agentID],[agentName],[invoiceNumber],[invoiceAmt],[invoiceDate],[posted] ,[postedDate],[suffix],[seq],[description],[amount],[itemAmount])
	  SELECT fv.fileInvID,
             fv.agentFileID,
			 af.fileNumber,
			 af.agentID,
             (SELECT [name] FROM [dbo].[agent] WHERE agent.[agentID] = af.[agentID]) AS [agentName],
             fv.invoiceNumber,
             fv.invoiceAmount,
             fv.invoiceDate,
			 (CASE WHEN fv.posted IS NOT NULL THEN fv.posted ELSE 0 END) AS posted ,
             fv.postedDate,
             fvi.suffix,
             fvi.seq,
             fvi.description,
             fvi.amount,
             fvi.postedamount
      FROM [dbo].[fileinv] fv 
	     INNER JOIN agentfile af
      ON af.agentFileID = fv.agentFileID
	     INNER JOIN fileinvitem fvi
      ON fv.fileinvID = fvi.fileinvID
      WHERE af.agentID = (CASE WHEN @pcAgentID           = 'ALL' THEN af.agentID ELSE @pcAgentID END)
	    AND (fv.agentFileID = CASE WHEN @agentFileID != 0 THEN @agentFileID ELSE fv.agentFileID END) 
	    AND (fv.posted = 1 )
		AND fv.postedDate >= (CASE WHEN @pFromPostDate IS NULL THEN fv.postedDate  ELSE @pFromPostDate END) 
		AND fv.postedDate <= (CASE WHEN @pToPostDate   IS NULL THEN fv.postedDate  ELSE @pToPostDate   END)
	
    END
    ELSE IF @posted  = 'U' /* Get unposted fileInvoice */
	BEGIN
      INSERT INTO #fileInvoice ([fileInvID],[agentFileID],[fileNumber],[agentID],[agentName],[invoiceNumber],[invoiceAmt],[invoiceDate],[posted],[postedDate],[suffix],[seq],[description],[amount],[itemAmount])
	  SELECT fv.fileInvID,
             fv.agentFileID,
			 af.fileNumber,
			 af.agentID,
             (SELECT [name] FROM [dbo].[agent] WHERE agent.[agentID] = af.[agentID]) AS [agentName],
             fv.invoiceNumber,
             fv.invoiceDate,
             fv.invoiceDate,
			 (CASE WHEN fv.posted IS NOT NULL THEN fv.posted ELSE 0 END) AS posted ,
             fv.postedDate,
             fvi.suffix,
             fvi.seq,
             fvi.description,
             fvi.amount,
			 fvi.postedamount
      FROM [dbo].[fileinv] fv 
	     INNER JOIN agentfile af
      ON af.agentFileID = fv.agentFileID
	     INNER JOIN fileinvitem fvi
      ON   fv.fileinvID = fvi.fileinvID
      WHERE af.agentID = (CASE WHEN @pcAgentID           = 'ALL' THEN af.agentID ELSE @pcAgentID END)
	    AND (fv.agentFileID = CASE WHEN @agentFileID != 0 THEN @agentFileID ELSE fv.agentFileID END) 
	    AND (fv.posted = 0 )
		AND fv.invoiceDate >= (CASE WHEN @pInvoiceStartDate IS NULL THEN fv.invoiceDate ELSE @pInvoiceStartDate END)
		AND fv.invoiceDate <= (CASE WHEN @pInvoiceEndDate   IS NULL THEN fv.invoiceDate ELSE @pInvoiceEndDate   END)
		
    END
    ELSE /* get posted and unposted fileInvoice data */
	BEGIN
      INSERT INTO #fileInvoice ([fileInvID],[agentFileID],[fileNumber],[agentID],[agentName],[invoiceNumber],[invoiceAmt],[invoiceDate],[posted],[postedDate],[suffix],[seq],[description],[amount],[itemAmount])
	  SELECT fv.fileInvID,
             fv.agentFileID,
			 af.fileNumber,
			 af.agentID,
             (SELECT [name] FROM [dbo].[agent] WHERE agent.[agentID] = af.[agentID]) AS [agentName],
             fv.invoiceNumber,
             fv.invoiceAmount,
             fv.invoiceDate,
			 (CASE WHEN fv.posted IS NOT NULL THEN fv.posted ELSE 0 END) AS posted ,
             fv.postedDate,
             fvi.suffix,
             fvi.seq,
             fvi.description,
             fvi.amount,
			 fvi.postedamount 
      FROM [dbo].[fileinv] fv 
	     INNER JOIN agentfile af
      ON af.agentFileID = fv.agentFileID
	     INNER JOIN fileinvitem fvi
      ON   fv.fileinvID = fvi.fileinvID
      WHERE (fv.agentFileID = CASE WHEN @agentFileID != 0 THEN @agentFileID ELSE fv.agentFileID END) 
	   
    END
    
	SELECT * FROM #fileInvoice 
	     
	DROP TABLE #fileInvoice
END
