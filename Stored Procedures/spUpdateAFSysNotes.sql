SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spUpdateAFSysNotes]
	-- Add the parameters for the stored procedure here
  @cAgentID  VARCHAR(MAX),
  @cfileID  VARCHAR(MAX),
  @listAFDelete VARCHAR(MAX) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
  SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;
  
  DECLARE @AFIDlist  VARCHAR(MAX),
		  @sourceAFID VARCHAR(30) = '',
	      @targetAFID VARCHAR(30) = '', 
		  @entry1 VARCHAR(30) = '',
          @entry2 VARCHAR(30) = '',
		  @maxSeqEntry1 INTEGER = 0,
		  @maxSeqEntry2 INTEGER = 0,
          @Pos INTEGER = 0,
          @Str NVARCHAR(MAX),
          @Delimiter NCHAR(1) = ',',
		  @sourceMaxSeq INTEGER = 0,
		  @imaxSeq integer,
		  @cSourceAFID varchar(max),
		  @ctmpAFID varchar(max),
		  @cLastSysnote varchar(max) = '',
		  @cStage varchar(max) = '',
		  @posColon integer,
		  @posComma integer,
		  @dInvAmt decimal(18,2),
		  @dOutstdAmt  decimal(18,2)
		  
  /* Get all records with particular AgentID and FileID */
  SELECT @AFIDlist = COALESCE(@AFIDlist + ',', '') + CAST(agentfileID AS VARCHAR(max))
      FROM agentfile
      WHERE agentID = @cAgentID and
			fileID  = @cFileID

  CREATE TABLE #tbAFIDList
  (
    [AFID] VARCHAR(10)
  )
  
  INSERT INTO #tbAFIDList
  SELECT [field] FROM [dbo].[GetEntityFilter] ('AFDuplicate', @AFIDlist)

  /* Get AgentFileID of record having max seq (Source AgentFileID)*/
  select @imaxSeq = max(seq), @cSourceAFID = entityid from sysnote where sysnote.entity = 'AF' and 
			sysnote.entityID in(select AFID from #tbAFIDList) group by entityID
  
  /* Put AgentFileID of all records other than that having max seq in other table */
  DECLARE cur CURSOR LOCAL FOR
  SELECT  AFID
  FROM    #tbAFIDList tb where tb.AFID <> @cSourceAFID

  OPEN cur
  FETCH NEXT FROM cur INTO @ctmpAFID
  WHILE @@FETCH_STATUS = 0 
  BEGIN
    UPDATE sysnote set entityID = @csourceAFID,
					   seq      = seq + @imaxSeq
    where entity   = 'AF'        and 
	      entityID = @ctmpAFID
    /* Put this AgentFileID in list of AgentFileIDs to be deleted */		   
    SET @listAFDelete = COALESCE(@listAFDelete + ',', '') + @ctmpAFID
	/* Update max seq of source AgentFileID */
    SELECT @imaxSeq = max(seq) from sysnote where sysnote.entity = 'AF' and 
			sysnote.entityID = @csourceAFID
    FETCH NEXT FROM cur INTO @ctmpAFID
  END
  CLOSE cur
  DEALLOCATE cur
  
  /* Set last sysnote as notes */
  UPDATE [agentfile] SET 
     @cLastSysnote = [agentfile].notes  =   (select max(sysnote.notes) FROM sysnote where 
	             entity   = 'AF'         and 
				 entityID = @cSourceAFID and 
				 seq in (select max(seq) from sysnote where entity = 'AF' and 
													entityID = @cSourceAFID 
													group by entity, entityID)) 
  where agentfile.agentid = @cAgentID and agentfile.fileid = @cFileID

  /* Set last stage as stage */ 
  SET @posColon = CHARINDEX(':', @cLastSysnote)
  SET @posComma = CHARINDEX(',', @cLastSysnote)
  
  IF (@posColon < @posComma)  
    BEGIN 
	   SET @cStage =  SUBSTRING(@cLastSysnote ,@PosColon+1, @posComma-@PosColon-1)
	END
	ELSE 
	BEGIN 
	  SET @cStage =  SUBSTRING(@cLastSysnote ,1, @posComma-1) 
	END
	
  update [agentfile] SET 
     [agentfile].stage = @cStage
	 where agentfile.agentid = @cAgentID and agentfile.fileid = @cFileID

  UPDATE  [agentfile]
    SET @dInvAmt = invoiceAmt = ISNULL((select sum(netPremium) from batch b inner join 
                     batchform bf on b.batchid = bf.batchid 
					  where b.agentID = @cAgentID and
							b.stat = 'C' and
						    bf.fileID = @cfileID group by b.agentID, bf.fileID ),0) 
							where agentfile.agentid = @cAgentID and agentfile.fileid = @cFileID
	 UPDATE  [agentfile]
    SET @dOutstdAmt = osAmt      = ISNULL((select ar.remainingamt from artran ar where
								ar.entity    = 'A'
							and ar.entityID  = @cAgentID      
							and ar.fileID    = @cfileID
							and ar.transtype = 'F'
							and ar.type      = 'F'
							and ar.Seq       = 0),0)
							where agentfile.agentid = @cAgentID and agentfile.fileid = @cFileID
	 UPDATE  [agentfile]
    SET paidAmt    = abs(invoiceAmt - osAmt)  where agentfile.agentid = @cAgentID and agentfile.fileid = @cFileID
    UPDATE  [agentfile]
    SET liabilityAmt = (select max(policy.liabilityAmount)	from policy where
						policy.agentID = @cAgentID  
					and policy.fileID  = @cfileID)
where agentfile.agentid = @cAgentID and agentfile.fileid = @cFileID

    UPDATE  [agentfile]
    SET closingDate = b.closingDt  from
	 (select max([closingDate]) as closingDt from [dbo].[agentfile] af where af.agentID = @cAgentID and
															   af.fileID = @cFileID  and 
															   af.closingDate is not NULL)b
where agentfile.agentid = @cAgentID and agentfile.fileid = @cFileID

    UPDATE  [agentfile]
    SET reportingDate = r.reportingDt  from
	 (select min([reportingDate]) as reportingDt from [dbo].[agentfile] af where af.agentID = @cAgentID and
															   af.fileID = @cFileID  and 
															   af.reportingDate is not NULL)r
where agentfile.agentid = @cAgentID and agentfile.fileid = @cFileID

END
