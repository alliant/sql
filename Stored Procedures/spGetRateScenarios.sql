USE [compass]
GO
/****** Object:  StoredProcedure [dbo].[spGetRateScenarios]    Script Date: 10/31/2019 6:39:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Sachin Chaturvedi>
-- Create date: <10-30-2019>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetRateScenarios]
	-- Add the parameters for the stored procedure here
  @piCardSetID INTEGER = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
  SELECT  *
  FROM    [dbo].[ratescenario] q
   where q.[cardSetID] = (CASE WHEN @piCardSetID  = 0     THEN q.[cardSetID]  ELSE @piCardSetID  END) 	
END
