GO
/****** Object:  StoredProcedure [dbo].[spGetOrder]    Script Date: 8/21/2019 10:38:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetOrder]
	-- Add the parameters for the stored procedure here
	@orderID VARCHAR(30) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  SELECT  [orderID],
          [orderType],
          (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'TPS' AND [objAction] = 'Order' AND [objProperty] = 'Type' AND [objID] = o.[orderType]) AS [typeDesc],
          [agentID],
          (SELECT [name] FROM [dbo].[agent] WHERE [agentID] = o.[agentID]) AS [agentName],
          [stateID],
          [countyID],
          (SELECT [description] FROM [dbo].[county] WHERE [stateID] = o.[stateID] AND [countyID] = o.[countyID]) AS [countyDesc],
          [stat],
          (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'TPS' AND [objAction] = 'Order' AND [objProperty] = 'Status' AND [objID] = o.[stat]) AS [statDesc],
          [customer],
          [email],
          [manager],
          [uid],
          (SELECT [name] FROM [dbo].[sysuser] WHERE [uid] = o.[uid]) AS [uidDesc],
          [reference],
          [createDate],
          [createdBy],
          [completedDate],
          [effDate],
          [legalDesc] AS [legal],
          [underwriter],
          [fileNumber]
  FROM    [dbo].[order] o
  WHERE   [orderID] = CASE WHEN @orderID = '' THEN [orderID] ELSE @orderID END
END
