GO
/****** Object:  StoredProcedure [dbo].[spDeleteDuplicateAF]    Script Date: 7/5/2018 4:24:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spDeleteDuplicateAF]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
  SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;
  
  DECLARE @List VARCHAR(MAX) ,
        @tmpAgentFileIDs VARCHAR(MAX),
		@tmpAID VARCHAR(MAX),
		@tmpFID VARCHAR(MAX),
		@listAFDelete  VARCHAR(MAX)
		
  
  CREATE TABLE #T1 ([AID] VARCHAR(30),
				  [FID] VARCHAR(30)/*,
				  [AgentFileIDs] VARCHAR(30),
				  [sourceMaxSeq] integer*/
				  )
  /* Get agentID and FileID for duplicate records */
  INSERT INTO #T1 ([AID],[FID]/*,AgentFileIDs*/) 
  select  agentID,
            fileID /*
			''*/
    from    dbo.agentfile
    group by agentID, fileID
    having count(*) > 1

  /* Merge the information for all duplicates in one record and delete others */
  DECLARE cur CURSOR LOCAL FOR
  SELECT  AID,FID
  FROM    #T1 t

  OPEN cur
  FETCH NEXT FROM cur INTO @tmpAID, @tmpFID
  WHILE @@FETCH_STATUS = 0 
  BEGIN
    EXEC [dbo].[spUpdateAFSysNotes] @tmpAID, @tmpFID, @listAFDelete output
	CREATE TABLE #AFObsolete
    (
    [AFID] VARCHAR(10)
    )

    INSERT INTO #AFObsolete
      SELECT [field] FROM [dbo].[GetEntityFilter] ('AFObsolete', @listAFDelete)

    DELETE from dbo.agentfile where agentfile.agentFileID in (select [AFID] from #AFObsolete)
    DROP TABLE #AFObsolete
    FETCH NEXT FROM cur INTO @tmpAID, @tmpFID
  END
  CLOSE cur
  DEALLOCATE cur

  /* select * from #T1  order by aID ,fID  */
  drop table #T1
END









