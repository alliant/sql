
/****** Object:  StoredProcedure [dbo].[spGetTasks]    Script Date: 5/4/2023 12:58:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetTaskTopics]
	-- Add the parameters for the stored procedure here
	@Active VARCHAR(1) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	CREATE TABLE #tempTaskTopic
	(
		[topic]         VARCHAR(100),
		[description]   VARCHAR(500),
		[Active]        BIT,
	    [isSecure]      BIT
		
	)
	IF (@Active = '')
	 SET @Active = 'B'

	IF (@Active = 'A')  /* (A)ctive */
     INSERT INTO #tempTaskTopic([topic],[description],[Active],[isSecure])
     SELECT [topic],
		    [description],
		    [active],
			[isSecure]
	 FROM [dbo].[tasktopic]
     WHERE tasktopic.active = 1

	ELSE IF (@Active = 'I')  /* (I)nactive */
	 INSERT INTO #tempTaskTopic([topic],[description],[Active],[isSecure])
     SELECT [topic],
		    [description],
		    [active],
			[isSecure]
	 FROM [dbo].[tasktopic]
     WHERE tasktopic.active = 0

	ELSE IF (@Active = 'B') /* (B)oth Active and Inactive */
	 INSERT INTO #tempTaskTopic([topic],[description],[Active],[isSecure])
     SELECT [topic],
		   [description],
		   [active],
		   [isSecure]
	 FROM [dbo].[tasktopic]

    SELECT * FROM #tempTaskTopic

	DROP TABLE #tempTaskTopic

END