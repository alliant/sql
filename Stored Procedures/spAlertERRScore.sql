USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spAlertERRScore]    Script Date: 12/27/2017 8:09:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spAlertERRScore]
  @agentID VARCHAR(MAX) = '',
  @user VARCHAR(100) = 'compass@alliantnational.com',
  @preview BIT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;
  
  CREATE TABLE #agentTable ([agentID] VARCHAR(30))
  INSERT INTO #agentTable ([agentID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)
  
  DECLARE @alertID INTEGER = 0,
          -- Used for the preview
          @source VARCHAR(30) = 'QAR',
          @processCode VARCHAR(30) = 'QAR02',
          @effDate DATETIME = NULL,
          @score DECIMAL(18,2) = 0,
          -- Notes for the alert note
          @note VARCHAR(MAX) = ''
          
  CREATE TABLE #scoreTable 
  (
    [agentID] VARCHAR(20),
    [qarDate] DATETIME,
    [errScore] INTEGER
  )
  
  --Insert the current scores
  INSERT INTO #scoreTable ([agentID], [qarDate] , [errScore])
  SELECT  q.[agentID],
          d.[qarDate],
          ISNULL(q.[score],0) AS 'errScore'
  FROM    [dbo].[qar] q INNER JOIN
          [dbo].[agent] a
  ON      q.[agentID] = a.[agentID] INNER JOIN
          (
          SELECT  [agentID],
                  MAX([auditFinishDate]) AS 'qarDate'
          FROM    [dbo].[qar]
          WHERE   [stat] = 'C'
          GROUP BY [agentID]
          ) d
  ON      q.[agentID] = d.[agentID]
  AND     q.[auditFinishDate] = d.[qarDate] 
  AND     q.[stat] = 'C'
  AND     q.[agentID] IN (SELECT [agentID] FROM #agentTable)
  
  IF (@preview = 0)
  BEGIN
    -- ERR Score Alert
    DECLARE cur CURSOR LOCAL FOR
    SELECT  [agentID],
            [errScore],
            [qarDate],
            'The ERR Score for ' + CONVERT(VARCHAR, [qarDate], 101) + ' is ' + [dbo].[FormatNumber] ([errScore], 0)
    FROM    #scoreTable

    OPEN cur
    FETCH NEXT FROM cur INTO @agentID, @score, @effDate, @note
    WHILE @@FETCH_STATUS = 0 BEGIN
      EXEC [dbo].[spInsertAlert] @source = @source, 
                                  @processCode = @processCode, 
                                  @user = @user, 
                                  @agentID = @agentID,
                                  @score = @score, 
                                  @effDate = @effDate,
                                  @note = @note
      FETCH NEXT FROM cur INTO @agentID, @score, @effDate, @note
    END
    CLOSE cur
    DEALLOCATE cur
  END

  SELECT  [agentID] AS [agentID],
          @source AS [source],
          @processCode AS [processCode],
          [dbo].[GetAlertThreshold] (@processCode, [errScore]) AS [threshold],
          [dbo].[GetAlertThresholdRange] (@processCode, [errScore]) AS [thresholdRange],
          [dbo].[GetAlertSeverity] (@processCode, [errScore]) AS [severity],
          [errScore] AS [score],
          [dbo].[GetAlertScoreFormat] (@processCode, [errScore]) AS [scoreDesc],
          [qarDate] AS [effDate],
          [dbo].[GetAlertOwner] (@processCode) AS [owner],
          'The ERR Score for ' + CONVERT(VARCHAR, [qarDate], 101) + ' is ' + [dbo].[FormatNumber] ([errScore], 0)  AS [note]
  FROM    #scoreTable

  DROP TABLE #agentTable
  DROP TABLE #scoreTable
END
