USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spInsertProperty]    Script Date: 4/8/2016 2:10:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		John Oliver (joliver)
-- Create date: 4.8.2016
-- Description:	Used to insert a new row into the sysprop table
-- =============================================
ALTER PROCEDURE [dbo].[spInsertProperty]
	-- Add the parameters for the stored procedure here
  @appCode         varchar(20)  = NULL, -- The AppCode of the Compass Module
  @objAction       varchar(100) = NULL,
  @objID           varchar(100) = NULL,
  @objProperty     varchar(100) = NULL,
  @objValue        varchar(100) = NULL,
  @objName         varchar(200) = '',
  @objDesc         varchar(max) = '',
  @objRef          varchar(max) = '',
  @modifiedBy      varchar(100)  = NULL,
  @comments        varchar(max) = ''   -- Additional notes
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  -- Insert statements for procedure here
  DECLARE @isError bit = 0,
          @count INTEGER = 0

  IF (@objAction IS NULL)
  BEGIN
    RAISERROR(N'The object action is missing',10,1)
    SET @isError = 1
  END
  IF (@objID IS NULL)
  BEGIN
    RAISERROR(N'The object ID is missing.',10,1)
  END
  IF (@objProperty IS NULL)
  BEGIN
    RAISERROR(N'The object property is missing',10,1)
    SET @isError = 1
  END
  IF (@objValue IS NULL)
  BEGIN
    RAISERROR(N'The object value is missing',10,1)
    SET @isError = 1
  END
  IF @isError = 1
  BEGIN
    RETURN(1)
  END
  
  SELECT @count=COUNT(*) FROM [dbo].[sysprop] WHERE [appCode] = @appCode AND [objAction] = @objAction AND [objProperty] = @objProperty AND [objID] = @objID AND [objValue] = @objValue
  IF (@count = 0)
  BEGIN
    INSERT INTO [dbo].[sysprop]
    (           
           [appCode]
          ,[objAction]
          ,[objID]
          ,[objProperty]
          ,[objValue]
          ,[objName]
          ,[objDesc]
          ,[objRef]
          ,[lastModified]
          ,[modifiedBy]
          ,[comments]
    )
    VALUES
    (
           @appCode
          ,@objAction
          ,@objID
          ,@objProperty
          ,@objValue
          ,@objName
          ,@objDesc
          ,@objRef
          ,GETDATE()
          ,@modifiedBy
          ,@comments
    )
  END
  ELSE
  BEGIN
      UPDATE  [dbo].[sysprop]
      SET     [objValue] = @objValue,
              [objName] = @objName,
              [objDesc] = @objDesc,
              [objRef] = @objRef,
              [lastModified] = GETDATE(),
              [modifiedBy] = @modifiedBy
      WHERE   [appCode] = @appCode
      AND     [objAction] = @objAction
      AND     [objProperty] = @objProperty
      AND     [objID] = @objID
      AND     [objValue] = @objValue
  END
END
