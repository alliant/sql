/****** Object:  StoredProcedure [dbo].[spGetOpenProductionFiles]    Script Date: 8/2/2024 10:52:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetOpenProductionFiles] 
	-- Add the parameters for the stored procedure here
	@agentID      VARCHAR(max)  = '',
	@FileID       VARCHAR(MAX)  = '',
	@ReferenceID  Integer       = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @remaining decimal,
	        @applied decimal

    -- Insert statements for procedure here
    CREATE TABLE #artran (artranID         INTEGER,
	                      revenueType      VARCHAR(50),
						  tranID           VARCHAR(50),
						  seq              INTEGER,
						  sourceType       VARCHAR(50),
						  sourceID         VARCHAR(50),
						  type             VARCHAR(50),
	                      entity           VARCHAR(50),
	                      entityID         VARCHAR(50),
						  entityName       VARCHAR(200),
						  fileNumber       VARCHAR(50),
	                      fileID           VARCHAR(50),
						  tranAmt          [DECIMAL](18, 2) NULL,
						  remainingAmt     [DECIMAL](18, 2) NULL,
					      tranDate         DATETIME,
                          void             BIT,
                          voidDate	       DATETIME,
                          voidBy	       VARCHAR(100),
                          notes            VARCHAR(MAX),
						  arNotes          VARCHAR(MAX), 
                          createdBy        VARCHAR(100),
                          createdDate      DATETIME,
                          dueDate          DATETIME,
                          fullyPaid        BIT,
                          reference        VARCHAR(100),
                          appliedAmt       [DECIMAL](18, 2) NULL,
                          transType        VARCHAR(20),
                          postBy           VARCHAR(100), 						  
						  postDate         DATETIME,
						  ledgerID         INTEGER,
						  voidledgerID     INTEGER, 
						  appliedamtbyref  [DECIMAL](18, 2) NULL,
						  accumbalance     [DECIMAL](18, 2) NULL)


   if @ReferenceID = 0 
    BEGIN
    INSERT INTO #artran (artranID,seq, sourceType, type, entity, entityID,  fileNumber, fileID, tranAmt,
	                         remainingAmt, 
		    			     appliedAmt,tranDate)

	select fl.filearID, fl.seq, fl.sourceType,fl.type,'A',fl.agentID,fl.fileID,fl.fileID,
	CASE WHEN exists(select * from filear where filear.fileID = fl.fileID and type = 'I' and filear.agentFileID = fl.agentFileID )
	THEN CASE WHEN exists(select * from filear where filear.fileID = fl.fileID and type = 'W' and filear.agentFileID = fl.agentFileID ) 
	 THEN ((select sum(amount) from filear where filear.fileID = fl.fileID and type = 'I' and filear.agentFileID = fl.agentFileID) + (select sum(amount) from filear where filear.fileID = fl.fileID and type = 'W' and filear.agentFileID = fl.agentFileID) )
	 ELSE (select sum(amount) from filear where filear.fileID = fl.fileID and type = 'I' and filear.agentFileID = fl.agentFileID) END
	 ELSE (select sum(amount) from filear where filear.fileID = fl.fileID and type = 'F' and filear.agentFileID = fl.agentFileID) END,fl.amount,(select - sum(amount) from filear where filear.fileID = fl.fileID and type = 'A' and filear.agentFileID = fl.agentFileID),
	 (select min(tranDate) from filear where filear.fileID = fl.fileID and  filear.agentFileID = fl.agentFileID )
	from filear as fl
	where fl.agentID = (CASE WHEN @agentID  = '' THEN fl.agentID  ELSE @agentID  END)   and
	        fl.sourceType = 'P' and
			fl.type = 'F'  and fl.seq = 0  and
	      fl.fileID  = (CASE WHEN @FileID  = '' THEN fl.fileID  ELSE @FileID  END)
  
   delete  #artran  where #artran.type = 'A' and  exists( select * from filear where filear.agentID = #artran.entityID and filear.fileID = #artran.fileID  and filear.type = 'F')
  
    END
  else
   BEGIN
    INSERT INTO #artran (artranID,seq, sourceType, type, entity, entityID,  fileNumber, fileID, tranAmt,
	                         remainingAmt, 
		    			     appliedAmt,appliedamtbyref)
    select fl.filearID,fl.seq ,fl.sourceType,'F','A',fl.agentID,fl.fileID,fl.fileID,
	CASE WHEN exists(select * from filear where filear.fileID = fl.fileID and type = 'W' and filear.agentFileID = fl.agentFileID )
	 THEN ((select sum(amount) from filear where filear.fileID = fl.fileID and type = 'I' and filear.agentFileID = fl.agentFileID) + (select sum(amount) from filear where filear.fileID = fl.fileID and type = 'W' and filear.agentFileID = fl.agentFileID) )
	 else (select sum(amount) from filear where filear.fileID = fl.fileID and type = 'I' and filear.agentFileID = fl.agentFileID) END ,
	 (select amount from filear where filear.fileID = fl.fileID and type = 'F' and filear.agentFileID = fl.agentFileID), 
	 (select - sum(amount) from filear where filear.fileID = fl.fileID and type = 'A' and filear.agentFileID = fl.agentFileID),fl.amount
	from filear as fl left outer join artran as ar  on ar.arTranID = fl.arTranID 
	left outer join artran as at on at.arTranID = ar.sourceID and at.entityID    = ar.entityID  
	where fl.agentID = (CASE WHEN @agentID  = '' THEN fl.agentID  ELSE @agentID  END) and fl.sourceType = 'P' and
	      fl.type = 'A'  and
	      fl.fileID  = (CASE WHEN @FileID  = '' THEN fl.fileID  ELSE @FileID  END) and
		  fl.arTranID = ar.arTranID  and
		  ar.sourceID = cast(@ReferenceID as varchar(20))  and 
		  at.arTranID = ar.sourceID and 
		  EXISTS(SELECT * FROM artran at WHERE        at.entity      = ar.entity
			                                     AND  at.entityID    = ar.entityID 
											     AND  at.arTranID    = cast(ar.sourceID as integer) 
			                                     AND at.type = 'P') 




    update #artran set artranID = (select filearID from filear as fl  where fl.agentID = #artran.entityID and fl.fileID = #artran.fileID and fl.sourceType = 'P' and fl.type = 'F') 

    

   END

	select * from #artran


	drop table #artran 
								 

END
