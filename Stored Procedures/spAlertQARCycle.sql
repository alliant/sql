GO
/****** Object:  StoredProcedure [dbo].[spAlertQARCycle]    Script Date: 12/27/2017 10:27:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spAlertQARCycle]
  @agentID VARCHAR(MAX) = '',
  @user VARCHAR(100) = 'compass@alliantnational.com',
  @preview BIT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  IF (@agentID = '')
    SET @agentID = 'ALL'
  
  CREATE TABLE #agentTable ([agentID] VARCHAR(30))
  INSERT INTO #agentTable ([agentID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)

  DECLARE @alertID INTEGER = 0,
          -- Used for the preview
          @source VARCHAR(30) = 'QAR',
          @processCode VARCHAR(30) = 'QAR01',
          @threshold VARCHAR(MAX) = '',
          @severity INTEGER,
          @effDate DATETIME = NULL,
          @score DECIMAL(18,2) = 0,
          @owner VARCHAR(30) = '',
          -- Used to capture the three threshold values
          @threshold3 DECIMAL(18,2),
          @threshold2 DECIMAL(18,2),
          @threshold1 DECIMAL(18,2),
          -- Notes for the alert note
          @note VARCHAR(MAX) = ''
    
  CREATE TABLE #dateTable
  (
    [agentID] VARCHAR(30),
    [effDate] DATETIME,
    [score] INTEGER,
    [note] VARCHAR(MAX)
  )

  INSERT INTO #dateTable
  SELECT  prev.[agentID],
          prev.[date] AS [effDate],
          DATEDIFF(day,prev.[date],ISNULL(curr.[evalDate],GETDATE())) AS [score],
          CASE
            WHEN curr.[agentID] IS NULL THEN 'No QAR has been scheduled'
            WHEN curr.[agentID] IS NOT NULL THEN 
            CASE
              WHEN curr.[date] < GETDATE() THEN 'The scheduled QAR/ERR is past due by ' + [dbo].[FormatNumber] (DATEDIFF(DAY,ISNULL(curr.[evalDate],GETDATE()),GETDATE()), 0) + ' day(s)'
              WHEN curr.[date] > GETDATE() THEN 'The next QAR/ERR is scheduled in ' + [dbo].[FormatNumber] (DATEDIFF(DAY,ISNULL(curr.[evalDate],GETDATE()),GETDATE()), 0) + ' day(s)'
            END
          END AS [note]
  FROM    (
          SELECT  a.[agentID],
                  ISNULL(MAX(q.[auditFinishDate]),ISNULL(MAX(a.[activeDate]),MAX(a.[createDate]))) AS [date]
          FROM    [dbo].[agent] a LEFT OUTER JOIN
                  [dbo].[qar] q
          ON      q.[agentID] = a.[agentID]
          AND    (q.[auditType] = 'Q' OR q.[auditType] = 'E')
          AND     q.[stat] = 'C'
          WHERE   a.[stat] = 'A'
          GROUP BY a.[agentID]
          ) prev LEFT OUTER JOIN
          (
          SELECT  q.[agentID],
                  ISNULL(MAX(q.[auditStartDate]),ISNULL(MAX(q.[schedStartDate]),MAX(q.[createDate]))) AS [date],
                  MAX(q.[createDate]) AS [evalDate]
          FROM    [dbo].[agent] a INNER JOIN
                  [dbo].[qar] q
          ON      q.[agentID] = a.[agentID]
          WHERE  (q.[auditType] = 'Q' OR q.[auditType] = 'E')
          AND    (q.[stat] = 'P' OR q.[stat] = 'Q' OR q.[stat] = 'A')
          GROUP BY q.[agentID]
          ) curr
  ON      prev.[agentID] = curr.[agentID]
  WHERE   prev.[agentID] IN (SELECT [agentID] FROM #agentTable)
  
  IF (@preview = 0)
  BEGIN
    -- QAR within cycle Alert
    DECLARE cur CURSOR LOCAL FOR
    SELECT  [agentID],
            [score],
            [effDate],
            [note]
    FROM    #dateTable

    OPEN cur
    FETCH NEXT FROM cur INTO @agentID, @score, @effDate, @note
    WHILE @@FETCH_STATUS = 0 BEGIN
      EXEC [dbo].[spInsertAlert] @source = @source,
                                 @processCode = @processCode, 
                                 @user = @user,
                                 @agentID = @agentID, 
                                 @score = @score, 
                                 @effDate = @effDate,
                                 @note = @note
      FETCH NEXT FROM cur INTO @agentID, @score, @effDate, @note
    END
    CLOSE cur
    DEALLOCATE cur
  END
  
  SELECT  [agentID] AS [agentID],
          @source AS [source],
          @processCode AS [processCode],
          [dbo].[GetAlertThreshold] (@processCode, [score]) AS [threshold],
          [dbo].[GetAlertThresholdRange] (@processCode, [score]) AS [thresholdRange],
          [dbo].[GetAlertSeverity] (@processCode, [score]) AS [severity],
          [score] AS [score],
          [dbo].[GetAlertScoreFormat] (@processCode, [score]) AS [scoreDesc],
          [effDate],
          [dbo].[GetAlertOwner] (@processCode) AS [owner],
          [note]
  FROM    #dateTable

  DROP TABLE #agentTable
  DROP TABLE #dateTable
END
