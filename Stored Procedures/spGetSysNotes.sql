-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
USE [COMPASS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetSysNotes] 
	-- Add the parameters for the stored procedure here
	@ipPageNo        INTEGER,
	@ipNoOfRows      INTEGER,
	@pcEntity        VARCHAR(10)  = 'ALL',
    @pcEntityID      VARCHAR(50)  = 'ALL',
    @pcUID           VARCHAR(100) = 'ALL'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @iNoCalls  INTEGER = 0,
	        @iCount    INTEGER = 0

	CREATE TABLE #ttsysnote (sysNoteID    integer,
							 seq          integer,
							 UID	      varchar(100),
							 entity	      varchar(100),
							 entityID     varchar(100),
							 notes        varchar(320),
							 rowNum       INTEGER,
							 dateCreated  DATETIME,
							 dateModified DATETIME)

    CREATE TABLE #tempsysnote (sysNoteID    integer,
							   seq          integer,
							   UID	        varchar(100),
							   entity	    varchar(100),
							   entityID     varchar(100),
							   notes        varchar(320),
							   rowNum       INTEGER,
							   dateCreated  DATETIME,
							   dateModified DATETIME)

    INSERT INTO #ttsysnote([sysNoteID],[seq],[UID],[entity],[entityID],[notes],[dateCreated],[dateModified]) 
	  SELECT sysnote.sysNoteID,
		     sysnote.seq,
		     sysnote.UID,
		     sysnote.entity,
		     sysnote.entityID,
		     sysnote.notes,
		     sysnote.dateCreated,
	         sysnote.dateModified
	    FROM sysnote WHERE sysnote.entity     = (CASE WHEN @pcEntity   = 'ALL' THEN sysnote.entity   ELSE @pcEntity   END)
			           AND sysnote.entityID   = (CASE WHEN @pcEntityID = 'ALL' THEN sysnote.entityID ELSE @pcEntityID END)  
			           AND sysnote.uid        = (CASE WHEN @pcUID      = 'ALL' THEN sysnote.uid      ELSE @pcUID      END) 
	UPDATE #ttsysnote SET @iNoCalls = #ttsysnote.rowNum = @iNoCalls + 1	 

    INSERT INTO #tempsysnote([sysNoteID],[seq],[UID],[entity],[entityID],[notes],[rowNum],[dateCreated],[dateModified])
	  SELECT TOP (@ipNoOfRows) 
	      #ttsysnote.sysNoteID,
		  #ttsysnote.seq,
		  #ttsysnote.UID,
		  #ttsysnote.entity,
		  #ttsysnote.entityID,
		  #ttsysnote.notes,
		  #ttsysnote.rowNum,
		  #ttsysnote.dateCreated,
	      #ttsysnote.dateModified
	 FROM #ttsysnote WHERE #ttsysnote.rowNum >= (@ipNoOfRows * (@ipPageNo - 1) + 1)    												 
	
	INSERT INTO #tempsysnote(entity,rowNum) values('TotalNotes',@iNoCalls)
	
	SELECT * FROM  #tempsysnote
END
GO
