USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spReportQARScore]    Script Date: 10/11/2018 9:49:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportQARScore]
	-- Add the parameters for the stored procedure here
  @stateID VARCHAR(100) = 'ALL',
  @agentID VARCHAR(MAX) = 'ALL'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  CREATE TABLE #stateTable ([stateID] VARCHAR(2))
  INSERT INTO #stateTable ([stateID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('State', @stateID)

  CREATE TABLE #agentTable ([agentID] VARCHAR(30))
  INSERT INTO #agentTable ([agentID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)

  CREATE TABLE #qarTable
  (
    [agentID] VARCHAR(20),
    [auditType] VARCHAR(20),
    [qarScore] INTEGER,
    [auditDate] DATETIME,
    [errScore] INTEGER,
    [seq] INTEGER
  )

  INSERT INTO #qarTable ([agentID],[auditType],[qarScore],[auditDate],[errScore],[seq])
  SELECT  q.[agentID],
          q.[auditType],
          q.[score] AS [qarScore],
          COALESCE(q.[auditReviewDate],q.[auditStartDate]) AS [auditDate],
          s.[score] AS [errScore],
          ROW_NUMBER() OVER(PARTITION BY q.[agentID] ORDER BY COALESCE(q.[auditReviewDate],q.[auditStartDate]) DESC) AS [seq]
  FROM    [dbo].[qar] q LEFT OUTER JOIN
          [dbo].[qarsection] s
  ON      q.[qarID] = s.[qarID]
  AND     s.[sectionID] = 6
  WHERE   q.[stat] = 'C'
  AND     q.[auditType] IN ('Q','E')
  AND     q.[score] > 0
  AND     s.[score] > 0

  SELECT  a.[agentID],
          a.[name],
          a.[stateID],
          a.[stat],
          q1.[auditType] AS [lastAuditType],
          q1.[qarScore] AS [lastQarScore],
          q1.[errScore] AS [lastErrScore],
          q1.[auditDate] AS [lastAuditDate],
          q2.[auditType] AS [secondAuditType],
          q2.[qarScore] AS [secondQarScore],
          q2.[errScore] AS [secondErrScore],
          q2.[auditDate] AS [secondAuditDate]
  FROM    [dbo].[agent] a LEFT OUTER JOIN
          #qarTable q1 
  ON      a.[agentID] = q1.[agentID]
  AND     q1.[seq] = 1 LEFT OUTER JOIN
          #qarTable q2
  ON      a.[agentID] = q2.[agentID]
  AND     q2.[seq] = 2 INNER JOIN
          #agentTable at
  ON      a.[agentID] = at.[agentID] INNER JOIN
          #stateTable st
  ON      a.[stateID] = st.[stateID]
  ORDER BY a.[agentID]

  DROP TABLE #qarTable
  DROP TABLE #agentTable
  DROP TABLE #stateTable
END
