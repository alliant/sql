-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetProduct] 
	-- Add the parameters for the stored procedure here
     
	 @stateIDlist VARCHAR(200) = '',
	 @activeOnly BIT = 0,
	 @productID INTEGER = 0

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	CREATE TABLE #tempProduct
	(
		[productID]     INTEGER,
		[stateID]       VARCHAR(2),
		[description]   VARCHAR(100),
		[price]         DECIMAL(18,2),
		[effectivedate] DATE,
		[expirydate]    DATE,
		[revenuetype]   VARCHAR(50),
		[active]        BIT
	)
	IF @stateIDlist <> ''
	INSERT INTO #tempProduct([productID], [stateID], [description], [price], [effectivedate], [expirydate], [revenuetype], [active])
	SELECT  [productID],
            [stateID],   
            [description],
            [price],
            [effectivedate],
            [expirydate],
		    [revenuetype],
            [active]
    FROM    [dbo].[product] 
    WHERE   [stateid]  IN (select value from string_split(@stateIDlist,','))
     AND    [active]    = CASE WHEN @activeOnly != 0 THEN 1 ELSE product.active END 
	 AND    [productid] = CASE WHEN @productID != 0 THEN @productID ELSE product.productid END
	ELSE
	INSERT INTO #tempProduct([productID], [stateID], [description], [price], [effectivedate], [expirydate], [revenuetype], [active])
	SELECT  [productID],
            [stateID],   
            [description],
            [price],
            [effectivedate],
            [expirydate],
		    [revenuetype],
            [active]
    FROM    [dbo].[product] 
	WHERE    [active]    = CASE WHEN @activeOnly != 0 THEN 1 ELSE product.active END 
	 AND    [productid] = CASE WHEN @productID != 0 THEN @productID ELSE product.productid END
  
	SELECT * FROM #tempProduct

	DROP TABLE #tempProduct
	
END
GO
