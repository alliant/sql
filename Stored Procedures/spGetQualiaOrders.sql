USE [COMPASS_DVLP]
GO
/****** Object:  StoredProcedure [dbo].[spGetQualiaOrders]    Script Date: 3/1/2021 4:01:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetQualiaOrders]
	-- Add the parameters for the stored procedure here
	@qualiaID VARCHAR(50) = '',
  @stat VARCHAR(50) = 'ALL'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  IF @stat = 'ALL'
    SET @stat = 'N,P,D,X,C,S'

  CREATE TABLE #status ([stat] VARCHAR(10))
  INSERT INTO #status ([stat])
  SELECT value FROM STRING_SPLIT(@stat, ',')

  -- Insert statements for procedure here
  SELECT  [qualiaID],
          [customer_id],
          [customer_deployment],
          [orderID],
          [agentID],
          [ccOrderID],
          [uid],
          [stat],
          [orderDate],
          [completeDate],
          [cancelDate],
          [dueDate],
          [fileNumber],
          [fileID],
          (SELECT [description] FROM [qualiaactivity] WHERE [qualiaID] = q.[qualiaID] AND [seq] = 2) AS [description],
          ISNULL((SELECT [name] FROM [agent] WHERE [agentID] = q.[agentID]), '') AS [agentName],
          ISNULL((SELECT [name] FROM [sysuser] WHERE [uid] = q.[uid]), '') AS [uidDesc],
          CASE [stat]
            WHEN 'N' THEN 'New'
            WHEN 'X' THEN 'Cancelled'
            WHEN 'C' THEN 'Completed'
            WHEN 'P' THEN 'In Process'
            WHEN 'D' THEN 'Denied'
            WHEN 'S' THEN 'Submitted'
          END AS [statDesc]
  FROM    [qualia] q
  WHERE   [qualiaID] = CASE WHEN @qualiaID = '' THEN [qualiaID] ELSE @qualiaID END
  AND     q.[stat] IN (SELECT [stat] FROM #status)
END
