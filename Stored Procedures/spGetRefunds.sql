
/****** Object:  StoredProcedure [dbo].[spGetRefunds]    Script Date: 6/15/2021 6:04:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<SA>
-- Create date: <22-04-2021>
-- Description:	<Get refunds Payments/Credits Report>
-- =============================================
ALTER PROCEDURE [dbo].[spGetRefunds] 
	-- Add the parameters for the stored procedure here 
	@refundID          varchar(10)  = '',
    @entityID          varchar(10)  = 'ALL',
	@searchString      varchar(MAX) = '',
	@fromDate          datetime     = null,
	@toDate            datetime     = null,
	@includeVoid	   bit,
	@includeArchived   bit
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  -- Temporary table definition
  create table #UnappliedTran  (unappliedtranID  integer,
								revenuetype      varchar(50),    /* revenue type of payment */
								entity           varchar(50),
								entityID         varchar(50),
								tranID           VARCHAR(50),
								arTranID         VARCHAR(50),
								sourceID         VARCHAR(50),
								reference        varchar(50),
								referencedate    datetime,
								receiptdate      datetime,
								referenceamt     [decimal](18, 2) NULL,
								remainingamt     [decimal](18, 2) NULL,
								void             bit,
								voiddate         datetime,
								voidby           varchar(100),
								voidbyusername   varchar(100),
								description      varchar(max),
								createddate      datetime,
								createdby        varchar(100),
								username         varchar(100),
								filenumber       varchar(100),
								fileID           varchar(100),
								fileExist        varchar(3) default 'NO',
								posted           bit,            /* true if the payment is posted */
								transType        varchar(20),    /* 'F' if payment is file bed else '' */
								archived         bit,
								postdate         datetime,
								postby           varchar(100),
								postbyname       varchar(100),
								entityname       varchar(100),             /* Client side */
								transDate        datetime,                /* Client side */
								appliedamt       [decimal](18, 2) NULL,   /* Client side */
								refundamt        [decimal](18, 2) NULL,
                                type             varchar(20), /* 'P' or 'C' if payment or credit type tran */								
								totalrefundamt   [decimal](18, 2) NULL
								)


     /*-------Posted/Refund Payments-----------*/
	insert into #UnappliedTran (unappliedtranID, revenuetype, entity, entityID, tranID ,artranID, reference, sourceID, referencedate, 
						receiptdate, referenceamt , void, voiddate, voidby, description, 
						createddate, createdby, filenumber, fileID, posted, transType, archived, 
						postdate, postby, entityname, transDate, type)
	select ap.arpmtID, ap.revenuetype, ap.entity, ap.entityID,at.tranID ,at.artranID, at.reference, at.sourceID, ap.checkdate, 
			ap.receiptdate, ap.checkamt , at.void, at.voiddate, at.voidby, ap.description, 
			ap.createddate, ap.createdby, ap.filenumber, ap.fileID, ap.posted, ap.transType, ap.archived, 
			ap.postdate, ap.postby, a.name, at.tranDate, at.type  
		from arpmt ap 
			inner join agent a 
		on  ap.EntityID = a.agentID 
			inner join artran at 
		on  ap.arpmtID = at.tranID AND at.type = 'P' 
		where  ap.posted     = 1
		  AND ap.entity    = 'A' 
		  AND ap.entityID  = (CASE WHEN @entityID = 'ALL' THEN ap.entityID  ELSE @entityID END)
		  AND ((@searchString <> '' AND (ap.checknum LIKE '%' + @searchString + '%') or  (cast(at.sourceID as varchar) LIKE '%' + @searchString + '%' )))
		  AND at.tranDate >= (CASE WHEN @fromDate IS NULL THEN at.tranDate  ELSE @fromDate END) 
		  AND at.tranDate <= (CASE WHEN @toDate   IS NULL THEN at.tranDate  ELSE @toDate   END)
		  AND at.sourceID = (CASE WHEN @refundID   = '' THEN at.sourceID ELSE @refundID   END) 
          AND at.sourceType = 'RB'
		  AND at.void          = (CASE WHEN @includeVoid  = 1 THEN at.void ELSE 0 END)
		  And at.tranAmt < 0
		   
    /*-------Posted/Refund Credits-----------*/
	insert into #UnappliedTran (unappliedtranID, revenuetype, entity, entityID, tranID ,artranID, reference, sourceID, referencedate,
	                    referenceamt , void, voiddate, voidby, description, createddate, createdby, filenumber, fileID, posted, 
						transType, archived, postdate, postby, entityname, transDate, type)
	select am.armiscID, am.revenuetype, am.entity, am.entityID,at.tranID ,at.artranID, at.reference, at.sourceID, am.transDate, 
		    am.transamt , at.void, at.voiddate, at.voidBy, am.notes, 
			am.createdate, am.createdby, am.filenumber, am.fileID, am.posted, am.transType, am.archived, 
			am.postdate, am.postedBy, a.name, at.tranDate, at.type  
		from armisc am 
			inner join agent a 
		on  am.EntityID = a.agentID 
			inner join artran at 
		on  am.armiscID = at.tranID AND at.type = 'C'
		where  am.posted     = 1
		  AND am.entity    = 'A' 
		  AND am.entityID  = (CASE WHEN @entityID = 'ALL' THEN am.entityID  ELSE @entityID END)
		  AND ((@searchString <> '' AND (am.reference LIKE '%' + @searchString + '%' )  or  (cast(at.sourceID as varchar) LIKE '%' + @searchString + '%' )))
		  AND at.tranDate >= (CASE WHEN @fromDate IS NULL THEN at.tranDate  ELSE @fromDate END) 
		  AND at.tranDate <= (CASE WHEN @toDate   IS NULL THEN at.tranDate  ELSE @toDate   END)
		  AND at.sourceID = (CASE WHEN @refundID   = ''  THEN at.sourceID ELSE @refundID   END) 
		  AND at.sourceType = 'RB'
          AND at.void          = (CASE WHEN @includeVoid  = 1 THEN at.void ELSE 0 END)
          And at.tranAmt < 0
		  
    /*-------Posted/Refund Payments (Archived)-----------*/
	if @includeArchived = 1
	begin
		insert into #UnappliedTran (unappliedtranID, revenuetype, entity, entityID, tranID ,artranID, reference, sourceID, referencedate, 
							receiptdate, referenceamt , void, voiddate, voidby, description, 
							createddate, createdby, filenumber, fileID, posted, transType, archived, 
							postdate, postby, entityname, transDate, type)
		select ap.arpmtID, ap.revenuetype, ap.entity, ap.entityID,at.tranID ,at.artranID, at.reference, at.sourceID, ap.checkdate, 
				ap.receiptdate, ap.checkamt , at.void, at.voiddate, at.voidby, ap.description, 
				ap.createddate, ap.createdby, ap.filenumber, ap.fileID, ap.posted, ap.transType, ap.archived, 
				ap.postdate, ap.postby, a.name, at.tranDate, at.type  
			from arpmt ap 
				inner join agent a 
			on  ap.EntityID = a.agentID 
				inner join artranh at 
			on  ap.arpmtID = at.tranID AND at.type = 'P' 
			where  ap.posted     = 1
			AND ap.entity    = 'A' 
			AND ap.entityID  = (CASE WHEN @entityID = 'ALL' THEN ap.entityID  ELSE @entityID END)
			AND ((@searchString <> '' AND (ap.checknum LIKE '%' + @searchString + '%') or  (cast(at.sourceID as varchar) LIKE '%' + @searchString + '%' )))
			AND at.tranDate >= (CASE WHEN @fromDate IS NULL THEN at.tranDate  ELSE @fromDate END) 
			AND at.tranDate <= (CASE WHEN @toDate   IS NULL THEN at.tranDate  ELSE @toDate   END)
			AND at.sourceID = (CASE WHEN @refundID   = '' THEN at.sourceID ELSE @refundID   END) 
			AND at.sourceType = 'RB'
			AND at.void          = (CASE WHEN @includeVoid  = 1 THEN at.void ELSE 0 END)
			And at.tranAmt < 0
			
		UPDATE #UnappliedTran
		SET #UnappliedTran.appliedAmt = (SELECT SUM(atn.tranAmt) FROM artranh atn WHERE atn.type      = 'A'
																				   AND atn.entityID   = #UnappliedTran.entityID
																				   AND atn.artranID  = (select artranID from artranh where artranh.tranID = #UnappliedTran.unappliedtranID 
																											AND artranh.seq = 0 AND artranh.type = #UnappliedTran.type ) 
																			 GROUP BY atn.entityID,atn.sourceID)
																	,
			#UnappliedTran.refundamt = (select sum(atn.tranAmt) from artranh atn where atn.entity     = 'A'
																					AND atn.entityID  = #UnappliedTran.entityID
																					AND atn.tranID    = #UnappliedTran.tranID
																					AND (atn.type      =  #UnappliedTran.type ) 
																					AND atn.seq       > 0
																					AND atn.sourceID = #UnappliedTran.sourceID 
																					AND atn.tranAmt < 0
																			GROUP BY atn.entityID,atn.tranID),
			#UnappliedTran.username = (select sysuser.name from sysuser where sysuser.UID = #UnappliedTran.createdby),
			#UnappliedTran.voidbyusername = (select sysuser.name from sysuser where sysuser.UID = #UnappliedTran.voidby),
			#UnappliedTran.postbyname = (select sysuser.name from sysuser where sysuser.UID = #UnappliedTran.postby),


			#UnappliedTran.fileExist = (select 'YES' where not EXISTS(select 1 from agentfile 
															where agentfile.agentID = #UnappliedTran.entityID AND
																  agentfile.fileID  = #UnappliedTran.fileID))
  
 
		UPDATE #UnappliedTran
		SET #UnappliedTran.remainingAmt = #UnappliedTran.referenceAmt + ISNULL(#UnappliedTran.appliedAmt,0) + ISNULL(#UnappliedTran.refundamt,0),
			#UnappliedTran.totalrefundamt = (select sum(atn.tranAmt) from artranh atn where atn.reference = #UnappliedTran.reference
																				AND atn.sourcetype        = 'RB'
																				AND atn.tranAmt < 0)	  
	  
	  
	end
	  
   UPDATE #UnappliedTran
    SET #UnappliedTran.appliedAmt = (SELECT SUM(atn.tranAmt) FROM artran atn WHERE atn.type     = 'A'
																		AND atn.entityID   = #UnappliedTran.entityID
																		AND atn.arTranID   = (select arTranID from artran where artran.tranID = #UnappliedTran.unappliedtranID 
																		                      AND artran.seq = 0 AND artran.type = #UnappliedTran.type ) 
																	GROUP BY atn.entityID,atn.sourceID)
																	,
	#UnappliedTran.refundamt = (select sum(atn.tranAmt) from artran atn where atn.entity    = 'A'
																			AND atn.entityID  = #UnappliedTran.entityID
																			AND atn.tranID    = #UnappliedTran.tranID
																			AND (atn.type      =  #UnappliedTran.type ) 
																			AND atn.seq       > 0
																			AND atn.sourceID = #UnappliedTran.sourceID 
																	        AND atn.tranAmt < 0
																	GROUP BY atn.entityID,atn.tranID),
            #UnappliedTran.username = (select sysuser.name from sysuser where sysuser.UID = #UnappliedTran.createdby),
		    #UnappliedTran.voidbyusername = (select sysuser.name from sysuser where sysuser.UID = #UnappliedTran.voidby),
			#UnappliedTran.postbyname = (select sysuser.name from sysuser where sysuser.UID = #UnappliedTran.postby),


	        #UnappliedTran.fileExist = (select 'YES' where not EXISTS(select 1 from agentfile 
		                                                   where agentfile.agentID = #UnappliedTran.entityID AND
                                                                  agentfile.fileID  = #UnappliedTran.fileID))
  
  UPDATE #UnappliedTran
     SET #UnappliedTran.remainingAmt = #UnappliedTran.referenceAmt + ISNULL(#UnappliedTran.appliedAmt,0) + ISNULL(#UnappliedTran.refundamt,0),
	     #UnappliedTran.totalrefundamt = (select sum(atn.tranAmt) from artran atn where atn.reference    = #UnappliedTran.reference
																			AND atn.sourcetype      = 'RB'
																			AND atn.tranAmt < 0)
   END

  select * from #UnappliedTran order by entityID


