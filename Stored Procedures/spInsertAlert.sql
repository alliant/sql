USE [COMPASS_DVLP]
GO
/****** Object:  StoredProcedure [dbo].[spInsertAgentFile]    Script Date: 3/20/2020 9:12:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spInsertAlert]
	@source VARCHAR(100) = '',
  @processCode VARCHAR(100) = '',
  @user VARCHAR(100) = '',
  @agentID VARCHAR(20) = '',
  @score DECIMAL(18,2) = 0,
  @effDate DATETIME = NULL,
  @greaterThan BIT = 1,
  @note VARCHAR(MAX) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  DECLARE @alertID INTEGER = 0,
          -- Used for the alert row
          @severity INTEGER,
          @owner VARCHAR(50),
          @threshold DECIMAL(18,2),
          @description VARCHAR(MAX) = 'Created Automatically',
          -- Used to see if the parameters are valid
          @validCode BIT = 0,
          @validUser BIT = 0,
          @validAgent BIT = 0,
          -- Transfer the note?
          @transferNote BIT = 1,
          -- Should we auto-close the alert
          @doAutoClose BIT = 0,
          @autoClose BIT = 0,
          -- The previouse alert ID
          @prevAlertID INTEGER = 0,
          -- The rules don't apply to user-defined alerts
          @isUserDefined BIT = 0

  IF (@note <> '')
    SET @description = @note
  
  IF (@source = '')
  BEGIN
    RAISERROR(N'The source is missing',10,1)
    RETURN 0
  END

  IF (@processCode = '')
  BEGIN
    RAISERROR(N'The process code is missing',10,1)
    RETURN 0
  END
  
  SET @owner = [dbo].[GetAlertOwner] (@processCode)
  IF (@owner = '')
  BEGIN
    RAISERROR(N'The property is not valid',10,1)
    RETURN 0
  END
    
  IF (@user = '')
    SET @user = 'compass@alliantnational.com'
  SELECT @validUser=COUNT(*) FROM [dbo].[sysuser] WHERE [uid] = @user
  IF (@validUser = 0)
  BEGIN
    RAISERROR(N'The user is not valid',10,1)
    RETURN 0
  END

  SELECT @validAgent=COUNT(*) FROM [dbo].[agent] WHERE [agentID] = @agentID
  IF (@validAgent = 0)
  BEGIN
    RAISERROR(N'The agent is not valid',10,1)
    RETURN 0
  END

  SELECT @validCode = COUNT(*) FROM [dbo].[syscode] WHERE [codeType] = 'Alert' AND [code] = @processCode
  IF (@validCode = 0)
  BEGIN
    RAISERROR(N'The code is not valid',10,1)
    RETURN 0
  END

  -- Get severity and threshold
  SET @severity = [dbo].[GetAlertSeverity] (@processCode, @score)
  SET @threshold = [dbo].[GetAlertThreshold] (@processCode, @score)

  IF (@effDate IS NULL)
    SET @effDate = GETDATE()

  -- Auto-close the alert for severity 0
  IF (@severity = 0)
    SET @autoClose = 1

  SELECT @isUserDefined = CASE WHEN [objValue] = 'User Defined' THEN 1 ELSE 0 END FROM [dbo].[sysprop] WHERE [appCode] = 'AMD' AND [objAction] = 'Alert' AND [objProperty] = 'Threshold' AND [objID] = @processCode

  -- If the alert is not user defined
  IF (@isUserDefined = 0)
  BEGIN
    -- Get the active alert if one
    SELECT  @prevAlertID = [alertID]
    FROM    [dbo].[alert]
    WHERE   [agentID] = @agentID
    AND     [source] = @source
    AND     [processCode] = @processCode
    AND     [active] = 1

    -- If there is a previous alert that isn't set to close automatically
    IF (@prevAlertID > 0 AND @autoClose = 0)
    BEGIN
      -- Decide if the alert shoud auto close (the second comma-delimited list item in sysprop.objDesc)
      SET @doAutoClose = [dbo].[GetAlertAutoClose] (@processCode)
      IF (@doAutoClose = 1)
      BEGIN
        SELECT  @autoClose = 1
        FROM    [dbo].[alert]
        WHERE   [alertID] = @prevAlertID
        AND     [severity] = @severity
        AND     [stat] = 'C'
      END
      ELSE
        -- If not auto close, only for severity 1
        IF (@severity = 1)
        BEGIN
          -- Only close automitcally if the score is better than last time and closed
          SET @greaterThan = [dbo].[GetAlertGreaterOrLessThan] (@processCode)
          IF (@greaterThan = 1)
            SELECT  @autoClose = 1
            FROM    [dbo].[alert]
            WHERE   [alertID] = @prevAlertID
            AND     [severity] = @severity
            AND     [stat] = 'C'
            AND     [score] >= @score
          ELSE
            SELECT  @autoClose = 1
            FROM    [dbo].[alert]
            WHERE   [alertID] = @prevAlertID
            AND     [severity] = @severity
            AND     [stat] = 'C'
            AND     [score] <= @score
        END
    END
  END

  -- Set all active alerts of the type/agent to inactive
  UPDATE  [dbo].[alert]
  SET     [active] = 0
  WHERE   [agentID] = @agentID
  AND     [source] = @source
  AND     [processCode] = @processCode
  AND     [active] = 1

  --We are good so get the next key
  EXEC [dbo].[spGetNextSequence] @keyType = 'alert', @key = @alertID OUTPUT

  -- Insert
  INSERT INTO [dbo].[alert] ([alertID],[agentID],[source],[processCode],[description],[threshold],[score],[severity],[createdBy],[dateCreated],[effDate],[stat],[owner],[active])
  SELECT  @alertID,
          @agentID,
          @source,
          @processCode,
          @description,
          @threshold,
          @score,
          @severity,
          @user,
          GETDATE(),
          @effDate,
          'O',
          @owner,
          1

  -- Auto-close an alert
  IF (@autoClose = 1)
    UPDATE  [dbo].[alert]
    SET     [dateClosed] = GETDATE(),
            [closedBy] = @user,
            [stat] = 'C'
    WHERE   [agentID] = @agentID
    AND     [source] = @source
    AND     [processCode] = @processCode
    AND     [active] = 1

  IF (@prevAlertID > 0)
  BEGIN
    SELECT  @transferNote = 0
    FROM    [dbo].[alert]
    WHERE   [alertID] = @prevAlertID
    AND     [severity] = 0

    IF (@transferNote = 1)
      UPDATE  [dbo].[alertnote]
      SET     [alertID] = @alertID
      WHERE   [alertID] = @prevAlertID
  END

  RETURN 0
END
GO
