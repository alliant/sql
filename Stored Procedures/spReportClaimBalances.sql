USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spReportClaimBalances]    Script Date: 1/4/2017 1:22:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportClaimBalances]
	-- Add the parameters for the stored procedure here
	@claimID INTEGER = 0,
  @asOfDate DATETIME = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  IF (@asOfDate IS NULL)
  BEGIN
    SET @asOfDate = GETDATE()
  END
   
  -- Insert statements for procedure here
  CREATE TABLE #claimTable
  (
    [claimID] INTEGER
  )

  CREATE TABLE #invoiceDetailsTable 
  (
    [claimID] INTEGER,
    [refCategory] VARCHAR(100),
    [pendingAmount] DECIMAL(18,2),
    [approvedAmount] DECIMAL(18,2),
    [completedAmount] DECIMAL(18,2)
  )

  CREATE TABLE #reserveDetailsTable
  (
    [claimID] INTEGER,
    [refCategory] VARCHAR(100),
    [pendingAmount] DECIMAL(18,2),
    [approvedAmount] DECIMAL(18,2)
  )
  
  DECLARE @insertClaimBalance [dbo].[ClaimBalance]
  SELECT * INTO #combinedDetailsTable FROM @insertClaimBalance

  IF (@claimID <> 0)
  BEGIN
    INSERT INTO #claimTable ([claimID]) VALUES (@claimID)
  END
  ELSE
  BEGIN
    INSERT INTO #claimTable ([claimID])
    SELECT  [claimID]
    FROM    [claim]
    GROUP BY [claimID]
  END

  -- invoice details (LAE)
  INSERT INTO #invoiceDetailsTable ([claimID],[refCategory],[pendingAmount],[approvedAmount],[completedAmount])
  SELECT  clm.[claimID],
          'E',
          SUM(ISNULL([pendingAmount],0)) AS pendingAmount,
          SUM(ISNULL([approvedAmount],0)) AS approvedAmount,
          SUM(ISNULL([completedAmount],0)) AS completedAmount
  FROM    #claimTable clm LEFT OUTER JOIN
          GetInvoiceDetails(@asOfDate) inv
       ON clm.[claimID] = CONVERT(INTEGER,inv.[refID])
      AND inv.[refCategory] = 'E'
  GROUP BY clm.[claimID]
  -- invoice details (Loss)
  INSERT INTO #invoiceDetailsTable ([claimID],[refCategory],[pendingAmount],[approvedAmount],[completedAmount])
  SELECT  clm.[claimID],
          'L',
          SUM(ISNULL([pendingAmount],0)) AS pendingAmount,
          SUM(ISNULL([approvedAmount],0)) AS approvedAmount,
          SUM(ISNULL([completedAmount],0)) AS completedAmount
  FROM    #claimTable clm LEFT OUTER JOIN
          GetInvoiceDetails(@asOfDate) inv
       ON clm.[claimID] = CONVERT(INTEGER,inv.[refID])
      AND inv.[refCategory] = 'L'
  GROUP BY clm.[claimID]
  
  -- claim reserve details (LAE)
  INSERT INTO #reserveDetailsTable ([claimID],[refCategory],[pendingAmount],[approvedAmount])
  SELECT  clm.[claimID],
          'E',
          SUM(ISNULL([pendingAmount],0)) AS pendingAmount,
          SUM(ISNULL([approvedAmount],0)) AS approvedAmount
  FROM    #claimTable clm LEFT OUTER JOIN
          GetClaimReserveDetails(@asOfDate) res
       ON clm.[claimID] = res.[claimID]
      AND res.[refCategory] = 'E'
  GROUP BY clm.[claimID]
  -- claim reserve details (Loss)
  INSERT INTO #reserveDetailsTable ([claimID],[refCategory],[pendingAmount],[approvedAmount])
  SELECT  clm.[claimID],
          'L',
          SUM(ISNULL([pendingAmount],0)) AS pendingAmount,
          SUM(ISNULL([approvedAmount],0)) AS approvedAmount
  FROM    #claimTable clm LEFT OUTER JOIN
          GetClaimReserveDetails(@asOfDate) res
       ON clm.[claimID] = res.[claimID]
      AND res.[refCategory] = 'L'
  GROUP BY clm.[claimID]

  -- Get the details
  INSERT INTO #combinedDetailsTable ([claimID],[refCategory],[asOfDate],[pendingInvoiceAmount],[approvedInvoiceAmount],[completedInvoiceAmount],[pendingReserveAmount],[approvedReserveAmount],[reserveBalance])
  SELECT  inv.[claimID],
          inv.[refCategory],
          @asOfDate,
          ISNULL(inv.[pendingAmount],0),
          ISNULL(inv.[approvedAmount],0),
          ISNULL(inv.[completedAmount],0),
          ISNULL(res.[pendingAmount],0),
          ISNULL(res.[approvedAmount],0),
          0
  FROM    #invoiceDetailsTable inv INNER JOIN 
          #reserveDetailsTable res
        ON res.[claimID] = inv.[claimID]
      AND res.[refCategory] = inv.[refCategory]
      
  -- Update the reserve balance
  UPDATE  #combinedDetailsTable
  SET     [reserveBalance] = [approvedReserveAmount] - [completedInvoiceAmount]

  IF object_id('tempdb..#insertClaimBalance') IS NULL
  BEGIN
    SELECT  [claimID],
            [refCategory],
            [asOfDate],
            [pendingInvoiceAmount],
            [approvedInvoiceAmount],
            [completedInvoiceAmount],
            [pendingReserveAmount],
            [approvedReserveAmount],
            [reserveBalance]
    FROM    #combinedDetailsTable
    ORDER BY [claimID],
             [refCategory]
  END
  ELSE
  BEGIN
    -- Get the claims
    INSERT INTO #insertClaimBalance ([claimID],[refCategory],[asOfDate],[pendingInvoiceAmount],[approvedInvoiceAmount],[completedInvoiceAmount],[pendingReserveAmount],[approvedReserveAmount],[reserveBalance])
    SELECT  [claimID],
            [refCategory],
            [asOfDate],
            [pendingInvoiceAmount],
            [approvedInvoiceAmount],
            [completedInvoiceAmount],
            [pendingReserveAmount],
            [approvedReserveAmount],
            [reserveBalance]
    FROM    #combinedDetailsTable
    ORDER BY [claimID],[refCategory]
  END
END
