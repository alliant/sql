USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spAgentReviewScore]    Script Date: 4/3/2018 1:14:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spAgentReviewScore]
	-- Add the parameters for the stored procedure here
  @agentID VARCHAR(MAX) = 'ALL',
  @UID varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SET @agentID = [dbo].[StandardizeAgentID](@agentID)
  
  -- Insert statements for procedure here
  SELECT  a.[agentID],
          q.[seq],
          q.[qarScore] AS [qarScore],
          q.[errScore] AS [errScore],
          q.[auditDate] AS [auditDate]
  FROM    [dbo].[agent] a INNER JOIN
          (
          SELECT  q.[agentID],
                  q.[score] AS [qarScore],
                  COALESCE(q.[auditReviewDate],q.[auditStartDate]) AS [auditDate],
                  s.[score] AS [errScore],
                  ROW_NUMBER() OVER(PARTITION BY q.[agentID] ORDER BY COALESCE(q.[auditReviewDate],q.[auditStartDate]) DESC) AS [seq]
          FROM    [dbo].[qar] q LEFT OUTER JOIN
                  [dbo].[qarsection] s
          ON      q.[qarID] = s.[qarID]
          AND     s.[sectionID] = 6
          WHERE   q.[stat] = 'C'
          AND     q.[auditType] IN ('Q','E')
          AND     q.[score] > 0
          AND     s.[score] > 0
          ) q
  ON      a.[agentID] = q.[agentID]
  WHERE  (a.[agentID] = (CASE  WHEN @agentID != 'ALL' THEN @agentID ELSE a.[agentID] END)) 
  AND    (dbo.CanAccessAgent(@UID ,a.[agentID]) = 1) 
  
END
