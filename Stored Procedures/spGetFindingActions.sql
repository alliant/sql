USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetFindingActions]    Script Date: 12/14/2018 2:58:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetFindingActions]
	-- Add the parameters for the stored procedure here
	@stage VARCHAR (30) = '',
    @year INTEGER  = 0,
	@month INTEGER = 0,
	@entity VARCHAR (30)= '',
	@entityID VARCHAR (30)= '',
	@owner VARCHAR (30)= '',
	@status VARCHAR(30) = 'ALL',
    @findingID INTEGER = 0,
    @actionID INTEGER = 0
AS
BEGIN
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  CREATE TABLE #status
  (
    [status] VARCHAR(10)
  )

  INSERT INTO #status
  SELECT [field] FROM [dbo].[GetEntityFilter] ('ActionStatus', @status)
  
  SELECT  --common elements
          CONVERT(VARCHAR,f.[findingID]) + '-' + CONVERT(VARCHAR,a.[actionID]) AS [findingactionID],
          f.[entity],
          f.[entityID],
          f.[refType],
          f.[refID],
          f.[source],
          f.[sourceID],
          COALESCE(f.[sourceQuestion],'') AS [question], -- COALESCE(f.[sourceQuestion],f.[description]) AS [question],
          COALESCE(CASE WHEN f.[comments] = '' THEN NULL ELSE f.[comments] END,'') AS [finding], --COALESCE(CASE WHEN f.[comments] = '' THEN NULL ELSE f.[comments] END,f.[description]) AS [finding],
          COALESCE(a.[comments],'') AS [action],
          COALESCE((SELECT [uid] FROM [qar] WHERE [qarID] = f.[sourceID]),COALESCE(a.[uid],f.[uid])) AS [ownerID],
          -- Finding elements
          f.[findingID],
          f.[severity],
          '',
          f.[comments] AS [findingComments],
          f.[stat] AS [stage],
          f.[uid] AS [findingUID],
          f.[createdDate] AS [findingCreatedDate],
          f.[closedDate] AS [findingClosedDate],
          -- Action elements
          a.[actionID] AS [actionID],
          a.[targetProcess],
          a.[actionType],
          a.[uid] AS [actionUID],
          a.[stat],
          a.[startDate],
          a.[completeStatus],
          a.[method],
          a.[createdDate] AS [actionCreatedDate],
          a.[openedDate],
          a.[completedDate],
          a.[CancellationDate],
          a.[originalDueDate],
          a.[dueDate],
          a.[extendedDate],
          a.[followupDate],
          a.[referenceActionID],
          CASE WHEN d.[entityID] IS NOT NULL 
            THEN 1
            ELSE 0
          END AS [hasDocument],
          '' AS [iconMessages],
          pa.[stat] AS [approvalStat],
          pa.[createdDate] AS [sentDate],
          pa.[completedDate] AS [approvalDate]
  FROM    [dbo].[finding] f INNER JOIN
          [dbo].[action] a
  ON      f.[findingID] = a.[findingID] LEFT OUTER JOIN
          [dbo].[sysdoc] d
  ON      d.[entityID] = f.[entityID]
  AND     d.[entityType] = 'Action' LEFT OUTER JOIN
          [dbo].[periodApproval] pa
  ON      pa.[periodID] = (SELECT [periodID] FROM [dbo].[period] WHERE a.[createdDate] BETWEEN [startDate] AND [endDate])
  WHERE   f.[findingID] = CASE WHEN @findingID = 0 THEN f.[findingID] ELSE @findingID END
  AND     a.[stat] IN (SELECT [status] FROM #status)
  AND     a.[actionID] = CASE WHEN @actionID = 0 THEN a.[actionID] ELSE @actionID END
  AND     f.[stat] = CASE WHEN @stage = '' THEN  f.[stat] ELSE @stage END
  AND     ( [dbo].[checkFindingDate](@year, @month, f.[createdDate], f.[closedDate], f.[stat]) = 1 )
  AND     f.[entity] = CASE WHEN @entity = '' THEN  f.[entity] ELSE @entity END
  AND     f.[entityID] = CASE WHEN @entityID = '' THEN  f.[entityID] ELSE @entityID END
  AND     f.[uid] = CASE WHEN @owner = '' THEN f.[uid] ELSE @owner END
  AND     a.[createdDate] > DATEADD(YEAR,-1,GETDATE())
  UNION ALL
  SELECT  CONVERT(VARCHAR,f.[findingID]) + '-0' AS [findingactionID],
          f.[entity],
          f.[entityID],
          f.[refType],
          f.[refID],
          f.[source],
          f.[sourceID],
          COALESCE(f.[sourceQuestion],'') AS [question], --COALESCE(f.[sourceQuestion],f.[description]) AS [question],
          COALESCE(CASE WHEN f.[comments] = '' THEN NULL ELSE f.[comments] END,'') AS [finding], --COALESCE(CASE WHEN f.[comments] = '' THEN NULL ELSE f.[comments] END,f.[description]) AS [finding],
          '' AS [action],
          COALESCE((SELECT [uid] FROM [qar] WHERE [qarID] = f.[sourceID]),f.[uid]) AS [ownerID],
          -- Finding elements
          f.[findingID],
          f.[severity],
          '', 
          f.[comments] AS [findingComments],
          f.[stat] AS [stage],
          f.[uid] AS [findingUID],
          f.[createdDate] AS [findingCreatedDate],
          f.[closedDate] AS [findingClosedDate],
          -- Action elements
          0 AS [actionID],
          '' AS [targetProcess],
          '' AS [actionType],
          '' AS [actionUID],
          'P' AS [stat],
          NULL AS [startDate],
          NULL AS [completeStatus],
          '' AS [method],
          NULL AS [actionCreatedDate],
          NULL AS [openedDate],
          NULL AS [completedDate],
          NULL AS [CancellationDate],
          NULL AS [originalDueDate],
          NULL AS [dueDate],
          NULL AS [extendedDate],
          NULL AS [followupDate],
          '' AS [referenceActionID],
          '0',
          'No action assigned' AS [iconMessages],
          '',
          NULL,
          NULL
  FROM    [dbo].[finding] f
  WHERE   [findingID] NOT IN (SELECT [findingID] FROM [action])
  AND     f.[findingID] = CASE WHEN @findingID = 0 THEN f.[findingID] ELSE @findingID END
  AND     f.[stat] = CASE WHEN @stage = '' THEN  f.[stat] ELSE @stage END
  AND     ([dbo].[checkFindingDate](@year, @month, f.[createdDate], f.[closedDate], f.[stat]) = 1)
  AND     f.[entity] = CASE WHEN @entity = '' THEN  f.[entity] ELSE @entity END
  AND     f.[entityID] = CASE WHEN @entityID = '' THEN  f.[entityID] ELSE @entityID END
  AND     f.[uid] = CASE WHEN @owner = '' THEN f.[uid] ELSE @owner END
  AND    (@status = 'O' OR @status = 'P' OR @status = 'ALL')
END
