GO
/****** Object:  StoredProcedure [dbo].[spDeleteAFWithNoChild]    Script Date: 7/5/2018 4:24:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spDeleteAFWithNoChild]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
  SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;
  
  DELETE FROM agentfile WHERE 
	NOT EXISTS(SELECT policyID from policy WHERE policy.agentID = agentfile.agentID AND policy.fileID = agentfile.fileID)  AND 
	NOT EXISTS(SELECT cplID from CPL WHERE CPL.agentID = agentfile.agentID AND CPL.fileID = agentfile.fileID)              AND 
	NOT EXISTS(SELECT artranID from artran WHERE artran.entityID = agentfile.agentID AND artran.fileID = agentfile.fileID) AND
	NOT EXISTS(SELECT b.batchid from batch b inner join 
                     batchform bf on b.batchid = bf.batchid 
					  WHERE b.agentID = agentfile.agentID and
							/* b.stat = 'C' and */
						    bf.fileID = agentfile.fileID)
							
  /* Delete sysnotes whose Agent File is not there */
  DELETE FROM sysnote WHERE NOT EXISTS(SELECT agentfileID from agentfile WHERE agentfile.agentfileID = sysnote.entityID) and sysnote.entity = 'AF'
  
END



   