USE [compass]
GO
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetARTranh]
	-- Add the parameters for the stored procedure here
	@stateID         VARCHAR(MAX) = 'ALL',
	@agentID         VARCHAR(MAX) = 'ALL',
	@filenumber      VARCHAR(MAX) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	CREATE TABLE #agentTable ([agentID] VARCHAR(30))
    INSERT INTO #agentTable ([agentID])
    SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)

    CREATE TABLE #stateTable ([stateID] VARCHAR(2))
    INSERT INTO #stateTable ([stateID])
    SELECT [field] FROM [dbo].[GetEntityFilter] ('State', @stateID)

    -- Insert statements for procedure here
	SELECT artranh.artranhID,
	       artranh.revenueType,
		   artranh.tranID,
		   artranh.seq,
		   artranh.sourceType,
		   artranh.type,
		   artranh.entityID,
		   artranh.sourceID,
		   artranh.fileNumber,
		   artranh.fileID,
		   artranh.tranAmt,
		   artranh.remainingAmt,
		   artranh.tranDate,
		   artranh.void,
		   artranh.voidDate,
		   artranh.voidBy,
		   artranh.notes,
		   artranh.createdDate,
		   artranh.createdBy,
		   artranh.dueDate,
		   artranh.fullyPaid,
		   artranh.reference,
		   artranh.entity,
		   artranh.appliedAmt,
		   artranh.transType,
		   artranh.postDate,
		   artranh.postBy,
		   a.name as agentname
		    from artranh 
           INNER JOIN agent a
		   ON a.agentID = artranh.entityID
	    WHERE artranh.entity     = 'A'
		  AND artranh.entityID   IN (SELECT [agentID] FROM #agentTable)
		  AND a.stateID          IN (SELECT [stateID] FROM #stateTable)
		  AND artranh.fileNumber like @filenumber + '%' 
END
GO
