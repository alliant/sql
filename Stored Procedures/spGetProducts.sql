USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetProducts]    Script Date: 8/21/2019 11:01:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetProducts]
	-- Add the parameters for the stored procedure here
	@productID VARCHAR(50) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  SELECT  [productID],
          [stateID],
          [productType],
          [description],
          [notes],
          [effDate],
          [expDate],
          [active],
          [displayName],
          [extension]
  FROM    [dbo].[product]
  WHERE   [productID] = CASE WHEN @productID = '' THEN [productID] ELSE @productID END
END
