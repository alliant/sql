USE [COMPASS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spAgentFileDataFix]
AS
BEGIN
  SET NOCOUNT ON;

  /* Merge information from duplicate agent files records into one and delete other records */
  EXEC [dbo].[spDeleteDuplicateAF]
  
  /* Populate the four columns newly added for all agentfile records */
  declare @dtfirstPolicyIssueDt datetime ,
		  @dtfirstCPLIssueDt    datetime 

  update agentfile set @dtfirstPolicyIssueDt = agentfile.firstPolicyIssueDt = 
  (select min(p.issueDate) from policy p where p.agentID = agentfile.agentID and 
                                               p.fileID = agentfile.fileID  and
											   p.issueDate is not null 
	group by p.agentID,p.fileID),
  
  @dtfirstCPLIssueDt = agentfile.firstCPLIssueDt = 
  (select min(c.issueDate) from CPL c where c.agentID = agentfile.agentID and 
											c.fileID = agentfile.fileID and
											c.issueDate is not null 
	group by c.agentID,c.fileID),
 
  agentfile.FileType = dbo.GetAgentFileType(agentfile.agentID,agentfile.fileID),
 
  agentfile.origin = dbo.GetAgentFileOrigin(agentfile.agentID,
										    agentfile.fileID,
										    @dtfirstPolicyIssueDt,
										    @dtfirstCPLIssueDt)

END
