/****** Object:  StoredProcedure [dbo].[spGetGLProductionInvoice]    Script Date: 8/2/2024 10:21:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetGLProductionInvoice] 
	-- Add the parameters for the stored procedure here
	@agentID     VARCHAR(MAX)  = 'ALL',
    @startDate   DATETIME      = NULL ,
	@endDate     DATETIME      = NULL 
AS
BEGIN
    SET NOCOUNT ON;

    -- Insert statements for procedure here
    CREATE TABLE #glTransaction (agentID         VARCHAR(80),
	                             artranid		 INTEGER,
								 type            VARCHAR(1), 
                                 Reference       VARCHAR(50),
                                 tranID          VARCHAR(50),
                                 postingDate     DATETIME,
                                 argldef         VARCHAR(500),
				                 arglref         VARCHAR(500),
		                         debit           [decimal](18, 2) NULL,
     				             credit          [decimal](18, 2) NULL,
								 voided          VARCHAR(20) default '',
								 notes		     VARCHAR(MAX)
     	 						)
  
    /*-------Debit And Credit-----------*/			
    INSERT INTO #glTransaction ( agentid,artranID,type,reference, tranID,postingdate, debit, credit, arglref, notes)       
    SELECT ar.entityid,ar.arTranID,ar.type,ar.reference, ar.tranid,ar.trandate, l.debitAmount, l.creditAmount , l.accountID, l.notes
    FROM artran ar 
	INNER JOIN ledger l
    ON ar.ledgerID = l.ledgerID and ar.arTranID = l.sourceID 
    	    
    WHERE ar.tranDate >= @startDate 
	  AND ar.tranDate <= @endDate 
	  AND ar.entityid = (CASE WHEN @agentID  = 'ALL' THEN ar.entityid  ELSE @agentID  END) 
	  AND (ar.type = 'I' AND ar.transtype = 'P') and ar.void = 0

   /*-------Void Debit And  Void Invoice-----------*/			
    INSERT INTO #glTransaction ( agentid,artranid,type,reference, tranID,postingdate, debit, credit, arglref, voided, notes)    
    
    SELECT  ar.entityid,ar.arTranID,ar.type,ar.reference, ar.tranid,ar.voidDate, l.debitAmount, l.creditAmount , l.accountID , 'Yes',
			'Void Invoice ' + CONVERT(VARCHAR, ar.tranDate, 101) 
    FROM artran ar
	 		INNER JOIN ledger l
    ON ar.voidledgerID = l.ledgerID
    WHERE ar.voidDate >= @startDate 
	  AND ar.voidDate <= @endDate 
	  AND ar.entityid = (CASE WHEN @agentID  = 'ALL' THEN ar.entityid  ELSE @agentID  END) 
      AND (ar.type = 'I' AND ar.transtype = 'P') AND ar.void = 1
  
   UPDATE #glTransaction SET credit = ISNULL(credit,0),
               		         debit  = ISNULL(debit,0)

   DELETE FROM #glTransaction  WHERE credit = 0 and debit = 0
   SELECT * FROM #glTransaction ORDER BY tranID
   DROP TABLE #glTransaction

END