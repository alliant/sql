-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author:		<Rahul Sharma>
-- Create date: <21-11-2019>
-- Description:	<Get agentfile records>
-- =============================================
ALTER PROCEDURE [dbo].[spGetAgentFiles] 
	-- Add the parameters for the stored procedure here
    @pcAgentID     VARCHAR(50) = 'ALL',
    @pcFileID      VARCHAR(100) = 'ALL',
	@plClosedFiles BIT,
	@plGetMatchingFileIDs BIT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    
	-- Insert statements for procedure here

	-- Temporary table definition
    CREATE TABLE #agentfile (agentfileID        INTEGER,			            
                             agentID            VARCHAR(50),
		    		     	 fileID             VARCHAR(50),
						     fileNumber         VARCHAR(50),
						     stat               VARCHAR(20),
						     stage              VARCHAR(50),
						     addr1              VARCHAR(1000),
						     addr2              VARCHAR(1000),
						     addr3              VARCHAR(1000),
						     addr4              VARCHAR(1000),
						     city               VARCHAR(50),
						     countyID           VARCHAR(50),
						     state              VARCHAR(20),
						     zip                VARCHAR(20),
						     notes              VARCHAR(max),
						     closingDate        DATETIME,
						     reportingDate      DATETIME,
						     transactionType    VARCHAR(20),
						     insuredType        VARCHAR(20),
						     liabilityAmt       DECIMAL(18,2),
						     invoiceAmt         DECIMAL(18,2),
						     paidAmt            DECIMAL(18,2),
						     osAmt              DECIMAL(18,2),
						     arNotes            VARCHAR(max),
						     agentname          VARCHAR(100),
							 origin             VARCHAR(30),
							 firstCPLIssueDt    DATETIME,
							 firstPolicyIssueDt DATETIME,
							 fileType           VARCHAR(30))

    
	INSERT INTO #agentfile(agentfileID,
                           agentID,
				     	   fileID,
						   fileNumber,
						   stat,
						   stage,
						   addr1,
						   addr2,
                           addr3,						 
						   addr4,
						   city,
						   countyID,
						   state,
						   zip,
						   notes,
						   closingDate,
						   reportingDate,
						   transactionType,
						   insuredType,
						   liabilityAmt,
						   invoiceAmt,
						   paidAmt,
						   osAmt,
						   arNotes,
						   origin,
						   firstCPLIssueDt,
						   firstPolicyIssueDt,
						   fileType)  
						 	     	
      SELECT agentfile.agentfileID,
	         agentfile.agentID,
		     agentfile.fileID,
		     agentfile.fileNumber,
		     agentfile.stat,
		     agentfile.stage,
		     agentfile.addr1,
		     agentfile.addr2,
             agentfile.addr3,
		     agentfile.addr4,
		     agentfile.city,
		     agentfile.countyID,
		     agentfile.state,
		     agentfile.zip,
		     agentfile.notes,
		     agentfile.closingDate,
		     agentfile.reportingDate,
		     agentfile.transactionType,
		     agentfile.insuredType,
		     agentfile.liabilityAmt,
		     agentfile.invoiceAmt,
		     agentfile.paidAmt, 
	         agentfile.osAmt,
		     agentfile.arNotes,
             agentfile.origin,
             agentfile.firstCPLIssueDt,
             agentfile.firstPolicyIssueDt,
             agentfile.fileType			 
        FROM agentfile
        WHERE agentfile.agentID    = (CASE WHEN @pcAgentID      = 'ALL' THEN agentfile.agentID     ELSE @pcAgentID    END)            
          AND agentfile.stat       = (CASE WHEN @plClosedFiles  = 1     THEN agentfile.stat        ELSE 'O'           END)
		  AND ((@pcFileID  = 'ALL' AND agentfile.fileID = agentfile.fileID) 
           OR  (@pcFileID <> 'ALL' AND @plGetMatchingFileIDs = 1 AND agentfile.fileID LIKE @pcFileID + '%')
		   OR  (@pcFileID <> 'ALL' AND @plGetMatchingFileIDs = 0 AND agentfile.fileID = @pcFileID ))
     
    UPDATE #agentfile
      SET #agentfile.agentname = agent.name
      FROM #agentfile
    INNER JOIN agent ON (#agentfile.agentID = agent.agentID)
 	
    SELECT * FROM #agentfile
END
GO
