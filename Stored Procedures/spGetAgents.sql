
/****** Object:  StoredProcedure [dbo].[spGetAgents]    Script Date: 13-08-2020 17:09:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- Modification: Moved logic of agentfilter.p into this SP
-- =============================================
ALTER PROCEDURE [dbo].[spGetAgents]
	-- Add the parameters for the stored procedure here
	@agentID VARCHAR(MAX) = '',
	@UID VARCHAR(100) = ''
	  
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;   
  SELECT  a.[agentID] ,
          a.[corporationID],
          a.[stateID],
          /*(SELECT [description] FROM [dbo].[state] WHERE [stateID] = a.[stateID]) AS [stateName],*/
          a.[stat],
		  (SELECT sysprop.objValue FROM sysprop where sysprop.appCode = 'AMD' AND sysprop.objAction = 'Agent' AND sysprop.objID = a.stat) AS [statusDesc],
          a.[name],
          a.[legalName],
          a.[addr1],
          a.[addr2],
          a.[addr3],
          a.[addr4],
          a.[city],
         /* a.[county],*/
          a.[state],
		  a.[region],
          a.[zip],
          a.[contact],
          a.[phone],
          a.[fax],
          a.[email],
		  a.[domain],
          a.[website],
          /*a.[stateLicense],
          a.[stateLicenseEff],
          a.[stateLicenseExp],*/
          a.[eoRequired],
         /* a.[eoCompany],
          a.[eoPolicy],
          a.[eoCoverage],
          a.[eoAggregate],
          a.[eoStartDate],
          a.[eoEndDate],*/
          a.[software],
          a.[isAiga],
          a.[isAffiliated],
          /*a.[createDate],*/
          /*a.[agentDate],*/
          a.[cancelDate],
          a.[closedDate],
          /*a.[orgType],*/
          a.[policyLimit],
          a.[liabilityLimit],
          /*a.[lastRemitDate],
          a.[lastRemitAmt],
          a.[lastAuditDate],
          a.[lastAuditScore],
          a.[nextAuditDue],
          a.[lastCertDate],
          a.[nextCertDue],
          a.[comments],*/
          a.[contractID],
          a.[contractDate],
          /*a.[maxCoverage],*/
          a.[remitType],
          a.[remitValue],
          a.[remitAlert],
          a.[prospectDate],
          a.[activeDate],
          a.[reviewDate],
          a.[swVendor],
          a.[swVersion],
         /* a.[batchCount],
          a.[batchTotal],*/
          am.[uid]   AS [manager],
		  s.[name]   AS [managerDesc],
		  ac.[name]  AS [contactName],
		  ac.[phone] AS [contactPhone],
		  ac.[email] AS [contactEmail],
          a.[altaUID],
          a.[mname],
          a.[maddr1],
          a.[maddr2],
          a.[maddr3],
          a.[maddr4],
          a.[mcity],
          a.[mstate],
          a.[mzip],
          a.[submissionDate],
          a.[appCompleteDate],
          a.[stateUID],
          a.[NPN],
          a.[withdrawnDate],
          a.[invoiceDue],
          a.[dueInDays],
          a.[dueOnDays],
		      a.[ARCashGLRef],
		      a.[ARCashGLDesc],
		      a.[ARGLRef],
		      a.[ARGLDesc],
			  a.[ARWriteoffGLRef],
			  a.[ARWriteoffGLDesc],
			  a.[ARRefundGLRef],
		      a.[ARRefundGLDesc],
          orgrole.[orgID],
		      orgrole.[orgRoleID],
		      org.[Name] AS orgName,
	  CASE WHEN fav.[sysFavoriteID] IS NOT NULL 
            THEN 'True'
            ELSE 'False'
          END AS [isFavorite],  
          CASE WHEN d.[entityID] IS NOT NULL 
            THEN 'True'
            ELSE 'False'
          END AS [hasDocument]
  FROM    [dbo].[agent] a LEFT OUTER JOIN
          [dbo].[agentmanager] am
  ON      a.[agentID] = am.[agentID]
  AND     am.[isPrimary] = 1
  AND     am.[stat] = 'A'      LEFT OUTER JOIN
          (
          SELECT  [name],
                  [phone],
                  [email],
				  [agentID],
				  [category]
          FROM    [dbo].[agentcontact]
          )  ac
  ON      ac.[agentID] = a.[agentID]
  AND     ac.[category] = 'AR' LEFT OUTER JOIN
          (
          SELECT  [name],
			      [UID]
				  FROM    [dbo].[sysuser]
          ) s
  ON      s.[UID] = am.[UID]   LEFT OUTER JOIN
          [sysfavorite] fav
  ON      a.[agentID] = fav.[entityID]
  AND     fav.[entity] = 'A'  
  AND     fav.[uid] = @UID   LEFT OUTER JOIN
          [sysdoc] d
  ON      a.[agentID] = d.[entityID]
  AND     d.[entityType] = 'Agent' LEFT OUTER JOIN
          (
          SELECT  [orgRoleID],
                  [orgID],
                  [source],
                  [sourceID],
                  ROW_NUMBER() OVER(PARTITION BY [sourceID] ORDER BY [sourceID] DESC) AS [rowID]
          FROM    [dbo].[orgrole]
          ) orgrole
  ON      a.[agentID] = orgrole.[sourceID]
  AND     orgrole.[source] = 'Agent'
  AND     orgrole.[rowID] = 1 LEFT OUTER JOIN
          [Organization] org
  ON      org.orgID = orgrole.orgID		  
  WHERE (dbo.CanAccessAgent(@UID , a.[agentID]) = 1)
	AND  a.[agentID] = CASE WHEN @agentID != '' THEN 
					          @agentID 
					        ELSE
					          a.[agentID]
                       END
	
  ORDER BY a.[agentID]
END
