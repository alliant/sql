USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spReportReinsurance]    Script Date: 5/10/2018 3:18:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportReinsurance] 
	-- Add the parameters for the stored procedure here
	@periodID INTEGER = 0,
  @reinsure DECIMAL = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  IF @periodID = 0
    SELECT @periodID = MAX([periodID]) FROM [dbo].[period] WHERE [active] = 0

  SELECT  *
  FROM    (
          SELECT  b.[agentID],
                  bf.[fileNumber],
                  bf.[policyID],
                  SUM(bf.[liabilityDelta]) AS [fullLiability],
                  MAX(bf.[liabilityDelta]) AS [liability]
          FROM    [dbo].[batch] b INNER JOIN
                  [dbo].[batchform] bf
          ON      bf.[batchID] = b.[batchID]
          AND     bf.[formType] = 'P'
          WHERE   b.[periodID] = @periodID
          GROUP BY b.[agentID],bf.[fileNumber],bf.[policyID]
          ) g INNER JOIN
          (
          SELECT  a.[agentID],
                  a.[stateID],
                  a.[name],
                  bf.[policyID],
                  bf.[fileNumber],
                  bf.[formID],
                  sf.[formCode],
                  bf.[effDate]
          FROM    [dbo].[batch] b INNER JOIN
                  [dbo].[batchform] bf
          ON      bf.[batchID] = b.[batchID]
          AND     bf.[formType] = 'P' INNER JOIN
                  [dbo].[stateform] sf
          ON      sf.[stateID] = b.[stateID]
          AND     sf.[formID] = bf.[formID] INNER JOIN
                  [dbo].[agent] a
          ON      b.[agentID] = a.[agentID]
          WHERE   b.[periodID] = @periodID
          ) a
  ON      g.[agentID] = a.[agentID]
  AND     g.[fileNumber] = a.[fileNumber]
  AND     g.[policyID] = a.[policyID]
END
