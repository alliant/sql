USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetOrderPerson]    Script Date: 8/21/2019 10:38:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetOrderPerson]
	-- Add the parameters for the stored procedure here
	@orderID VARCHAR(30) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  SELECT  [orderID],
          [seq],
          [orderSeq],
          [personType],
          [name],
          [joinBy],
          [clause],
          'false' AS [isNew],
          'false' AS [isModify],
          'false' AS [isDelete]
  FROM    [dbo].[orderperson]
  WHERE   [orderID] = CASE WHEN @orderID = '' THEN [orderID] ELSE @orderID END
END
