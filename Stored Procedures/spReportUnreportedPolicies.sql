GO
/****** Object:  StoredProcedure [dbo].[spReportUnremittedPolicies]    Script Date: 5/15/2023 8:41:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:    <Author,,Name>
-- Create date: <Create Date,,>
-- Description: <Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportUnreportedPolicies]
  -- Add the parameters for the stored procedure here
  @stateID VARCHAR(20) = '',
  @agentID VARCHAR(20) = ''
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  WITH CTE AS 
  (
  SELECT  p.[policyID],
          ag.[agentID], 
          ag.[name],
          ag.[stat],
          (SELECT [objValue] FROM [sysprop] WHERE [appCode] = 'AMD' AND [objAction] = 'Agent' AND [objProperty] = 'Status' AND [objID] = ag.[stat]) AS [statDesc],
          sc.[code] AS [regionID],
          sc.[description] AS [regionDesc], 
          p.[companyID],
          o.[name] AS [office],
          su.[uid] AS [managerID],
          su.[name] AS [managerName],  
          p.[stateID],
          (SELECT [description] FROM [dbo].[state] WHERE [stateID] = p.[stateID]) AS [stateDesc],
          p.[issueDate],
          DATEDIFF(day,p.[issuedate],GETDATE()) AS [totaldays], 
          CASE
            WHEN (DATEDIFF(day,p.[issuedate],GETDATE()) <= 30)                                                  THEN '<=30 Days'
            WHEN (DATEDIFF(day,p.[issuedate],GETDATE()) > 30  AND DATEDIFF(day,p.[issuedate],GETDATE()) <= 60)  THEN '31-60 Days'
            WHEN (DATEDIFF(day,p.[issuedate],GETDATE()) > 60  AND DATEDIFF(day,p.[issuedate],GETDATE()) <= 90)  THEN '61-90 Days'
            WHEN (DATEDIFF(day,p.[issuedate],GETDATE()) > 90  AND DATEDIFF(day,p.[issuedate],GETDATE()) <= 180) THEN '91-180 Days'
            WHEN (DATEDIFF(day,p.[issuedate],GETDATE()) > 180 AND DATEDIFF(day,p.[issuedate],GETDATE()) <= 270) THEN '181-270 Days'
            WHEN (DATEDIFF(day,p.[issuedate],GETDATE()) > 270 AND DATEDIFF(day,p.[issuedate],GETDATE()) <= 365) THEN '271-365 Days'
            WHEN (DATEDIFF(day,p.[issuedate],GETDATE()) > 365 AND DATEDIFF(day,p.[issuedate],GETDATE()) <= 730) THEN '366-730 Days'
            WHEN (DATEDIFF(day,p.[issuedate],GETDATE()) > 730)                                                  THEN '>730 Days' 
          END as [agingrange]
  FROM    [dbo].[policy] p INNER JOIN
          [dbo].[office] o 
  ON      o.[officeID] = p.[companyID] INNER JOIN
          [dbo].[agent] ag 
  ON      ag.[agentID] = o.[agentID] LEFT OUTER JOIN 
          [dbo].[agentmanager] am  
  ON      ag.[agentID] = am.[agentID] 
  AND     am.[isPrimary] = 1
  AND     (am.[effDate] <= GETDATE() AND (am.[expDate] > GETDATE() OR am.[expDate] IS NULL )) LEFT OUTER JOIN 
          [dbo].sysuser su 
  ON      su.[uid] = am.[uid] LEFT OUTER JOIN
          [dbo].[syscode] sc 
  ON      sc.[codeType] = 'region'
  AND     sc.[code] = ag.[region]
  WHERE   NOT EXISTS
          (
          SELECT  bf.[policyID]
          FROM    [dbo].[batch] b INNER JOIN 
                  [dbo].[batchform] bf 
          ON      bf.[batchID] = b.[batchID] 
          AND     bf.[policyID] = p.[policyID] INNER JOIN 
                  [dbo].[stateform] sf 
          ON      sf.[stateID] = b.[stateID] 
          AND     sf.[formID] = bf.[formID]
          )
  AND     p.[issueDate] >= '01-01-2018' 
  AND     p.[stat] = 'I'
  AND     p.[stateID] = CASE WHEN @stateID = '' THEN p.[stateID] ELSE @stateID END
  AND     ag.[agentID] = CASE WHEN @agentID = '' THEN ag.[agentID] ELSE @agentID END
  GROUP BY p.[policyID],p.[companyID],ag.[agentID],ag.[name],sc.[code],sc.[description],ag.[stat],o.[name],su.[uid],su.[name],p.[stateID],p.[issueDate],ag.[stat]
  )

  SELECT  ISNULL([companyID],'') AS [officeID],
          ISNULL([office],'') AS [officeName] ,
          [agentID],
          [name] AS [agentName],
          [stat] AS [agentStat],
          [statDesc] AS [agentStatDesc],
          [regionID],
          [regionDesc],
          [managerID],
          [managerName],
          [stateID],
          [stateDesc],
          COUNT(CASE WHEN [agingrange] = '<=30 Days' then 1 END)    AS [bucketLT30],
          COUNT(CASE WHEN [agingrange] = '31-60 Days' then 1 END)   AS [bucket31to60],
          COUNT(CASE WHEN [agingrange] = '61-90 Days' then 1 END)   AS [bucket61to90],
          COUNT(CASE WHEN [agingrange] = '91-180 Days' then 1 END)  AS [bucket91to180],
          COUNT(CASE WHEN [agingrange] = '181-270 Days' then 1 END) AS [bucket181to270],
          COUNT(CASE WHEN [agingrange] = '271-365 Days' then 1 END) AS [bucket271to365],
          COUNT(CASE WHEN [agingrange] = '366-730 Days' then 1 END) AS [bucket366to730],
          COUNT(CASE WHEN [agingrange] = '>730 Days' then 1 END)    AS [bucketGT731],
          COUNT([agingrange]) AS [total]
  FROM    [CTE]
  WHERE   [companyID] IS NOT NULL
  AND     [companyID] <> ''
  GROUP BY [office],[agentID],[name],[stat],[statDesc],[regionID],[regionDesc],[companyID],[managerID],[managerName],[stateID],[stateDesc]
END
