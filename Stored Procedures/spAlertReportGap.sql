GO
/****** Object:  StoredProcedure [dbo].[spAlertNetPremium]    Script Date: 7/5/2017 2:23:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spAlertReportGap]
  @agentID VARCHAR(MAX) = '',
  @periodID INTEGER = 0,
  @user VARCHAR(100) = 'compass@alliantnational.com',
  @preview BIT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  IF (@agentID = '')
    SET @agentID = 'ALL'
  
  CREATE TABLE #agentTable ([agentID] VARCHAR(30))
  INSERT INTO #agentTable ([agentID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)

  DECLARE @effDate DATETIME = NULL,
          -- Used for the preview
          @source VARCHAR(30) = 'OPS',
          @processCode VARCHAR(30) = 'OPS03',
          @score DECIMAL(18,2),
          -- Notes for the alert note
          @note VARCHAR(MAX) = ''

  CREATE TABLE #gapTable 
  (
    [agentID] VARCHAR(20),
    [periodID] INTEGER,
    [dayCount] INTEGER,
    [batchCount] INTEGER,
    [reportGap] INTEGER
  )

  INSERT INTO #gapTable ([agentID], [periodID], [dayCount], [batchCount], [reportGap])
  SELECT  b.[agentID],
          b.[periodID],
          SUM(b.[dayCount]) AS 'dayCount',
          COUNT(b.[batchID]) AS 'batchCount',
          SUM(b.[dayCount]) / CONVERT(DECIMAL,COUNT(b.[batchID])) AS 'monthData'
  FROM    (
          SELECT  DISTINCT
                  b.[batchID],
                  b.[agentID],
                  bf.[policyID],
                  b.[periodID],
                  DATEDIFF(day,bf.[effDate],b.[receivedDate]) AS 'dayCount'
          FROM    [dbo].[batch] b INNER JOIN
                  [dbo].[batchform] bf
          ON      b.[batchID] = bf.[batchID]
          WHERE   b.[periodID] = CASE WHEN @periodID = 0 THEN (SELECT MAX([periodID]) FROM [dbo].[period] WHERE [active] = 0) ELSE @periodID END
          ) b INNER JOIN
          [dbo].[agent] a
  ON      b.[agentID] = a.[agentID]
  WHERE   a.[agentID] IN (SELECT [agentID] FROM #agentTable)
  GROUP BY b.[agentID], b.[periodID]

  IF (@preview = 0)
  BEGIN
    -- Report Gap Alert
    DECLARE cur CURSOR LOCAL FOR
    SELECT  [agentID],
            [reportGap],
            (SELECT [endDate] FROM [period] WHERE [periodID] = gap.[periodID]),
            'The calculation is Sum of the days between Effective date to Received date across all forms in the batch (' + [dbo].[FormatNumber] ([dayCount], 0) + ') / Number of forms in the batch (' + [dbo].[FormatNumber] ([batchCount], 0) + ')'
    FROM    #gapTable gap
    
    OPEN cur
    FETCH NEXT FROM cur INTO @agentID, @score, @effDate, @note
    WHILE @@FETCH_STATUS = 0 BEGIN
      EXEC [dbo].[spInsertAlert] @source = @source,
                                 @processCode = @processCode, 
                                 @user = @user, 
                                 @agentID = @agentID, 
                                 @score = @score, 
                                 @effDate = @effDate,
                                 @note = @note
      FETCH NEXT FROM cur INTO @agentID, @score, @effDate, @note
    END
    CLOSE cur
    DEALLOCATE cur
  END

  SELECT  [agentID] AS [agentID],
          @source AS [source],
          @processCode AS [processCode],
          [dbo].[GetAlertThreshold] (@processCode, [reportGap]) AS [threshold],
          [dbo].[GetAlertThresholdRange] (@processCode, [reportGap]) AS [thresholdRange],
          [dbo].[GetAlertSeverity] (@processCode, [reportGap]) AS [severity],
          [reportGap] AS [score],
          [dbo].[GetAlertScoreFormat] (@processCode, [reportGap]) AS [scoreDesc],
          (SELECT [endDate] FROM [dbo].[period] WHERE [periodID] = CASE WHEN @periodID = 0 THEN gap.[periodID] ELSE @periodID END) AS [effDate],
          [dbo].[GetAlertOwner] (@processCode) AS [owner],
          'The calculation is Sum of the days between Effective date to Received date across all forms in the batch (' + [dbo].[FormatNumber] ([dayCount], 0) + ') / Number of forms in the batch (' + [dbo].[FormatNumber] ([batchCount], 0) + ')' AS [note]
  FROM    #gapTable gap

  DROP TABLE #agentTable
  DROP TABLE #gapTable
END
