USE [COMPASS_DVLP]
GO
/****** Object:  StoredProcedure [dbo].[spGetClaimAttrCodes]    Script Date: 1/8/2021 3:57:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetClaimAttrCodes]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SELECT  [code],
          [description],
          [datatype] AS [type],
          [list] AS [comments]
  FROM    [claimattrcode]
END
