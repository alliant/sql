/****** Object:  StoredProcedure [dbo].[spCreateMissingAF]    Script Date: 3/20/2020 9:12:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spCreateMissingAF]
	-- Add the parameters for the stored procedure here
	@agentID VARCHAR(20),
	@fileNumber VARCHAR(1000),
	@fileID VARCHAR(1000),
	@addr1 VARCHAR(100),
	@addr2 VARCHAR(100),
	@addr3 VARCHAR(100),
	@addr4 VARCHAR(100),
	@city VARCHAR(100),
	@countyID VARCHAR(50),
	@state VARCHAR(20),
	@zip VARCHAR(20),
	@stage VARCHAR(20),
	@transactionType VARCHAR(20),
	@insuredType VARCHAR(20),
	@reportingDate DATETIME,
	@liability DECIMAL(18,2),
	@notes VARCHAR(MAX),
	@agentFileID INTEGER OUTPUT,
	@isError     BIT = 0      OUTPUT,
	@errMsg      VARCHAR(500) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET ANSI_WARNINGS OFF;
	declare @firstPolDt datetime,
			@firstCPLDt datetime,
			@transcount BIT = 0
  
    SELECT @agentFileID = [agentFileID] FROM [dbo].[agentfile] WHERE [agentID] = @agentID AND [fileID] = @fileID

	BEGIN TRY
		IF(@@TRANCOUNT = 0)			
		BEGIN
			BEGIN TRANSACTION
			SET @transCount = 1
		END	

		IF @agentFileID <> 0
		BEGIN
			UPDATE  [agentfile]
			SET     [addr1] = (CASE WHEN (@addr1 <> '' AND @addr1 IS NOT NULL) THEN @addr1 ELSE [addr1] END),
					[addr2] = (CASE WHEN (@addr2 <> '' AND @addr2 IS NOT NULL) THEN @addr2 ELSE [addr2] END),
					[addr3] = (CASE WHEN (@addr3 <> '' AND @addr3 IS NOT NULL) THEN @addr3 ELSE [addr3] END),
					[addr4] = (CASE WHEN (@addr4 <> '' AND @addr4 IS NOT NULL) THEN @addr4 ELSE [addr4] END),
					[city] =  (CASE WHEN (@city <> '' AND @city IS NOT NULL) THEN @city  ELSE [city]  END),
					[countyID] = (CASE WHEN (@countyID <> '' AND @countyID IS NOT NULL) THEN @countyID ELSE [countyID] END),
					[state] = (CASE WHEN (@state <> '' AND @state IS NOT NULL) THEN @state ELSE [state] END),
					[zip] = (CASE WHEN (@zip <> '' AND @zip IS NOT NULL) THEN @zip ELSE [zip] END),
					[stage] = @stage,
					[transactionType] = (CASE WHEN (@transactionType <> '' AND @transactionType IS NOT NULL) THEN @transactionType ELSE [transactionType] END),
						[reportingDate] = (CASE WHEN (@reportingDate IS NOT NULL AND [reportingDate] > @reportingDate) THEN @reportingDate ELSE [reportingDate] END),
						[liabilityAmt]  = (CASE WHEN (@liability IS NOT NULL AND @liability <> 0 AND @liability > [liabilityAmt]) THEN @liability ELSE [liabilityAmt] END),
					[insuredType] = (CASE WHEN (@insuredType <> '' AND @insuredType IS NOT NULL) THEN @insuredType ELSE [insuredType] END),
					[notes] = @notes
			WHERE   [agentID] = @agentID 
			AND     [fileID] = @fileID
		END
		ELSE
		BEGIN
			EXEC [dbo].[spGetNextSequence] @keyType = 'agentFile', @key = @agentFileID OUTPUT
			IF(@agentFileID = -1)
				RAISERROR('Sequence ''agentFile'' is not defined. Please contact system administrator.', 16, 1)

			INSERT INTO [dbo].[agentfile] ([agentFileID],[agentID],[fileNumber],[fileID],[stat],[addr1],[addr2],[addr3],[addr4],[city],[countyID],[state],[zip],[stage],[liabilityAmt],[transactionType],[reportingDate],[insuredType],[notes]) VALUES
			(
				@agentFileID,
				@agentID,
				@fileNumber,
				@fileID,
				(CASE WHEN EXISTS(SELECT artranID FROM artran 
									WHERE arTran.entity = 'A'       AND 
									arTran.entityID     = @agentID  AND 
									arTran.fileid       = @fileID   AND 
									arTran.transType    = 'F') 
					THEN 'O' ELSE 'C' END),
				@addr1,
				@addr2,
				@addr3,
				@addr4,
				@city,
				@countyID,
				@state,
				@zip,
				@stage,
				@liability,
				@transactionType,
				@reportingDate,
				@insuredType,
				@notes)

			UPDATE  [agentfile]
			SET  liabilityAmt = (select max(policy.liabilityAmount)	from policy where
								policy.agentID = @agentID  
							and policy.fileID  = @fileID)
			where agentfile.agentid = @AgentID and agentfile.fileid = @FileID
		END
	  
		UPDATE  [agentfile]
		SET  invoiceAmt =  ISNULL((select sum(netdelta) from batch b inner join 
							batchform bf on b.batchid = bf.batchid 
							where b.agentID = @agentID and
								b.stat = 'C' and
								bf.fileID = @fileID group by b.agentID, bf.fileID ),0)
		where agentfile.agentid = @AgentID and agentfile.fileid = @FileID

		UPDATE  [agentfile]
		SET  osAmt      = ISNULL((select ar.remainingamt from artran ar where
									ar.entity    = 'A'
								and ar.entityID  = @agentID      
								and ar.fileID    = @fileID
								and ar.transtype = 'F'
								and ar.type      = 'F'
								and ar.Seq       = 0),0)
			where agentfile.agentid = @AgentID and agentfile.fileid = @FileID

		UPDATE  [agentfile]
		SET paidAmt    = abs(invoiceAmt - osAmt)  where agentfile.agentid = @AgentID and agentfile.fileid = @FileID
   
		UPDATE  [agentfile]
		SET closingDate = case when agentfile.stat = 'C' then CURRENT_TIMESTAMP ELSE NULL 
							end 
		where agentfile.agentid = @AgentID and agentfile.fileid = @FileID

 				  
		UPDATE agentfile set 
		agentfile.firstPolicyIssueDt =  
		(select min(p.issueDate) from policy p where p.agentID = @agentID and 
		p.fileID = @fileID  and
		p.issueDate is not null 
		group by   p.agentID,p.fileID) 
		where agentfile.agentid = @AgentID and agentfile.fileid = @FileID

		UPDATE agentfile set 
		agentfile.firstCPLIssueDt = 
		(select min(c.issueDate) from CPL c where c.agentID = @agentID and 
		c.fileID = @fileID  and
		c.issueDate is not null 
		group by  c.agentID,c.fileID) 
		where agentfile.agentid = @AgentID and agentfile.fileid = @FileID
 
		
																

		SELECT  @firstPolDt =  firstPolicyIssueDt, @firstCPLDt = firstCPLIssueDt from agentfile where 
								agentid = @AgentID and
								fileID  = @FileID
 
		UPDATE agentfile set agentfile.origin = dbo.GetAgentFileOrigin(@agentID,
												@fileID,
												@firstPolDt,
												@firstCPLDt) where agentfile.agentid = @AgentID 
																and agentfile.fileid = @fileID
  		;
  		WITH CTE_Ln(agentID, fileID, totalLoanPolicy) as
  		(select b.agentID, bf.fileID, count(*) from batch b inner join batchform bf 
                            on b.batchID = bf.batchID 
							inner join stateform sf 
							 on b.stateID = sf.stateID AND 
							    bf.formID = sf.formID inner join agentfile on
							  b.agentID = agentfile.agentID 
							  and bf.fileID = agentfile.fileID 
							  where bf.formType = 'P' 
							  and sf.insuredType = 'L' 
							group by b.agentID, bf.fileID
  		),
  		CTE_Ow(agentID, fileID, totalOwnerPolicy) as
  		(select b.agentID, bf.fileID, count(*) from batch b inner join batchform bf 
                            on b.batchID = bf.batchID 
							inner join stateform sf 
							 on b.stateID = sf.stateID AND 
							    bf.formID = sf.formID 
							inner join agentfile on
							  b.agentID = agentfile.agentID 
							  and bf.fileID = agentfile.fileID 
							  where bf.formType = 'P' 
							  and sf.insuredType = 'O' 
							group by b.agentID, bf.fileID
  		)

  		update agentfile set agentfile.fileType = 
              		case when (CTE_Ln.totalLoanPolicy > 0) and (CTE_Ow.totalOwnerPolicy > 0) then 'Purchase'
					 when CTE_Ln.totalLoanPolicy > 0 then 'Refinance'
					when CTE_Ow.totalOwnerPolicy > 0 then 'Cash Purchase' 					
			   end
			    from agentfile 
			   left outer join CTE_Ln on 
			     agentfile.agentID  = CTE_Ln.agentID and
				 agentfile.fileID   = CTE_Ln.fileID 
				left outer join CTE_Ow on 
			     agentfile.agentID  = CTE_Ow.agentID and
				 agentfile.fileID   = CTE_Ow.fileID 
				 where agentfile.agentid = @AgentID 
				   and agentfile.fileid = @fileID
	 
  		IF(@transCount = 1)
		  COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		SET @isError = ERROR_STATE()
		SET @errMsg  = ERROR_MESSAGE()
		IF(@transCount = 1)
			ROLLBACK TRANSACTION
		RETURN
	END CATCH
							
END
