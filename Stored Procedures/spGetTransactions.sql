-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
/****** Object:  StoredProcedure [dbo].[spGetTransactions]    Script Date: 1/18/2021 8:15:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author:		<Rahul Sharma>
-- Create date: <16-11-2020>
-- Description:	<Get artran records>
-- =============================================
ALTER PROCEDURE [dbo].[spGetTransactions] 
	-- Add the parameters for the stored procedure here
    @piArTranID                   INTEGER     = 0,
	@pcEntity                     VARCHAR(50) = 'ALL',
	@pcEntityID                   VARCHAR(50) = 'ALL',	
	/*------------applicable for file-----------*/
	@plIncludeFile                BIT         = 0,
	@plIncludeFullyPaidFile       BIT         = 0,
    @plIncludeArchivedFile        BIT         = 0,	
    /*----------applicable for invoice----------*/
	@plIncludeInvoice             BIT         = 0,
	@plIncludeVoidInvoice         BIT         = 0,
    @plIncludeFullyPaidInvoice    BIT         = 0,	    
	/*-----------applicable for credit----------*/
	@plIncludeCredit              BIT         = 0,
	@plIncludeVoidCredit          BIT         = 0,
	@plIncludeFullyAppliedCredit  BIT         = 0,
	/*----------applicable for payment----------*/
	@plIncludePayment             BIT         = 0,
	@plIncludeVoidPayment         BIT         = 0,
	@plIncludeFullyAppliedPayment BIT         = 0,
	@plIncludeArchivedPayment     BIT         = 0,
	/*----------applicable for payment----------*/
	@plIncludeRefund              BIT         = 0,
	@plIncludeVoidRefund          BIT         = 0,
	@plIncludeArchivedRefund      BIT         = 0,
	/*---------------date range-----------------*/	
    @pFromPostDate          DATETIME          = NULL,
	@pToPostDate            DATETIME          = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    
	-- Insert statements for procedure here
    
	CREATE TABLE #artran (artranID         INTEGER,
	                      revenueType      VARCHAR(50),
						  tranID           VARCHAR(50),
						  seq              INTEGER,
						  sourceType       VARCHAR(50),
						  sourceID         VARCHAR(50),
						  type             VARCHAR(50),
	                      entity           VARCHAR(50),
	                      entityID         VARCHAR(50),
						  entityName       VARCHAR(200),
						  fileNumber       VARCHAR(50),
	                      fileID           VARCHAR(50),
						  tranAmt          [DECIMAL](18, 2) NULL,
						  remainingAmt     [DECIMAL](18, 2) NULL,
					      tranDate         DATETIME,
                          void             BIT,
                          voidDate	       DATETIME,
                          voidBy	       VARCHAR(100),
                          notes            VARCHAR(MAX),						   
                          createdBy        VARCHAR(100),
                          createdDate      DATETIME,
                          dueDate          DATETIME,
                          fullyPaid        BIT,
                          reference        VARCHAR(100),
                          appliedAmt       [DECIMAL](18, 2) NULL,
                          transType        VARCHAR(20),
                          postBy           VARCHAR(100), 						  
						  postDate         DATETIME,
						  ledgerID         INTEGER,
						  voidledgerID     INTEGER,
						  userName         VARCHAR(32),
						  postByName       VARCHAR(32),
						  voidByName       VARCHAR(32),
						  archived         BIT) /* Client Side */ 

    CREATE TABLE #DateRangefileIDs (fileID VARCHAR(50))  

    IF @piArTranID <> 0
	BEGIN
	  INSERT INTO #artran (artranID,  revenueType,  tranID, seq, sourceType, sourceID, type, entity, entityID, fileNumber, fileID, tranAmt,
	                       remainingAmt, tranDate, void, voidDate, voidBy, notes, createdBy, createdDate, dueDate, fullyPaid, reference,
		    			   appliedAmt, transType, postBy, postDate, ledgerID, voidledgerID) 
						 
      SELECT artran.artranID, artran.revenueType, artran.tranID, artran.seq, artran.sourceType, artran.sourceID, artran.type, artran.entity,
	         artran.entityID, artran.fileNumber, artran.fileID, artran.tranAmt, artran.remainingAmt, artran.tranDate, artran.void, artran.voidDate,
		     artran.voidBy, artran.notes, artran.createdBy, artran.createdDate, artran.dueDate, artran.fullyPaid, artran.reference, artran.appliedAmt,
		     artran.transType, artran.postBy, artran.postDate, artran.ledgerID, artran.voidledgerID
        FROM artran
        WHERE artran.artranID = @piArTranID
		
	  /* If File type record then calculate total amount */
	  UPDATE #artran
	  SET #artran.tranAmt = s.tranAmt
	  FROM #artran
	    INNER JOIN (SELECT at.entityID, at.fileID, SUM (at.tranAmt) AS tranAmt FROM artran at WHERE at.entity       = (CASE WHEN @pcEntity   = 'ALL' THEN at.entity   ELSE @pcEntity   END)
                                                                                                  AND at.entityID   = (CASE WHEN @pcEntityID = 'ALL' THEN at.entityID ELSE @pcEntityID END)		  
		                                                                                          AND (at.type      = 'I' OR at.type  = 'R') 
		                                                                                          AND  at.transtype = 'F' 		    																							     
			    																	              GROUP BY at.entityID, at.fileID) s	 
      ON  #artran.entityID    = s.entityID
	  AND (#artran.fileID     = s.fileID OR ISNULL(#artran.fileID, s.fileID) IS NULL)		
	  WHERE #artran.type      = 'F'
	    AND #artran.transtype = 'F'
	    AND #artran.seq       =  0	
	END /* IF @piArTranID <> 0 */
	ELSE
	BEGIN
	  /*-----------Include file type transactions------------*/
	  IF @plIncludeFile = 1 
      BEGIN
	    IF @pFromPostDate IS NOT NULL and @pToPostDate IS NOT NULL
	      BEGIN 
	        INSERT INTO #DateRangefileIDs(fileID) (SELECT fileID FROM artran at WHERE at.entity  = (CASE WHEN @pcEntity   = 'ALL' THEN at.entity   ELSE @pcEntity   END)
                                                                                                  AND at.entityID  = (CASE WHEN @pcEntityID = 'ALL' THEN at.entityID ELSE @pcEntityID END)
																	                              AND at.transtype = 'F'
																	                              AND (at.tranDate >= @pFromPostDate AND at.tranDate <= @pToPostDate))
	      
		    IF @plIncludeArchivedFile = 1
			  BEGIN 
	            INSERT INTO #DateRangefileIDs(fileID) (SELECT fileID FROM artranh at WHERE at.entity  = (CASE WHEN @pcEntity   = 'ALL' THEN at.entity   ELSE @pcEntity   END)
                                                                                                       AND at.entityID  = (CASE WHEN @pcEntityID = 'ALL' THEN at.entityID ELSE @pcEntityID END)
																	                                   AND at.transtype = 'F'
																	                                   AND (at.tranDate >= @pFromPostDate AND at.tranDate <= @pToPostDate))
              END
		  
		  END

	    INSERT INTO #artran (artranID,  revenueType,  tranID, seq, sourceType, sourceID, type, entity, entityID, fileNumber, fileID, tranAmt,
	                         remainingAmt, tranDate, void, voidDate, voidBy, notes, createdBy, createdDate, dueDate, fullyPaid, reference,
			     			 appliedAmt, transType, postBy, postDate, ledgerID, voidledgerID) 
						 
        SELECT artran.artranID, artran.revenueType, artran.tranID, artran.seq, artran.sourceType, artran.sourceID, artran.type, artran.entity,
	           artran.entityID, artran.fileNumber, artran.fileID, s.tranAmt, artran.remainingAmt, artran.tranDate, artran.void, artran.voidDate,
		       artran.voidBy, artran.notes, artran.createdBy, artran.createdDate, artran.dueDate, artran.fullyPaid, artran.reference, artran.appliedAmt,
		       artran.transType, artran.postBy, artran.postDate, artran.ledgerID, artran.voidledgerID
          FROM artran
		  LEFT JOIN (SELECT at.entityID, at.fileID, SUM (at.tranAmt) AS tranAmt FROM artran at WHERE at.entity      = (CASE WHEN @pcEntity   = 'ALL' THEN at.entity   ELSE @pcEntity   END)
                                                                                                  AND at.entityID   = (CASE WHEN @pcEntityID = 'ALL' THEN at.entityID ELSE @pcEntityID END)		  
		                                                                                          AND (at.type      = 'I' OR at.type  = 'R') 
		                                                                                          AND  at.transtype = 'F' 		    																							     
			    																	              GROUP BY at.entityID, at.fileID) s	 
          ON artran.entityID = s.entityID AND (artran.fileID = s.fileID OR ISNULL(artran.fileID, s.fileID) IS NULL)	
          WHERE artran.entity    = (CASE WHEN @pcEntity   = 'ALL'    THEN artran.entity    ELSE @pcEntity      END) 
		    AND artran.entityID  = (CASE WHEN @pcEntityID = 'ALL'    THEN artran.entityID  ELSE @pcEntityID    END)
		    AND artran.type      = 'F' 
		    AND artran.transtype = 'F' 
			AND artran.seq       = 0			
		    AND ((@plIncludeFullyPaidFile = 0 AND artran.remainingAmt <> 0) OR @plIncludeFullyPaidFile = 1) 
			AND ((@pFromPostDate IS NULL OR @pToPostDate IS NULL
			     ) OR 
			      artran.fileID IN (SELECT DISTINCT fileID FROM #DateRangefileIDs)
			    )
		
        IF @plIncludeArchivedFile = 1
		  BEGIN
	        INSERT INTO #artran (artranID,  revenueType,  tranID, seq, sourceType, sourceID, type, entity, entityID, fileNumber, fileID, tranAmt,
	                             remainingAmt, tranDate, void, voidDate, voidBy, notes, createdBy, createdDate, dueDate, fullyPaid, reference,
			     			     appliedAmt, transType, postBy, postDate, ledgerID, voidledgerID, archived) 
						 
            SELECT artranh.artranID, artranh.revenueType, artranh.tranID, artranh.seq, artranh.sourceType, artranh.sourceID, artranh.type, artranh.entity,
	               artranh.entityID, artranh.fileNumber, artranh.fileID, s.tranAmt, artranh.remainingAmt, artranh.tranDate, artranh.void, artranh.voidDate,
		           artranh.voidBy, artranh.notes, artranh.createdBy, artranh.createdDate, artranh.dueDate, artranh.fullyPaid, artranh.reference, artranh.appliedAmt,
		           artranh.transType, artranh.postBy, artranh.postDate, artranh.ledgerID, artranh.voidledgerID, 1
              FROM artranh
		      LEFT JOIN (SELECT at.entityID, at.fileID, SUM (at.tranAmt) AS tranAmt FROM artranh at WHERE at.entity      = (CASE WHEN @pcEntity   = 'ALL' THEN at.entity   ELSE @pcEntity   END)
                                                                                                      AND at.entityID   = (CASE WHEN @pcEntityID = 'ALL' THEN at.entityID ELSE @pcEntityID END)		  
		                                                                                              AND (at.type      = 'I' OR at.type  = 'R') 
		                                                                                              AND  at.transtype = 'F' 		    																							     
			    																	                  GROUP BY at.entityID, at.fileID) s	 
              ON artranh.entityID = s.entityID AND (artranh.fileID = s.fileID OR ISNULL(artranh.fileID, s.fileID) IS NULL)	
              WHERE artranh.entity    = (CASE WHEN @pcEntity   = 'ALL'    THEN artranh.entity    ELSE @pcEntity      END) 
		        AND artranh.entityID  = (CASE WHEN @pcEntityID = 'ALL'    THEN artranh.entityID  ELSE @pcEntityID    END)
		        AND artranh.type      = 'F' 
		        AND artranh.transtype = 'F' 
			    AND artranh.seq       = 0 
			    AND ((@pFromPostDate IS NULL OR @pToPostDate IS NULL
			         ) OR 
			          artranh.fileID IN (SELECT DISTINCT fileID FROM #DateRangefileIDs)
			        )
		  END
	  END
	  
	  /*-----------Include misc type invoice transactions------------*/
	  IF @plIncludeInvoice = 1 
      BEGIN
	    INSERT INTO #artran (artranID,  revenueType,  tranID, seq, sourceType, sourceID, type, entity, entityID, fileNumber, fileID, tranAmt,
	                         remainingAmt, tranDate, void, voidDate, voidBy, notes, createdBy, createdDate, dueDate, fullyPaid, reference,
			     			 appliedAmt, transType, postBy, postDate, ledgerID, voidledgerID) 
						 
        SELECT artran.artranID, artran.revenueType, artran.tranID, artran.seq, artran.sourceType, artran.sourceID, artran.type, artran.entity,
	           artran.entityID, artran.fileNumber, artran.fileID, artran.tranAmt, artran.remainingAmt, artran.tranDate, artran.void, artran.voidDate,
		       artran.voidBy, artran.notes, artran.createdBy, artran.createdDate, artran.dueDate, artran.fullyPaid, artran.reference, artran.appliedAmt,
		       artran.transType, artran.postBy, artran.postDate, artran.ledgerID, artran.voidledgerID
          FROM artran
          WHERE artran.entity    = (CASE WHEN @pcEntity   = 'ALL'    THEN artran.entity    ELSE @pcEntity      END) 
		    AND artran.entityID  = (CASE WHEN @pcEntityID = 'ALL'    THEN artran.entityID  ELSE @pcEntityID    END)	
		    AND artran.type      = 'I' 
		    AND artran.transtype = ' ' 
		    AND artran.seq       = 0
			AND ((@plIncludeVoidInvoice = 0 AND (artran.void = 0 OR artran.void IS NULL)) OR @plIncludeVoidInvoice = 1)
		    AND ((@plIncludeFullyPaidInvoice = 0 AND artran.remainingAmt <> 0) OR @plIncludeFullyPaidInvoice = 1)			
	        AND artran.tranDate >= (CASE WHEN @pFromPostDate IS NULL THEN artran.tranDate  ELSE @pFromPostDate END) 
		    AND artran.tranDate <= (CASE WHEN @pToPostDate   IS NULL THEN artran.tranDate  ELSE @pToPostDate   END)
	  END
	  	  
	  /*-----------Include credit type transactions------------*/
	  IF @plIncludeCredit = 1 
      BEGIN
	    INSERT INTO #artran (artranID,  revenueType,  tranID, seq, sourceType, sourceID, type, entity, entityID, fileNumber, fileID, tranAmt,
	                         remainingAmt, tranDate, void, voidDate, voidBy, notes, createdBy, createdDate, dueDate, fullyPaid, reference,
			     			 appliedAmt, transType, postBy, postDate, ledgerID, voidledgerID) 
						 
        SELECT artran.artranID, artran.revenueType, artran.tranID, artran.seq, artran.sourceType, artran.sourceID, artran.type, artran.entity,
	           artran.entityID, artran.fileNumber, artran.fileID, artran.tranAmt, artran.remainingAmt, artran.tranDate, artran.void, artran.voidDate,
		       artran.voidBy, artran.notes, artran.createdBy, artran.createdDate, artran.dueDate, artran.fullyPaid, artran.reference, artran.appliedAmt,
		       artran.transType, artran.postBy, artran.postDate, artran.ledgerID, artran.voidledgerID
          FROM artran
          WHERE artran.entity    = (CASE WHEN @pcEntity   = 'ALL'    THEN artran.entity    ELSE @pcEntity      END) 
		    AND artran.entityID  = (CASE WHEN @pcEntityID = 'ALL'    THEN artran.entityID  ELSE @pcEntityID    END)
		    AND artran.type      = 'C' 
			AND artran.seq       = 0 
		    AND ((@plIncludeVoidCredit = 0 AND (artran.void = 0 OR artran.void IS NULL)) OR @plIncludeVoidCredit = 1)
		    AND ((@plIncludeFullyAppliedCredit = 0 AND artran.remainingAmt <> 0) OR @plIncludeFullyAppliedCredit = 1)		
	        AND artran.tranDate >= (CASE WHEN @pFromPostDate IS NULL THEN artran.tranDate  ELSE @pFromPostDate END) 
		    AND artran.tranDate <= (CASE WHEN @pToPostDate   IS NULL THEN artran.tranDate  ELSE @pToPostDate   END)
	  END
	  
	  /*-----------Include payment type transactions------------*/
	  IF @plIncludePayment = 1 
      BEGIN
	    INSERT INTO #artran (artranID,  revenueType,  tranID, seq, sourceType, sourceID, type, entity, entityID, fileNumber, fileID, tranAmt,
	                         remainingAmt, tranDate, void, voidDate, voidBy, notes, createdBy, createdDate, dueDate, fullyPaid, reference,
			     			 appliedAmt, transType, postBy, postDate, ledgerID, voidledgerID) 
						 
        SELECT artran.artranID, artran.revenueType, artran.tranID, artran.seq, artran.sourceType, artran.sourceID, artran.type, artran.entity,
	           artran.entityID, artran.fileNumber, artran.fileID, artran.tranAmt, artran.remainingAmt, artran.tranDate, artran.void, artran.voidDate,
		       artran.voidBy, artran.notes, artran.createdBy, artran.createdDate, artran.dueDate, artran.fullyPaid, artran.reference, artran.appliedAmt,
		       artran.transType, artran.postBy, artran.postDate, artran.ledgerID, artran.voidledgerID
        FROM artran
        WHERE artran.entity    = (CASE WHEN @pcEntity   = 'ALL'    THEN artran.entity    ELSE @pcEntity      END) 
		  AND artran.entityID  = (CASE WHEN @pcEntityID = 'ALL'    THEN artran.entityID  ELSE @pcEntityID    END)
          AND artran.type      = 'P' 
		  AND artran.seq       = 0 
		  AND ((@plIncludeVoidPayment = 0 AND (artran.void = 0 OR artran.void IS NULL)) OR @plIncludeVoidPayment = 1)
		  AND ((@plIncludeFullyAppliedPayment = 0 AND artran.remainingAmt <> 0) OR @plIncludeFullyAppliedPayment = 1)		
	      AND artran.tranDate >= (CASE WHEN @pFromPostDate IS NULL THEN artran.tranDate  ELSE @pFromPostDate END) 
		  AND artran.tranDate <= (CASE WHEN @pToPostDate   IS NULL THEN artran.tranDate  ELSE @pToPostDate   END)
	  
	    IF @plIncludeArchivedPayment = 1 
		BEGIN
	      INSERT INTO #artran (artranID,  revenueType,  tranID, seq, sourceType, sourceID, type, entity, entityID, fileNumber, fileID, tranAmt,
	                           remainingAmt, tranDate, void, voidDate, voidBy, notes, createdBy, createdDate, dueDate, fullyPaid, reference,
			     			   appliedAmt, transType, postBy, postDate, ledgerID, voidledgerID, archived) 
						 
          SELECT artranh.artranID, artranh.revenueType, artranh.tranID, artranh.seq, artranh.sourceType, artranh.sourceID, artranh.type, artranh.entity,
	             artranh.entityID, artranh.fileNumber, artranh.fileID, artranh.tranAmt, artranh.remainingAmt, artranh.tranDate, artranh.void, artranh.voidDate,
		         artranh.voidBy, artranh.notes, artranh.createdBy, artranh.createdDate, artranh.dueDate, artranh.fullyPaid, artranh.reference, artranh.appliedAmt,
		         artranh.transType, artranh.postBy, artranh.postDate, artranh.ledgerID, artranh.voidledgerID, 1
          FROM artranh
          WHERE artranh.entity    = (CASE WHEN @pcEntity   = 'ALL'    THEN artranh.entity    ELSE @pcEntity      END) 
		    AND artranh.entityID  = (CASE WHEN @pcEntityID = 'ALL'    THEN artranh.entityID  ELSE @pcEntityID    END)
            AND artranh.type      = 'P' 
		    AND artranh.seq       = 0 
		    AND ((@plIncludeVoidPayment = 0 AND (artranh.void = 0 OR artranh.void IS NULL)) OR @plIncludeVoidPayment = 1)		
	        AND artranh.tranDate >= (CASE WHEN @pFromPostDate IS NULL THEN artranh.tranDate  ELSE @pFromPostDate END) 
		    AND artranh.tranDate <= (CASE WHEN @pToPostDate   IS NULL THEN artranh.tranDate  ELSE @pToPostDate   END)
	    END	
	  END
	  
	  /*-----------Include refund type transactions------------*/
	  IF @plIncludeRefund = 1 
      BEGIN
	    INSERT INTO #artran (artranID,  revenueType,  tranID, seq, sourceType, sourceID, type, entity, entityID, fileNumber, fileID, tranAmt,
	                         remainingAmt, tranDate, void, voidDate, voidBy, notes, createdBy, createdDate, dueDate, fullyPaid, reference,
			    			 appliedAmt, transType, postBy, postDate, ledgerID, voidledgerID) 
						 
        SELECT artran.artranID, artran.revenueType, artran.tranID, artran.seq, artran.sourceType, artran.sourceID, 'RF', artran.entity,
	           artran.entityID, artran.fileNumber, artran.fileID, ABS(artran.tranAmt), artran.remainingAmt, artran.tranDate, artran.void, artran.voidDate,
		       artran.voidBy, artran.notes, artran.createdBy, artran.createdDate, artran.dueDate, artran.fullyPaid, artran.reference, artran.appliedAmt,
		       artran.transType, artran.postBy, artran.postDate, artran.ledgerID, artran.voidledgerID
        FROM artran
        WHERE artran.entity    =  (CASE WHEN @pcEntity   = 'ALL'    THEN artran.entity    ELSE @pcEntity      END) 
		  AND artran.entityID  =  (CASE WHEN @pcEntityID = 'ALL'    THEN artran.entityID  ELSE @pcEntityID    END)
		  AND (artran.type = 'P' or artran.type = 'C')
		  AND artran.seq       > 0 
		  AND artran.tranAmt   < 0 
		  AND ((@plIncludeVoidRefund = 0 AND (artran.void = 0 OR artran.void IS NULL)) OR @plIncludeVoidRefund = 1)			
	      AND artran.tranDate >= (CASE WHEN @pFromPostDate IS NULL THEN artran.tranDate  ELSE @pFromPostDate END) 
		  AND artran.tranDate <= (CASE WHEN @pToPostDate   IS NULL THEN artran.tranDate  ELSE @pToPostDate   END)
	  
	    IF @plIncludeArchivedRefund = 1
		BEGIN
	      INSERT INTO #artran (artranID,  revenueType,  tranID, seq, sourceType, sourceID, type, entity, entityID, fileNumber, fileID, tranAmt,
	                           remainingAmt, tranDate, void, voidDate, voidBy, notes, createdBy, createdDate, dueDate, fullyPaid, reference,
			    			   appliedAmt, transType, postBy, postDate, ledgerID, voidledgerID, archived) 
						 
          SELECT artranh.artranID, artranh.revenueType, artranh.tranID, artranh.seq, artranh.sourceType, artranh.sourceID, 'RF', artranh.entity,
	             artranh.entityID, artranh.fileNumber, artranh.fileID, ABS(artranh.tranAmt), artranh.remainingAmt, artranh.tranDate, artranh.void, artranh.voidDate,
		         artranh.voidBy, artranh.notes, artranh.createdBy, artranh.createdDate, artranh.dueDate, artranh.fullyPaid, artranh.reference, artranh.appliedAmt,
		         artranh.transType, artranh.postBy, artranh.postDate, artranh.ledgerID, artranh.voidledgerID, 1
          FROM artranh
          WHERE artranh.entity    =  (CASE WHEN @pcEntity   = 'ALL'    THEN artranh.entity    ELSE @pcEntity      END) 
		    AND artranh.entityID  =  (CASE WHEN @pcEntityID = 'ALL'    THEN artranh.entityID  ELSE @pcEntityID    END)
		    AND (artranh.type      = 'P' or artranh.type      = 'C') 
		    AND artranh.seq       > 0 
		    AND artranh.tranAmt   < 0 
		    AND ((@plIncludeVoidRefund = 0 AND (artranh.void = 0 OR artranh.void IS NULL)) OR @plIncludeVoidRefund = 1)			
	        AND artranh.tranDate >= (CASE WHEN @pFromPostDate IS NULL THEN artranh.tranDate  ELSE @pFromPostDate END) 
		    AND artranh.tranDate <= (CASE WHEN @pToPostDate   IS NULL THEN artranh.tranDate  ELSE @pToPostDate   END)
	    END
	  END	  
	END /* ELSE */
					
	/* Update userName, postByName, voidByName */
    UPDATE #artran	
	SET #artran.userName   = (SELECT sysuser.name FROM sysuser WHERE sysuser.uid = #artran.createdBy),
	    #artran.postByName = (SELECT sysuser.name FROM sysuser WHERE sysuser.uid = #artran.postby),
	    #artran.voidByName = (SELECT sysuser.name FROM sysuser WHERE sysuser.uid = #artran.voidby)
	  FROM #artran
	
	/* Update agent name when entity is agent */
    UPDATE #artran
    SET #artran.entityName = agent.name
	  FROM agent 
	  WHERE #artran.entity   = 'A'
	    AND #artran.entityID = agent.agentID	
		
	SELECT * FROM #artran
END
GO