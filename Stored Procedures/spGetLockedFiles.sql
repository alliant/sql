USE [COMPASS_ALFA]
GO
/****** Object:  StoredProcedure [dbo].[spGetLockedFiles]    Script Date: 11/22/2023 8:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Shefali 
-- Create date: 10/25/2023
-- Description:	To get Locked Files
-- =============================================
CREATE PROCEDURE [dbo].[spGetLockedFiles] 
	-- Add the parameters for the stored procedure here
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select af.[agentfileID],
	       af.[fileNumber],
		   af.[agentID],
		   (select name from agent where agentID = af.[agentID]) as [agentName],
		   s.[lockDate],
		   s.[uid] as [lockedBy],
		   (select name from sysuser where uid = s.[uid]) as [lockedByName]
		   from [dbo].[agentfile] af
	inner join [dbo].[syslock] s
	on (s.[entityType] = 'AgentFile' and s.[entityID] = af.[agentfileID])
END

