USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spAlertQARScoreDrop]    Script Date: 12/27/2017 10:00:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spAlertQARScoreDrop]
  @agentID VARCHAR(MAX) = '',
  @user VARCHAR(100) = 'compass@alliantnational.com',
  @preview BIT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;
  
  CREATE TABLE #agentTable ([agentID] VARCHAR(30))
  INSERT INTO #agentTable ([agentID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)
  
  DECLARE @alertID INTEGER = 0,
          -- Used for the preview
          @source VARCHAR(30) = 'QAR',
          @processCode VARCHAR(30) = 'QAR05',
          @effDate DATETIME = NULL,
          @score DECIMAL(18,2) = 0,
          -- Notes for the alert note
          @note VARCHAR(MAX) = ''

  CREATE TABLE #scoreTable 
  (
    [agentID] VARCHAR(20),
    [qarDate] DATETIME,
    [qarScore] INTEGER,
    [errScore] INTEGER,
    [prevQarDate] DATETIME,
    [prevQarScore] INTEGER,
    [prevErrScore] INTEGER,
    [qarScoreDrop] INTEGER,
    [errScoreDrop] INTEGER
  )
  
  --Insert the current scores
  INSERT INTO #scoreTable ([agentID], [qarDate], [qarScore], [errScore], [prevQarScore], [prevErrScore])
  SELECT  q.[agentID],
          q2.[qarDate],
          ISNULL(q.[score],0) AS [qarScore],
          ISNULL(s.[score],0) AS [errScore],
          0,
          0
  FROM    [dbo].[qar] q INNER JOIN
          [dbo].[agent] a
  ON      q.[agentID] = a.[agentID] INNER JOIN
          (
          SELECT  [agentID],
                  MAX([auditFinishDate]) AS [qarDate]
          FROM    [dbo].[qar]
          WHERE   [stat] = 'C'
          AND     [auditType] = 'Q'
          GROUP BY [agentID]
          ) q2
  ON      q.[agentID] = q2.[agentID]
  AND     q.[auditFinishDate] = q2.[qarDate] LEFT OUTER JOIN
          [dbo].[qarsection] s
  ON      q.[qarID] = s.[qarID]
  AND     s.[sectionID] = 6
  WHERE   q.[stat] = 'C'
  AND     q.[agentID] IN (SELECT [agentID] FROM #agentTable)

  --Insert the previous scores
  MERGE INTO #scoreTable score
  USING   (
          SELECT  q.[agentID],
                  q2.[qarDate],
                  ISNULL(q.[score],0) AS [qarScore],
                  ISNULL(s.[score],0) AS [errScore]
          FROM    [dbo].[qar] q INNER JOIN
                  (
                  SELECT  [agentID],
                          MAX([auditFinishDate]) AS [qarDate]
                  FROM    [dbo].[qar] q
                  WHERE   [auditFinishDate] != (SELECT MAX([auditFinishDate]) FROM [dbo].[qar] WHERE [agentID] = q.[agentID])
                  AND     [auditType] = 'Q'
                  GROUP BY [agentID]
                  ) q2
          ON      q.[agentID] = q2.[agentID]
          AND     q.[auditFinishDate] = q2.[qarDate] LEFT OUTER JOIN
                  [dbo].[qarsection] s
          ON      q.[qarID] = s.[qarID]
          WHERE   s.[sectionID] = 6
          ) prev
  ON      score.[agentID] = prev.[agentID]
  WHEN MATCHED THEN
  UPDATE SET [prevQarDate] = prev.[qarDate],
             [prevQarScore] = prev.[qarScore],
             [prevErrScore] = prev.[errScore],
             [qarScoreDrop] = prev.[qarScore] - score.[qarScore],
             [errScoreDrop] = prev.[errScore] - score.[errScore];

  UPDATE  #scoreTable
  SET     [qarScoreDrop] = 0
  WHERE   [qarScoreDrop] < 0
  OR      [qarScoreDrop] IS NULL
              
  IF (@preview = 0)
  BEGIN
    -- QAR Score Drop
    DECLARE cur CURSOR LOCAL FOR
    SELECT  [agentID],
            ISNULL([qarScoreDrop],0),
            [qarDate],
            'The calculation is Last Years QAR Score (' + [dbo].[FormatNumber] ([prevQarScore], 0) + ') - Current Years QAR Score (' + [dbo].[FormatNumber] ([qarScore], 0) + ')'
    FROM    #scoreTable

    OPEN cur
    FETCH NEXT FROM cur INTO @agentID, @score, @effDate, @note
    WHILE @@FETCH_STATUS = 0 BEGIN
      EXEC [dbo].[spInsertAlert] @source = @source,
                                  @processCode = @processCode, 
                                  @user = @user, 
                                  @agentID = @agentID, 
                                  @score = @score, 
                                  @effDate = @effDate,
                                  @note = @note
      FETCH NEXT FROM cur INTO @agentID, @score, @effDate, @note
    END
    CLOSE cur
    DEALLOCATE cur
  END

  SELECT  [agentID] AS [agentID],
          @source AS [source],
          @processCode AS [processCode],
          [dbo].[GetAlertThreshold] (@processCode, [qarScoreDrop]) AS [threshold],
          [dbo].[GetAlertThresholdRange] (@processCode, [qarScoreDrop]) AS [thresholdRange],
          [dbo].[GetAlertSeverity] (@processCode, [qarScoreDrop]) AS [severity],
          [qarScoreDrop] AS [score],
          [dbo].[GetAlertScoreFormat] (@processCode, [qarScoreDrop]) AS [scoreDesc],
          [qarDate] AS [effDate],
          [dbo].[GetAlertOwner] (@processCode) AS [owner],
           'The calculation is Last Years QAR Score (' + [dbo].[FormatNumber] ([prevQarScore], 0) + ') - Current Years QAR Score (' + [dbo].[FormatNumber] ([qarScore], 0) + ')' AS [note]
  FROM    #scoreTable

  DROP TABLE #agentTable
  DROP TABLE #scoreTable
END
