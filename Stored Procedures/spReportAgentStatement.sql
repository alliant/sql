USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spReportAgentStatement]    Script Date: 5/19/2017 2:59:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportAgentStatement]
	-- Add the parameters for the stored procedure here
  @asOfDate DATETIME = NULL,
  @agentName VARCHAR(10) = ''
AS
BEGIN
  DECLARE @waivedAmount DECIMAL(18,2) = 250
  
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  CREATE TABLE #deductibleTable
  (
  agentID VARCHAR(20),
  name VARCHAR(200),
  equalAmount DECIMAL(18,2),
  lessAmount DECIMAL(18,2),
  greaterAmount DECIMAL(18,2),
  effDate DATETIME
  )

  --Add the deductible amounts for NETCO
  INSERT INTO #deductibleTable
  SELECT [agentID],[name],5500,0,0,GETDATE() FROM [agent] WHERE [name] LIKE '%NETCO%'

  --Add the deductible amounts for Investors
  INSERT INTO #deductibleTable
  SELECT [agentID],[name],0,15000,25000,'2013-04-05' FROM [agent] WHERE [name] LIKE '%Investors%'

  --Add all other agents
  INSERT INTO #deductibleTable
  SELECT [agentID],[name],0,0,0,GETDATE() FROM [agent] WHERE [agentID] NOT IN (SELECT [agentID] FROM #deductibleTable)

  --Delete from deductible table
  IF @agentName <> ''
    DELETE FROM #deductibleTable WHERE [name] NOT LIKE '%' + @agentName + '%'

  CREATE TABLE #balanceTable
  (
  claimID INT,
  refCategory VARCHAR(10),
  asOfDate DATETIME,
  pendingInvoiceAmount DECIMAL(18,2),
  approvedInvoiceAmount  DECIMAL(18,2),
  completedInvoiceAmount  DECIMAL(18,2),
  pendingReserveAmount  DECIMAL(18,2),
  approvedReserveAmount DECIMAL(18,2),
  reserveBalance DECIMAL(18,2)
  )

  INSERT INTO #balanceTable
  EXEC [dbo].[spReportClaimBalances] @asOfDate = @asOfDate

  SELECT  [claimID],
          [policyID],
          [fileNumber],
          [category],
          [stat],
          [dateClosed],
          [stateID],
          [laePaid],
          [lossPaid],
          [totalPaid],
          [deductible],
          [dbo].GetMinimum([totalPaid],[deductible]) AS 'unadjusted',
          CASE WHEN [totalPaid] < @waivedAmount
           THEN 0
           ELSE [dbo].GetMaximum([dbo].GetMinimum([totalPaid],[deductible]) - [billedAmount],0)
          END AS 'billable',
          [billedAmount],
          [billedDate],
          [invoiceNumber],
          [paidAmount],
          [paidDate],
          [age],
          CASE WHEN [totalPaid] < @waivedAmount
           THEN [totalPaid]
           ELSE 0
          END AS 'deMinimus',
          [waivedAmount]
  FROM    (
          SELECT  c.[claimID],
                  p.[policyID],
                  c.[fileNumber],
                  -- We need to convert the varchar to a varchar because of the function ISNULL
                  -- Progress doesn't like ISNULL without converting to a varchar for some reason
                  CONVERT(VARCHAR,ISNULL(code.[category],'')) AS 'category',
                  (SELECT [objValue] FROM [sysprop] WHERE [appCode] = 'CLM' AND [objAction] = 'ClaimDescription' AND [objProperty] = 'Status' AND [objID] = c.[stat]) AS 'stat',
                  c.[dateClosed],
                  c.[stateID],
                  ISNULL(piv.[E],0) AS 'laePaid',
                  ISNULL(piv.[L],0) AS 'lossPaid',
                  ISNULL(piv.[E],0) + ISNULL(piv.[L],0) AS 'totalPaid',
                  CASE
                   WHEN a.[name] LIKE '%NETCO%' THEN a.[equalAmount]
                   WHEN a.[name] LIKE '%Investors%' THEN
                    CASE 
                     WHEN DATEDIFF(d,COALESCE(p.[effDate],cc.[effDate]),a.[effDate]) <= 0 THEN a.[lessAmount]
                     WHEN DATEDIFF(d,COALESCE(p.[effDate],cc.[effDate]),a.[effDate]) >  0 THEN a.[greaterAmount]
                    END
                   ELSE 0
                  END AS 'deductible',
                  r.[billedAmount],
                  r.[billedDate],
                  -- We need to convert the varchar to a varchar because of the function ISNULL
                  -- Progress doesn't like ISNULL without converting to a varchar for some reason
                  CONVERT(VARCHAR,r.[invoiceNumber]) AS 'invoiceNumber',
                  r.[paidDate],
                  r.[paidAmount],
                  r.[age],
                  r.[waivedAmount]
          FROM    [dbo].[claim] c INNER JOIN
                  (
                  SELECT  [claimID],
                          [coverageID],
                          [effDate]
                  FROM    [dbo].[claimcoverage]
                  WHERE   [seq] = 1
                  ) cc
          ON      c.[claimID] = cc.[claimID] INNER JOIN
                  [dbo].[policy] p
          ON      cc.[coverageID] = CONVERT(VARCHAR,p.[policyID]) INNER JOIN
                  #deductibleTable a
          ON      c.[agentID] = a.[agentID] INNER JOIN
                  (
                  SELECT  [claimID],
                          [refCategory],
                          [completedInvoiceAmount]
                  FROM    #balanceTable
                  ) AS b
                  PIVOT
                  (
                  SUM([completedInvoiceAmount])
                  FOR [refCategory] IN ([E],[L])
                  ) AS piv
          ON      c.[claimID] = piv.[claimID] INNER JOIN
                  (
                  SELECT  CONVERT(INTEGER,inv.[refID]) AS 'claimID',
                          ISNULL(SUBSTRING(
                          (
                              SELECT  ', ' + [invoiceNumber] AS [text()]
                              FROM    [arinv]
                              WHERE   [refID] = inv.[refID]
                              AND     [invoiceNumber] <> ''
                              FOR XML PATH ('')
                          ), 3, 1000),'') AS 'invoiceNumber',
                          SUM(trx.[originalAmount]) AS 'billedAmount',
                          SUM(trx.[completedAmount]) AS 'paidAmount',
                          SUM(inv.[waivedAmount]) AS 'waivedAmount',
                          MAX(inv.[dateRequested]) AS 'billedDate',
                          MAX(inv.[completeDate]) AS 'paidDate',
                          DATEDIFF(d,MAX(inv.[dateRequested]),MAX(COALESCE(inv.[completeDate],@asOfDate))) AS 'age'
                  FROM    [arinv] inv INNER JOIN
                          [dbo].[GetReceivableDetails] (@asOfDate) trx
                  ON      inv.[arinvID] = trx.[arinvID]
                  GROUP BY inv.[refID]
                  ) r
          ON      c.[claimID] = r.[claimID] LEFT OUTER JOIN
                  (
                  SELECT  cc2.[claimID],
                          SUBSTRING(
                          (
                            SELECT  ', ' + sc.[description] AS [text()]
                            FROM    [syscode] sc INNER JOIN
                                    [claimcode] cc
                            ON      cc.[code] = sc.[code]
                            AND     cc.[codeType] = sc.[codeType]
                            WHERE   cc.[claimID] = cc2.[claimID]
                            AND     sc.[codeType] = 'ClaimDescription'
                            ORDER BY cc.[claimID]
                            FOR XML PATH('')
                          ), 3, 1000) AS [category]
                  FROM    [claimcode] cc2
                  GROUP BY cc2.[claimID]
                  ) code
          ON      c.[claimID] = code.[claimID]
          ) a
  ORDER BY [claimID]

  DROP TABLE #deductibleTable
  DROP TABLE #balanceTable
END
