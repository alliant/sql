USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spReportAgentActivitiesYOY]    Script Date: 9/12/2018 12:43:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportAgentActivitiesYOY]
	-- Add the parameters for the stored procedure here
  @agentID VARCHAR(MAX) = 'ALL',
  @stateID VARCHAR(MAX) = 'ALL',
  @category VARCHAR(10) = 'N',
  @startYear INTEGER = 0,
  @endYear INTEGER = 0,
  @UID varchar(100)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;
  SET @agentID = [dbo].[StandardizeAgentID](@agentID);

  IF (@endYear = 0)
    SET @endYear = YEAR(GETDATE())

  IF (@startYear = 0)
    SET @startYear = @endYear - 1

  -- Get the state
  CREATE TABLE #stateTable ([stateID] VARCHAR(2))
  INSERT INTO #stateTable ([stateID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('State', @stateID)

  SELECT  a.[agentID],
          a.[stateID],
          a.[stat],
          ISNULL((SELECT [uid] FROM [agentmanager] WHERE [agentID] = a.[agentID] AND [isPrimary] = 1 AND [stat] = 'A'),'') AS [manager],
          CASE WHEN a.[corporationID] = '' THEN a.[name] ELSE a.[corporationID] END AS [corporationID],
          @category AS [category],
          ISNULL(aa.[type],'A') AS [type],
          ISNULL(aa.[prevMonth1],0)  AS [prevMonth1], 
          ISNULL(aa.[prevMonth2],0)  AS [prevMonth2], 
          ISNULL(aa.[prevMonth3],0)  AS [prevMonth3], 
          ISNULL(aa.[prevMonth4],0)  AS [prevMonth4], 
          ISNULL(aa.[prevMonth5],0)  AS [prevMonth5], 
          ISNULL(aa.[prevMonth6],0)  AS [prevMonth6], 
          ISNULL(aa.[prevMonth7],0)  AS [prevMonth7], 
          ISNULL(aa.[prevMonth8],0)  AS [prevMonth8], 
          ISNULL(aa.[prevMonth9],0)  AS [prevMonth9], 
          ISNULL(aa.[prevMonth10],0) AS [prevMonth10],
          ISNULL(aa.[prevMonth11],0) AS [prevMonth11],
          ISNULL(aa.[prevMonth12],0) AS [prevMonth12],
          ISNULL(aa.[currMonth1],0)  AS [currMonth1], 
          ISNULL(aa.[currMonth2],0)  AS [currMonth2], 
          ISNULL(aa.[currMonth3],0)  AS [currMonth3], 
          ISNULL(aa.[currMonth4],0)  AS [currMonth4], 
          ISNULL(aa.[currMonth5],0)  AS [currMonth5], 
          ISNULL(aa.[currMonth6],0)  AS [currMonth6], 
          ISNULL(aa.[currMonth7],0)  AS [currMonth7], 
          ISNULL(aa.[currMonth8],0)  AS [currMonth8], 
          ISNULL(aa.[currMonth9],0)  AS [currMonth9], 
          ISNULL(aa.[currMonth10],0) AS [currMonth10],
          ISNULL(aa.[currMonth11],0) AS [currMonth11],
          ISNULL(aa.[currMonth12],0) AS [currMonth12]
  FROM    [dbo].[agent] a LEFT OUTER JOIN
          (
          SELECT  [agentID],
                  [category],
                  [type],
                  SUM([prevMonth1])  AS [prevMonth1],
                  SUM([prevMonth2])  AS [prevMonth2],
                  SUM([prevMonth3])  AS [prevMonth3],
                  SUM([prevMonth4])  AS [prevMonth4],
                  SUM([prevMonth5])  AS [prevMonth5],
                  SUM([prevMonth6])  AS [prevMonth6],
                  SUM([prevMonth7])  AS [prevMonth7],
                  SUM([prevMonth8])  AS [prevMonth8],
                  SUM([prevMonth9])  AS [prevMonth9],
                  SUM([prevMonth10]) AS [prevMonth10],
                  SUM([prevMonth11]) AS [prevMonth11],
                  SUM([prevMonth12]) AS [prevMonth12],
                  SUM([currMonth1])  AS [currMonth1],
                  SUM([currMonth2])  AS [currMonth2],
                  SUM([currMonth3])  AS [currMonth3],
                  SUM([currMonth4])  AS [currMonth4],
                  SUM([currMonth5])  AS [currMonth5],
                  SUM([currMonth6])  AS [currMonth6],
                  SUM([currMonth7])  AS [currMonth7],
                  SUM([currMonth8])  AS [currMonth8],
                  SUM([currMonth9])  AS [currMonth9],
                  SUM([currMonth10]) AS [currMonth10],
                  SUM([currMonth11]) AS [currMonth11],
                  SUM([currMonth12]) AS [currMonth12]
          FROM    (
                  SELECT  [agentID],
                          [category],
                          [type],
                          [month1]  AS [prevMonth1],
                          [month2]  AS [prevMonth2],
                          [month3]  AS [prevMonth3],
                          [month4]  AS [prevMonth4],
                          [month5]  AS [prevMonth5],
                          [month6]  AS [prevMonth6],
                          [month7]  AS [prevMonth7],
                          [month8]  AS [prevMonth8],
                          [month9]  AS [prevMonth9],
                          [month10] AS [prevMonth10],
                          [month11] AS [prevMonth11],
                          [month12] AS [prevMonth12],
                          0         AS [currMonth1],
                          0         AS [currMonth2],
                          0         AS [currMonth3],
                          0         AS [currMonth4],
                          0         AS [currMonth5],
                          0         AS [currMonth6],
                          0         AS [currMonth7],
                          0         AS [currMonth8],
                          0         AS [currMonth9],
                          0         AS [currMonth10],
                          0         AS [currMonth11],
                          0         AS [currMonth12]
                  FROM    [dbo].[GetActivityTable] ('ALL', @startYear)
                  UNION ALL
                  SELECT  [agentID],
                          [category],
                          [type],
                          0         AS [prevMonth1],
                          0         AS [prevMonth2],
                          0         AS [prevMonth3],
                          0         AS [prevMonth4],
                          0         AS [prevMonth5],
                          0         AS [prevMonth6],
                          0         AS [prevMonth7],
                          0         AS [prevMonth8],
                          0         AS [prevMonth9],
                          0         AS [prevMonth10],
                          0         AS [prevMonth11],
                          0         AS [prevMonth12],
                          [month1]  AS [currMonth1],
                          [month2]  AS [currMonth2],
                          [month3]  AS [currMonth3],
                          [month4]  AS [currMonth4],
                          [month5]  AS [currMonth5],
                          [month6]  AS [currMonth6],
                          [month7]  AS [currMonth7],
                          [month8]  AS [currMonth8],
                          [month9]  AS [currMonth9],
                          [month10] AS [currMonth10],
                          [month11] AS [currMonth11],
                          [month12] AS [currMonth12]
                  FROM    [dbo].[GetActivityTable] ('ALL', @endYear)
                  ) aa
          WHERE   [category] = @category
          GROUP BY [agentID],[category],[type]
          ) aa
  ON      a.[agentID] = aa.[agentID] INNER JOIN
          #stateTable st
  ON      a.[stateID] = st.[stateID]
  WHERE (a.[agentID] = (CASE  WHEN @agentID != 'ALL' THEN @agentID ELSE a.[agentID] END)) 
  AND   (dbo.CanAccessAgent(@UID ,a.[agentID]) = 1)
  ORDER BY [agentID]

  DROP TABLE #stateTable
END
