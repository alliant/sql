GO
/****** Object:  StoredProcedure [dbo].[spGetIDVerificationRate]    Script Date: 2/18/2025 12:08:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetIDVerificationRate]
	-- Add the parameters for the stored procedure here
     @agentID VARCHAR(max) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	CREATE TABLE #ttidverificationrate
	(
	  [Agent]  varchar(max),
	  [Rate]   decimal,
	  [Tax]    decimal
	  )

    -- Insert statements for procedure here
	insert into #ttidverificationrate (Rate,Tax,Agent)
	select coverageAmt,deductibleAmt, (agent.name + ' (' + agent.agentID + ')' )  from qualification 
	left outer join fulfillment on fulfillment.qualificationID = qualification.qualificationID
	left outer join agent on agent.agentID = fulfillment.entityID 
	where fulfillment.entityID = (CASE WHEN @agentID = '' THEN fulfillment.entityID ELSE @agentID END)	
	and   fulfillment.entity = 'Agent'
	and  qualification.insuranceBroker = 'SMT'
	and ((qualification.effectivedate is null) or (qualification.effectivedate < GETDATE() ))
    and ((qualification.expirationDate is null) or (qualification.expirationDate > GETDATE()) )

	select * from #ttidverificationrate
	drop table #ttidverificationrate
END
