USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spReportAgentGrpClaimCostsSumm]    Script Date: 7/28/2021 7:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportAgentGrpClaimCostsSumm]
	-- Add the parameters for the stored procedure here
    @endDate DATETIME = NULL,
    @year INT = 0,
	@stateID     VARCHAR(MAX) = '',
	@YearOfSigning integer = 0,
	@Software VARCHAR(MAX) = '',
	@Company VARCHAR(MAX) = '',
	@Organization VARCHAR(MAX) = '',
	@Manager VARCHAR(MAX) = '',
	@TagList VARCHAR(MAX) = '',
	@AffiliationList VARCHAR(MAX) = '',
	@uid varchar(100) = ''
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
  SET NOCOUNT ON;
  
  DECLARE @agentID VARCHAR(30)
  
  CREATE TABLE #claimcostssumm (
	     			       agentID      VARCHAR(80),
						   period       INTEGER,
						   startDate    DATETIME,
						   endDate      DATETIME,
						   netPremium   [decimal](10, 2) NULL,
						   claimCosts   [decimal](10, 2) NULL,
						   costRatio    [decimal](10, 2) NULL,
						   claimCostsAE [decimal](10, 2) NULL,
						   costRatioAE  [decimal](10, 2) NULL
						   )
  CREATE TABLE #claimcostssummFiltered (
	     			       agentID      VARCHAR(80),
						   period       INTEGER,
						   startDate    DATETIME,
						   endDate      DATETIME,
						   netPremium   [decimal](10, 2) NULL,
						   claimCosts   [decimal](10, 2) NULL,
						   costRatio    [decimal](10, 2) NULL,
						   claimCostsAE [decimal](10, 2) NULL,
						   costRatioAE  [decimal](10, 2) NULL
						   )
  CREATE TABLE #agentsGroup
   (
   [agentID] VARCHAR(20),
   [corporationID] VARCHAR(MAX),  
   [stateID] VARCHAR(2),
   [stateName] VARCHAR(20), 
   [stat] VARCHAR(1),
   [name] VARCHAR(MAX),
   [contractDate] datetime,
   [swVendor] VARCHAR(100),
   [manager] VARCHAR(MAX),
   [orgID] VARCHAR(50),
   [orgRoleID] integer, 
   [orgName] VARCHAR(Max)
   )	

INSERT INTO #agentsGroup ([agentID] , [corporationID]  , [stateID] , [stateName] , [stat], [name], [contractDate] , [swVendor], [manager], [orgID] , [orgRoleID] , [orgName]  )
Exec SpGetConsolidatedAgents @StateID ,
	@YearOfSigning ,
	@Software,
	@Company  ,
	@Organization ,
    @Manager  ,
	@TagList ,
	@AffiliationList  ,
	@UID 

--Get claimcostssumm of each agent
  INSERT INTO #claimcostssumm ( [agentID],[period],[startDate],[endDate],[netPremium],[claimCosts],[costRatio],[claimCostsAE],[costRatioAE])
    EXEC [dbo].[spReportClaimCostsSummary] @agentID = 'ALL', @endDate = NULL, @year = 0
	
  INSERT INTO #claimcostssummFiltered  select * from  #claimcostssumm ccs  where 
         (ccs.[agentID]  IN (SELECT [agentID] FROM #agentsGroup)) 
 
  SELECT * FROM #claimcostssummFiltered
  DROP TABLE #claimcostssumm
  DROP TABLE #claimcostssummFiltered
  DROP TABLE #agentsGroup
END
