-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
USE [COMPASS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Rahul Sharma>
-- Create date: <28-11-2019>
-- Description:	<Get sysnote records>
-- =============================================
CREATE PROCEDURE [dbo].[spGetSysNote] 
	-- Add the parameters for the stored procedure here
    @pcUID       VARCHAR(100) = 'ALL',
    @pcEntity    VARCHAR(100) = 'ALL',
    @pcEntityID  VARCHAR(50) = 'ALL'

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    
	-- Insert statements for procedure here

	-- Temporary table definition
    CREATE TABLE #sysnote (sysNoteID      INTEGER,			            
                           seq            INTEGER,
		    		       uid            VARCHAR(100),
						   entity         VARCHAR(100),
						   entityID       VARCHAR(50),
						   notes          VARCHAR(max),
						   dateCreated    DATETIME,
						   dateModified   DATETIME,
						   userName       VARCHAR(100))

    
	INSERT INTO #sysnote(sysNoteID,
                         seq,
				     	 uid,
						 entity,
						 entityID,
						 dateCreated,
						 dateModified,
						 notes)  
						 	     	
      SELECT sysnote.sysNoteID,
	         sysnote.seq,
		     sysnote.uid,
		     sysnote.entity,
		     sysnote.entityID,
		     sysnote.dateCreated,
		     sysnote.dateModified,
		     sysnote.notes
        FROM sysnote
        WHERE sysnote.entity    = (CASE WHEN @pcEntity   = 'ALL' THEN sysnote.entity    ELSE @pcEntity    END) 
          AND sysnote.entityID  = (CASE WHEN @pcEntityID = 'ALL' THEN sysnote.entityID  ELSE @pcEntityID  END)           
          AND sysnote.uid       = (CASE WHEN @pcUID      = 'ALL' THEN sysnote.uid       ELSE @pcUID       END)
     
    UPDATE #sysnote
      SET #sysnote.username = sysuser.name
      FROM #sysnote
    INNER JOIN sysuser ON (#sysnote.uid = sysuser.uid)
 	
    SELECT * FROM #sysnote
END
GO