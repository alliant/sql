-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetSysUsers]
	-- Add the parameters for the stored procedure here
	@ipAction    VARCHAR(1) = 'B',
	@ipUID       VARCHAR(100) = 'ALL',
	@ipStartDate DATETIME     = NULL,
	@ipEndDate   DATETIME     = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    --Create temp table to store all uniqe UIDs
	CREATE TABLE #ttUser    (uid VARCHAR(100))
	CREATE TABLE #ttAllUser (uid VARCHAR(100))
    
   -- Return users which are active within given date range
	IF (@ipAction = 'A') 
	  BEGIN
	    --Insert all uniqe Users in #ttUser table called with in date range 
	    INSERT INTO #ttUser SELECT DISTINCT syslog.uid FROM syslog 
	                              WHERE syslog.createdate >= @ipStartDate 
	                                AND syslog.createdate <= @ipendDate 
	  END
   -- Return users which are not active within given date range
	ELSE IF(@ipAction = 'I')
	  BEGIN
	    --Insert all uniqe Users in #sysLog table as per selection criteria
	    INSERT INTO #ttUser SELECT DISTINCT sysLog.uid FROM syslog 
	                          WHERE sysLog.createDate >= @ipStartDate 
	                            AND sysLog.createDate <= @ipEndDate  

        INSERT INTO #ttAllUser SELECT sysUser.uid from sysUser where isActive = 1
        -- Delete all records from #ttSysUser not persent in #sysLog
        DELETE #ttAllUser FROM #ttAllUser inner join #ttUser ON #ttAllUser.uid = #ttUser.uid

        DELETE FROM #ttUser

	    INSERT INTO #ttUser SELECT #ttAllUser.uid from  #ttAllUser
		                      
	  END
   -- Return all users as per search criteria
	ELSE
	  BEGIN
	    INSERT INTO #ttUser SELECT sysUser.uid from sysUser  
		                       WHERE sysUser.uid = (CASE WHEN @ipUID  = 'ALL' THEN sysUser.uid   ELSE @ipUID END)  
	  END                    

    --Return all sysuser table records as per #ttUser table
    SELECT sysuser.uid,
	       sysuser.name,
		   sysuser.initials,
		   sysuser.email,
		   sysuser.role,           
		   '' AS [password],
		   sysuser.isActive,
		   sysuser.createDate,
		   sysuser.passwordSetDate,
		   sysuser.passwordExpired,
		   sysuser.comments,
		   sysuser.department,
		   sysuser.vendorid,
       ISNULL(sysuser.outOfOffice,0) AS [outOfOffice],
       sysuser.outStartDate,
       sysuser.outEndDate
		    from sysuser join #ttUser on sysuser.uid = #ttUser.uid order by uid
END
GO

