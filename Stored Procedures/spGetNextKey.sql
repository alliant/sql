USE [COMPASS_DVLP]
GO
/****** Object:  StoredProcedure [dbo].[spGetNextKey]    Script Date: 5/11/2021 2:00:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetNextKey]
  @keyType VARCHAR(50),
	@key INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

  SELECT @key = [seq] + 1 FROM [dbo].[syskey] WHERE [type] = @keyType

  IF (@key IS NOT NULL)
  BEGIN
    UPDATE  [dbo].[syskey]
    SET     [seq] = @key
    WHERE   [type] = @keyType
  END
END
