USE [COMPASS_ALFA]
GO
/****** Object:  StoredProcedure [dbo].[spGetRegionReportedNPROlap]    Script Date: 3/16/2023 8:02:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetRegionReportedNPROlap]
	-- Add the parameters for the stored procedure here
  @year varchar(4),
  @month varchar(14),
  @olapDataSource varchar(100),
  @olapCatalog varchar(100),
  @lag varchar(4),
  @UID varchar(100) = ''
AS
BEGIN

declare @sql varchar(max)

create table  #tempdataset
(
yearname varchar(4),
monthname varchar(50) ,
region varchar(20),
NoOfCPLReported decimal(18,2),
NoOfPolicyReported decimal(18,2), 
netpremium decimal(18,2)

)

if(@year = '')  or (@year= NULL)
  set @year = year(getdate())

if(@month= '')  or (@month= NULL)
  set @month = month(getdate())

if(@lag= '')  or (@lag= NULL)
  set @lag = 12

/*
 SELECT {[Measures].[No Of CPL Reported],[Measures].[Policies reported],[Measures].[Net Premium]} 
ON COLUMNS , NON EMPTY CrossJoin(Hierarchize({DrilldownLevel({[Date].[Month].[All]})}),Hierarchize({DrilldownLevel({[Agent].[Region].[All]})})) 
ON ROWS  FROM [NPR] WHERE ([Date].[Year].&[2022])
*/

set @sql = 
'
    insert into #tempdataset
	Select a.* FROM OpenRowset
	   (
	        ''MSOLAP'',''DATASOURCE=' + @olapDataSource + '; Initial Catalog=' + @olapCatalog + ';'' ,
       ''
			 SELECT NON EMPTY {[Measures].[No Of CPL Reported],[Measures].[Policies reported],[Measures].[Net Premium]} 
			ON COLUMNS , NON EMPTY { (  {[Date].[Calendar].[Months].&[' + @month + ']&[' + @Year + '].lag(' + @lag  + ') : [Date].[Calendar].[Months].&[' + @month + ']&[' + @Year + '] } * [Agent].[Region].[Region].ALLMEMBERS ) }   
			ON ROWS  FROM [NPR]
  	  '' 
	  ) as a 
'

Begin try
	EXEC (@sql)
End try
Begin catch
	select error_message()
	print 'No Data'
End catch
 
select * from #tempdataset

drop table #tempdataset
 
END