GO
/****** Object:  StoredProcedure [dbo].[spAlertClaimsRatio]    Script Date: 7/5/2017 12:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spAlertClaimsRatio]
  @agentID VARCHAR(MAX) = '',
  @user VARCHAR(100) = 'compass@alliantnational.com',
  @preview BIT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;
  
  CREATE TABLE #agentTable ([agentID] VARCHAR(30))
  INSERT INTO #agentTable ([agentID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)

  DECLARE @startDate DATETIME = NULL,
          @endDate DATETIME = NULL,
          @date DATETIME = NULL,
          @alertID INTEGER = 0,
          -- Used for the preview
          @source VARCHAR(30) = 'CLM',
          @processCode VARCHAR(30) = 'CLM01',
          @score DECIMAL(18,2) = 0,
          -- Notes for the alert note
          @note VARCHAR(MAX) = ''
          
  SELECT @endDate = MAX([endDate]) FROM [dbo].[period] WHERE [active] = 0
  SET @startDate = DATEADD(m, DATEDIFF(m, 0, DATEADD(yy,-3,@endDate)), 0)
  SELECT @date = @endDate

  DECLARE @insertClaimsRatio [dbo].[ClaimsRatio]
  SELECT * INTO #insertClaimsRatio FROM @insertClaimsRatio

  -- Get the claims ratio
  EXEC [dbo].[spReportClaimsRatio] @startDate = @startDate, @endDate = @endDate

  DELETE
  FROM    #insertClaimsRatio
  WHERE   [agentID] NOT IN (SELECT [agentID] FROM #agentTable)

  UPDATE  #insertClaimsRatio
  SET     [actualCostRatio] = 0
  WHERE   [actualCostRatio] < 0

  IF (@preview = 0)
  BEGIN
    -- Actual Costs Ratio
    DECLARE cur CURSOR LOCAL FOR
    SELECT  [agentID],
            [actualCostRatio],
            'The calculation is (LAE Completed Payments (' + [dbo].[FormatNumber] ([laeComplete], 0) + ') + Loss Completed Payments (' + [dbo].[FormatNumber] ([lossComplete], 0) + ') - Recoveries (' + [dbo].[FormatNumber] ([recoveries], 0) + ')) / Net Premium (' + [dbo].[FormatNumber] ([netPremium], 0) + ')'
    FROM    #insertClaimsRatio

    OPEN cur
    FETCH NEXT FROM cur INTO @agentID, @score, @note
    WHILE @@FETCH_STATUS = 0 
    BEGIN
      EXEC [dbo].[spInsertAlert] @source = @source, 
                                 @processCode = @processCode, 
                                 @user = @user, 
                                 @agentID = @agentID, 
                                 @score = @score,
                                 @effDate = @date,
                                 @note = @note
      FETCH NEXT FROM cur INTO @agentID, @score, @note
    END
    CLOSE cur
    DEALLOCATE cur
  END
  
  SELECT  [agentID] AS [agentID],
          @source AS [source],
          @processCode AS [processCode],
          [dbo].[GetAlertThreshold] (@processCode, [actualCostRatio]) AS [threshold],
          [dbo].[GetAlertThresholdRange] (@processCode, [actualCostRatio]) AS [thresholdRange],
          [dbo].[GetAlertSeverity] (@processCode, [actualCostRatio]) AS [severity],
          [actualCostRatio] AS [score],
          [dbo].[GetAlertScoreFormat] (@processCode, [actualCostRatio]) AS [scoreDesc],
          @date AS [effDate],
          [dbo].[GetAlertOwner] (@processCode) AS [owner],
          'The calculation is (LAE Completed Payments (' + [dbo].[FormatNumber] ([laeComplete], 0) + ') + Loss Completed Payments (' + [dbo].[FormatNumber] ([lossComplete], 0) + ') - Recoveries (' + [dbo].[FormatNumber] ([recoveries], 0) + ')) / Net Premium (' + [dbo].[FormatNumber] ([netPremium], 0) + ')' AS [note]
  FROM    #insertClaimsRatio
  ORDER BY [agentID]

  DROP TABLE #agentTable
  DROP TABLE #insertClaimsRatio
END
