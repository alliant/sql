/****** Object:  StoredProcedure [dbo].[spProductionFileStatement]    Script Date: 8/2/2024 10:19:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spProductionFileStatement] 
	-- Add the parameters for the stored procedure here
	@agentID         VARCHAR(MAX) = 'ALL',
	@startDate       DATETIME     = null,
	@endDate         DATETIME     = null
	
as
BEGIN
	-- set NOCOUNT on added to prevent extra result sets from
	-- interfering with select statements.
	set NOCOUNT on;

	create table #agentFile (agentfileid  varchar(80),
						     agentID      varchar(80),
	                         fileNumber   varchar(50),
		        		     fileID       varchar(50),
						     arAmount     [decimal](17, 2) null,
						     periodCount  integer,						
						     tranDate     date)
								 
	create table #fileInvoice (agentFileID   varchar(80),
                               fileInvID     integer,
                               description   varchar(100),
                               invoiceAmount [decimal](18,2) null,
							   seq           integer,
                               )
							   
	create table #filePayments (agentID       varchar(80),
	                            fileID        varchar(80),
								fileNumber    varchar(50),
								sourceID      varchar(50),
							    sourceType    varchar(50),
                                paymentAmt    [decimal](17, 2) null,
								type          varchar(50),
                                receviedDate  datetime,                                   
                                checkNumber   varchar(100),
						        checkAmt      [decimal](17,2) null,
								refundAmt     [decimal](17,2) null,
								remainingAmt  [decimal](17,2) null
                                )
	
	create table #fileStatement (agentFileID   varchar(80),
	                             fileNumber    varchar(50),
								 arAmount      [decimal](17, 2) null,
						         periodCount   integer,
                                 tranDate      datetime,
                                 fileInvID     integer,
                                 description   varchar(100),
                                 invoiceAmount [decimal](18,2) null,
								 seq           integer,
	                             agentID       varchar(80),
	                             fileID        varchar(80),
								 sourceID      varchar(50),
							     sourceType    varchar(50),
								 type          varchar(50),
                                 paymentAmt    [decimal](17, 2) null,
                                 receviedDate  datetime,                                   
                                 checkNumber   varchar(100),
						         checkAmt      [decimal](17,2) null,
								 refundAmt     [decimal](17,2) null,
								 remainingAmt  [decimal](17,2) null
                                 )

	insert into #agentFile ([agentfileid],[agentID],[fileID],[arAmount],[periodCount],[tranDate])

    exec [dbo].[spFileswithBalanceorActivity] @startDate = @startDate, @endDate = @endDate, @agentID = @agentID 
	
	update #agentFile set fileNumber = (select fileNumber from agentfile where agentfile.agentFileID = #agentFile.agentfileid)
	delete #agentFile where agentfileid not in (select agentfileid from agentfile)

	/*Now pick the file Invoice Items from fileInv and fileInvItem table for files #fileFulfilled*/
	insert into #fileInvoice (agentFileID,fileInvID,description,invoiceAmount,seq)
	select fi.agentfileid,
	       fi.fileinvid,
		   fii.description,
		   fii.amount,
		   fii.seq
	       from fileInv fi
	inner join fileInvitem fii on (fii.fileinvid = fi.fileinvid)
	inner join #agentFile ff on (ff.agentFileID = fi.agentFileID)
	where fi.invoiceDate is not null
	  and (fi.invoiceDate >= cast(@startDate as date) and fi.invoiceDate <= cast(@endDate as date))
	
	/*Now fetch the payments applied to the production files*/
	insert into #filePayments ([agentID],[fileID],[sourceType],[sourceID],[paymentAmt],[type])
	select ff.agentID,
	       ff.fileID,
		   at.sourceType,
		   at.sourceID,
		   sum(at.tranAmt),
		   at.type
	  from #agentFile ff
	  left outer join
	  artran at
	  on (at.entityID   = ff.agentID and at.fileID    = ff.fileID)
	  where (at.type = 'A' and at.transType = 'P')
      and (at.tranDate >= cast(@startDate as date) 
	  and at.tranDate <= cast(@endDate as date))
      group by ff.[agentID],ff.[fileID],at.[sourceType],at.[sourceID],at.[type]

	update #filePayments 
      set #filePayments.receviedDate = at.tranDate,
	      #filePayments.checkAmt     = at.tranAmt,
		  #filePayments.checkNumber  = at.reference      
	  from artran at
	  where at.arTranID = cast(#filePayments.sourceID as int)

	/*Now fetch the adjustments amount to the production files*/
	insert into #filePayments (agentID,fileID,paymentAmt,type)
	select fa.agentID,
	       fa.fileID,
	       sum(fa.amount),
	       fa.type
	from #agentFile ff 
	left outer join fileAr fa on fa.agentFileID = ff.agentFileID
	where fa.type = 'W' and fa.tranDate <= @endDate
	group by fa.[agentID],fa.[fileID],fa.[type]

	/*Now fetch the unapplied payments/open payments to the production files*/
	insert into #filePayments ([agentID],[fileID],[type],[paymentAmt],[remainingAmt],[refundAmt]) 
	select at.entityID,
	       at.fileID,
		   at.type,
		   sum(case when ((at.void = 0 or (at.voidDate > @endDate or at.type = 'P'))
			         and  at.tranDate <= @endDate)
			         then  at.tranAmt else 0 end),
		   at.remainingAmt,
		   sum(case when (at.type = 'P' and at.seq > 0 and at.tranDate <= @endDate)
			        then at.tranAmt else 0 end)
	       from artran at
	       where at.type = 'P' and at.remainingAmt > 0.00
	       and at.[entityID] = (case when @agentID = 'ALL' then at.entityID else @agentID end)
	       group by at.entityID,at.fileID,at.type,at.remainingAmt

    update #filePayments	
      set  #filePayments.fileNumber   = at.fileNumber,
	       #filePayments.receviedDate = at.tranDate,
	       #filePayments.checkAmt     = at.tranAmt,
		   #filePayments.checkNumber  = at.reference 
	  from artran at
      where at.entityID        = #filePayments.agentID
	    and at.fileID          = #filePayments.fileID
		and at.type            = #filePayments.type
        and #filePayments.type = 'P' 
   
	insert into #fileStatement (agentFileID,agentID,fileID,fileNumber ,arAmount,periodCount,tranDate ,fileInvID ,description ,invoiceAmount,seq,
	sourceID,sourceType ,type, paymentAmt , receviedDate ,checkNumber ,checkAmt)

	select af.agentfileid,af.agentID,af.fileID,af.fileNumber ,af.arAmount,af.periodCount,af.tranDate,fi.fileInvID,fi.description,fi.invoiceAmount,fi.seq,
	fp.sourceID,fp.sourceType ,fp.type, fp.paymentAmt , fp.receviedDate ,fp.checkNumber ,fp.checkAmt
	from #agentFile af 
	inner join #fileInvoice fi on fi.agentFileID = af.agentfileid
	inner join #filePayments fp on fp.agentID = af.agentID and fp.fileID = af.fileID
	where fp.type <> 'P'

	insert  into #fileStatement (agentID,fileID,fileNumber,sourceID,sourceType ,type, paymentAmt, receviedDate,checkNumber ,checkAmt ,remainingAmt,refundAmt)
	select agentID,fileID,fileNumber,sourceID,sourceType ,type, paymentAmt, receviedDate,checkNumber ,checkAmt ,remainingAmt,refundAmt
	from #filePayments fp where fp.type = 'P'

	select * from #fileStatement
end
