GO
/****** Object:  StoredProcedure [dbo].[spGetSysDest]    Script Date: 1/31/2020 11:33:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetSysDest]
	-- Add the parameters for the stored procedure here
  @entity VARCHAR(30) = 'ALL',
  @entityID VARCHAR(30) = 'ALL',
  @destination VARCHAR(30) = 'ALL',
  @action VARCHAR(30) = 'ALL'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  IF @entity = ''
    SET @entity = 'ALL'

  IF @action = ''
    SET @action = 'ALL'
	
  IF @destination = ''
    SET @destination = 'ALL'

  -- Insert statements for procedure here
	SELECT  [destID],
          [entityType],
          (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'SYS' AND [objAction] = 'Destination' AND [objProperty] = 'Entity' AND [objID] = [entityType]) AS [entityDesc],
          [entityID],
          CASE [entityType]
            WHEN 'G' THEN ISNULL((SELECT [name] FROM [dbo].[agent] WHERE [agentID] = [entityID]),'Unknown')
            WHEN 'C' THEN 'Alliant National'
            WHEN 'A' THEN ISNULL((SELECT [name] FROM [dbo].[sysuser] WHERE [uid] = [entityID]),'Unknown')
            ELSE 'Unknown Entity'
          END AS [entityName],
          [action],
          (SELECT [name] FROM [dbo].[sysaction] WHERE  [sysaction].[action] = [sysdest].[action] AND [isDestination] = 1) AS [actionDesc],
          [destType],
          (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'SYS' AND [objAction] = 'Destination' AND [objProperty] = 'Type' AND [objID] = [destType]) AS [destTypeDesc],
          [destName],
          CASE [destType]
            WHEN 'E' THEN ISNULL((SELECT [name] FROM [sysuser] WHERE [uid] = [destName]), [destName])
            WHEN 'S' THEN ISNULL((SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'SYS' AND [objAction] = 'DestType' AND [objProperty] = 'S' AND [objID] = [destName]), [destName])
            ELSE [destName]
          END AS [destNameDesc]
          
  FROM    [dbo].[sysdest]
  WHERE   [entityType] = CASE WHEN @entity = 'ALL' THEN [entityType] ELSE @entity END
  AND     [entityID] = CASE WHEN @entityID = 'ALL' THEN [entityID] ELSE @entityID END
  AND     [action] = CASE WHEN @action = 'ALL' THEN [action] ELSE @action END
  AND     [destType] = CASE WHEN @destination = 'ALL' THEN [destType] ELSE @destination END
END
