GO
/****** Object:  StoredProcedure [dbo].[spGetAgentsForCRM]    Script Date: 3/9/2017 9:45:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetAgentSummaries]
	-- Add the parameters for the stored procedure here
  @agentID VARCHAR(MAX) = 'ALL',
  @stateID VARCHAR(200) = 'ALL',
  @startDate DATETIME = NULL,
  @UID VARCHAR(100) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET @agentID = [dbo].[StandardizeAgentID](@agentID);

  CREATE TABLE #stateTable ([stateID] VARCHAR(2))
  INSERT INTO #stateTable ([stateID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('State', @stateID)

  DECLARE @sql NVARCHAR(MAX),
          @period1 VARCHAR(10) = '',
          @period2 VARCHAR(10) = '',
          @period3 VARCHAR(10) = '',
          @type VARCHAR(10) = '',
          @category VARCHAR(10) = '',
          @id VARCHAR(10) = '',
          @index INTEGER = 1,
          @list VARCHAR(1000),
          @column VARCHAR(30)

  IF (@startDate IS NULL)
    SET @startDate = GETDATE()

  SET @period1 = CONVERT(VARCHAR,YEAR(DATEADD(m,0,@startDate))) + RIGHT('00'+CONVERT(VARCHAR,MONTH(DATEADD(m,0,@startDate))),2)
  SET @period2 = CONVERT(VARCHAR,YEAR(DATEADD(m,-1,@startDate))) + RIGHT('00'+CONVERT(VARCHAR,MONTH(DATEADD(m,-1,@startDate))),2)
  SET @period3 = CONVERT(VARCHAR,YEAR(DATEADD(m,-2,@startDate))) + RIGHT('00'+CONVERT(VARCHAR,MONTH(DATEADD(m,-2,@startDate))),2)
  
  CREATE TABLE #scoreTable ([agentID] VARCHAR(20), [qarDate] DATETIME, [qarScore] INTEGER, [errScore] INTEGER)
  CREATE TABLE #batchTable ([agentID] VARCHAR(20), [id] VARCHAR(20), [month1Data] DECIMAL(18,2), [month2Data] DECIMAL(18,2), [month3Data] DECIMAL(18,2))
  
  --Insert the scores
  INSERT INTO #scoreTable ([agentID], [qarDate], [qarScore], [errScore])
  SELECT  q.[agentID],
          q2.[qarDate],
          ISNULL(q.[score],0) AS [qarScore],
          ISNULL(s.[score],0) AS [errScore]
  FROM    [dbo].[qar] q INNER JOIN
          [dbo].[agent] a
  ON      q.[agentID] = a.[agentID] INNER JOIN
          (
          SELECT  [agentID],
                  MAX([auditFinishDate]) AS [qarDate]
          FROM    [dbo].[qar]
          WHERE   [stat] = 'C'
          GROUP BY [agentID]
          ) q2
  ON      q.[agentID] = q2.[agentID]
  AND     q.[auditFinishDate] = q2.[qarDate] LEFT OUTER JOIN
          [dbo].[qarsection] s
  ON      q.[qarID] = s.[qarID]
  AND     s.[sectionID] = 6
  WHERE   q.[stat] = 'C'

  --Loop through the number of columns
  SELECT @list = COALESCE(@list + ',', '') + [objID] FROM [dbo].[sysprop] WHERE [appCode] = 'AMD' AND [objAction] = 'Main' AND [objProperty] = 'View'
  
  WHILE @index <= [dbo].[GetNumEntries] (@list, ',')
  BEGIN
    --Get the data
    SET @id = [dbo].[GetEntry] (@index, @list, ',')
    SELECT @column=[objName],@type=[objDesc],@category=[objRef] FROM [dbo].[sysprop] WHERE [appCode] = 'AMD' AND [objAction] = 'Main' AND [objProperty] = 'View' AND [objID] = @id

    --Insert the batch figures
    SET @sql = 
        CASE @type
          WHEN 'gap' THEN
            N'SELECT  p.[agentID],
                      ''' + @id + ''' AS [id],
                      p.[' + @period1 + '] AS [month1Data],
                      p.[' + @period2 + '] AS [month2Data],
                      p.[' + @period3 + '] AS [month3Data]
              FROM    (
                      SELECT  b.[agentID],
                              b.[periodID],
                              SUM(b.[dayCnt]) / CONVERT(DECIMAL,COUNT(b.[batchID])) AS [monthData]
                      FROM    (
                              SELECT  DISTINCT
                                      b.[batchID],
                                      b.[agentID],
                                      bf.[policyID],
                                      b.[periodID],
                                      DATEDIFF(day,bf.[effDate],b.[receivedDate]) AS [dayCnt]
                              FROM    [dbo].[batch] b INNER JOIN
                                      [dbo].[batchform] bf
                              ON      b.[batchID] = bf.[batchID]
                              WHERE   b.[periodID] = ' + @period1 + '
                              ) b
                      GROUP BY b.[agentID],
                               b.[periodID]
                      UNION ALL
                      SELECT  b.[agentID],
                              b.[periodID],
                              SUM(b.[dayCnt]) / CONVERT(DECIMAL,COUNT(b.[batchID])) AS [monthData]
                      FROM    (
                              SELECT  DISTINCT
                                      b.[batchID],
                                      b.[agentID],
                                      bf.[policyID],
                                      b.[periodID],
                                      DATEDIFF(day,bf.[effDate],b.[receivedDate]) AS [dayCnt]
                              FROM    [dbo].[batch] b INNER JOIN
                                      [dbo].[batchform] bf
                              ON      b.[batchID] = bf.[batchID]
                              WHERE   b.[periodID] = ' + @period2 + '
                              ) b
                      GROUP BY b.[agentID],
                               b.[periodID]
                      UNION ALL
                      SELECT  b.[agentID],
                              b.[periodID],
                              SUM(b.[dayCnt]) / CONVERT(DECIMAL,COUNT(b.[batchID])) AS [monthData]
                      FROM    (
                              SELECT  DISTINCT
                                      b.[batchID],
                                      b.[agentID],
                                      b.[periodID],
                                      bf.[policyID],
                                      DATEDIFF(day,bf.[effDate],b.[receivedDate]) AS [dayCnt]
                              FROM    [dbo].[batch] b INNER JOIN
                                      [dbo].[batchform] bf
                              ON      b.[batchID] = bf.[batchID]
                              WHERE   b.[periodID] = ' + @period3 + '
                              ) b
                      GROUP BY b.[agentID],
                               b.[periodID]
                      ) src
                      PIVOT
                      (
                      SUM([monthData])
                      FOR [periodID] in ([' + @period1 + '],[' + @period2 + '],[' + @period3 + '])
                      ) p'
          WHEN 'count' THEN
            CASE @id
              WHEN 'PC' THEN
                N'SELECT  p.[agentID],
                          ''' + @id + ''' AS [id],
                          p.[' + @period1 + '] AS [month1Data],
                          p.[' + @period2 + '] AS [month2Data],
                          p.[' + @period3 + '] AS [month3Data]
                  FROM    (
                          SELECT  a.[agentID],
                                  b.[periodID],
                                  bf.[' + @column + '] AS [monthData]
                          FROM    [dbo].[agent] a LEFT OUTER JOIN
                                  [dbo].[batch] b
                          ON      b.[agentID] = a.[agentID] INNER JOIN
                                  [dbo].[batchform] bf
                          ON      b.[batchID] = bf.[batchID]
                          WHERE   b.[periodID] IN (' + @period1 + ',' + @period2 + ',' + @period3 + ')
                          AND     bf.[formType] = ''P''
                          ) src
                          PIVOT
                          (
                          COUNT([monthData])
                          FOR [periodID] in ([' + @period1 + '],[' + @period2 + '],[' + @period3 + '])
                          ) p'
              ELSE
                N'SELECT  p.[agentID],
                          ''' + @id + ''' AS [id],
                          p.[' + @period1 + '] AS [month1Data],
                          p.[' + @period2 + '] AS [month2Data],
                          p.[' + @period3 + '] AS [month3Data]
                  FROM    (
                          SELECT  DISTINCT
                                  a.[agentID],
                                  b.[periodID],
                                  bf.[' + @column + '] AS [monthData]
                          FROM    [dbo].[agent] a LEFT OUTER JOIN
                                  [dbo].[batch] b
                          ON      b.[agentID] = a.[agentID] INNER JOIN
                                  [dbo].[batchform] bf
                          ON      b.[batchID] = bf.[batchID]
                          WHERE   b.[periodID] IN (' + @period1 + ',' + @period2 + ',' + @period3 + ')
                          ) src
                          PIVOT
                          (
                          COUNT([monthData])
                          FOR [periodID] in ([' + @period1 + '],[' + @period2 + '],[' + @period3 + '])
                          ) p'
            END
          WHEN 'sum' THEN
            N'SELECT  p.[agentID],
                      ''' + @id + ''' AS [id],
                      p.[' + @period1 + '] AS [month1Data],
                      p.[' + @period2 + '] AS [month2Data],
                      p.[' + @period3 + '] AS [month3Data]
              FROM    (
                      SELECT  DISTINCT
                              a.[agentID],
                              b.[periodID],
                              SUM(b.[' + @column + ']) AS [monthData]
                      FROM    [dbo].[agent] a LEFT OUTER JOIN
                              [dbo].[batch] b
                      ON      b.[agentID] = a.[agentID]
                      WHERE   b.[periodID] IN (' + @period1 + ',' + @period2 + ',' + @period3 + ')
                      GROUP BY a.[agentID],b.[periodID]
                      ) src
                      PIVOT
                      (
                      SUM([monthData])
                      FOR [periodID] in ([' + @period1 + '],[' + @period2 + '],[' + @period3 + '])
                      ) p'
        END
  
    INSERT INTO #batchTable ([agentID],[id],[month1Data],[month2Data],[month3Data])
    EXECUTE sp_executesql @sql

    SET @sql = N'SELECT  p.[agentID],
                         ''' + @id + 'P'' AS [id],
                         p.[' + @period1 + '] AS [month1Data],
                         p.[' + @period2 + '] AS [month2Data],
                         p.[' + @period3 + '] AS [month3Data]
                 FROM    (
                         SELECT  a.[agentID],
                                 a.[periodID],
                                 a.[plan]
                         FROM    (
                                 SELECT  [agentID],
                                         CASE
                                           WHEN [month] = ''month1''  THEN [year] * 100 + 000001
                                           WHEN [month] = ''month2''  THEN [year] * 100 + 000002
                                           WHEN [month] = ''month3''  THEN [year] * 100 + 000003
                                           WHEN [month] = ''month4''  THEN [year] * 100 + 000004
                                           WHEN [month] = ''month5''  THEN [year] * 100 + 000005
                                           WHEN [month] = ''month6''  THEN [year] * 100 + 000006
                                           WHEN [month] = ''month7''  THEN [year] * 100 + 000007
                                           WHEN [month] = ''month8''  THEN [year] * 100 + 000008
                                           WHEN [month] = ''month9''  THEN [year] * 100 + 000009
                                           WHEN [month] = ''month10'' THEN [year] * 100 + 000010
                                           WHEN [month] = ''month11'' THEN [year] * 100 + 000011
                                           WHEN [month] = ''month12'' THEN [year] * 100 + 000012
                                         END AS [periodID],
                                         [plan]
                                 FROM    [dbo].[agentactivity]
                                 UNPIVOT 
                                 (
                                   [plan]
                                   FOR [month] IN ([month1],[month2],[month3],[month4],[month5],[month6],[month7],[month8],[month9],[month10],[month11],[month12])
                                 ) up
                                 WHERE  ([type] = ''P'' OR [type] = ''I'')
                                 AND     [category] = ''' + @category + '''
                                 ) a
                         WHERE   a.[periodID] IN (SELECT [periodID] FROM [dbo].[period])
                         ) src
                         PIVOT
                         (
                         SUM([plan])
                         FOR src.[periodID] in ([' + @period1 + '],[' + @period2 + '],[' + @period3 + '])
                         ) p'

    INSERT INTO #batchTable ([agentID],[id],[month1Data],[month2Data],[month3Data])
    EXECUTE sp_executesql @sql

    SET @index += 1
  END

  --Select the agents
  SET @sql = N'
  SELECT  agent.[agentID],
          agent.[stateID],
          agent.[stat],
          agent.[name],
          CONVERT(VARCHAR(50),ISNULL(manager.[primaryManager],'''')) AS [manager],
          CONVERT(VARCHAR(500),ISNULL(manager.[secondManager],'''')) AS [secondManager],
          score.[qarDate],
          ISNULL(score.[qarScore],0) AS [qarScore],
          ISNULL(score.[errScore],0) AS [errScore],
          ISNULL(batch.[month' + [dbo].[GetEntry] (1, @list, ',') + '_1],0) AS [month' + [dbo].[GetEntry] (1, @list, ',') + '_1],
          ISNULL(batch.[month' + [dbo].[GetEntry] (1, @list, ',') + '_2],0) AS [month' + [dbo].[GetEntry] (1, @list, ',') + '_2],
          ISNULL(batch.[month' + [dbo].[GetEntry] (1, @list, ',') + '_3],0) AS [month' + [dbo].[GetEntry] (1, @list, ',') + '_3],
          ISNULL(batch.[month' + [dbo].[GetEntry] (2, @list, ',') + '_1],0) AS [month' + [dbo].[GetEntry] (2, @list, ',') + '_1],
          ISNULL(batch.[month' + [dbo].[GetEntry] (2, @list, ',') + '_2],0) AS [month' + [dbo].[GetEntry] (2, @list, ',') + '_2],
          ISNULL(batch.[month' + [dbo].[GetEntry] (2, @list, ',') + '_3],0) AS [month' + [dbo].[GetEntry] (2, @list, ',') + '_3],
          ISNULL(batch.[month' + [dbo].[GetEntry] (3, @list, ',') + '_1],0) AS [month' + [dbo].[GetEntry] (3, @list, ',') + '_1],
          ISNULL(batch.[month' + [dbo].[GetEntry] (3, @list, ',') + '_2],0) AS [month' + [dbo].[GetEntry] (3, @list, ',') + '_2],
          ISNULL(batch.[month' + [dbo].[GetEntry] (3, @list, ',') + '_3],0) AS [month' + [dbo].[GetEntry] (3, @list, ',') + '_3],
          ISNULL(batch.[month' + [dbo].[GetEntry] (4, @list, ',') + '_1],0) AS [month' + [dbo].[GetEntry] (4, @list, ',') + '_1],
          ISNULL(batch.[month' + [dbo].[GetEntry] (4, @list, ',') + '_2],0) AS [month' + [dbo].[GetEntry] (4, @list, ',') + '_2],
          ISNULL(batch.[month' + [dbo].[GetEntry] (4, @list, ',') + '_3],0) AS [month' + [dbo].[GetEntry] (4, @list, ',') + '_3],
          ISNULL(batch.[month' + [dbo].[GetEntry] (5, @list, ',') + '_1],0) AS [month' + [dbo].[GetEntry] (5, @list, ',') + '_1],
          ISNULL(batch.[month' + [dbo].[GetEntry] (5, @list, ',') + '_2],0) AS [month' + [dbo].[GetEntry] (5, @list, ',') + '_2],
          ISNULL(batch.[month' + [dbo].[GetEntry] (5, @list, ',') + '_3],0) AS [month' + [dbo].[GetEntry] (5, @list, ',') + '_3],
          ISNULL(batch.[month' + [dbo].[GetEntry] (6, @list, ',') + '_1],0) AS [month' + [dbo].[GetEntry] (6, @list, ',') + '_1],
          ISNULL(batch.[month' + [dbo].[GetEntry] (6, @list, ',') + '_2],0) AS [month' + [dbo].[GetEntry] (6, @list, ',') + '_2],
          ISNULL(batch.[month' + [dbo].[GetEntry] (6, @list, ',') + '_3],0) AS [month' + [dbo].[GetEntry] (6, @list, ',') + '_3],
          ISNULL(batch.[month' + [dbo].[GetEntry] (7, @list, ',') + '_1],0) AS [month' + [dbo].[GetEntry] (7, @list, ',') + '_1],
          ISNULL(batch.[month' + [dbo].[GetEntry] (7, @list, ',') + '_2],0) AS [month' + [dbo].[GetEntry] (7, @list, ',') + '_2],
          ISNULL(batch.[month' + [dbo].[GetEntry] (7, @list, ',') + '_3],0) AS [month' + [dbo].[GetEntry] (7, @list, ',') + '_3],
          ISNULL(batch.[month' + [dbo].[GetEntry] (1, @list, ',') + 'P_1],0) AS [month' + [dbo].[GetEntry] (1, @list, ',') + 'P_1],
          ISNULL(batch.[month' + [dbo].[GetEntry] (1, @list, ',') + 'P_2],0) AS [month' + [dbo].[GetEntry] (1, @list, ',') + 'P_2],
          ISNULL(batch.[month' + [dbo].[GetEntry] (1, @list, ',') + 'P_3],0) AS [month' + [dbo].[GetEntry] (1, @list, ',') + 'P_3],
          ISNULL(batch.[month' + [dbo].[GetEntry] (2, @list, ',') + 'P_1],0) AS [month' + [dbo].[GetEntry] (2, @list, ',') + 'P_1],
          ISNULL(batch.[month' + [dbo].[GetEntry] (2, @list, ',') + 'P_2],0) AS [month' + [dbo].[GetEntry] (2, @list, ',') + 'P_2],
          ISNULL(batch.[month' + [dbo].[GetEntry] (2, @list, ',') + 'P_3],0) AS [month' + [dbo].[GetEntry] (2, @list, ',') + 'P_3],
          ISNULL(batch.[month' + [dbo].[GetEntry] (3, @list, ',') + 'P_1],0) AS [month' + [dbo].[GetEntry] (3, @list, ',') + 'P_1],
          ISNULL(batch.[month' + [dbo].[GetEntry] (3, @list, ',') + 'P_2],0) AS [month' + [dbo].[GetEntry] (3, @list, ',') + 'P_2],
          ISNULL(batch.[month' + [dbo].[GetEntry] (3, @list, ',') + 'P_3],0) AS [month' + [dbo].[GetEntry] (3, @list, ',') + 'P_3],
          ISNULL(batch.[month' + [dbo].[GetEntry] (4, @list, ',') + 'P_1],0) AS [month' + [dbo].[GetEntry] (4, @list, ',') + 'P_1],
          ISNULL(batch.[month' + [dbo].[GetEntry] (4, @list, ',') + 'P_2],0) AS [month' + [dbo].[GetEntry] (4, @list, ',') + 'P_2],
          ISNULL(batch.[month' + [dbo].[GetEntry] (4, @list, ',') + 'P_3],0) AS [month' + [dbo].[GetEntry] (4, @list, ',') + 'P_3],
          ISNULL(batch.[month' + [dbo].[GetEntry] (5, @list, ',') + 'P_1],0) AS [month' + [dbo].[GetEntry] (5, @list, ',') + 'P_1],
          ISNULL(batch.[month' + [dbo].[GetEntry] (5, @list, ',') + 'P_2],0) AS [month' + [dbo].[GetEntry] (5, @list, ',') + 'P_2],
          ISNULL(batch.[month' + [dbo].[GetEntry] (5, @list, ',') + 'P_3],0) AS [month' + [dbo].[GetEntry] (5, @list, ',') + 'P_3],
          ISNULL(batch.[month' + [dbo].[GetEntry] (6, @list, ',') + 'P_1],0) AS [month' + [dbo].[GetEntry] (6, @list, ',') + 'P_1],
          ISNULL(batch.[month' + [dbo].[GetEntry] (6, @list, ',') + 'P_2],0) AS [month' + [dbo].[GetEntry] (6, @list, ',') + 'P_2],
          ISNULL(batch.[month' + [dbo].[GetEntry] (6, @list, ',') + 'P_3],0) AS [month' + [dbo].[GetEntry] (6, @list, ',') + 'P_3],
          ISNULL(batch.[month' + [dbo].[GetEntry] (7, @list, ',') + 'P_1],0) AS [month' + [dbo].[GetEntry] (7, @list, ',') + 'P_1],
          ISNULL(batch.[month' + [dbo].[GetEntry] (7, @list, ',') + 'P_2],0) AS [month' + [dbo].[GetEntry] (7, @list, ',') + 'P_2],
          ISNULL(batch.[month' + [dbo].[GetEntry] (7, @list, ',') + 'P_3],0) AS [month' + [dbo].[GetEntry] (7, @list, ',') + 'P_3],
          CONVERT(VARCHAR(500),ISNULL(alert.[alertWarn],'''')) AS [alertWarn],
          CONVERT(VARCHAR(500),ISNULL(alert.[alertCrit],'''')) AS [alertCrit],
          CONVERT(VARCHAR,CASE WHEN alert.[alertCrit] IS NOT NULL THEN 2 WHEN alert.[alertWarn] IS NOT NULL THEN 1 ELSE 0 END) AS [alertShow],
		  CASE WHEN fav.[sysFavoriteID] IS NOT NULL 
            THEN ''True''
            ELSE ''False''
          END AS [isFavorite],
          CASE WHEN doc.[entityID] IS NOT NULL 
            THEN ''True''
            ELSE ''False''
          END AS [hasDocument],
          note.[lastNoteDate]
  FROM    [dbo].[agent] agent LEFT OUTER JOIN
          #scoreTable score 
  ON      agent.[agentID] = score.[agentID] LEFT OUTER JOIN
          (
          SELECT  [agentID],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (1, @list, ',') + '''  THEN ISNULL([month1Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (1, @list, ',') + '_1],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (1, @list, ',') + '''  THEN ISNULL([month2Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (1, @list, ',') + '_2],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (1, @list, ',') + '''  THEN ISNULL([month3Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (1, @list, ',') + '_3],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (2, @list, ',') + '''  THEN ISNULL([month1Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (2, @list, ',') + '_1],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (2, @list, ',') + '''  THEN ISNULL([month2Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (2, @list, ',') + '_2],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (2, @list, ',') + '''  THEN ISNULL([month3Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (2, @list, ',') + '_3],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (3, @list, ',') + '''  THEN ISNULL([month1Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (3, @list, ',') + '_1],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (3, @list, ',') + '''  THEN ISNULL([month2Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (3, @list, ',') + '_2],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (3, @list, ',') + '''  THEN ISNULL([month3Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (3, @list, ',') + '_3],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (4, @list, ',') + '''  THEN ISNULL([month1Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (4, @list, ',') + '_1],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (4, @list, ',') + '''  THEN ISNULL([month2Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (4, @list, ',') + '_2],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (4, @list, ',') + '''  THEN ISNULL([month3Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (4, @list, ',') + '_3],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (5, @list, ',') + '''  THEN ISNULL([month1Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (5, @list, ',') + '_1],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (5, @list, ',') + '''  THEN ISNULL([month2Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (5, @list, ',') + '_2],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (5, @list, ',') + '''  THEN ISNULL([month3Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (5, @list, ',') + '_3],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (6, @list, ',') + '''  THEN ISNULL([month1Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (6, @list, ',') + '_1],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (6, @list, ',') + '''  THEN ISNULL([month2Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (6, @list, ',') + '_2],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (6, @list, ',') + '''  THEN ISNULL([month3Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (6, @list, ',') + '_3],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (7, @list, ',') + '''  THEN ISNULL([month1Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (7, @list, ',') + '_1],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (7, @list, ',') + '''  THEN ISNULL([month2Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (7, @list, ',') + '_2],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (7, @list, ',') + '''  THEN ISNULL([month3Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (7, @list, ',') + '_3],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (1, @list, ',') + 'P'' THEN ISNULL([month1Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (1, @list, ',') + 'P_1],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (1, @list, ',') + 'P'' THEN ISNULL([month2Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (1, @list, ',') + 'P_2],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (1, @list, ',') + 'P'' THEN ISNULL([month3Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (1, @list, ',') + 'P_3],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (2, @list, ',') + 'P'' THEN ISNULL([month1Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (2, @list, ',') + 'P_1],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (2, @list, ',') + 'P'' THEN ISNULL([month2Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (2, @list, ',') + 'P_2],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (2, @list, ',') + 'P'' THEN ISNULL([month3Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (2, @list, ',') + 'P_3],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (3, @list, ',') + 'P'' THEN ISNULL([month1Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (3, @list, ',') + 'P_1],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (3, @list, ',') + 'P'' THEN ISNULL([month2Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (3, @list, ',') + 'P_2],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (3, @list, ',') + 'P'' THEN ISNULL([month3Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (3, @list, ',') + 'P_3],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (4, @list, ',') + 'P'' THEN ISNULL([month1Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (4, @list, ',') + 'P_1],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (4, @list, ',') + 'P'' THEN ISNULL([month2Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (4, @list, ',') + 'P_2],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (4, @list, ',') + 'P'' THEN ISNULL([month3Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (4, @list, ',') + 'P_3],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (5, @list, ',') + 'P'' THEN ISNULL([month1Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (5, @list, ',') + 'P_1],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (5, @list, ',') + 'P'' THEN ISNULL([month2Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (5, @list, ',') + 'P_2],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (5, @list, ',') + 'P'' THEN ISNULL([month3Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (5, @list, ',') + 'P_3],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (6, @list, ',') + 'P'' THEN ISNULL([month1Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (6, @list, ',') + 'P_1],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (6, @list, ',') + 'P'' THEN ISNULL([month2Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (6, @list, ',') + 'P_2],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (6, @list, ',') + 'P'' THEN ISNULL([month3Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (6, @list, ',') + 'P_3],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (7, @list, ',') + 'P'' THEN ISNULL([month1Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (7, @list, ',') + 'P_1],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (7, @list, ',') + 'P'' THEN ISNULL([month2Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (7, @list, ',') + 'P_2],
                  SUM(CASE WHEN [id] = ''' + [dbo].[GetEntry] (7, @list, ',') + 'P'' THEN ISNULL([month3Data],0) ELSE 0 END) AS [month' + [dbo].[GetEntry] (7, @list, ',') + 'P_3]
          FROM    #batchTable
          GROUP BY [agentID]
          ) batch
  ON      agent.[agentID] = batch.[agentID] LEFT OUTER JOIN
          [sysfavorite] fav
  ON      agent.[agentID] = fav.[entityID]
  AND     fav.[entity] = ''A''  
  AND     fav.[uid] = ''' + @UID + '''      LEFT OUTER JOIN
          [sysdoc] doc
  ON      agent.[agentID] = CONVERT(INTEGER,doc.[entityID])
  AND     doc.[entityType] = ''Agent''  LEFT OUTER JOIN
          (
          SELECT  [agentID],
                  MAX([noteDate]) as [lastNoteDate]
          FROM    [dbo].[agentnote]
          GROUP BY [agentID]
          ) note 
  ON      agent.[agentID] = note.[agentID] LEFT OUTER JOIN
          ( 
          SELECT  am2.[agentID],
                  CONVERT(VARCHAR(1000), SUBSTRING((SELECT '','' + am.[uid] AS [text()] FROM [agentmanager] am WHERE am.[agentID] = am2.[agentID] AND am.[isPrimary] = 1 AND am.[stat] = ''A'' ORDER BY am.[agentID] FOR xml path('''') ), 2, 1000)) AS [primaryManager],
                  CONVERT(VARCHAR(1000), SUBSTRING((SELECT '','' + am.[uid] AS [text()] FROM [agentmanager] am WHERE am.[agentID] = am2.[agentID] AND am.[isPrimary] = 0 AND am.[stat] = ''A'' ORDER BY am.[agentID] FOR xml path('''') ), 2, 1000)) AS [secondManager]
          FROM    [agentmanager] am2
          WHERE   [stat] = ''A''
          GROUP BY am2.[agentID]
          ) manager
  ON      agent.[agentID] = manager.[agentID] LEFT OUTER JOIN
          ( 
          SELECT  al2.[agentID],
                  CONVERT(VARCHAR(1000), SUBSTRING((SELECT '', '' + al.[processCode] AS [text()] FROM [alert] al WHERE al2.[agentID] = al.[agentID] AND al.[stat] = ''O'' AND al.[active] = 1 AND al.[severity] = ''1'' ORDER BY al.[agentID] FOR xml path('''') ), 3, 1000)) AS [alertWarn],
                  CONVERT(VARCHAR(1000), SUBSTRING((SELECT '', '' + al.[processCode] AS [text()] FROM [alert] al WHERE al2.[agentID] = al.[agentID] AND al.[stat] = ''O'' AND al.[active] = 1 AND al.[severity] = ''2'' ORDER BY al.[agentID] FOR xml path('''') ), 3, 1000)) AS [alertCrit]
          FROM    [alert] al2
          GROUP BY al2.[agentID]
          ) alert
  ON      agent.[agentID] = alert.[agentID]
  WHERE   agent.[agentID] IN (SELECT [agentID] FROM [dbo].[agent] WHERE ([agentID] = (CASE  WHEN ''' + @agentID + ''' != ''ALL'' THEN ''' + @agentID + ''' ELSE [agentID] END)) AND ([dbo].[CanAccessAgent](''' + @UID + ''' , [agentID]) = 1))
  AND     agent.[stateID] IN (SELECT [stateID] FROM #stateTable)
  ORDER BY agent.[agentID]'

  EXECUTE sp_executesql @sql

  DROP TABLE #batchTable
  DROP TABLE #scoreTable
  DROP TABLE #stateTable
END
