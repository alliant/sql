-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetAgentsNprActualToPlan] 
	-- Add the parameters for the stored procedure here
	@ipPageNo        INTEGER,
	@ipNoOfRows      INTEGER,
	@cNprSortOrder   VARCHAR(1),
    @agentID         VARCHAR(MAX) = 'ALL',
    @stateID         VARCHAR(200) = 'ALL',
	@UID             VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @iNoCalls INTEGER     = 0,
	        @iCount   INTEGER     = 0

	CREATE TABLE #ttAgentPremium (entityId       VARCHAR(100),
							      name           VARCHAR(200),
							      stat           VARCHAR(1),
								  email          VARCHAR(200),
							      city           VARCHAR(50),
						          state          VARCHAR(2),
							      zip            VARCHAR(20),
	                              netPermium     DECIMAL(18,2),
								  plannedPermium DECIMAL(18,2),
								  cpl            DECIMAL(18,2))

    CREATE TABLE #ttAllAgents (rowNum    INTEGER,
	                          entityId   VARCHAR(100),
							  name       VARCHAR(200),
							  stat       VARCHAR(1),
							   email      VARCHAR(200),
							  city       VARCHAR(50),
						      state      VARCHAR(2),
							  zip        VARCHAR(20),
	                          netPermium DECIMAL(18,2),
							  cpl        DECIMAL(18,2))


    CREATE TABLE #profile (type         VARCHAR(10),
	                       rowNum       INTEGER,
	                       entityId     VARCHAR(100),
						   favorite      BIT,
						   sysfavoriteID INTEGER,
						   name          VARCHAR(200),
						   stat          VARCHAR(1),
						   email      VARCHAR(200),
						   city          VARCHAR(50),
					       state         VARCHAR(2),
					   	   zip           VARCHAR(20),
	                       netPermium    DECIMAL(18,2),
						   cpl           DECIMAL(18,2),
						   noOfPeople    integer)

   CREATE TABLE #totalCount (type         VARCHAR(10),
                           rowNum            INTEGER,
	                       entityId     VARCHAR(100),
						   favorite      BIT,
						   sysfavoriteID INTEGER,
						   name          VARCHAR(200),
						   stat          VARCHAR(1),
						   email      VARCHAR(200),
						   city          VARCHAR(50),
					       state         VARCHAR(2),
					   	   zip           VARCHAR(20),
	                       netPermium    DECIMAL(18,2),
						   cpl           DECIMAL(18,2),
						   noOfPeople    INTEGER )



    CREATE TABLE #agentactivity ([activityID]    INTEGER NOT NULL,
                                 [agentID]       VARCHAR(50) NULL,
                                 [name]          VARCHAR(200) NULL,
                                 [stateID]       VARCHAR(50) NULL,
                                 [year]          INTEGER NULL,
                                 [type]          VARCHAR(50) NULL,
                                 [stat]          VARCHAR(1) NULL,
                                 [category]      VARCHAR(50) NULL,
                                 [corporationID] VARCHAR(100) NULL,
                                 [month1]        DECIMAL(18, 2) NULL,
                                 [month2]        DECIMAL(18, 2) NULL,
                                 [month3]        DECIMAL(18, 2) NULL,
                                 [month4]        DECIMAL(18, 2) NULL,
                                 [month5]        DECIMAL(18, 2) NULL,
                                 [month6]        DECIMAL(18, 2) NULL,
                                 [month7]        DECIMAL(18, 2) NULL,
                                 [month8]        DECIMAL(18, 2) NULL,
                                 [month9]        DECIMAL(18, 2) NULL,
                                 [month10]       DECIMAL(18, 2) NULL,
                                 [month11]       DECIMAL(18, 2) NULL,
                                 [month12]       DECIMAL(18, 2) NULL
                                 ) ON [PRIMARY]

    INSERT INTO #agentactivity
    EXEC [spReportAgentActivities] @year = 2019, @agentID = @agentID, @category = 'N' , @UID = @UID

    INSERT INTO #agentactivity
    EXEC [spReportAgentActivities] @year = 2019, @agentID = @agentID, @category = 'I' , @UID = @UID
	
	INSERT INTO #ttAgentPremium([entityId],[name],[stat],[email],[city],[state],[zip])
	  SELECT distinct aa.agentID,
	          aa.name,
	           a.stat,
			   a.email,
			   a.city,
			   a.state,
			   a.zip 			   
      FROM #agentactivity aa join agent a
	    ON aa.agentId = a.agentID


    UPDATE #ttAgentPremium
        SET #ttAgentPremium.netPermium = p.netPremium1
        FROM #ttAgentPremium
        CROSS APPLY (SELECT (p.month1 +
	                         p.month2 +
	                         p.month3 +
		           	    	 p.month4 +
		 				     p.month5 +
		 				     p.month6 +
						     p.month7 +
						     p.month8 +
						     p.month9 +
						     p.month10 +
						     p.month11 +
						     p.month12) as netPremium1
                        FROM #agentActivity p
                         WHERE #ttAgentPremium.entityId = p.agentID
					       and p.category = 'N'
					       and p.[type]   = 'A') as p
   
       UPDATE #ttAgentPremium
         SET #ttAgentPremium.plannedPermium = p.plannedPremium1
         FROM #ttAgentPremium
         CROSS APPLY (SELECT (p.month1 +
	                          p.month2 +
						      p.month3 +
						      p.month4 +
						      p.month5 +
						      p.month6 +
						      p.month7 +
						      p.month8 +
						      p.month9 +
						      p.month10 +
						      p.month11 +
						      p.month12) as plannedPremium1
                       FROM #agentActivity p
                         WHERE #ttAgentPremium.entityId = p.agentID
					       and p.category = 'N'
					       and p.[type]   = 'P') as p
   

     UPDATE #ttAgentPremium
        SET #ttAgentPremium.cpl = p.cpl1
         FROM #ttAgentPremium
         CROSS APPLY (SELECT (p.month1 +
	                          p.month2 +
						      p.month3 +
						      p.month4 +
						      p.month5 +
						      p.month6 +
						      p.month7 +
						      p.month8 +
						      p.month9 +
						      p.month10 +
						      p.month11 +
						      p.month12) as cpl1
                        FROM #agentActivity p
                         WHERE #ttAgentPremium.entityId = p.agentID
					       and p.category = 'I'
					       and p.[type]   = 'A') as p


    INSERT INTO #ttAllAgents([entityId],[name],[stat],[email],[city],[state],[zip],[netPermium],[cpl]) 
	  SELECT #ttAgentPremium.entityId,
	         #ttAgentPremium.name,
	         #ttAgentPremium.stat,
			 #ttAgentPremium.email,
	         #ttAgentPremium.city,
	         #ttAgentPremium.state,
	         #ttAgentPremium.zip,
	         ISNULL(#ttAgentPremium.[netPermium],0) AS [netPermium], 
			 ISNULL(#ttAgentPremium.[cpl],0) AS [cpl]
	    FROM #ttAgentPremium WHERE #ttAgentPremium.entityId = CASE WHEN @cNprSortOrder = 'U' AND #ttAgentPremium.plannedPermium > #ttAgentPremium.netPermium
									                               THEN #ttAgentPremium.entityId
																   WHEN @cNprSortOrder = 'T' AND #ttAgentPremium.netPermium >= #ttAgentPremium.plannedPermium
																   THEN #ttAgentPremium.entityId
																   ELSE 0
		                                                       END 
		                                                     
	/* count total Agents */
	SELECT @iCount = COUNT(*) FROM #ttAllAgents
	
	INSERT INTO #profile([type],[entityId],[name],[stat],[email],[city],[state],[zip],[netPermium],[cpl])
    SELECT 
	      'A',
		  #ttAllAgents.entityId,
		  #ttAllAgents.name,
		  #ttAllAgents.stat,
		  #ttAllAgents.email,
		  #ttAllAgents.city,
		  #ttAllAgents.state,
		  #ttAllAgents.zip,
	      ISNULL(#ttAllAgents.[netPermium],0) AS [netPermium], 
		  ISNULL(#ttAllAgents.[cpl],0) AS [cpl]
	 FROM #ttAllAgents -- Insert statements for procedure here	


    UPDATE #profile SET #profile.favorite      = 'true',
	                    #profile.sysfavoriteID = sysfavorite.sysfavoriteID
	  FROM #profile inner join sysfavorite ON sysfavorite.entity   = 'A'
											AND #profile.entityId  = sysfavorite.entityID 
   UPDATE #profile set #profile.noOfPeople = (select COUNT(*) from personagent where personagent.agentID = #profile.entityID);	
	 /* Last record count */
    INSERT INTO #totalCount(#ttTopAgents.TYPE,#ttTopAgents.rowNum) values('AgentCount',@iCount)
	
	/********************Top 10 rows sorted according to netPermium**************************/
	
	SELECT * FROM
	(
	SELECT TOP(@ipNoOfRows) * 
	  FROM ( SELECT [type],
	                ROW_NUMBER ( ) OVER (ORDER BY CASE WHEN @cNprSortOrder = 'U' 
		                                               THEN #profile.netPermium 
                                                  END,
									              CASE WHEN @cNprSortOrder = 'T' 
		                                               THEN #profile.netPermium 
                                                  END DESC ) AS rowNum , 
					[entityId],
					[favorite],
					[sysfavoriteID], 
					[name],
					[stat],
					[email],
					[city],
					[state],
					[zip],
					[netPermium],
					[cpl],         /* giving row number sequentially according to netPermium*/						
					[noOfPeople]
	         FROM #profile) q      /* all fields data */
	WHERE rowNum >= (@ipNoOfRows * (@ipPageNo - 1) + 1)     /* pagination */
	UNION
	SELECT * 
	FROM #totalCount ) t
	
	
	
END

GO
