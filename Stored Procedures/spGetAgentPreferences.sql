USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetAgentPreferences]    Script Date: 6/8/2020 11:04:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetAgentPreferences] 
	-- Add the parameters for the stored procedure here
	@agentID VARCHAR(50) = 'ALL'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  -- Insert statements for procedure here
  SELECT  [agentID],
          [seq],
          [prefCode],
          [prefValue],
          [prefLike]
  FROM    [dbo].[agentpreference]
  WHERE   [agentID] = CASE WHEN @agentID = 'ALL' THEN [agentID] ELSE @agentID END
END
