USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spCalculateReportGap]    Script Date: 5/16/2016 10:07:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		  John Oliver
-- Create date: 5/16/2016
-- Description:	Calculates the Report and Policy
--              Distance Gaps
-- =============================================
ALTER PROCEDURE [dbo].[spCalculateReportGap]
	-- Add the parameters for the stored procedure here
	@batchID     int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
  SELECT  SUM(a.dayCnt) / CONVERT(DECIMAL,COUNT(a.batchID)) AS 'reportGap'
  FROM    (
          SELECT  DISTINCT
                  b.[batchID],
                  bf.[policyID],
                  b.[periodYear],
                  b.[periodMonth],
                  b.[receivedDate],
                  bf.[effDate],
                  datediff(day,bf.[effDate],b.[receivedDate]) AS 'dayCnt'
          FROM    batchform bf inner join
                  batch b
          ON      b.batchID = bf.[batchID]
          WHERE   b.batchID = @batchID
          ) a
END
