USE [COMPASS_ALFA]
GO
/****** Object:  StoredProcedure [dbo].[spGetRegiondetailNPROlap]    Script Date: 3/16/2023 6:42:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetRegiondetailNPROlap]
	-- Add the parameters for the stored procedure here
  @year varchar(4),
  @month varchar(14),
  @region varchar(100),
  @datasource varchar(100),
  @intialcatalog varchar(100),
  @lag varchar(4),
  @UID varchar(100) = ''
AS
BEGIN

declare @sql varchar(max)
declare @group varchar (100)

create table  #tempdataset
(
agentname varchar(500),
agentid varchar(20) ,
stateid varchar(20),
manager varchar(100), 
latestauditscore varchar(20), 
noofcplreported decimal(18,2),
policyreported decimal(18,2),
netpremium decimal(18,2),
cplreported12Monthsbefore decimal(18,2),
policyreported12Monthsbefore decimal(18,2),
netpremium12Monthsbefore decimal(18,2)
)
 
if(@year = '')  or (@year= NULL)
  set @year = year(getdate())

if(@month= '')  or (@month= NULL)
  set @month = month(getdate())

if(@lag= '')  or (@lag= NULL)
  set @lag = 12


 set @sql = 
'
insert into #tempdataset
SELECT a."[Agent].[Agent].[Agent].[MEMBER_CAPTION]" as AgentName,
	   a."[Agent].[AgentID].[AgentID].[MEMBER_CAPTION]" as AgentID,
	   a."[Agent].[State ID].[State ID].[MEMBER_CAPTION]" as StateID,
	   a."[Agent].[Manager].[Manager].[MEMBER_CAPTION]" as Manager ,
	   a."[Agent].[Latest Audit Score].[Latest Audit Score].[MEMBER_CAPTION]" as LatestAuditScore,
 	   a."[Measures].[No Of CPL Reported]" as NoofCPLReported, 
	   a."[Measures].[Policies reported]" as PoliciesReported, 
	   a."[Measures].[Net Premium]"  as NetPremium ,
 	   a."[Measures].[CPLReported12MonthsBefore]" as CPLReported12MonthsBefore, 
	   a."[Measures].[Policiesreported12MonthsBefore]" as Policiesreported12MonthsBefore, 
	   a."[Measures].[NetPremium12MonthsBefore]"  as NetPremium12MonthsBefore 

 FROM OpenRowset(''MSOLAP'',''DATASOURCE=' + @datasource + '; Initial Catalog=' + @intialcatalog + ';'',
 '' WITH
  MEMBER [Measures].[NetPremium12MonthsBefore] AS
       (    [Date].[Calendar].[Months].&[' + @Month + ']&[' + @Year + '].lag(' + @lag + ') ,  [Measures].[Net Premium]     )
  MEMBER [Measures].[Policiesreported12MonthsBefore] AS
       (    [Date].[Calendar].[Months].&[' + @Month + ']&[' + @Year + '].lag(' + @lag + ') ,  [Measures].[Policies reported]     )
  MEMBER [Measures].[CPLReported12MonthsBefore] AS
       (    [Date].[Calendar].[Months].&[' + @Month + ']&[' + @Year + '].lag(' + @lag + ') ,  [Measures].[No Of CPL Reported]     )
 SELECT NON EMPTY {[Measures].[No Of CPL Reported], [Measures].[Policies reported],   [Measures].[Net Premium] , [CPLReported12MonthsBefore] , [Measures].[Policiesreported12MonthsBefore] , [Measures].[NetPremium12MonthsBefore] } ON COLUMNS, 
                      NON EMPTY { ([Agent].[Agent].[Agent].ALLMEMBERS * 
                                           [Agent].[AgentID].[AgentID].ALLMEMBERS * 
                                           [Agent].[State ID].[State ID].ALLMEMBERS * 
                                           [Agent].[Manager].[Manager].ALLMEMBERS * 
                                           [Agent].[Latest Audit Score].[Latest Audit Score].ALLMEMBERS ) } 
           DIMENSION PROPERTIES MEMBER_CAPTION, MEMBER_UNIQUE_NAME ON ROWS FROM 
            ( SELECT ( { [Date].[Calendar].[Years].&[' + @Year + '], [Date].[Calendar].[Months].&[' + @Month + ']&[' + @Year + '] } ) 
             ON COLUMNS FROM [NPR]) Where    ({[Date].[Calendar].[Months].&[' + @Month + ']&[' + @Year + ']} , { [Agent].[Region].&[' + @Region + ']})
 ''
  ) as a
 '


Begin try
	EXEC (@sql)
End try
Begin catch
	select error_message()
	print 'No Data'
End catch
 
select * from #tempdataset

drop table #tempdataset
 
 END