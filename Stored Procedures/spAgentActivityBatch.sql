-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spAgentActivityBatch]
	-- Add the parameters for the stored procedure here
	@agentID VARCHAR(MAX) = 'ALL',
	@year INTEGER = 0,
	@month INTEGER = 0,
	@isError BIT = 0     OUTPUT,
	@errMsg VARCHAR(200) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET ANSI_WARNINGS OFF;
  
	CREATE TABLE #agentTable ([agentID] VARCHAR(30))
	INSERT INTO #agentTable ([agentID])
	SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)

	-- Used for the insert
	DECLARE @month1 DECIMAL(18,2) = 0,
			    @month2 DECIMAL(18,2) = 0,
			    @month3 DECIMAL(18,2) = 0,
			    @month4 DECIMAL(18,2) = 0,
			    @month5 DECIMAL(18,2) = 0,
			    @month6 DECIMAL(18,2) = 0,
			    @month7 DECIMAL(18,2) = 0,
			    @month8 DECIMAL(18,2) = 0,
			    @month9 DECIMAL(18,2) = 0,
			    @month10 DECIMAL(18,2) = 0,
			    @month11 DECIMAL(18,2) = 0,
			    @month12 DECIMAL(18,2) = 0,
			    @type VARCHAR(20) = '',
			    @category VARCHAR(20) = '',
			    @activityID INTEGER,
			    @transCount BIT = 0

	SET @errMsg = '' 
	SET @isError = 0

	IF (@year = 0)
		SET @year = YEAR(GETDATE())
    
	IF (@month = 0)
		SET @month = MONTH(GETDATE())

	CREATE TABLE #exportTable
	(
		[agentID] VARCHAR(30),
		[type] VARCHAR(20),
		[category] VARCHAR(20),
		[month1] DECIMAL(18,2),
		[month2] DECIMAL(18,2),
		[month3] DECIMAL(18,2),
		[month4] DECIMAL(18,2),
		[month5] DECIMAL(18,2),
		[month6] DECIMAL(18,2),
		[month7] DECIMAL(18,2),
		[month8] DECIMAL(18,2),
		[month9] DECIMAL(18,2),
		[month10] DECIMAL(18,2),
		[month11] DECIMAL(18,2),
		[month12] DECIMAL(18,2)
	)

	-- The policies reported
	INSERT INTO #exportTable ([agentID],[type],[category],[month1],[month2],[month3],[month4],[month5],[month6],[month7],[month8],[month9],[month10],[month11],[month12])
	SELECT  p.[agentID],
			    'R' AS [type],
			    'P' AS [category],
			    ISNULL(p.[1], 0) AS [month1],
			    ISNULL(p.[2], 0) AS [month2],
			    ISNULL(p.[3], 0) AS [month3],
			    ISNULL(p.[4], 0) AS [month4],
			    ISNULL(p.[5], 0) AS [month5],
			    ISNULL(p.[6], 0) AS [month6],
			    ISNULL(p.[7], 0) AS [month7],
			    ISNULL(p.[8], 0) AS [month8],
			    ISNULL(p.[9], 0) AS [month9],
			    ISNULL(p.[10], 0) AS [month10],
			    ISNULL(p.[11], 0) AS [month11],
			    ISNULL(p.[12], 0) AS [month12]
	FROM    (
          SELECT  a.[agentID],
					        b.[periodMonth],
					        bf.[policyID]
			    FROM    [dbo].[agent] a LEFT OUTER JOIN
                  [dbo].[batch] b 
          ON      a.[agentID] = b.[agentID] LEFT OUTER JOIN
					        [dbo].[batchform] bf
			    ON      b.[batchID] = bf.[batchID]
			    AND     bf.[formType] = 'P'
			    AND     b.[periodYear] = @year
			    AND     b.[periodMonth] = @month
			    WHERE   a.[agentID] IN (SELECT [agentID] FROM #agentTable)
			    ) src
			    PIVOT
			    (
			    COUNT([policyID])
			    FOR [periodMonth] IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])
			    ) p
	UNION ALL
	-- The gross premium
	SELECT  p.[agentID],
			    'A' AS [type],
			    'G' AS [category],
			    ISNULL(p.[1], 0) AS [month1],
			    ISNULL(p.[2], 0) AS [month2],
			    ISNULL(p.[3], 0) AS [month3],
			    ISNULL(p.[4], 0) AS [month4],
			    ISNULL(p.[5], 0) AS [month5],
			    ISNULL(p.[6], 0) AS [month6],
			    ISNULL(p.[7], 0) AS [month7],
			    ISNULL(p.[8], 0) AS [month8],
			    ISNULL(p.[9], 0) AS [month9],
			    ISNULL(p.[10], 0) AS [month10],
			    ISNULL(p.[11], 0) AS [month11],
			    ISNULL(p.[12], 0) AS [month12]
	FROM    (
          SELECT  a.[agentID],
					        b.[periodMonth],
					        bf.[grossDelta]
			    FROM    [dbo].[agent] a LEFT OUTER JOIN
                  [dbo].[batch] b 
          ON      a.[agentID] = b.[agentID] LEFT OUTER JOIN
					        [dbo].[batchform] bf
			    ON      b.[batchID] = bf.[batchID]
			    AND     b.[periodYear] = @year
			    AND     b.[periodMonth] = @month
			    WHERE   a.[agentID] IN (SELECT [agentID] FROM #agentTable)
			    ) src
			    PIVOT
			    (
			    SUM([grossDelta])
			    FOR [periodMonth] IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])
			    ) p
	UNION ALL
	-- The net premium
	SELECT  p.[agentID],
			    'A' AS [type],
			    'N' AS [category],
			    ISNULL(p.[1], 0) AS [month1],
			    ISNULL(p.[2], 0) AS [month2],
			    ISNULL(p.[3], 0) AS [month3],
			    ISNULL(p.[4], 0) AS [month4],
			    ISNULL(p.[5], 0) AS [month5],
			    ISNULL(p.[6], 0) AS [month6],
			    ISNULL(p.[7], 0) AS [month7],
			    ISNULL(p.[8], 0) AS [month8],
			    ISNULL(p.[9], 0) AS [month9],
			    ISNULL(p.[10], 0) AS [month10],
			    ISNULL(p.[11], 0) AS [month11],
			    ISNULL(p.[12], 0) AS [month12]
	FROM    (
          SELECT  a.[agentID],
					        b.[periodMonth],
					        bf.[netDelta]
			    FROM    [dbo].[agent] a LEFT OUTER JOIN
                  [dbo].[batch] b 
          ON      a.[agentID] = b.[agentID] LEFT OUTER JOIN
					        [dbo].[batchform] bf
			    ON      b.[batchID] = bf.[batchID]
			    AND     b.[periodYear] = @year
			    AND     b.[periodMonth] = @month
			    WHERE   a.[agentID] IN (SELECT [agentID] FROM #agentTable)
			    ) src
			    PIVOT
			    (
			    SUM([netDelta])
			    FOR [periodMonth] IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])
			    ) p

	DECLARE cur CURSOR LOCAL FOR
	SELECT  e.*
	FROM    #exportTable e INNER JOIN
			[dbo].[agent] a
	ON      e.[agentID] = a.[agentID]


	BEGIN TRY
		IF(@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION 
			SET @transCount = 1
		END

		OPEN cur
		FETCH NEXT FROM cur INTO @agentID,@type,@category,@month1,@month2,@month3,@month4,@month5,@month6,@month7,@month8,@month9,@month10,@month11,@month12
		WHILE @@FETCH_STATUS = 0 
		BEGIN
			EXEC [dbo].[spInsertAgentActivity] @agentID = @agentID,
												@year = @year,
												@stat = 'C',
												@category = @category,
												@type = @type,
												@currentMonth = @month,
												@month1 = @month1,
												@month2 = @month2,
												@month3 = @month3,
												@month4 = @month4,
												@month5 = @month5,
												@month6 = @month6,
												@month7 = @month7,
												@month8 = @month8,
												@month9 = @month9,
												@month10 = @month10,
												@month11 = @month11,
												@month12 = @month12,
												@isAdmin = 1,
												@activityID = @activityID OUTPUT,
												@isError = @isError OUTPUT,
												@errMsg  = @errMsg OUTPUT
			
			IF(@isError = 1)
				RAISERROR(@errMsg, 16, 1)

			FETCH NEXT FROM cur INTO @agentID,@type,@category,@month1,@month2,@month3,@month4,@month5,@month6,@month7,@month8,@month9,@month10,@month11,@month12
		END
		CLOSE cur
		DEALLOCATE cur

		DROP TABLE #agentTable
		DROP TABLE #exportTable

		IF(@transCount = 1)
			COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		SET @isError = ERROR_STATE()
		SET @errMsg  = ERROR_MESSAGE()
		IF(@transCount = 1)
			ROLLBACK TRANSACTION
		RETURN
	END CATCH


END
GO
