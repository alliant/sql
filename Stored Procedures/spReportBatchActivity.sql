GO
/****** Object:  StoredProcedure [dbo].[spReportBatchActivity]    Script Date: 4/11/2017 3:03:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportBatchActivity]
  @year INTEGER = 0,
  @agent VARCHAR(MAX) = 'ALL',
  @state VARCHAR(MAX) = 'ALL',
  @UID varchar(100)
AS
BEGIN
  DECLARE @NextString NVARCHAR(100),
          @Pos INTEGER = 0,
          @Str NVARCHAR(MAX),
          @Delimiter NCHAR(1) = ','
  
  SET @agent = [dbo].[StandardizeAgentID](@agent)
  
  -- Get the year
  CREATE TABLE #yearTable ([year] INTEGER)
  IF (@year > 0)
    INSERT INTO #yearTable
    SELECT DISTINCT [periodYear] FROM [dbo].[period] WHERE [periodYear] = @year
  ELSE
    INSERT INTO #yearTable
    SELECT DISTINCT [periodYear] FROM [dbo].[period]
    
  -- Get the state
  CREATE TABLE #stateTable ([stateID] VARCHAR(2))
  INSERT INTO #stateTable ([stateID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('State', @state)

  -- Get the agent
  CREATE TABLE #agentTable ([agentID] VARCHAR(30))
  INSERT INTO #agentTable ([agentID])
  SELECT a.[agentID] FROM [agent] a WHERE (a.[agentID] = (CASE  WHEN @agent != 'ALL' THEN @agent ELSE a.[agentID] END)) 
                                      AND (dbo.CanAccessAgent(@UID ,a.[agentID]) = 1)

  SET NOCOUNT ON;
  SELECT  batch.[agentID],
          agent.[name],
          agent.[stateID],
          agent.[stat],
          (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'AMD' AND [objAction] = 'Agent' AND [objProperty] = 'Status' AND [objID] = agent.[stat]) AS [statDesc],
          am.[uid] AS [manager],
          (SELECT [name] FROM [dbo].[sysuser] WHERE [uid] = am.[uid]) AS [managerDesc],
          batch.[periodYear],
          SUM(ISNULL([janGap],0)) AS 'janGap',
          SUM(ISNULL([febGap],0)) AS 'febGap',
          SUM(ISNULL([marGap],0)) AS 'marGap',
          SUM(ISNULL([aprGap],0)) AS 'aprGap',
          SUM(ISNULL([mayGap],0)) AS 'mayGap',
          SUM(ISNULL([junGap],0)) AS 'junGap',
          SUM(ISNULL([julGap],0)) AS 'julGap',
          SUM(ISNULL([augGap],0)) AS 'augGap',
          SUM(ISNULL([sepGap],0)) AS 'sepGap',
          SUM(ISNULL([octGap],0)) AS 'octGap',
          SUM(ISNULL([novGap],0)) AS 'novGap',
          SUM(ISNULL([decGap],0)) AS 'decGap',
          SUM(ISNULL([janBatches],0)) AS 'janBatches',
          SUM(ISNULL([febBatches],0)) AS 'febBatches',
          SUM(ISNULL([marBatches],0)) AS 'marBatches',
          SUM(ISNULL([aprBatches],0)) AS 'aprBatches',
          SUM(ISNULL([mayBatches],0)) AS 'mayBatches',
          SUM(ISNULL([junBatches],0)) AS 'junBatches',
          SUM(ISNULL([julBatches],0)) AS 'julBatches',
          SUM(ISNULL([augBatches],0)) AS 'augBatches',
          SUM(ISNULL([sepBatches],0)) AS 'sepBatches',
          SUM(ISNULL([octBatches],0)) AS 'octBatches',
          SUM(ISNULL([novBatches],0)) AS 'novBatches',
          SUM(ISNULL([decBatches],0)) AS 'decBatches',
          SUM(ISNULL([janFiles],0)) AS 'janFiles',
          SUM(ISNULL([febFiles],0)) AS 'febFiles',
          SUM(ISNULL([marFiles],0)) AS 'marFiles',
          SUM(ISNULL([aprFiles],0)) AS 'aprFiles',
          SUM(ISNULL([mayFiles],0)) AS 'mayFiles',
          SUM(ISNULL([junFiles],0)) AS 'junFiles',
          SUM(ISNULL([julFiles],0)) AS 'julFiles',
          SUM(ISNULL([augFiles],0)) AS 'augFiles',
          SUM(ISNULL([sepFiles],0)) AS 'sepFiles',
          SUM(ISNULL([octFiles],0)) AS 'octFiles',
          SUM(ISNULL([novFiles],0)) AS 'novFiles',
          SUM(ISNULL([decFiles],0)) AS 'decFiles',
          SUM(ISNULL([janPolicies],0)) AS 'janPolicies',
          SUM(ISNULL([febPolicies],0)) AS 'febPolicies',
          SUM(ISNULL([marPolicies],0)) AS 'marPolicies',
          SUM(ISNULL([aprPolicies],0)) AS 'aprPolicies',
          SUM(ISNULL([mayPolicies],0)) AS 'mayPolicies',
          SUM(ISNULL([junPolicies],0)) AS 'junPolicies',
          SUM(ISNULL([julPolicies],0)) AS 'julPolicies',
          SUM(ISNULL([augPolicies],0)) AS 'augPolicies',
          SUM(ISNULL([sepPolicies],0)) AS 'sepPolicies',
          SUM(ISNULL([octPolicies],0)) AS 'octPolicies',
          SUM(ISNULL([novPolicies],0)) AS 'novPolicies',
          SUM(ISNULL([decPolicies],0)) AS 'decPolicies',
          SUM(ISNULL([janNet],0)) AS 'janNet',
          SUM(ISNULL([febNet],0)) AS 'febNet',
          SUM(ISNULL([marNet],0)) AS 'marNet',
          SUM(ISNULL([aprNet],0)) AS 'aprNet',
          SUM(ISNULL([mayNet],0)) AS 'mayNet',
          SUM(ISNULL([junNet],0)) AS 'junNet',
          SUM(ISNULL([julNet],0)) AS 'julNet',
          SUM(ISNULL([augNet],0)) AS 'augNet',
          SUM(ISNULL([sepNet],0)) AS 'sepNet',
          SUM(ISNULL([octNet],0)) AS 'octNet',
          SUM(ISNULL([novNet],0)) AS 'novNet',
          SUM(ISNULL([decNet],0)) AS 'decNet',
          SUM(ISNULL([janGross],0)) AS 'janGross',
          SUM(ISNULL([febGross],0)) AS 'febGross',
          SUM(ISNULL([marGross],0)) AS 'marGross',
          SUM(ISNULL([aprGross],0)) AS 'aprGross',
          SUM(ISNULL([mayGross],0)) AS 'mayGross',
          SUM(ISNULL([junGross],0)) AS 'junGross',
          SUM(ISNULL([julGross],0)) AS 'julGross',
          SUM(ISNULL([augGross],0)) AS 'augGross',
          SUM(ISNULL([sepGross],0)) AS 'sepGross',
          SUM(ISNULL([octGross],0)) AS 'octGross',
          SUM(ISNULL([novGross],0)) AS 'novGross',
          SUM(ISNULL([decGross],0)) AS 'decGross',
          SUM(ISNULL([janLiab],0)) AS 'janLiab',
          SUM(ISNULL([febLiab],0)) AS 'febLiab',
          SUM(ISNULL([marLiab],0)) AS 'marLiab',
          SUM(ISNULL([aprLiab],0)) AS 'aprLiab',
          SUM(ISNULL([mayLiab],0)) AS 'mayLiab',
          SUM(ISNULL([junLiab],0)) AS 'junLiab',
          SUM(ISNULL([julLiab],0)) AS 'julLiab',
          SUM(ISNULL([augLiab],0)) AS 'augLiab',
          SUM(ISNULL([sepLiab],0)) AS 'sepLiab',
          SUM(ISNULL([octLiab],0)) AS 'octLiab',
          SUM(ISNULL([novLiab],0)) AS 'novLiab',
          SUM(ISNULL([decLiab],0)) AS 'decLiab',
          SUM(ISNULL([janRetain],0)) AS 'janRetain',
          SUM(ISNULL([febRetain],0)) AS 'febRetain',
          SUM(ISNULL([marRetain],0)) AS 'marRetain',
          SUM(ISNULL([aprRetain],0)) AS 'aprRetain',
          SUM(ISNULL([mayRetain],0)) AS 'mayRetain',
          SUM(ISNULL([junRetain],0)) AS 'junRetain',
          SUM(ISNULL([julRetain],0)) AS 'julRetain',
          SUM(ISNULL([augRetain],0)) AS 'augRetain',
          SUM(ISNULL([sepRetain],0)) AS 'sepRetain',
          SUM(ISNULL([octRetain],0)) AS 'octRetain',
          SUM(ISNULL([novRetain],0)) AS 'novRetain',
          SUM(ISNULL([decRetain],0)) AS 'decRetain'
  FROM    (
          SELECT  [agentID],
                  [stateID],
                  [periodYear],
                  SUM([1B])  AS 'janBatches',
                  SUM([2B])  AS 'febBatches',
                  SUM([3B])  AS 'marBatches',
                  SUM([4B])  AS 'aprBatches',
                  SUM([5B])  AS 'mayBatches',
                  SUM([6B])  AS 'junBatches',
                  SUM([7B])  AS 'julBatches',
                  SUM([8B])  AS 'augBatches',
                  SUM([9B])  AS 'sepBatches',
                  SUM([10B]) AS 'octBatches',
                  SUM([11B]) AS 'novBatches',
                  SUM([12B]) AS 'decBatches',
                  SUM([1N])  AS 'janNet',
                  SUM([2N])  AS 'febNet',
                  SUM([3N])  AS 'marNet',
                  SUM([4N])  AS 'aprNet',
                  SUM([5N])  AS 'mayNet',
                  SUM([6N])  AS 'junNet',
                  SUM([7N])  AS 'julNet',
                  SUM([8N])  AS 'augNet',
                  SUM([9N])  AS 'sepNet',
                  SUM([10N]) AS 'octNet',
                  SUM([11N]) AS 'novNet',
                  SUM([12N]) AS 'decNet',
                  SUM([1G])  AS 'janGross',
                  SUM([2G])  AS 'febGross',
                  SUM([3G])  AS 'marGross',
                  SUM([4G])  AS 'aprGross',
                  SUM([5G])  AS 'mayGross',
                  SUM([6G])  AS 'junGross',
                  SUM([7G])  AS 'julGross',
                  SUM([8G])  AS 'augGross',
                  SUM([9G])  AS 'sepGross',
                  SUM([10G]) AS 'octGross',
                  SUM([11G]) AS 'novGross',
                  SUM([12G]) AS 'decGross',
                  SUM([1L])  AS 'janLiab',
                  SUM([2L])  AS 'febLiab',
                  SUM([3L])  AS 'marLiab',
                  SUM([4L])  AS 'aprLiab',
                  SUM([5L])  AS 'mayLiab',
                  SUM([6L])  AS 'junLiab',
                  SUM([7L])  AS 'julLiab',
                  SUM([8L])  AS 'augLiab',
                  SUM([9L])  AS 'sepLiab',
                  SUM([10L]) AS 'octLiab',
                  SUM([11L]) AS 'novLiab',
                  SUM([12L]) AS 'decLiab',
                  SUM([1R])  AS 'janRetain',
                  SUM([2R])  AS 'febRetain',
                  SUM([3R])  AS 'marRetain',
                  SUM([4R])  AS 'aprRetain',
                  SUM([5R])  AS 'mayRetain',
                  SUM([6R])  AS 'junRetain',
                  SUM([7R])  AS 'julRetain',
                  SUM([8R])  AS 'augRetain',
                  SUM([9R])  AS 'sepRetain',
                  SUM([10R]) AS 'octRetain',
                  SUM([11R]) AS 'novRetain',
                  SUM([12R]) AS 'decRetain'
          FROM    (
                  SELECT  [agentID],
                          [stateID],
                          [periodYear],
                          [netMonth],
                          [grossMonth],
                          [liabilityMonth],
                          [retainedMonth],
                          [batchMonth],
                          [netPremiumDelta],
                          [grossPremiumDelta],
                          [liabilityDelta],
                          [retainedPremiumDelta],
                          [batchID]
                  FROM    (
                          SELECT  DISTINCT  
                                  b.[batchID],
                                  b.[agentID],
                                  b.[stateID],
                                  b.[periodYear],
                                  CONVERT(VARCHAR,b.[periodMonth]) + 'N' AS 'netMonth',
                                  CONVERT(VARCHAR,b.[periodMonth]) + 'G' AS 'grossMonth',
                                  CONVERT(VARCHAR,b.[periodMonth]) + 'L' AS 'liabilityMonth',
                                  CONVERT(VARCHAR,b.[periodMonth]) + 'R' AS 'retainedMonth',
                                  CONVERT(VARCHAR,b.[periodMonth]) + 'B' AS 'batchMonth',
                                  b.[netPremiumDelta],
                                  b.[grossPremiumDelta],
                                  b.[liabilityDelta],
                                  b.[retainedPremiumDelta]
                          FROM    [dbo].[batch] b INNER JOIN
                                  #agentTable agentTable
                          ON      b.[agentID] = agentTable.[agentID] INNER JOIN
                                  #stateTable stateTable
                          ON      b.[stateID] = stateTable.[stateID] INNER JOIN
                                  #yearTable yearTable
                          ON      b.[periodYear] = yearTable.[year]
                          ) b
                  ) src
                  PIVOT
                  (
                  SUM([netPremiumDelta])
                  FOR [netMonth] in ([1N],[2N],[3N],[4N],[5N],[6N],[7N],[8N],[9N],[10N],[11N],[12N])
                  ) p
                  PIVOT
                  (
                  SUM([grossPremiumDelta])
                  FOR [grossMonth] in ([1G],[2G],[3G],[4G],[5G],[6G],[7G],[8G],[9G],[10G],[11G],[12G])
                  ) p
                  PIVOT
                  (
                  SUM([liabilityDelta])
                  FOR [liabilityMonth] in ([1L],[2L],[3L],[4L],[5L],[6L],[7L],[8L],[9L],[10L],[11L],[12L])
                  ) p
                  PIVOT
                  (
                  SUM([retainedPremiumDelta])
                  FOR [retainedMonth] in ([1R],[2R],[3R],[4R],[5R],[6R],[7R],[8R],[9R],[10R],[11R],[12R])
                  ) p
                  PIVOT
                  (
                  COUNT([batchID])
                  FOR [batchMonth] in ([1B],[2B],[3B],[4B],[5B],[6B],[7B],[8B],[9B],[10B],[11B],[12B])
                  ) p
          GROUP BY [agentID],
                   [stateID],
                   [periodYear]
          ) batch INNER JOIN
          (
          SELECT  [agentID],
                  [stateID],
                  [periodYear],
                  SUM([1T])  AS 'janGap',
                  SUM([2T])  AS 'febGap',
                  SUM([3T])  AS 'marGap',
                  SUM([4T])  AS 'aprGap',
                  SUM([5T])  AS 'mayGap',
                  SUM([6T])  AS 'junGap',
                  SUM([7T])  AS 'julGap',
                  SUM([8T])  AS 'augGap',
                  SUM([9T])  AS 'sepGap',
                  SUM([10T]) AS 'octGap',
                  SUM([11T]) AS 'novGap',
                  SUM([12T]) AS 'decGap',
                  SUM([1F])  AS 'janFiles',
                  SUM([2F])  AS 'febFiles',
                  SUM([3F])  AS 'marFiles',
                  SUM([4F])  AS 'aprFiles',
                  SUM([5F])  AS 'mayFiles',
                  SUM([6F])  AS 'junFiles',
                  SUM([7F])  AS 'julFiles',
                  SUM([8F])  AS 'augFiles',
                  SUM([9F])  AS 'sepFiles',
                  SUM([10F]) AS 'octFiles',
                  SUM([11F]) AS 'novFiles',
                  SUM([12F]) AS 'decFiles',
                  SUM([1P])  AS 'janPolicies',
                  SUM([2P])  AS 'febPolicies',
                  SUM([3P])  AS 'marPolicies',
                  SUM([4P])  AS 'aprPolicies',
                  SUM([5P])  AS 'mayPolicies',
                  SUM([6P])  AS 'junPolicies',
                  SUM([7P])  AS 'julPolicies',
                  SUM([8P])  AS 'augPolicies',
                  SUM([9P])  AS 'sepPolicies',
                  SUM([10P]) AS 'octPolicies',
                  SUM([11P]) AS 'novPolicies',
                  SUM([12P]) AS 'decPolicies'
          FROM    (
                  SELECT  [agentID],
                          [stateID],
                          [periodYear],
                          CONVERT(VARCHAR,b.[periodMonth]) + 'T' AS 'gapMonth',
                          CONVERT(VARCHAR,b.[periodMonth]) + 'F' AS 'fileMonth',
                          CONVERT(VARCHAR,b.[periodMonth]) + 'P' AS 'policyMonth',
                          SUM([dayCnt]) / COUNT(*) AS 'reportGap',
                          COUNT(DISTINCT [fileNumber]) AS 'fileCount',
                          COUNT(DISTINCT [policyID]) AS 'policyCount'
                  FROM    (
                          SELECT  DISTINCT  
                                  b.[batchID],
                                  bf.[fileNumber],
                                  bf.[policyID],
                                  b.[agentID],
                                  b.[stateID],
                                  b.[periodYear],
                                  b.[periodMonth],
                                  DATEDIFF(DAY,bf.[effDate],b.[receivedDate]) AS 'dayCnt'
                          FROM    [dbo].[batchform] bf INNER JOIN
                                  [dbo].[batch] b
                          ON      b.[batchID] = bf.[batchID] INNER JOIN
                                  #agentTable agentTable
                          ON      b.[agentID] = agentTable.[agentID] INNER JOIN
                                  #stateTable stateTable
                          ON      b.[stateID] = stateTable.[stateID] INNER JOIN
                                  #yearTable yearTable
                          ON      b.[periodYear] = yearTable.[year]
                          ) b
                  GROUP BY [agentID],
                           [stateID],
                           [periodYear],
                           [periodMonth]
                  ) src
                  PIVOT
                  (
                  SUM([reportGap])
                  FOR [gapMonth] in ([1T],[2T],[3T],[4T],[5T],[6T],[7T],[8T],[9T],[10T],[11T],[12T])
                  ) p
                  PIVOT
                  (
                  SUM([fileCount])
                  FOR [fileMonth] in ([1F],[2F],[3F],[4F],[5F],[6F],[7F],[8F],[9F],[10F],[11F],[12F])
                  ) p
                  PIVOT
                  (
                  SUM([policyCount])
                  FOR [policyMonth] in ([1P],[2P],[3P],[4P],[5P],[6P],[7P],[8P],[9P],[10P],[11P],[12P])
                  ) p
          GROUP BY [agentID],
                   [stateID],
                   [periodYear]
          ) gap
  ON      batch.[agentID] = gap.[agentID]
  AND     batch.[stateID] = gap.[stateID]
  AND     batch.[periodYear] = gap.[periodYear] INNER JOIN
          [dbo].[agent] agent
  ON      agent.[agentID] = batch.[agentID] INNER JOIN
          [dbo].[agentmanager] am
  ON      agent.[agentID] = am.[agentID]
  AND     am.[isPrimary] = 1
  AND     am.[stat] = 'A'
  GROUP BY batch.[agentID],
           agent.[name],
           agent.[stateID],
           batch.[periodYear],
           agent.[stat],
           am.[uid]
  ORDER BY batch.[agentID],
           agent.[stateID],
           batch.[periodYear]

  DROP TABLE #yearTable
  DROP TABLE #agentTable
  DROP TABLE #stateTable
END
