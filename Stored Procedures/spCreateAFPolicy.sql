/****** Object:  StoredProcedure [dbo].[spCreateAFPolicy]    Script Date: 3/20/2020 8:16:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spCreateAFPolicy]
	-- Add the parameters for the stored procedure here
	@pcActionID VARCHAR(100)    = '',
	@pcClientID VARCHAR(100)    = '',
	@isError    BIT = 0      OUTPUT,
	@errMsg     VARCHAR(500) OUTPUT	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
  
  
  SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  DECLARE @agentID VARCHAR(20),
          @fileNumber VARCHAR(100),
		  @fileID VARCHAR(100),
          @addr1 VARCHAR(100) = '',
          @addr2 VARCHAR(100) = '',
          @addr3 VARCHAR(100) = '',
          @addr4 VARCHAR(100) = '',
          @city VARCHAR(100) = '',
          @countyID VARCHAR(50) = '',
          @state VARCHAR(2) = '',
          @zip VARCHAR(20) = '',
          @stage VARCHAR(20),
          @transactionType VARCHAR(20) = '',
          @insuredType VARCHAR(20) = '',
          @reportingDate DATETIME = NULL,
          @liability DECIMAL(18,2) = 0,
          @notes VARCHAR(MAX),
          @agentFileID INTEGER,
		  @transCount BIT = 0
	
	SET @isError = 0
	SET @errMsg  = ''

	-- Insert statements for procedure here
	CREATE TABLE #fileID
	(
		[agentID] VARCHAR(20),
		[policyID] INTEGER,
		[liability] DECIMAL(18,2),
		[fileNumber] VARCHAR(1000),
		[fileID] VARCHAR(1000)
	)

	INSERT INTO #fileID ([agentID],[policyID],[liability],[fileNumber],[fileID])
	SELECT  [agentID],
			[policyID], 
			[liabilityAmount], 
			[fileNumber],
			[fileID]
	FROM    [dbo].[policy]
	WHERE   ([fileID] <> '' AND [fileID] IS NOT NULL)
	AND     ([agentID] <> '' AND [agentID] IS NOT NULL) 
	AND NOT EXISTS(SELECT agentFileID from agentfile af WHERE af.agentID = [policy].[agentID] AND
															af.fileID  = [policy].[fileID])



	--Insert into agentfile and sysnote
	DECLARE cur CURSOR LOCAL FOR
	SELECT  [agentID],
			[fileNumber], 
			[fileID],
			[liability], 
			@pcActionID  AS [stage],
			@pcActionID + ', '  + @pcClientID + ',' + ' Created missing AgentFile for Policy ' + CONVERT(VARCHAR,[policyID])  AS [notes]
	FROM    #fileID f

	BEGIN TRY
		IF(@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @transCount = 1
		END

		OPEN cur
		FETCH NEXT FROM cur INTO @agentID, @fileNumber, @fileID, @liability,  @stage, @notes
		WHILE @@FETCH_STATUS = 0 
		BEGIN
			SET @agentFileID = 0
			EXEC [dbo].[spCreateMissingAF] @agentID, @fileNumber, @fileID, @addr1, @addr2, @addr3, @addr4, @city, @countyID, @state, @zip, @stage, @transactionType, @insuredType, @reportingDate, @liability, @notes, @agentFileID OUTPUT, @isError OUTPUT, @errMsg OUTPUT
			IF(@isError = 1)
				RAISERROR(@errMsg,16,1)

			EXEC [dbo].[spInsertSysNote] 'AF', @agentFileID, @notes, @isError OUTPUT, @errMsg OUTPUT
			IF(@isError = 1)
				RAISERROR(@errMsg,16,1)

			FETCH NEXT FROM cur INTO @agentID, @fileNumber, @fileID, @liability,  @stage, @notes
		END
		CLOSE cur
		DEALLOCATE cur

		DROP TABLE #fileID

		IF(@transCount = 1)
			COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		SET @isError = ERROR_STATE()
		SET @errMsg  = ERROR_MESSAGE()
		IF(@transCount = 1)
			ROLLBACK TRANSACTION
		RETURN	
	END CATCH
END
