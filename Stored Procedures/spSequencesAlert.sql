/****** Object:  StoredProcedure [dbo].[spSequencesAlert]    Script Date: 4/27/2022 12:53:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>

-- Description:	THIS PROCEDURE IS NOT USED ANYWHERE

-- =============================================
ALTER PROCEDURE [dbo].[spSequencesAlert]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
  SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  CREATE TABLE #tempseq
    (
      [seqname]     VARCHAR(50),
	  [maximumVal]  VARCHAR(50),
	  [currentVal]  VARCHAR(50),
	  [exhausted]   DECIMAL(38,0),
    )

  INSERT INTO #tempseq
  SELECT 
      seqname    = TRIM(CAST(s.name AS CHARACTER)),
	  maximumVal = CAST(s.maximum_value AS DECIMAL(38,0)),
	  currentVal = CAST(s.current_value  AS DECIMAL(38,0)),
	  exhausted  = (CAST(s.current_value AS DECIMAL(38,0)) / CAST(s.maximum_value AS DECIMAL(38,0)) * 100)  /*--calculating the percentage for exhausted current value of sequence--*/
  FROM sys.sequences as s
	
/*-------if exhausted percentage is greater or equals to 95 percent-----------*/
  SELECT * FROM #tempseq WHERE #tempseq.exhausted >= (SELECT [description] FROM [dbo].[syscode] 
    WHERE syscode.codeType = 'SequencesAlert' and
          syscode.code     = 'Threshold'  );  

  DROP TABLE #tempseq

END
