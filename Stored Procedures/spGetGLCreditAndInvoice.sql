
/****** Object:  StoredProcedure [dbo].[spGetGLCreditAndInvoice]    Script Date: 7/4/2021 11:04:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetGLCreditAndInvoice] 
	-- Add the parameters for the stored procedure here
	@agentID     VARCHAR(MAX)  = 'ALL',
    @recordType  VARCHAR(1)    = 'B',
    @startDate   DATETIME      = NULL,
	@endDate     DATETIME      = NULL,
    @archived    bit           = 0	
AS
BEGIN
    SET NOCOUNT ON;
    -- Insert statements for procedure here
    CREATE TABLE #glTransaction (agentID         VARCHAR(80),
	                             artranid		integer,
								 TYPE            VARCHAR(1), 
                                 Reference       VARCHAR(50),
                                 tranID          VARCHAR(50),
                                 postingDate     DATETIME,
                                 argldef         VARCHAR(500),
				                 arglref         VARCHAR(500),
		                         debit           [decimal](18, 2) NULL,
     				             credit          [decimal](18, 2) NULL,
								 voided          VARCHAR(20) default '',
								 notes		     VARCHAR(MAX)
     	 						)
  /* For Credit records */				  
  IF (@recordType = 'C') or (@recordType = 'A')
  BEGIN
    /*------- Debit And Credit-----------*/			
    INSERT INTO #glTransaction ( agentid,type,reference, tranID,postingdate, debit, credit, arglref, notes)       
    SELECT ar.entityid,ar.type,ar.reference, ar.tranid,ar.trandate , l.debitAmount,  l.creditAmount , l.accountID,
		   CASE WHEN ar.voidDate IS NOT NULL THEN ('Credit voided ' + CONVERT(VARCHAR, ar.voidDate, 101)) ELSE '' END AS [notes] 
	FROM artran ar 
	      INNER JOIN arMisc m 
	ON ar.tranId = m.arMiscID 
		  INNER JOIN ledger l
	ON m.ledgerID = l.ledgerID
    WHERE ar.tranDate >= @startDate 
	      AND ar.tranDate <= @endDate 
		  AND ar.entityid = (CASE WHEN @agentID  = 'ALL' THEN ar.entityid  ELSE @agentID  END) 
		  AND (ar.type = 'C' AND ar.seq = 0)

    /*-------Void Debit And  Void Credit-----------*/			
    INSERT INTO #glTransaction ( agentid,type,reference, tranID,postingdate, debit, credit, arglref, voided, notes)    
   
    SELECT  ar.entityid,ar.type,ar.reference, ar.tranid,ar.voidDate, l.debitAmount, l.creditAmount , l.accountID  , 'Yes',
			'Void credit ' + CONVERT(VARCHAR, ar.tranDate, 101) 
    FROM artran ar
          INNER JOIN arMisc m 
	ON ar.tranId = m.arMiscID 
	      INNER JOIN ledger l
	ON m.voidledgerID = l.ledgerID 
    WHERE ar.voidDate >= @startDate 
          AND ar.voidDate <= @endDate 
	      AND ar.entityid = (CASE WHEN @agentID  = 'ALL' THEN ar.entityid  ELSE @agentID  END) 
	      AND (ar.type = 'C' AND ar.seq = 0) AND ar.void = 1
    
	    /*-------Refund credit-----------*/			
    INSERT INTO #glTransaction ( agentid,artranid,reference, tranID,postingdate, arglref,credit, notes, type)    
   
    select  ar.entityid,ar.artranid,ar.reference, ar.tranid,ar.trandate,  lg.accountID , abs(ar.tranamt) as tranamt,
			CASE WHEN ar.voidDate IS NOT NULL THEN ('Refund voided ' + CONVERT(VARCHAR, ar.voidDate, 101)) 
			    ELSE ('Credit ' + CONVERT(VARCHAR, m.transDate, 101) + ' refunded') END AS [notes], ar.type
	  from artran ar
	     left outer join ledger lg
        on ar.ledgerID = lg.ledgerID
		 left outer join arMisc m 
	     ON ar.tranId = m.arMiscID 
	  where ar.tranDate >= @startDate 
	    and ar.tranDate <= @endDate 
		and ar.entityid = (case when @agentID  = 'ALL' then ar.entityid  else @agentID  end) 
		and (ar.type = 'C' and ar.seq >= 1) and ar.tranAmt < 0
		and lg.creditAmount > 0
		
	/*-------Refund Debit-----------*/			
    INSERT INTO #glTransaction ( agentid,artranid,reference, tranID,postingdate, arglref,debit, notes, type)    
   
    select  ar.entityid,ar.artranid,ar.reference, ar.tranid,ar.trandate,  lg.accountID , abs(ar.tranamt) as tranamt,
			CASE WHEN ar.voidDate IS NOT NULL THEN ('Refund voided ' + CONVERT(VARCHAR, ar.voidDate, 101)) 
			          ELSE ('Credit ' + CONVERT(VARCHAR, m.transDate, 101) + ' refunded') END AS [notes], ar.type
	  from artran ar
	     left outer join ledger lg
        on ar.ledgerID = lg.ledgerID
		 left outer join arMisc m 
	    ON ar.tranId = m.arMiscID
	  where ar.tranDate >= @startDate 
	    and ar.tranDate <= @endDate 
		and ar.entityid = (case when @agentID  = 'ALL' then ar.entityid  else @agentID  end) 
		and (ar.type = 'C' and ar.seq >= 1) and ar.tranAmt < 0
		and lg.debitAmount > 0

    /*-------Void Refund credit ------*/			
    INSERT INTO #glTransaction ( agentid,artranid,reference, tranID,postingdate, arglref, credit, voided, notes, type)    
   
    select  ar.entityid,ar.artranid,ar.reference, ar.tranid,ar.voidDate,  lg.accountID ,abs(ar.tranamt) as tranamt, 'Yes',
				'Void Refund ' + CONVERT(VARCHAR, [tranDate], 101), ar.type
	  from artran ar
	     left outer join ledger lg
        on ar.voidledgerID = lg.ledgerID 
	  where ar.voidDate >= @startDate 
	    and ar.voidDate <= @endDate 
		and ar.entityid = (case when @agentID  = 'ALL' then ar.entityid  else @agentID  end) 
		and (ar.type = 'C' and ar.seq >= 1)
		and lg.creditAmount > 0
		
	/*-------Void Refund Debit ------*/			
    INSERT INTO #glTransaction ( agentid,artranid,reference, tranID,postingdate, arglref, debit, voided, notes, type)    
   
    select  ar.entityid,ar.artranid,ar.reference, ar.tranid,ar.voidDate,  lg.accountID ,abs(ar.tranamt) as tranamt, 'Yes',
				'Void Refund ' + CONVERT(VARCHAR, [tranDate], 101), ar.type
	  from artran ar
		    left outer join ledger lg
        on ar.voidledgerID = lg.ledgerID 
	  where ar.voidDate >= @startDate 
	    and ar.voidDate <= @endDate 
		and ar.entityid = (case when @agentID  = 'ALL' then ar.entityid  else @agentID  end) 
		and (ar.type = 'C' and ar.seq >= 1)
        and lg.debitAmount > 0	 


  END

  /* For Invoice records */
  IF (@recordType = 'I') OR (@recordType = 'A')
  BEGIN
    /*-------Debit And Credit-----------*/			
    INSERT INTO #glTransaction ( agentid,type,reference, tranID,postingdate, debit, credit, arglref, notes)       
    SELECT ar.entityid,ar.type,ar.reference, ar.tranid,ar.trandate, l.debitAmount, l.creditAmount , l.accountID,
	       CASE WHEN ar.voidDate IS NOT NULL THEN ('Invoice voided ' + CONVERT(VARCHAR, ar.voidDate, 101)) ELSE '' END AS [notes]
    FROM artran ar 
 	  	    INNER JOIN arMisc m 
    ON ar.tranId = m.arMiscID 
	   	    INNER JOIN ledger l
    ON m.ledgerID = l.ledgerID and ar.tranID = l.sourceID 
    	    
    WHERE ar.tranDate >= @startDate 
			AND ar.tranDate <= @endDate 
			AND ar.entityid = (CASE WHEN @agentID  = 'ALL' THEN ar.entityid  ELSE @agentID  END) 
			AND (ar.type = 'I' AND ar.transtype = ' ' AND ar.seq = 0)
			AND l.notes not like 'History Transaction%' /* do not include historic transactions */
    
    /*-------Void Debit And  Void Credit-----------*/			
    INSERT INTO #glTransaction ( agentid,type,reference, tranID,postingdate, debit, credit, arglref, voided, notes)    
    
    SELECT  ar.entityid,ar.type,ar.reference, ar.tranid,ar.voidDate, l.debitAmount, l.creditAmount , l.accountID , 'Yes',
			'Void Invoice ' + CONVERT(VARCHAR, ar.tranDate, 101) 
    FROM artran ar
	        INNER JOIN arMisc m 
    ON ar.tranId = m.arMiscID 
	 		INNER JOIN ledger l
    ON m.voidledgerID = l.ledgerID and ar.tranID = l.sourceID
    WHERE ar.voidDate >= @startDate 
	        AND ar.voidDate <= @endDate 
	    	AND ar.entityid = (CASE WHEN @agentID  = 'ALL' THEN ar.entityid  ELSE @agentID  END) 
		    AND (ar.type = 'I' AND ar.transtype = ' ' AND ar.seq = 0) AND ar.void = 1

  END
  
  /* For Write-off records */				  
  IF (@recordType = 'W') or (@recordType = 'A')
  BEGIN
    /*------- Debit And Credit-----------*/			
    INSERT INTO #glTransaction ( agentid,type,reference, tranID,postingdate, debit, credit, arglref, notes)       
    SELECT ar.entityid,ar.type,ar.reference, ar.tranid,ar.trandate , l.debitAmount,  l.creditAmount , l.accountID,
		   CASE WHEN ar.voidDate IS NOT NULL THEN ('Write-off voided ' + CONVERT(VARCHAR, ar.voidDate, 101)) ELSE '' END AS [notes] 
	FROM artran ar 
	      INNER JOIN arWriteoff w 
	ON ar.tranId = w.arWriteoffID 
		  INNER JOIN ledger l
	ON w.ledgerID = l.ledgerID
    WHERE ar.tranDate >= @startDate 
	      AND ar.tranDate <= @endDate 
		  AND ar.entityid = (CASE WHEN @agentID  = 'ALL' THEN ar.entityid  ELSE @agentID  END) 
		  AND (ar.type = 'W' AND ar.seq = 0)

    /*-------Void Debit And  Void Credit-----------*/			
    INSERT INTO #glTransaction ( agentid,type,reference, tranID,postingdate, debit, credit, arglref, voided, notes)    
   
    SELECT  ar.entityid,ar.type,ar.reference, ar.tranid,ar.voidDate, l.debitAmount, l.creditAmount , l.accountID  , 'Yes',
			'Void Write-off ' + CONVERT(VARCHAR, ar.tranDate, 101) 
    FROM artran ar
          INNER JOIN arWriteoff w 
	ON ar.tranId = w.arWriteoffID 
	      INNER JOIN ledger l
	ON w.voidledgerID = l.ledgerID 
    WHERE ar.voidDate >= @startDate 
          AND ar.voidDate <= @endDate 
	      AND ar.entityid = (CASE WHEN @agentID  = 'ALL' THEN ar.entityid  ELSE @agentID  END) 
	      AND (ar.type = 'W' AND ar.seq = 0) AND ar.void = 1
		  
	IF @archived = 1
	BEGIN
		/*------- Debit And Credit-----------*/			
		INSERT INTO #glTransaction ( agentid,type,reference, tranID,postingdate, debit, credit, arglref, notes)       
		SELECT ar.entityid,ar.type,ar.reference, ar.tranid,ar.trandate , l.debitAmount,  l.creditAmount , l.accountID,
			CASE WHEN ar.voidDate IS NOT NULL THEN ('Write-off voided ' + CONVERT(VARCHAR, ar.voidDate, 101)) ELSE '' END AS [notes] 
		FROM artranh ar 
			INNER JOIN arWriteoff w 
		ON ar.tranId = w.arWriteoffID 
			INNER JOIN ledger l
		ON w.ledgerID = l.ledgerID
		WHERE ar.tranDate >= @startDate 
			AND ar.tranDate <= @endDate 
			AND ar.entityid = (CASE WHEN @agentID  = 'ALL' THEN ar.entityid  ELSE @agentID  END) 
			AND (ar.type = 'W' AND ar.seq = 0)

		/*-------Void Debit And  Void Credit-----------*/			
		INSERT INTO #glTransaction ( agentid,type,reference, tranID,postingdate, debit, credit, arglref, voided, notes)    
   
		SELECT  ar.entityid,ar.type,ar.reference, ar.tranid,ar.voidDate, l.debitAmount, l.creditAmount , l.accountID  , 'Yes',
				'Void Write-off ' + CONVERT(VARCHAR, ar.tranDate, 101) 
		FROM artranh ar
			INNER JOIN arWriteoff w 
		ON ar.tranId = w.arWriteoffID 
			INNER JOIN ledger l
		ON w.voidledgerID = l.ledgerID 
		WHERE ar.voidDate >= @startDate 
			AND ar.voidDate <= @endDate 
			AND ar.entityid = (CASE WHEN @agentID  = 'ALL' THEN ar.entityid  ELSE @agentID  END) 
			AND (ar.type = 'W' AND ar.seq = 0) AND ar.void = 1
	END
    
  END

  UPDATE #glTransaction SET credit = ISNULL(credit,0),
               		        debit  = ISNULL(debit,0)

  DELETE FROM #glTransaction  WHERE credit = 0 and debit = 0
  SELECT * FROM #glTransaction ORDER BY tranID
  DROP TABLE #glTransaction

END
