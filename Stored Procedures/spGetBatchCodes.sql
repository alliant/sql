USE [COMPASS_DVLP]
GO
/****** Object:  StoredProcedure [dbo].[spGetBatchCodes]    Script Date: 1/8/2021 3:04:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetBatchCodes]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SELECT  [type] + [code] AS [err],
          [description]
  FROM    [batchcode]
  ORDER BY [type], [code]
END
