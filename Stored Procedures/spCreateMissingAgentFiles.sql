USE [COMPASS_DVLP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spCreateMissingAgentFiles]
AS
BEGIN
SET NOCOUNT ON;

/* Query to re-normalize the fileID in Policy table where policy.fileid <> normalize(policy.fileNumber) */
update policy 
set policy.fileid = [dbo].[NormalizeFileID] ([fileNumber])
where policy.filenumber <> '' and policy.filenumber is not null and policy.fileid <> [dbo].[NormalizeFileID] ([fileNumber])

/* Query to re-normalize the fileID in CPL table where CPL.fileid <> normalize(CPL.fileNumber) */
update CPL 
set CPL.fileid = [dbo].[NormalizeFileID] ([fileNumber])
where CPL.filenumber <> '' and CPL.filenumber is not null and CPL.fileid <> [dbo].[NormalizeFileID] ([fileNumber])

/* Query to re-normalize the fileID in BatchForm table where BatchForm.fileid <> normalize(BatchForm.fileNumber) */
update BatchForm 
set BatchForm.fileid = [dbo].[NormalizeFileID] ([fileNumber])
where BatchForm.filenumber <> '' and BatchForm.filenumber is not null and BatchForm.fileid <> [dbo].[NormalizeFileID] ([fileNumber])

/* Create missing files from Policy table in agentFile table. */
EXEC [dbo].[spCreateFileIDPolicy]

/* Create missing files from BatchForm table in agentFile table. */
EXEC [dbo].[spCreateFileIDBatchForm]

/* Create missing files from CPL table in agentFile table. */
EXEC [dbo].[spCreateFileIDCPL]

/* Update the Invoice Amount, Outstanding Amount and Paid Amount in AgentFile table */
 UPDATE agentFile
SET agentfile.invoiceAmt = ISNULL((SELECT SUM(netDelta) FROM batchform 
                           WHERE fileID  = agentfile.fileID 
                           AND batchID IN (SELECT batchID FROM batch
                           WHERE batch.agentID = agentfile.agentID )),0),
			   
    agentfile.osAmt = ISNULL((SELECT remainingamt FROM artran 
                      WHERE artran.entity   = 'A'
                       and artran.entityID  = agentfile.agentID      
                       and artran.fileID    = agentfile.fileID
                       and artran.transtype = 'F'
                       and artran.type      = 'F'
                       and artran.seq       = 0),0),   

   agentfile.paidAmt = ISNULL((agentfile.invoiceAmt - agentfile.osAmt),0)
END
