USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetPlatformContacts]    Script Date: 8/21/2019 11:04:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetPlatformContacts]
	-- Add the parameters for the stored procedure here
	@platformID INTEGER = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  SELECT  [firstName] + ' ' + [lastName] AS [name],
          [address1] AS [addr],
          [city],
          [state],
          [zip],
          [active]
  FROM    [person]
  WHERE   [personID] = 0
END
