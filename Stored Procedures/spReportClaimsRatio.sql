USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spReportClaimsRatio]    Script Date: 3/17/2017 3:28:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportClaimsRatio]
  @startDate DATETIME = NULL,
  @endDate DATETIME = NULL,
  @agentError BIT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  DECLARE @insertClaimBalanceDelta [dbo].[ClaimBalanceDelta]
  SELECT * INTO #insertClaimBalanceDelta FROM @insertClaimBalanceDelta

  DECLARE @insertClaimsRatio [dbo].[ClaimsRatio]
  SELECT * INTO #claimsRatio FROM @insertClaimsRatio

  -- Decide the dates
  IF (@startDate IS NULL)
    SET @startDate = '2005-01-01'

  IF (@endDate IS NULL)
    SET @endDate = GETDATE()
    
  -- The end date should be the last second of the day
  SET @endDate = DATEADD(s,-1,DATEADD(d,1,@endDate))

  -- Get the claim balances
  EXEC [dbo].[spReportClaimBalancesDelta] @startDate = @startDate, @endDate = @endDate

  -- Get the deltas
  INSERT INTO #claimsRatio ([agentID],[laeComplete],[lossComplete],[laeReserve],[lossReserve],[recoveries],[pendingRecoveries],[netPremium],[actualCostRatio],[costIncurredRatio],[actualCostAgentErrorRatio],[costIncurredAgentErrorRatio])
  SELECT  COALESCE(b.[agentID],c.[agentID]) AS [agentID],
          ISNULL(CASE WHEN @agentError = 0 THEN c.[laeCompletePayments] ELSE c.[laeCompletePaymentsAgentError] END,0) AS [laeComplete],
          ISNULL(CASE WHEN @agentError = 0 THEN c.[lossCompletePayments] ELSE c.[lossCompletePaymentsAgentError] END,0) AS [lossComplete],
          ISNULL(CASE WHEN @agentError = 0 THEN c.[laeReserveBalance] ELSE c.[laeReserveBalanceAgentError] END,0) AS [laeReserve],
          ISNULL(CASE WHEN @agentError = 0 THEN c.[lossReserveBalance] ELSE c.[lossReserveBalanceAgentError] END,0) AS [lossReserve],
          ISNULL(c.[recoveries],0) AS [recoveries],
          ISNULL(c.[pendingRecoveries],0) AS [pendingRecoveries],
          ISNULL(b.[netPremium],0) AS [netPremium],
         (ISNULL(c.[laeCompletePayments],0) +
          ISNULL(c.[lossCompletePayments],0) -
          ISNULL(c.[recoveries],0)) / 
          ISNULL(CASE WHEN b.[netPremium] = 0 THEN NULL ELSE b.[netPremium] END,1) AS [actualCostRatio],
         (ISNULL(c.[laeCompletePayments],0) +
          ISNULL(c.[lossCompletePayments],0) +
          ISNULL(c.[laeReserveBalance],0) +
          ISNULL(c.[lossReserveBalance],0) -
          ISNULL(c.[recoveries],0)) / 
          ISNULL(CASE WHEN b.[netPremium] = 0 THEN NULL ELSE b.[netPremium] END,1) AS [costIncurredRatio],
         (ISNULL(c.[laeCompletePaymentsAgentError],0) +
          ISNULL(c.[lossCompletePaymentsAgentError],0) -
          ISNULL(c.[recoveries],0)) / 
          ISNULL(CASE WHEN b.[netPremium] = 0 THEN NULL ELSE b.[netPremium] END,1) AS [actualCostAgentErrorRatio],
         (ISNULL(c.[laeCompletePaymentsAgentError],0) +
          ISNULL(c.[lossCompletePaymentsAgentError],0) +
          ISNULL(c.[laeReserveBalanceAgentError],0) +
          ISNULL(c.[lossReserveBalanceAgentError],0) -
          ISNULL(c.[recoveries],0)) / 
          ISNULL(CASE WHEN b.[netPremium] = 0 THEN NULL ELSE b.[netPremium] END,1) AS [costIncurredAgentErrorRatio]
  FROM    [agent] a LEFT OUTER JOIN
          (
          SELECT  [agentID],
                  CONVERT(DECIMAL(18,2),ISNULL(SUM(batch.[netPremiumDelta]),0)) AS [netPremium]
          FROM    [dbo].[batch] INNER JOIN
                  [dbo].[period]
          ON      batch.[periodID] = period.[periodID]
          WHERE   period.[startDate] >= @startDate
          AND     period.[endDate] <= @endDate
          GROUP BY [agentID]
          ) b
  ON      a.[agentID] = b.[agentID] LEFT OUTER JOIN
          (
          SELECT  c.[agentID],
                  SUM(d.[laeCompletedInvoiceAmount]) AS [laeCompletePayments],
                  SUM(d.[laeReserveBalance]) AS [laeReserveBalance],
                  SUM(d.[lossCompletedInvoiceAmount]) AS [lossCompletePayments],
                  SUM(d.[lossReserveBalance]) AS [lossReserveBalance],
                  SUM(CASE WHEN c.[agentError] = 'Y' THEN d.[laeCompletedInvoiceAmount] ELSE 0 END) AS [laeCompletePaymentsAgentError],
                  SUM(CASE WHEN c.[agentError] = 'Y' THEN d.[lossCompletedInvoiceAmount] ELSE 0 END) AS [lossCompletePaymentsAgentError],
                  SUM(CASE WHEN c.[agentError] = 'Y' THEN d.[laeReserveBalance] ELSE 0 END) AS [laeReserveBalanceAgentError],
                  SUM(CASE WHEN c.[agentError] = 'Y' THEN d.[lossReserveBalance] ELSE 0 END) AS [lossReserveBalanceAgentError],
                  SUM(d.[pendingRecoveries]) AS [pendingRecoveries],
                  SUM(d.[paidRecoveries]) AS [recoveries]
          FROM    [dbo].[claim] c INNER JOIN
                  #insertClaimBalanceDelta d
          ON      c.[claimID] = d.[claimID]
          GROUP BY c.[agentID]
          ) c
  ON      b.[agentID] = c.[agentID]

  IF object_id('tempdb..#insertClaimsRatio') IS NULL
    SELECT  [agentID],
            [laeComplete] AS [laePaidDelta],
            [lossComplete] AS [lossPaidDelta],
            [laeReserve] AS [laeReserveDelta],
            [lossReserve] AS [lossReserveDelta],
            [recoveries] AS [recoveriesDelta],
            [pendingRecoveries],
            [netPremium]
    FROM    #claimsRatio
    ORDER BY [agentID]
  ELSE
    INSERT INTO #insertClaimsRatio ([agentID],[laeComplete],[lossComplete],[laeReserve],[lossReserve],[recoveries],[pendingRecoveries],[netPremium],[actualCostRatio],[costIncurredRatio],[actualCostAgentErrorRatio],[costIncurredAgentErrorRatio])
    SELECT  [agentID],
            [laeComplete],
            [lossComplete],
            [laeReserve],
            [lossReserve],
            [recoveries],
            [pendingRecoveries],
            [netPremium],
            [actualCostRatio],
            [costIncurredRatio],
            [actualCostAgentErrorRatio],
            [costIncurredAgentErrorRatio]
    FROM    #claimsRatio

  DROP TABLE #insertClaimBalanceDelta
  DROP TABLE #claimsRatio
END
