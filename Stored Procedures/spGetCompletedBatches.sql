-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
USE [COMPASS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author:		<Rahul Sharma>
-- Create date: <21-11-2019>
-- Description:	<Get batch records>
-- =============================================
ALTER PROCEDURE [dbo].[spGetCompletedBatches] 
	-- Add the parameters for the stored procedure here
	@piPeriodID    INTEGER   = 0,
	@plIncludeAll  BIT       = 0,
	@plOnlyPosted  BIT       = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    
	-- Insert statements for procedure here
    
	CREATE TABLE #arbatch (batchID              INTEGER,
	                       periodID             INTEGER,
						   yearID               INTEGER,
                           stateID              VARCHAR(10),						   
	                       agentID              VARCHAR(20),
						   agentName            VARCHAR(200),
						   agentStat            VARCHAR(10),						   
						   createDate           DATETIME,
						   receivedDate         DATETIME,
						   invoiceDate          DATETIME,
						   tranDate             DATETIME, 
						   liabilityDelta       [DECIMAL](18, 2) NULL,
						   grossPremiumDelta    [DECIMAL](18, 2) NULL,
						   netPremiumDelta      [DECIMAL](18, 2) NULL,
						   retainedPremiumDelta [DECIMAL](18, 2) NULL,						  
						   fileCount            INTEGER,
						   policyCount          INTEGER,
						   endorsementCount     INTEGER,
						   cplCount             INTEGER,
						   posted               BIT,
						   agentManager         VARCHAR(100),
						   ledgerID             INTEGER,
						   reference            VARCHAR(100))   
			
	INSERT INTO #arbatch (batchID, periodID, yearID, stateID, agentID, createDate, receivedDate, invoiceDate, tranDate, liabilityDelta, 
	                      grossPremiumDelta, netPremiumDelta, retainedPremiumDelta, fileCount, policyCount, endorsementCount, cplCount, posted, ledgerID, reference)
						  
    SELECT b.batchID, b.periodID, b.yearID, b.stateID, b.agentID, b.createDate, b.receivedDate, b.invoiceDate, b.transDate, b.liabilityDelta,
           b.grossPremiumDelta, b.netPremiumDelta, b.retainedPremiumDelta, b.fileCount, b.policyCount, b.endorsementCount, b.cplCount, b.posted, b.ledgerID, b.reference  	
      FROM batch b
      WHERE b.periodID = (CASE WHEN @piPeriodID = 0  THEN b.periodID ELSE @piPeriodID  END)
	    AND b.stat     = 'C'
		AND ((@plIncludeAll = 0 
		AND ((@plOnlyPosted  = 1 AND b.posted = @plOnlyPosted) OR (@plOnlyPosted = 0 AND b.posted = 0))) OR @plIncludeAll = 1)
	
    UPDATE #arbatch
    SET #arbatch.agentName = agent.name,
	    #arbatch.agentStat = agent.stat
	  FROM agent 
	  WHERE #arbatch.agentID = agent.agentID
	
    UPDATE #arbatch
    SET #arbatch.agentManager = agentManager.UID
	  FROM agentManager 
	  WHERE #arbatch.agentID       = agentManager.agentID	
	    AND agentManager.stat      = 'A'
        AND agentManager.isPrimary = 1
	
    SELECT * FROM #arbatch ORDER BY #arbatch.batchID	
		
END
GO
