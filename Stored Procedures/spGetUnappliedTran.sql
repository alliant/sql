USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetUnappliedTran]    Script Date: 7/5/2021 9:28:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<MK>
-- Create date: <01-03-2021>
-- Description:	<Get Unapplied Payments/Credits Report>
-- =============================================
ALTER PROCEDURE [dbo].[spGetUnappliedTran] 
	-- Add the parameters for the stored procedure here 
	@entity            varchar(1)   = 'A',
	@entityID          varchar(10)  = 'ALL',
	@plIncludePayments BIT          = 0,
	@plIncludeCredits  BIT          = 0,
	@searchString      varchar(MAX) = '',
	@fromDate          datetime     = null,
	@toDate            datetime     = null
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  -- Temporary table definition
  create table #UnappliedTran  (unappliedtranID  integer,
								revenuetype      varchar(50),    /* revenue type of payment */
								entity           varchar(50),
								entityID         varchar(50),
								tranID           VARCHAR(50),
								arTranID         VARCHAR(50),
								reference        varchar(50),
								referencedate    datetime,
								receiptdate      datetime,
								referenceamt     [decimal](18, 2) NULL,
								remainingamt     [decimal](18, 2) NULL,
								void             bit,
								voiddate         datetime,
								voidby           varchar(100),
								voidbyusername   varchar(100),
								description      varchar(max),
								createddate      datetime,
								createdby        varchar(100),
								username         varchar(100),
								filenumber       varchar(100),
								fileID           varchar(100),
								fileExist        varchar(3) default 'NO',
								posted           bit,            /* true if the payment is posted */
								transType        varchar(20),    /* 'F' if payment is file bed else '' */
								archived         bit,
								postdate         datetime,
								postby           varchar(100),
								postbyname       varchar(100),
								entityname       varchar(100),            /* Client side */
								transDate        datetime,                /* Client side */
								appliedamt       [decimal](18, 2) NULL,   /* Client side */
								refundamt        [decimal](18, 2) NULL,
                                type             varchar(20) /* 'P' or 'C' if payment or credit type tran */								
								)
 
  IF @plIncludePayments = 1								
  BEGIN
    /*-------Posted/Refund Payments-----------*/
	insert into #UnappliedTran (unappliedtranID, revenuetype, entity, entityID, tranID ,artranID, reference, referencedate, 
						receiptdate, referenceamt , void, voiddate, voidby, description, 
						createddate, createdby, filenumber, fileID, posted, transType, archived, 
						postdate, postby, entityname, transDate, type)
	select ap.arpmtID, ap.revenuetype, ap.entity, ap.entityID,at.tranID ,at.artranID, ap.checknum, ap.checkdate, 
			ap.receiptdate, ap.checkamt , ap.void, ap.voiddate, ap.voidby, ap.description, 
			ap.createddate, ap.createdby, ap.filenumber, ap.fileID, ap.posted, ap.transType, ap.archived, 
			ap.postdate, ap.postby, a.name, at.tranDate, at.type  
		from arpmt ap 
			inner join agent a 
		on  ap.EntityID = a.agentID 
			inner join artran at 
		on  ap.arpmtID = at.tranID and at.type = 'P' and seq = 0
		where ap.entity    = @entity 
		  AND ap.entityID  = (CASE WHEN @entityID = 'ALL' THEN ap.entityID  ELSE @entityID END)
		  and ((@searchString <> '' and ap.checknum LIKE '%' + @searchString + '%' ) or @searchString = '' )
		  AND ap.transDate >= (CASE WHEN @fromDate IS NULL THEN ap.transDate  ELSE @fromDate END) 
		  AND ap.transDate <= (CASE WHEN @toDate   IS NULL THEN ap.transDate  ELSE @toDate   END)
		  and ap.posted     = 1
		  AND (ap.void = 0 OR ap.void IS NULL)
		  AND (at.remainingAmt > 0)
          
  UPDATE #UnappliedTran
     SET #UnappliedTran.appliedAmt = (SELECT SUM(atn.tranAmt) FROM artran atn WHERE atn.entity     = 'A'
																		and atn.entityID   = #UnappliedTran.entityID
																		and atn.sourceType = 'P'
																		and atn.sourceID   = CAST(#UnappliedTran.arTranID as varchar)
																		and atn.type       = 'A'
																	GROUP BY atn.entityID,atn.sourceID),
			#UnappliedTran.refundamt = (select sum(atn.tranAmt) from artran atn where atn.entity    = 'A'
																			and atn.entityID  = #UnappliedTran.entityID
																			and atn.tranID    = #UnappliedTran.tranID
																			and atn.type      = 'P'
																			and atn.seq       > 0
																	GROUP BY atn.entityID,atn.tranID),
            #UnappliedTran.username = (select sysuser.name from sysuser where sysuser.UID = #UnappliedTran.createdby),
		    #UnappliedTran.voidbyusername = (select sysuser.name from sysuser where sysuser.UID = #UnappliedTran.voidby),
			#UnappliedTran.postbyname = (select sysuser.name from sysuser where sysuser.UID = #UnappliedTran.postby),

	        #UnappliedTran.fileExist = (select 'YES' where not EXISTS(select 1 from agentfile 
		                                                   where agentfile.agentID = #UnappliedTran.entityID and
                                                                 agentfile.fileID  = #UnappliedTran.fileID))
  UPDATE #UnappliedTran
     SET #UnappliedTran.remainingAmt = #UnappliedTran.referenceAmt + ISNULL(#UnappliedTran.appliedAmt,0) + ISNULL(#UnappliedTran.refundamt,0)
  
  END
  ELSE IF @plIncludeCredits = 1								
  BEGIN
    /*-------Posted/Refund Payments-----------*/
	insert into #UnappliedTran (unappliedtranID, revenuetype, entity, entityID, tranID ,artranID, reference, referencedate,
	                    referenceamt , void, voiddate, voidby, description, createddate, createdby, filenumber, fileID, posted, 
						transType, archived, postdate, postby, entityname, transDate, type)
	select am.armiscID, am.revenuetype, am.entity, am.entityID,at.tranID ,at.artranID, am.reference, am.transDate, 
		    am.transamt , am.void, am.voiddate, am.voidedBy, am.notes, 
			am.createdate, am.createdby, am.filenumber, am.fileID, am.posted, am.transType, am.archived, 
			am.postdate, am.postedBy, a.name, at.tranDate, at.type  
		from armisc am 
			inner join agent a 
		on  am.EntityID = a.agentID 
			inner join artran at 
		on  am.armiscID = at.tranID and at.type = 'C'  and seq = 0
		where am.entity    = @entity 
		  AND am.entityID  = (CASE WHEN @entityID = 'ALL' THEN am.entityID  ELSE @entityID END)
		  and ((@searchString <> '' and ((am.reference LIKE '%' + @searchString + '%' ) or (cast(am.armiscID as varchar) LIKE '%' + @searchString + '%' ))) or @searchString = '' )
		  AND am.transDate >= (CASE WHEN @fromDate IS NULL THEN am.transDate  ELSE @fromDate END) 
		  AND am.transDate <= (CASE WHEN @toDate   IS NULL THEN am.transDate  ELSE @toDate   END)
		  and am.posted     = 1
		  AND (am.void = 0 OR am.void IS NULL)

		  and (at.remainingAmt > 0)
          
  UPDATE #UnappliedTran
     SET #UnappliedTran.reference = cast(#UnappliedTran.unappliedtranID as varchar) + ' ' + #UnappliedTran.reference,
	        #UnappliedTran.appliedAmt = (SELECT SUM(atn.tranAmt) FROM artran atn WHERE atn.entity     = 'A'
																		and atn.entityID   = #UnappliedTran.entityID
																		and atn.sourceType = 'C'
																		and atn.sourceID   = CAST(#UnappliedTran.arTranID as varchar)
																		and atn.type       = 'A'
																	GROUP BY atn.entityID,atn.sourceID),
			#UnappliedTran.refundamt = (select sum(atn.tranAmt) from artran atn where atn.entity    = 'A'
																			and atn.entityID  = #UnappliedTran.entityID
																			and atn.tranID    = #UnappliedTran.tranID
																			and atn.type      = 'C'
																			and atn.seq       > 0
																	GROUP BY atn.entityID,atn.tranID),
            #UnappliedTran.username = (select sysuser.name from sysuser where sysuser.UID = #UnappliedTran.createdby),
		    #UnappliedTran.voidbyusername = (select sysuser.name from sysuser where sysuser.UID = #UnappliedTran.voidby),
			#UnappliedTran.postbyname = (select sysuser.name from sysuser where sysuser.UID = #UnappliedTran.postby),

	        #UnappliedTran.fileExist = (select 'YES' where not EXISTS(select 1 from agentfile 
		                                                   where agentfile.agentID = #UnappliedTran.entityID and
                                                                 agentfile.fileID  = #UnappliedTran.fileID))
  UPDATE #UnappliedTran
     SET #UnappliedTran.remainingAmt = #UnappliedTran.referenceAmt + ISNULL(#UnappliedTran.appliedAmt,0) + ISNULL(#UnappliedTran.refundamt,0)
  
  END
  
  select * from #UnappliedTran order by entityID

END
GO