-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetStates]
	-- Add the parameters for the stored procedure here
	@stateID VARCHAR(50) = '',
  @status VARCHAR(50) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  -- Insert statements for procedure here
	SELECT  [stateID],
          [seq],
          [description],
          [active],
          [appDate],
          [license],
          [licEffDate],
          [licExpDate],
          [cancelDate],
          [lastAuditDate],
          [name],
          [addr1],
          [addr2],
          [addr3],
          [addr4],
          [city],
          [county],
          [state],
		  [region],
          [zip],
          [contact],
          [phone],
          [fax],
          [email],
          [website],
          [comments],
          [externalApproval],
          CASE WHEN d.[entityID] IS NOT NULL 
            THEN 'True'
            ELSE 'False'
          END AS [hasDocument]
  FROM    [dbo].[state] s LEFT OUTER JOIN
          [sysdoc] d
  ON      s.[stateID] = d.[entityID]
  AND     d.[entityType] = 'State'
  WHERE   s.[stateID] = CASE WHEN @stateID = '' THEN s.[stateID] ELSE @stateID END
  AND   ((@status = 'A' AND s.[active] = 1) OR (@status = 'I' AND s.[active] = 0) OR (@status = ''))
  ORDER BY s.[stateID]
END
GO
