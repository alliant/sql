USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spReportQARSeverityChanges]    Script Date: 11/6/2017 1:28:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportQARSeverityChanges]
	-- Add the parameters for the stored procedure here
	@auditID INTEGER = 0,
  @year INTEGER = 0,
  @stateID VARCHAR(3) = 'ALL',
  @agentID VARCHAR(MAX) = 'ALL',
  @auditor VARCHAR(50) = 'ALL'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;
  
  CREATE TABLE #agentTable ([agentID] VARCHAR(30))
  INSERT INTO #agentTable ([agentID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)
  
  -- Add all the qars
  CREATE TABLE #auditTable ([auditID] INTEGER, [year] INTEGER, [auditor] VARCHAR(50),  [score] INTEGER,  [grade] INTEGER, [stateID] VARCHAR(2), [agentID] VARCHAR(50), [agentName] VARCHAR(200))
  IF (@auditID > 0)
  BEGIN
    INSERT INTO #auditTable ([auditID], [year], [auditor],  [score], [grade], [stateID], [agentID])
    SELECT  q.[qarID],
            q.[auditYear],
            q.[uid],
	    q.[score],
	    q.[grade],
            a.[stateID],
            a.[agentID]
    FROM    [dbo].[qar] q INNER JOIN
            [dbo].[agent] a
    ON      q.[agentID] = a.[agentID]
    WHERE   q.[qarID] = @auditID
  END
  ELSE
  BEGIN
    INSERT INTO #auditTable ([auditID], [year], [auditor],  [score], [grade], [stateID], [agentID])
    SELECT  q.[qarID],
            q.[auditYear],
            q.[uid],
	    q.[score],
	    q.[grade],
            a.[stateID],
            a.[agentID]
    FROM    [dbo].[qar] q INNER JOIN
            [dbo].[agent] a
    ON      q.[agentID] = a.[agentID]

    -- Delete if the year isn't correct
    IF (@year > 0)
      DELETE FROM #auditTable WHERE [year] <> @year

    -- Delete if the auditor is not correct
    IF (@auditor <> 'ALL')
      DELETE FROM #auditTable WHERE [auditor] <> @auditor

    -- Delete if the state is not correct
    IF (@stateID <> 'ALL')
      DELETE FROM #auditTable WHERE [stateID] <> @stateID

    -- Delete if the state is not correct
    IF (@agentID <> 'ALL')
      DELETE FROM #auditTable WHERE [agentID] NOT IN (SELECT [agentID] FROM #agentTable)
  END

  SELECT  q.[qarID],
          q.[version],
	  q.[score],
	  q.[grade],
          qq.[questionID],
          qq.[description] AS 'questionDesc',
          (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'QAR' AND [objAction] = 'Audit' AND [objProperty] = 'Severity' AND [objID] =  qq.[severity]) AS 'originalSeverity',
          (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'QAR' AND [objAction] = 'Audit' AND [objProperty] = 'Severity' AND [objID] =  qf.[severity]) AS 'reportedSeverity',
          qf.[comments] AS 'changeDesc',
          q.[agentID],
          (SELECT [name] FROM [dbo].[agent] WHERE [agentID] = q.[agentID]) AS 'agentName',
          q.[auditor],
          a.[stateID] AS 'state',
          q.[auditFinishDate],
          (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'QAR' AND [objAction] = 'Audit' AND [objProperty] = 'Type' AND [objID] =  q.[auditType]) AS 'auditType',
          ISNULL((SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'QAR' AND [objAction] = 'ERR' AND [objProperty] = 'Type' AND [objID] =  q.[errType]),'') AS 'errType'
  FROM    [dbo].[qar] q INNER JOIN
          #auditTable a
  ON      q.[qarID] = a.[auditID] INNER JOIN
          [dbo].[qarfinding] qf
  ON      q.[qarID] = qf.[qarID] INNER JOIN
          [dbo].[qarquestion] qq
  ON      qf.[questionSeq] = qq.[seq]
  AND     q.[version] = qq.[version]
  WHERE   qq.[severity] <> qf.[severity]
  ORDER BY [qarID]

  DROP TABLE #agentTable
  DROP TABLE #auditTable
END
