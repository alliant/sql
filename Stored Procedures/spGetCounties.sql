USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetCounties]    Script Date: 3/20/2019 3:15:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetCounties]
	-- Add the parameters for the stored procedure here
	@stateID VARCHAR(2) = '',
  @countyID VARCHAR(20) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  CREATE TABLE #regionTable
  (
    [regionID] VARCHAR(10),
    [stateID] VARCHAR(10)
  )

  INSERT INTO #regionTable ([regionID],[stateID])
  EXEC [dbo].[spGetRegions]

	SELECT  [stateID],
          [countyID],
          [description],
          (SELECT [regionID] FROM #regionTable WHERE [stateID] = c.[stateID]) AS [regionID],
          [month],
          [year]
  FROM    [dbo].[county] c
  WHERE   [stateID] = CASE WHEN @stateID = '' THEN [stateID] ELSE @stateID END
  AND     [countyID] = CASE WHEN @countyID = '' THEN [countyID] ELSE @countyID END
END
