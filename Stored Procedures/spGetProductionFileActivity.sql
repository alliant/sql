GO
/****** Object:  StoredProcedure [dbo].[spGetProductionFileActivity]    Script Date: 2/18/2025 12:09:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetProductionFileActivity]  
	-- Add the parameters for the stored procedure here
	@agentID                 VARCHAR(MAX) = 'ALL',
	@startDate               DATETIME     = NULL,
	@endDate                 DATETIME     = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
     CREATE TABLE #fileAR (agentfileID	   INTEGER      DEFAULT 0,
	                       agentID         VARCHAR(50)  DEFAULT '',
						   agent           VARCHAR(250) DEFAULT '',
						   agentName       VARCHAR(200) DEFAULT '',
						   fileID          VARCHAR(50)  DEFAULT '',
						   tranDate        DATETIME,
						   amount          [decimal](18, 2) NULL DEFAULT 0,
						   invoicedAmount  [decimal](18, 2) NULL DEFAULT 0,
     				       cancelledAmount [decimal](18, 2) NULL DEFAULT 0,
                           paidAmount      [decimal](18, 2) NULL DEFAULT 0,
						   propertyInfo    VARCHAR(500) DEFAULT ''
     	 		           )

	 CREATE TABLE #agentFile (agentfileid  varchar(80),
					      	  agentID      VARCHAR(80),
	                          fileNumber   VARCHAR(50),
		        		      fileID       VARCHAR(50),
						      periodCount  INTEGER,
							  trandate     date )

     CREATE TABLE #agentTable ([agentID] VARCHAR(30))
     INSERT INTO #agentTable ([agentID])
     SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)

	 INSERT INTO #agentFile ([agentfileid],[agentID],[fileID],[periodCount],[trandate])
	 SELECT fileAR.agentFileID, 
       fileAR.agentID,
       fileAR.fileID,
		COUNT(CASE WHEN fileAR.tranDate >= CAST(@startDate AS DATE) /* file is fulfilled or has activity during statement period  */
                    AND fileAR.tranDate <= CAST(@endDate AS DATE)
					AND fileAR.Type = 'I'
                    THEN 1 ELSE NULL END),
        Max(CASE WHEN fileAR.tranDate <= CAST(@endDate AS DATE) /* latest invoice date as of statement end date */
                    AND fileAR.Type = 'I'						   
                    THEN fileAR.tranDate ELSE NULL END)
    FROM [dbo].[fileAR]  
    INNER JOIN 
    [dbo].[agent] a 
    ON a.agentID = fileAR.agentid
    WHERE fileAR.[agentid] IN (SELECT [agentID] FROM #agentTable)
		AND (fileAR.type = 'I' AND (fileAR.tranDate >= CAST(@startDate AS DATE) AND fileAR.tranDate <= CAST(@endDate AS DATE)))
	GROUP BY fileAR.agentid,fileAR.fileID,fileAR.agentFileID

	UPDATE #agentFile set fileNumber = (select fileNumber from agentfile where agentfile.agentFileID = #agentFile.agentfileid)
	--SELECT * from #agentFile
	DROP TABLE  #agentTable

	/* calculate toatal invoice amount */
	INSERT INTO #fileAR( [agentfileID], [agentID], [agent], [agentName], [fileID], [invoicedAmount],[cancelledAmount],[paidAmount])
    SELECT af.agentfileID, 
		   af.agentID, 
		   (SELECT [name] FROM [dbo].[agent] WHERE [agentID] = af.[agentID]) + ' (' + af.agentID + ')', 
		   (SELECT [name] FROM [dbo].[agent] WHERE [agentID] = af.[agentID]) as [agentName],
		   af.fileNumber, 
		   COALESCE((SELECT SUM(fr.amount) FROM fileAR fr 
		             WHERE fr.agentFileID = af.agentfileid AND fr.type = 'I' AND (fr.tranDate >= CAST(@startDate AS DATE) AND fr.tranDate <= CAST(@endDate AS DATE))),0),  /* file total invoiced amount  */
           COALESCE((SELECT SUM(fr.amount) FROM fileAR fr 
		             WHERE fr.agentFileID = af.agentfileid AND fr.type = 'W' AND (fr.tranDate >= CAST(@startDate AS DATE) AND fr.tranDate <= CAST(@endDate AS DATE))),0),  /* file total write-off amount   */
	       COALESCE((SELECT SUM(fr.amount) FROM fileAR fr 
		             WHERE fr.agentFileID = af.agentfileid AND fr.type = 'A' AND (fr.tranDate >= CAST(@startDate AS DATE) AND fr.tranDate <= CAST(@endDate AS DATE))),0)  /* file total applied amount   */
	FROM fileAR fa
    INNER JOIN #agentFile af ON (af.agentFileID = fa.agentFileID)
	WHERE fa.tranDate >= CAST(@startDate AS DATE)
       AND fa.tranDate <= CAST(@endDate AS DATE)
	  GROUP BY af.agentfileid,af.agentID,af.fileID,af.fileNumber
	
	UPDATE #fileAR
	   SET #fileAR.tranDate = af.tranDate
	FROM #agentFile af
	WHERE af.agentfileid = #fileAR.agentfileID
	  AND af.agentID     = #fileAR.agentID
	  AND af.fileNumber  = #fileAR.fileID

    UPDATE #fileAR
       SET #fileAR.propertyInfo =
       TRIM(', ' FROM
        (CASE WHEN fileproperty.addr1 > '' THEN fileproperty.addr1 ELSE '' END) +
        (CASE WHEN fileproperty.addr2 > ''
              THEN CASE WHEN fileproperty.addr1 > '' THEN ', ' ELSE '' END + fileproperty.addr2 ELSE '' END) +
        (CASE WHEN fileproperty.addr3 > ''
              THEN CASE WHEN fileproperty.addr1 > '' OR fileproperty.addr2 > '' THEN ', ' ELSE '' END + fileproperty.addr3 ELSE '' END) +
        (CASE WHEN fileproperty.addr4 > ''
              THEN CASE WHEN fileproperty.addr1 > '' OR fileproperty.addr2 > '' OR fileproperty.addr3 > '' THEN ', ' ELSE '' END + fileproperty.addr4 ELSE '' END) +
        (CASE WHEN fileproperty.city > ''
              THEN CASE WHEN fileproperty.addr1 > '' OR fileproperty.addr2 > '' OR fileproperty.addr3 > '' OR fileproperty.addr4 > '' THEN ', ' ELSE '' END + fileproperty.city ELSE '' END) +
        (CASE WHEN fileProperty.countyID > ''
              AND EXISTS(SELECT * FROM county WHERE county.countyID = fileProperty.countyID AND county.stateID = fileproperty.stateID)
              THEN CASE WHEN fileproperty.addr1 > '' OR fileproperty.addr2 > '' OR fileproperty.addr3 > '' OR fileproperty.addr4 > '' OR fileproperty.city > '' THEN ', ' ELSE '' END +
                  (SELECT county.description FROM county WHERE county.countyID = fileProperty.countyID AND county.stateID = fileproperty.stateID)
             ELSE ''
         END) +
        (CASE WHEN fileproperty.stateID > ''
              THEN CASE WHEN fileproperty.addr1 > '' OR fileproperty.addr2 > '' OR fileproperty.addr3 > '' OR fileproperty.addr4 > '' OR fileproperty.city > '' OR
                               (fileProperty.countyID > '' AND EXISTS(SELECT * FROM county WHERE county.countyID = fileProperty.countyID AND county.stateID = fileproperty.stateID))
                    THEN ', ' ELSE '' END + fileproperty.stateID
              ELSE ''
         END) +
        (CASE WHEN fileproperty.zip > ''
              THEN CASE WHEN fileproperty.addr1 > '' OR fileproperty.addr2 > '' OR fileproperty.addr3 > '' OR fileproperty.addr4 > '' OR fileproperty.city > '' OR
                               fileproperty.stateID > '' OR
                               (fileProperty.countyID > '' AND EXISTS(SELECT * FROM county WHERE county.countyID = fileProperty.countyID AND county.stateID = fileproperty.stateID))
              THEN ', ' ELSE '' END + fileproperty.zip
              ELSE ''
         END))
    FROM fileProperty
    INNER JOIN #fileAR fa ON fileproperty.agentFileID = fa.agentfileid

	SELECT * FROM #fileAR

END
