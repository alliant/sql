USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spReportClaimByAgent]    Script Date: 3/22/2017 12:52:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportClaimSummary]
	@agentID VARCHAR(MAX) = 'ALL',
  @stateID VARCHAR(10) = 'ALL',
  @status VARCHAR(5) = '',
  @assignedTo VARCHAR(50) = '',
  @dormantDays INTEGER = 0,
  @startDate DATETIME = NULL,
  @endDate DATETIME = NULL,
  @UID varchar(100)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  SET @agentID = [dbo].[StandardizeAgentID](@agentID)

  --Insert all claims into the temp table
  CREATE TABLE #claimTable ([claimID] INTEGER, [stateID] VARCHAR(50), [agentID] VARCHAR(50))
  INSERT INTO #claimTable ([claimID], [stateID], [agentID])
  SELECT [claimID], [stateID], [agentID] FROM [dbo].[claim]

  DELETE FROM #claimTable WHERE [agentID] NOT IN (SELECT [agentID] FROM [dbo].[agent] WHERE ([agentID] = (CASE  WHEN @agentID != 'ALL' THEN @agentID ELSE [agentID] END)) 
                                                                                      AND  (dbo.CanAccessAgent(@UID , [agentID]) = 1) )
  
  -- Get the state
  CREATE TABLE #stateTable ([stateID] VARCHAR(2))
  INSERT INTO #stateTable ([stateID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('State', @stateID)
  DELETE FROM #claimTable WHERE [stateID] NOT IN (SELECT [stateID] FROM #stateTable)

  DECLARE @NextString NVARCHAR(100),
          @Pos INTEGER = 0,
          @Str NVARCHAR(MAX),
          @Delimiter NCHAR(1) = ','
  
  IF (@status <> '')
  BEGIN
    DELETE FROM #claimTable
    WHERE [claimID] IN (SELECT [claimID] FROM [dbo].[claim] WHERE [stat] <> @status)
  END

  IF (@status <> '')
  BEGIN
    DELETE FROM #claimTable
    WHERE [claimID] IN (SELECT [claimID] FROM [dbo].[claim] WHERE [assignedTo] <> @assignedTo)
  END

  IF (@dormantDays > 0)
  BEGIN
    SET @dormantDays = @dormantDays - (@dormantDays * 2)
    
    DELETE FROM #claimTable
    WHERE [claimID] NOT IN (SELECT [claimID] FROM [dbo].[claimnote] GROUP BY [claimID] HAVING MAX([noteDate]) > DATEADD(d, @dormantDays, GETDATE()))
  END

  -- Decide the dates
  IF (@startDate IS NULL)
    SET @startDate = '2005-01-01'

  IF (@endDate IS NULL)
    SET @endDate = DATEADD(d,-1,DATEADD(m, DATEDIFF(m,0,GETDATE())+1,0))
    
  -- The end date should be the last second of the day
  SET @endDate = DATEADD(s,-1,DATEADD(d,1,@endDate))

  -- Get the claim balances

  DECLARE @insertClaimBalanceDelta [dbo].[ClaimBalanceDelta]
  SELECT * INTO #insertClaimBalanceDelta FROM @insertClaimBalanceDelta
  EXEC [dbo].[spReportClaimBalancesDelta] @startDate = @startDate, @endDate = @endDate

 -- Insert statements for procedure here
  SELECT  claim.[claimID],
          claim.[stat],
          (SELECT [objValue] FROM [sysprop] WHERE [appCode] = 'CLM' AND [objAction] = 'ClaimDescription' AND [objProperty] = 'Status' AND [objID] = claim.[stat]) AS [statDesc],
          claim.[description],
          claim.[stage],
          (SELECT [objValue] FROM [sysprop] WHERE [appCode] = 'CLM' AND [objAction] = 'ClaimDescription' AND [objProperty] = 'Stage' AND [objID] = claim.[stage]) AS [stageDesc],
          claim.[type],
          (SELECT [objValue] FROM [sysprop] WHERE [appCode] = 'CLM' AND [objAction] = 'ClaimDescription' AND [objProperty] = 'Type' AND [objID] = claim.[type]) AS [typeDesc],
          claim.[action],
          (SELECT [objValue] FROM [sysprop] WHERE [appCode] = 'CLM' AND [objAction] = 'ClaimDescription' AND [objProperty] = 'Action' AND [objID] = claim.[action]) AS [actionDesc],
          claim.[difficulty],
          claim.[urgency],
          claim.[stateID],
          agent.[agentID],
          agent.[name] AS [agentName],
          claim.[dateCreated] AS [dateOpened],
          CONVERT(BIT,CASE WHEN claim.[agentError] = 'Y' THEN 1 ELSE 0 END) AS [agentError],
          (SELECT [objValue] FROM [sysprop] WHERE [appCode] = 'CLM' AND [objAction] = 'ClaimDescription' AND [objProperty] = 'AgentError' AND [objID] = claim.[agentError]) AS [agentErrorDesc],
          claim.[fileNumber],
          claim.[assignedTo],
          note.[lastNote],
          note.[noteCnt],
          CONVERT(VARCHAR(MAX),ISNULL(claimcode.[descriptionCode],'')) AS [descriptionCode],
          CONVERT(VARCHAR(MAX),ISNULL(claimcode.[descriptionDesc],'')) AS [descriptionDesc],
          CONVERT(VARCHAR(MAX),ISNULL(claimcode.[causeCode],'')) AS [causeCode],
          CONVERT(VARCHAR(MAX),ISNULL(claimcode.[causeDesc],'')) AS [causeDesc],
          claim.[altaResponsibility] AS [altaResponsibilityCode],
          CONVERT(VARCHAR(MAX),ISNULL((SELECT [description] FROM [syscode] WHERE [code] = claim.[altaResponsibility] and [codeType] = 'ClaimAltaResp'),'')) AS [altaResponsibilityDesc],
          claim.[altaRisk] AS 'altaRiskCode',
          CONVERT(VARCHAR(MAX),ISNULL((SELECT [description] FROM [syscode] WHERE [code] = claim.[altaRisk] and [codeType] = 'ClaimAltaRisk'),'')) AS [altaRiskDesc],
          balance.[laePendingInvoiceAmount] AS [laeOpen],
          balance.[laeApprovedInvoiceAmount] AS [laeApproved],
          balance.[laeCompletedInvoiceAmount] AS [laeLTD],
          balance.[laePendingReserveAmount] AS [laeAdjOpen],
          balance.[laeApprovedReserveAmount] AS [laeAdjLTD],
          balance.[laeReserveBalance] AS [laeReserve],
          balance.[lossPendingInvoiceAmount] AS [lossOpen],
          balance.[lossApprovedInvoiceAmount] AS [lossApproved],
          balance.[lossCompletedInvoiceAmount] AS [lossLTD],
          balance.[lossPendingReserveAmount] AS [lossAdjOpen],
          balance.[lossApprovedReserveAmount] AS [lossAdjLTD],
          balance.[lossReserveBalance] AS [lossReserve],
          ISNULL(recoveries.[recoveries],0) AS [recoveries],
          ISNULL(recoveries.[pendingRecoveries],0) AS [pendingRecoveries],
          ISNULL(recoveries.[pendingDeductibleRecoveries],0) AS [pendingDeductibleRecoveries],
          ISNULL(recoveries.[deductibleRecoveries],0) AS [deductibleRecoveries],
          CONVERT(VARCHAR,ISNULL(recoveries.[deductibleInvoice],'')) AS [deductibleInvoice],
          CONVERT(VARCHAR(MAX),ISNULL(claimcoverage.[policyID],'')) AS [policyID],
          claimcoverage.[policyDate],
          CONVERT(VARCHAR(MAX),ISNULL(claimcoverage.[policyType],'')) AS [policyType],
          CONVERT(VARCHAR(MAX),ISNULL(claimcoverage.[insuredName],'')) AS [insuredName],
          CONVERT(VARCHAR(MAX),ISNULL(claimcoverage.[insuredAddr],'')) AS [insuredAddr]
  FROM    [dbo].[claim] INNER JOIN
          #claimTable claimTable
  ON      claimTable.[claimID] = claim.[claimID] INNER JOIN
          [dbo].[agent]
  ON      claim.[agentID] = agent.[agentID] LEFT OUTER JOIN
          (
          SELECT  [claimID],
                  MAX([noteDate]) AS 'lastNote',
                  COUNT([seq]) AS 'noteCnt'
          FROM    [dbo].[claimnote]
          GROUP BY [claimID]
          ) note
  ON      claim.[claimID] = note.[claimID] LEFT OUTER JOIN
          (
          SELECT  cc2.[claimID],
                  SUBSTRING(
                  (
                    SELECT  ', ' + cc.[code] AS [text()]
                    FROM    [claimcode] cc
                    WHERE   cc.[claimID] = cc2.[claimID]
                    AND     cc.[codeType] = 'ClaimDescription'
                    ORDER BY cc.[claimID]
                    FOR XML PATH('')
                  ), 3, 1000) AS 'descriptionCode',
                  SUBSTRING(
                  (
                    SELECT  ', ' + sc.[description] AS [text()]
                    FROM    [syscode] sc INNER JOIN
                            [claimcode] cc
                    ON      cc.[code] = sc.[code]
                    AND     cc.[codeType] = sc.[codeType]
                    WHERE   cc.[claimID] = cc2.[claimID]
                    AND     sc.[codeType] = 'ClaimDescription'
                    ORDER BY cc.[claimID]
                    FOR XML PATH('')
                  ), 3, 1000) AS 'descriptionDesc',
                  SUBSTRING(
                  (
                    SELECT  ', ' + cc.[code] AS [text()]
                    FROM    [claimcode] cc
                    WHERE   cc.[claimID] = cc2.[claimID]
                    AND     cc.[codeType] = 'ClaimCause'
                    ORDER BY cc.[claimID]
                    FOR XML PATH('')
                  ), 3, 1000) AS 'causeCode',
                  SUBSTRING(
                  (
                    SELECT  ', ' + sc.[description] AS [text()]
                    FROM    [syscode] sc INNER JOIN
                            [claimcode] cc
                    ON      cc.[code] = sc.[code]
                    AND     cc.[codeType] = sc.[codeType]
                    WHERE   cc.[claimID] = cc2.[claimID]
                    AND     sc.[codeType] = 'ClaimCause'
                    ORDER BY cc.[claimID]
                    FOR XML PATH('')
                  ), 3, 1000) AS 'causeDesc'
          FROM    [claimcode] cc2
          GROUP BY cc2.[claimID]
          ) claimcode
  ON      claim.[claimID] = claimcode.[claimID] LEFT OUTER JOIN
          (
          SELECT  cc.[claimID],
                  cc.[coverageID] AS 'policyID',
                  COALESCE(cc.[effDate],p.[effDate]) AS 'policyDate',
                  sf.[description] AS 'policyType',
                  cc.[insuredName],
                  '' AS 'insuredAddr'
          FROM    [dbo].[claimcoverage] cc INNER JOIN
                  [dbo].[policy] p
          ON      cc.[coverageID] = CONVERT(VARCHAR,p.[policyID]) INNER JOIN
                  [dbo].[stateform] sf
          ON      p.[formID] = sf.[formID]
          AND     p.[stateID] = sf.[stateID]
          WHERE   cc.[seq] = 1
          ) claimcoverage
  ON      claim.[claimID] = claimcoverage.[claimID] LEFT OUTER JOIN
          #insertClaimBalanceDelta balance
  ON      claim.[claimID] = balance.[claimID] LEFT OUTER JOIN
          (
          SELECT  CONVERT(INTEGER,inv.[refID]) AS 'claimID',
                  SUM(CASE WHEN inv.[stat] = 'O' OR inv.[stat] = 'A' THEN ISNULL([requestedAmount],0) ELSE 0 END) - 
                  SUM(CASE WHEN inv.[stat] = 'O' OR inv.[stat] = 'A' THEN ISNULL([transAmount],0) ELSE 0 END) AS 'pendingRecoveries',
                  SUM(ISNULL([transAmount],0)) AS 'recoveries',
                  SUM(CASE WHEN (inv.[stat] = 'O' OR inv.[stat] = 'A') AND inv.[refCategory] = 'C' THEN ISNULL([requestedAmount],0) ELSE 0 END) - 
                  SUM(CASE WHEN (inv.[stat] = 'O' OR inv.[stat] = 'A') AND inv.[refCategory] = 'C' THEN ISNULL([transAmount],0) ELSE 0 END) AS 'pendingDeductibleRecoveries',
                  SUM(CASE WHEN inv.[refCategory] = 'C' THEN ISNULL([transAmount],0) ELSE 0 END) AS 'deductibleRecoveries',
                  SUBSTRING(
                  (
                    SELECT  ', ' + CONVERT(VARCHAR, inv2.[arinvID]) AS [text()]
                    FROM    [arinv] inv2
                    WHERE   inv2.[refID] = inv.[refID]
                    AND     inv2.[refCategory] = 'C'
                    ORDER BY inv2.[arinvID]
                    FOR XML PATH('')
                  ), 3, 1000) AS 'deductibleInvoice'
          FROM    [arinv] inv LEFT OUTER JOIN
                  [artrx] trx
          ON      inv.[arinvID] = trx.[arinvID]
          WHERE   inv.[dateRequested] BETWEEN @startDate AND @endDate
          GROUP BY inv.[refID]
          ) recoveries
  ON      claim.[claimID] = recoveries.[claimID]


  DROP TABLE #stateTable
  DROP TABLE #claimTable
  DROP TABLE #insertClaimBalanceDelta
END
