
/****** Object:  StoredProcedure [dbo].[spGetActiveAgents]    Script Date: 4/4/2023 9:16:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetActiveAgents]
	-- Add the parameters for the stored procedure here
   	@StateIDList VARCHAR(200) = '',
	@UID VARCHAR(100) = ''
AS
BEGIN  

	CREATE TABLE #tempAgent
	(
		[agentID]         VARCHAR(20),
		[stateID]         VARCHAR(2),
		[stateName]       VARCHAR(50),		
		[statusCode]      VARCHAR(1),
		[StatusDesc]      VARCHAR(40),
		[name]            VARCHAR(200),
		[legalName]       VARCHAR(200),
		[address]         VARCHAR(350),
		[addrState]       VARCHAR(2),
		[addrStateName]   VARCHAR(50),
		[region]          VARCHAR(50) ,
		[regionDesc]      VARCHAR(100)
	)

   if (@StateIDList <> '')
      INSERT INTO #tempAgent([agentID],[stateID],[stateName],[statusCode],[statusDesc],[name],[legalName],[address],[addrState],[addrStateName],[region] ,[regionDesc])
	  SELECT a.[agentID],
	         a.[stateID],
		     (SELECT [description] FROM [dbo].[state] WHERE [stateID] = a.[stateID]) AS [stateName],
		     a.[stat] AS [statusCode],
		     (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'AMD' AND [objAction] = 'agent' AND [objProperty] = 'Status' AND [objID] = a.[stat]) AS [statusDesc],
		     a.[name],
		     a.[legalName],
		     a.[addr1] + ' ' + a.[addr2] + ' ' + a.[addr3] + ' ' + a.[addr4] AS [address],
		     a.[state] AS [addrState],
		    (SELECT [description] FROM [dbo].[state] WHERE [stateID] = a.[state]) AS [addrStateName],
		     a.[region],
		    (select [description] from [dbo].[syscode] where [codeType] = 'Region' AND [code] = a.[region]) AS [regionDesc]	
	  FROM [dbo].[agent] a 
	  WHERE a.[agentID] IN (select agentID from [dbo].[office] where a.[agentID] = office.agentID)
	  AND  a.[stat] IN ('A','P')
	  AND  a.[stateID] IN (SELECT value FROM STRING_SPLIT(@StateIDList,',') )
	  AND  (dbo.CanAccessAgent(@UID , a.[agentID]) = 1)
   ELSE
     INSERT INTO #tempAgent([agentID],[stateID],[stateName],[statusCode],[statusDesc],[name],[legalName],[address],[addrState],[addrStateName],[region] ,[regionDesc])
	 SELECT a.[agentID],
	         a.[stateID],
		     (SELECT [description] FROM [dbo].[state] WHERE [stateID] = a.[stateID]) AS [stateName],
		     a.[stat] AS [statusCode],
		     (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'AMD' AND [objAction] = 'agent' AND [objProperty] = 'Status' AND [objID] = a.[stat]) AS [statusDesc],
		     a.[name],
		     a.[legalName],
		     a.[addr1] + ' ' + a.[addr2] + ' ' + a.[addr3] + ' ' + a.[addr4] AS [address],
		     a.[state] AS [addrState],
		    (SELECT [description] FROM [dbo].[state] WHERE [stateID] = a.[state]) AS [addrStateName],
		     a.[region],
		    (select [description] from [dbo].[syscode] where [codeType] = 'Region' AND [code] = a.[region]) AS [regionDesc]	
	  FROM [dbo].[agent] a
	  WHERE a.[agentID] IN (select agentID from [dbo].[office] where a.[agentID] = office.agentID)
	  AND a.[stat] IN ('A','P')  
	  AND  a.[stateID] IN ( a.[stateID] )
	  AND  (dbo.CanAccessAgent(@UID , a.[agentID]) = 1)


    SELECT * FROM #tempAgent

	DROP TABLE #tempAgent

END
