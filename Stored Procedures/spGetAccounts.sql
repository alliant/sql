USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetBatchCounts]    Script Date: 8/9/2016 2:38:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		  John Oliver
-- Create date: 5/16/2016
-- Description:	Gets the vendors from GP
-- =============================================
ALTER PROCEDURE [dbo].[spGetAccounts]
	-- Add the parameters for the stored procedure here
  @accountID VARCHAR(30) = 'ALL'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  -- Build the report
  SELECT  RTRIM(a.[ACTNUMST]) AS [acct],
          RTRIM(am.[ACTDESCR]) AS [acctname],
          ISNULL(s.[stateID],'U') AS [state]
  FROM    [ANTIC].[dbo].[GL00100] am INNER JOIN
          [ANTIC].[dbo].[GL00105] a
  ON      a.[ACTINDX] = am.[ACTINDX] LEFT OUTER JOIN
          [dbo].[state] s
  ON      s.[seq] = CONVERT(INTEGER,SUBSTRING(RTRIM(a.[ACTNUMST]),LEN(RTRIM(a.[ACTNUMST]))-1,2))
  AND     s.[active] = 1
  ORDER BY a.[ACTNUMST]
END