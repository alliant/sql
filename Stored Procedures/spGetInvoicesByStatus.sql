GO
/****** Object:  StoredProcedure [dbo].[spGetInvoicesByStatus]    Script Date: 1/10/2017 1:39:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetInvoicesByStatus] 
  @status VARCHAR(100) = 'O',
  @vendorName VARCHAR(1000) = '',
  @startDate DATETIME = NULL,
  @endDate DATETIME = NULL,
  @type VARCHAR(100) = 'ALL'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  CREATE TABLE #statusTable
  (
    [stat] VARCHAR(20)
  )
  
  CREATE TABLE #typeTable
  (
    [type] VARCHAR(20)
  )

  DECLARE @tempStatus VARCHAR(20),
          @aptrxStatus VARCHAR(20),
          @pos INT = 0,
          @strLen INT
           
  SET @strLen = LEN(@status)
  
  WHILE @pos < @strLen
  BEGIN
    SET @pos = @pos + 1
    SET @tempStatus = SUBSTRING(@status,@pos,1)

    INSERT INTO #statusTable
    SELECT  @tempStatus
  END

  SET @aptrxStatus = 'C'
  IF (@status = 'V')
    SET @aptrxStatus = 'V'

  IF (@startDate IS NULL)
  BEGIN
    SET @startDate = '2005-01-01'
  END

  IF (@endDate IS NULL)
  BEGIN
    SET @endDate = DATEADD(d,1,GETDATE())
  END

  IF (@type = 'ALL')
  BEGIN
    INSERT INTO #typeTable
    SELECT [refType] FROM [apinv] GROUP BY [refType]
  END
  ELSE
  BEGIN
    INSERT INTO #typeTable
    SELECT @type
  END
  
  CREATE TABLE #invoiceTable
  (
    [apinvID] INTEGER,
    [aptrxID] INTEGER,
    [stat] VARCHAR(50),
    [invoiceType] VARCHAR(1),
    [refDescription] VARCHAR(50),
    [refType] VARCHAR(20),
    [refID] VARCHAR(100),
    [refCategory] VARCHAR(100),
    [vendorID] VARCHAR(50),
    [vendorName] VARCHAR(200),
    [invoiceNumber] VARCHAR(50),
    [invoiceDate] DATETIME,
    [recDate] DATETIME,
    [amount] DECIMAL(18,2),
    [age] INTEGER,
    [hasDocument] BIT,
    [contention] BIT,
    [appDate] DATETIME,
    [transAmount] DECIMAL(18,2),
    [transDate] DATETIME,
    [uid] VARCHAR(1000),
    [approval] VARCHAR(1000)
  )

  -- Get the payables
  INSERT INTO #invoiceTable ([apinvID],[aptrxID],[stat],[invoiceType],[refDescription],[refType],[refID],[refCategory],[vendorID],[vendorName],[invoiceNumber],[invoiceDate],[recDate],[amount],[age],[hasDocument],[contention],[appDate],[transAmount],[transDate],[uid],[approval])
  SELECT  inv.[apinvID],
          inv.[aptrxID],
          inv.[stat],
          inv.[invoiceType],
          inv.[refDescription],
          inv.[refType],
          inv.[refID],
          inv.[refCategory],
          inv.[vendorID],
          inv.[vendorName],
          inv.[invoiceNumber],
          inv.[invoiceDate],
          inv.[recDate],
          inv.[amount],
          CASE 
           WHEN inv.[age] < 0 THEN 0
           ELSE inv.[age]
          END AS 'age',
          inv.[hasDocument],
          inv.[contention],
          inv.[appDate],
          inv.[transAmount],
          inv.[transDate],
          inv.[uid],
          inv.[approval]
  FROM    (
          SELECT  inv.[apinvID],
                  ISNULL(trx.[aptrxID],0) AS 'aptrxID',
                  inv.[stat],
                  CASE
                   WHEN inv.[refType] = 'C' THEN 'Claim'
                  END AS 'refDescription',
                  inv.[refType],
                  inv.[refID],
                  inv.[contention],
                  'P' AS 'invoiceType',
                  inv.[refCategory],
                  inv.[vendorID],
                  inv.[vendorName],
                  inv.[invoiceNumber],
                  inv.[invoiceDate],
                  inv.[dateReceived] AS 'recDate',
                  ISNULL(inv.[invoiceDate],@startDate) AS 'searchDate',
                  CASE
                   WHEN inv.[stat] = 'C' or inv.[stat] = 'V' THEN trx.[transAmount]
                   WHEN inv.[stat] = 'A' THEN app.[amount]
                   WHEN inv.[stat] = 'O' or inv.[stat] = 'D' THEN inv.[amount]
                  END AS 'amount',
                  CASE WHEN doc.[entityID] IS NOT NULL 
                   THEN 'True'
                   ELSE 'False'
                  END AS 'hasDocument',
                  app.[dateActed] AS 'appDate',
                  trx.[transAmount],
                  trx.[transDate],
                  CONVERT(VARCHAR(1000),ISNULL(app.[uid],'')) AS 'uid',
                  CONVERT(VARCHAR(1000),ISNULL(app.[name],'')) AS 'approval',
                  CASE WHEN inv.[stat] = 'C' OR inv.[stat] = 'V'
                   THEN DATEDIFF(DAY,inv.[invoiceDate],trx.[transDate])
                   ELSE DATEDIFF(DAY,inv.[invoiceDate],GETDATE())
                  END AS 'age'
          FROM    [apinv] inv INNER JOIN
                  (
                  SELECT  DISTINCT
                          app.[apinvID],
                          MIN(app.[amount]) as 'amount',
                          MAX(app.[dateActed]) AS 'dateActed',
                          SUBSTRING(
                          (
                              SELECT  ', ' + usr.[uid] AS [text()]
                              FROM    [sysuser] usr INNER JOIN
                                      [apinva] app2
                              ON      app2.[uid] = usr.[uid]
                              WHERE   app.[apinvID] = app2.[apinvID]
                              AND     app2.[stat] = 'P'
                              ORDER BY app.[apinvID]
                              FOR XML PATH ('')
                          ), 3, 1000) [uid],
                          SUBSTRING(
                          (
                              SELECT  ', ' + usr.[name] AS [text()]
                              FROM    [sysuser] usr INNER JOIN
                                      [apinva] app2
                              ON      app2.[uid] = usr.[uid]
                              WHERE   app.[apinvID] = app2.[apinvID]
                              AND     app2.[stat] = 'P'
                              ORDER BY app.[apinvID]
                              FOR XML PATH ('')
                          ), 3, 1000) [name]
                  FROM    [apinva] app
                  GROUP BY app.[apinvID]
                  ) app
          ON      inv.[apinvID] = app.[apinvID] LEFT OUTER JOIN
                  [aptrx] trx
          ON      inv.[apinvID] = trx.[apinvID]
          AND     trx.[transType] = @aptrxStatus LEFT OUTER JOIN
                  [sysdoc] doc
          ON      inv.[apinvID] = CONVERT(INTEGER,doc.[entityID])
          AND     doc.[entityType] = 'Invoice-AP' INNER JOIN
                  #statusTable statusTable
          ON      inv.[stat] = statusTable.[stat] INNER JOIN
                  #typeTable typeTable
          ON      inv.[refType] = typeTable.[type]
          WHERE   inv.[vendorName] LIKE '%' + @vendorName + '%'
          ) inv
  WHERE   inv.[searchDate] BETWEEN @startDate AND @endDate
  
  -- Get the receivable invoices
  IF (CHARINDEX('O',@status) > 0 OR CHARINDEX('A',@status) > 0 OR CHARINDEX('W',@status) > 0)
  BEGIN
    INSERT INTO #invoiceTable ([apinvID],[aptrxID],[stat],[invoiceType],[refDescription],[refType],[refID],[refCategory],[vendorID],[vendorName],[invoiceNumber],[invoiceDate],[recDate],[amount],[age],[hasDocument],[contention],[appDate],[transAmount],[transDate],[uid],[approval])
    SELECT  inv.[arinvID],
            0,
            inv.[stat],
            inv.[invoiceType],
            inv.[refDescription],
            inv.[refType],
            inv.[refID],
            inv.[refCategory],
            '',
            inv.[name],
            inv.[invoiceNumber],
            inv.[invoiceDate],
            inv.[recDate],
            inv.[amount],
            CASE 
             WHEN inv.[age] < 0 THEN 0
             ELSE inv.[age]
            END AS 'age',
            inv.[hasDocument],
            CONVERT(BIT,0),
            inv.[appDate],
            inv.[transAmount],
            inv.[transDate],
            '',
            ''
    FROM    (
            SELECT  inv.[arinvID],
                    inv.[stat],
                    CASE
                     WHEN inv.[refType] = 'C' THEN 'Claim'
                    END AS 'refDescription',
                    inv.[refType],
                    inv.[refID],
                    'R' AS 'invoiceType',
                    inv.[refCategory],
                    inv.[name],
                    inv.[invoiceNumber],
                    inv.[dateRequested] AS 'invoiceDate',
                    inv.[dateRequested] AS 'recDate',
                    ISNULL(inv.[dateRequested],@startDate) AS 'searchDate',
                    CASE WHEN inv.[stat] = 'W' 
                     THEN inv.[requestedAmount]
                     ELSE inv.[requestedAmount] - ISNULL(trx.[transAmount],0)
                    END AS 'amount',
                    CASE WHEN doc.[entityID] IS NOT NULL 
                     THEN 'True'
                     ELSE 'False'
                    END AS 'hasDocument',
                    inv.[finalizeDate] AS 'appDate',
                    inv.[waivedAmount] AS 'transAmount',
                    inv.[completeDate] AS 'transDate',
                    CASE WHEN inv.[stat] = 'W'
                     THEN DATEDIFF(DAY,inv.[dateRequested],inv.[completeDate])
                     ELSE DATEDIFF(DAY,inv.[dateRequested],GETDATE())
                    END AS 'age'
            FROM    [arinv] inv LEFT OUTER JOIN
                    (
                    SELECT  [arinvID],
                            SUM([transAmount]) AS 'transAmount'
                    FROM    [artrx]
                    GROUP BY [arinvID]
                    ) trx
            ON      inv.[arinvID] = trx.[arinvID] LEFT OUTER JOIN
                    [sysdoc] doc
            ON      inv.[arinvID] = CONVERT(INTEGER,doc.[entityID])
            AND     doc.[entityType] = 'Invoice-AR' INNER JOIN
                    #typeTable typeTable
            ON      inv.[refType] = typeTable.[type] INNER JOIN
                    #statusTable statusTable
            ON      inv.[stat] = statusTable.[stat]
            WHERE   inv.[name] LIKE '%' + @vendorName + '%'
            ) inv
    WHERE   inv.[searchDate] BETWEEN @startDate AND @endDate
  END

  IF (@status = 'C')
  BEGIN
    INSERT INTO #invoiceTable ([apinvID],[aptrxID],[stat],[invoiceType],[refDescription],[refType],[refID],[refCategory],[vendorID],[vendorName],[invoiceNumber],[invoiceDate],[recDate],[amount],[age],[hasDocument],[contention],[appDate],[transAmount],[transDate],[uid],[approval])
    SELECT  inv.[arinvID],
            inv.[artrxID],
            inv.[stat],
            inv.[invoiceType],
            inv.[refDescription],
            inv.[refType],
            inv.[refID],
            inv.[refCategory],
            '',
            inv.[name],
            inv.[invoiceNumber],
            inv.[invoiceDate],
            inv.[recDate],
            inv.[amount],
            CASE 
             WHEN inv.[age] < 0 THEN 0
             ELSE inv.[age]
            END AS 'age',
            inv.[hasDocument],
            CONVERT(BIT,0),
            inv.[appDate],
            inv.[transAmount],
            inv.[transDate],
            '',
            ''
    FROM    (
            SELECT  inv.[arinvID],
                    trx.[artrxID],
                    trx.[transType] AS 'stat',
                    CASE
                     WHEN inv.[refType] = 'C' THEN 'Claim'
                    END AS 'refDescription',
                    inv.[refType],
                    inv.[refID],
                    'R' AS 'invoiceType',
                    inv.[refCategory],
                    inv.[name],
                    inv.[invoiceNumber],
                    inv.[dateRequested] AS 'invoiceDate',
                    inv.[dateRequested] AS 'recDate',
                    ISNULL(inv.[dateRequested],@startDate) AS 'searchDate',
                    trx.[transAmount] AS 'amount',
                    CASE WHEN doc.[entityID] IS NOT NULL 
                     THEN 'True'
                     ELSE 'False'
                    END AS 'hasDocument',
                    inv.[finalizeDate] AS 'appDate',
                    trx.[transAmount],
                    trx.[transDate],
                    DATEDIFF(DAY,inv.[dateRequested],trx.[transDate]) AS 'age'
            FROM    [arinv] inv INNER JOIN
                    [artrx] trx
            ON      inv.[arinvID] = trx.[arinvID]
            AND     trx.[transType] = 'C'
            AND     trx.[artrxID] NOT IN (
                    SELECT  [artrxID] 
                    FROM    [artrx] 
                    WHERE   [transType] = 'V'
                    ) LEFT OUTER JOIN
                    [sysdoc] doc
            ON      inv.[arinvID] = CONVERT(INTEGER,doc.[entityID])
            AND     doc.[entityType] = 'Invoice-AR' INNER JOIN
                    #typeTable typeTable
            ON      inv.[refType] = typeTable.[type]
            WHERE   inv.[name] LIKE '%' + @vendorName + '%'
            ) inv
    WHERE   inv.[searchDate] BETWEEN @startDate AND @endDate
  END

  IF (@status = 'V')
  BEGIN
    INSERT INTO #invoiceTable ([apinvID],[aptrxID],[stat],[invoiceType],[refDescription],[refType],[refID],[refCategory],[vendorID],[vendorName],[invoiceNumber],[invoiceDate],[recDate],[amount],[age],[hasDocument],[contention],[appDate],[transAmount],[transDate],[uid],[approval])
    SELECT  inv.[arinvID],
            inv.[artrxID],
            inv.[stat],
            inv.[invoiceType],
            inv.[refDescription],
            inv.[refType],
            inv.[refID],
            inv.[refCategory],
            '',
            inv.[name],
            inv.[invoiceNumber],
            inv.[invoiceDate],
            inv.[recDate],
            inv.[amount],
            CASE 
             WHEN inv.[age] < 0 THEN 0
             ELSE inv.[age]
            END AS 'age',
            inv.[hasDocument],
            CONVERT(BIT,0),
            inv.[appDate],
            inv.[transAmount],
            inv.[transDate],
            '',
            ''
    FROM    (
            SELECT  inv.[arinvID],
                    trx.[artrxID],
                    trx.[transType] AS 'stat',
                    CASE
                     WHEN inv.[refType] = 'C' THEN 'Claim'
                    END AS 'refDescription',
                    inv.[refType],
                    inv.[refID],
                    'R' AS 'invoiceType',
                    inv.[refCategory],
                    inv.[name],
                    inv.[invoiceNumber],
                    inv.[dateRequested] AS 'invoiceDate',
                    inv.[dateRequested] AS 'recDate',
                    ISNULL(inv.[dateRequested],@startDate) AS 'searchDate',
                    trx.[transAmount] AS 'amount',
                    CASE WHEN doc.[entityID] IS NOT NULL 
                     THEN 'True'
                     ELSE 'False'
                    END AS 'hasDocument',
                    inv.[finalizeDate] AS 'appDate',
                    trx.[transAmount],
                    trx.[transDate],
                    DATEDIFF(DAY,inv.[dateRequested],trx.[transDate]) AS 'age'
            FROM    [arinv] inv INNER JOIN
                    [artrx] trx
            ON      inv.[arinvID] = trx.[arinvID]
            AND     trx.[transType] = @aptrxStatus LEFT OUTER JOIN
                    [sysdoc] doc
            ON      inv.[arinvID] = CONVERT(INTEGER,doc.[entityID])
            AND     doc.[entityType] = 'Invoice-AR' INNER JOIN
                    #typeTable typeTable
            ON      inv.[refType] = typeTable.[type]
            WHERE   inv.[name] LIKE '%' + @vendorName + '%'
            ) inv
    WHERE   inv.[searchDate] BETWEEN @startDate AND @endDate
  END

  SELECT * FROM #invoiceTable
END
