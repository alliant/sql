SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spGetAgentProduct] 
	@agentID   VARCHAR(50) = '',
	@productID INTEGER     = 0
AS
BEGIN
	CREATE TABLE #tempAgentProduct
	(
		[agentID]     VARCHAR(50),
		[productID]   INTEGER,
		[routing]     VARCHAR(10),		
		[uid]         VARCHAR(100),
		[notUid]      VARCHAR(8000),
		[prodQueueID] INTEGER,
		[price]       DECIMAL(18,2),
		[dueDays]     INTEGER
	)


	INSERT INTO #tempAgentProduct([agentID], [productID], [routing], [uid], [notUid], [prodQueueID], [price], [dueDays])
	SELECT [agentID],
		   [productID],		   
		   (CASE 
			WHEN [routing] = 'U' THEN 'User' 
			WHEN [routing] = 'Q' THEN 'Queue' 
			WHEN [routing] = 'B' THEN 'Both' 
			WHEN [routing] = 'N' THEN 'None'
			ELSE [routing] END) AS [routing],
		   [uid],
		   [notUid],
		   [prodQueueID],
		   [price],
		   [dueDays] 
	FROM agentproduct
	WHERE agentproduct.agentid   = CASE WHEN @agentID   != '' THEN @agentID   ELSE agentproduct.agentid   END
	  AND agentproduct.productid = CASE WHEN @productID != 0  THEN @productID ELSE agentproduct.productid END

	SELECT * FROM #tempAgentProduct

	DROP TABLE #tempAgentProduct	
END

