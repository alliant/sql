USE [COMPASS_ALFA]
GO
/****** Object:  StoredProcedure [dbo].[spGetActiveUsers]    Script Date: 4/5/2023 2:28:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetActiveUsers]
	-- Add the parameters for the stored procedure here
	@ipAction    VARCHAR(1) = 'A'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    IF (@ipAction = 'A')
    BEGIN
	  SELECT sysuser.uid,
	         sysuser.name,
		     sysuser.email
		     FROM sysuser WHERE sysuser.isActive = 1 
    END
END