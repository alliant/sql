USE [COMPASS]
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE spInsertAlertNote
	@alertID INTEGER = 0,
  @user VARCHAR(100) = '',
  @description VARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  DECLARE @validAlert BIT = 0,
          @validUser BIT = 0,
          @seq INTEGER = 1
  
  IF (@alertID = 0)
  BEGIN
    RAISERROR(N'The alertID is missing',10,1)
    RETURN 0
  END

  SELECT @validAlert = COUNT(*) FROM [dbo].[alert] WHERE [alertID] = @alertID
  IF (@validAlert = 0)
  BEGIN
    RAISERROR(N'The alertID is not valid',10,1)
    RETURN 0
  END
    
  SELECT @validUser = COUNT(*) FROM [dbo].[sysuser] WHERE [uid] = @user
  IF (@validUser = 0)
  BEGIN
    RAISERROR(N'The user is not valid',10,1)
    RETURN 0
  END
  
  SELECT @seq = MAX([seq]) + 1 FROM [dbo].[alertnote] WHERE [alertID] = @alertID
  IF (@seq IS NULL)
    SET @seq = 1

  INSERT INTO [dbo].[alertnote] ([alertID],[seq],[dateCreated],[createdBy],[category],[description])
  SELECT  @alertID,
          @seq,
          GETDATE(),
          @user,
          '0',
          @description

  RETURN 1
END
GO
