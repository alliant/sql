USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetBatchCounts]    Script Date: 8/9/2016 2:38:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		  John Oliver
-- Create date: 5/16/2016
-- Description:	Gets the vendors from GP
-- =============================================
ALTER PROCEDURE [dbo].[spGetGPVendor]
	-- Add the parameters for the stored procedure here
	@vendorNbr VARCHAR(30) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  DECLARE @space VARCHAR(1) = ' '

  -- Build the report
  IF (@vendorNbr IS NOT NULL)
  BEGIN
    SELECT  rtrim(vm.VENDORID) as 'vendorID',
            rtrim(vm.VENDNAME) as 'name',
            case when vm.VENDSTTS = 1 then 'yes' else 'no' end as 'active',
            rtrim(vm.ADDRESS1) + rtrim(vm.ADDRESS2) as 'address',
            rtrim(vm.CITY) as 'city',
            rtrim(vm.STATE) as 'state',
            rtrim(vm.ZIPCODE) as 'zipcode',
            rtrim(a.ACTNUMST) as 'acct',
            rtrim(am.ACTDESCR) as 'acctname',
            rtrim(d.SGMNTID) as 'dept',
            rtrim(d.DSCRIPTN) as 'deptname'
    FROM    ANTIC.dbo.PM00200 vm INNER JOIN
            ANTIC.dbo.GL00100 am INNER JOIN
            ANTIC.dbo.GL00105 a INNER JOIN
            ANTIC.dbo.GL40200 d
         ON d.SGMNTID = a.ACTNUMBR_3
         ON a.ACTINDX = am.ACTINDX
         ON am.ACTINDX = vm.PMPRCHIX
    WHERE   vM.VENDORID = @vendorNbr
  END
  ELSE
  BEGIN
    SELECT  rtrim(vm.VENDORID) as 'vendorID',
            rtrim(vm.VENDNAME) as 'name',
            case when vm.VENDSTTS = 1 then 'yes' else 'no' end as 'active',
            rtrim(vm.ADDRESS1) + rtrim(vm.ADDRESS2) as 'address',
            rtrim(vm.CITY) as 'city',
            rtrim(vm.STATE) as 'state',
            rtrim(vm.ZIPCODE) as 'zipcode',
            rtrim(a.ACTNUMST) as 'acct',
            rtrim(am.ACTDESCR) as 'acctname',
            rtrim(d.SGMNTID) as 'dept',
            rtrim(d.DSCRIPTN) as 'deptname'
    FROM    ANTIC.dbo.PM00200 vm INNER JOIN
            ANTIC.dbo.GL00100 am INNER JOIN
            ANTIC.dbo.GL00105 a INNER JOIN
            ANTIC.dbo.GL40200 d
         ON d.SGMNTID = a.ACTNUMBR_3
         ON a.ACTINDX = am.ACTINDX
         ON am.ACTINDX = vm.PMPRCHIX
  END
END