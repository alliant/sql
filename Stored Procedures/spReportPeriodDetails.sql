GO
/****** Object:  StoredProcedure [dbo].[spReportPeriodDetails]    Script Date: 6/26/2023 1:25:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportPeriodDetails] 
	-- Add the parameters for the stored procedure here
	@periodID INTEGER = 0,
  @stateID VARCHAR(20) = 'ALL'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  IF (@periodID = 0)
    SELECT @periodID = MAX([periodID]) FROM [dbo].[period] WHERE [active] = 0

  -- The query
  SELECT  DISTINCT
          a.[agentID],
          bf.[fileID],
          a.[name],
          bf.[formID],
          ISNULL(sf.[formCode],'') AS [formCode],
          bf.[policyID],
          bf.[fileNumber],
          b.[periodYear] AS [postYear],
          b.[periodMonth] AS [postMonth],
          b.[batchID],
          bf.[formType],
          ISNULL(sf.[insuredType],'') AS [insuredType],
          bf.[statCode],
          bf.[rateCode],
          bf.[liabilityDelta] AS [liabilityAmount],
          bf.[reprocess],
          bf.[grossDelta] AS [grossPremium],
          bf.[retentionDelta] AS [retentionPremium],
          bf.[netDelta] AS [netPremium],
          b.[stateID] AS [stateID],
          bf.[countyID] AS [countyID],
          s.[description] AS [stateDesc],
          CASE WHEN bf.[residential] = 1 THEN 'RS' ELSE 'NR' END AS [propType],
          bf.[effDate] AS [effDate],
          p.[issueDate] AS [issueDate],
          ar2.[tranDate] AS [paidDate]
  FROM    [dbo].[agent] a INNER JOIN
          [dbo].[batch] b
  ON      a.[agentID] = b.[agentID] INNER JOIN
          [dbo].[batchform] bf
  ON      b.[batchID] = bf.[batchID] INNER JOIN
          [dbo].[state] s
  ON      s.[stateID] = b.[stateID] LEFT OUTER JOIN
          [dbo].[stateform] sf
  ON      sf.[stateID] = b.[stateID]
  AND     sf.[formID] = bf.[formID] LEFT OUTER JOIN
          [dbo].[policy] p
  ON      bf.[policyID] = p.[policyID] LEFT OUTER JOIN
          [dbo].[artran] ar1
  ON      ar1.[entity] = 'A'  
  AND     ar1.[entityid] = b.[agentid]
  AND     ar1.[fileid] = bf.[fileid]
  AND     ar1.[seq] = 0
  AND     ar1.[transtype] = 'F' LEFT OUTER JOIN
          [dbo].[artran] ar2
  ON      ar2.[arTranID] = (SELECT TOP 1 [arTranID]
                            FROM   arTran
                            WHERE  [entity] = ar1.[entity] 
                            AND    [entityID] = ar1.[entityID]
                            AND    [fileID] = ar1.[fileID]
                            AND    [type] = 'A'
                            AND    [transType] = 'F')
  WHERE   b.[periodID] = @periodID
  ORDER BY a.[agentID], b.[batchID], bf.[policyID]
END
