GO
/****** Object:  StoredProcedure [dbo].[spGetOrderCode]    Script Date: 8/21/2019 10:38:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetOrderCode]
	-- Add the parameters for the stored procedure here
	@orderID VARCHAR(30) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  SELECT  [orderID],
          [codeType],
          (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'TPS' AND [objAction] = 'ProductCode' AND [objProperty] = 'Type' AND [objID] = o.[codeType]) AS [typeDesc],
          [seq],
          [orderSeq],
          [productCodeID],
          [indent],
          [description],
          [placeholders],
          [placevalues],
          'false' AS [isNew],
          'false' AS [isModify],
          'false' AS [isDelete]
  FROM    [dbo].[ordercode] o
  WHERE   [orderID] = CASE WHEN @orderID = '' THEN [orderID] ELSE @orderID END
END
