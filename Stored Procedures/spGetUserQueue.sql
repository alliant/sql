USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetUserQueue]    Script Date: 6/17/2021 2:26:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Shubham>
-- Create date: <05/05/2021>
-- Description:	<return the queue list of user>
-- =============================================
ALTER PROCEDURE [dbo].[spGetUserQueue] 
	-- Add the parameters for the stored procedure here
	@UID  varchar(100) = NULL,
	@stat varchar(1)  = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT  q.[queueID],
          qi.[seq],
          q.[actionID],
          (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'SYS' AND [objAction] = 'Destination' AND [objProperty] = 'Action' AND [objID] = q.[actionID]) AS [actionDesc],
          q.[appCode],
		  q.[queueDate],
		  q.[startDate],
		  q.[endDate],
          q.[notifyRequestor],
		  q.[uid],
          q.[stat] AS [queueStat],
          (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'SYS' AND [objAction] = 'Queue' AND [objProperty] = 'Status' AND [objID] = q.[stat]) AS [queueStatDesc],
          qi.[parameters],
          qi.[destinations],
          qi.[stat] AS [itemStat],
          (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'SYS' AND [objAction] = 'Queue' AND [objProperty] = 'Status' AND [objID] = qi.[stat]) AS [itemStatDesc],
          qi.[faultMsg] AS [itemFault]
  FROM    [dbo].[sysqueue] q INNER JOIN
          [dbo].[sysqueueitem] qi
  ON      q.[queueID] = qi.[queueID]
  WHERE   q.[uid]  = case when @UID  = '' then q.[uid]  else @UID end
    and   q.[stat] = case when @stat = '' then q.[stat] else @stat end
  ORDER BY q.[startDate]

END
