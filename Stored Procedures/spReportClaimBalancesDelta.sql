USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spReportClaimBalanceDelta]    Script Date: 9/19/2018 6:31:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportClaimBalancesDelta]
	-- Add the parameters for the stored procedure here
  @claimID INTEGER = 0,
	@startDate DATETIME = NULL,
  @endDate DATETIME = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  DECLARE @insertClaimBalance [dbo].[ClaimBalance]
  SELECT * INTO #insertClaimBalance FROM @insertClaimBalance

  DECLARE @insertClaimBalanceDelta [dbo].[ClaimBalanceDelta]
  SELECT * INTO #deltaTable FROM @insertClaimBalanceDelta

  -- Insert statements for procedure here

  -- Decide the dates
  IF (@startDate IS NULL)
    SET @startDate = '2005-01-01'

  IF (@endDate IS NULL)
    SET @endDate = GETDATE()

  -- Get the claim balances
  EXEC [dbo].[spReportClaimBalances] @claimID = @claimID, @asOfDate = @startDate

  EXEC [dbo].[spReportClaimBalances] @claimID = @claimID, @asOfDate = @endDate

  INSERT INTO #deltaTable ([claimID],[laePendingInvoiceAmount],[laeApprovedInvoiceAmount],[laeCompletedInvoiceAmount],[laePendingReserveAmount],[laeApprovedReserveAmount],[laeReserveBalance],[lossPendingInvoiceAmount],[lossApprovedInvoiceAmount],[lossCompletedInvoiceAmount],[lossPendingReserveAmount],[lossApprovedReserveAmount],[lossReserveBalance],[pendingRecoveries],[paidRecoveries])
  SELECT  b.[claimID],
          b.[laePendingInvoiceAmount],
          b.[laeApprovedInvoiceAmount],
          b.[laeCompletedInvoiceAmount],
          b.[laePendingReserveAmount],
          b.[laeApprovedReserveAmount],
          b.[laeReserveBalance],
          b.[lossPendingInvoiceAmount],
          b.[lossApprovedInvoiceAmount],
          b.[lossCompletedInvoiceAmount],
          b.[lossPendingReserveAmount],
          b.[lossApprovedReserveAmount],
          b.[lossReserveBalance],
          ISNULL(r.[pendingAmount],0) AS [pendingRecoveries],
          ISNULL(r.[transAmount],0) AS [paidRecoveries]
  FROM    (
          SELECT  [claimID],
                  SUM([EPI]) AS [laePendingInvoiceAmount],
                  SUM([EAI]) AS [laeApprovedInvoiceAmount],
                  SUM([ECI]) AS [laeCompletedInvoiceAmount],
                  SUM([EPR]) AS [laePendingReserveAmount],
                  SUM([EAR]) AS [laeApprovedReserveAmount],
                  SUM([ER])  AS [laeReserveBalance],
                  SUM([LPI]) AS [lossPendingInvoiceAmount],
                  SUM([LAI]) AS [lossApprovedInvoiceAmount],
                  SUM([LCI]) AS [lossCompletedInvoiceAmount],
                  SUM([LPR]) AS [lossPendingReserveAmount],
                  SUM([LAR]) AS [lossApprovedReserveAmount],
                  SUM([LR])  AS [lossReserveBalance]
          FROM    (
                  SELECT  curr.[claimID],
                          curr.[refCategory] + 'PI' AS [pendingInvoice],
                          curr.[refCategory] + 'AI' AS [approvedInvoice],
                          curr.[refCategory] + 'CI' AS [completedInvoice],
                          curr.[refCategory] + 'PR' AS [pendingReserve],
                          curr.[refCategory] + 'AR' AS [approveReserve],
                          curr.[refCategory] + 'R'  AS [reserve],
                          curr.[pendingInvoiceAmount] - prev.[pendingInvoiceAmount] AS [pendingInvoiceAmount],
                          curr.[approvedInvoiceAmount] - prev.[approvedInvoiceAmount] AS [approvedInvoiceAmount],
                          curr.[completedInvoiceAmount] - prev.[completedInvoiceAmount] AS [completedInvoiceAmount],
                          curr.[pendingReserveAmount] - prev.[pendingReserveAmount] AS [pendingReserveAmount],
                          curr.[approvedReserveAmount] - prev.[approvedReserveAmount] AS [approvedReserveAmount],
                          curr.[reserveBalance] - prev.[reserveBalance] AS [reserveBalance]
                  FROM    #insertClaimBalance curr INNER JOIN
                          #insertClaimBalance prev
                  ON      prev.[refCategory] = curr.[refCategory]
                  AND     prev.[claimID] = curr.[claimID]
                  WHERE   prev.[asOfDate] = @startDate
                  AND     curr.[asOfDate] = @endDate
                  ) AS a
                  PIVOT
                  (
                  SUM([pendingInvoiceAmount])
                  FOR [pendingInvoice] IN ([EPI],[LPI])
                  ) AS p
                  PIVOT
                  (
                  SUM([approvedInvoiceAmount])
                  FOR [approvedInvoice] IN ([EAI],[LAI])
                  ) AS p
                  PIVOT
                  (
                  SUM([completedInvoiceAmount])
                  FOR [completedInvoice] IN ([ECI],[LCI])
                  ) AS p
                  PIVOT
                  (
                  SUM([pendingReserveAmount])
                  FOR [pendingReserve] IN ([EPR],[LPR])
                  ) AS p
                  PIVOT
                  (
                  SUM([approvedReserveAmount])
                  FOR [approveReserve] IN ([EAR],[LAR])
                  ) AS p
                  PIVOT
                  (
                  SUM([reserveBalance])
                  FOR [reserve] IN (ER,LR)
                  ) AS p
          GROUP BY [claimID]
          ) b LEFT OUTER JOIN
          (
          SELECT  CONVERT(INTEGER,inv.[refID]) AS [claimID],
                  SUM(CASE WHEN inv.[stat] = 'O' OR inv.[stat] = 'A' THEN [requestedAmount] ELSE 0 END) - SUM(CASE WHEN inv.[stat] = 'O' OR inv.[stat] = 'A' THEN [transAmount] ELSE 0 END) AS [pendingAmount],
                  SUM([transAmount]) AS [transAmount]
          FROM    [arinv] inv LEFT OUTER JOIN
                  [artrx] trx
          ON      inv.[arinvID] = trx.[arinvID]
          WHERE   trx.[transDate] BETWEEN @startDate AND @endDate
          AND     inv.[refID] = CASE WHEN @claimID = 0 THEN inv.[refID] ELSE CONVERT(VARCHAR,inv.[refID]) END
          GROUP BY inv.[refID]
          ) r
  ON      b.[claimID] = r.[claimID]

  IF object_id('tempdb..#insertClaimBalanceDelta') IS NULL
    SELECT * FROM #deltaTable
  ELSE
    INSERT INTO #insertClaimBalanceDelta ([claimID],[laePendingInvoiceAmount],[laeApprovedInvoiceAmount],[laeCompletedInvoiceAmount],[laePendingReserveAmount],[laeApprovedReserveAmount],[laeReserveBalance],[lossPendingInvoiceAmount],[lossApprovedInvoiceAmount],[lossCompletedInvoiceAmount],[lossPendingReserveAmount],[lossApprovedReserveAmount],[lossReserveBalance],[pendingRecoveries],[paidRecoveries])
    SELECT  [claimID],
            [laePendingInvoiceAmount],
            [laeApprovedInvoiceAmount],
            [laeCompletedInvoiceAmount],
            [laePendingReserveAmount],
            [laeApprovedReserveAmount],
            [laeReserveBalance],
            [lossPendingInvoiceAmount],
            [lossApprovedInvoiceAmount],
            [lossCompletedInvoiceAmount],
            [lossPendingReserveAmount],
            [lossApprovedReserveAmount],
            [lossReserveBalance],
            [pendingRecoveries],
            [paidRecoveries]
    FROM    #deltaTable

  DROP TABLE #insertClaimBalance
  DROP TABLE #deltaTable
END
