USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spReportAgency]    Script Date: 2/25/2019 12:49:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportAgency]
	-- Add the parameters for the stored procedure here
	@date DATETIME = NULL
AS
BEGIN
  DECLARE @col VARCHAR(MAX),
          @buf VARCHAR(MAX),
          @add VARCHAR(MAX),
          @sql NVARCHAR(MAX)

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  IF (@date IS NULL) 
    SELECT @date = DATEADD(DAY, -1, GETDATE())

  CREATE TABLE #agencyTotal
  (
    [agentID] VARCHAR(30),
    [category] VARCHAR(30),
    [yearCount] DECIMAL(18,2),
    [lastDay] DECIMAL(18,2),
    [month1Curr] DECIMAL(18,2),
    [month2Curr] DECIMAL(18,2),
    [month3Curr] DECIMAL(18,2),
    [month4Curr] DECIMAL(18,2),
    [month5Curr] DECIMAL(18,2),
    [month6Curr] DECIMAL(18,2),
    [month7Curr] DECIMAL(18,2),
    [month8Curr] DECIMAL(18,2),
    [month9Curr] DECIMAL(18,2),
    [month10Curr] DECIMAL(18,2),
    [month11Curr] DECIMAL(18,2),
    [month12Curr] DECIMAL(18,2),
    [month1Prev] DECIMAL(18,2),
    [month2Prev] DECIMAL(18,2),
    [month3Prev] DECIMAL(18,2),
    [month4Prev] DECIMAL(18,2),
    [month5Prev] DECIMAL(18,2),
    [month6Prev] DECIMAL(18,2),
    [month7Prev] DECIMAL(18,2),
    [month8Prev] DECIMAL(18,2),
    [month9Prev] DECIMAL(18,2),
    [month10Prev] DECIMAL(18,2),
    [month11Prev] DECIMAL(18,2),
    [month12Prev] DECIMAL(18,2)
  )

  -- Insert the CPL counts
	INSERT INTO #agencyTotal ([agentID],[category],[yearCount],[lastDay],[month1Curr],[month2Curr],[month3Curr],[month4Curr],[month5Curr],[month6Curr],[month7Curr],[month8Curr],[month9Curr],[month10Curr],[month11Curr],[month12Curr],[month1Prev],[month2Prev],[month3Prev],[month4Prev],[month5Prev],[month6Prev],[month7Prev],[month8Prev],[month9Prev],[month10Prev],[month11Prev],[month12Prev])
  SELECT  a.[agentID],
          'C' AS [category],
          ISNULL([yearCount],0) AS [yearCount],
          0 AS [lastDay],
          ISNULL([month1Curr],0) AS [month1Curr],
          ISNULL([month2Curr],0) AS [month2Curr],
          ISNULL([month3Curr],0) AS [month3Curr],
          ISNULL([month4Curr],0) AS [month4Curr],
          ISNULL([month5Curr],0) AS [month5Curr],
          ISNULL([month6Curr],0) AS [month6Curr],
          ISNULL([month7Curr],0) AS [month7Curr],
          ISNULL([month8Curr],0) AS [month8Curr],
          ISNULL([month9Curr],0) AS [month9Curr],
          ISNULL([month10Curr],0) AS [month10Curr],
          ISNULL([month11Curr],0) AS [month11Curr],
          ISNULL([month12Curr],0) AS [month12Curr],
          ISNULL([month1Prev],0) AS [month1Prev],
          ISNULL([month2Prev],0) AS [month2Prev],
          ISNULL([month3Prev],0) AS [month3Prev],
          ISNULL([month4Prev],0) AS [month4Prev],
          ISNULL([month5Prev],0) AS [month5Prev],
          ISNULL([month6Prev],0) AS [month6Prev],
          ISNULL([month7Prev],0) AS [month7Prev],
          ISNULL([month8Prev],0) AS [month8Prev],
          ISNULL([month9Prev],0) AS [month9Prev],
          ISNULL([month10Prev],0) AS [month10Prev],
          ISNULL([month11Prev],0) AS [month11Prev],
          ISNULL([month12Prev],0) AS [month12Prev]
  FROM    [dbo].[agent] a LEFT OUTER JOIN
          (
          SELECT  curr.[agentID],
                  curr.[month1] + curr.[month2] + curr.[month3] + curr.[month4] + curr.[month5] + curr.[month6] + curr.[month7] + curr.[month8] + curr.[month9] + curr.[month10] + curr.[month11] + curr.[month12] AS [yearCount],
                  ISNULL(curr.[month1],0)  AS [month1Curr],
                  ISNULL(curr.[month2],0)  AS [month2Curr],
                  ISNULL(curr.[month3],0)  AS [month3Curr],
                  ISNULL(curr.[month4],0)  AS [month4Curr],
                  ISNULL(curr.[month5],0)  AS [month5Curr],
                  ISNULL(curr.[month6],0)  AS [month6Curr],
                  ISNULL(curr.[month7],0)  AS [month7Curr],
                  ISNULL(curr.[month8],0)  AS [month8Curr],
                  ISNULL(curr.[month9],0)  AS [month9Curr],
                  ISNULL(curr.[month10],0) AS [month10Curr],
                  ISNULL(curr.[month11],0) AS [month11Curr],
                  ISNULL(curr.[month12],0) AS [month12Curr],
                  ISNULL(prev.[month1],0)  AS [month1Prev],
                  ISNULL(prev.[month2],0)  AS [month2Prev],
                  ISNULL(prev.[month3],0)  AS [month3Prev],
                  ISNULL(prev.[month4],0)  AS [month4Prev],
                  ISNULL(prev.[month5],0)  AS [month5Prev],
                  ISNULL(prev.[month6],0)  AS [month6Prev],
                  ISNULL(prev.[month7],0)  AS [month7Prev],
                  ISNULL(prev.[month8],0)  AS [month8Prev],
                  ISNULL(prev.[month9],0)  AS [month9Prev],
                  ISNULL(prev.[month10],0) AS [month10Prev],
                  ISNULL(prev.[month11],0) AS [month11Prev],
                  ISNULL(prev.[month12],0) AS [month12Prev]
          FROM    [dbo].[agentactivity] curr FULL OUTER JOIN
                  [dbo].[agentactivity] prev
          ON      curr.[agentID] = prev.[agentID]
          AND     curr.[category] = prev.[category]
          AND     curr.[type] = prev.[type]
          AND     curr.[year] = prev.[year] + 1
          WHERE  (curr.[year] = YEAR(@date) OR prev.[year] = YEAR(DATEADD(YEAR, -1, @date)))
          AND     curr.[category] = 'I'
          AND     curr.[type] = 'A'
          ) aa
  ON      a.[agentID] = aa.[agentID]

  -- Insert the batch count
  INSERT INTO #agencyTotal ([agentID],[category],[yearCount],[lastDay],[month1Curr],[month2Curr],[month3Curr],[month4Curr],[month5Curr],[month6Curr],[month7Curr],[month8Curr],[month9Curr],[month10Curr],[month11Curr],[month12Curr],[month1Prev],[month2Prev],[month3Prev],[month4Prev],[month5Prev],[month6Prev],[month7Prev],[month8Prev],[month9Prev],[month10Prev],[month11Prev],[month12Prev])
  SELECT  [agentID],
          'B' AS [category],
          0 AS [yearCount],
          0 AS [lastDay],
          0 AS [month1Curr],
          0 AS [month2Curr],
          0 AS [month3Curr],
          0 AS [month4Curr],
          0 AS [month5Curr],
          0 AS [month6Curr],
          0 AS [month7Curr],
          0 AS [month8Curr],
          0 AS [month9Curr],
          0 AS [month10Curr],
          0 AS [month11Curr],
          0 AS [month12Curr],
          0 AS [month1Prev],
          0 AS [month2Prev],
          0 AS [month3Prev],
          0 AS [month4Prev],
          0 AS [month5Prev],
          0 AS [month6Prev],
          0 AS [month7Prev],
          0 AS [month8Prev],
          0 AS [month9Prev],
          0 AS [month10Prev],
          0 AS [month11Prev],
          0 AS [month12Prev]
  FROM    [dbo].[agent]

  SELECT @col = 
      STUFF((SELECT DISTINCT
                    ',' + QUOTENAME(p.[periodID])
             FROM   [dbo].[batch] b INNER JOIN
                    [dbo].[period] p
             ON     b.[periodID] = p.[periodID]
             WHERE  p.[periodYear] >= YEAR(@date) - 1
             FOR XML PATH(''), TYPE
             ).value('.', 'NVARCHAR(MAX)'),1,1,'')

  SELECT @buf = 
      STUFF((SELECT DISTINCT
                    ',' + '[month' + CONVERT(VARCHAR,p.[periodMonth]) + CASE WHEN p.[periodYear] = YEAR(GETDATE()) THEN 'Curr' ELSE 'Prev' END + '] = ' + QUOTENAME(p.[periodID])
             FROM   [dbo].[batch] b INNER JOIN
                    [dbo].[period] p
             ON     b.[periodID] = p.[periodID]
             WHERE  p.[periodYear] >= YEAR(@date) - 1
             FOR XML PATH(''), TYPE
             ).value('.', 'NVARCHAR(MAX)'),1,1,'')

  SELECT @add = 
      STUFF((SELECT DISTINCT
                    ' + ' + QUOTENAME(p.[periodID])
             FROM   [dbo].[batch] b INNER JOIN
                    [dbo].[period] p
             ON     b.[periodID] = p.[periodID]
             WHERE  p.[periodYear] = YEAR(@date)
             FOR XML PATH(''), TYPE
             ).value('.', 'NVARCHAR(MAX)'),1,3,'')

  SET @sql = N'
      UPDATE  #agencyTotal
      SET     [yearCount] = ' + @add + ',
              ' + @buf + '
      FROM    #agencyTotal a INNER JOIN
              (
              SELECT  b.[agentID],
                      p.[periodID],
                      b.[batchID]
              FROM    [dbo].[batch] b INNER JOIN
                      [dbo].[period] p
              ON      b.[periodID] = p.[periodID]
              WHERE   p.[periodYear] >= YEAR(' + CONVERT(VARCHAR,@date,101) + ') - 1
              ) s
              PIVOT
              (
              COUNT([batchID])
              FOR [periodID] IN (' + @col + ')
              ) p
      ON     a.[agentID] = p.[agentID]
      AND    a.[category] = ''B'''

  PRINT @sql
  EXEC sp_executesql @sql
  
  -- Update the batch last 24 hour count
  UPDATE  #agencyTotal
  SET     [lastDay] = ISNULL(b.[lastDay],0)
  FROM    #agencyTotal a LEFT OUTER JOIN
          (
          SELECT  b.[agentID],
                  COUNT(b.[batchID]) AS [lastDay]
          FROM    [dbo].[batch] b
          WHERE   CONVERT(DATE,b.[receivedDate],101) = CONVERT(DATE,@date,101)
          GROUP BY b.[agentID]
          ) b
  ON      a.[agentID] = b.[agentID]
  WHERE   a.[category] = 'B'

  -- Insert the net premium actual
	INSERT INTO #agencyTotal ([agentID],[category],[yearCount],[month1Curr],[month2Curr],[month3Curr],[month4Curr],[month5Curr],[month6Curr],[month7Curr],[month8Curr],[month9Curr],[month10Curr],[month11Curr],[month12Curr],[month1Prev],[month2Prev],[month3Prev],[month4Prev],[month5Prev],[month6Prev],[month7Prev],[month8Prev],[month9Prev],[month10Prev],[month11Prev],[month12Prev])
  SELECT  a.[agentID],
          'NPR' AS [category],
          ISNULL([yearCount],0) AS [yearCount],
          ISNULL([month1Curr],0) AS [month1Curr],
          ISNULL([month2Curr],0) AS [month2Curr],
          ISNULL([month3Curr],0) AS [month3Curr],
          ISNULL([month4Curr],0) AS [month4Curr],
          ISNULL([month5Curr],0) AS [month5Curr],
          ISNULL([month6Curr],0) AS [month6Curr],
          ISNULL([month7Curr],0) AS [month7Curr],
          ISNULL([month8Curr],0) AS [month8Curr],
          ISNULL([month9Curr],0) AS [month9Curr],
          ISNULL([month10Curr],0) AS [month10Curr],
          ISNULL([month11Curr],0) AS [month11Curr],
          ISNULL([month12Curr],0) AS [month12Curr],
          ISNULL([month1Prev],0) AS [month1Prev],
          ISNULL([month2Prev],0) AS [month2Prev],
          ISNULL([month3Prev],0) AS [month3Prev],
          ISNULL([month4Prev],0) AS [month4Prev],
          ISNULL([month5Prev],0) AS [month5Prev],
          ISNULL([month6Prev],0) AS [month6Prev],
          ISNULL([month7Prev],0) AS [month7Prev],
          ISNULL([month8Prev],0) AS [month8Prev],
          ISNULL([month9Prev],0) AS [month9Prev],
          ISNULL([month10Prev],0) AS [month10Prev],
          ISNULL([month11Prev],0) AS [month11Prev],
          ISNULL([month12Prev],0) AS [month12Prev]
  FROM    [dbo].[agent] a LEFT OUTER JOIN
          (
          SELECT  curr.[agentID],
                  curr.[month1] + curr.[month2] + curr.[month3] + curr.[month4] + curr.[month5] + curr.[month6] + curr.[month7] + curr.[month8] + curr.[month9] + curr.[month10] + curr.[month11] + curr.[month12] AS [yearCount],
                  ISNULL(curr.[month1],0)  AS [month1Curr],
                  ISNULL(curr.[month2],0)  AS [month2Curr],
                  ISNULL(curr.[month3],0)  AS [month3Curr],
                  ISNULL(curr.[month4],0)  AS [month4Curr],
                  ISNULL(curr.[month5],0)  AS [month5Curr],
                  ISNULL(curr.[month6],0)  AS [month6Curr],
                  ISNULL(curr.[month7],0)  AS [month7Curr],
                  ISNULL(curr.[month8],0)  AS [month8Curr],
                  ISNULL(curr.[month9],0)  AS [month9Curr],
                  ISNULL(curr.[month10],0) AS [month10Curr],
                  ISNULL(curr.[month11],0) AS [month11Curr],
                  ISNULL(curr.[month12],0) AS [month12Curr],
                  ISNULL(prev.[month1],0)  AS [month1Prev],
                  ISNULL(prev.[month2],0)  AS [month2Prev],
                  ISNULL(prev.[month3],0)  AS [month3Prev],
                  ISNULL(prev.[month4],0)  AS [month4Prev],
                  ISNULL(prev.[month5],0)  AS [month5Prev],
                  ISNULL(prev.[month6],0)  AS [month6Prev],
                  ISNULL(prev.[month7],0)  AS [month7Prev],
                  ISNULL(prev.[month8],0)  AS [month8Prev],
                  ISNULL(prev.[month9],0)  AS [month9Prev],
                  ISNULL(prev.[month10],0) AS [month10Prev],
                  ISNULL(prev.[month11],0) AS [month11Prev],
                  ISNULL(prev.[month12],0) AS [month12Prev]
          FROM    [dbo].[agentactivity] curr FULL OUTER JOIN
                  [dbo].[agentactivity] prev
          ON      curr.[agentID] = prev.[agentID]
          AND     curr.[category] = prev.[category]
          AND     curr.[type] = prev.[type]
          AND     curr.[year] = prev.[year] + 1
          WHERE  (curr.[year] = YEAR(@date) OR prev.[year] = YEAR(DATEADD(YEAR, -1, @date)))
          AND     curr.[category] = 'N'
          AND     curr.[type] = 'A'
          ) aa
  ON      a.[agentID] = aa.[agentID]

  -- Update the net premium last 24 hour count
  UPDATE  #agencyTotal
  SET     [lastDay] = ISNULL(b.[lastDay],0)
  FROM    #agencyTotal a LEFT OUTER JOIN
          (
          SELECT  b.[agentID],
                  SUM(bf.[netDelta]) AS [lastDay]
          FROM    [dbo].[batch] b INNER JOIN
                  [dbo].[batchform] bf
          ON      b.[batchID] = bf.[batchID]
          WHERE   CONVERT(DATE,b.[receivedDate],101) = CONVERT(DATE,@date,101)
          GROUP BY b.[agentID]
          ) b
  ON      a.[agentID] = b.[agentID]
  WHERE   a.[category] = 'NPR'

  -- Insert the net premium plan
	INSERT INTO #agencyTotal ([agentID],[category],[yearCount],[lastDay],[month1Curr],[month2Curr],[month3Curr],[month4Curr],[month5Curr],[month6Curr],[month7Curr],[month8Curr],[month9Curr],[month10Curr],[month11Curr],[month12Curr],[month1Prev],[month2Prev],[month3Prev],[month4Prev],[month5Prev],[month6Prev],[month7Prev],[month8Prev],[month9Prev],[month10Prev],[month11Prev],[month12Prev])
    SELECT  a.[agentID],
          'NPP' AS [category],
          ISNULL([yearCount],0) AS [yearCount],
          -9999999 AS [lastDay],
          ISNULL([month1Curr],0) AS [month1Curr],
          ISNULL([month2Curr],0) AS [month2Curr],
          ISNULL([month3Curr],0) AS [month3Curr],
          ISNULL([month4Curr],0) AS [month4Curr],
          ISNULL([month5Curr],0) AS [month5Curr],
          ISNULL([month6Curr],0) AS [month6Curr],
          ISNULL([month7Curr],0) AS [month7Curr],
          ISNULL([month8Curr],0) AS [month8Curr],
          ISNULL([month9Curr],0) AS [month9Curr],
          ISNULL([month10Curr],0) AS [month10Curr],
          ISNULL([month11Curr],0) AS [month11Curr],
          ISNULL([month12Curr],0) AS [month12Curr],
          ISNULL([month1Prev],0) AS [month1Prev],
          ISNULL([month2Prev],0) AS [month2Prev],
          ISNULL([month3Prev],0) AS [month3Prev],
          ISNULL([month4Prev],0) AS [month4Prev],
          ISNULL([month5Prev],0) AS [month5Prev],
          ISNULL([month6Prev],0) AS [month6Prev],
          ISNULL([month7Prev],0) AS [month7Prev],
          ISNULL([month8Prev],0) AS [month8Prev],
          ISNULL([month9Prev],0) AS [month9Prev],
          ISNULL([month10Prev],0) AS [month10Prev],
          ISNULL([month11Prev],0) AS [month11Prev],
          ISNULL([month12Prev],0) AS [month12Prev]
  FROM    [dbo].[agent] a LEFT OUTER JOIN
          (
          SELECT  curr.[agentID],
                  'NPP' AS [category],
                  curr.[month1] + curr.[month2] + curr.[month3] + curr.[month4] + curr.[month5] + curr.[month6] + curr.[month7] + curr.[month8] + curr.[month9] + curr.[month10] + curr.[month11] + curr.[month12] AS [yearCount],
                  ISNULL(curr.[month1],0)  AS [month1Curr],
                  ISNULL(curr.[month2],0)  AS [month2Curr],
                  ISNULL(curr.[month3],0)  AS [month3Curr],
                  ISNULL(curr.[month4],0)  AS [month4Curr],
                  ISNULL(curr.[month5],0)  AS [month5Curr],
                  ISNULL(curr.[month6],0)  AS [month6Curr],
                  ISNULL(curr.[month7],0)  AS [month7Curr],
                  ISNULL(curr.[month8],0)  AS [month8Curr],
                  ISNULL(curr.[month9],0)  AS [month9Curr],
                  ISNULL(curr.[month10],0) AS [month10Curr],
                  ISNULL(curr.[month11],0) AS [month11Curr],
                  ISNULL(curr.[month12],0) AS [month12Curr],
                  ISNULL(prev.[month1],0)  AS [month1Prev],
                  ISNULL(prev.[month2],0)  AS [month2Prev],
                  ISNULL(prev.[month3],0)  AS [month3Prev],
                  ISNULL(prev.[month4],0)  AS [month4Prev],
                  ISNULL(prev.[month5],0)  AS [month5Prev],
                  ISNULL(prev.[month6],0)  AS [month6Prev],
                  ISNULL(prev.[month7],0)  AS [month7Prev],
                  ISNULL(prev.[month8],0)  AS [month8Prev],
                  ISNULL(prev.[month9],0)  AS [month9Prev],
                  ISNULL(prev.[month10],0) AS [month10Prev],
                  ISNULL(prev.[month11],0) AS [month11Prev],
                  ISNULL(prev.[month12],0) AS [month12Prev]
          FROM    [dbo].[agentactivity] curr FULL OUTER JOIN
                  [dbo].[agentactivity] prev
          ON      curr.[agentID] = prev.[agentID]
          AND     curr.[category] = prev.[category]
          AND     curr.[type] = prev.[type]
          AND     curr.[year] = prev.[year] + 1
          WHERE  (curr.[year] = YEAR(@date) OR prev.[year] = YEAR(DATEADD(YEAR, -1, @date)))
          AND     curr.[category] = 'N'
          AND     curr.[type] = 'P'
          ) aa
  ON      a.[agentID] = aa.[agentID]

  -- Insert the net premium percent
	INSERT INTO #agencyTotal ([agentID],[category],[yearCount],[lastDay],[month1Curr],[month2Curr],[month3Curr],[month4Curr],[month5Curr],[month6Curr],[month7Curr],[month8Curr],[month9Curr],[month10Curr],[month11Curr],[month12Curr],[month1Prev],[month2Prev],[month3Prev],[month4Prev],[month5Prev],[month6Prev],[month7Prev],[month8Prev],[month9Prev],[month10Prev],[month11Prev],[month12Prev])
  SELECT  r.[agentID],
          'NP%' AS [category],
          CASE WHEN p.[yearCount]   = 0 THEN 0 ELSE r.[yearCount] / p.[yearCount]  * 100    END,
          -9999999 AS [lastDay],
          CASE WHEN p.[month1Curr]  = 0 THEN 0 ELSE r.[month1Curr]  / p.[month1Curr]  * 100 END,
          CASE WHEN p.[month2Curr]  = 0 THEN 0 ELSE r.[month2Curr]  / p.[month2Curr]  * 100 END,
          CASE WHEN p.[month3Curr]  = 0 THEN 0 ELSE r.[month3Curr]  / p.[month3Curr]  * 100 END,
          CASE WHEN p.[month4Curr]  = 0 THEN 0 ELSE r.[month4Curr]  / p.[month4Curr]  * 100 END,
          CASE WHEN p.[month5Curr]  = 0 THEN 0 ELSE r.[month5Curr]  / p.[month5Curr]  * 100 END,
          CASE WHEN p.[month6Curr]  = 0 THEN 0 ELSE r.[month6Curr]  / p.[month6Curr]  * 100 END,
          CASE WHEN p.[month7Curr]  = 0 THEN 0 ELSE r.[month7Curr]  / p.[month7Curr]  * 100 END,
          CASE WHEN p.[month8Curr]  = 0 THEN 0 ELSE r.[month8Curr]  / p.[month8Curr]  * 100 END,
          CASE WHEN p.[month9Curr]  = 0 THEN 0 ELSE r.[month9Curr]  / p.[month9Curr]  * 100 END,
          CASE WHEN p.[month10Curr] = 0 THEN 0 ELSE r.[month10Curr] / p.[month10Curr] * 100 END,
          CASE WHEN p.[month11Curr] = 0 THEN 0 ELSE r.[month11Curr] / p.[month11Curr] * 100 END,
          CASE WHEN p.[month12Curr] = 0 THEN 0 ELSE r.[month12Curr] / p.[month12Curr] * 100 END,
          CASE WHEN p.[month1Prev]  = 0 THEN 0 ELSE r.[month1Prev] /  p.[month1Prev]  * 100 END,
          CASE WHEN p.[month2Prev]  = 0 THEN 0 ELSE r.[month2Prev] /  p.[month2Prev]  * 100 END,
          CASE WHEN p.[month3Prev]  = 0 THEN 0 ELSE r.[month3Prev] /  p.[month3Prev]  * 100 END,
          CASE WHEN p.[month4Prev]  = 0 THEN 0 ELSE r.[month4Prev] /  p.[month4Prev]  * 100 END,
          CASE WHEN p.[month5Prev]  = 0 THEN 0 ELSE r.[month5Prev] /  p.[month5Prev]  * 100 END,
          CASE WHEN p.[month6Prev]  = 0 THEN 0 ELSE r.[month6Prev] /  p.[month6Prev]  * 100 END,
          CASE WHEN p.[month7Prev]  = 0 THEN 0 ELSE r.[month7Prev] /  p.[month7Prev]  * 100 END,
          CASE WHEN p.[month8Prev]  = 0 THEN 0 ELSE r.[month8Prev] /  p.[month8Prev]  * 100 END,
          CASE WHEN p.[month9Prev]  = 0 THEN 0 ELSE r.[month9Prev] /  p.[month9Prev]  * 100 END,
          CASE WHEN p.[month10Prev] = 0 THEN 0 ELSE r.[month10Prev] / p.[month10Prev] * 100 END,
          CASE WHEN p.[month11Prev] = 0 THEN 0 ELSE r.[month11Prev] / p.[month11Prev] * 100 END,
          CASE WHEN p.[month12Prev] = 0 THEN 0 ELSE r.[month12Prev] / p.[month12Prev] * 100 END
  FROM    #agencyTotal r FULL OUTER JOIN
          #agencyTotal p
  ON      r.[agentID] = p.[agentID]
  WHERE   r.[category] = 'NPR'
  AND     p.[category] = 'NPP'

  -- The ORDER BY clause is
  --   1. By Manager
  --   2. Net Premium Plan (in a case statement)
  --   3. Yearly Count
  --   4. Agent Name
  --   4. Agent ID
  SELECT  a.[name] AS [agentName],
          a.[agentID],
          a.[stat] AS [agentStat],
          m.[uid] AS [manager],
          [category],
          [yearCount],
          [lastDay],
          [month1Curr],
          [month2Curr],
          [month3Curr],
          [month4Curr],
          [month5Curr],
          [month6Curr],
          [month7Curr],
          [month8Curr],
          [month9Curr],
          [month10Curr],
          [month11Curr],
          [month12Curr],
          [month1Prev],
          [month2Prev],
          [month3Prev],
          [month4Prev],
          [month5Prev],
          [month6Prev],
          [month7Prev],
          [month8Prev],
          [month9Prev],
          [month10Prev],
          [month11Prev],
          [month12Prev]
  FROM    [dbo].[agent] a INNER JOIN
          #agencyTotal aa
  ON      a.[agentID] = aa.[agentID] INNER JOIN
          [dbo].[agentmanager] m
  ON      a.[agentID] = m.[agentID]
  WHERE   m.[isPrimary] = 1
  AND     m.[stat] = 'A'
  ORDER BY m.[uid], CASE WHEN [category] = 'NPP' THEN 0 ELSE 1 END, aa.[yearCount] DESC, a.[name], a.[agentID]
END
