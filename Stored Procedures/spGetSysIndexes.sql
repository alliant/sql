-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
USE [COMPASS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Gurvindar>
-- Create date: <07-16-2109>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE spGetSysIndexes
	-- Add the parameters for the stored procedure here
  @pcObjType VARCHAR(30) = 'ALL',
  @pcObjID VARCHAR(30)   = 'ALL'
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
  SELECT  *
  FROM    [dbo].[sysindex] s
  where s.[objType] = (CASE WHEN @pcObjType   = 'ALL' THEN s.[objType]    ELSE @pcObjType   END) 
    and s.[objID]   = (CASE WHEN @pcObjID     = 'ALL' THEN s.[objID]      ELSE @pcObjID     END) 
   	
END
GO
