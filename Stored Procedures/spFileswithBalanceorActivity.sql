
/****** Object:  StoredProcedure [dbo].[spFileswithBalanceorActivity]    Script Date: 6/10/2024 7:36:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spFileswithBalanceorActivity]
  @startDate DATETIME = NULL,
  @endDate   DATETIME = NULL,
  @agentID   VARCHAR(MAX) = 'ALL'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;


  CREATE TABLE #agentFile (agentfileid  varchar(80),
						 agentID      VARCHAR(80),
		        		 fileID       VARCHAR(50),
						 arAmount     [DECIMAL](17, 2) NULL,
						 periodCount  INTEGER,	
						 trandate     date 
						 )
						     
	-- Insert statements for procedure here
	CREATE TABLE #agentTable ([agentID] VARCHAR(30))
    INSERT INTO #agentTable ([agentID])
    SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)

     /* Get all file with activity and its balance as of given date from fileAR */
   INSERT INTO #agentFile ([agentfileid],[agentID],[fileID],[arAmount],[periodCount], [trandate])
   SELECT fileAR.agentFileID, 
       fileAR.agentID,
       fileAR.fileID,
       SUM(CASE WHEN fileAR.tranDate <= CAST(@endDate AS DATE) /* file balance as of statement end date */
			        AND fileAR.Type <> 'F'
				    THEN fileAR.amount ELSE 0 END),
		COUNT(CASE WHEN fileAR.tranDate >= CAST(@startDate AS DATE) /* file is fulfilled or has activity during statement period  */
                    AND fileAR.tranDate <= CAST(@endDate AS DATE)
					AND fileAR.Type <> 'F'						   
                    THEN 1 ELSE NULL END),
	    Max(CASE WHEN fileAR.tranDate <= CAST(@endDate AS DATE) /* latest invoice date as of statement end date */
                    AND fileAR.Type = 'I'						   
                    THEN fileAR.tranDate ELSE NULL END) 		   
    FROM [dbo].[fileAR]  
    INNER JOIN 
    [dbo].[agent] a 
    ON a.agentID = fileAR.agentid
    WHERE fileAR.[agentid] IN (SELECT [agentID] FROM #agentTable)
		AND (fileAR.type = 'I' or fileAR.type = 'A' OR fileAR.type = 'W')
	GROUP BY fileAR.agentid,fileAR.fileID,fileAR.agentFileID


	DELETE #agentFile
	    WHERE  #agentFile.periodCount = 0 
		  AND ( #agentFile.arAmount = 0)	


	SELECT * FROM #agentfile
	DROP TABLE  #agentfile
	DROP TABLE  #agentTable
  
END
