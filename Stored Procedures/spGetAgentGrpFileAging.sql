USE [COMPASS]
GO
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetAgentGrpFileAging]
	-- Add the parameters for the stored procedure here
	
	/* @agentID     VARCHAR(MAX) = 'ALL', */
	@asOfDate    DATETIME     = NULL ,
	@reportType  VARCHAR(1),
	@reportData  VARCHAR(10),
	@allagents   BIT,
	@UID varchar(100) = '',	
	@stateID     VARCHAR(MAX) = 'ALL',
	@YearOfSigning integer = 0,
	@Software VARCHAR(MAX) = '',
	@Company VARCHAR(MAX) = '',
	@Organization VARCHAR(MAX) = '',
	@Manager VARCHAR(MAX) = '',
	@TagList VARCHAR(MAX) = '',
	@AffiliationList VARCHAR(MAX) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	/* CREATE TABLE #agentTable ([agentID] VARCHAR(30))
    INSERT INTO #agentTable ([agentID])
    SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID) */
    
	declare @agentID     VARCHAR(MAX) = 'ALL'
	
    /* CREATE TABLE #stateTable ([stateID] VARCHAR(2))
    INSERT INTO #stateTable ([stateID])
    SELECT [field] FROM [dbo].[GetEntityFilter] ('State', @stateID) */

	CREATE TABLE #arAging (stateID      VARCHAR(2),
	     			       agentID      VARCHAR(80),
						   name         VARCHAR(200),
						   manager      VARCHAR(200),
						   fileNumber   VARCHAR(50),
						   fileID       VARCHAR(50),
						   tranDate     datetime,
                           receiptDate  datetime,
						   postedDate   DATETIME,
						   invoiced     [decimal](17, 2) NULL, 
						   artranID     Integer,
						   stat         VARCHAR(1),
						   type         VARCHAR(1),
						   tranID       VARCHAR(50),
                           due0_30      [decimal](17, 2) NULL,
						   due31_60     [decimal](17, 2) NULL,
						   due61_90     [decimal](17, 2) NULL,
						   due91_       [decimal](17, 2) NULL,
						   balance      [decimal](17, 2) NULL)
						   
    CREATE TABLE #arAgingFiltered (stateID      VARCHAR(2),
	     			       agentID      VARCHAR(80),
						   name         VARCHAR(200),
						   manager      VARCHAR(200),
						   fileNumber   VARCHAR(50),
						   fileID       VARCHAR(50),
						   tranDate     datetime,
                           receiptDate  datetime,
						   postedDate   DATETIME,
						   invoiced     [decimal](17, 2) NULL, 
						   artranID     Integer,
						   stat         VARCHAR(1),
						   type         VARCHAR(1),
						   tranID       VARCHAR(50),
                           due0_30      [decimal](17, 2) NULL,
						   due31_60     [decimal](17, 2) NULL,
						   due61_90     [decimal](17, 2) NULL,
						   due91_       [decimal](17, 2) NULL,
						   balance      [decimal](17, 2) NULL)
						   
    CREATE TABLE #agentsGroup
   (
   [agentID] VARCHAR(20),
   [corporationID] VARCHAR(MAX),  
   [stateID] VARCHAR(2),
   [stateName] VARCHAR(20), 
   [stat] VARCHAR(1),
   [name] VARCHAR(MAX),
   [contractDate] datetime,
   [swVendor] VARCHAR(100),
   [manager] VARCHAR(MAX),
   [orgID] VARCHAR(50),
   [orgRoleID] integer, 
   [orgName] VARCHAR(Max)
   )	

INSERT INTO #agentsGroup ([agentID] , [corporationID]  , [stateID] , [stateName] , [stat], [name], [contractDate] , [swVendor], [manager], [orgID] , [orgRoleID] , [orgName]  )
Exec SpGetConsolidatedAgents @StateID ,
	@YearOfSigning ,
	@Software,
	@Company  ,
	@Organization ,
    @Manager  ,
	@TagList ,
	@AffiliationList  ,
	@UID 

--Get activities of each agent
  INSERT INTO #arAging ([stateID], [agentID],[name],[manager],[fileNumber],[fileID],[tranDate],[receiptDate],[postedDate],[invoiced],[artranID],[stat],[type],[tranID],[due0_30],[due31_60],[due61_90],[due91_],[balance])
    EXEC [dbo].[spGetFileAging] @StateID = 'ALL', @AgentID = 'ALL', @asOfDate = @asOfDate, @reportType = @reportType, @reportData = @reportData, @allagents = 0
   	
  INSERT INTO #arAgingFiltered  select * from  #arAging ag  where 
         (ag.[agentID]  IN (SELECT [agentID] FROM #agentsGroup)) 
 
  select * from #arAgingFiltered
  drop table #arAging
  drop table #arAgingFiltered
  drop table #agentsGroup	
	     			     
END
