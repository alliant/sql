USE [COMPASS_DVLP]
GO
/****** Object:  StoredProcedure [dbo].[spGetNextKey]    Script Date: 5/11/2021 2:00:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetNextSequence]
  @keyType VARCHAR(50),
	@key INT OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
  SET NOCOUNT ON;
  IF (@keyType NOT LIKE '%seq_%')
    SET @keyType = 'seq_' + @keyType

  DECLARE @count INTEGER = 0
  SELECT @count = COUNT(*) FROM [sys].[sequences] WHERE [name] = @keyType
  IF (@count = 0)
  BEGIN
    SET @key = -1
  END
  ELSE
  BEGIN
    DECLARE @sql NVARCHAR(MAX) = ''
    DECLARE @ParmDefinition NVARCHAR(500)
    SET @ParmDefinition = N'@SeqVal INT OUTPUT';  
    SET @sql = 'select @SeqVal = NEXT VALUE FOR dbo.' + @keyType

    EXECUTE sp_executesql @sql, @ParmDefinition, @SeqVal=@key output
  END
END
