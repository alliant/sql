USE [COMPASS_ALFA]
GO
/****** Object:  StoredProcedure [dbo].[spGetAgentActuals]    Script Date: 6/21/2022 3:48:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetAgentActuals] 
	-- Add the parameters for the stored procedure here
	@piAgentID VARCHAR(MAX) = 'ALL',
    @activityStartDate DATE = '',
	@activityEndDate DATE   = ''
AS
BEGIN

SET @activityEndDate = (CASE  WHEN @activityEndDate != '' THEN @activityEndDate ELSE (DATEADD(day , -1, CAST(GETDATE() AS DATE))) END) 

CREATE TABLE #agentActuals (agentID					 VARCHAR(10),
							officeID				 VARCHAR(50),
							countyID				 VARCHAR(50),
							activityDate			 DATE,
							fileID					 VARCHAR(50),
							noOfLenderPolicyReported INTEGER ,
							noOfLenderPolicyIssued   INTEGER ,
							noOfLenderpolicyvoided   INTEGER ,
							noOfOwnerPolicyReported	 INTEGER ,
							noOfOwnerPolicyIssued	 INTEGER ,
							noOfOwnerpolicyvoided	 INTEGER ,
							noOfCPLReported		     INTEGER ,
							noOfCPLIssued		     INTEGER ,
							noOfCPLvoided		     INTEGER ,
							grossPremium		     DECIMAL(10, 2) DEFAULT 0,
							netPremium               DECIMAL(10, 2) DEFAULT 0,
							liabilityAmount          DECIMAL(10, 2) DEFAULT 0,  
							transactionType          VARCHAR(10)			   
							)

CREATE TABLE #tempAgentActuals (agentID				     VARCHAR(10),
								officeID				 VARCHAR(50),
								countyID				 VARCHAR(50),
								activityDate			 DATE,
								fileID					 VARCHAR(50),
								noOfLenderPolicyReported INTEGER ,
								noOfLenderPolicyIssued   INTEGER ,
								noOfLenderpolicyvoided   INTEGER ,
								noOfOwnerPolicyReported	 INTEGER ,
								noOfOwnerPolicyIssued	 INTEGER ,
								noOfOwnerpolicyvoided	 INTEGER ,
								noOfCPLReported		     INTEGER ,
								noOfCPLIssued		     INTEGER ,
								noOfCPLvoided		     INTEGER ,
								grossPremium		     DECIMAL(10, 2) DEFAULT 0,
								netPremium               DECIMAL(10, 2) DEFAULT 0,
								liabilityAmount          DECIMAL(10, 2) DEFAULT 0,  
								transactionType          VARCHAR(10)			    
								) 

WHILE (@activityStartDate <= @activityEndDate)
BEGIN

INSERT INTO #agentActuals (agentID,activityDate,fileID,noOfLenderPolicyReported,noOfOwnerPolicyReported,noOfCPLReported,grossPremium,netPremium,liabilityAmount)
SELECT  ag.agentID,
        @activityStartDate,
		batchform.fileID, 
		/*Reported*/
		(CASE WHEN batchform.formType = 'P' AND (stateform.insuredType = 'L' OR stateform.insuredType = 'B') THEN COUNT(batchform.formType) END), /*noOfLenderPolicyReported*/
		(CASE WHEN batchform.formType = 'P' AND stateform.insuredType = 'O' THEN COUNT(batchform.formType) END), /*noOfOwnerPolicyReported*/
		(CASE WHEN batchform.formType = 'C' THEN COUNT(batchform.formType) END),

		SUM(batchform.grossDelta),
		SUM(batchform.NetDelta),
		MAX(batchform.liabilityAmount)
		
	    FROM batch INNER JOIN agent ag ON  batch.agentID = ag.agentID
        INNER JOIN batchform ON batchform.batchID = batch.batchID 
		INNER JOIN stateform on stateform.stateID = batch.stateID and stateform.formID = batchform.formID
		WHERE ag.agentID = (CASE  WHEN @piAgentID != 'ALL' THEN @piAgentID ELSE ag.[agentID] END) AND CONVERT(DATE, batch.postDATE) = @activityStartDATE
        GROUP BY ag.agentID, batchform.fileID, batchform.formType,  stateform.insuredType

INSERT INTO #agentActuals (agentID,activityDate,fileID,noOfLenderPolicyIssued,noOfOwnerPolicyIssued)
SELECT  ag.agentID,
        @activityStartDate,
		policy.fileID,
		 /*issued*/		/*need to improve formID */
		(CASE WHEN  policy.formID = 'L' THEN COUNT(policy.formID) END), /*noOfLenderPolicyIssued*/
		(CASE WHEN  policy.formID = 'O' THEN COUNT(policy.formID) END)  /*noOfOwnerPolicyIssued*/
        
		FROM policy INNER JOIN agent ag ON   ag.agentID = policy.agentID 
        WHERE ag.agentID = (CASE  WHEN @piAgentID != 'ALL' THEN @piAgentID ELSE ag.[agentID] END) AND CONVERT(DATE, policy.issueDate) = @activityStartDate
        GROUP BY ag.agentID, policy.fileID ,policy.formID

INSERT INTO #agentActuals (agentID,activityDate,fileID,noOfLenderpolicyvoided,noOfOwnerpolicyvoided)
SELECT  ag.agentID,
        @activityStartDate,
		policy.fileID,
		/*voided*/ /*need to improve formID*/
		(CASE WHEN  policy.formID = 'L' THEN COUNT(policy.formID) END), /*noOfLenderpolicyvoided*/
		(CASE WHEN  policy.formID = 'O' THEN COUNT(policy.formID) END)  /*noOfOwnerpolicyvoided*/
        
		FROM policy INNER JOIN agent ag ON   ag.agentID = policy.agentID 
        WHERE ag.agentID = (CASE  WHEN @piAgentID != 'ALL' THEN @piAgentID ELSE ag.[agentID] END) AND CONVERT(DATE, policy.voidDate) = @activityStartDate
        GROUP BY ag.agentID, policy.fileID ,policy.formID
		
INSERT INTO #agentActuals (agentID,activityDate,fileID,noOfCPLIssued)
SELECT  ag.agentID,
        @activityStartDate,
		cpl.fileID ,
		COUNT(cpl.fileID) /*noOfCPLIssued*/
		FROM cpl INNER JOIN agent ag ON   ag.agentID = cpl.agentID 
         WHERE ag.agentID = (CASE  WHEN @piAgentID != 'ALL' THEN @piAgentID ELSE ag.[agentID] END) AND CONVERT(DATE, cpl.issueDate) = @activityStartDate
        GROUP BY ag.agentID, cpl.fileID

INSERT INTO #agentActuals (agentID,activityDate,fileID,noOfCPLvoided)
SELECT  ag.agentID,
        @activityStartDate,
		cpl.fileID,
		/*void*/
		COUNT(cpl.fileID) /*noOfCPLvoided*/
		FROM cpl INNER JOIN agent ag ON   ag.agentID = cpl.agentID 
        WHERE ag.agentID = (CASE  WHEN @piAgentID != 'ALL' THEN @piAgentID ELSE ag.[agentID] END) AND CONVERT(DATE, cpl.voidDate) = @activityStartDate
        GROUP BY ag.agentID, cpl.fileID


    SET @activityStartDate = DATEADD(DAY, 1, @activityStartDate) /*increment current date*/
END

INSERT INTO #tempAgentActuals (agentID,activityDate,fileID,noOfLenderPolicyReported,noOfLenderPolicyIssued,
                               noOfLenderpolicyvoided,noOfOwnerPolicyReported,noOfOwnerPolicyIssued,noOfOwnerpolicyvoided,noOfCPLReported,
					           noOfCPLIssued,noOfCPLvoided,grossPremium,netPremium,liabilityAmount)
SELECT  aA.agentID,
        aA.activityDate, 
		aA.fileID, 
		SUM(aA.noOfLenderPolicyReported),
		SUM(aA.noOfLenderPolicyIssued), 
		SUM(aA.noOfLenderpolicyvoided),
        SUM(aA.noOfOwnerPolicyReported),
		SUM(aA.noOfOwnerPolicyIssued),
		SUM(aA.noOfOwnerpolicyvoided),
        SUM(aA.noOfCPLReported),
		SUM(aA.noOfCPLIssued),
		SUM(aA.noOfCPLvoided),
        SUM(aA.grossPremium),
		SUM(aA.netPremium),
		MAX(aA.liabilityAmount)
		FROM #agentActuals aA 
		GROUP BY aA.activityDate, aA.agentID, aA.fileID 
		  
UPDATE #tempAgentActuals
SET  #tempAgentActuals.officeID	       = (SELECT TOP 1 officeID FROM cpl WHERE cpl.fileID = #tempAgentActuals.fileID AND cpl.agentID = #tempAgentActuals.agentID AND (CPL.officeID <> '' OR CPL.officeID IS NOT NULL)),
	 #tempAgentActuals.countyID	       = (SELECT TOP 1 countyID FROM batchform INNER JOIN  batch ON batchform.batchID = batch.batchID WHERE batch.agentID = #tempAgentActuals.agentID AND batchform.formtype = 'P'),
     #tempAgentActuals.transactionType = (CASE WHEN ((#tempAgentActuals.noOfLenderPolicyReported >= 1 OR #tempAgentActuals.noOfLenderPolicyIssued >= 1 OR #tempAgentActuals.noOfLenderpolicyvoided >= 1) 
										        AND (#tempAgentActuals.noOfOwnerPolicyReported   >= 1 OR #tempAgentActuals.noOfOwnerPolicyIssued  >= 1 OR #tempAgentActuals.noOfOwnerpolicyvoided  >= 1)) THEN 'Both' 
											   WHEN (#tempAgentActuals.NoofLenderPolicyReported  >= 1 OR #tempAgentActuals.noOfLenderPolicyIssued >= 1 OR #tempAgentActuals.noOfLenderpolicyvoided >= 1)  THEN 'Loan' 
	                                           WHEN (#tempAgentActuals.noOfOwnerPolicyReported   >= 1 OR #tempAgentActuals.noOfOwnerPolicyIssued  >= 1 OR #tempAgentActuals.noOfOwnerpolicyvoided  >= 1)  THEN 'Owner' 
									           ELSE 'None' END)

DELETE FROM #tempAgentActuals WHERE (#tempAgentActuals.noOfLenderPolicyReported = 0 OR #tempAgentActuals.noOfLenderPolicyReported IS NULL) AND (#tempAgentActuals.noOfLenderPolicyIssued  = 0 OR #tempAgentActuals.noOfLenderPolicyIssued  IS NULL) 
                                AND (#tempAgentActuals.noOfLenderpolicyvoided   = 0 OR #tempAgentActuals.noOfLenderpolicyvoided   IS NULL) AND (#tempAgentActuals.noOfOwnerPolicyReported = 0 OR #tempAgentActuals.noOfOwnerPolicyReported IS NULL) 
							    AND (#tempAgentActuals.noOfOwnerPolicyIssued    = 0 OR #tempAgentActuals.noOfOwnerPolicyIssued    IS NULL) AND (#tempAgentActuals.noOfOwnerpolicyvoided   = 0 OR #tempAgentActuals.noOfOwnerpolicyvoided   IS NULL) 
							    AND (#tempAgentActuals.noOfCPLReported          = 0 OR #tempAgentActuals.noOfCPLReported          IS NULL) AND (#tempAgentActuals.noOfCPLIssued           = 0 OR #tempAgentActuals.noOfCPLIssued           IS NULL) 
							    AND (#tempAgentActuals.noOfCPLvoided            = 0 OR #tempAgentActuals.noOfCPLvoided            IS NULL) 
								
SELECT aA.agentID,
       aA.officeID,
	   aA.countyID,
	   aA.activityDate, 
	   aA.fileID, 
	   COALESCE(aA.noOfLenderPolicyReported,0) as noOfLenderPolicyReported,
	   COALESCE(aA.noOfLenderPolicyIssued,0) as noOfLenderPolicyIssued, 
	   COALESCE(aA.noOfLenderpolicyvoided,0) as noOfLenderpolicyvoided,
	   COALESCE(aA.noOfOwnerPolicyReported,0) as noOfOwnerPolicyReported,
	   COALESCE(aA.noOfOwnerPolicyIssued,0) as noOfOwnerPolicyIssued,
	   COALESCE(aA.noOfOwnerpolicyvoided,0) as noOfOwnerpolicyvoided,
	   COALESCE(aA.noOfCPLReported,0) as noOfCPLReported,
	   COALESCE(aA.noOfCPLIssued,0) as noOfCPLIssued,
	   COALESCE(aA.noOfCPLvoided,0) as noOfCPLvoided,
	   COALESCE(aA.grossPremium,0) as grossPremium,
	   COALESCE(aA.netPremium,0) as netPremium,
	   COALESCE(aA.liabilityAmount,0) as liabilityAmount,
	   aA.transactionType
FROM #tempAgentActuals aA ORDER BY aA.agentID, aA.activityDate
   
DROP TABLE #agentActuals
Drop Table #tempAgentActuals
END
