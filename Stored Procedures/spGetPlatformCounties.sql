USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetPlatformCounties]    Script Date: 8/21/2019 11:04:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetPlatformCounties]
	-- Add the parameters for the stored procedure here
	@platformID INTEGER = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  SELECT  [platformID],
          [stateID],
          [countyID],
          [costPerSearch],
          [active],
          [month],
          [year]
  FROM    [dbo].[platformcounty]
  WHERE   [platformID] = CASE WHEN @platformID = 0 THEN [platformID] ELSE @platformID END
END
