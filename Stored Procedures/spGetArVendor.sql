/****** Object:  StoredProcedure [dbo].[spGetArVendor]    Script Date: 3/3/2023 2:39:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		KR
-- Create date: 28.11.2022
-- Description:	get ar vendor
-- =============================================
ALTER PROCEDURE [dbo].[spGetArVendor]
   @vendorId varchar(30) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [vendorid],[category],[vendorname] 
	from [dbo].[vendor] 
	where [vendorid] =  CASE WHEN @vendorId = '' THEN [vendorid] ELSE @vendorId END
	AND  [active] = 1 
END
