USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetAlerts]    Script Date: 10/9/2017 2:53:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetAlerts] 
  @alertID INT = 0,
  @agentID VARCHAR(MAX) = 'ALL',
  @stateID VARCHAR(200) = 'ALL',
  @UID varchar(100)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SET @agentID = [dbo].[StandardizeAgentID](@agentID)

  -- Get the state
  CREATE TABLE #stateTable ([stateID] VARCHAR(2))
  INSERT INTO #stateTable ([stateID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('State', @stateID)

  DECLARE @NextString NVARCHAR(100),
          @Pos INTEGER = 0,
          @Str NVARCHAR(MAX),
          @Delimiter NCHAR(1) = ','

  -- Get the correct alert
  CREATE TABLE #alertTable ([alertID] INTEGER)
  IF (@alertID != 0)
  BEGIN
    INSERT INTO #alertTable
    SELECT @alertID
  END
  ELSE
  BEGIN
    INSERT INTO #alertTable
    SELECT DISTINCT [alertID] FROM [dbo].[alert]
  END

  -- Get the data
  SELECT  alert.[alertID],
          agent.[agentID],
          agent.[name] AS [agentName],
          agent.[stateID],
          alert.[source],
          alert.[processCode],
          alert.[threshold],
          [dbo].[GetAlertThresholdRange] (alert.[processCode], alert.[score]) AS [thresholdRange],
          alert.[score],
          [dbo].[GetAlertScoreFormat] (alert.[processCode], alert.[score]) AS [scoreDesc],
          alert.[severity],
          alert.[description],
          alert.[dateCreated],
          alert.[createdBy],
          alert.[dateClosed],
          alert.[effDate],
          alert.[closedBy],
          alert.[stat],
          alert.[active],
          alert.[owner],
          note.[lastNoteDate]
  FROM    (
          SELECT  a.*
          FROM    [dbo].[alert] a INNER JOIN
                  #alertTable alert
          ON      a.[alertID] = alert.[alertID]
          ) alert INNER JOIN
          (
          SELECT  a.[agentID],
                  a.[name],
                  a.[stateID]
          FROM    [dbo].[agent] a  INNER JOIN
                  #stateTable s
          ON  a.[stateID] = s.[stateID]  where (a.[agentID] = (CASE  WHEN @agentID != 'ALL' THEN @agentID ELSE a.[agentID] END)) 
	                                     AND   (dbo.CanAccessAgent(@UID , a.[agentID]) = 1) 
          ) agent
  ON      alert.[agentID] = agent.[agentID] LEFT OUTER JOIN
          (
          SELECT  [alertID],
                  MAX([dateCreated]) AS [lastNoteDate]
          FROM    [dbo].[alertnote]
          GROUP BY [alertID]
          ) note
  ON      alert.[alertID] = note.[alertID]

  DROP TABLE #stateTable
END
