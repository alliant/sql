-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetSysLogs]
	-- Add the parameters for the stored procedure here
	@ipActionType VARCHAR(1),
	@ipUID        VARCHAR(100) = 'ALL',
	@ipAction     VARCHAR(32)  = 'ALL',
	@ipStartDate  DATETIME,
	@ipEndDate    DATETIME  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

   -- Return all sysLog for actions return error as per search criteria
	IF(@ipActionType = 'E')
	  BEGIN
	    SELECT sysLog.logID,
		       syslog.createdate,
			   sysLog.action,
			   sysLog.uid,
			   sysLog.addr,
			   sysLog.role,
			   sysLog.progexec,
			   sysLog.duration,
			   sysLog.iserror,
			   sysLog.faultCode,
			   sysLog.refType,
			   sysLog.refNum,
			   sysLog.applicationID
		FROM sysLog 
	        WHERE sysLog.action      = (CASE WHEN @ipAction  = 'ALL' THEN sysLog.action   ELSE @ipAction END)
			  AND sysLog.uid         = (CASE WHEN @ipUID     = 'ALL' THEN sysLog.uid      ELSE @ipUID    END)  
			  AND sysLog.isError     = 'true'
	          AND sysLog.createDate >= @ipStartDate 
	          AND sysLog.createDate <= @ipEndDate 

	  END
    -- Return all sysLog as per search criteria
    ELSE
	  BEGIN
	    SELECT sysLog.logID,
		       syslog.createdate,
			   sysLog.action,
			   sysLog.uid,
			   sysLog.addr,
			   sysLog.role,
			   sysLog.progexec,
			   sysLog.duration,
			   sysLog.iserror,
			   sysLog.faultCode,
			   sysLog.refType,
			   sysLog.refNum,
			   sysLog.applicationID
		FROM sysLog 
	        WHERE sysLog.action      = (CASE WHEN @ipAction  = 'ALL' THEN sysLog.action   ELSE @ipAction END)
			  AND sysLog.uid         = (CASE WHEN @ipUID     = 'ALL' THEN sysLog.uid      ELSE @ipUID    END)  
	          AND sysLog.createDate >= @ipStartDate 
	          AND sysLog.createDate <= @ipEndDate 
	  END
END
GO
