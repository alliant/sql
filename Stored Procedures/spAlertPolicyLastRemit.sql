GO
/****** Object:  StoredProcedure [dbo].[spAlertCPLVolume]    Script Date: 7/5/2018 4:24:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spAlertPolicyLastRemit]
	-- Add the parameters for the stored procedure here
  @agentID VARCHAR(MAX) = 'ALL',
  @user VARCHAR(100) = 'compass@alliantnational.com',
  @preview BIT = 0,
  @lastRemit INTEGER = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;
  
  CREATE TABLE #agentTable ([agentID] VARCHAR(30))
  INSERT INTO #agentTable ([agentID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)

  DECLARE @source VARCHAR(30) = 'OPS',
          @processCode VARCHAR(30) = 'OPS02',
          @score DECIMAL(18,2) = 0,
          @date DATETIME = NULL,
          -- Notes for the alert note
          @note VARCHAR(MAX) = ''
  
  CREATE TABLE #remitTable ([agentID] VARCHAR(30), [date] DATETIME, [lastRemit] INTEGER)

  -- Days since last remit
  INSERT INTO #remitTable ([agentID],[date],[lastRemit])
  SELECT  [agentID],
          [date],
          DATEDIFF(day,[date],GETDATE())
  FROM    (
          SELECT  [agentID],
                  MAX(ISNULL([receivedDate],[createDate])) AS [date]
          FROM    [dbo].[batch]
          WHERE   [stat] = 'C'
          AND     [agentID] IN (SELECT [agentID] FROM #agentTable)
          GROUP BY [agentID]
          ) a

  -- Insert alert or preview  
  IF (@preview = 0)
  BEGIN
    DECLARE cur CURSOR LOCAL FOR
    SELECT  [agentID],
            [date],
            [lastRemit],
            'The last remittance on ' + CONVERT(VARCHAR, [date], 101) + ' was ' + [dbo].[FormatNumber] ([lastRemit], 0) + ' day(s) ago'
    FROM    #remitTable

    OPEN cur
    FETCH NEXT FROM cur INTO @agentID, @date, @score, @note
    WHILE @@FETCH_STATUS = 0 
    BEGIN
      EXEC [dbo].[spInsertAlert] @source = @source, 
                                 @processCode = @processCode, 
                                 @user = @user, 
                                 @agentID = @agentID, 
                                 @score = @score,
                                 @effDate = @date,
                                 @note = @note
      FETCH NEXT FROM cur INTO @agentID, @date, @score, @note
    END
    CLOSE cur
    DEALLOCATE cur
  END
  
  SELECT  [agentID] AS [agentID],
          @source AS [source],
          @processCode AS [processCode],
          [dbo].[GetAlertThreshold] (@processCode, [lastRemit]) AS [threshold],
          [dbo].[GetAlertThresholdRange] (@processCode, [lastRemit]) AS [thresholdRange],
          [dbo].[GetAlertSeverity] (@processCode, [lastRemit]) AS [severity],
          [lastRemit] AS [score],
          [dbo].[GetAlertScoreFormat] (@processCode, [lastRemit]) AS [scoreDesc],
          [date] AS [effDate],
          [dbo].[GetAlertOwner] (@processCode) AS [owner],
          'The last remittance on ' + CONVERT(VARCHAR, [date], 101) + ' was ' + [dbo].[FormatNumber] ([lastRemit], 0) + ' day(s) ago' AS [note]
  FROM    #remitTable

  DROP TABLE #agentTable
  DROP TABLE #remitTable
END
