USE [compass]
GO
/****** Object:  StoredProcedure [dbo].[spGetRateScenarioCards]    Script Date: 11/1/2019 11:24:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Sachin Chaturvedi>
-- Create date: <10-31-2019>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetRateScenarioCards]
	-- Add the parameters for the stored procedure here
  @piCardSetID INTEGER = 0,
  @piScenarioID INTEGER = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
  IF @piScenarioID  != 0  
  SELECT * FROM [dbo].[ratescenariocard] q where q.scenarioID = @piScenarioID
  ELSE IF @piCardSetID != 0 
  SELECT *
  FROM [dbo].[ratescenariocard] q where q.scenarioID in (select scenarioID from ratescenario where cardSetID = @piCardSetID) 	
END
