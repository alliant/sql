/****** Object:  StoredProcedure [dbo].[spGetOpenOrdersJobQueues]    Script Date: 9/1/2022 1:19:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetOpenOrdersJobQueues] 
	-- Add the parameters for the stored procedure here
	@userID VARCHAR(100) = ''
AS
BEGIN
	DECLARE @agentFileID    INTEGER,
			@suffix         INTEGER,
			@UID            VARCHAR(50),
			@prodQueueID    INTEGER,
			@viewQueues     VARCHAR(1000),
			@viewAssignedTo VARCHAR(1000),
			@skip           BIT


	CREATE TABLE #tempJobQueue
	(
		[agentFileID]      INTEGER,
		[agentID]          VARCHAR(50),
		[agentName]        VARCHAR(200),
		[fileNumber]       VARCHAR(50),
		[propertyAddress]  VARCHAR(1100),
		[stateID]          VARCHAR(2),
		[stateName]        VARCHAR(60),
		[isHold]           BIT,
		[holdBy]           VARCHAR(100),
		[dueDate]          DATETIME,
		[orderID]          INTEGER,
		[customerOrder]    VARCHAR(50),
		[currOrdProd]      VARCHAR(100),
		[productID]        INTEGER,
		[productName]      VARCHAR(80),
		[orderedDate]      DATETIME,
		[prodQueueID]      INTEGER,
		[prodQueueName]    VARCHAR(50),
		[assignedToUID]    VARCHAR(100),
		[assignedTo]       VARCHAR(100),
		[lastModifiedDate] DATETIME,
		[stat]             VARCHAR(50),
		[statusCode]       VARCHAR(1)
	)

	SELECT @viewQueues = viewQueues, @viewAssignedTo = viewassignedto FROM sysuser WHERE sysuser.uid = @userID

	INSERT INTO #tempJobQueue( [agentFileID], [agentID], [agentName], [fileNumber], [propertyAddress], [stateID], [stateName], [isHold],[holdBy],[dueDate],[orderID],[customerOrder],[currOrdProd],[productID],[productName],[orderedDate],[prodQueueID],[prodQueueName],[assignedToUID],[assignedTo],[lastModifiedDate],[statusCode])
	SELECT jobQueue.[agentFileID], 		   
		   agentFile.[agentID],
		   (SELECT name FROM agent WHERE agent.agentID = agentfile.agentID) AS [agentName],
		   agentfile.[fileNumber],
		   (SELECT TRIM(', ' FROM 
				(CASE WHEN fileproperty.addr1    > '' THEN fileproperty.addr1    + ', ' ELSE ''  END) + 
				(CASE WHEN fileproperty.addr2    > '' THEN fileproperty.addr2    + ', ' ELSE ''  END) + 
				(CASE WHEN fileproperty.addr3    > '' THEN fileproperty.addr3    + ', ' ELSE ''  END) + 
				(CASE WHEN fileproperty.addr4    > '' THEN fileproperty.addr4    + ', ' ELSE ''  END) +
				(CASE WHEN fileproperty.city     > '' THEN fileproperty.city     + ', ' ELSE ''  END) +
				(CASE WHEN fileproperty.stateID  > '' THEN fileproperty.stateID  + ', ' ELSE ''  END) +
				(CASE WHEN fileproperty.zip      > '' THEN fileproperty.zip      + ', ' ELSE ''  END) +
				(CASE WHEN fileproperty.countyID > '' THEN fileproperty.countyID + ', ' ELSE ''  END) +
				(CASE WHEN fileProperty.countyID > '' AND 
					EXISTS(SELECT * FROM county WHERE county.countyID = fileProperty.countyID AND county.stateID = fileproperty.stateID) THEN 
						(SELECT county.description FROM county WHERE county.countyID = fileProperty.countyID AND county.stateID = fileproperty.stateID) + ', '
				 ELSE '' END)) 				
				FROM fileProperty WHERE fileproperty.agentfileid = agentfile.agentFileID and fileproperty.seq = 1
		   ) AS [propertyAddress],
		   agentfile.[stateID],
		   (SELECT description FROM state WHERE state.stateID = agentfile.[stateID]),
		   jobQueue.[isHold],
		   jobQueue.[holdBy],
		   job.[dueDate],
		   job.[orderID],
		   job.[customerOrder],
		   job.[customerProduct],
		   job.[productID],
		   (SELECT description FROM product WHERE product.productid = job.[productID]) AS [productName],
		   job.[orderedDate],
		   jobQueue.[prodQueueID],
		   (SELECT description FROM prodQueue WHERE prodQueue.prodQueueID = jobqueue.[prodQueueID]) AS [prodQueueName],
		   jobQueue.[uid],
		   (SELECT name FROM sysuser WHERE sysuser.uid = jobqueue.[uid]) AS [assignedTo],
		   job.[lastModifiedDate],
		   job.[stat]

	FROM jobQueue 
	INNER JOIN job ON job.agentfileid = jobQueue.agentFileID AND job.suffix = jobQueue.suffix and (job.stat = 'N' or job.stat = 'P' or job.stat = 'R')
	INNER JOIN agentfile ON agentfile.agentFileID = jobQueue.agentfileid and agentfile.suffix = jobQueue.suffix
	WHERE (
			((jobqueue.uid = '' OR jobqueue.uid IS NULL) AND (jobqueue.prodqueueid = '' OR jobqueue.prodqueueid IS NULL)) OR
			(jobQueue.uid = @userID) OR 
			([dbo].[GetCanDo](@viewQueues, jobQueue.prodQueueID) = 1 AND (jobQueue.uid = '' OR [dbo].[GetCanDo](@viewAssignedTo, jobQueue.uid) = 1)) 
		)
    
	UPDATE #tempJobQueue
	set #tempJobQueue.stat = (CASE WHEN #tempJobQueue.[statusCode] = 'N' THEN 'New' 
		                           WHEN #tempJobQueue.[statusCode] = 'P' THEN 'Processing' 
				                   WHEN #tempJobQueue.[statusCode] = 'R' THEN 'Rendered'
								   WHEN #tempJobQueue.[statusCode] = 'F' THEN 'Fulfilled' 
		                           WHEN #tempJobQueue.[statusCode] = 'C' THEN 'Complete' 
				                   WHEN #tempJobQueue.[statusCode] = 'X' THEN 'Canceled'
								   ELSE '' END)
	
	SELECT * FROM #tempJobQueue

	DROP TABLE #tempJobQueue	

END


