GO
/****** Object:  StoredProcedure [dbo].[spGetFilesWithoutOrder]    Script Date: 2/18/2025 12:10:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetFilesWithoutOrder]
	-- Add the parameters for the stored procedure here
	@cStateID VARCHAR(3) = 'ALL',
	@dMinCreatedDate DATE = NULL 
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  -- Insert statements for procedure here
  -- Temporary table definition
    CREATE TABLE #queryfile (	   agent             VARCHAR(200),
								   agentfileID       INTEGER,	
	                               agentID           VARCHAR(20),
								   fileID            VARCHAR(50),
						           fileNumber        VARCHAR(50),
								   closingDate       DATETIME,
								   createdDate       DATETIME,
								   notes			 VARCHAR(8000),
								   stateID           VARCHAR(2),
								   stateDesc         VARCHAR(60),
								   county			 VARCHAR(60),
								   openTasks         INTEGER,
								   LastCompletedTask VARCHAR(60),
								   completedBy       VARCHAR(80),
								   CompletedDate     DATETIME
								   )
IF (@dMinCreatedDate IS NULL)
    SET @dMinCreatedDate = '2005-01-01'

INSERT INTO #queryfile(agent,agentfileID,agentID,fileID,fileNumber,closingDate,createdDate,notes,stateID,stateDesc)  
SELECT 
 a.Name + ' (' + a.agentID + ')' as agent,
 af.agentFileID,
 af.agentID,
 fileID,
 fileNumber,
 closingDate,
 createdDate,
 af.Notes,
 a.stateID,
 s.description as stateDesc 
FROM agentfile af LEFT OUTER JOIN agent a
ON a.agentID = af.agentID LEFT OUTER JOIN state s
on s.stateID = a.stateID 
WHERE source = 'P'															   AND 
 a.stateID  = (CASE WHEN @cstateID = 'ALL' THEN a.stateID else @cstateID END)  AND
 (af.createddate >= @dMinCreatedDate)               AND
 NOT EXISTS (SELECT orderID FROM job WHERE job.agentfileid = af.agentFileID)

UPDATE #queryfile set #queryfile.openTasks = (select COUNT(*) from filetask where filetask.agentfileID = #queryfile.agentfileID
																					AND filetask.completeddate is null),

					  #queryfile.LastCompletedTask = (select ft.topic from (select top 1 topic from filetask where filetask.agentfileID = #queryfile.agentfileID
																					AND filetask.completeddate is not null
																					order by filetask.completeddate desc) ft),
					  #queryfile.completedBy       = (select fc.name from (select top 1 filetask.assignedto, sysuser.name from filetask inner join 
																			sysuser on 
																			filetask.assignedto = sysuser.uid where 
																			 filetask.agentfileID = #queryfile.agentfileID AND 
																			 filetask.completeddate is not null
																			  order by filetask.completeddate desc) fc),
					  #queryfile.CompletedDate     = (select cd.CompletedDate from (select top 1 CompletedDate from filetask where filetask.agentfileID = #queryfile.agentfileID
																					AND filetask.completeddate is not null
																					order by filetask.completeddate desc) cd),
					  #queryfile.county            = (select cty.description from (select top 1 description from fileproperty inner join 
																			county on 
																			fileproperty.countyid = county.countyID AND
																			fileproperty.stateid = county.stateid
																			where fileproperty.agentfileID = #queryfile.agentfileID
																			 AND county.countyid is not null) cty)


 SELECT * FROM #queryfile order by stateID, createdDate
 DROP TABLE #queryfile
END