GO
/****** Object:  StoredProcedure [dbo].[spAlertReserve]    Script Date: 11/29/2017 9:28:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spAlertReserve]
  @agentID VARCHAR(MAX) = '',
  @endDate DATETIME = NULL,
  @user VARCHAR(100) = 'compass@alliantnational.com',
  @preview BIT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  IF (@agentID = '')
    SET @agentID = 'ALL'
  
  CREATE TABLE #agentTable ([agentID] VARCHAR(30))
  INSERT INTO #agentTable ([agentID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)
  
  DECLARE -- Used for the preview
          @source VARCHAR(30) = 'CLM',
          @processCode VARCHAR(30) = 'CLM02',
          @effDate DATETIME = NULL,
          @score DECIMAL(18,2) = 0,
          -- Notes for the alert note
          @note VARCHAR(MAX) = '',
          -- Start date
          @startDate DATETIME = NULL

  IF (@endDate IS NULL)
    SET @endDate = GETDATE()

  SET @startDate = DATEADD(YEAR, -1, @endDate)

  CREATE TABLE #ratioTable
  (
    [agentID] VARCHAR(20),
    [laePaidDelta] DECIMAL(18,2),
    [lossPaidDelta]  DECIMAL(18,2),
    [laeReserveDelta]  DECIMAL(18,2),
    [lossReserveDelta]  DECIMAL(18,2),
    [recoveriesDelta] DECIMAL(18,2),
    [pendingRecoveries] DECIMAL(18,2),
    [netPremium] DECIMAL(18,2)
  )
  
  -- Insert statements for procedure here
  INSERT INTO #ratioTable ([agentID],[laePaidDelta],[lossPaidDelta],[laeReserveDelta],[lossReserveDelta],[recoveriesDelta],[pendingRecoveries],[netPremium])
  EXEC [dbo].[spReportClaimsRatio] @startDate = @startDate, @endDate = @endDate

  -- Get the final table
  CREATE TABLE #reserveTable
  (
    [agentID] VARCHAR(30),
    [netPremium] DECIMAL(18,2),
    [costsIncurred] DECIMAL(18,2),
    [date] DATETIME,
    [ratio] DECIMAL(18,2)
  )

  INSERT INTO #reserveTable ([agentID],[netPremium],[costsIncurred],[date],[ratio])
  SELECT  [agentID],
          ISNULL([netPremium],0),
          [laePaidDelta] + [lossPaidDelta] + [laeReserveDelta] + [lossReserveDelta] - [recoveriesDelta],
          @endDate,
          ([laePaidDelta] + [lossPaidDelta] + [laeReserveDelta] + [lossReserveDelta] - [recoveriesDelta]) / CASE WHEN ISNULL([netPremium],0) = 0 THEN 1 ELSE [netPremium] END
  FROM    #ratioTable
  WHERE   [agentID] IN (SELECT [agentID] FROM #agentTable)
             
  IF (@preview = 0)
  BEGIN
    -- Net Premium Over Reserve Score Alert
    DECLARE cur CURSOR LOCAL FOR
    SELECT  [agentID],
            [ratio],
            [date],
            'The calculation is Costs Incurred (' + [dbo].[FormatNumber] ([costsIncurred], 0) + ') / Net Premium (' + [dbo].[FormatNumber] ([netPremium], 0) + ') for last year (' +  CONVERT(VARCHAR,@startDate,101) + ' and ' + CONVERT(VARCHAR,@endDate,101) + ')'
    FROM    #reserveTable

    OPEN cur
    FETCH NEXT FROM cur INTO @agentID, @score, @effDate, @note
    WHILE @@FETCH_STATUS = 0 BEGIN
      EXEC [dbo].[spInsertAlert] @source = @source,
                                 @processCode = @processCode, 
                                 @user = @user, 
                                 @agentID = @agentID,
                                 @score = @score,
                                 @effDate = @effDate,
                                 @note = @note
      FETCH NEXT FROM cur INTO @agentID, @score, @effDate, @note
    END
    CLOSE cur
    DEALLOCATE cur
  END

  SELECT  [agentID] AS [agentID],
          @source AS [source],
          @processCode AS [processCode],
          [dbo].[GetAlertThreshold] (@processCode, [ratio]) AS [threshold],
          [dbo].[GetAlertThresholdRange] (@processCode, [ratio]) AS [thresholdRange],
          [dbo].[GetAlertSeverity] (@processCode, [ratio]) AS [severity],
          [ratio] AS [score],
          [dbo].[GetAlertScoreFormat] (@processCode, [ratio]) AS [scoreDesc],
          [date] AS [effDate],
          [dbo].[GetAlertOwner] (@processCode) AS [owner],
          'The calculation is Costs Incurred (' + [dbo].[FormatNumber] ([costsIncurred], 0) + ') / Net Premium (' + [dbo].[FormatNumber] ([netPremium], 0) + ') for last year (' +  CONVERT(VARCHAR,@startDate,101) + ' and ' + CONVERT(VARCHAR,@endDate,101) + ')' AS [note]
  FROM    #reserveTable

  DROP TABLE #agentTable
  DROP TABLE #ratioTable
  DROP TABLE #reserveTable
END
