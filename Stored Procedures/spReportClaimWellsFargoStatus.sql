USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spReportClaimWellsFargo]    Script Date: 1/18/2017 1:39:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportClaimWellsFargoStatus] 
	-- Add the parameters for the stored procedure here
  @month INTEGER = NULL,
  @year INTEGER = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  IF (@month IS NULL)
    SET @month = MONTH(GETDATE())

  IF (@year IS NULL)
    SET @year = YEAR(GETDATE())

  --set the first day of month and last day
  DECLARE @firstDay DATETIME,
          @lastDay DATETIME

  SET @firstDay = CONVERT(VARCHAR,@year) + '-' + CONVERT(VARCHAR,@month) + '-01'
  SET @lastDay = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,CAST(CAST(@year AS varchar) + '-' + CAST(@month AS varchar) + '-01' AS DATETIME))+1,0))

  CREATE TABLE #claimTable ([claimID] INTEGER)

  INSERT INTO #claimTable
  SELECT  [claimID]
  FROM    [claimattr]
  WHERE   [attrCode] = 'CA10'
  AND     [attrValue] = 'Wells Fargo'

  SELECT  attr.[serviceName],
          attr.[loanNumber],
          clm.[claimID],
          attr.[statusToLender],
          attr.[acceptedAction],
          attr.[denialReason],
          clm.[dateCreated],
          ISNULL((SELECT [description] FROM [syscode] WHERE [code] = clm.[altaRisk] AND [codeType] = 'ClaimAltaRisk'),'') AS [description],
          (SELECT [name] FROM [sysuser] WHERE [uid] = clm.[assignedTo]) AS [assignedTo],
          ISNULL(contact.[counsel],'') AS [titleCounsel],
          ISNULL(prop.[addr1],'') AS [address],
          ISNULL(prop.[city],'') AS [city],
          ISNULL((SELECT [description] FROM [state] WHERE [stateID] = prop.[stateID]),'') AS [state],
          ISNULL(prop.[zipcode],'') AS [zipcode],
          clm.[summary],
          attr.[repurchase],
          attr.[reconsideration],
          attr.[litigation]
  FROM    [claim] clm LEFT OUTER JOIN
          [claimproperty] prop
  ON      prop.[claimID] = clm.[claimID]
  AND     prop.[seq] = 1 LEFT OUTER JOIN 
          ( 
          SELECT  cc.[claimID], 
                  CONVERT(VARCHAR(1000), SUBSTRING((SELECT ', ' + cc2.[fullName] AS [text()] FROM [claimcontact] cc2 WHERE cc2.[claimID] = cc.[claimID] AND (cc2.[role] = '8' OR cc2.[role] = '9') ORDER BY cc2.[contactID] FOR xml path('')), 3, 1000)) AS [counsel]
          FROM    [claimcontact] cc 
          GROUP BY cc.[claimID]
          ) contact 
  ON      clm.[claimID] = contact.[claimID] INNER JOIN
          (
          SELECT  [claimID],
                  ISNULL([CA1],'') AS [loanNumber],
                  ISNULL([CA2],'') AS [serviceName],
                  ISNULL([CA3],'') AS [acceptedAction],
                  ISNULL([CA4],'') AS [denialReason],
                  ISNULL([CA5],'') AS [titleCounsel],
                  ISNULL([CA6],'Unknown') AS [reconsideration],
                  ISNULL([CA7],'Unknown') AS [repurchase],
                  CASE WHEN [CA9] IS NULL
                    THEN 'No'
                    ELSE 'Yes'
                  END AS [litigation],
                  ISNULL([CA11],'') AS [statusToLender]
          FROM    (
                  SELECT  [claimID],
                          [attrCode],
                          [attrValue]
                  FROM    [claimattr]
                  WHERE   [claimID] IN (SELECT [claimID] FROM #claimTable)
                  ) a
                  PIVOT
                  (
                  MAX([attrValue])
                  FOR [attrCode] IN ([CA1], [CA2], [CA3], [CA4], [CA5], [CA6], [CA7], [CA9], [CA11])
                  ) PIV
          ) attr
  ON      clm.[claimID] = attr.[claimID]
  WHERE   clm.[dateCreated] BETWEEN @firstDay AND @lastDay
  OR      clm.[dateClosed] BETWEEN @firstDay AND @lastDay
END
