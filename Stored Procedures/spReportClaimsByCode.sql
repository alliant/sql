USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spReportClaimByCode]    Script Date: 4/19/2017 7:13:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportClaimsByCode]
	-- Add the parameters for the stored procedure here
  @code VARCHAR(50) = '36',
  @codeType VARCHAR(50) = 'ClaimDescription',
  @stateList VARCHAR(100) = 'AZ,CO,FL,MO,TX',
  @startDate DATETIME = NULL,
  @endDate DATETIME = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;
  DECLARE @stateID VARCHAR(2),
          @quotedStateList VARCHAR(100) = '',
          @stringStateList VARCHAR(100) = '',
          @groupStateList VARCHAR(200) = '',
          @counter INTEGER = 0,
          @Pos INTEGER,
          @Delimiter NCHAR(1) = ',',
          @claimSQL NVARCHAR(MAX)

  -- Get the start date
  IF (@startDate IS NULL)
    SET @startDate = '2005-01-01'

  IF (@endDate IS NULL)
    SET @endDate = DATEADD(ms, -1, DATEADD(dd, 1, DATEDIFF(dd, 0, GetDate())))
  SET @endDate = DATEADD(s,-1,DATEADD(d,1,@endDate))

  -- Quote the states
  SET @stateList = @stateList + @Delimiter
  SET @Pos = CHARINDEX(@Delimiter, @stateList)
  WHILE (@Pos <> 0)
	BEGIN
    SET @counter = @counter + 1
    SET @stateID = SUBSTRING(@stateList,1,@Pos-1)
    SET @quotedStateList = @quotedStateList + QUOTENAME(@stateID) + ','
    SET @stringStateList = @stringStateList + '''' + @stateID + ''','
    SET @groupStateList = @groupStateList + 'SUM(' + QUOTENAME(@stateID) + ') AS [State' + CONVERT(VARCHAR,@counter) + '],'

		SET @stateList = SUBSTRING(@stateList,@Pos+1,LEN(@stateList))
		SET @Pos = CHARINDEX(@Delimiter, @stateList)
	END
  SET @quotedStateList = SUBSTRING(@quotedStateList,1,LEN(@quotedStateList)-1)
  SET @stringStateList = SUBSTRING(@stringStateList,1,LEN(@stringStateList)-1)
  SET @groupStateList = SUBSTRING(@groupStateList,1,LEN(@groupStateList)-1)

  SET @claimSQL = '
  SELECT  [agentName],
          [claimID],
          [claimType],
          [claimStat],
          [agentError],
          ''ClaimByCode'' AS [rowType],
          ' + @groupStateList + '
  FROM    (
          SELECT  agent.[agentID],
                  agent.[name] AS ''agentName'',
                  claim.[claimID],
                  claim.[type] AS ''claimType'',
                  claim.[agentError] AS ''agentError'',
                  CASE
                    WHEN claim.[stat] = ''C'' AND claim.[dateClosed] > ''' + CONVERT(VARCHAR,@endDate,120) + ''' THEN ''O''
                    ELSE claim.[stat]
                  END AS ''claimStat'',
                  claim.[stateID],
                  aptrx.[transAmount]
          FROM    [dbo].[claim] claim INNER JOIN
                  [dbo].[agent] agent
          ON      claim.[agentID] = agent.[agentID] INNER JOIN
                  [dbo].[aptrx] aptrx
          ON      claim.[claimID] = CONVERT(INTEGER,aptrx.[refID]) INNER JOIN
                  [dbo].[claimcode] cc
          ON      claim.[claimID] = cc.[claimID]
          WHERE   claim.[stateID] IN (' + @stringStateList + ')
          AND     cc.[code] = ''' + @code + '''
          AND     cc.[codeType] = ''' + @codeType + '''
          AND     claim.[dateCreated] BETWEEN ''' + CONVERT(VARCHAR,@startDate,120) + ''' AND ''' + CONVERT(VARCHAR,@endDate,120) + '''
          AND    (claim.[dateClosed] IS NULL OR claim.[dateClosed] < ''' + CONVERT(VARCHAR,@endDate,120) + ''')
          ) src
          PIVOT
          (
          SUM([transAmount])
          FOR [stateID] IN (' + @quotedStateList + ')
          ) p
  GROUP BY [agentName],[claimID],[claimType],[claimStat],[agentError]
  UNION ALL
  SELECT  '''',
          '''',
          '''',
          '''',
          '''',
          ''ClaimsTotal'',
          ' + @groupStateList + '
  FROM    (
          SELECT  agent.[agentID],
                  agent.[name] AS ''agentName'',
                  claim.[stateID],
                  aptrx.[transAmount]
          FROM    [dbo].[claim] claim INNER JOIN
                  [dbo].[agent] agent
          ON      claim.[agentID] = agent.[agentID] INNER JOIN
                  [dbo].[aptrx] aptrx
          ON      claim.[claimID] = CONVERT(INTEGER,aptrx.[refID])
          WHERE   claim.[stateID] IN (' + @stringStateList + ')
          AND     claim.[dateCreated] BETWEEN ''' + CONVERT(VARCHAR,@startDate,120) + ''' AND ''' + CONVERT(VARCHAR,@endDate,120) + '''
          AND    (claim.[dateClosed] IS NULL OR claim.[dateClosed] < ''' + CONVERT(VARCHAR,@endDate,120) + ''')
          ) src
          PIVOT
          (
          SUM([transAmount])
          FOR [stateID] IN (' + @quotedStateList + ')
          ) p'
          
  EXECUTE(@claimSQL)
  PRINT @claimSQL
END
