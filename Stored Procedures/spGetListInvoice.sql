USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetClaim]    Script Date: 9/23/2016 9:35:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetListInvoice] 
	-- Add the parameters for the stored procedure here
	@fieldList VARCHAR(MAX) = '',
  @filterList VARCHAR(MAX) = '',
  @asOfDate DATETIME = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  DECLARE @sql NVARCHAR(MAX)

  IF (@fieldList <> '')
    SET @fieldList = ',' + @fieldList

  IF (@asOfDate IS NULL)
    SET @asOfDate = DATEADD(d,-1,DATEADD(m, DATEDIFF(m,0,GETDATE())+1,0))
  ELSE
    SET @asOfDate = DATEADD(s,-1,DATEADD(d,1,@asOfDate))

  CREATE TABLE #invoiceTable
  (
  apinvID INTEGER,
  refID VARCHAR(100),
  refCategory VARCHAR(100),
  asOfDate DATETIME,
  pendingAmount DECIMAL(18,2),
  approvedAmount DECIMAL(18,2),
  completedAmount DECIMAL(18,2),
  stat VARCHAR(20)
  )

  -- get the claim balances
  INSERT INTO #invoiceTable
  SELECT * FROM GetInvoiceDetails(@asOfDate)

  CREATE TABLE #balanceTable
  (
  claimID INT,
  refCategory VARCHAR(10),
  asOfDate DATETIME,
  pendingInvoiceAmount DECIMAL(18,2),
  approvedInvoiceAmount  DECIMAL(18,2),
  completedInvoiceAmount  DECIMAL(18,2),
  pendingReserveAmount  DECIMAL(18,2),
  approvedReserveAmount DECIMAL(18,2),
  reserveBalance DECIMAL(18,2)
  )

  -- get the claim balances
  INSERT INTO #balanceTable
  EXEC [dbo].[spReportClaimBalances] @asOfDate = @asOfDate

  SET @sql = '
  SELECT  DISTINCT 
          apinv.[apinvID] AS keyfield' + @fieldList + '
  FROM    [claim] INNER JOIN
          (
          SELECT  lae.[claimID],
                  lae.[laePendingInvoice],
                  lae.[laeApprovedInvoice],
                  lae.[laeCompletedInvoice],
                  lae.[laePendingReserve],
                  lae.[laeApprovedReserve],
                  loss.[lossPendingInvoice],
                  loss.[lossApprovedInvoice],
                  loss.[lossCompletedInvoice],
                  loss.[lossPendingReserve],
                  loss.[lossApprovedReserve]
          FROM    (
                  SELECT  [claimID],
                          [pendingInvoiceAmount] AS ''laePendingInvoice'',
                          [approvedInvoiceAmount] AS ''laeApprovedInvoice'',
                          [completedInvoiceAmount] AS ''laeCompletedInvoice'',
                          [pendingReserveAmount] AS ''laePendingReserve'',
                          [approvedReserveAmount] AS ''laeApprovedReserve''
                  FROM    #balanceTable
                  WHERE   [refCategory] = ''E''
                  ) lae INNER JOIN
                  (
                  SELECT  [claimID],
                          [pendingInvoiceAmount] AS ''lossPendingInvoice'',
                          [approvedInvoiceAmount] AS ''lossApprovedInvoice'',
                          [completedInvoiceAmount] AS ''lossCompletedInvoice'',
                          [pendingReserveAmount] AS ''lossPendingReserve'',
                          [approvedReserveAmount] AS ''lossApprovedReserve''
                  FROM    #balanceTable
                  WHERE   [refCategory] = ''L''
                  ) loss
          ON      lae.[claimID] = loss.[claimID]
          ) balance
  ON      claim.[claimID] = balance.[claimID] LEFT OUTER JOIN
          [apinv] 
  ON      claim.[claimID] = convert(integer,apinv.[refID]) INNER JOIN
          (
          SELECT  COALESCE(lae.[apinvID],loss.[apinvID]) AS ''apinvID'',
                  ISNULL(lae.[laePending],0) AS ''laePending'',
                  ISNULL(lae.[laeApproved],0) AS ''laeApproved'',
                  ISNULL(lae.[laeCompleted],0) AS ''laeCompleted'',
                  ISNULL(loss.[lossPending],0) AS ''lossPending'',
                  ISNULL(loss.[lossApproved],0) AS ''lossApproved'',
                  ISNULL(loss.[lossCompleted],0) AS ''lossCompleted''
          FROM    (
                  SELECT  [apinvID],
                          [pendingAmount] AS ''laePending'',
                          [approvedAmount] AS ''laeApproved'',
                          [completedAmount] AS ''laeCompleted''
                  FROM    #invoiceTable
                  WHERE   [refCategory] = ''E''
                  ) lae FULL OUTER JOIN
                  (
                  SELECT  [apinvID],
                          [pendingAmount] AS ''lossPending'',
                          [approvedAmount] AS ''lossApproved'',
                          [completedAmount] AS ''lossCompleted''
                  FROM    #invoiceTable
                  WHERE   [refCategory] = ''L''
                  ) loss
          ON      lae.[apinvID] = loss.[apinvID]
          ) invoice
  ON      apinv.[apinvID] = invoice.[apinvID] INNER JOIN
          (
          SELECT  app.[apinvID],
                  CONVERT(VARCHAR(1000),SUBSTRING(
                  (
                      SELECT  '', '' + usr.[name] AS [text()]
                      FROM    [sysuser] usr INNER JOIN
                              [apinva] app2
                      ON      app2.[uid] = usr.[uid]
                      WHERE   app.[apinvID] = app2.[apinvID]
                      AND     app2.[stat] = ''P''
                      ORDER BY app.[apinvID]
                      FOR XML PATH ('''')
                  ), 3, 1000)) AS [pending],
                  CONVERT(VARCHAR(1000),SUBSTRING(
                  (
                      SELECT  '', '' + usr.[name] AS [text()]
                      FROM    [sysuser] usr INNER JOIN
                              [apinva] app2
                      ON      app2.[uid] = usr.[uid]
                      WHERE   app.[apinvID] = app2.[apinvID]
                      AND     app2.[stat] = ''A''
                      ORDER BY app.[apinvID]
                      FOR XML PATH ('''')
                  ), 3, 1000)) AS [approved]
          FROM    [apinva] app
          GROUP BY app.[apinvID]
          ) apinva
  ON      apinv.[apinvID] = apinva.[apinvID] LEFT OUTER JOIN
          aptrx
  ON      apinv.[apinvID] = aptrx.[apinvID] LEFT OUTER JOIN
          claimadjreq
  ON      claim.[claimID] = claimadjreq.[claimID] LEFT OUTER JOIN
          claimadjtrx
  ON      claim.[claimID] = claimadjtrx.[claimID]'

  IF (@filterList <> '')
  BEGIN
    SET @sql = @sql + ' ' + @filterList
    print @filterList
  END

  EXECUTE sp_executesql @sql
	
END
