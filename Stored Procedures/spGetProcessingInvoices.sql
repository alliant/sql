USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetProcessingInvoices]    Script Date: 4/14/2021 6:08:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetProcessingInvoices]
	-- Add the parameters for the stored procedure here	
	@agentID         VARCHAR(MAX) = '',
	@startDate       DATE         = NULL,
	@endDate         DATE         = NULL,
	@asOfDate        DATE         = NULL
AS
BEGIN


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for procedure here
	CREATE TABLE #agentTable ([agentID] VARCHAR(30))
    INSERT INTO #agentTable ([agentID])
    SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)

	CREATE TABLE #invoicestatement (  recordType  VARCHAR(10),
	                            batchid           INTEGER,
               	                fileNumber        VARCHAR(500),
				                fileID            VARCHAR(500),
								policyid          INTEGER,
     				            liabilityamount   [decimal](18, 2) NULL,
					            grosspremium      [decimal](18, 2) NULL,
					            retentionpremium  [decimal](18, 2) NULL,
					            netdelta          [decimal](18, 2) NULL,
							    formDesc          VARCHAR(500),
								formType          VARCHAR(50),
                                formID            VARCHAR(50),
                                statCode          VARCHAR(50),
    							batchreceivedate  datetime NULL,	
                                duedate           datetime NULL,								
								paymentApplied    [decimal](18, 2) NULL,
								PmtID			  INTEGER,
								depositID         INTEGER,
								receivedDate      DATETIME NULL,
								checkNum          VARCHAR(50),
								CheckAmt          [decimal](18, 2) NULL
     	 					   )
							   
    INSERT INTO #invoicestatement ( [batchid],  [batchreceivedate], [fileNumber], [fileID], [policyid],[liabilityamount], [grosspremium], [retentionpremium],[netdelta], [formType], [formID], [formDesc], [statCode], [recordType])

    SELECT  b.batchid, b.receivedDate,  bf.filenumber , bf.fileid , bf.policyid ,bf.liabilityamount , bf.grossdelta , bf.retentiondelta ,bf.netdelta , bf.formType , bf.formID , sf.description,  bf.statCode, 'F'
	  FROM artran ar INNER JOIN 
          [dbo].[agent] a ON 
           a.agentID = ar.entityID       INNER JOIN  
		  batchform bf ON 
		      ar.sourceID = bf.batchid 
		   AND ar.fileID = bf.fileID     INNER JOIN  
		  batch b ON 
		   bf.batchID = b.batchID        INNER JOIN
          stateform sf ON
		      sf.stateID = b.stateID 
		  AND sf.formID  = bf.formID
	  WHERE ar.entity = 'A' AND
	        ar.[entityID] IN (SELECT [agentID] FROM #agentTable) AND
			ar.type in ('I', 'R')								 AND
			ar.transType = 'F'									 AND
			ar.sourceType = 'B'									 AND
	        ar.tranDate >= CAST(@startDate AS DATE)				 AND 
			ar.tranDate <= CAST(@endDate AS DATE)				 AND
			  1 = (CASE WHEN bf.formtype in ('C','T') 
			       THEN 
			         CASE WHEN (','+ar.formIDList+',' LIKE '%,'+bf.formid+',%') THEN 1 END  
			       WHEN bf.formtype IN ('P','E') 
			        THEN 
			         CASE WHEN (ar.reference = CAST(bf.policyID AS VARCHAR))	THEN 1 END
			      END)
			 
	INSERT INTO #invoicestatement ( [netdelta], [fileNumber] , [fileID], [paymentApplied], [receivedDate], [checkNum] , 
									[CheckAmt], [recordType], [PmtID] , [depositID])

	SELECT  ar.tranAmt, ar.filenumber, ar.fileID, ar.tranAmt, ar.tranDate, ar.reference, 
									ap.checkAmt, 'A', ap.arpmtID, ap.depositID
	FROM artran ar  INNER JOIN 
	 artran ar1 ON 
	ar1.artranid  = ar.sourceid 
	AND ar1.type = 'P'
	AND ar1.seq = 0 INNER JOIN
	 arpmt ap ON
	ap.arPmtid = ar1.tranid	
	WHERE ar.entity = 'A'
	 AND ar.[entityID] IN (SELECT [agentID] FROM #agentTable)
	 AND ar.type = 'A'
	 AND ar.transType = 'F'
	 AND ar.fileID IN (SELECT DISTINCT fileID FROM #invoicestatement) 
	 AND ar.tranDate >= CAST(@startDate AS DATE)  
	 AND ar.tranDate <= CAST(@asOfDate AS DATE)
 
	SELECT * FROM #invoicestatement  ORDER BY fileID

END
			
			