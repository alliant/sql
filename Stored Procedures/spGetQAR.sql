USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetQAR]    Script Date: 11/29/2018 8:12:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetQAR]
	-- Add the parameters for the stored procedure here
	@auditID INTEGER = 0,
  @year INTEGER = 0,
  @stateID VARCHAR(3) = 'ALL',
  @agentID VARCHAR(MAX) = 'ALL',
  @auditor VARCHAR(50) = 'ALL',
  @status VARCHAR(10) = 'ALL'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  
  CREATE TABLE #agentTable ([agentID] VARCHAR(30))
  INSERT INTO #agentTable ([agentID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)

  -- Add all the qars
  CREATE TABLE #auditTable ([auditID] INTEGER, [year] INTEGER, [auditor] VARCHAR(50), [stateID] VARCHAR(2), [agentID] VARCHAR(50), [agentName] VARCHAR(200), [status] VARCHAR(10))
  IF (@auditID > 0)
  BEGIN
    INSERT INTO #auditTable ([auditID], [year], [auditor], [stateID], [agentID], [status])
    SELECT  q.[qarID],
            q.[auditYear],
            q.[uid],
            a.[stateID],
            a.[agentID],
            q.[stat]
    FROM    [dbo].[qar] q INNER JOIN
            [dbo].[agent] a
    ON      q.[agentID] = a.[agentID]
    WHERE   q.[qarID] = @auditID
  END
  ELSE
  BEGIN
    INSERT INTO #auditTable ([auditID], [year], [auditor], [stateID], [agentID], [status])
    SELECT  q.[qarID],
            q.[auditYear],
            q.[uid],
            a.[stateID],
            a.[agentID],
            q.[stat]
    FROM    [dbo].[qar] q INNER JOIN
            [dbo].[agent] a
    ON      q.[agentID] = a.[agentID]

    -- Delete if the year isn't correct
    IF (@year > 0)
      DELETE FROM #auditTable WHERE [year] <> @year

    -- Delete if the auditor is not correct
    IF (@auditor <> 'ALL')
      DELETE FROM #auditTable WHERE [auditor] <> @auditor

    -- Delete if the state is not correct
    IF (@stateID <> 'ALL')
      DELETE FROM #auditTable WHERE [stateID] <> @stateID

    -- Delete if the state is not correct
    IF (@agentID <> 'ALL')
      DELETE FROM #auditTable WHERE [agentID] NOT IN (SELECT [agentID] FROM #agentTable)

    -- Delete if the status is not correct
    IF (@status <> 'ALL')
      DELETE FROM #auditTable WHERE [status] <> @status
  END

  SELECT  q.[qarID],
          q.[version],
          q.[auditYear],
          q.[agentID],
          a.[name],
          a.[addr1] + CASE WHEN a.[addr2] = '' THEN '' ELSE ' ' END + a.[addr2] AS 'addr',
          a.[city],
          a.[state],
          a.[zip],
          q.[auditStartDate],
          q.[auditFinishDate],
          q.[schedStartDate],
          q.[schedFinishDate],
          q.[onsiteStartDate],
          q.[onsiteFinishDate],
          q.[auditor],
          q.[uid],
          q.[contactName],
          q.[contactPhone],
          q.[contactFax],
          q.[contactEmail],
          q.[contactJobTitle] AS 'contactPosition',
          q.[parentCompanyName] AS 'parentName',
          q.[parentCompanyAddress] AS 'parentAddr',
          q.[parentCompanyCity] AS 'parentCity',
          q.[parentCompanyState] AS 'parentState',
          q.[parentCompanyZip] AS 'parentZip',
          q.[auditType],
          q.[errType],
          q.[stat],
          q.[comments],
          a.[stateID],
          q.[mainOffice],
          q.[offices] AS 'numOffices',
          q.[employees] AS 'numEmployees',
          q.[underwriters] AS 'numUnderwriters',
          q.[ranking],
          q.[services],
          q.[draftReportDate],
          (CASE WHEN (q.[score] != NULL or q.[score] != 0) THEN q.[score] ELSE 0 END) as 'score',
	  (CASE WHEN (q.[grade] != NULL or q.[grade] != 0) THEN q.[grade] ELSE 0 END) as 'grade',
          n.[dateCreated] AS 'submittedAt',
          (SELECT [name] FROM [dbo].[sysuser] WHERE [uid] = CASE WHEN n.[uid] = '' THEN q.[uid] ELSE n.[uid] END) AS 'submittedBy'
  FROM    [dbo].[qar] q INNER JOIN
          #auditTable at
  ON      q.[qarID] = at.[auditID] INNER JOIN
          [dbo].[agent] a
  ON      q.[agentID] = a.[agentID] LEFT OUTER JOIN
          [dbo].[qarnote] n
  ON      q.[qarID] = n.[qarID]
  AND     n.[seq] = 1

  DROP TABLE #agentTable
  DROP TABLE #auditTable
END
