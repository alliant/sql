USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetPlatforms]    Script Date: 8/21/2019 11:04:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetPlatforms]
	-- Add the parameters for the stored procedure here
	@platformID INTEGER = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  SELECT  [platformID],
          [commonName],
          [company],
          [companyPhone],
          [vendorID],
          [notes]
  FROM    [dbo].[platform]
  WHERE   [platformID] = CASE WHEN @platformID = 0 THEN [platformID] ELSE @platformID END
END
