USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spReportQARAuditors]    Script Date: 11/13/2017 8:08:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportQARAuditors]
	-- Add the parameters for the stored procedure here
  @auditor VARCHAR(50) = 'ALL'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  -- Insert statements for procedure here
	SELECT  q.[qarID],
          q.[auditYear],
          q.[auditType],
          q.[stat],
          q.[schedStartDate],
          q.[auditFinishDate],
          a.[agentID],
          a.[name] AS 'agentName',
          a.[city] AS 'agentCity',
          a.[state] AS 'agentState',
          q.[stateID],
          (CASE WHEN (q.[score] != NULL or q.[score] != 0) THEN q.[score] ELSE 0 END) as 'auditscore',
	  (CASE WHEN (q.[grade] != NULL or q.[grade] != 0) THEN q.[grade] ELSE 0 END) as 'grade',
          (SELECT [name] FROM [dbo].[sysuser] WHERE [uid] = q.[uid]) AS 'primaryAuditor',
          CONVERT(VARCHAR(MAX),ISNULL(qa.[secondaryAuditor],'')) AS 'secondaryAuditor'
  FROM    [dbo].[qar] q INNER JOIN
          [dbo].[agent] a 
  ON      q.[agentID] = a.[agentID] LEFT OUTER JOIN
          (
          SELECT  q.[qarID],
                  SUBSTRING(
                  (
                    SELECT  ', ' + u.[name] AS [text()]
                    FROM    [dbo].[qarauditor] qa INNER JOIN
                            [dbo].[sysuser] u
                    ON      qa.[uid] = u.[uid]
                    WHERE   q.[qarID] = qa.[qarID]
                    ORDER BY qa.[uid]
                    FOR XML PATH('')
                  ), 3, 1000) AS 'secondaryAuditor',
                  SUBSTRING(
                  (
                    SELECT  ', ' + u.[uid] AS [text()]
                    FROM    [dbo].[qarauditor] qa INNER JOIN
                            [dbo].[sysuser] u
                    ON      qa.[uid] = u.[uid]
                    WHERE   q.[qarID] = qa.[qarID]
                    ORDER BY qa.[uid]
                    FOR XML PATH('')
                  ), 3, 1000) AS 'secondaryUid'
          FROM    [dbo].[qar] q
          GROUP BY q.[qarID]
          ) qa
  ON      q.[qarID] = qa.[qarID]
  WHERE  (q.[uid] = CASE WHEN @auditor = 'ALL' THEN q.[uid] ELSE @auditor END OR qa.[secondaryUid] LIKE '%' + CASE WHEN @auditor = 'ALL' THEN '' ELSE @auditor END + '%')
END
