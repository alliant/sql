USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spReportAgentAccounting]    Script Date: 9/14/2017 11:38:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportAgentAccounting]
	-- Add the parameters for the stored procedure here
  @agentID VARCHAR(MAX) = '',
  @startDate DATETIME = NULL,
  @endDate DATETIME = NULL,
  @UID varchar(100)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SET @agentID = [dbo].[StandardizeAgentID](@agentID)

  -- Decide the dates
  IF (@startDate IS NULL)
    SET @startDate = '2005-01-01'

  IF (@endDate IS NULL)
    SET @endDate = DATEADD(d,-1,DATEADD(m, DATEDIFF(m,0,GETDATE())+1,0))
  ELSE
    -- The end date should be the last second of the day
    SET @endDate = DATEADD(s,-1,DATEADD(d,1,@endDate))
   
  SELECT  RTRIM(CM.[CUSTNMBR]) AS [agentID],
          RTRIM(T.[DOCNUMBR]) AS [paymentNumber],
          T.[DOCDATE] AS [paymentDate],
          RTRIM(T.[CHEKNMBR]) AS [checkNumber],
          RTRIM(T.[BACHNUMB]) AS [batchID],
          RTRIM(A.[APTODCNM]) AS [invoiceNumber],
          A.[APTODCDT] AS [invoiceDate],
          RTRIM(D.[CSPORNBR]) AS [fileNumber],
          T.[ORTRXAMT] AS [amountTotal],
          T.[CURTRXAM] AS [amountUnapplied],
          T.[ORTRXAMT] - T.[CURTRXAM] AS [amountApplied],
          A.[APPTOAMT] AS [invoiceAmount]
  FROM    [ANTIC].[dbo].[RM00101] CM INNER JOIN
          (
          SELECT  [CUSTNMBR],
                  [DOCDATE],
                  [GLPOSTDT],
                  [RMDTYPAL],
                  [CHEKNMBR],
                  [DOCNUMBR],
                  [ORTRXAMT],
                  [CURTRXAM],
                  [BACHNUMB]
          FROM    [ANTIC].[dbo].[RM20101]
          WHERE   [RMDTYPAL] = 9
          AND     [VOIDSTTS] = 0
          AND     [CSHRCTYP] = 0
          AND     [CHEKNMBR] <> ''
          UNION
          SELECT  [CUSTNMBR],
                  [DOCDATE],
                  [GLPOSTDT],
                  [RMDTYPAL],
                  [CHEKNMBR],
                  [DOCNUMBR],
                  [ORTRXAMT],
                  [CURTRXAM],
                  [BACHNUMB]
          FROM    [ANTIC].[dbo].[RM30101]
          WHERE   [RMDTYPAL] = 9
          AND     [VOIDSTTS] = 0
          AND     [CSHRCTYP] = 0
          AND     [CHEKNMBR] <> ''
          ) T
  ON      T.[CUSTNMBR] = CM.[CUSTNMBR] INNER JOIN
          (
          SELECT  tO1.[CUSTNMBR],
                  tO2.[APTODCTY],
                  tO2.[APTODCNM],
                  tO2.[APFRDCTY],
                  tO2.[APFRDCNM],
                  tO2.[APPTOAMT],
                  tO2.[APTODCDT]
          FROM    [ANTIC].[dbo].[RM20201] tO2 INNER JOIN 
                  [ANTIC].[dbo].[RM20101] tO1
          ON      tO2.[APTODCTY] = tO1.[RMDTYPAL]
          AND     tO2.[APTODCNM] = tO1.[DOCNUMBR]
          UNION
          SELECT  tO1.[CUSTNMBR],
                  tO2.[APTODCTY],
                  tO2.[APTODCNM],
                  tO2.[APFRDCTY],
                  tO2.[APFRDCNM],
                  tO2.[APPTOAMT],
                  tO2.[APTODCDT]
          FROM    [ANTIC].[dbo].[RM30201] tO2 INNER JOIN 
                  [ANTIC].[dbo].[RM30101] tO1
          ON      tO2.[APTODCTY] = tO1.[RMDTYPAL]
          AND     tO2.[APTODCNM] = tO1.[DOCNUMBR]
          ) A 
  ON      A.[APFRDCTY] = T.[RMDTYPAL]
  AND     A.[APFRDCNM] = T.[DOCNUMBR] INNER JOIN
          (
          SELECT  [CSPORNBR],
                  [RMDTYPAL],
                  [DOCNUMBR]
          FROM    [ANTIC].[dbo].[RM20101]
          UNION
          SELECT  [CSPORNBR],
                  [RMDTYPAL],
                  [DOCNUMBR]
          FROM    [ANTIC].[dbo].[RM30101]
          ) D 
  ON      A.[APTODCTY] = D.[RMDTYPAL]
  AND     A.[APTODCNM] = D.[DOCNUMBR]
  WHERE   CM.[CUSTNMBR] IN (SELECT [agentID] FROM [dbo].[agent] WHERE ([agentID] = (CASE  WHEN @agentID != 'ALL' THEN @agentID ELSE [agentID] END)) AND (dbo.CanAccessAgent(@UID , [agentID]) = 1))
  AND     T.[DOCDATE] BETWEEN @startDate AND @endDate
  ORDER BY [DOCDATE],[CHEKNMBR]

END

