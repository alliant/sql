USE [COMPASS_DVLP]
GO
/****** Object:  StoredProcedure [dbo].[spGetActionDestinations]    Script Date: 1/14/2021 9:05:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetActionDestinations]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  [action],
          ISNULL([htmlEmail],'') AS [emailHTML],
          ISNULL([subject],'') AS [emailSubject],
          [name] AS [displayName]
  FROM    [sysaction]
  WHERE   [isDestination] = 1
END
