USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spAgentReviewClaim]    Script Date: 4/5/2018 4:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spAgentReviewClaim]
	-- Add the parameters for the stored procedure here
    @agentID VARCHAR(MAX) = 'ALL',
	@UID varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SET @agentID = [dbo].[StandardizeAgentID](@agentID)
  
  DECLARE @startDate DATETIME = NULL,
          @endDate DATETIME = NULL

  DECLARE @insertClaimBalanceDelta [dbo].[ClaimBalanceDelta]
  SELECT * INTO #insertClaimBalanceDelta FROM @insertClaimBalanceDelta

  SELECT @endDate = MAX([endDate]) FROM [dbo].[period] WHERE [active] = 0
  SET @startDate = DATEADD(m, DATEDIFF(m, 0, DATEADD(yy,-3,@endDate)), 0)

  -- Get the claim balances delta
  EXEC [dbo].[spReportClaimBalancesDelta] @startDate = @startDate, @endDate = @endDate

  SELECT  COALESCE(b.[agentID],c.[agentID]) AS [agentID],
          c.[claimID],
          c.[description],
          c.[stat],
          c.[agentError],
          ISNULL(c.[laeCompletePayments],0) AS [laePaid],
          ISNULL(c.[lossCompletePayments],0) AS [lossPaid],
          ISNULL(c.[laeReserveBalance],0) AS [laeReserve],
          ISNULL(c.[lossReserveBalance],0) AS [lossReserve],
          ISNULL(c.[recoveries],0) AS [recoveries],
          ISNULL(b.[netPremium],0) AS [netPremium]
  FROM    (
          SELECT  [agentID],
                  CONVERT(DECIMAL(18,2),ISNULL(SUM(batch.[netPremiumDelta]),0)) AS [netPremium]
          FROM    [dbo].[batch] INNER JOIN
                  [dbo].[period]
          ON      batch.[periodID] = period.[periodID]
          WHERE   period.[startDate] >= @startDate
          AND     period.[endDate] <= @endDate
          AND     batch.[stat] = 'C'
          AND     (batch.[agentID] = (CASE  WHEN @agentID != 'ALL' THEN @agentID ELSE batch.[agentID] END)) 
		  AND     (dbo.CanAccessAgent(@UID ,batch.[agentID]) = 1)
 
         GROUP BY [agentID]
          ) b INNER JOIN
          (
          SELECT  agent.[agentID],
                  claim.[claimID],
                  ISNULL(cc.[claimDescription],'') AS [description],
                  claim.[agentError],
                  claim.[stat],
                  SUM(delta.[laeCompletedInvoiceAmount]) AS [laeCompletePayments],
                  SUM(delta.[lossCompletedInvoiceAmount]) AS [lossCompletePayments],
                  SUM(delta.[laeReserveBalance]) AS [laeReserveBalance],
                  SUM(delta.[lossReserveBalance]) AS [lossReserveBalance],
                  SUM(delta.[paidRecoveries]) AS [recoveries]
          FROM    [dbo].[agent] agent INNER JOIN
                  [dbo].[claim] claim
          ON      agent.[agentID] = claim.[agentID] LEFT OUTER JOIN
                  #insertClaimBalanceDelta delta
          ON      claim.[claimID] = delta.[claimID] LEFT OUTER JOIN 
                  ( 
                  SELECT  cc2.[claimID], 
                          CONVERT(VARCHAR(1000), SUBSTRING((SELECT ', ' + sc.[description] + ' (' + sc.[code] + ')' AS [text()] FROM [syscode] sc INNER JOIN [claimcode] cc ON cc.[code] = sc.[code] AND cc.[codeType] = sc.[codeType] WHERE cc.[claimID] = cc2.[claimID] AND sc.[codeType] = 'claimDescription' ORDER BY cc.[claimID] FOR xml path('') ), 3, 1000)) AS [claimDescription]
                  FROM    [claimcode] cc2
                  GROUP BY cc2.[claimID] 
                  ) cc 
          ON      claim.[claimID] = cc.[claimID]
          GROUP BY agent.[agentID], claim.[claimID], cc.[claimDescription], claim.[agentError], claim.[stat]
          ) c
  ON      b.[agentID] = c.[agentID]
  ORDER BY [agentID]
  
  DROP TABLE #insertClaimBalanceDelta
END
