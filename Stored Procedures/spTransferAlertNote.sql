USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spTransferAlertNote]    Script Date: 7/6/2018 3:41:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spTransferAlertNote] 
	-- Add the parameters for the stored procedure here
  @prevAlertID INTEGER = 0,
  @alertID INTEGER = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  UPDATE  [dbo].[alertnote]
  SET     [alertID] = @alertID
  WHERE   [alertID] = @prevAlertID
END
