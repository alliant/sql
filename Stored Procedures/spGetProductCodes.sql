USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetProductCodes]    Script Date: 8/21/2019 11:01:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetProductCodes]
	-- Add the parameters for the stored procedure here
	@productCodeID INTEGER = 0,
  @type VARCHAR(50) = 'ALL'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  IF (@type = '' OR @type IS NULL)
   SET @type = 'ALL'

  SELECT  [productCodeID],
          [productName],
          [stateID],
          [type],
          [description],
          [shortcut],
          [isActive],
          COALESCE([placeholders],'') AS [placeholders]
  FROM    [dbo].[productcode]
  WHERE   [productCodeID] = CASE WHEN @productCodeID = 0 THEN [productCodeID] ELSE @productCodeID END
  AND     [type] = CASE WHEN @type = 'ALL' THEN [type] ELSE @type END
END
