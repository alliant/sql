-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Rahul Sharma>
-- Create date: <02-19-2018>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE spGetQualifications
	-- Add the parameters for the stored procedure here
  @pcStateID VARCHAR(3)      = 'ALL',
  @pcEntity VARCHAR(50)      = 'ALL',
  @pcEntityID VARCHAR(50)    = 'ALL',
  @piQualificationID INTEGER = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
  SELECT  *
  FROM    [dbo].[qualification] q
  where q.[stateID]         = (CASE WHEN @pcStateID          = 'ALL' THEN q.[stateID]          ELSE @pcStateID          END) 
    and q.[entity]          = (CASE WHEN @pcEntity           = 'ALL' THEN q.[entity]           ELSE @pcEntity           END) 
    and q.[entityId]        = (CASE WHEN @pcEntityID         = 'ALL' THEN q.[entityId]         ELSE @pcEntityID         END)
    and q.[qualificationID] = (CASE WHEN @piQualificationID  = 0     THEN q.[qualificationID]  ELSE @piQualificationID  END) 	
END
GO
