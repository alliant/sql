USE [COMPASS_ALFA]
GO
/****** Object:  StoredProcedure [dbo].[spGetAgentMaster]    Script Date: 6/23/2022 8:28:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetAgentMaster] 
	-- Add the parameters for the stored procedure here
	@piAgentID VARCHAR(MAX) = 'ALL'

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	CREATE TABLE #agentMaster(agentID		      VARCHAR(100),
							  name			      VARCHAR(100),						
							  company		      VARCHAR(100),
							  organization        VARCHAR(1000),
							  State			      VARCHAR(100),
							  region		      VARCHAR(100),
							  primaryManager	  VARCHAR(100), 
							  status			  VARCHAR(100),
							  libilityLimit		  VARCHAR(100),
							  agentCompliantOrNot VARCHAR(100),
							  domain              VARCHAR(100),
							  startDate           DATE,
							  latestAuditScore    INTEGER,
							  auditDate           DATE,
							  policyOutStading    INTEGER,
							  )

INSERT INTO #agentMaster (agentID,name,company,organization,State,region,primaryManager,status,libilityLimit ,agentCompliantOrNot,
                          domain,startDate,latestAuditScore,auditDate,policyOutStading)
						  
SELECT ag.agentID,
       ag.name,
	   (CASE  WHEN  ag.corporationID = '' THEN (ag.name + '-' + ag.agentID) ELSE ag.corporationID END) ,
	   (SELECT (organization.name + '-' + organization.orgID) FROM organization WHERE organization.orgid = (SELECT orgrole.orgID FROM orgrole WHERE orgrole.source = 'Agent' AND orgrole.sourceID = ag.agentID)),
	   ag.state,
	   ag.region, 
	   am.[uid] AS manager, 
	   ag.stat,
	   ag.liabilityLimit ,
	   (SELECT TOP 1 comstat FROM comlog WHERE comlog.objRef = 'Agent' AND comlog.objID = ag.agentID AND exists(SELECT orgroleID FROM orgrole WHERE source = 'Agent' AND sourceID = ag.agentID) ORDER BY logDate DESC),
	   ag.domain, 
	   (CASE  WHEN  ag.activeDate IS NULL THEN ( case when ag.prospectDate IS NULL THEN ag.createDate ELSE ag.prospectDate END)  ELSE ag.activeDate END),
	   (SELECT TOP 1 grade FROM qar WHERE qar.agentID =  ag.agentID  AND qar.stat = 'C' ORDER BY auditFinishDate DESC),
	   (SELECT TOP 1 auditFinishDate FROM qar WHERE qar.agentID = ag.agentID AND qar.stat = 'C' ORDER BY auditFinishdate DESC),
	   (SELECT COUNT(policy.policyID) FROM policy WHERE policy.stat = 'I' AND policy.agentID = ag.agentID) 
	   FROM agent ag 
	   LEFT OUTER JOIN agentmanager am
	   ON ag.agentID = am.agentID
	   WHERE ag.agentID = (CASE  WHEN @piAgentID != 'ALL' THEN @piAgentID ELSE ag.[agentID] END)
	   AND am.isPrimary = 1 AND am.stat = 'A' AND (am.effDate <= GETDATE() AND (am.expDate > GETDATE() OR am.expDate IS NULL ))

UPDATE #agentMaster 
SET #agentMaster.startDate = CASE WHEN #agentMaster.startDate IS NULL THEN '07-01-2005' ELSE #agentMaster.startDate END 

SELECT * FROM #agentMaster

DROP TABLE #agentMaster
END
