USE [COMPASS]
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetBatchForAgent]
	-- Add the parameters for the stored procedure here
	@periodID INTEGER = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  IF @periodID = 0
    SELECT @periodID = [periodID] FROM [dbo].[period] WHERE [periodYear] = YEAR(GETDATE()) AND [periodMonth] = MONTH(GETDATE())

  SELECT  DISTINCT
          a.[stat] AS [agentStat],
          (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'AMD' AND [objAction] = 'Agent' AND [objProperty] = 'Status' AND [objID] = a.[stat]) AS [agentStatDesc],
          a.[name] AS [agentName],
          ISNULL(b.[batchID],0) AS [batchID],
          COALESCE(b.[periodID],@periodID) AS [periodID],
          COALESCE(b.[agentID],a.[agentID]) AS [agentID],
          COALESCE(b.[stateID],a.[stateID]) AS [state],
          COALESCE(b.[stateID],a.[stateID]) AS [stateID],
          (SELECT [description] FROM [dbo].[state] WHERE [stateID] = COALESCE(b.[stateID],a.[stateID])) AS [stateDesc],
          a.[activeDate] AS [agentActiveDate],
          CASE 
            WHEN a.[activeDate] IS NULL OR a.[activeDate] > p.[startDate] THEN 'NA'
            WHEN a.[activeDate] < p.[startDate] AND a.[stat] = 'A' AND b.[batchID] IS NULL THEN 'NR'
            WHEN a.[activeDate] < p.[startDate] AND a.[stat] <> 'A' AND b.[batchID] IS NULL THEN 'N/A'
            ELSE b.[stat]
          END AS [stat],
          (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'OPS' AND [objAction] = 'Batch' AND [objProperty] = 'Status' AND [objID] = 
            CASE 
              WHEN a.[activeDate] IS NULL OR a.[activeDate] > p.[startDate] THEN 'NA'
              WHEN a.[activeDate] < p.[startDate] AND a.[stat] = 'A' AND b.[batchID] IS NULL THEN 'NR'
              WHEN a.[activeDate] < p.[startDate] AND a.[stat] <> 'A' AND b.[batchID] IS NULL THEN 'N/A'
              ELSE b.[stat]
            END
          ) AS [statDesc],
          ISNULL(b.[grossPremiumReported],0) AS [grossPremiumReported],
          ISNULL(b.[grossPremiumDelta],0) AS [grossPremiumDelta],
          ISNULL(b.[netPremiumReported],0) AS [netPremiumReported],
          ISNULL(b.[netPremiumDelta],0) AS [netPremiumDelta],
          ISNULL(b.[retainedPremiumDelta],0) AS [retainedPremiumDelta]
  FROM    [dbo].[agent] a LEFT OUTER JOIN
          (
          SELECT  b.[batchID],
                  b.[periodID],
                  b.[yearID],
                  b.[agentID],
                  b.[stateID],
                  b.[stat],
                  ISNULL(b.[grossPremiumReported],0) AS [grossPremiumReported],
                  ISNULL(bf.[grossPremiumDelta],0) AS [grossPremiumDelta],
                  ISNULL(b.[netPremiumReported],0) AS [netPremiumReported],
                  ISNULL(bf.[netPremiumDelta],0) AS [netPremiumDelta],
                  ISNULL(bf.[retainedPremiumDelta],0) AS [retainedPremiumDelta]
          FROM    [dbo].[batch] b LEFT OUTER JOIN
                  (
                  SELECT  bf.[batchID],
                          SUM(bf.[liabilityAmount]) AS [liabilityAmount],
                          SUM(bf.[liabilityDelta]) AS [liabilityDelta],
                          SUM(bf.[grossPremium]) AS [grossPremiumProcessed],
                          SUM(bf.[grossDelta]) AS [grossPremiumDelta],
                          SUM(bf.[netPremium]) AS [netPremiumProcessed],
                          SUM(bf.[netDelta]) AS [netPremiumDelta],
                          SUM(bf.[retentionPremium]) AS [retainedPremiumProcessed],
                          SUM(bf.[retentionDelta]) AS [retainedPremiumDelta],
                          COUNT(bf.[batchID]) AS [fileCount],
                          SUM(CASE WHEN bf.[formType] = 'P' THEN 1 ELSE 0 END) AS [policyCount],
                          SUM(CASE WHEN bf.[formType] = 'E' THEN 1 ELSE 0 END) AS [endorsementCount],
                          SUM(CASE WHEN bf.[formType] = 'C' THEN 1 ELSE 0 END) AS [cplCount]
                  FROM    [dbo].[batchform] bf
                  GROUP BY bf.[batchID]
                  ) bf
          ON      b.[batchID] = bf.[batchID] LEFT OUTER JOIN
                  [dbo].[sysdoc] d
          ON      d.[entityType] = 'Batch'
          AND     d.[entityID] = CONVERT(VARCHAR, b.[batchID]) LEFT OUTER JOIN
                  [dbo].[syslock] l
          ON      l.[entityType] = 'Batch'
          AND     l.[entityID] = CONVERT(VARCHAR, b.[batchID])
          ) b
  ON      a.[agentID] = b.[agentID]
  AND     b.[periodID] = @periodID INNER JOIN
          [dbo].[period] p
  ON      p.[periodID] = @periodID
  WHERE   a.[stat] <> 'P'
  ORDER BY [stateID],[agentName]
END
GO
