USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetAgentGrpQAR]    Script Date: 11/29/2018 8:12:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetAgentGrpQAR]
	-- Add the parameters for the stored procedure here
    @auditID INTEGER = 0,
    @year INTEGER = 0,
    @auditor VARCHAR(50) = 'ALL',
    @status VARCHAR(10) = 'ALL',
    @UID VARCHAR(100) = '',
    @stateID     VARCHAR(MAX) = '',
	@YearOfSigning integer = 0,
	@Software VARCHAR(MAX) = '',
	@Company VARCHAR(MAX) = '',
	@Organization VARCHAR(MAX) = '',
	@Manager VARCHAR(MAX) = '',
	@TagList VARCHAR(MAX) = '',
	@AffiliationList VARCHAR(MAX) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
  SET NOCOUNT ON;
	
  DECLARE @agentID     VARCHAR(MAX) = 'ALL' 
  
  CREATE TABLE #agentTable ([agentID] VARCHAR(30))
  INSERT INTO #agentTable ([agentID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)

  CREATE TABLE #auditTbl 
  (
  [qarID] INTEGER, 
  [version] VARCHAR(2),
  [auditYear] INTEGER, 
  [agentID] VARCHAR(10),
  [name] VARCHAR(MAX),  
  [addr] VARCHAR(MAX), 
  [city] VARCHAR(50), 
  [state] VARCHAR(2),
  [zip] VARCHAR(20), 
  [auditStartDate] DATETIME,
  [auditFinishDate] DATETIME,
  [schedStartDate] DATETIME,
  [schedFinishDate] DATETIME,
  [onsiteStartDate] DATETIME,
  [onsiteFinishDate] DATETIME,
  [auditor] VARCHAR(100), 
  [uid] VARCHAR(100), 
  [contactName] VARCHAR(80),
  [contactPhone] VARCHAR(40),
  [contactFax] VARCHAR(40),
  [contactEmail] VARCHAR(100),
  [contactPosition] VARCHAR(200) ,
  [parentName] VARCHAR(200),
  [parentAddr] VARCHAR(200),
  [parentCity] VARCHAR(50),
  [parentState] VARCHAR(2) ,
  [parentZip] VARCHAR(10),
  [auditType] VARCHAR(1),
  [errType] VARCHAR(1),
  [stat] VARCHAR(1),
  [comments] VARCHAR(MAX),
  [stateID] VARCHAR(2),
  [mainOffice] BIT,
  [numOffices] INTEGER ,
  [numEmployees] INTEGER ,
  [numUnderwriters] INTEGER ,
  [ranking] INTEGER,
  [services] VARCHAR(100),
  [draftReportDate] DATETIME,
  [score] INTEGER,
  [grade] INTEGER,
  [submittedAt] DATETIME,
  [submittedBy] VARCHAR(70)
  )
  
  CREATE TABLE #auditTblFiltered 
  (
  [qarID] INTEGER, 
  [version] VARCHAR(2),
  [auditYear] INTEGER, 
  [agentID] VARCHAR(10),
  [name] VARCHAR(MAX),  
  [addr] VARCHAR(MAX), 
  [city] VARCHAR(50), 
  [state] VARCHAR(2),
  [zip] VARCHAR(20), 
  [auditStartDate] DATETIME,
  [auditFinishDate] DATETIME,
  [schedStartDate] DATETIME,
  [schedFinishDate] DATETIME,
  [onsiteStartDate] DATETIME,
  [onsiteFinishDate] DATETIME,
  [auditor] VARCHAR(100), 
  [uid] VARCHAR(100), 
  [contactName] VARCHAR(80),
  [contactPhone] VARCHAR(40),
  [contactFax] VARCHAR(40),
  [contactEmail] VARCHAR(100),
  [contactPosition] VARCHAR(200) ,
  [parentName] VARCHAR(200),
  [parentAddr] VARCHAR(200),
  [parentCity] VARCHAR(50),
  [parentState] VARCHAR(2) ,
  [parentZip] VARCHAR(10),
  [auditType] VARCHAR(1),
  [errType] VARCHAR(1),
  [stat] VARCHAR(1),
  [comments] VARCHAR(MAX),
  [stateID] VARCHAR(2),
  [mainOffice] BIT,
  [numOffices] INTEGER ,
  [numEmployees] INTEGER ,
  [numUnderwriters] INTEGER ,
  [ranking] INTEGER,
  [services] VARCHAR(100),
  [draftReportDate] DATETIME,
  [score] INTEGER,
  [grade] INTEGER,
  [submittedAt] DATETIME,
  [submittedBy] VARCHAR(70)
  )

 CREATE TABLE #agentsGroup
   (
   [agentID] VARCHAR(20),
   [corporationID] VARCHAR(MAX),  
   [stateID] VARCHAR(2),
   [stateName] VARCHAR(20), 
   [stat] VARCHAR(1),
   [name] VARCHAR(MAX),
   [contractDate] datetime,
   [swVendor] VARCHAR(100),
   [manager] VARCHAR(MAX),
   [orgID] VARCHAR(50),
   [orgRoleID] integer, 
   [orgName] VARCHAR(Max)
   )	

INSERT INTO #agentsGroup ([agentID] , [corporationID]  , [stateID] , [stateName] , [stat], [name], [contractDate] , [swVendor], [manager], [orgID] , [orgRoleID] , [orgName]  )
Exec SpGetConsolidatedAgents @StateID ,
	@YearOfSigning ,
	@Software,
	@Company  ,
	@Organization ,
    @Manager  ,
	@TagList ,
	@AffiliationList  ,
	@UID 
	
INSERT INTO #auditTbl ([qarID], 
  [version],
  [auditYear], 
  [agentID],
  [name],  
  [addr], 
  [city], 
  [state],
  [zip], 
  [auditStartDate],
  [auditFinishDate],
  [schedStartDate],
  [schedFinishDate],
  [onsiteStartDate],
  [onsiteFinishDate],
  [auditor],
  [uid],
  [contactName],
  [contactPhone] ,
  [contactFax] ,
  [contactEmail],
  [contactPosition] ,
  [parentName],
  [parentAddr] ,
  [parentCity],
  [parentState],
  [parentZip],
  [auditType],
  [errType],
  [stat] ,
  [comments] ,
  [stateID] ,
  [mainOffice] ,
  [numOffices],
  [numEmployees],
  [numUnderwriters],
  [ranking],
  [services] ,
  [draftReportDate] ,
  [score] ,
  [grade] ,
  [submittedAt] ,
  [submittedBy] )
    EXEC [dbo].[spGetQAR] @auditID = 0, @year = 0, @stateID = 'ALL', @agentID = 'ALL', @auditor = 'ALL', @status = 'ALL'	
	
INSERT INTO #auditTblFiltered  select * from  #auditTbl ad  where 
         (ad.[agentID]  IN (SELECT [agentID] FROM #agentsGroup)) 
 
  SELECT * FROM #auditTblFiltered  
  DROP TABLE #agentsGroup
  DROP TABLE #agentTable
  DROP TABLE #auditTbl
  DROP TABLE #auditTblFiltered
END
