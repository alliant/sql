USE [COMPASS]
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Plameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Anjly>
-- Create date: <Create Date,,07-27-2020>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetLedger]
	-- Add the parameters for the stored procedure here
	@ledgerID     INTEGER      = 0,
	@accountID    VARCHAR(30)  = 'ALL',
	@startDate    DATETIME     = NULL,
	@endDate      DATETIME     = NULL 
AS
IF @ledgerID = 0 and @startDate IS NULL and @endDate IS NULL 
BEGIN    
  RETURN
END  
ELSE 
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;
	
  create table #tempLedger (ledgerID      integer,
                            seq           integer,
                            agentID       varchar(100),
		      	            accountID     varchar(100),
		                    creditAmount  [decimal](18, 2) null,
     				        debitAmount   [decimal](18, 2) null,
					        transDate     datetime,
                            createdBy     varchar(100),
    			   	        createDate    datetime,	
                            notes         varchar(500),								
                            sourceID      varchar(50),
                            source        varchar(10),
	    			        agentName     varchar(500),
					        accountDesc   varchar(500),
				            createdByName varchar(500),
     	 		            void          bit
							)
		
  -- Insert statements for procedure here	  
  insert into #tempLedger ( ledgerID, seq,  agentID, accountID,  creditAmount, debitAmount, transDate , createdBy , createDate, notes, sourceID,  source, agentName, createdByName)    
   
  select l.ledgerID, l.seq, l.agentID  , l.accountID,  l.creditAmount , l.debitAmount ,l.transDate, l.createdBy, l.createDate, l.notes, l.sourceID, l.source ,ag.name, su.name
	  from ledger l
	  left outer join agent ag on l.agentID = ag.agentID
	  left outer join sysuser su on l.createdBy = su.uid 
	  where l.ledgerID   = (case when @ledgerID   = 0      then l.ledgerID  else @ledgerID end)  and
	        l.accountID  = (case when @accountID  = 'ALL'  then l.accountID else @accountID end) and 
			l.transdate >= (case when @startDate  is null  then l.transdate else @startDate end) and 
			l.transdate <= (case when @endDate    is null  then l.transdate else @endDate end) 
			order by l.ledgerID desc

    /* Update void column #tempLedger */
  UPDATE #tempLedger
  SET #tempLedger.void = (case when #tempLedger.source  = 'P'  then (select arpmt.void from arpmt where arpmt.arpmtID = #tempLedger.sourceID and arpmt.voidLedgerID = #tempLedger.ledgerID)  
                               when #tempLedger.source  = 'C'  then (select armisc.void from armisc where armisc.armiscID = #tempLedger.sourceID and armisc.voidLedgerID = #tempLedger.ledgerID)
							   when #tempLedger.source  = 'I'  then (select armisc.void from armisc where armisc.armiscID = #tempLedger.sourceID and armisc.voidLedgerID = #tempLedger.ledgerID)
							   when #tempLedger.source  = 'RF' then (select artran.void from artran where artran.artranID = #tempLedger.sourceID and artran.voidLedgerID = #tempLedger.ledgerID)
							   when #tempLedger.source  = 'D'  then (select ardeposit.void from ardeposit where ardeposit.depositID = #tempLedger.sourceID and ardeposit.voidLedgerID = #tempLedger.ledgerID)
							   when #tempLedger.source  = 'W'  then (select arWriteoff.void from arWriteoff where arWriteoff.arWriteoffID = #tempLedger.sourceID and arWriteoff.voidLedgerID = #tempLedger.ledgerID)
							   when #tempLedger.source  = 'B'  then 0
					     end)

  select * from #tempLedger	
END
