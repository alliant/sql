
/****** Object:  StoredProcedure [dbo].[spGetGLPayment]    Script Date: 2/24/2021 3:04:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetGLPayment] 
	-- Add the parameters for the stored procedure here
	@agentID     VARCHAR(MAX) = 'ALL',
    @startDate   DATETIME     = NULL ,
	@endDate     DATETIME     = NULL,
    @archived    bit          = 0	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
   CREATE TABLE #glPayment (agentID             VARCHAR(80),
							artranid		integer,
                               chknum           VARCHAR(50),
                               pmtID            VARCHAR(50),
                               postingDate      DATETIME,
                               argldef          VARCHAR(500),
				               arglref          VARCHAR(500),
		                       debit            [decimal](18, 2) NULL,
     				           credit           [decimal](18, 2) NULL,
                               depositRef       VARCHAR(50),
                               voided           VARCHAR(20) default '',
						       notes            VARCHAR(MAX)
     	 		      )

    /*-------credit -----------*/
    INSERT INTO #glPayment ( agentid,artranid,chknum, pmtid,postingdate, arglref, credit, notes)       
    select ar.entityid,ar.artranid,ar.reference, ar.tranid,ar.trandate, lg.accountID , ar.tranamt,
			CASE WHEN ar.voidDate IS NOT NULL THEN ('Payment voided ' + CONVERT(VARCHAR, ar.voidDate, 101)) ELSE '' END AS    [notes]		
	    from artran ar 
		    inner join arpmt ap 
	    ON ar.tranID = ap.arpmtid
		    left outer join ledger lg
        on ap.ledgerID = lg.ledgerID			
    	where ar.tranDate >= @startDate 
		  and ar.tranDate <= @endDate 
		  and ar.entityid = (case when @agentID  = 'ALL' then ar.entityid  else @agentID  end) 
		  and (ar.type = 'p' and ar.seq = 0)
		  and lg.creditAmount > 0
		  and (lg.source = 'P' or lg.source = 'D')
		  and lg.notes not like 'History Transaction%'
		  
	/*-------Debit -----------*/	  
    INSERT INTO #glPayment ( agentid,artranid,chknum, pmtid,postingdate, arglref, debit, notes)       
    select ar.entityid,ar.artranid,ar.reference, ar.tranid,ar.trandate, lg.accountID , ar.tranamt,
			CASE WHEN ar.voidDate IS NOT NULL THEN ('Payment voided ' + CONVERT(VARCHAR, ar.voidDate, 101)) ELSE '' END AS    [notes] 
	    from artran ar 
		    inner join arpmt ap 
	    ON ar.tranID = ap.arpmtid
		    left outer join ledger lg
        on ap.ledgerID = lg.ledgerID			
    	where ar.tranDate >= @startDate 
		  and ar.tranDate <= @endDate 
		  and ar.entityid = (case when @agentID  = 'ALL' then ar.entityid  else @agentID  end) 
		  and (ar.type = 'p' and ar.seq = 0)
		  and lg.debitAmount > 0
		  and (lg.source = 'P' or lg.source = 'D')
		  and lg.notes not like 'History Transaction%'
		  	  

    /*-------void credit-----------*/			
    INSERT INTO #glPayment ( agentid,artranid,chknum, pmtid,postingdate, arglref, credit, voided,notes)    
   
    select  ar.entityid,ar.artranid,ar.reference, ar.tranid,ar.voidDate,  lg.accountID ,  ar.tranamt, 'Yes',
				'Void Payment ' + CONVERT(VARCHAR, [tranDate], 101)
	  from artran ar
	     inner join arpmt ap 
	    ON ar.tranID = ap.arpmtid
		    left outer join ledger lg
        on ap.voidledgerID = lg.ledgerID
	  where ar.voidDate >= @startDate 
	    and ar.voidDate <= @endDate 
		and ar.entityid = (case when @agentID  = 'ALL' then ar.entityid  else @agentID  end) 
		and (ar.type = 'p' and ar.seq = 0) and ar.void = 1
		and lg.creditAmount > 0
		
	/*-------void debit-----------*/			
    INSERT INTO #glPayment ( agentid,artranid,chknum, pmtid,postingdate, arglref, debit, voided,notes)    
   
    select  ar.entityid,ar.artranid,ar.reference, ar.tranid,ar.voidDate,  lg.accountID , ar.tranamt , 'Yes',
				'Void Payment ' + CONVERT(VARCHAR, [tranDate], 101)
	  from artran ar
	     inner join arpmt ap 
	    ON ar.tranID = ap.arpmtid
		    left outer join ledger lg
        on ap.voidledgerID = lg.ledgerID
	  where ar.voidDate >= @startDate 
	    and ar.voidDate <= @endDate 
		and ar.entityid = (case when @agentID  = 'ALL' then ar.entityid  else @agentID  end) 
		and (ar.type = 'p' and ar.seq = 0) and ar.void = 1
		and lg.debitAmount > 0
		  	  
    /*-------Refund credit-----------*/			
    INSERT INTO #glPayment ( agentid,artranid,chknum, pmtid,postingdate, arglref,credit, notes)    
   
    select  ar.entityid,ar.artranid,ar.reference, ar.tranid,ar.trandate,  lg.accountID , abs(ar.tranamt) as tranamt,
			CASE WHEN ar.voidDate IS NOT NULL THEN ('Refund voided ' + CONVERT(VARCHAR, ar.voidDate, 101)) 
			    ELSE ('Payment ' + CONVERT(VARCHAR, ap.transDate, 101) + ' refunded') END AS [notes]
	  from artran ar
	     left outer join ledger lg
        on ar.ledgerID = lg.ledgerID
		 left outer join arpmt ap
        on ar.tranid = ap.arpmtID
	  where ar.tranDate >= @startDate 
	    and ar.tranDate <= @endDate 
		and ar.entityid = (case when @agentID  = 'ALL' then ar.entityid  else @agentID  end) 
		and (ar.type = 'p' and ar.seq >= 1) and ar.tranAmt < 0
		and lg.creditAmount > 0
		
	/*-------Refund Debit-----------*/			
    INSERT INTO #glPayment ( agentid,artranid,chknum, pmtid,postingdate, arglref,debit, notes)    
   
    select  ar.entityid,ar.artranid,ar.reference, ar.tranid,ar.trandate,  lg.accountID , abs(ar.tranamt) as tranamt,
			CASE WHEN ar.voidDate IS NOT NULL THEN ('Refund voided ' + CONVERT(VARCHAR, ar.voidDate, 101)) 
			          ELSE ('Payment ' + CONVERT(VARCHAR, ap.transDate, 101) + ' refunded') END AS [notes]
	  from artran ar
	     left outer join ledger lg
        on ar.ledgerID = lg.ledgerID
		 left outer join arpmt ap
        on ar.tranid = ap.arpmtID
	  where ar.tranDate >= @startDate 
	    and ar.tranDate <= @endDate 
		and ar.entityid = (case when @agentID  = 'ALL' then ar.entityid  else @agentID  end) 
		and (ar.type = 'p' and ar.seq >= 1) and ar.tranAmt < 0
		and lg.debitAmount > 0

    /*-------Void Refund credit ------*/			
    INSERT INTO #glPayment ( agentid,artranid,chknum, pmtid,postingdate, arglref, credit, voided, notes)    
   
    select  ar.entityid,ar.artranid,ar.reference, ar.tranid,ar.voidDate,  lg.accountID ,abs(ar.tranamt) as tranamt, 'Yes',
				'Void Refund ' + CONVERT(VARCHAR, [tranDate], 101)
	  from artran ar
	     left outer join ledger lg
        on ar.voidledgerID = lg.ledgerID 
	  where ar.voidDate >= @startDate 
	    and ar.voidDate <= @endDate 
		and ar.entityid = (case when @agentID  = 'ALL' then ar.entityid  else @agentID  end) 
		and (ar.type = 'p' and ar.seq >= 1)
		and lg.creditAmount > 0
		
	/*-------Void Refund Debit ------*/			
    INSERT INTO #glPayment ( agentid,artranid,chknum, pmtid,postingdate, arglref, debit, voided, notes)    
   
    select  ar.entityid,ar.artranid,ar.reference, ar.tranid,ar.voidDate,  lg.accountID ,abs(ar.tranamt) as tranamt, 'Yes',
				'Void Refund ' + CONVERT(VARCHAR, [tranDate], 101)
	  from artran ar
		    left outer join ledger lg
        on ar.voidledgerID = lg.ledgerID 
	  where ar.voidDate >= @startDate 
	    and ar.voidDate <= @endDate 
		and ar.entityid = (case when @agentID  = 'ALL' then ar.entityid  else @agentID  end) 
		and (ar.type = 'p' and ar.seq >= 1)
        and lg.debitAmount > 0		

	IF @archived = 1
	BEGIN
		/*-------credit -----------*/
		INSERT INTO #glPayment (agentid,artranid,chknum, pmtid,postingdate, arglref, credit, notes)       
		select ar.entityid,ar.artranID,ar.reference, ar.tranid,ar.trandate, lg.accountID , ar.tranamt,
				CASE WHEN ar.voidDate IS NOT NULL THEN ('Payment voided ' + CONVERT(VARCHAR, ar.voidDate, 101)) ELSE '' END AS    [notes]		
			from artranh ar 
				inner join arpmt ap 
			ON ar.tranID = ap.arpmtid
				left outer join ledger lg
			on ap.ledgerID = lg.ledgerID			
			where ar.tranDate >= @startDate 
			and ar.tranDate <= @endDate 
			and ar.entityid = (case when @agentID  = 'ALL' then ar.entityid  else @agentID  end) 
			and (ar.type = 'p' and ar.seq = 0)
			and lg.creditAmount > 0
			and (lg.source = 'P' or lg.source = 'D')
			and lg.notes not like 'History Transaction%'
		  
		/*-------Debit -----------*/	  
		INSERT INTO #glPayment ( agentid,artranid,chknum, pmtid,postingdate, arglref, debit, notes)       
		select ar.entityid,ar.artranID,ar.reference, ar.tranid,ar.trandate, lg.accountID , ar.tranamt,
				CASE WHEN ar.voidDate IS NOT NULL THEN ('Payment voided ' + CONVERT(VARCHAR, ar.voidDate, 101)) ELSE '' END AS    [notes] 
			from artranh ar 
				inner join arpmt ap 
			ON ar.tranID = ap.arpmtid
				left outer join ledger lg
			on ap.ledgerID = lg.ledgerID			
			where ar.tranDate >= @startDate 
			and ar.tranDate <= @endDate 
			and ar.entityid = (case when @agentID  = 'ALL' then ar.entityid  else @agentID  end) 
			and (ar.type = 'p' and ar.seq = 0)
			and lg.debitAmount > 0
			and (lg.source = 'P' or lg.source = 'D')
			and lg.notes not like 'History Transaction%'
		  	  

		/*-------void credit-----------*/			
		INSERT INTO #glPayment ( agentid,artranid,chknum, pmtid,postingdate, arglref, credit, voided,notes)    
   
		select  ar.entityid,ar.artranID,ar.reference, ar.tranid,ar.voidDate,  lg.accountID ,  ar.tranamt, 'Yes',
					'Void Payment ' + CONVERT(VARCHAR, [tranDate], 101)
		from artranh ar
			inner join arpmt ap 
			ON ar.tranID = ap.arpmtid
				left outer join ledger lg
			on ap.voidledgerID = lg.ledgerID
		where ar.voidDate >= @startDate 
			and ar.voidDate <= @endDate 
			and ar.entityid = (case when @agentID  = 'ALL' then ar.entityid  else @agentID  end) 
			and (ar.type = 'p' and ar.seq = 0) and ar.void = 1
			and lg.creditAmount > 0
		
		/*-------void debit-----------*/			
		INSERT INTO #glPayment ( agentid,artranid,chknum, pmtid,postingdate, arglref, debit, voided,notes)    
   
		select  ar.entityid,ar.artranID,ar.reference, ar.tranid,ar.voidDate,  lg.accountID , ar.tranamt , 'Yes',
					'Void Payment ' + CONVERT(VARCHAR, [tranDate], 101)
		from artranh ar
			inner join arpmt ap 
			ON ar.tranID = ap.arpmtid
				left outer join ledger lg
			on ap.voidledgerID = lg.ledgerID
		where ar.voidDate >= @startDate 
			and ar.voidDate <= @endDate 
			and ar.entityid = (case when @agentID  = 'ALL' then ar.entityid  else @agentID  end) 
			and (ar.type = 'p' and ar.seq = 0) and ar.void = 1
			and lg.debitAmount > 0
		  	  
		/*-------Refund credit-----------*/			
		INSERT INTO #glPayment ( agentid,artranid,chknum, pmtid,postingdate, arglref,credit, notes)    
   
		select  ar.entityid,ar.artranID,ar.reference, ar.tranid,ar.trandate,  lg.accountID , abs(ar.tranamt) as tranamt,
				CASE WHEN ar.voidDate IS NOT NULL THEN ('Refund voided ' + CONVERT(VARCHAR, ar.voidDate, 101)) 
					ELSE ('Payment ' + CONVERT(VARCHAR, ap.transDate, 101) + ' refunded') END AS [notes]
		from artranh ar
			left outer join ledger lg
			on ar.ledgerID = lg.ledgerID
			left outer join arpmt ap
			on ar.tranid = ap.arpmtID
		where ar.tranDate >= @startDate 
			and ar.tranDate <= @endDate 
			and ar.entityid = (case when @agentID  = 'ALL' then ar.entityid  else @agentID  end) 
			and (ar.type = 'p' and ar.seq >= 1) and ar.tranAmt < 0
			and lg.creditAmount > 0
		
		/*-------Refund Debit-----------*/			
		INSERT INTO #glPayment ( agentid,artranid,chknum, pmtid,postingdate, arglref,debit, notes)    
   
		select  ar.entityid,ar.artranID,ar.reference, ar.tranid,ar.trandate,  lg.accountID , abs(ar.tranamt) as tranamt,
				CASE WHEN ar.voidDate IS NOT NULL THEN ('Refund voided ' + CONVERT(VARCHAR, ar.voidDate, 101)) 
						ELSE ('Payment ' + CONVERT(VARCHAR, ap.transDate, 101) + ' refunded') END AS [notes]
		from artranh ar
			left outer join ledger lg
			on ar.ledgerID = lg.ledgerID
			left outer join arpmt ap
			on ar.tranid = ap.arpmtID
		where ar.tranDate >= @startDate 
			and ar.tranDate <= @endDate 
			and ar.entityid = (case when @agentID  = 'ALL' then ar.entityid  else @agentID  end) 
			and (ar.type = 'p' and ar.seq >= 1) and ar.tranAmt < 0
			and lg.debitAmount > 0

		/*-------Void Refund credit ------*/			
		INSERT INTO #glPayment ( agentid,artranid,chknum, pmtid,postingdate, arglref, credit, voided, notes)    
   
		select  ar.entityid,ar.artranID,ar.reference, ar.tranid,ar.voidDate,  lg.accountID ,abs(ar.tranamt) as tranamt, 'Yes',
					'Void Refund ' + CONVERT(VARCHAR, [tranDate], 101)
		from artranh ar
			left outer join ledger lg
			on ar.voidledgerID = lg.ledgerID 
		where ar.voidDate >= @startDate 
			and ar.voidDate <= @endDate 
			and ar.entityid = (case when @agentID  = 'ALL' then ar.entityid  else @agentID  end) 
			and (ar.type = 'p' and ar.seq >= 1)
			and lg.creditAmount > 0
		
		/*-------Void Refund Debit ------*/			
		INSERT INTO #glPayment ( agentid,artranid,chknum, pmtid,postingdate, arglref, debit, voided, notes)    
   
		select  ar.entityid,ar.artranID,ar.reference, ar.tranid,ar.voidDate,  lg.accountID ,abs(ar.tranamt) as tranamt, 'Yes',
					'Void Refund ' + CONVERT(VARCHAR, [tranDate], 101)
		from artranh ar
				left outer join ledger lg
			on ar.voidledgerID = lg.ledgerID 
		where ar.voidDate >= @startDate 
			and ar.voidDate <= @endDate 
			and ar.entityid = (case when @agentID  = 'ALL' then ar.entityid  else @agentID  end) 
			and (ar.type = 'p' and ar.seq >= 1)
			and lg.debitAmount > 0
    END
  

    update #glPayment
	  set depositRef  = ad.depositRef
	  from arpmt ap
	  inner join
	  ardeposit ad
	  on ad.depositID = ap.depositID
	  where pmtid = ap.arPmtID

    update #glPayment set credit = ISNULL(credit,0),
		     		      debit  = ISNULL(debit,0)

    select * from #glPayment order by pmtid,artranid 
	drop table #glPayment     

END
