USE [COMPASS]
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[spArGLInfo]
	-- Add the parameters for the stored procedure here
	@agentID     VARCHAR(MAX) = 'ALL',
	@startDate    DATE         = NULL ,
	@endDate      DATE         = NULL 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
CREATE TABLE #GLInfo (agentid           VARCHAR(500),
                      agentName         VARCHAR(500),
                      trandate          datetime NULL,
				      fileID            VARCHAR(500),
		              tranid            VARCHAR(500),
     				  reference          VARCHAR(500),
					  type              VARCHAR(500),
					  transtype         VARCHAR(500),
                      void              bit,
                      creditacct1       VARCHAR(50),
    			   	  creditacct2        VARCHAR(500),	
                      debitcct1         VARCHAR(500),								
                      debitacct2        VARCHAR(500),
                      creditamt1        [decimal](18, 2) NULL,
	    			  creditamt2        [decimal](18, 2) NULL,
					  debitamt1         [decimal](18, 2) NULL,
				      debitamt2         [decimal](18, 2) NULL,
					  revenuetype       VARCHAR(500),
				      artranid          integer,
				   	  batchid           integer
     	 		      )
		
delete #GLInfo	
			
INSERT INTO #GLInfo ( agentid, agentName, trandate,  tranid, reference,  type, transtype, void, creditacct1 ,  debitcct1, creditamt1, debitamt1, artranid, revenuetype)    
   
select ar.entityid, a.name, ar.trandate, ar.tranid  , ar.reference,  ar.type , ar.transtype ,ar.void,  a.arcashglref ,a.arglref , ar.tranamt , ar.tranamt ,ar.artranid, ar.revenueType
	  from artran ar inner join agent a on ar.entityid = a.agentid 
	  where ar.tranDate >= @startDate and ar.tranDate <= @endDate and
	        ar.entityid = (case when @agentID  = ''     then ar.entityid  else @agentID  end) and
	       ((ar.type = 'p' and ar.seq = 0) or ar.type = 'c' or ar.type = 'i') and ar.transtype = ''

INSERT INTO #GLInfo ( agentid, agentName, trandate,  tranid, reference,  type, transtype, void, creditacct1 ,  debitcct1, creditamt1, debitamt1, artranid, revenuetype)    
   
select ar.entityid, a.name, ar.trandate, ar.tranid  , ar.reference,  ar.type , ar.transtype ,ar.void,  a.arglref, a.arcashglref, ar.tranamt , ar.tranamt ,ar.artranid, ar.revenueType
	  from artran ar inner join agent a on ar.entityid = a.agentid 
	  where ar.tranDate >= @startDate and ar.tranDate <= @endDate and
	        ar.entityid = (case when @agentID  = ''     then ar.entityid  else @agentID  end) and
			(ar.type = 'p' and ar.seq = 1) and ar.void = 0 and ar.transtype = ''
	  
INSERT INTO #GLInfo ( agentid, agentName, trandate,  tranid, reference,  type, transtype, void, creditacct1 ,  debitcct1, creditamt1, debitamt1, artranid, revenuetype)    
   
select ar.entityid, a.name,  ar.trandate, ar.tranid  , ar.reference,  ar.type , ar.transtype ,ar.void,  a.arglref, a.arcashglref, ar.tranamt , ar.tranamt ,ar.artranid, ar.revenueType
	  from artran ar inner join agent a on ar.entityid = a.agentid 
	  where ar.tranDate >= @startDate and ar.tranDate <= @endDate and
	        ar.entityid = (case when @agentID  = ''     then ar.entityid  else @agentID  end) and
			(ar.type = 'p' or ar.type = 'c' or ar.type = 'i') and ar.void = 1 and ar.transtype = ''
	  
INSERT INTO #GLInfo ( agentid, trandate,  fileid, reference,  type, transtype, void, creditacct1 , creditacct2 , debitcct1, creditamt1, creditamt2,  debitamt1, artranid, revenuetype,batchid)    
   
select ar.entityid, ar.trandate, ar.fileid  , ar.reference,  ar.type , ar.transtype ,ar.void,  arrev.retainedGLRef, arrev.netGLRef, arrev.grossGLRef, bf.retentionDelta , bf.netDelta, bf.grossDelta ,ar.artranid, ar.revenueType, bf.batchID
	  from artran ar
	  inner join arrevenue arrev on ar.revenueType = arrev.revenueType
	  inner join batchform bf on ar.sourceID = cast(bf.batchID as VARCHAR) and ar.reference = cast( bf.policyID as varchar) and ar.revenueType = bf.revenueType 
	  where ar.tranDate >= @startDate and ar.tranDate <= @endDate and
	        ar.entityid = (case when @agentID  = ''     then ar.entityid  else @agentID  end) and
		   ( ar.type = 'r' or ar.type = 'i') and ar.void = 0 and ar.transtype = 'f'

select * from #GLInfo

 
	
END
