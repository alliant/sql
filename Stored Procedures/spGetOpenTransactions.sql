-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetOpenTransactions]    Script Date: 4/7/2021 12:06:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author:		<Rahul Sharma>
-- Create date: <16-11-2020>
-- Description:	<Get artran records>
-- =============================================
ALTER PROCEDURE [dbo].[spGetOpenTransactions] 
	-- Add the parameters for the stored procedure here
	@pcEntityID       VARCHAR(50) = 'ALL',
	@piBatchID        INTEGER     = 0,
	@pcFileID         VARCHAR(50) = 'ALL',
	@pcInvoiceID      VARCHAR(50) = 'ALL',
    @pcType           VARCHAR(50) = 'ALL',
    @piReferenceID    INTEGER     = 0,	
	@pcRevenueType    VARCHAR(50) = 'ALL'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    
	-- Insert statements for procedure here
    
	CREATE TABLE #artran (artranID         INTEGER,
	                      revenueType      VARCHAR(50),
						  tranID           VARCHAR(50),
						  seq              INTEGER,
						  sourceType       VARCHAR(50),
						  sourceID         VARCHAR(50),
						  type             VARCHAR(50),
	                      entity           VARCHAR(50),
	                      entityID         VARCHAR(50),
						  entityName       VARCHAR(200),
						  fileNumber       VARCHAR(50),
	                      fileID           VARCHAR(50),
						  tranAmt          [DECIMAL](18, 2) NULL,
						  remainingAmt     [DECIMAL](18, 2) NULL,
					      tranDate         DATETIME,
                          void             BIT,
                          voidDate	       DATETIME,
                          voidBy	       VARCHAR(100),
                          notes            VARCHAR(MAX),
						  arNotes          VARCHAR(MAX), 
                          createdBy        VARCHAR(100),
                          createdDate      DATETIME,
                          dueDate          DATETIME,
                          fullyPaid        BIT,
                          reference        VARCHAR(100),
                          appliedAmt       [DECIMAL](18, 2) NULL,
                          transType        VARCHAR(20),
                          postBy           VARCHAR(100), 						  
						  postDate         DATETIME,
						  ledgerID         INTEGER,
						  voidledgerID     INTEGER,
						  appliedamtbyref  [DECIMAL](18, 2) NULL, 
    	                  batchFileAmtList VARCHAR(MAX), /* Client Side */ 
						  batchList        VARCHAR(MAX)) /* Client Side */ 
    
	IF @piReferenceID = 0
	BEGIN
	  IF @piBatchID <> 0 
	  BEGIN
	    INSERT INTO #artran (artranID,  revenueType,  tranID, seq, sourceType, sourceID, type, entity, entityID, entityName, fileNumber, fileID, tranAmt,
	                         remainingAmt, tranDate, void, voidDate, voidBy, notes, createdBy, createdDate, dueDate, fullyPaid, reference,
		    			     appliedAmt, transType, postBy, postDate, ledgerID, voidledgerID) 
						 
        SELECT artran.artranID, artran.revenueType, artran.tranID, artran.seq, artran.sourceType, artran.sourceID, artran.type, artran.entity,
	           artran.entityID, agent.name, artran.fileNumber, artran.fileID, artran.tranAmt, artran.remainingAmt, artran.tranDate, artran.void, artran.voidDate,
		       artran.voidBy, artran.notes, artran.createdBy, artran.createdDate, artran.dueDate, artran.fullyPaid, artran.reference, artran.appliedAmt,
		       artran.transType, artran.postBy, artran.postDate, artran.ledgerID, artran.voidledgerID
          FROM artran
		  INNER JOIN agent
		  ON artran.entityID = agent.agentID	  
          WHERE artran.entity       = 'A' 
		    AND artran.entityID     = (CASE WHEN @pcEntityID = 'ALL' THEN artran.entityID  ELSE @pcEntityID END)
			AND ((artran.type = 'F' AND artran.transtype = 'F' AND artran.fileID = (CASE WHEN @pcFileID = 'ALL' THEN artran.fileID ELSE @pcFileID END)) OR (artran.type = 'I' AND artran.transtype = ' ' AND artran.tranID = (CASE WHEN @pcInvoiceID = 'ALL' THEN artran.tranID ELSE @pcInvoiceID END)))  		       
	        AND artran.seq          = 0
		    AND artran.remainingAmt > 0
			AND EXISTS(SELECT * FROM artran at WHERE  at.entity      = artran.entity
			                                     AND  at.entityID    = artran.entityID 
												 AND (((at.fileID      = artran.fileID OR at.fileNumber = artran.fileNumber) 
												       AND  at.transtype   = 'F' AND  at.sourceType  = 'B'
											           AND  at.sourceID    = CAST(@piBatchID AS VARCHAR)) 
												      OR (at.transtype = ' ' AND at.tranID = artran.tranID ))
			                                     AND (at.type = 'I' OR at.type = 'R'))

        /* Calculate total amount of file type records */
	    UPDATE #artran
	      SET #artran.tranAmt = s.tranAmt,
		      #artran.arNotes = agentfile.arNotes
	      FROM #artran
	      INNER JOIN (SELECT at.entityID, at.fileID, SUM (at.tranAmt) AS tranAmt FROM artran at WHERE  at.entity    = 'A'
		                                                                                          AND  at.entityID  = (CASE WHEN @pcEntityID = 'ALL' THEN at.entityID ELSE @pcEntityID END)
                                                                                                  AND  at.fileID    = (CASE WHEN @pcFileID   = 'ALL' THEN at.fileID   ELSE @pcFileID   END)																								  
		                                                                                          AND (at.type = 'I' OR at.type  = 'R') 
		                                                                                          AND  at.transtype = 'F' 		    													                     
			    																	              GROUP BY at.entityID, at.fileID) s	 
          ON #artran.entityID = s.entityID AND (#artran.fileID = s.fileID OR ISNULL(#artran.fileID, s.fileID) IS NULL)
          LEFT JOIN agentfile
		  ON #artran.entityID = agentfile.agentID AND #artran.fileID = agentfile.fileID		  
	      WHERE #artran.type      = 'F'
	        AND #artran.transtype = 'F'
	        AND #artran.seq       =  0
		  
	  END /* IF @piBatchID <> 0 */
	  ELSE
	  BEGIN
	    /*-----------Include file and misc invoices transactions------------*/
	    INSERT INTO #artran (artranID,  revenueType,  tranID, seq, sourceType, sourceID, type, entity, entityID, entityName, fileNumber, fileID, tranAmt,
	                         remainingAmt, tranDate, void, voidDate, voidBy, notes, createdBy, createdDate, dueDate, fullyPaid, reference,
			     			 appliedAmt, transType, postBy, postDate, ledgerID, voidledgerID) 
						 
        SELECT artran.artranID, artran.revenueType, artran.tranID, artran.seq, artran.sourceType, artran.sourceID, artran.type, artran.entity,
	           artran.entityID, agent.name, artran.fileNumber, artran.fileID, artran.tranAmt, artran.remainingAmt, artran.tranDate, artran.void, artran.voidDate,
		       artran.voidBy, artran.notes, artran.createdBy, artran.createdDate, artran.dueDate, artran.fullyPaid, artran.reference, artran.appliedAmt,
		       artran.transType, artran.postBy, artran.postDate, artran.ledgerID, artran.voidledgerID
          FROM artran
		  INNER JOIN agent
		  ON artran.entityID = agent.agentID
          WHERE artran.entity    = 'A'
		    AND artran.entityID  = (CASE WHEN @pcEntityID = 'ALL' THEN artran.entityID  ELSE @pcEntityID END)
		    AND ((artran.type = 'F' AND artran.transtype = 'F' AND artran.fileID = (CASE WHEN @pcFileID = 'ALL' THEN artran.fileID ELSE @pcFileID END)) OR (artran.type = 'I' AND artran.transtype = ' ' AND artran.tranID = (CASE WHEN @pcInvoiceID = 'ALL' THEN artran.tranID ELSE @pcInvoiceID END)))  		       
			AND artran.seq       = 0
            AND (artran.void = 0 OR artran.void IS NULL)			  
		    AND artran.remainingAmt > 0	
        
		/* Calculate total amount of file type records */
	    UPDATE #artran
	      SET #artran.tranAmt = s.tranAmt,
		      #artran.arNotes = agentfile.arNotes
	      FROM #artran
	      INNER JOIN (SELECT at.entityID, at.fileID, SUM (at.tranAmt) AS tranAmt FROM artran at WHERE  at.entity    = 'A'
		                                                                                          AND  at.entityID  = (CASE WHEN @pcEntityID = 'ALL' THEN at.entityID ELSE @pcEntityID END)
                                                                                                  AND  at.fileID    = (CASE WHEN @pcFileID   = 'ALL' THEN at.fileID   ELSE @pcFileID   END)																								  
		                                                                                          AND (at.type = 'I' OR at.type  = 'R') 
		                                                                                          AND  at.transtype = 'F' 		    													                     
			    																	              GROUP BY at.entityID, at.fileID) s	 
          ON #artran.entityID = s.entityID AND (#artran.fileID = s.fileID OR ISNULL(#artran.fileID, s.fileID) IS NULL)
          LEFT JOIN agentfile
		  ON #artran.entityID = agentfile.agentID AND #artran.fileID = agentfile.fileID		  
	      WHERE #artran.type      = 'F'
	        AND #artran.transtype = 'F'
	        AND #artran.seq       =  0
			
	  END /* ELSE OF @piBatchID <> 0 */
	END /* IF @piReferenceID = 0 */
	ELSE
	BEGIN
	  IF @pcRevenueType <> 'ALL'
	  BEGIN
	    /*-----------Include file type transactions where payment/credit is applied------------*/
	    INSERT INTO #artran (artranID,  revenueType,  tranID, seq, sourceType, sourceID, type, entity, entityID, entityName, fileNumber, fileID, tranAmt,
	                         remainingAmt, appliedAmtByRef, tranDate, void, voidDate, voidBy, notes, createdBy, createdDate, dueDate, fullyPaid, reference,
			     			 appliedAmt, transType, postBy, postDate, ledgerID, voidledgerID, arNotes) 
						 
        SELECT artran.artranID, artran.revenueType, artran.tranID, artran.seq, artran.sourceType, artran.sourceID, artran.type, artran.entity,
	           artran.entityID, agent.name, artran.fileNumber, artran.fileID, s.tranAmt, (ISNULL(s.tranAmt,0) - ISNULL(s.appliedAmt,0)), s.appliedAmt, artran.tranDate, artran.void, artran.voidDate,
		       artran.voidBy, artran.notes, artran.createdBy, artran.createdDate, artran.dueDate, artran.fullyPaid, artran.reference, artran.appliedAmt,
		       artran.transType, artran.postBy, artran.postDate, artran.ledgerID, artran.voidledgerID, agentfile.arNotes
          FROM artran
		  INNER JOIN agent
		  ON artran.entityID = agent.agentID
		  INNER JOIN (SELECT at.entityID, 
		                     at.fileID, 
							 SUM (at.tranAmt) AS tranAmt, 
							 SUM (at.appliedAmt) AS appliedAmt 
							 FROM artran at 
							 WHERE at.entity       = 'A'
                               AND at.entityID     = (CASE WHEN @pcEntityID = 'ALL' THEN at.entityID ELSE @pcEntityID END)
                               AND at.fileID       = (CASE WHEN @pcFileID   = 'ALL' THEN at.fileID   ELSE @pcFileID   END)							   
		                       AND (at.type = 'I' OR at.type  = 'R') 
		                       AND  at.transtype   = 'F'
                               AND  at.revenueType = @pcRevenueType
			    			   GROUP BY at.entityID, at.fileID) s	 
          ON artran.entityID = s.entityID AND (artran.fileID = s.fileID OR ISNULL(artran.fileID, s.fileID) IS NULL)
		  LEFT JOIN agentfile
		  ON artran.entityID = agentfile.agentID AND artran.fileID = agentfile.fileID
          WHERE artran.entity    = 'A' 
		    AND artran.entityID  = (CASE WHEN @pcEntityID = 'ALL' THEN artran.entityID  ELSE @pcEntityID END)
			AND artran.fileID    = (CASE WHEN @pcFileID   = 'ALL' THEN artran.fileID    ELSE @pcFileID   END)			
		    AND artran.type      = 'F' 
		    AND artran.transtype = 'F' 
			AND artran.seq       = 0
            AND EXISTS(SELECT * FROM artran at WHERE  at.entity      = artran.entity
									              AND at.entityID    = artran.entityID
												  AND (at.fileID     = artran.fileID OR at.fileNumber = artran.fileNumber)
			                                      AND (at.type = 'I' OR at.type = 'R')									              
											      AND at.transtype   = 'F'												  
												  AND at.sourceType  = 'B'
											      AND at.sourceID    = (CASE WHEN @piBatchID = 0 THEN at.sourceID ELSE CAST(@piBatchID AS VARCHAR) END)
												  AND at.revenuetype = @pcRevenueType)
		    AND ISNULL((SELECT SUM(at.tranAmt) AS tranAmt FROM artran at WHERE  at.entity    = artran.entity
																			AND at.entityID  = artran.entityID
																			AND (at.fileID   = artran.fileID OR at.fileNumber = artran.fileNumber)
			                                                                AND at.type      = 'A'																			
																			AND at.transType = 'F'),0) = 0
		
        /*-----------Include misc type invoice transactions where payment/credit is applied------------*/		
		INSERT INTO #artran (artranID,  revenueType,  tranID, seq, sourceType, sourceID, type, entity, entityID, entityName, fileNumber, fileID, tranAmt,
	                         remainingAmt, appliedAmtByRef, tranDate, void, voidDate, voidBy, notes, createdBy, createdDate, dueDate, fullyPaid, reference,
			     			 appliedAmt, transType, postBy, postDate, ledgerID, voidledgerID) 
						 
        SELECT artran.artranID, artran.revenueType, artran.tranID, artran.seq, artran.sourceType, artran.sourceID, artran.type, artran.entity,
	           artran.entityID, agent.name, artran.fileNumber, artran.fileID, artran.tranAmt, artran.remainingAmt, ABS(s.tranAmt), artran.tranDate, artran.void, artran.voidDate,
		       artran.voidBy, artran.notes, artran.createdBy, artran.createdDate, artran.dueDate, artran.fullyPaid, artran.reference, artran.appliedAmt,
		       artran.transType, artran.postBy, artran.postDate, artran.ledgerID, artran.voidledgerID
          FROM artran
		  INNER JOIN agent
		  ON artran.entityID = agent.agentID
		  INNER JOIN (SELECT at.entityID, 
		                     at.tranID, 
							 SUM (at.tranAmt) AS tranAmt 
							 FROM artran at 
							 WHERE at.entity     = 'A'
                               AND at.entityID   = (CASE WHEN @pcEntityID  = 'ALL' THEN at.entityID ELSE @pcEntityID  END)
							   AND at.tranID     = (CASE WHEN @pcInvoiceID = 'ALL' THEN at.tranID   ELSE @pcInvoiceID END)
							   AND at.type       = 'A' 
		                       AND at.transtype  = ' ' 
						       AND at.sourceType = (CASE WHEN @pcType = 'ALL' THEN at.sourceType ELSE @pcType END)
	                           AND at.sourceID   = CAST(@piReferenceID as VARCHAR)
							   GROUP BY at.entityID, at.tranID) s	 
          ON artran.entityID = s.entityID AND artran.tranID = s.tranID	
          WHERE artran.entity      = 'A' 
		    AND artran.entityID    = (CASE WHEN @pcEntityID  = 'ALL' THEN artran.entityID  ELSE @pcEntityID  END)
            AND artran.tranID      = (CASE WHEN @pcInvoiceID = 'ALL' THEN artran.tranID    ELSE @pcInvoiceID END)			
		    AND artran.type        = 'I' 
		    AND artran.transtype   = ' ' 
		    AND artran.seq         = 0
			AND artran.revenuetype = @pcRevenueType
			AND (artran.void = 0 OR artran.void IS NULL)		    
            AND (artran.remainingAmt > 0 
			 OR ISNULL((SELECT SUM(at.tranAmt) AS tranAmt FROM artran at 
										                     WHERE at.entity     = artran.entity
															   AND at.entityID   = artran.entityID
															   AND at.tranID     = artran.tranID
															   AND at.type       = 'A'															    
															   AND at.transType  = ' '
															   AND at.sourceType = (CASE WHEN @pcType = 'ALL' THEN at.sourceType ELSE @pcType END)
															   AND at.sourceID   = CAST(@piReferenceID as VARCHAR)),0) <> 0)
	  END
	  ELSE
	  BEGIN
	    /*-----------Include file type transactions where payment/credit is applied------------*/
	    INSERT INTO #artran (artranID,  revenueType,  tranID, seq, sourceType, sourceID, type, entity, entityID, entityName, fileNumber, fileID, tranAmt,
	                         remainingAmt, appliedAmtByRef, tranDate, void, voidDate, voidBy, notes, createdBy, createdDate, dueDate, fullyPaid, reference,
			     		     appliedAmt, transType, postBy, postDate, ledgerID, voidledgerID, arNotes) 
						 
        SELECT artran.artranID, artran.revenueType, artran.tranID, artran.seq, artran.sourceType, artran.sourceID, artran.type, artran.entity,
	           artran.entityID, agent.name, artran.fileNumber, artran.fileID, s.tranAmt, artran.remainingAmt, ABS(ds.appliedAmtByRef), artran.tranDate, artran.void, artran.voidDate,
		       artran.voidBy, artran.notes, artran.createdBy, artran.createdDate, artran.dueDate, artran.fullyPaid, artran.reference, artran.appliedAmt,
		       artran.transType, artran.postBy, artran.postDate, artran.ledgerID, artran.voidledgerID, agentfile.arNotes
          FROM artran
		  INNER JOIN agent
		  ON artran.entityID = agent.agentID
		  INNER JOIN (SELECT at.entityID, 
		                     at.fileID, 
							 SUM (at.tranAmt) AS tranAmt 
							 FROM artran at 
							 WHERE at.entity     = 'A'
                               AND at.entityID   = (CASE WHEN @pcEntityID = 'ALL' THEN at.entityID ELSE @pcEntityID END)
                               AND at.fileID     = (CASE WHEN @pcFileID   = 'ALL' THEN at.fileID   ELSE @pcFileID   END)										   
		                       AND (at.type      = 'I' OR at.type  = 'R') 
		                       AND  at.transtype = 'F' 		    																							     
			    			   GROUP BY at.entityID, at.fileID) s	 
          ON artran.entityID = s.entityID AND (artran.fileID = s.fileID OR ISNULL(artran.fileID, s.fileID) IS NULL)		  
		  INNER JOIN (SELECT at.entityID, 
		                     at.fileID, 
							 SUM (at.tranAmt) AS appliedAmtByRef 
							 FROM artran at 
							 WHERE at.entity     = 'A'
                               AND at.entityID   = (CASE WHEN @pcEntityID = 'ALL' THEN at.entityID ELSE @pcEntityID END)
							   AND at.fileID     = (CASE WHEN @pcFileID   = 'ALL' THEN at.fileID   ELSE @pcFileID   END)			
		                       AND at.type       = 'A' 
		                       AND at.transtype  = 'F'
                               AND (at.tranID IS NULL OR at.tranID = '')																											 
							   AND at.sourceType = (CASE WHEN @pcType = 'ALL' THEN at.sourceType ELSE @pcType END)
	                           AND at.sourceID   = CAST(@piReferenceID as VARCHAR)
						       GROUP BY at.entityID, at.fileID) ds	 
          ON artran.entityID = ds.entityID AND (artran.fileID = ds.fileID OR ISNULL(artran.fileID, ds.fileID) IS NULL)
		  LEFT JOIN agentfile
		  ON artran.entityID = agentfile.agentID AND artran.fileID = agentfile.fileID
          WHERE artran.entity    = 'A' 
		    AND artran.entityID  = (CASE WHEN @pcEntityID = 'ALL' THEN artran.entityID  ELSE @pcEntityID  END)
			AND artran.fileID    = (CASE WHEN @pcFileID   = 'ALL' THEN artran.fileID    ELSE @pcFileID    END)			
		    AND artran.type      = 'F' 
		    AND artran.transtype = 'F' 
	        AND artran.seq       = 0
            AND ((@piBatchID > 0 AND 
			      EXISTS(SELECT * FROM artran at WHERE at.entity    = artran.entity
									               AND at.entityID   = artran.entityID
												   AND (at.fileID    = artran.fileID OR at.fileNumber = artran.fileNumber)
			                                       AND (at.type = 'I' OR at.type = 'R')									               
											       AND at.transtype  = 'F'												  
												   AND at.sourceType = 'B'
											       AND at.sourceID   = CAST(@piBatchID AS VARCHAR))) OR @piBatchID = 0)			
	        AND ISNULL((SELECT SUM(at.tranAmt) AS tranAmt FROM artran at 
										                    WHERE at.entity     = artran.entity
															  AND at.entityID   = artran.entityID
															  AND (at.fileID    = artran.fileID OR at.fileNumber = artran.fileNumber)
														      AND at.type       = 'A'															  
															  AND at.transType  = 'F'
															  AND at.sourceType = (CASE WHEN @pcType = 'ALL' THEN at.sourceType ELSE @pcType END)
															  AND at.sourceID   = CAST(@piReferenceID as VARCHAR)),0) <> 0
	  
	      /*-----------Include misc type invoice transactions where payment/credit is applied------------*/
	      INSERT INTO #artran (artranID,  revenueType,  tranID, seq, sourceType, sourceID, type, entity, entityID, entityName, fileNumber, fileID, tranAmt,
	                           remainingAmt, appliedAmtByRef, tranDate, void, voidDate, voidBy, notes, createdBy, createdDate, dueDate, fullyPaid, reference,
			     			   appliedAmt, transType, postBy, postDate, ledgerID, voidledgerID) 
						 
          SELECT artran.artranID, artran.revenueType, artran.tranID, artran.seq, artran.sourceType, artran.sourceID, artran.type, artran.entity,
	             artran.entityID, agent.name, artran.fileNumber, artran.fileID, artran.tranAmt, artran.remainingAmt, ABS(s.tranAmt), artran.tranDate, artran.void, artran.voidDate,
		         artran.voidBy, artran.notes, artran.createdBy, artran.createdDate, artran.dueDate, artran.fullyPaid, artran.reference, artran.appliedAmt,
		         artran.transType, artran.postBy, artran.postDate, artran.ledgerID, artran.voidledgerID
            FROM artran
		    INNER JOIN agent
		    ON artran.entityID = agent.agentID
		    INNER JOIN (SELECT at.entityID, 
		                       at.tranID, 
							   SUM (at.tranAmt) AS tranAmt 
							   FROM artran at 
							   WHERE at.entity     = 'A'
                                 AND at.entityID   = (CASE WHEN @pcEntityID  = 'ALL' THEN at.entityID ELSE @pcEntityID END)
								 AND at.tranID     = (CASE WHEN @pcInvoiceID = 'ALL' THEN at.tranID   ELSE @pcInvoiceID END)
							     AND at.type       = 'A' 
		                         AND at.transtype  = ' ' 
						         AND at.sourceType = (CASE WHEN @pcType = 'ALL' THEN at.sourceType ELSE @pcType END)
	                             AND at.sourceID   = CAST(@piReferenceID as VARCHAR)
							     GROUP BY at.entityID, at.tranID) s	 
            ON artran.entityID = s.entityID AND artran.tranID = s.tranID
            WHERE artran.entity    = 'A' 
		      AND artran.entityID  = (CASE WHEN @pcEntityID  = 'ALL' THEN artran.entityID  ELSE @pcEntityID  END)
              AND artran.tranID    = (CASE WHEN @pcInvoiceID = 'ALL' THEN artran.tranID    ELSE @pcInvoiceID END)			  
		      AND artran.type      = 'I' 
		      AND artran.transtype = ' ' 
		      AND artran.seq       = 0
			  AND (artran.void = 0 OR artran.void IS NULL)	
              AND ((@piBatchID > 0 AND artran.remainingAmt > 0) 
			   OR (ISNULL((SELECT SUM(at.tranAmt) AS tranAmt FROM artran at 
										                     WHERE at.entity     = artran.entity
															   AND at.entityID   = artran.entityID
															   AND at.tranID     = artran.tranID
														       AND at.type       = 'A'															    
															   AND at.transType  = ' '
															   AND at.sourceType = (CASE WHEN @pcType = 'ALL' THEN at.sourceType ELSE @pcType END)
															   AND at.sourceID   = CAST(@piReferenceID as VARCHAR)),0) <> 0))	
	  END
	END /* ELSE OF @piReferenceID = 0 */
						
	IF @piBatchID = 0
    BEGIN
	  UPDATE #artran
		SET	 
		 #artran.batchList = STUFF((SELECT DISTINCT ',' + arTran.sourceID FROM arTran 
									 WHERE  arTran.entity	  = #arTran.entity
									   AND  arTran.entityID   = #arTran.entityID
									   AND (arTran.fileID	  = #arTran.fileID or arTran.fileNumber = #arTran.fileNumber)
									   AND (arTran.type = 'I' or arTran.type = 'R') 
									   AND  arTran.transtype  = 'F'
									   AND  arTran.sourceType = 'B'									   									   
									   FOR XML PATH('')),1,1,''),
									   
         #artran.batchFileAmtList =  STUFF((SELECT ',' + arTran.sourceID + ',' + CAST(arTran.tranAmt as VARCHAR) 
		                              FROM (SELECT at.sourceID, SUM(at.tranAmt) AS tranAmt
									          FROM artran at 
											  WHERE  at.entity	   = #arTran.entity
									            AND  at.entityID   = #arTran.entityID 
												AND (at.fileID	   = #arTran.fileID or at.fileNumber = #arTran.fileNumber)
									            AND (at.type = 'I' or at.type = 'R') 
									            AND  at.transtype  = 'F'
									            AND  at.sourceType = 'B'									            
								                GROUP BY at.sourceID) arTran 									 									      
									   FOR XML PATH('')),1,1,'')									   									   
		FROM #artran
		WHERE #artran.type      = 'F'
		  AND #artran.transtype = 'F'
		  AND #artran.seq       =  0
    END
    ELSE
	BEGIN
	  UPDATE #artran
		SET	 							   
         #artran.batchFileAmtList =  STUFF((SELECT ',' + arTran.sourceID + ',' + CAST(arTran.tranAmt as VARCHAR) 
		                              FROM (SELECT at.sourceID, SUM(at.tranAmt) AS tranAmt
									          FROM artran at 
											  WHERE  at.entity	   = #arTran.entity
									            AND  at.entityID   = #arTran.entityID 
												AND (at.fileID	   = #arTran.fileID or at.fileNumber = #arTran.fileNumber)
									            AND (at.type = 'I' or at.type = 'R') 
									            AND  at.transtype  = 'F'
									            AND  at.sourceType = 'B'
												AND  at.sourceID   = CAST(@piBatchID AS VARCHAR)
												GROUP BY at.sourceID) arTran 									 									      
									   FOR XML PATH('')),1,1,'')									   									   
		FROM #artran
		WHERE #artran.type      = 'F'
		  AND #artran.transtype = 'F'
		  AND #artran.seq       =  0
	END
	
	SELECT * FROM #artran
END
GO