-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Shubham>
-- Create date: <07-02-2020>
-- Description:	<Get Deposit Payments Report>
-- =============================================
ALTER PROCEDURE [dbo].[spGetPayments] 
	-- Add the parameters for the stored procedure here          
    @depositID     integer      = 0,
	@postedPayment bit          = 0,
	@entity        varchar(1)   = 'A',
	@entityID      varchar(10)  = 'ALL',
	@fullyApplied  bit          = 0,
	@void          bit          = 0,
	@archived      bit          = 0,
	@searchString  varchar(MAX) = '',
	@fromDate      datetime     = null,
	@toDate        datetime     = null
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  DECLARE @searchAmt Decimal(18,2) = 0.00
  
  IF @searchString <> '' 
     SET @searchAmt = TRY_CAST(@searchString as decimal(18,2))

  -- Temporary table definition
  create table #ArPmt  (arPmtID       integer,
                        depositID     integer,        /* foreign key of arDeposit table */
                        revenuetype   varchar(50),    /* revenue type of payment */
                        entity        varchar(50),
					    entityID      varchar(50),
						tranID        VARCHAR(50),
						arTranID      VARCHAR(50),
					    checknum      varchar(50),
					    checkdate     datetime,
					    receiptdate   datetime,
					    checkamt      [decimal](18, 2) NULL,
					    remainingamt  [decimal](18, 2) NULL,
					    void          bit,
					    voiddate      datetime,
					    voidby        varchar(100),
						voidbyusername varchar(100),
					    description   varchar(max),
					    createddate   datetime,
					    createdby     varchar(100),
						username      varchar(100),
					    filenumber    varchar(100),
					    fileID        varchar(100),
						fileExist     varchar(3) default 'NO',
					    posted        bit,            /* true if the payment is posted */
					    transType     varchar(20),    /* 'F' if payment is file bed else '' */
					    archived      bit,
					    postdate      datetime,
					    postby        varchar(100),
						postbyname    varchar(100),
					    entityname    varchar(100),             /* Client side */
					    transDate     datetime,                /* Client side */
					    appliedamt    [decimal](18, 2) NULL,   /* Client side */
					    refundamt     [decimal](18, 2) NULL,   /* Client side */   
                        depositRef    varchar(50),
                        ledgerID      integer,
                        voidLedgerID  integer						
     		           )
						
  -- Insert statements for procedure here
  IF (@depositID > 0 AND @postedPayment = 1)
  BEGIN  
	/*-------Deposit Report for Posted Payments-----------*/
	insert into #ArPmt (arpmtID, depositID,revenuetype, entity, entityID, tranID ,artranID, checknum, checkdate, 
						receiptdate, checkamt , void, voiddate, voidby, description, 
						createddate, createdby, filenumber, fileID, posted, transType, archived, 
						postdate, postby, entityname, transDate, depositRef, ledgerID, voidledgerID)
	select ap.arpmtID, ap.depositID,ap.revenuetype, ap.entity, ap.entityID,at.tranID ,at.artranID, ap.checknum, ap.checkdate, 
			ap.receiptdate, ap.checkamt , ap.void, ap.voiddate, ap.voidby, ap.description, 
			ap.createddate, ap.createdby, ap.filenumber, ap.fileID, ap.posted, ap.transType, ap.archived, 
			ap.postdate, ap.postby, a.name, at.tranDate, ad.depositRef, ap.ledgerID, ap.voidLedgerID  
		from arpmt ap 
			inner join agent a 
		on  ap.EntityID = a.agentID 
			inner join artran at 
		on  ap.arpmtID = at.tranID and at.type = 'P' and seq = 0
			inner join ardeposit ad
		on ap.depositID = ad.depositID
		where ap.depositID  = @depositID
		  and ap.posted     = 1 
		  
    IF @archived = 1
    BEGIN
		/*-------Deposit Report for Posted Payments-----------*/
		insert into #ArPmt (arpmtID, depositID,revenuetype, entity, entityID, tranID ,artranID, checknum, checkdate, 
							receiptdate, checkamt , void, voiddate, voidby, description, 
							createddate, createdby, filenumber, fileID, posted, transType, archived, 
							postdate, postby, entityname, transDate, depositRef, ledgerID, voidledgerID)
		select ap.arpmtID, ap.depositID,ap.revenuetype, ap.entity, ap.entityID,at.tranID ,at.artranID, ap.checknum, ap.checkdate, 
				ap.receiptdate, ap.checkamt , ap.void, ap.voiddate, ap.voidby, ap.description, 
				ap.createddate, ap.createdby, ap.filenumber, ap.fileID, ap.posted, ap.transType, ap.archived, 
				ap.postdate, ap.postby, a.name, at.tranDate, ad.depositRef, ap.ledgerID, ap.voidLedgerID  
			from arpmt ap 
				inner join agent a 
			on  ap.EntityID = a.agentID 
				inner join artranh at 
			on  ap.arpmtID = at.tranID and at.type = 'P' and seq = 0
				inner join ardeposit ad
			on ap.depositID = ad.depositID
			where ap.depositID  = @depositID
			and ap.posted     = 1
			and ap.archived   = @archived
    END	
  END
  ELSE IF (@depositID > 0 AND @postedPayment = 0)
  BEGIN  
	/*-------Deposit Report for Unposted Payments-----------*/
	insert into #ArPmt (arpmtID, depositID,revenuetype, entity, entityID,checknum, checkdate, 
						receiptdate, checkamt , void, voiddate, voidby, description, 
						createddate, createdby, filenumber, fileID, posted, transType, archived, 
						postdate, postby, entityname, depositRef, ledgerID, voidLedgerID)
	select ap.arpmtID, ap.depositID,ap.revenuetype, ap.entity, ap.entityID, ap.checknum, ap.checkdate, 
			ap.receiptdate, ap.checkamt , ap.void, ap.voiddate, ap.voidby, ap.description, 
			ap.createddate, ap.createdby, ap.filenumber, ap.fileID, ap.posted, ap.transType, ap.archived, 
			ap.postdate, ap.postby, a.name, ad.depositRef, ap.ledgerID, ap.voidLedgerID   
		from arpmt ap 
			inner join agent a 
		on  ap.EntityID = a.agentID 
			inner join ardeposit ad
		on ap.depositID = ad.depositID
		where ap.depositID  = @depositID
		  and ap.posted     = 0 
		  
  END
  ELSE if @postedPayment = 0
  BEGIN
    /*-------Unposted Payments-----------*/	
	insert into #ArPmt (arpmtID, depositID,revenuetype, entity, entityID, tranID ,artranID, checknum, checkdate, 
						receiptdate, checkamt , void, voiddate, voidby, description, 
						createddate, createdby, filenumber, fileID, posted, transType, archived, 
						postdate, postby, entityname, transDate, depositRef)
	select ap.arpmtID, ap.depositID,ap.revenuetype, ap.entity, ap.entityID,at.tranID ,at.artranID,  ap.checknum, ap.checkdate, 
			ap.receiptdate, ap.checkamt , ap.void, ap.voiddate, ap.voidby, ap.description, 
			ap.createddate, ap.createdby, ap.filenumber, ap.fileID, ap.posted, ap.transType, ap.archived, 
			ap.postdate, ap.postby, a.name, at.tranDate, ad.depositRef  
		from arpmt ap 
			inner join agent a 
		on  ap.EntityID = a.agentID 
		    left outer join artran at 
		on  ap.arpmtID = at.tranID and at.type = 'P' and seq = 0
			left outer join ardeposit ad
		on ap.depositID = ad.depositID
		where ap.posted = 0 
		
  END
  ELSE
  BEGIN
    /*-------Posted/Refund Payments-----------*/
	insert into #ArPmt (arpmtID, depositID,revenuetype, entity, entityID, tranID ,artranID, checknum, checkdate, 
						receiptdate, checkamt , void, voiddate, voidby, description, 
						createddate, createdby, filenumber, fileID, posted, transType, archived, 
						postdate, postby, entityname, transDate, depositRef, ledgerID, voidLedgerID )
	select ap.arpmtID, ap.depositID,ap.revenuetype, ap.entity, ap.entityID,at.tranID ,at.artranID, ap.checknum, ap.checkdate, 
			ap.receiptdate, ap.checkamt , ap.void, ap.voiddate, ap.voidby, ap.description, 
			ap.createddate, ap.createdby, ap.filenumber, ap.fileID, ap.posted, ap.transType, ap.archived, 
			ap.postdate, ap.postby, a.name, at.tranDate, ad.depositRef, ap.ledgerID, ap.voidLedgerID   
		from arpmt ap 
			inner join agent a 
		on  ap.EntityID = a.agentID 
			inner join artran at 
		on  ap.arpmtID = at.tranID and at.type = 'P' and seq = 0 
			left outer join ardeposit ad
		on ap.depositID = ad.depositID
		where ap.entity    = @entity 
		  AND ap.entityID  = (CASE WHEN @entityID = 'ALL' THEN ap.entityID  ELSE @entityID END)
		  and (((@searchString <> '' and ad.depositRef LIKE '%' + @searchString + '%' ) or @searchString = '' )
		  or ((@searchString <> '' and ap.checknum LIKE '%' + @searchString + '%' ) or @searchString = '' )
		  or (( @searchAmt <> 0 and @searchAmt IS NOT NULL and ap.checkamt = @searchAmt ) or @searchAmt = 0 ))
		  AND ap.transDate >= (CASE WHEN @fromDate IS NULL THEN ap.transDate  ELSE @fromDate END) 
		  AND ap.transDate <= (CASE WHEN @toDate   IS NULL THEN ap.transDate  ELSE @toDate   END)
		  and ap.posted     = 1
		  AND ((@void = 0 AND (ap.void = 0 OR ap.void IS NULL)) OR @void = 1)
		  and ((@fullyApplied = 0 and at.remainingAmt > 0) or @fullyApplied = 1 )
		  
    if @archived = 1
	BEGIN
		/*-------Posted/Refund Payments-----------*/
		insert into #ArPmt (arpmtID, depositID,revenuetype, entity, entityID, tranID ,artranID, checknum, checkdate, 
							receiptdate, checkamt , void, voiddate, voidby, description, 
							createddate, createdby, filenumber, fileID, posted, transType, archived, 
							postdate, postby, entityname, transDate, depositRef, ledgerID, voidLedgerID )
		select ap.arpmtID, ap.depositID,ap.revenuetype, ap.entity, ap.entityID,at.tranID ,at.artranID, ap.checknum, ap.checkdate, 
				ap.receiptdate, ap.checkamt , ap.void, ap.voiddate, ap.voidby, ap.description, 
				ap.createddate, ap.createdby, ap.filenumber, ap.fileID, ap.posted, ap.transType, ap.archived, 
				ap.postdate, ap.postby, a.name, at.tranDate, ad.depositRef, ap.ledgerID, ap.voidLedgerID   
			from arpmt ap 
				inner join agent a 
			on  ap.EntityID = a.agentID 
				inner join artranh at 
			on  ap.arpmtID = at.tranID and at.type = 'P' and at.seq = 0 
				left outer join ardeposit ad
			on ap.depositID = ad.depositID
			where ap.entity    = @entity 
			AND ap.entityID  = (CASE WHEN @entityID = 'ALL' THEN ap.entityID  ELSE @entityID END)
			and (((@searchString <> '' and ad.depositRef LIKE '%' + @searchString + '%' ) or @searchString = '' )
			or ((@searchString <> '' and ap.checknum LIKE '%' + @searchString + '%' ) or @searchString = '' ))
			AND ap.transDate >= (CASE WHEN @fromDate IS NULL THEN ap.transDate  ELSE @fromDate END) 
			AND ap.transDate <= (CASE WHEN @toDate   IS NULL THEN ap.transDate  ELSE @toDate   END)
			and ap.posted     = 1
			AND ((@void = 0 AND (ap.void = 0 OR ap.void IS NULL)) OR @void = 1)
	END	  
  END
  
  UPDATE #ArPmt
     SET #ArPmt.appliedAmt = (SELECT SUM(atn.tranAmt) FROM artran atn WHERE atn.entity     = 'A'
																		and atn.entityID   = #ArPmt.entityID
																		and atn.sourceType = 'P'
																		and atn.sourceID   = CAST(#ArPmt.arTranID as varchar)
																		and atn.type       = 'A'
																	GROUP BY atn.entityID,atn.sourceID),
			#ArPmt.refundamt = (select sum(atn.tranAmt) from artran atn where atn.entity    = 'A'
																			and atn.entityID  = #ArPmt.entityID
																			and atn.tranID    = #ArPmt.tranID
																			and atn.type      = 'P'
																			and atn.seq       > 0
																	GROUP BY atn.entityID,atn.tranID)
  if @archived = 1
  BEGIN																	
	UPDATE #ArPmt
		SET #ArPmt.appliedAmt = (SELECT SUM(atn.tranAmt) FROM artranh atn WHERE atn.entity     = 'A'
																			and atn.entityID   = #ArPmt.entityID
																			and atn.sourceType = 'P'
																			and atn.sourceID   = CAST(#ArPmt.arTranID as varchar)
																			and atn.type       = 'A'
																		GROUP BY atn.entityID,atn.sourceID),
			#ArPmt.refundamt = (select sum(atn.tranAmt) from artranh atn where atn.entity      = 'A'
																			 and atn.entityID  = #ArPmt.entityID
																			 and atn.tranID    = #ArPmt.tranID
																			 and atn.type      = 'P'
																			 and atn.seq       > 0
																		GROUP BY atn.entityID,atn.tranID)								
  END
	UPDATE #ArPmt
     SET	#ArPmt.remainingAmt = #ArPmt.checkAmt + ISNULL(#ArPmt.appliedAmt,0)  + ISNULL(#ArPmt.refundamt,0),

            #ArPmt.username = (select sysuser.name from sysuser where sysuser.UID = #ArPmt.createdby),
		    #ArPmt.voidbyusername = (select sysuser.name from sysuser where sysuser.UID = #ArPmt.voidby),
			#ArPmt.postbyname = (select sysuser.name from sysuser where sysuser.UID = #ArPmt.postby),

	        #ArPmt.fileExist = (case when exists(select 1 from agentfile where agentfile.agentID = #ArPmt.entityID 
                                                                           and agentfile.fileID  = #ArPmt.fileID) then 'YES' else 'NO' end)  
																 
  select * from #ArPmt order by entityID
END


