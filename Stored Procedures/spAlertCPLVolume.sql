GO
/****** Object:  StoredProcedure [dbo].[spAlertCPLVolume]    Script Date: 7/5/2018 4:24:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spAlertCPLVolume]
	-- Add the parameters for the stored procedure here
  @agentID VARCHAR(30) = '',
  @user VARCHAR(100) = 'compass@alliantnational.com',
  @preview BIT = 0,
  @month1 INTEGER = 0,
  @month2 INTEGER = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  DECLARE  -- Used for the preview
          @source VARCHAR(30) = 'OPS',
          @processCode VARCHAR(30) = 'OPS01',
          @score DECIMAL(18,2) = 0,
          -- Notes for the alert note
          @note VARCHAR(MAX) = '',
          -- Used for last month's period
          @date DATETIME = NULL

  -- Set the note
  SET @note = 'The calculation is Two Months Ago CPL Use (' + [dbo].[FormatNumber] (@month2, 0) + ') - Last Months CPL Use (' + [dbo].[FormatNumber] (@month1, 0) + ') / Last Months CPL Use (' + [dbo].[FormatNumber] (@month1, 0) + ')'
  -- Set the score
  SET @score = @month2 - @month1
  IF (@month1 > 0)
    SET @score = @score / @month1
  -- Get the latest period end date
  SELECT @date = MAX([endDate]) FROM [dbo].[period] WHERE [active] = 0
  -- Insert alert or preview
  IF (@preview = 0)
    EXEC [dbo].[spInsertAlert] @source = @source, 
                               @processCode = @processCode, 
                               @user = @user, 
                               @agentID = @agentID, 
                               @score = @score,
                               @note = @note,
                               @effDate = @date

  SELECT  @agentID AS [agentID],
          @source AS [source],
          @processCode AS [processCode],
          [dbo].[GetAlertThreshold] (@processCode, @score) AS [threshold],
          [dbo].[GetAlertThresholdRange] (@processCode, @score) AS [thresholdRange],
          [dbo].[GetAlertSeverity] (@processCode, @score) AS [severity],
          @score AS [score],
          [dbo].[GetAlertScoreFormat] (@processCode, @score) AS [scoreDesc],
          @date AS [effDate],
          [dbo].[GetAlertOwner] (@processCode) AS [owner],
          @note AS [note]
END
