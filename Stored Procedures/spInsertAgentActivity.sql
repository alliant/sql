/****** Object:  StoredProcedure [dbo].[spInsertAgentActivity]    Script Date: 5/18/2018 1:11:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spInsertAgentActivity]
	-- Add the parameters for the stored procedure here
	@agentID VARCHAR(50) = '',
	@name VARCHAR(200) = '',
	@year INTEGER,
	@stat VARCHAR(50),
	@category VARCHAR(50),
	@type VARCHAR(50),
	@currentMonth INTEGER = 0,
	@month1 DECIMAL(18,2) = 0,
	@month2 DECIMAL(18,2) = 0,
	@month3 DECIMAL(18,2) = 0,
	@month4 DECIMAL(18,2) = 0,
	@month5 DECIMAL(18,2) = 0,
	@month6 DECIMAL(18,2) = 0,
	@month7 DECIMAL(18,2) = 0,
	@month8 DECIMAL(18,2) = 0,
	@month9 DECIMAL(18,2) = 0,
	@month10 DECIMAL(18,2) = 0,
	@month11 DECIMAL(18,2) = 0,
	@month12 DECIMAL(18,2) = 0,
	@isAdmin BIT = 0,
	@activityID INTEGER      OUTPUT,
	@isError    BIT = 0      OUTPUT,
	@errMsg     VARCHAR(500) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET ANSI_WARNINGS OFF;
	DECLARE @count   INTEGER     = 0,
			@stateID VARCHAR(20) = '',
			@transCount BIT = 0

	SET @errMsg = '' 
	SET @isError = 0

	IF (@agentID = '')
		SELECT @count = COUNT(*) FROM [dbo].[agentactivity] WHERE [year] = @year AND [type] = @type AND [category] = @category AND [name] = @name
	ELSE
		SELECT @count = COUNT(*) FROM [dbo].[agentactivity] WHERE [year] = @year AND [type] = @type AND [category] = @category AND [agentID] = @agentID
	
	BEGIN TRY
		IF(@@TRANCOUNT = 0)
		BEGIN
			BEGIN TRANSACTION
			SET @transCount = 1
		END

		IF (@count = 0)
		BEGIN
			SELECT @stateID=[stateID] FROM [dbo].[agent] WHERE [agentID] = @agentID
			EXEC [dbo].[spGetNextSequence] @keyType = 'agentActivity', @key = @activityID OUTPUT
			IF(@activityID = -1)
				RAISERROR('Sequence ''agentActivity'' is not defined. Please contact system administrator.', 16, 1)

			INSERT INTO [dbo].[agentactivity] 
			(
				[activityID],
				[name],
				[agentID],
				[stateID],
				[year],
				[stat],
				[category],
				[type],
				[month1],
				[month2],
				[month3],
				[month4],
				[month5],
				[month6],
				[month7],
				[month8],
				[month9],
				[month10],
				[month11],
				[month12]
			)
			VALUES 
			(
				@activityID,
				@name,
				@agentID,
				@stateID,
				@year,
				@stat,
				@category,
				@type,
				@month1,
				@month2,
				@month3,
				@month4,
				@month5,
				@month6,
				@month7,
				@month8,
				@month9,
				@month10,
				@month11,
				@month12
			)
		END
		ELSE
		BEGIN
			IF (@agentID = '')
				UPDATE  [dbo].[agentactivity]
				SET	[month1]  = CASE WHEN @currentMonth =  1 THEN @month1  ELSE [month1]  END,
					[month2]  = CASE WHEN @currentMonth =  2 THEN @month2  ELSE [month2]  END,
					[month3]  = CASE WHEN @currentMonth =  3 THEN @month3  ELSE [month3]  END,
					[month4]  = CASE WHEN @currentMonth =  4 THEN @month4  ELSE [month4]  END,
					[month5]  = CASE WHEN @currentMonth =  5 THEN @month5  ELSE [month5]  END,
					[month6]  = CASE WHEN @currentMonth =  6 THEN @month6  ELSE [month6]  END,
					[month7]  = CASE WHEN @currentMonth =  7 THEN @month7  ELSE [month7]  END,
					[month8]  = CASE WHEN @currentMonth =  8 THEN @month8  ELSE [month8]  END,
					[month9]  = CASE WHEN @currentMonth =  9 THEN @month9  ELSE [month9]  END,
					[month10] = CASE WHEN @currentMonth = 10 THEN @month10 ELSE [month10] END,
					[month11] = CASE WHEN @currentMonth = 11 THEN @month11 ELSE [month11] END,
					[month12] = CASE WHEN @currentMonth = 12 THEN @month12 ELSE [month12] END
				WHERE	[type] = @type
				AND     [category] = @category
				AND     [year] = @year
				AND     [name] = @name
			ELSE
				UPDATE  [dbo].[agentactivity]
				SET	[month1]  = CASE WHEN @currentMonth =  1 THEN @month1  ELSE [month1]  END,
					[month2]  = CASE WHEN @currentMonth =  2 THEN @month2  ELSE [month2]  END,
					[month3]  = CASE WHEN @currentMonth =  3 THEN @month3  ELSE [month3]  END,
					[month4]  = CASE WHEN @currentMonth =  4 THEN @month4  ELSE [month4]  END,
					[month5]  = CASE WHEN @currentMonth =  5 THEN @month5  ELSE [month5]  END,
					[month6]  = CASE WHEN @currentMonth =  6 THEN @month6  ELSE [month6]  END,
					[month7]  = CASE WHEN @currentMonth =  7 THEN @month7  ELSE [month7]  END,
					[month8]  = CASE WHEN @currentMonth =  8 THEN @month8  ELSE [month8]  END,
					[month9]  = CASE WHEN @currentMonth =  9 THEN @month9  ELSE [month9]  END,
					[month10] = CASE WHEN @currentMonth = 10 THEN @month10 ELSE [month10] END,
					[month11] = CASE WHEN @currentMonth = 11 THEN @month11 ELSE [month11] END,
					[month12] = CASE WHEN @currentMonth = 12 THEN @month12 ELSE [month12] END
				WHERE   [type] = @type
				AND     [category] = @category
				AND     [year] = @year
				AND     [agentID] = @agentID
		END

		IF(@transCount = 1)
			COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		SET @isError = ERROR_STATE()
		SET @errMsg  = ERROR_MESSAGE() 
		IF(@transCount = 1)
			ROLLBACK TRANSACTION		
		RETURN		 
	END CATCH

END
