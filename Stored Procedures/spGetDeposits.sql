
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Shubham>
-- Create date: <06-02-2020>
-- Description:	<Get Posted Deposit Report>
-- =============================================
ALTER PROCEDURE [dbo].[spGetDeposits] 
	-- Add the parameters for the stored procedure here
	@entityID         varchar(20) = '',
	@searchString     varchar(20) = '',
	@fromPostDate     datetime    = null,
	@toPostDate       datetime    = null,
    @includeVoid      bit,
    @includeArchived  bit	
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  -- Temporary table definition
  create table #ArDeposit  (depositID         integer,
                            bankID            varchar(50),
                            depositType       varchar(50),
                            depositDate       datetime,
							stat              varchar(50),
							depositRef        varchar(50),
							postedBy          varchar(50),
                            postDate          datetime,
							transDate         datetime,
                            amount            [decimal](18, 2) NULL,
                            pmtamount         [decimal](18, 2) NULL,
                            void              bit,
                            archived          bit,							
  							ledgerID          integer,
                            createDate        datetime,
			                createdBy         varchar(50),
							voidLedgerID      integer,
							voidedBy          varchar(50),
							voidDate          datetime
						   )
					  
  -- Insert statements for procedure here
					  
  /*-------Deposit Report for Posted Payments-----------*/			
  insert into #ArDeposit (depositID,  bankID,  depositType, depositDate, stat, depositRef, transDate, postedBy, postDate, amount, pmtamount, void, archived, ledgerID, createDate, createdBy, voidLedgerID, voidedBy, voidDate)
  select ad.depositID, ad.bankID, ad.depositType, ad.depositDate, ad.stat, ad.depositRef, ad.transdate, ad.postedBy, ad.postDate, ad.amount, s.checkamt, ad.void, ad.archived, ad.ledgerID, ad.createDate, ad.createdBy, ad.voidLedgerID, ad.voidedBy, ad.voidDate
    from ardeposit ad																																							   
	  left outer join (select ap.depositID , sum(ap.checkamt) as checkamt from arpmt ap where ap.posted   = 1 
	                                                                                     /* and ap.void     = 0 */
																						  group by ap.depositID) s
	  on s.depositID = ad.depositID
    where ad.posted        = 1
	  and ad.void          = (CASE WHEN @includeVoid  = 1 THEN ad.void ELSE 0 END)
	  and ad.archived      = 0
      and ad.transdate    >= (CASE WHEN @fromPostDate IS NULL THEN ad.transdate ELSE @fromPostDate END) 
	  and ad.transdate    <= (CASE WHEN @toPostDate IS NULL THEN ad.transdate ELSE @toPostDate END)	 

  if @includeArchived  = 1
  begin
   /*-------Deposit Report for Posted Payments-----------*/			
   insert into #ArDeposit (depositID,  bankID,  depositType, depositDate, stat, depositRef, transDate, postedBy, postDate, amount, pmtamount, void, archived, ledgerID, createDate, createdBy, voidLedgerID, voidedBy, voidDate)
   select ad.depositID, ad.bankID, ad.depositType, ad.depositDate, ad.stat, ad.depositRef, ad.transdate, ad.postedBy, ad.postDate, ad.amount, s.checkamt, ad.void, ad.archived, ad.ledgerID, ad.createDate, ad.createdBy, ad.voidLedgerID, ad.voidedBy, ad.voidDate
     from ardeposit ad																																							   
	   left outer join (select ap.depositID , sum(ap.checkamt) as checkamt from arpmt ap where ap.posted   = 1 
	                                                                                     /* and ap.void     = 0 */
																						  group by ap.depositID) s
	   on s.depositID = ad.depositID
     where ad.posted        = 1
	   and ad.void          = (CASE WHEN @includeVoid  = 1 THEN ad.void ELSE 0 END)
	   and ad.archived      = 1
       and ad.transdate    >= (CASE WHEN @fromPostDate IS NULL THEN ad.transdate ELSE @fromPostDate END) 
	   and ad.transdate    <= (CASE WHEN @toPostDate IS NULL THEN ad.transdate ELSE @toPostDate END)

  end  
	 		      
  select * from #ArDeposit where #ArDeposit.depositID IN (select arpmt.depositID from arpmt where arpmt.entityID = CASE WHEN @entityID = '' or @entityID = 'ALL' THEN arpmt.entityID ELSE @entityID END)
                              and (((@searchString <> '' and #ArDeposit.depositRef like '%' + @searchString + '%') 
							     or (@searchString = ''  and #ArDeposit.depositRef = #ArDeposit.depositRef))
                                 or #ArDeposit.depositID IN (select arpmt.depositID from arpmt where ((@searchString <> '' and arpmt.checknum like '%' + @searchString + '%')   
                                                                                                   or (@searchString  = '' and arpmt.checknum = arpmt.checknum))))
   order by depositID

END
GO
