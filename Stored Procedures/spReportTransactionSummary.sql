/****** Object:  StoredProcedure [dbo].[spReportTransactionSummary]    Script Date: 5/13/2022 4:11:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportTransactionSummary]
	-- Add the parameters for the stored procedure here
	@startPeriodID INTEGER = 0,
  @endPeriodID INTEGER = 0,
  @stateID VARCHAR(200) = 'ALL',
  @agentID VARCHAR(MAX) = 'ALL',
  @UID VARCHAR(100) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET @agentID = [dbo].[StandardizeAgentID](@agentID); 
	
	DECLARE @maxRow INTEGER,
			@index  INTEGER,
			@state  VARCHAR(2),
			@county VARCHAR(100),
			@origCounty VARCHAR(100)

	CREATE TABLE #temp
	(
		[stateID]   VARCHAR(2),
		[countyID]  VARCHAR(100),
		[formID]    VARCHAR(100),
		[statCode]  VARCHAR(100),
		[formType]  VARCHAR(100),
		[formCode]  VARCHAR(100),
		[formCount] INTEGER,
		[liabilityDelta] DECIMAL(17,2),
		[grossDelta]     DECIMAL(17,2),
		[netDelta]       DECIMAL(17,2),
		[retentionDelta] DECIMAL(17,2)
	)

	-- Get the state
	CREATE TABLE #stateTable ([stateID] VARCHAR(2))
	INSERT INTO #stateTable ([stateID])
	SELECT [field] FROM [dbo].[GetEntityFilter] ('State', @stateID)

	IF @startPeriodID = 0
		SELECT @startPeriodID = MAX([periodID]) FROM [dbo].[period] WHERE [active] = 0
	IF @endPeriodID = 0
		SELECT @endPeriodID = MAX([periodID]) FROM [dbo].[period] WHERE [active] = 0
  
	INSERT INTO #temp
	SELECT  b.[stateID],
			(CASE WHEN bf.[formType] = 'E' THEN bf1.countyID ELSE bf.[countyID] END) AS [countyID],		  
			bf.[formID],
			bf.[statCode],
			bf.[formType],
			(SELECT [formCode] FROM [dbo].[stateform] WHERE [stateID] = b.[stateID] AND [formID] = bf.[formID]) AS [formCode],
			COUNT(bf.[formID]) AS [formCount],
			SUM(CASE WHEN bf.[formType] = 'P' THEN bf.[liabilityDelta] ELSE 0 END) AS [liabilityDelta],
			SUM(bf.[grossDelta]) AS [grossDelta],
			SUM(bf.[netDelta]) AS [netDelta],
			SUM(bf.[retentionDelta]) AS [retentionDelta]
	FROM    [dbo].[batch] b INNER JOIN
			[dbo].[batchform] bf
	ON      b.[batchID] = bf.[batchID] INNER JOIN
			[dbo].[period] p
	ON      b.[periodID] = p.[periodID] INNER JOIN
			#stateTable s
	ON      b.[stateID] = s.[stateID] INNER JOIN
			[dbo].[batchform] bf1
	ON      bf1.batchID = bf.batchID and bf1.policyID = bf.policyID and bf1.fileNumber = bf.fileNumber and bf1.formType = 'P' 
	WHERE   p.[periodID] BETWEEN @startPeriodID AND @endPeriodID
	AND     b.[agentID] = (CASE WHEN @agentID != 'ALL' THEN @agentID ELSE b.[agentID] END) AND ([dbo].CanAccessAgent(@UID , b.[agentID]) = 1)
	GROUP BY b.[stateID],bf.[countyID],bf1.[countyID],bf.[formID],bf.[statCode],bf.[formType]

	SELECT  RowNum = ROW_NUMBER() OVER(order by stateID),* INTO #trxSummary FROM #temp

	SET @maxRow = (SELECT MAX([RowNum]) FROM #trxSummary)
	SET @index = (SELECT MIN([RowNum]) FROM #trxSummary)

	WHILE @index <= @maxRow
	BEGIN
		SELECT @state = [stateID], @county = [countyID] from #trxSummary WHERE [RowNum] = @index

		IF EXISTS(SELECT * FROM county WHERE county.stateID = @state and county.countyID = @county)
		BEGIN
			SET @origCounty = @county
		END
		ELSE IF EXISTS(SELECT * FROM county WHERE county.stateID = @state and county.description = @county)
		BEGIN
			SELECT @origCounty = county.countyID FROM county WHERE county.stateID = @state and county.description = @county
		END
		ELSE
		BEGIN
			SET @origCounty = @county
		END
		
		IF(@origCounty <> @county)
		BEGIN
			UPDATE #trxSummary
			SET #trxSummary.countyID = @origCounty FROM #trxSummary WHERE [RowNum] = @index 
		END

		SET @index = @index + 1
	END
  
    SELECT #trxSummary.stateID,
		   #trxSummary.countyID,
		   #trxSummary.formID,
		   #trxSummary.statCode,
		   #trxSummary.formType,
		   (SELECT [formCode] FROM [dbo].[stateform] WHERE [stateID] = #trxSummary.[stateID] AND [formID] = #trxSummary.[formID]) AS [formCode],
		   SUM(#trxSummary.[formCount]) AS [formCount],
		   SUM(CASE WHEN #trxSummary.[formType] = 'P' THEN #trxSummary.[liabilityDelta] ELSE 0 END) AS [liabilityDelta],
		   SUM(#trxSummary.[grossDelta]) AS [grossDelta],
		   SUM(#trxSummary.[netDelta]) AS [netDelta],
		   SUM(#trxSummary.[retentionDelta]) AS [retentionDelta]
	FROM #trxSummary GROUP BY #trxSummary.stateID, #trxSummary.countyID, #trxSummary.formID, #trxSummary.statCode, #trxSummary.formType 

	DROP TABLE #temp
	DROP TABLE #trxSummary


  --for each StateForm no-lock:
  --  create ttStateForm.
  --  buffer-copy stateForm to ttStateForm.
  --end.

  --std-in = 0.

  --for each period 
  --    where period.periodID >= sPeriodID
  --    and period.periodID <= ePeriodID
  --    no-lock,
  --  each batch
  --    where batch.periodID = period.periodID
  --    and (if tStateID = "ALL" then true else batch.stateID = tStateID)
  --    and (if tAgentID = "ALL" then true else batch.agentID = tAgentID)
  --    no-lock,
  --  each batchForm
  --    where batchForm.batchID = batch.batchID
  --    no-lock:

  --  find trxsummary
  --    where trxsummary.stateID = batch.stateID
  --    and trxsummary.formID = batchForm.formID
  --    and trxsummary.statCode = batchForm.statCode
  --    exclusive-lock no-error.
  --  if not avail trxsummary then
  --  do:
  --    create trxsummary.
  --    assign
  --      trxsummary.stateID = batch.stateID
  --      trxsummary.formID = batchform.formID
  --      trxsummary.statCode = batchForm.statCode
  --      trxsummary.formType = batchform.formType
  --      std-in = std-in + 1
  --      .
  
  --    for first ttStateForm
  --      where ttStateForm.stateID = batch.stateID
  --      and ttStateForm.formID = batchForm.formID
  --      no-lock:

  --      assign
  --        trxsummary.formCode = ttStateForm.formCode.
  --    end.
  --  end.

  --  if batchForm.formType = "P" then
  --  assign
  --    trxsummary.liabilityDelta = trxsummary.liabilityDelta + batchform.liabilityDelta.
  
  --  assign
  --    trxsummary.formCount = trxsummary.formCount + 1
  --    trxsummary.grossDelta = trxsummary.grossDelta + batchform.grossDelta
  --    trxsummary.netDelta = trxsummary.netDelta + batchform.netDelta
  --    trxsummary.retentionDelta = trxsummary.retentionDelta + batchform.retentionDelta.
  --end.
END

