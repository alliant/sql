USE [COMPASS]
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportDataCall]
	@startPeriodID INTEGER = 0,
  @endPeriodID INTEGER = 0,
  @stateID VARCHAR(200) = 'ALL',
  @agentID VARCHAR(MAX) = 'ALL',
  @ranges VARCHAR(500) = '4500,10000,20000,30000,40000,50000,60000,70000,80000,90000,100000,200000,300000,400000,500000,1000000,2000000,3000000,4000000,5000000,10000000,15000000,25000000,50000000,75000000,100000000',
  @UID VARCHAR(100) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   SET ANSI_WARNINGS OFF;
   SET @agentID = [dbo].[StandardizeAgentID](@agentID);
  -- Get the agent
  CREATE TABLE #agentTable ([agentID] VARCHAR(30))
  INSERT INTO #agentTable ([agentID])
  SELECT [agentID] from dbo.[agent] a WHERE (agentID = CASE WHEN @agentID != 'ALL' THEN @agentID ELSE a.[agentID] END) AND ([dbo].CanAccessAgent(@UID , a.[agentID]) = 1)
      
  -- Get the state
  CREATE TABLE #stateTable ([stateID] VARCHAR(2))
  INSERT INTO #stateTable ([stateID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('State', @stateID)

  DECLARE @minLiab DECIMAL(18,2) = 0,
          @maxLiab DECIMAL(18,2) = 0,
          @index INTEGER = 0

  IF @startPeriodID = 0
    SELECT @startPeriodID = MAX([periodID]) FROM [dbo].[period] WHERE [active] = 0
  IF @endPeriodID = 0
    SELECT @endPeriodID = MAX([periodID]) FROM [dbo].[period] WHERE [active] = 0

  CREATE TABLE #rangeTable
  (
    [seq] INTEGER,
    [description] VARCHAR(100),
    [minLiab] DECIMAL(18,2),
    [maxLiab] DECIMAL(18,2)
  )

  -- Insert the minimumm range
  INSERT INTO #rangeTable ([seq],[description],[minLiab],[maxLiab])
  SELECT 0, '<= $0', -999999999.99, 0
  -- Insert the range for endorsements with no policy
  INSERT INTO #rangeTable ([seq],[description],[minLiab],[maxLiab])
  SELECT 1, 'Zero (No Policy)', 0, 0
  -- Insert the ranges in the string
  WHILE @index < [dbo].[GetNumEntries] (@ranges, DEFAULT)
  BEGIN
    SET @index = @index + 1
    SET @minLiab = @maxLiab + 0.01
    SET @maxLiab = [dbo].[GetEntry] (@index, @ranges, DEFAULT)
    INSERT INTO #rangeTable ([seq],[description],[minLiab],[maxLiab])
    SELECT @index + 1, '$ ' + CONVERT(VARCHAR,@minLiab) + ' to ' + CONVERT(VARCHAR,@maxLiab), @minLiab, @maxLiab
  END
  -- Insert the top range
  SET @index = @index + 1
  SET @minLiab = @maxLiab + 0.01
  SET @maxLiab = 999999999.99
  INSERT INTO #rangeTable ([seq],[description],[minLiab],[maxLiab])
  SELECT @index + 1, '$ ' + CONVERT(VARCHAR,@minLiab) + '+', @minLiab, @maxLiab

  CREATE TABLE #policyTable
  (
    [policyID] INTEGER,
    [liability] DECIMAL(18,2)
  )

  -- Get the liability of the policy
  INSERT INTO #policyTable ([policyID],[liability])
  SELECT  bf.[policyID],
          SUM(bf.[liabilityDelta]) AS [liability]
  FROM    [batch] b INNER JOIN
          [batchform] bf
  ON      b.[batchID] = bf.[batchID]
  AND     b.[stat] = 'C' INNER JOIN
          [dbo].[period] p
  ON      b.[periodID] = p.[periodID]
  WHERE   b.[stateID] IN (SELECT [stateID] FROM #stateTable)
  AND     b.[agentID] IN (SELECT [agentID] FROM #agentTable)
  AND     p.[periodID] BETWEEN @startPeriodID AND @endPeriodID
  AND     bf.[formType] = 'P'
  GROUP BY bf.[policyID]

  CREATE TABLE #dataTable
  (
    [batchID] INTEGER,
    [periodID] INTEGER,
    [formType] VARCHAR(30),
    [statCode] VARCHAR(30),
    [policyID] INTEGER,
    [grossDelta] DECIMAL(18,2),
    [netDelta] DECIMAL(18,2),
    [retentionDelta] DECIMAL(18,2),
    [liabilityDelta] DECIMAL(18,2),
    [bucketLiability] DECIMAL(18,2)
  )
  
  -- Insert all batchforms
  INSERT INTO #dataTable ([batchID],[periodID],[formType],[statCode],[policyID],[grossDelta],[netDelta],[retentionDelta],[liabilityDelta],[bucketLiability])
  SELECT  bf.[batchID],
          b.[periodID],
          bf.[formType],
          bf.[statCode],
          bf.[policyID],
          bf.[grossDelta],
          bf.[netDelta],
          bf.[retentionDelta],
          bf.[liabilityDelta],
          po.[liability] AS [bucketLiability]
  FROM    [batch] b INNER JOIN
          [batchform] bf
  ON      b.[batchID] = bf.[batchID]
  AND     b.[stat] = 'C' INNER JOIN
          [dbo].[period] p 
  ON      b.[periodID] = p.[periodID] LEFT OUTER JOIN
          #policyTable po
  ON      bf.[policyID] = po.[policyID] INNER JOIN
          #agentTable a
  ON      b.[agentID] = a.[agentID] INNER JOIN
          #stateTable s
  ON      b.[stateID] = s.[stateID]
  WHERE   p.[periodID] BETWEEN @startPeriodID AND @endPeriodID
  
  -- Create a table to hold the end result table
  CREATE TABLE #exportTable
  (
    [seq] INTEGER,
    [description] VARCHAR(100),
    [minLiab] DECIMAL(18,2),
    [maxLiab] DECIMAL(18,2),
    [statCode] VARCHAR(100),
    [statDesc] VARCHAR(1000),
    [endorsementPremium] DECIMAL(18,2),
    [grossPremium] DECIMAL(18,2),
    [otherPremium] DECIMAL(18,2),
    [totalPremium] DECIMAL(18,2),
    [netPremium] DECIMAL(18,2),
    [retainedPremium] DECIMAL(18,2),
    [liabilityDelta] DECIMAL(18,2),
    [numForms] INTEGER,
    [numPolicies] INTEGER,
    [lowGross] DECIMAL(18,2),
    [highGross] DECIMAL(18,2),
    [lowNet] DECIMAL(18,2),
    [highNet] DECIMAL(18,2),
    [lowRetained] DECIMAL(18,2),
    [highRetained] DECIMAL(18,2),
    [lowLiability] DECIMAL(18,2),
    [highLiability] DECIMAL(18,2)
  )

  INSERT INTO #exportTable ([seq],[description],[minLiab],[maxLiab],[statCode],[statDesc],[endorsementPremium],[grossPremium],[otherPremium],[totalPremium],[netPremium],[retainedPremium],[liabilityDelta],[numForms],[numPolicies],[lowGross],[highGross],[lowNet],[highNet],[lowRetained],[highRetained],[lowLiability],[highLiability])
  -- The ranges with policies
  SELECT  r.[seq],
          r.[description],
          r.[minLiab],
          r.[maxLiab],
          r.[statCode],
          ISNULL((SELECT [description] FROM [dbo].[statcode] s WHERE s.[statCode] = r.[statCode] AND s.[stateID] = @stateID), ''),
          SUM(CASE WHEN d.[formType] = 'E' THEN d.[grossDelta] ELSE 0 END) AS [endorsementPremium],
          SUM(CASE WHEN d.[formType] = 'P' THEN d.[grossDelta] ELSE 0 END) AS [grossPremium],
          SUM(CASE WHEN d.[formType] <> 'E' AND d.[formType] <> 'P' THEN d.[grossDelta] ELSE 0 END) AS [otherPremium],
          ISNULL(SUM(d.[grossDelta]),0) AS [totalPremium],
          ISNULL(SUM(d.[netDelta]),0) AS [netPremium],
          ISNULL(SUM(d.[retentionDelta]),0) AS [retainedPremium],
          ISNULL(SUM(d.[liabilityDelta]),0) AS [amount],
          SUM(CASE WHEN d.[formType] = 'E' THEN 1 ELSE 0 END) AS [numForms],
          SUM(CASE WHEN d.[formType] = 'P' THEN 1 ELSE 0 END) AS [numPolicies],
          MIN(ISNULL(d.[grossDelta],0)) AS [lowGross],
          MAX(ISNULL(d.[grossDelta],0)) AS [highGross],
          MIN(ISNULL(d.[netDelta],0)) AS [lowNet],
          MAX(ISNULL(d.[netDelta],0)) AS [highNet],
          MIN(ISNULL(d.[retentionDelta],0)) AS [lowRetained],
          MAX(ISNULL(d.[retentionDelta],0)) AS [highRetained],
          MIN(ISNULL(d.[liabilityDelta],0)) AS [lowLiability],
          MAX(ISNULL(d.[liabilityDelta],0)) AS [highLiability]
  FROM    #dataTable d RIGHT OUTER JOIN
          (
          SELECT  [seq],
                  [description],
                  [minLiab],
                  [maxLiab],
                  [statCode]
          FROM    #dataTable CROSS JOIN
                  #rangeTable
          WHERE   [seq] <> 1
          GROUP BY [seq],[description],[minLiab],[maxLiab],[statCode]
          ) r
  ON      d.[statCode] = r.[statCode]
  AND     d.[bucketLiability] BETWEEN r.[minLiab] AND r.[maxLiab]
  AND     d.[bucketLiability] IS NOT NULL
  GROUP BY r.[seq],r.[description],r.[minLiab],r.[maxLiab],r.[statCode]
  UNION ALL
  -- Create the Total
  SELECT  r.[seq],
          r.[description],
          r.[minLiab],
          r.[maxLiab],
          'TOTAL' AS [statCode],
          '',
          SUM(CASE WHEN d.[formType] = 'E' THEN d.[grossDelta] ELSE 0 END) AS [endorsementPremium],
          SUM(CASE WHEN d.[formType] = 'P' THEN d.[grossDelta] ELSE 0 END) AS [grossPremium],
          SUM(CASE WHEN d.[formType] <> 'E' AND d.[formType] <> 'P' THEN d.[grossDelta] ELSE 0 END) AS [otherPremium],
          ISNULL(SUM(d.[grossDelta]),0) AS [totalPremium],
          ISNULL(SUM(d.[netDelta]),0) AS [netPremium],
          ISNULL(SUM(d.[retentionDelta]),0) AS [retainedPremium],
          ISNULL(SUM(d.[liabilityDelta]),0) AS [amount],
          SUM(CASE WHEN d.[formType] = 'E' THEN 1 ELSE 0 END) AS [numForms],
          SUM(CASE WHEN d.[formType] = 'P' THEN 1 ELSE 0 END) AS [numPolicies],
          MIN(ISNULL(d.[grossDelta],0)) AS [lowGross],
          MAX(ISNULL(d.[grossDelta],0)) AS [highGross],
          MIN(ISNULL(d.[netDelta],0)) AS [lowNet],
          MAX(ISNULL(d.[netDelta],0)) AS [highNet],
          MIN(ISNULL(d.[retentionDelta],0)) AS [lowRetained],
          MAX(ISNULL(d.[retentionDelta],0)) AS [highRetained],
          MIN(ISNULL(d.[liabilityDelta],0)) AS [lowLiability],
          MAX(ISNULL(d.[liabilityDelta],0)) AS [highLiability]
  FROM    #dataTable d RIGHT OUTER JOIN
          #rangeTable r
  ON      d.[bucketLiability] BETWEEN r.[minLiab] AND r.[maxLiab]
  WHERE   r.[seq] <> 1
  GROUP BY r.[seq],r.[description],r.[minLiab],r.[maxLiab]
  UNION ALL
  -- The ranges with no policies
  SELECT  r.[seq],
          r.[description],
          r.[minLiab],
          r.[maxLiab],
          r.[statCode],
          ISNULL((SELECT [description] FROM [dbo].[statcode] s WHERE s.[statCode] = r.[statCode] AND s.[stateID] = @stateID), ''),
          SUM(CASE WHEN d.[formType] = 'E' THEN d.[grossDelta] ELSE 0 END) AS [endorsementPremium],
          SUM(CASE WHEN d.[formType] = 'P' THEN d.[grossDelta] ELSE 0 END) AS [grossPremium],
          SUM(CASE WHEN d.[formType] <> 'E' AND d.[formType] <> 'P' THEN d.[grossDelta] ELSE 0 END) AS [otherPremium],
          ISNULL(SUM(d.[grossDelta]),0) AS [totalPremium],
          ISNULL(SUM(d.[netDelta]),0) AS [netPremium],
          ISNULL(SUM(d.[retentionDelta]),0) AS [retainedPremium],
          ISNULL(SUM(d.[liabilityDelta]),0) AS [amount],
          SUM(CASE WHEN d.[formType] = 'E' THEN 1 ELSE 0 END) AS [numForms],
          SUM(CASE WHEN d.[formType] = 'P' THEN 1 ELSE 0 END) AS [numPolicies],
          MIN(ISNULL(d.[grossDelta],0)) AS [lowGross],
          MAX(ISNULL(d.[grossDelta],0)) AS [highGross],
          MIN(ISNULL(d.[netDelta],0)) AS [lowNet],
          MAX(ISNULL(d.[netDelta],0)) AS [highNet],
          MIN(ISNULL(d.[retentionDelta],0)) AS [lowRetained],
          MAX(ISNULL(d.[retentionDelta],0)) AS [highRetained],
          MIN(ISNULL(d.[liabilityDelta],0)) AS [lowLiability],
          MAX(ISNULL(d.[liabilityDelta],0)) AS [highLiability]
  FROM    #dataTable d RIGHT OUTER JOIN
          (
          SELECT  [seq],
                  [description],
                  [minLiab],
                  [maxLiab],
                  [statCode]
          FROM    #dataTable CROSS JOIN
                  #rangeTable
          WHERE   [seq] = 1
          GROUP BY [seq],[description],[minLiab],[maxLiab],[statCode]
          ) r
  ON      d.[statCode] = r.[statCode]
  AND     d.[bucketLiability] IS NULL
  GROUP BY r.[seq],r.[description],r.[minLiab],r.[maxLiab],r.[statCode]
  UNION ALL
  -- Create the Total for endorsements with no policy
  SELECT  r.[seq],
          r.[description],
          r.[minLiab],
          r.[maxLiab],
          'TOTAL' AS [statCode],
          '',
          SUM(CASE WHEN d.[formType] = 'E' THEN d.[grossDelta] ELSE 0 END) AS [endorsementPremium],
          SUM(CASE WHEN d.[formType] = 'P' THEN d.[grossDelta] ELSE 0 END) AS [grossPremium],
          SUM(CASE WHEN d.[formType] <> 'E' AND d.[formType] <> 'P' THEN d.[grossDelta] ELSE 0 END) AS [otherPremium],
          ISNULL(SUM(d.[grossDelta]),0) AS [totalPremium],
          ISNULL(SUM(d.[netDelta]),0) AS [netPremium],
          ISNULL(SUM(d.[retentionDelta]),0) AS [retainedPremium],
          ISNULL(SUM(d.[liabilityDelta]),0) AS [amount],
          SUM(CASE WHEN d.[formType] = 'E' THEN 1 ELSE 0 END) AS [numForms],
          SUM(CASE WHEN d.[formType] = 'P' THEN 1 ELSE 0 END) AS [numPolicies],
          MIN(ISNULL(d.[grossDelta],0)) AS [lowGross],
          MAX(ISNULL(d.[grossDelta],0)) AS [highGross],
          MIN(ISNULL(d.[netDelta],0)) AS [lowNet],
          MAX(ISNULL(d.[netDelta],0)) AS [highNet],
          MIN(ISNULL(d.[retentionDelta],0)) AS [lowRetained],
          MAX(ISNULL(d.[retentionDelta],0)) AS [highRetained],
          MIN(ISNULL(d.[liabilityDelta],0)) AS [lowLiability],
          MAX(ISNULL(d.[liabilityDelta],0)) AS [highLiability]
  FROM    #dataTable d RIGHT OUTER JOIN
          (
          SELECT  [seq],
                  [description],
                  [minLiab],
                  [maxLiab],
                  [statCode]
          FROM    #dataTable CROSS JOIN
                  #rangeTable
          WHERE   [seq] = 1
          GROUP BY [seq],[description],[minLiab],[maxLiab],[statCode]
          ) r
  ON      d.[statCode] = r.[statCode]
  AND     d.[bucketLiability] IS NULL
  GROUP BY r.[seq],r.[description],r.[minLiab],r.[maxLiab]

  -- Return the export table
  SELECT  CASE WHEN @agentID = 'ALL' THEN @stateID ELSE @agentID END AS [ID],
          'S' AS [type],
          'S' AS [codeType],
          @startPeriodID AS [sPeriodID],
          @endPeriodID AS [ePeriodID],
          [description],
          [minLiab],
          [maxLiab],
          [statCode] AS [code],
          [statDesc] AS [codeDesc],
          [endorsementPremium],
          [grossPremium],
          [otherPremium],
          [totalPremium],
          [netPremium],
          [retainedPremium],
          [liabilityDelta] AS [amount],
          [numForms],
          [numPolicies],
          [lowGross],
          [highGross],
          [lowNet],
          [highNet],
          [lowRetained],
          [highRetained],
          [lowLiability],
          [highLiability],
          CASE WHEN [liabilityDelta] > 0 THEN [totalPremium] / ([liabilityDelta] / 1000) ELSE 0 END AS [grossRate],
          CASE WHEN [liabilityDelta] > 0 THEN [netPremium] / ([liabilityDelta] / 1000) ELSE 0 END AS [netRate]
  FROM    #exportTable
  ORDER BY [statCode],[seq]

  DROP TABLE #exportTable
  DROP TABLE #dataTable
  DROP TABLE #policyTable
  DROP TABLE #rangeTable
  DROP TABLE #stateTable
  DROP TABLE #agentTable
END
GO
