USE [compass]
GO
/****** Object:  StoredProcedure [dbo].[spCloseAgentFiles]    Script Date: 11/26/2019 11:24:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO 
-- =============================================
-- Author:		<Anjly>
-- Create date: <11-26-2019>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spCloseAgentFiles]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
 update agentfile 
  set agentfile.stat = 'C' , agentfile.closingdate = getdate() , agentfile.osAmt = 0 ,  agentfile.paidAmt = agentfile.invoiceAmt 
     where agentfile.fileid not in (select artran.fileid 
	                                   from artran
									    where artran.entityid = agentfile.agentid and 
										      artran.fileid   = agentfile.fileid )  	
END
