USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spCPLForUnrepFiles]    Script Date: 8/8/2019 12:21:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spCPLForUnrepFiles]
	-- Add the parameters for the stored procedure here  
  @agentID VARCHAR(MAX) = '',
  @fileNumber VARCHAR(MAX) = '',
  @StartDate DATE = NULL,
  @EndDate DATE = NULL  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	
  -- Insert statements for procedure here
  if (@agentID <> '')
  BEGIN
    SELECT cpl.stat,
	       cpl.cplID,
	       cpl.issueDate,
    	   cpl.fileNumber,		  
 		   cpl.grossPremium,
		   cpl.netPremium,
		   cpl.retentionPremium
		FROM dbo.cpl               
              WHERE cpl.agentID = @agentID
			  AND   cpl.issueDate BETWEEN @StartDate and @EndDate
			  AND   (cpl.fileNumber    LIKE '@fileNumber%' or
					 cpl. fileNumberID LIKE '@fileNumber%')			 					
			  AND   cpl.stat <> 'V'   		  		  
			  AND   cpl. fileNumberID NOT IN (select  fileNumberID FROM batchform where batchForm.batchID in
	     		  					       (select BatchID from batch where batch.agentID = @agentID
									                                   AND  batch.createDate >= @StartDate)
									       AND batchForm.formType = 'C')
  END
END
