USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spReportClaimCostsSummary]    Script Date: 6/24/2020 8:19:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportClaimCostsSummary]
	-- Add the parameters for the stored procedure here
	@agentID VARCHAR(30) = '',
  @endDate DATETIME = NULL,
  @year INT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  DECLARE @startDate DATETIME = NULL

  IF (@endDate IS NULL)
    SET @endDate = GETDATE()
	
  -- The end date should be the last second of the day
  SET @endDate = DATEADD(s,-1,DATEADD(d,1,@endDate))
  
  /* SELECT  @endDate = MAX(p.[endDate])
  FROM    [dbo].[period] p INNER JOIN
          [dbo].[batch] b
  ON      p.[periodID] = b.[periodID]
  AND     b.[stat] = 'C'
  WHERE   p.[endDate] <= @endDate
  AND     b.[agentID] = (CASE  WHEN @agentID != 'ALL' THEN @agentID ELSE [agentID] END)  */

  DECLARE @ClaimsRatio [dbo].[ClaimsRatio]
  SELECT * INTO #insertClaimsRatio FROM @ClaimsRatio
  SELECT * INTO #claimsRatio   FROM @ClaimsRatio
  SELECT * INTO #claimsRatioAE FROM @ClaimsRatio

  IF (@endDate IS NOT NULL)
  BEGIN
    IF (@year = 0)
    BEGIN
      --insert the ALL-Time row
      DELETE FROM #insertClaimsRatio
      EXEC [dbo].[spReportClaimsRatio] @endDate = @endDate, @agentError = 0
      INSERT INTO #claimsRatio
      SELECT * FROM #insertClaimsRatio WHERE [agentID] = (CASE  WHEN @agentID != 'ALL' THEN @agentID ELSE [agentID] END)

      --insert the ALL-Time row for agent error
      DELETE FROM #insertClaimsRatio
      EXEC [dbo].[spReportClaimsRatio] @endDate = @endDate, @agentError = 1
      INSERT INTO #claimsRatioAE
      SELECT * FROM #insertClaimsRatio WHERE [agentID] = (CASE  WHEN @agentID != 'ALL' THEN @agentID ELSE [agentID] END)
    END
    
    IF (@year = 1)
    BEGIN
      --insert the first year row
      SET @startDate = DATEADD(SECOND, 1, DATEADD(YEAR, -1, @endDate))
      DELETE FROM #insertClaimsRatio
      EXEC [dbo].[spReportClaimsRatio] @startDate = @startDate, @endDate = @endDate, @agentError = 0
      INSERT INTO #claimsRatio
      SELECT * FROM #insertClaimsRatio WHERE [agentID] = @agentID

      --insert the first year row for agent error
      DELETE FROM #insertClaimsRatio
      EXEC [dbo].[spReportClaimsRatio] @startDate = @startDate,  @endDate = @endDate, @agentError = 1
      INSERT INTO #claimsRatioAE
      SELECT * FROM #insertClaimsRatio WHERE [agentID] = @agentID
    END

    IF (@year = 2)
    BEGIN
      --insert the second year row
      SET @startDate = DATEADD(SECOND, 1, DATEADD(YEAR, -2, @endDate))
      DELETE FROM #insertClaimsRatio
      EXEC [dbo].[spReportClaimsRatio] @startDate = @startDate, @endDate = @endDate, @agentError = 0
      INSERT INTO #claimsRatio
      SELECT * FROM #insertClaimsRatio WHERE [agentID] = @agentID

      --insert the second year row for agent error
      DELETE FROM #insertClaimsRatio
      EXEC [dbo].[spReportClaimsRatio] @startDate = @startDate,  @endDate = @endDate, @agentError = 1
      INSERT INTO #claimsRatioAE
      SELECT * FROM #insertClaimsRatio WHERE [agentID] = @agentID
    END
    
    IF (@year = 3)
    BEGIN
      --insert the third year row
      SET @startDate = DATEADD(SECOND, 1, DATEADD(YEAR, -3, @endDate))
      DELETE FROM #insertClaimsRatio
      EXEC [dbo].[spReportClaimsRatio] @startDate = @startDate, @endDate = @endDate, @agentError = 0
      INSERT INTO #claimsRatio
      SELECT * FROM #insertClaimsRatio WHERE [agentID] = @agentID

      --insert the third year row for agent error
      DELETE FROM #insertClaimsRatio
      EXEC [dbo].[spReportClaimsRatio] @startDate = @startDate,  @endDate = @endDate, @agentError = 1
      INSERT INTO #claimsRatioAE
      SELECT * FROM #insertClaimsRatio WHERE [agentID] = @agentID
    END
  END

  SELECT  cr.[agentID],
          @year AS [period],
          @startDate AS [startDate],
          @endDate AS [endDate],
          cr.[netPremium],
          cr.[lossComplete] + cr.[laeComplete] - cr.[recoveries] AS [claimCosts],
          cr.[actualCostRatio] AS [costRatio],
          crAE.[lossComplete] + crAE.[laeComplete] - crAE.[recoveries] AS [claimCostsAE] ,
          cr.[actualCostAgentErrorRatio] AS  [costRatioAE] 
  FROM    #claimsRatio   cr   INNER JOIN
          #claimsRatioAE crAE
  ON      cr.[agentID] = crAE.[agentID]
END
