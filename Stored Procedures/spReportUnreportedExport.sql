GO
/****** Object:  StoredProcedure [dbo].[spReportUnremittedPolicies-details]    Script Date: 5/16/2023 11:12:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportUnreportedExport]
	-- Add the parameters for the stored procedure here
  @agentID VARCHAR(20) = '',
  @officeID INTEGER = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SELECT  p.[policyID],
          ag.[agentID], 
          ag.[name] AS [agentName],
          p.[companyID] AS [officeID],
          o.[name] AS [officeName],
          sc.[code] AS [regionID],
          sc.[description] AS [regionDesc], 
          su.[uid] AS [managerID],
          su.[name] AS [managerName],
          p.[stateID],
          (SELECT [description] FROM [dbo].[state] WHERE [stateID] = p.[stateID]) AS [stateDesc],
          [dbo].FormatAddress(o.[addr1], o.[addr2], '', '', o.[city], o.[state]) AS [officeAddress],
          p.[issueDate],
          p.[issuedLiabilityAmount] AS [liabilityAmount],
          p.[issuedGrossPremium] AS [grossPremium],
          p.[fileNumber],
          DATEDIFF(day,p.[issuedate],GETDATE()) AS [daysLate] 
  FROM    [dbo].[policy] p INNER JOIN
          [dbo].[office] o 
  ON      o.[officeID] = p.[companyID] INNER JOIN
          [dbo].[agent] ag 
  ON      ag.[agentID] = o.[agentID] 
  AND     ag.[stat] = 'A' LEFT OUTER JOIN 
          [dbo].[agentmanager] am  
  ON      ag.[agentID] = am.[agentID] 
  AND     am.[isPrimary] = 1 
  AND     am.[stat] = 'A' 
  AND     (am.[effDate] <= GETDATE() AND (am.[expDate] > GETDATE() OR am.[expDate] IS NULL )) LEFT OUTER JOIN 
          [dbo].sysuser su 
  ON      su.[uid] = am.[uid] LEFT OUTER JOIN
          [dbo].[syscode] sc 
  ON      sc.[codeType] = 'region'
  AND     sc.[code] = ag.[region]
  WHERE   NOT EXISTS
          (
          SELECT  bf.[policyID]
          FROM    [dbo].[batch] b INNER JOIN 
                  [dbo].[batchform] bf 
          ON      bf.[batchID] = b.[batchID] 
          AND     bf.[policyID] = p.[policyID] INNER JOIN 
                  [dbo].[stateform] sf 
          ON      sf.[stateID] = b.[stateID] 
          AND     sf.[formID] = bf.[formID]
          )
  AND     p.[issueDate] >= '01-01-2018' 
  AND     p.[stat] = 'I'
  AND     ag.[agentID] = CASE WHEN @agentID = '' THEN ag.[agentID] ELSE @agentID END
  AND     o.[officeID] = CASE WHEN @officeID = 0 THEN o.[officeID] ELSE @officeID END
  ORDER BY [daysLate] DESC

END
