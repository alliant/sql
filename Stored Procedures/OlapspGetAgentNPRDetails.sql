USE [COMPASS_ALFA]
GO
/****** Object:  StoredProcedure [dbo].[OlapspGetAgentNPRDetails]    Script Date: 6/23/2023 6:36:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[OlapspGetAgentNPRDetails]
	-- Add the parameters for the stored procedure here
  @year VARCHAR(4) = NULL,
  @month VARCHAR(2) = NULL,
  @olapDataSource varchar(100),
  @olapCatalog varchar(100),
  @UID VARCHAR(100) = ''
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
 
    declare @sql varchar(max)
    declare @yearname varchar(4)
    declare @monthname varchar(10)
    declare @lag varchar(4)
	declare @lag1 varchar(4)
	declare @lag2 varchar(4)
    declare @group varchar (100)
    declare @datasource varchar (100)
    declare @intialcatalog varchar (100)
	declare @netpremiumlabel varchar(20)

    create table  #tempdataset
    (
		AgentID varchar(20)   ,
		Name varchar(100),
		Stat varchar(10),
		State varchar(2),
		Manager varchar(30),
		Netpremium decimal(18,2),
		Netpremium1monthprev decimal(18,2),
		Netpremium2monthprev decimal(18,2),
		NetPlan decimal(18,2),
		NetpremiumdiffNetPlan decimal(18,2),
		NetpremiumYTD decimal(18,2),
		NetPlanYTD decimal(18,2),
		NetpremiumDiffplanYTD decimal(18,2),
		
    )

    -- enviroment variables
 --   set @datasource = 'polk'
 --   set @intialcatalog = 'Orbit'

    if @year is null 
		set @year = year(getdate())

    if @month is null
		set @month = month(getdate())

    set @lag = 0
	set @lag1 = 1
	set @lag2 = 2
    
   

    set @sql = 
    '
     insert into #tempdataset
      SELECT a."[Agent].[AgentID].[AgentID].[MEMBER_CAPTION]" as AgentID,
	   a."[Agent].[Agent].[Agent].[MEMBER_CAPTION]" as Name,
	   a."[Agent].[Status].[Status].[MEMBER_CAPTION]" as Stat,
	   a."[Agent].[State ID].[State ID].[MEMBER_CAPTION]" as State,
	   a."[Agent].[Manager].[Manager].[MEMBER_CAPTION]" as Manager ,
	   isnull(a."[Measures].[Net Premium]",0)  as Netpremium ,
 	   isnull(a."[Measures].[Current Month - 1]",0) as CurrentMonthLag1,
	   isnull(a."[Measures].[Current Month - 2]",0) as CurrentMonthLag2, 
	   isnull(a."[Measures].[Planned-NPR]",0)  as PlannedNPR,
	   isnull(a."[Measures].[NetpremiumdiffNetPlan]",0)  as NetpremiumdiffNetPlan ,
	   isnull(a."[Measures].[YTD]",0)  as YTD ,
	   isnull(a."[Measures].[YTD Plan]",0)  as YTDPlan,
	   isnull(a."[Measures].[YTD Actual - Plan]",0)  as YTDActualDiffPlan
	 FROM OpenRowset(''MSOLAP'',''DATASOURCE=' + @olapDataSource + '; Initial Catalog=' + @olapCatalog + ';'',
     '' WITH
			  MEMBER     [Measures].[Current Month - 1] AS  (    [Date].[Calendar].[Months].&[' + @month + ']&[' + @year + '].lag(' + @lag1 + ') ,  [Measures].[Net Premium]      ) 
			  MEMBER     [Measures].[Current Month - 2] AS  (    [Date].[Calendar].[Months].&[' + @month + ']&[' + @year + '].lag(' + @lag2 + ') ,  [Measures].[Net Premium]      )
			  MEMBER     [Measures].[NetpremiumdiffNetPlan] AS  ( [Measures].[Net Premium] -  [Measures].[Planned-NPR]     )
			  MEMBER     [Measures].[YTD] AS sum( PeriodsToDate( [Date].[Calendar].[Years], [Date].[Calendar].[Months].&[' + @month + ']&[' + @year + '] ) as periods , [Measures].[Net Premium])
			  MEMBER     [Measures].[YTD Plan] AS  sum( PeriodsToDate( [Date].[Calendar].[Years], [Date].[Calendar].[Months].&[' + @month + ']&[' + @year + '] ) as periods , [Measures].[Planned-NPR] )
			  MEMBER     [Measures].[YTD Actual - Plan] AS  ([Measures].[YTD] -    [Measures].[YTD Plan] )
			SELECT NON EMPTY { [Measures].[Net Premium] ,  [Measures].[Current Month - 1],   [Measures].[Current Month - 2], [Measures].[Planned-NPR], [Measures].[NetpremiumdiffNetPlan], [Measures].[YTD] , [Measures].[YTD Plan] ,  [Measures].[YTD Actual - Plan]   } ON COLUMNS, 
			NON EMPTY { ([Agent].[AgentID].[AgentID].ALLMEMBERS * [Agent].[Agent].[Agent].ALLMEMBERS * [Agent].[Status].[Status].ALLMEMBERS *  [Agent].[State ID].[State ID].ALLMEMBERS  *  [Agent].[Manager].[Manager].ALLMEMBERS ) } 
			DIMENSION PROPERTIES MEMBER_CAPTION, MEMBER_UNIQUE_NAME ON ROWS FROM [NPR]
			WHERE  [Date].[Calendar].[Months].&[' + @month + ']&[' + @year + '] 
        '' 
        ) as a 
    '


    Begin try
        EXEC (@sql)
    End try
    Begin catch
        select error_message()
        print 'No Data'
    End catch
    
	set @netpremiumlabel = @year + @month

    select 		@year Year,
				@month Month,
	            AgentID ,
		        td.Name    ,
		        stat   ,
		        td.State  ,
		        Manager ,
				Netpremium   ,
				Netpremium1monthprev ,
				Netpremium2monthprev ,
				NetPlan ,
				NetpremiumdiffNetPlan,
				NetpremiumYTD ,
				NetPlanYTD ,
				NetpremiumDiffplanYTD ,
				CASE WHEN fav.[sysFavoriteID] IS NOT NULL 
                THEN 'true'
                ELSE 'false'
                END   AS isFavorite,
		        s.description AS  stateName,
				Manager AS managerDesc,
				sp.objValue AS statDesc from #tempdataset td LEFT OUTER JOIN
               [sysfavorite] fav
    ON         td.[agentID] = fav.[entityID]
    AND        fav.[entity] = 'A'  
    AND        fav.[uid] = @UID LEFT OUTER JOIN
               [state] s 
    ON         s.stateID = td.State LEFT OUTER JOIN
               [sysprop] sp
	ON         sp.objID = td.Stat
	AND        sp.appCode = 'AMD'
    AND        sp.objAction = 'Agent'
	AND        sp.objProperty = 'Status'
		 		 where 
			  	( Netpremium <> 0 or Netpremium1monthprev <> 0 or  Netpremium2monthprev <> 0 or  NetpremiumYTD <> 0 or NetPlan <> 0 or  NetPlanYTD <> 0 or  
			 	 NetpremiumDiffplanYTD <> 0) 
		
    drop table #tempdataset
 
 END
