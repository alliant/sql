GO
/****** Object:  StoredProcedure [dbo].[spAlertERRScoreDrop]    Script Date: 12/27/2017 8:45:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spAlertERRScoreDrop]
  @agentID VARCHAR(MAX) = '',
  @user VARCHAR(100) = 'compass@alliantnational.com',
  @preview BIT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;
  
  CREATE TABLE #agentTable ([agentID] VARCHAR(30))
  INSERT INTO #agentTable ([agentID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)
  
  DECLARE @alertID INTEGER = 0,
          -- Used for the preview
          @source VARCHAR(30) = 'QAR',
          @processCode VARCHAR(30) = 'QAR03',
          @effDate DATETIME = NULL,
          @score DECIMAL(18,2) = 0,
          -- Notes for the alert note
          @note VARCHAR(MAX) = ''
          
  CREATE TABLE #scoreTable 
  (
    [agentID] VARCHAR(20),
    [qarDate] DATETIME,
    [errScore] INTEGER,
    [prevQarDate] DATETIME,
    [prevErrScore] INTEGER,
    [errScoreDrop] INTEGER
  )

  --Insert the current scores
  INSERT INTO #scoreTable ([agentID], [qarDate], [errScore],  [prevErrScore])
  SELECT  q.[agentID],
          q2.[qarDate],
          ISNULL(q.[score],0) AS [errScore],
          0
  FROM    [dbo].[qar] q INNER JOIN
          [dbo].[agent] a
  ON      q.[agentID] = a.[agentID] INNER JOIN
          (
          SELECT  [agentID],
                  MAX([auditFinishDate]) AS [qarDate]
          FROM    [dbo].[qar]
          WHERE   [stat] = 'C'
          GROUP BY [agentID]
          ) q2
  ON      q.[agentID] = q2.[agentID]
  AND     q.[auditFinishDate] = q2.[qarDate] 
  AND     q.[stat] = 'C'
  AND    (q.[auditType] = 'Q' OR q.[auditType] = 'E')
  AND     q.[agentID] IN (SELECT [agentID] FROM #agentTable)

  --Insert the previous scores
  MERGE INTO #scoreTable score
  USING   (
          SELECT  q.[agentID],
                  q2.[qarDate],
                  ISNULL(q.[score],0) AS [errScore]
          FROM    [dbo].[qar] q INNER JOIN
                  (
                  SELECT  [agentID],
                          MAX([auditFinishDate]) AS [qarDate]
                  FROM    [dbo].[qar] q
                  WHERE   [auditFinishDate] != (SELECT MAX([auditFinishDate]) FROM [dbo].[qar] WHERE [agentID] = q.[agentID])
                  GROUP BY [agentID]
                  ) q2
          ON      q.[agentID] = q2.[agentID]
          AND     q.[auditFinishDate] = q2.[qarDate] 
                  ) prev
  ON      score.[agentID] = prev.[agentID]
  WHEN MATCHED THEN
  UPDATE SET [prevQarDate] = prev.[qarDate],
             [prevErrScore] = prev.[errScore],
             [errScoreDrop] = prev.[errScore] - score.[errScore];
             
  UPDATE  #scoreTable
  SET     [errScoreDrop] = 0
  WHERE   [errScoreDrop] < 0
  OR      [errScoreDrop] IS NULL

  IF (@preview = 0)
  BEGIN
    -- ERR Score Drop
    DECLARE cur CURSOR LOCAL FOR
    SELECT  [agentID],
            ISNULL([errScoreDrop],0),
            [qarDate],
            'The calculation is Last Years ERR Score (' + [dbo].[FormatNumber] ([prevErrScore], 0) + ') - Current Years ERR Score (' + [dbo].[FormatNumber] ([errScore], 0) + ')'
    FROM    #scoreTable

    OPEN cur
    FETCH NEXT FROM cur INTO @agentID, @score, @effDate, @note
    WHILE @@FETCH_STATUS = 0 BEGIN
      EXEC [dbo].[spInsertAlert] @source = @source, 
                                  @processCode = @processCode, 
                                  @user = @user, 
                                  @agentID = @agentID, 
                                  @score = @score, 
                                  @effDate = @effDate,
                                  @note = @note
      FETCH NEXT FROM cur INTO @agentID, @score, @effDate, @note
    END
    CLOSE cur
    DEALLOCATE cur
  END
  
  SELECT  [agentID] AS [agentID],
          @source AS [source],
          @processCode AS [processCode],
          [dbo].[GetAlertThreshold] (@processCode, [errScoreDrop]) AS [threshold],
          [dbo].[GetAlertThresholdRange] (@processCode, [errScoreDrop]) AS [thresholdRange],
          [dbo].[GetAlertSeverity] (@processCode, [errScoreDrop]) AS [severity],
          [prevErrScore] - [errScore] AS [score],
          [dbo].[GetAlertScoreFormat] (@processCode, [errScoreDrop]) AS [scoreDesc],
          [qarDate] AS [effDate],
          [dbo].[GetAlertOwner] (@processCode) AS [owner],
          'The calculation is Last Years ERR Score (' + SUBSTRING(CONVERT(VARCHAR,CONVERT(money,[prevErrScore]), 1), 1, LEN(CONVERT(VARCHAR,CONVERT(money,[prevErrScore]), 1)) - 3) + ') - Current Years ERR Score (' + SUBSTRING(CONVERT(VARCHAR,CONVERT(money,[errScore]), 1), 1, LEN(CONVERT(VARCHAR,CONVERT(money,[errScore]), 1)) - 3) + ')' AS [note]
  FROM    #scoreTable

  DROP TABLE #agentTable
  DROP TABLE #scoreTable
END
