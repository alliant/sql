
/****** Object:  StoredProcedure [dbo].[spGetAllAgents]    Script Date: 11/20/2024 2:48:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetAllAgents]
	-- Add the parameters for the stored procedure here
   	@StateIDList VARCHAR(8000) = '',
	@StatusList  VARCHAR(15)  = '',
	@UID VARCHAR(100) = '',
	@OnlyProductionFiles BIT
AS
BEGIN  

	CREATE TABLE #tempAgent
	(
		[agentID]         VARCHAR(20),
		[stateID]         VARCHAR(2),
		[state]           VARCHAR(50),		
		[stat]            VARCHAR(1),
		[StatusDesc]      VARCHAR(40),
		[name]            VARCHAR(200),
		[legalName]       VARCHAR(200),
		[address]         VARCHAR(350),
		[addrState]       VARCHAR(2),
		[addrStateName]   VARCHAR(50),
		[region]          VARCHAR(50) ,
		[regionDesc]      VARCHAR(100),
		[corporationID]   VARCHAR(200),
		[manager]         VARCHAR(100),
		[managerDesc]     VARCHAR(100),
		[city]            VARCHAR(50)
	)

	if @OnlyProductionFiles = 1
	 begin
       INSERT INTO #tempAgent([agentID],[stateID],[state],[stat],[statusDesc],[name],[legalName],[address],[addrState],[addrStateName],[region],[regionDesc],[corporationID],[manager],[managerDesc],[city])
	   SELECT a.[agentID],
	          a.[stateID],
	       (SELECT [description] FROM [dbo].[state] WHERE [stateID] = a.[stateID]) AS [state],
	       a.[stat],
	       (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'AMD' AND [objAction] = 'agent' AND [objProperty] = 'Status' AND [objID] = a.[stat]) AS [statusDesc],
	       a.[name],
	       a.[legalName],
	       a.[addr1] + ' ' + a.[addr2] + ' ' + a.[addr3] + ' ' + a.[addr4] AS [address],
	       a.[state] AS [addrState],
	      (SELECT [description] FROM [dbo].[state] WHERE [stateID] = a.[state]) AS [addrStateName],
	       a.[region],
	      (select [description] from [dbo].[syscode] where [codeType] = 'Region' AND [code] = a.[region]) AS [regionDesc],
	     (CASE WHEN	a.[corporationID] <> '' THEN a.[corporationID] ELSE a.[legalname] END),
	   am.[uid] as manager,
	   s.[name],
	   a.[city]
	   FROM [dbo].[agent] a 
	   LEFT OUTER JOIN [dbo].[agentmanager] am /* to get manager */
       ON  a.[agentID] = am.[agentID]
	   AND am.[isPrimary] = 1
       AND am.[stat] = 'A' 
	   LEFT OUTER JOIN  /* to get managerdesc */
           (SELECT  [name],[UID] FROM  [dbo].[sysuser]) s
       ON   s.[UID] = am.[UID]
	   WHERE a.[agentID] IN (select agentID from [dbo].[office] where a.[agentID] = office.agentID)
	   AND ( a.[stat] IN (SELECT value FROM STRING_SPLIT(@StatusList,',')) OR @StatusList = '')
	   AND ( a.[stateID] IN (SELECT value FROM STRING_SPLIT(@StateIDList,',')) OR @StateIDList = '')
	   AND  (dbo.CanAccessAgent(@UID , a.[agentID]) = 1)
	  AND a.[agentID] IN (select distinct agentID from [dbo].[agentfile] af where a.[agentID] = af.agentID and (af.source = 'P' or af.source = 'Legacy'))
     end
	else 
	 begin
	   INSERT INTO #tempAgent([agentID],[stateID],[state],[stat],[statusDesc],[name],[legalName],[address],[addrState],[addrStateName],[region],[regionDesc],[corporationID],[manager],[managerDesc],[city])
	   SELECT a.[agentID],
	          a.[stateID],
	 	     (SELECT [description] FROM [dbo].[state] WHERE [stateID] = a.[stateID]) AS [state],
	 	     a.[stat],
	 	     (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'AMD' AND [objAction] = 'agent' AND [objProperty] = 'Status' AND [objID] = a.[stat]) AS [statusDesc],
	 	     a.[name],
	 	     a.[legalName],
	 	     a.[addr1] + ' ' + a.[addr2] + ' ' + a.[addr3] + ' ' + a.[addr4] AS [address],
	 	     a.[state] AS [addrState],
	 	    (SELECT [description] FROM [dbo].[state] WHERE [stateID] = a.[state]) AS [addrStateName],
	 	     a.[region],
	 	    (select [description] from [dbo].[syscode] where [codeType] = 'Region' AND [code] = a.[region]) AS [regionDesc],
	 	   (CASE WHEN	a.[corporationID] <> '' THEN a.[corporationID] ELSE a.[legalname] END),
	 		am.[uid] as manager,
	 		s.[name],
	 		a.[city]
	   FROM [dbo].[agent] a 
	   LEFT OUTER JOIN [dbo].[agentmanager] am /* to get manager */
       ON  a.[agentID] = am.[agentID]
	   AND am.[isPrimary] = 1
       AND am.[stat] = 'A' 
	   LEFT OUTER JOIN  /* to get managerdesc */
           (SELECT  [name],[UID] FROM  [dbo].[sysuser]) s
       ON   s.[UID] = am.[UID]
	   WHERE a.[agentID] IN (select agentID from [dbo].[office] where a.[agentID] = office.agentID)
	   AND ( a.[stat] IN (SELECT value FROM STRING_SPLIT(@StatusList,',')) OR @StatusList = '')
	   AND ( a.[stateID] IN (SELECT value FROM STRING_SPLIT(@StateIDList,',')) OR @StateIDList = '')
	   AND  (dbo.CanAccessAgent(@UID , a.[agentID]) = 1)
	 end

    SELECT * FROM #tempAgent

	DROP TABLE #tempAgent

END
