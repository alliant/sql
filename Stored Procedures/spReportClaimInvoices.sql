USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spReportClaimsInvoices]    Script Date: 12/14/2016 1:29:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportClaimInvoices]
	-- Add the parameters for the stored procedure here
	@startDate DATETIME = NULL,
  @endDate DATETIME = NULL,
  @category VARCHAR(1) = 'ALL',
  @status VARCHAR(1) = 'ALL'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  IF (@startDate IS NULL)
    SET @startDate = '2005-01-01'

  IF (@endDate IS NULL)
    SET @endDate = GETDATE()

  -- Get the category
  CREATE TABLE #categoryTable ([category] VARCHAR(10))
  IF (@category = 'ALL')
    INSERT INTO #categoryTable SELECT [refCategory] FROM [apinv] GROUP BY [refCategory]
  ELSE
    INSERT INTO #categoryTable SELECT @category

  -- Get the status
  CREATE TABLE #statusTable ([stat] VARCHAR(10))
  IF (@status = 'ALL')
    INSERT INTO #statusTable SELECT [stat] FROM [claim] GROUP BY [stat]
  ELSE
    INSERT INTO #statusTable SELECT @status

  -- invoice details
  SELECT  clm.[claimID],
          clm.[assignedTo],
          CASE WHEN trx.[transDate] IS NULL
            THEN DATEDIFF(day,inv.[invoiceDate],@endDate)
            ELSE DATEDIFF(day,inv.[invoiceDate],trx.[transDate])
          END AS 'age',
          trx.[transDate],
          dist.[acct],
          0,
          0
  FROM    [claim] clm INNER JOIN
          (
          SELECT  CONVERT(IINTEGER,inv.[refID]) AS 'claimID',
                  SUBSTRING(
                  (
                    SELECT  ', ' + dist.[acctName] AS [text()]
                    FROM    [apinvd] dist
                    WHERE   dist.[apinvID] = inv.[apinvID]
                    ORDER BY dist.[apinvID]
                    FOR XML PATH('')
                  ), 3, 1000) AS [acct]
          FROM    [apinv] inv
          GROUP BY dist2.[apinvID]
          ) dist
  ON      clm.[claimID] = dist.[claimID] LEFT OUTER JOIN
          (
          SELECT  res.[claimID],
                  res.[refCategory],
                  inv.[transDate],
                  SUM(res.[transAmount]) - SUM(ISNULL(inv.[amount],0)) AS 'reserve'
          FROM    [claimadjtrx] res LEFT OUTER JOIN
                  (
                  SELECT  CONVERT(INTEGER,inv.[refID]) AS 'claimID',
                          MAX(inv.[invoiceDate]) AS 'invoiceDate',
                          MAX(trx.[transDate]) AS 'transDate',
                          SUM(trx.[transAmount]) AS 'amount'
                  FROM    [apinv] inv LEFT OUTER JOIN
                          [aptrx] trx
                  ON      inv.[apinvID] = trx.[apinvID]
                  GROUP BY inv.[refID]
                  ) inv
          ON      res.[claimID] = inv.[claimID]
          GROUP BY res.[claimID],res.[refCategory],inv.[transDate]
          ) trx
  ON      clm.[claimID] = trx.[claimID]
END
