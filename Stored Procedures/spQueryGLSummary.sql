USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spQueryGLSummary]    Script Date: 4/7/2021 6:24:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Sachin Anthwal>
-- Create date: <21-01-2020>
-- Description:	<Get GL Summary Report>
-- =============================================
ALTER PROCEDURE [dbo].[spQueryGLSummary] 
	-- Add the parameters for the stored procedure here
    @batchid     integer  = 0,
	@posted      bit      = 0,
    @startDate   datetime = NULL,          
    @endDate     datetime = NULL    
AS
IF @batchid = 0 and @startDate IS NULL  and @endDate IS NULL 
   BEGIN    
       RETURN
   END  
ELSE 
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  -- Temporary table definition
  create table #GLperiodprocessinggl (batchid             integer,
                                      batchpostingdate    datetime,
                                      glDesc              varchar(500),
			     	                  glRef               varchar(500),
		                              debit               [decimal](18, 2) NULL,
                 	    			  credit              [decimal](18, 2) NULL
     		                         )
					  
  -- Insert statements for procedure here
  insert into #GLperiodprocessinggl (batchid, glRef, debit, credit, batchpostingdate)  
  select lg.sourceID , lg.accountID ,ISNULL( lg.debitamount,0), ISNULL(lg.creditamount,0), lg.transDate 
   from ledger lg 
   where lg.transdate >= (case when @startDate is null then lg.transdate else @startDate end) 
	 and lg.transdate <= (case when @endDate   is null then lg.transdate else @endDate   end)                         
	 and lg.sourceID in (select b.batchid from batch b 
		    			  where b.batchID     = (case when @batchid = 0 then b.batchID else @batchid end)
						  and ((@posted = 1 and b.posted = @posted))
						)
     and lg.source = 'B'
     and lg.notes not like 'History Transaction%'
									 
  select * from #GLperiodprocessinggl
  where debit <> 0 or credit <> 0
  order by batchid, glref
END
