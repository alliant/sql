USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spReportBusinessMix]    Script Date: 4/17/2017 11:50:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportBusinessMix]
	-- Add the parameters for the stored procedure here
	@startPeriodID INTEGER = NULL,
  @endPeriodID INTEGER = NULL,
  @stateID VARCHAR(200) = 'ALL',
  @agentID VARCHAR(MAX) = 'ALL',
  @range VARCHAR(500) = '4500,10000,20000,30000,40000,50000,60000,70000,80000,90000,100000,200000,300000,400000,500000,1000000,2000000,3000000,4000000,5000000,10000000,15000000,25000000,50000000,75000000,100000000'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  -- Get the agent
  CREATE TABLE #agentTable ([agentID] VARCHAR(30))
  INSERT INTO #agentTable ([agentID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)
  
  -- Get the state
  CREATE TABLE #stateTable ([stateID] VARCHAR(2))
  INSERT INTO #stateTable ([stateID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('State', @stateID)

  -- Used for the temp tables
  DECLARE @minLiab DECIMAL(18,2),
          @maxLiab DECIMAL(18,2),
          @Pos INTEGER,
          @Delimiter NCHAR(1) = ',',
          @validPeriod BIT = 0

  CREATE TABLE #mixTable (
    [type] VARCHAR(50),
    [ID] VARCHAR(50),
    [codeType] VARCHAR(50),
    [code] VARCHAR(50),
    [sPeriodID] INTEGER,
    [ePeriodID] INTEGER,
    [minLiab] DECIMAL(18,2),
    [maxLiab] DECIMAL(18,2),
    [description] VARCHAR(50),
    [numPolicies] INTEGER,
    [numForms] INTEGER,
    [grossPremium] DECIMAL(18,2),
    [endorsementPremium] DECIMAL(18,2),
    [otherPremium] DECIMAL(18,2),
    [totalPremium] DECIMAL(18,2),
    [netPremium] DECIMAL(18,2),
    [retainedPremium] DECIMAL(18,2),
    [amount] DECIMAL(18,2),
    [grossRate] DECIMAL(18,2),
    [netRate] DECIMAL(18,2),
    [lowGross] DECIMAL(18,2),
    [highGross] DECIMAL(18,2),
    [lowNet] DECIMAL(18,2),
    [highNet] DECIMAL(18,2),
    [lowRetained] DECIMAL(18,2),
    [highRetained] DECIMAL(18,2),
    [lowLiability] DECIMAL(18,2),
    [highLiability] DECIMAL(18,2)
  )

  -- validate end period
  SELECT  @validPeriod=COUNT([periodID])
  FROM    [dbo].[period]
  WHERE   [periodID] = @endPeriodID

  IF (@validPeriod = 0)
  BEGIN
    SELECT  @endPeriodID=MAX([periodID])
    FROM    [dbo].[period]
  END
  -- validate start period
  SELECT  @validPeriod=COUNT([periodID])
  FROM    [dbo].[period]
  WHERE   [periodID] = @startPeriodID

  IF (@validPeriod = 0)
  BEGIN
    SELECT  @startPeriodID=MAX([periodID])
    FROM    [dbo].[period]
    WHERE   [periodID] <> @endPeriodID
  END

  -- Set the ranges
  -- Less than zero liability
  SET @minLiab = -999999999.99
  SET @maxLiab = -0.01
  --INSERT INTO #mixTable
  SELECT  [type],
          [ID],
          [codeType],
          [code],
          [sPeriodID],
          [ePeriodID],
          [minLiab],
          [maxLiab],
          [description],
          SUM([numPolicies]) AS [numPolicies],
          SUM([numForms]) AS [numForms]
  FROM    (
          SELECT  CASE WHEN @agentID <> 'ALL' THEN 'A' ELSE 'S' END AS [type],
                  CASE WHEN @agentID <> 'ALL' THEN agentTable.[agentID] ELSE stateTable.[stateID] END AS [ID],
                  'F' AS [codeType],
                  bf.[formType] AS [code],
                  @startPeriodID AS [sPeriodID],
                  @endPeriodID AS [ePeriodID],
                  @minLiab AS [minLiab], 
                  @maxLiab AS [maxLiab], 
                  '< $0' AS [description],
                  SUM(CASE WHEN bf.[formType]  = 'P' THEN 1 ELSE 0 END) AS [numPolicies],
                  SUM(CASE WHEN bf.[formType] <> 'P' THEN 1 ELSE 0 END) AS [numForms]
          FROM    [dbo].[batch] b INNER JOIN
                  [dbo].[batchform] bf
          ON      b.[batchID] = bf.[batchID] INNER JOIN
                  [dbo].[period] p
          ON      b.[periodID] = p.[periodID] INNER JOIN
                  #agentTable agentTable
          ON      b.[agentID] = agentTable.[agentID] INNER JOIN
                  #stateTable stateTable
          ON      b.[stateID] = stateTable.[stateID]
          WHERE   b.[stat] = 'C'
          AND     p.[periodID] BETWEEN @startPeriodID AND @endPeriodID
          GROUP BY stateTable.[stateID],agentTable.[agentID],bf.[formType],bf.[policyID]
          HAVING  MAX(bf.[liabilityDelta]) BETWEEN @minLiab AND @maxLiab
          ) a
  GROUP BY [type],
           [ID],
           [codeType],
           [code],
           [sPeriodID],
           [ePeriodID],
           [minLiab],
           [maxLiab],
           [description]
  -- Zero liability
  -- More than zero liability
  SET @range = @range + @Delimiter
  SET @Pos = CHARINDEX(@Delimiter, @range)
  SET @minLiab = 0.01
  WHILE (@Pos <> 0)
	BEGIN
    SET @maxLiab = CONVERT(DECIMAL(18,2),SUBSTRING(@range,1,@Pos-1))

    --INSERT INTO #mixTable
    --SELECT  stateTable.[stateID],
    --        agentTable.[agentID],
    --        bf.[formType],
    --        ISNULL(bf.[policyID],0) AS 'policyID',
    --        SUM(ISNULL(bf.[liabilityDelta],0)) AS 'liabilityDelta',
    --        @minLiab, 
    --        @maxLiab, 
    --        '$ ' + convert(varchar, convert(money, @minLiab), 1) + ' to ' + '$ ' + convert(varchar, convert(money, @maxLiab), 1)
    --FROM    [dbo].[batch] b INNER JOIN
    --        [dbo].[batchform] bf
    --ON      b.[batchID] = bf.[batchID] INNER JOIN
    --        [dbo].[period] p
    --ON      b.[periodID] = p.[periodID] INNER JOIN
    --        #agentTable agentTable
    --ON      b.[agentID] = agentTable.[agentID] INNER JOIN
    --        #stateTable stateTable
    --ON      b.[stateID] = stateTable.[stateID]
    --WHERE   b.[stat] = 'C'
    --AND     p.[periodID] BETWEEN @startPeriodID AND @endPeriodID
    --GROUP BY stateTable.[stateID],agentTable.[agentID],bf.[policyID]
    --HAVING  SUM(bf.[liabilityDelta]) BETWEEN @minLiab AND @maxLiab

		SET @range = SUBSTRING(@range,@Pos+1,LEN(@range))
		SET @Pos = CHARINDEX(@Delimiter, @range)
    SET @minLiab = @maxLiab + 0.01
	END

  DROP TABLE #agentTable
  DROP TABLE #stateTable
  DROP TABLE #mixTable
END
