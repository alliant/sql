GO
/****** Object:  StoredProcedure [dbo].[spAgentsFilter]    Script Date: 25-09-2020 13:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		VJ
-- Create date: 09-25-2019
-- Description:	spAgentfilter check user access and return agent table according 
-- Modification: 
-- =============================================
ALTER PROCEDURE [dbo].[spAgentFilter]
	-- Add the parameters for the stored procedure here
	@agentID VARCHAR(MAX) = '',
	@UID VARCHAR(100) = ''
	  
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;
  SET @agentID = [dbo].[StandardizeAgentID](@agentID);  
  
  CREATE TABLE #agent ([agentID] VARCHAR(50))

  --IF role equals to 'Administrator' then access all agent 
IF (@agentID = '' OR @agentID = 'ALL' OR @agentID is null) AND (
   (SELECT sysuser.role FROM sysuser WHERE sysuser.uid = @UID) LIKE '%' + 'Administrator' + '%' OR 
   (SELECT  sysprop.objValue FROM sysprop where sysprop.appCode = 'SYS' and sysprop.objAction = 'AgentFilter'  and sysprop.objID = @UID) = '' OR 
   (SELECT  sysprop.objValue FROM sysprop where sysprop.appCode = 'SYS' and sysprop.objAction = 'AgentFilter'  and sysprop.objID = @UID) = '*')
  INSERT INTO #agent  ([agentID]) Values( 'ALL')
ELSE
  BEGIN
    INSERT INTO #agent  
    SELECT  a.[agentID] FROM    [dbo].[agent] a 
    WHERE (a.[agentID] = CASE WHEN @agentID != 'ALL' THEN @agentID ELSE a.[agentID] END) AND ([dbo].CanAccessAgent(@UID , a.[agentID]) = 1)
    ORDER BY a.[agentID]
  END	

select [agentID] from  #agent
END
