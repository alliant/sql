/****** Object:  StoredProcedure [dbo].[spGetEmptyFiles]    Script Date: 10/27/2023 2:21:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author:		<S Chandu>
-- Create date: <20-04-2023>
-- Description:	<Get agentfile records>
-- =============================================
ALTER PROCEDURE [dbo].[spGetEmptyFiles]  
	-- Add the parameters for the stored procedure here
	    @pcAction  VARCHAR(2) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    
	-- Insert statements for procedure here

	-- Temporary table definition
    CREATE TABLE #searchagentfile (agentfileID       INTEGER,	
	                               agentName         VARCHAR(200),
		    		           	   fileID            VARCHAR(50),
						           fileNumber        VARCHAR(50),
						           propertyAddress   VARCHAR(500),
								   parcelID          VARCHAR(500),
								   book              VARCHAR(50),
								   pageNumber        VARCHAR(50),
								   condomium         VARCHAR(100),
								   subdivison        VARCHAR(250),
								   buyerDescription  VARCHAR(8000),
								   sellerDescription VARCHAR(8000)
								   )

    IF @pcAction = 'O'
     INSERT INTO #searchagentfile(agentfileID,agentName,fileID,fileNumber,propertyAddress,parcelID,book,pageNumber,condomium,subdivison,buyerDescription,sellerDescription)  					 	     	
      SELECT af.agentfileID,
	         a.name + ' (' + a.agentID + ')' AS agentName,
		     af.fileID,
		     af.fileNumber,
        	 TRIM(', ' FROM 
			 (CASE WHEN (fp.[addr1]      = '' OR fp.[addr1]    IS NULL)   THEN  '' ELSE fp.[addr1]   + ', ' END) + 
			 (CASE WHEN (fp.[addr2]      = '' OR fp.[addr2]    IS NULL)   THEN  '' ELSE fp.[addr2]   + ', ' END) +
			 (CASE WHEN (fp.[addr3]      = '' OR fp.[addr3]    IS NULL)   THEN  '' ELSE fp.[addr3]   + ', ' END) +
			 (CASE WHEN (fp.[addr4]      = '' OR fp.[addr4]    IS NULL)   THEN  '' ELSE fp.[addr4]   + ', ' END) +
			 (CASE WHEN (fp.[city]       = '' OR fp.[city]     IS NULL)   THEN  '' ELSE fp.[city]    + ', ' END) +
			 (CASE WHEN (fp.[stateid]    = '' OR fp.[stateid]  IS NULL)   THEN  '' ELSE fp.[stateid] + ', ' END) +
			 (CASE WHEN (fp.[zip]        = '' OR fp.[zip]      IS NULL)   THEN  '' ELSE fp.[zip]     + ', ' END) +
			 (CASE WHEN (c.[description] = '' OR c.description IS NULL)   THEN  '' ELSE c.description       END))
			  AS propertyAddress,
		     fp.parcelID,
             fp.book,
		     fp.pageNumber,
		     fp.condominum,
		     fp.subdivision,
		     af.buyerDescription,
			 af.sellerDescription
        FROM agentfile af
		INNER JOIN [dbo].[agent] a ON ( a.agentID = af.agentID and (a.stat = 'A' or a.stat = 'P') )
		LEFT OUTER JOIN [dbo].[fileproperty] fp ON af.agentFileID = fp.agentfileid
		LEFT OUTER JOIN [dbo].[county] c ON c.stateID = fp.stateid and c.countyID = fp.countyid
        WHERE af.stage = '' 
		 AND NOT EXISTS(select agentFileID from job where job.agentfileid = af.agentFileID and ( job.stat = 'N' or job.stat = 'P' ) )
    ELSE IF @pcAction = 'T'
     INSERT INTO #searchagentfile(agentfileID,agentName,fileID,fileNumber,propertyAddress,parcelID,book,pageNumber,condomium,subdivison,buyerDescription,sellerDescription)  					 	     	
      SELECT af.agentfileID,
	         a.name + ' (' + a.agentID + ')' AS agentName,
		     af.fileID,
		     af.fileNumber,
        	 TRIM(', ' FROM 
			 (CASE WHEN (fp.[addr1]      = '' OR fp.[addr1]    IS NULL)   THEN  '' ELSE fp.[addr1]   + ', ' END) + 
			 (CASE WHEN (fp.[addr2]      = '' OR fp.[addr2]    IS NULL)   THEN  '' ELSE fp.[addr2]   + ', ' END) +
			 (CASE WHEN (fp.[addr3]      = '' OR fp.[addr3]    IS NULL)   THEN  '' ELSE fp.[addr3]   + ', ' END) +
			 (CASE WHEN (fp.[addr4]      = '' OR fp.[addr4]    IS NULL)   THEN  '' ELSE fp.[addr4]   + ', ' END) +
			 (CASE WHEN (fp.[city]       = '' OR fp.[city]     IS NULL)   THEN  '' ELSE fp.[city]    + ', ' END) +
			 (CASE WHEN (fp.[stateid]    = '' OR fp.[stateid]  IS NULL)   THEN  '' ELSE fp.[stateid] + ', ' END) +
			 (CASE WHEN (fp.[zip]        = '' OR fp.[zip]      IS NULL)   THEN  '' ELSE fp.[zip]     + ', ' END) +
			 (CASE WHEN (c.[description] = '' OR c.description IS NULL)   THEN  '' ELSE c.description       END))
			  AS propertyAddress,
		     fp.parcelID,
             fp.book,
		     fp.pageNumber,
		     fp.condominum,
		     fp.subdivision,
		     af.buyerDescription,
			 af.sellerDescription
        FROM agentfile af 
		INNER JOIN [dbo].[agent] a ON ( a.agentID = af.agentID and  (a.stat = 'A' or a.stat = 'P'))
		LEFT OUTER JOIN [dbo].[fileproperty] fp ON af.agentFileID = fp.agentfileid
		LEFT OUTER JOIN [dbo].[county] c ON c.stateID = fp.stateid and c.countyID = fp.countyid
        WHERE af.stage = '' 
		AND NOT EXISTS(select agentFileID from filetask where filetask.agentfileid = af.agentFileID)
		AND EXISTS(select agentfileiD from job where job.agentfileid = af.agentFileID and (job.stat = 'N' or job.stat = 'P'))
          
 	
    SELECT * FROM #searchagentfile

    DROP TABLE #searchagentfile
END 