
/****** Object:  StoredProcedure [dbo].[spAgentReviewCorrective]    Script Date: 6/28/2018 1:01:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spAgentReviewCorrective]
	-- Add the parameters for the stored procedure here
  @agentID VARCHAR(MAX) = 'ALL',
  @UID varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SET @agentID = [dbo].[StandardizeAgentID](@agentID)
  
  -- Insert statements for procedure here
  SELECT  f.[refID] AS [agentID],
          f.[findingID],
          a.[actionID],
          q.[qarID],
          a.[stat],
          '',  
          ISNULL(CASE WHEN f.[uid] = '' THEN NULL ELSE f.[uid] END,ISNULL(CASE WHEN a.[uid] = '' THEN NULL ELSE a.[uid] END,q.[uid])) AS [uid],
          ISNULL(f.[comments],'') AS [f],
          ISNULL(a.[comments],'') AS [a],
          ISNULL(f.[createdDate],q.[auditFinishDate]) AS [dateCreated],
          CONVERT(VARCHAR,a.[dueDate],120) AS [dateDue],
          CONVERT(VARCHAR,a.[completedDate],120) AS [dateCompleted]
  FROM    [dbo].[finding] f INNER JOIN
          [dbo].[action] a
  ON      f.[findingID] = a.[findingID] LEFT OUTER JOIN
          [dbo].[qar] q
  ON      f.[sourceID] = CONVERT(VARCHAR,q.[qarID])
  WHERE   f.[refType] = 'A'
  AND    (f.[refID] = (CASE  WHEN @agentID != 'ALL' THEN @agentID ELSE f.[refID] END)) 
  AND    (dbo.CanAccessAgent(@UID ,f.[refID]) = 1)  
  
END
