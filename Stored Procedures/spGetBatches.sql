USE [COMPASS]
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetBatches]
	-- Add the parameters for the stored procedure here
	@batchID INTEGER = 0,
  @periodID INTEGER = 0,
  @type VARCHAR(10) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  CREATE TABLE #formTable
  (
    [formType] VARCHAR(30)
  )

  IF @type = ''
  BEGIN
    INSERT INTO #formTable ([formType]) VALUES ('E')
    INSERT INTO #formTable ([formType]) VALUES ('P')
  END
  ELSE IF @type = 'ALL'
  BEGIN
    INSERT INTO #formTable ([formType])
    SELECT  [formType]
    FROM    [dbo].[batchform]
    GROUP BY [formType]
  END
  ELSE
  BEGIN
    INSERT INTO #formTable ([formType]) VALUES (@type)
  END

  IF @periodID = 0
    SELECT @periodID = [periodID] FROM [dbo].[period] WHERE [periodYear] = YEAR(GETDATE()) AND [periodMonth] = MONTH(GETDATE())

  SELECT  b.[batchID],
          b.[periodID],
          b.[yearID],
          b.[agentID],
          a.[stat] AS [agentStat],
          a.[name] AS [agentName],
          b.[stateID],
          b.[stateID] AS [state],
          b.[stat],
          b.[createDate],
          b.[receivedDate],
          b.[startDate],
          b.[duration],
          b.[endDate],
          b.[acctDate],
          ISNULL(bf.[liabilityAmount],0) AS [liabilityAmount],
          ISNULL(bf.[liabilityDelta],0) AS [liabilityDelta],
          ISNULL(b.[grossPremiumReported],0) AS [grossPremiumReported],
          ISNULL(bf.[grossPremiumProcessed],0) AS [grossPremiumProcessed],
          ISNULL(b.[grossPremiumReported],0) - ISNULL(bf.[grossPremiumProcessed],0) AS [grossPremiumDiff],
          ISNULL(bf.[grossPremiumDelta],0) AS [grossPremiumDelta],
          ISNULL(b.[netPremiumReported],0) AS [netPremiumReported],
          ISNULL(bf.[netPremiumProcessed],0) AS [netPremiumProcessed],
          ISNULL(b.[netPremiumReported],0) - ISNULL(bf.[netPremiumProcessed],0) AS [netPremiumDiff],
          ISNULL(bf.[netPremiumDelta],0) AS [netPremiumDelta],
          ISNULL(bf.[retainedPremiumProcessed],0) AS [retainedPremiumProcessed],
          ISNULL(bf.[retainedPremiumDelta],0) AS [retainedPremiumDelta],
          ISNULL(bf.[fileCount],0) AS [fileCount],
          ISNULL(bf.[policyCount],0) AS [policyCount],
          ISNULL(bf.[endorsementCount],0) AS [endorsementCount],
          ISNULL(bf.[cplCount],0) AS [cplCount],
          b.[periodMonth],
          b.[periodYear],
          b.[agentName],
          b.[reference],
          b.[cashReceived],
          b.[rcvdVia],
          b.[processStat],
          b.[invoiceDate],
          l.[uid] AS [lockedBy],
          l.[lockDate] AS [lockedDate],
          CASE WHEN d.[entityID] IS NOT NULL 
            THEN 'True'
            ELSE 'False'
          END AS [hasDocument]
  FROM    [dbo].[batch] b LEFT OUTER JOIN
          (
          SELECT  bf.[batchID],
                  SUM(bf.[liabilityAmount]) AS [liabilityAmount],
                  SUM(bf.[liabilityDelta]) AS [liabilityDelta],
                  SUM(bf.[grossPremium]) AS [grossPremiumProcessed],
                  SUM(bf.[grossDelta]) AS [grossPremiumDelta],
                  SUM(bf.[netPremium]) AS [netPremiumProcessed],
                  SUM(bf.[netDelta]) AS [netPremiumDelta],
                  SUM(bf.[retentionPremium]) AS [retainedPremiumProcessed],
                  SUM(bf.[retentionDelta]) AS [retainedPremiumDelta],
                  COUNT(bf.[batchID]) AS [fileCount],
                  SUM(CASE WHEN bf.[formType] = 'P' THEN 1 ELSE 0 END) AS [policyCount],
                  SUM(CASE WHEN bf.[formType] = 'E' THEN 1 ELSE 0 END) AS [endorsementCount],
                  SUM(CASE WHEN bf.[formType] = 'C' THEN 1 ELSE 0 END) AS [cplCount]
          FROM    [dbo].[batchform] bf
          WHERE   bf.[formType] IN (SELECT [formType] FROM #formTable)
          GROUP BY bf.[batchID]
          ) bf
  ON      b.[batchID] = bf.[batchID] LEFT OUTER JOIN
          [dbo].[sysdoc] d
  ON      d.[entityType] = 'Batch'
  AND     d.[entityID] = CONVERT(VARCHAR, b.[batchID]) LEFT OUTER JOIN
          [dbo].[syslock] l
  ON      l.[entityType] = 'Batch'
  AND     l.[entityID] = CONVERT(VARCHAR, b.[batchID]) INNER JOIN
          [dbo].[agent] a
  ON      b.[agentID] = a.[agentID]
  WHERE   b.[batchID] = CASE WHEN @batchID = 0 THEN b.[batchID] ELSE @batchID END
  AND     b.[periodID] = @periodID

  DROP TABLE #formTable
END
GO
