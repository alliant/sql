-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Alter PROCEDURE [dbo].[spGetSysActions]
	-- Add the parameters for the stored procedure here
	@ipActionType VARCHAR(1),
	@ipAction     VARCHAR(32)  = 'ALL',
	@ipStartDate  DATETIME     = NULL,
	@ipEndDate    DATETIME     = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    --Create temp table to store all uniqe Actions
	CREATE TABLE #ttAction (action VARCHAR(32))
	CREATE TABLE #ttAllAction (action VARCHAR(32))
    
   -- Return sysAction called with in given date range
	if (@ipActionType = 'A') 
	  BEGIN
	    --Insert all uniqe Actions in #ttAction table called with in date range 
	    INSERT INTO #ttAction SELECT DISTINCT syslog.action FROM syslog 
	                              WHERE syslog.createdate >= @ipStartDate 
	                                AND syslog.createdate <= @ipendDate 
	  END

   -- Return sysAction not called with in given date range
	ELSE IF(@ipActionType = 'I')
	  BEGIN
	  --Insert all uniqe Actions in #sysLog table as per selection criteria
	  INSERT INTO #ttAction SELECT DISTINCT sysLog.action FROM syslog 
	                               WHERE sysLog.createDate >= @ipStartDate 
	                                 AND sysLog.createDate <= @ipEndDate 

      INSERT INTO #ttAllAction SELECT sysaction.action from sysaction 
      -- Delete all records from #ttSysAction not persent in #sysLog
      DELETE #ttAllAction FROM #ttAllAction inner join #ttAction ON #ttAllAction.action = #ttAction.action

	  DELETE FROM #ttAction

	  INSERT INTO #ttAction SELECT #ttAllAction.action from  #ttAllAction

	 END
    -- Return sysAction as per search criteria
	 ELSE
	   BEGIN
	     INSERT INTO #ttAction SELECT sysaction.action from sysaction 
		                         WHERE sysaction.action = (CASE WHEN @ipAction  = 'ALL' THEN sysaction.action   ELSE @ipAction END)
	   END

    --Return all sysaction table records as per #sysLog table
    SELECT sysaction.action,
		   sysaction.description,
		   sysaction.progExec,
		   sysaction.isActive,
		   sysaction.isAnonymous,
		   sysaction.isSecure,
		   sysaction.isLog,
		   sysaction.isAudit,
		   sysaction.roles,
		   sysaction.userids,
		   sysaction.addrs,
		   sysaction.createDate,
		   sysaction.emails,
		   sysaction.emailSuccess,
		   sysaction.emailFailure,
		   sysaction.isEventsEnabled,
		   sysaction.comments,
		   sysaction.isQueueable,
		   sysaction.isDestination,
		   sysaction.name,
		   sysaction.htmlEmail,
		   sysaction.subject,
		   sysaction.progQueue,
		   sysaction.templateID
		   from sysaction join #ttAction on sysaction.action = #ttAction.action 
END
GO
