USE [COMPASS_ALFA]
GO
/****** Object:  StoredProcedure [dbo].[spGetStateCounties]    Script Date: 4/5/2023 12:26:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetStateCounties]
	-- Add the parameters for the stored procedure here
	@StateList VARCHAR(200) = ''
AS
BEGIN
	-- interfering with SELECT statements.
	IF @StateList <> ''
	  SELECT * FROM dbo.county c
			WHERE [stateID] in (SELECT value FROM STRING_SPLIT(@StateList,','))
	ELSE 
	  SELECT * FROM dbo.county c
END