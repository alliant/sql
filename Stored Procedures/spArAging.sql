USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spArAging]    Script Date: 2/11/2020 4:43:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spArAging]
	-- Add the parameters for the stored procedure here
	@stateID     VARCHAR(MAX) = 'ALL',
	@agentID     VARCHAR(MAX) = 'ALL'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	CREATE TABLE #agentTable ([agentID] VARCHAR(30))
    INSERT INTO #agentTable ([agentID])
    SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)

    CREATE TABLE #stateTable ([stateID] VARCHAR(2))
    INSERT INTO #stateTable ([stateID])
    SELECT [field] FROM [dbo].[GetEntityFilter] ('State', @stateID)


	CREATE TABLE #agentFile (agentID      VARCHAR(80),
	                         fileID   VARCHAR(50),
							 arAmount     [decimal](17, 2) NULL,
							 postedDate   DATETIME,
							 invoiced     [decimal](17, 2) NULL,
							 tranDate     datetime,
							 artranID     Integer,
							 tranID       VARCHAR(50),
							 type         VARCHAR(1))


	CREATE TABLE #arAging (stateID      VARCHAR(2),
	     			       agentID      VARCHAR(80),
						   orgID        VARCHAR(50),
						   name         VARCHAR(200),
						   managerID    integer,
						   manager      VARCHAR(200),
						   fileNumber   VARCHAR(50),
						   fileID       VARCHAR(50),
                           receiptDate  datetime,
						   postedDate   DATETIME,
						   invoiced     [decimal](17, 2) NULL, 
						   artranID     Integer,
						   stat         VARCHAR(1),
						   type         VARCHAR(1),
						   tranID       VARCHAR(50),
                           due0_30      [decimal](17, 2) NULL,
						   due31_60     [decimal](17, 2) NULL,
						   due61_90     [decimal](17, 2) NULL,
						   due91_       [decimal](17, 2) NULL,
						   balance      [decimal](17, 2) NULL)

     DECLARE -- Used for the preview
          @asOfDate DATETIME = CAST(CURRENT_TIMESTAMP AS DATETIME)

    /* Get all file and its remaining amount as of given date from artran */
	INSERT INTO #agentFile ([agentID],[fileID],arAmount,invoiced,type)
	SELECT at.entityID,
			at.fileID, 
			SUM(CASE When at.tranDate <= CAST(@asOfDate AS DATE)			         
					Then at.tranamt Else 0 End ), 
			SUM(CASE When (at.tranDate <= CAST(@asOfDate AS DATE))
					AND (at.type <> 'A')			         
					Then at.tranamt Else 0 End ),
			'A'		   
	FROM [dbo].[artran] at 
	inner join 
	[dbo].[agent] a 
	ON a.agentID = at.entityID
	WHERE (at.type = 'I' OR at.type = 'R' OR at.type = 'A')
		AND at.transType = 'F'
		AND at.[entityID] IN (SELECT [agentID] FROM #agentTable)
		and a.[stateID]   IN (SELECT [stateID] FROM #stateTable)
	GROUP BY at.entityID,at.fileID
	
	/* Set agent ID and tranDate for artran type 'F' */
	Update #agentFile SET tranDate   = at.tranDate,
						artranID   = at.arTranID,
						postedDate = at.postDate 
	FROM #agentFile
	INNER JOIN
	[dbo].[artran] at
	on  at.entityID   = #agentfile.agentID
	and at.fileID = #agentfile.fileID
	WHERE  at.type = 'F'
		and  at.seq  = 0
		and  #agentFile.type = 'A'
		
	
	INSERT INTO #agentFile ([agentID],[tranID],arAmount,[type])
    SELECT at.entityID,
	       at.tranID, 
	       SUM(CASE When at.tranDate <= CAST(@asOfDate AS DATE)			         
	                Then at.tranamt Else 0 End ),
		   'I'
	FROM [dbo].[artran] at 
	inner join 
	[dbo].[agent] a 
	ON a.agentID = at.entityID
    WHERE at.type = 'I'
	  AND at.transType = ''
	  AND (at.void = 0 or at.voidDate > CAST(@asOfDate AS DATE))
	  AND at.[entityID] IN (SELECT [agentID] FROM #agentTable)
	  and a.[stateID]   IN (SELECT [stateID] FROM #stateTable)
	GROUP BY at.entityID,at.tranID
	
	update #agentFile
	set #agentFile.artranID   = at.artranID,
	    #agentFile.tranDate   = at.tranDate,
	    #agentFile.postedDate = at.postDate, 
	    #agentFile.fileID     = at.reference
	from artran at
	where #agentFile.agentID = at.entityID
	  and #agentFile.tranID  = at.tranID
	  and #agentFile.type    = 'I'
	  and at.type = 'I'
	  and at.seq             = 0
	 
	update #agentFile
	set #agentFile.arAmount = #agentFile.arAmount + s.tranamt
	from #agentFile
	  INNER JOIN (select at.tranID, sum (at.tranAmt) as tranAmt from artran at where at.type = 'A' and at.tranDate <= CAST(@asOfDate AS DATE) group by at.tranID ) s	 
    ON #agentFile.tranID = s.tranID
	where #agentFile.type = 'I'

    /* Payment type records */ 
	INSERT INTO #agentFile ([agentID],[tranID],arAmount,[type])
      SELECT at.entityID, at.tranID, 
	         SUM(CASE When at.tranDate <= CAST(@asOfDate AS DATE)			         
		              Then at.tranamt Else 0 End ),
			'P'		   
	  FROM [dbo].[artran] at 
	  inner join 
	  [dbo].[agent] a 
	  ON a.agentID = at.entityID
      WHERE at.type = 'P'
	    AND (at.void = 0 or at.voidDate > CAST(@asOfDate AS DATE))
	    AND at.[entityID] IN (SELECT [agentID] FROM #agentTable)
		and a.[stateID]   IN (SELECT [stateID] FROM #stateTable)
	  GROUP BY at.entityID,at.tranID

    update #agentFile
	  set #agentFile.artranID   =  at.artranID,
	      #agentFile.tranDate   = at.tranDate,
		  #agentFile.postedDate = at.postDate, 
		  #agentFile.fileID = at.reference
	  from artran at
	  where #agentFile.agentID = at.entityID
	    and #agentFile.tranID  = at.tranID
	    and #agentFile.type    = 'P'
		and at.type = 'P'
		and at.seq             = 0

    update #agentFile
	  set #agentFile.arAmount = #agentFile.arAmount + s.tranamt
	  from #agentFile
	    INNER JOIN (select at.sourceID, sum (at.tranAmt) as tranAmt from artran at where at.type = 'A' and at.tranDate <= CAST(@asOfDate AS DATE) group by at.sourceID ) s	 
      ON #agentFile.artranID = s.sourceID
	  where #agentFile.type = 'P'
	  
    /* Credit type records */ 
	INSERT INTO #agentFile ([agentID],[tranID],arAmount,[type])
      SELECT at.entityID, at.tranID, 
	         SUM(CASE When at.tranDate <= CAST(@asOfDate AS DATE)			         
		              Then at.tranamt Else 0 End ),
			'C'		   
	  FROM [dbo].[artran] at 
	  inner join 
	  [dbo].[agent] a 
	  ON a.agentID = at.entityID
      WHERE at.type = 'C'
	    AND (at.void = 0 or at.voidDate > CAST(@asOfDate AS DATE))
	    AND at.[entityID] IN (SELECT [agentID] FROM #agentTable)
		and a.[stateID]   IN (SELECT [stateID] FROM #stateTable)
	  GROUP BY at.entityID,at.tranID

    update #agentFile
	  set #agentFile.artranID   =  at.artranID,
	      #agentFile.tranDate   = at.tranDate,
		  #agentFile.postedDate = at.postDate, 
		  #agentFile.fileID = at.reference
	  from artran at
	  where #agentFile.agentID = at.entityID
	    and #agentFile.tranID  = at.tranID
	    and #agentFile.type    = 'C'
		and at.type = 'C'
		and at.seq             = 0

    update #agentFile
	  set #agentFile.arAmount = #agentFile.arAmount + s.tranamt
	  from #agentFile
	    INNER JOIN (select at.sourceID, sum (at.tranAmt) as tranAmt from artran at where at.type = 'A' and at.tranDate <= CAST(@asOfDate AS DATE) group by at.sourceID ) s	 
      ON #agentFile.artranID = s.sourceID
	  where #agentFile.type = 'C'
      
    
	Delete #agentFile where arAmount = 0

	INSERT INTO #arAging([agentID],[type],[due0_30],[due31_60],[due61_90],[due91_])
	SELECT af.agentID,
	       af.type,
	       sum(Case when (@asOfDate -  af.tranDate) >= 0
	                and (@asOfDate -  af.tranDate) <= 30  
		 	   	     Then af.arAmount else 0 END) as [due0_30], 

 	       sum(Case when (@asOfDate -  af.tranDate) > 30
	                and (@asOfDate -  af.tranDate) <= 60 
			 	     Then af.arAmount else 0 END) as [due31_60], 

	       sum(Case when (@asOfDate -  af.tranDate) > 60
	                and (@asOfDate -  af.tranDate) <= 90 
				     Then af.arAmount else 0 END) as [due61_90],	

	       sum(Case when (@asOfDate -  af.tranDate) > 90
			          Then af.arAmount else 0 END) as [due91_]
    FROM #agentFile af
	WHERE af.arAmount <> 0
      and af.type     = 'A' 
	GROUP BY af.agentID,af.type
       
    INSERT INTO #arAging([agentID],[type],[due0_30],[due31_60],[due61_90],[due91_])
	SELECT af.agentID,
	       af.type,
	       sum(Case when (@asOfDate -  af.tranDate) >= 0
	                and (@asOfDate -  af.tranDate) <= 30  
				     Then af.arAmount else 0 END) as [due0_30], 

	       sum(Case when (@asOfDate -  af.tranDate) > 30
	                and (@asOfDate -  af.tranDate) <= 60  
				     Then af.arAmount else 0 END) as [due31_60], 

	       sum(Case when (@asOfDate -  af.tranDate) > 60
	                and (@asOfDate -  af.tranDate) <= 90  
				     Then af.arAmount else 0 END) as [due61_90],	

	       sum(Case when (@asOfDate -  af.tranDate) > 90
			         Then af.arAmount else 0 END) as [due91_]
    FROM #agentFile af
	WHERE af.arAmount <> 0
	  and af.type = 'I'
	GROUP BY af.agentID,af.type

    INSERT INTO #arAging([agentID],[type],[due0_30],[due31_60],[due61_90],[due91_])
	SELECT af.agentID,
	       af.type,
	       sum(Case when (@asOfDate -  af.tranDate) >= 0
	                and (@asOfDate -  af.tranDate) <= 30  
				    Then af.arAmount else 0 END) as [due0_30], 

	       sum(Case when (@asOfDate -  af.tranDate) > 30
	                and (@asOfDate -  af.tranDate) <= 60  
				    Then af.arAmount else 0 END) as [due31_60], 

	       sum(Case when (@asOfDate -  af.tranDate) > 60
	                and (@asOfDate -  af.tranDate) <= 90  
				    Then af.arAmount else 0 END) as [due61_90],	

	       sum(Case when (@asOfDate -  af.tranDate) > 90
			        Then af.arAmount else 0 END) as [due91_]
    FROM #agentFile af
	WHERE af.arAmount <> 0
	  and af.type = 'P'
    GROUP BY af.agentID,af.type

    INSERT INTO #arAging([agentID],[type],[due0_30],[due31_60],[due61_90],[due91_])
	SELECT af.agentID,
	       af.type,
	       sum(Case when (@asOfDate -  af.tranDate) >= 0
	                and (@asOfDate -  af.tranDate) <= 30  
			        Then af.arAmount else 0 END) as [due0_30], 

	       sum(Case when (@asOfDate -  af.tranDate) > 30
	                and (@asOfDate -  af.tranDate) <= 60  
			        Then af.arAmount else 0 END) as [due31_60], 

	       sum(Case when (@asOfDate -  af.tranDate) > 60
	                and (@asOfDate -  af.tranDate) <= 90  
			        Then af.arAmount else 0 END) as [due61_90],	

	       sum(Case when (@asOfDate -  af.tranDate) > 90
		           Then af.arAmount else 0 END) as [due91_]
    FROM #agentFile af
	WHERE af.arAmount <> 0
	  and af.type = 'C'
	GROUP BY af.agentID,af.type	   


	Update #arAging set   #arAging.artranID   = af.arTranID, 
	                      #arAging.postedDate = af.postedDate,
						  #arAging.invoiced   = af.invoiced, 
						  #arAging.fileID     = af.fileID
	  from #arAging
	  INNER JOIN
	  #agentFile af
	   on af.agentID    = #arAging.agentID
	  and af.tranID     = #arAging.tranID
	  and af.type       = #arAging.type

    UPDATE #arAging
      SET #arAging.stateID    = agent.stateID,
          #arAging.name       = agent.name,
		  #arAging.stat       = agent.stat,
		  #arAging.balance    = #arAging.due0_30 + #arAging.due31_60 + #arAging.due61_90 + #arAging.due91_
	      FROM agent 
	      WHERE #arAging.agentID  = agent.agentID


    UPDATE #arAging
      SET #arAging.managerID  = am.managerID,
	      #arAging.manager    = am.uid	
	  from [dbo].[agentmanager] am
        where am.[isPrimary]      = 1
          AND   am.[stat]         = 'A'
	      AND   #arAging.agentID  = am.agentID
    
	update #arAging
	  set #arAging.manager = s.name
	  from sysuser s
	  where #arAging.manager = s.uid

    update #arAging
	  set #arAging.fileNumber = at.filenumber
	  from artran at
	  where  at.entityID   = #arAging.agentID 
	    and  at.fileID     = #arAging.fileID
		and  at.type       = 'F'
		and  #arAging.type = 'A'
    
    update #arAging
      set #arAging.orgID = o.orgID
      from orgrole o
      where o.source   = 'Agent'
        and o.sourceID = #arAging.agentID

    Delete #arAging where #arAging.balance = 0
    SELECT * FROM #ArAging ORDER BY #ArAging.stateID,#ArAging.agentID
END
