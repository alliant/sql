/****** Object:  StoredProcedure [dbo].[spGetProductionActivity]    Script Date: 7/12/2024 3:14:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetProductionActivity] 
	-- Add the parameters for the stored procedure here
	@agentID                 VARCHAR(MAX) = 'ALL',
	@startDate               DATETIME     = NULL,
	@endDate                 DATETIME     = NULL,
	@excludeZeroBalanceFiles BIT  = 0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  
	-- Insert statements for procedure here
     CREATE TABLE #fileAR (agentfileID	   INTEGER,
	                       agentID         VARCHAR(50),
						   agent           VARCHAR(250),
						   agentName       VARCHAR(200),
						   fileID          VARCHAR(50),
						   tranDate        DATETIME,
						   amount          [decimal](18, 2) NULL,
						   invoicedAmount  [decimal](18, 2) NULL,
     				       cancelledAmount [decimal](18, 2) NULL,
                           paidAmount      [decimal](18, 2) NULL,
						   propertyInfo    VARCHAR(500)
     	 		           )

	 CREATE TABLE #agentFile (agentfileid  varchar(80),
					      	  agentID      VARCHAR(80),
	                          fileNumber   VARCHAR(50),
		        		      fileID       VARCHAR(50),
						      arAmount     [DECIMAL](17, 2) NULL,
						      periodCount  INTEGER,						
						      tranDate     DATE)

    INSERT INTO #agentFile ([agentfileid],[agentID],[fileID],[arAmount],[periodCount],[tranDate])
    EXEC [dbo].[spFileswithBalanceorActivity] @startDate = @startDate, @endDate = @endDate, @agentID = @agentID 

    update #agentFile set fileNumber = (select fileNumber from agentfile where agentfile.agentFileID = #agentFile.agentfileid)

	/* calculate toatal invoice amount */
	INSERT INTO #fileAR( [agentfileID], [agentID], [agent], [agentName], [fileID], [invoicedAmount],[cancelledAmount],[paidAmount])
    SELECT af.agentfileID, 
		   af.agentID, 
		   (SELECT [name] FROM [dbo].[agent] WHERE [agentID] = af.[agentID]) + ' (' + af.agentID + ')', 
		   (SELECT [name] FROM [dbo].[agent] WHERE [agentID] = af.[agentID]) as [agentName],
		   af.fileNumber, 
		   COALESCE((SELECT SUM(fr.amount) FROM fileAR fr 
		             WHERE fr.agentFileID = af.agentfileid AND fr.type = 'I' AND fr.tranDate <= @endDate),0),  /* file total invoiced amount  */
           COALESCE((SELECT SUM(fr.amount) FROM fileAR fr 
		             WHERE fr.agentFileID = af.agentfileid AND fr.type = 'W' AND fr.tranDate <= @endDate),0),  /* file total write-off amount   */
	       COALESCE((SELECT SUM(fr.amount) FROM fileAR fr 
		             WHERE fr.agentFileID = af.agentfileid AND fr.type = 'A' AND fr.tranDate <= @endDate),0)  /* file total applied amount   */
	FROM fileAR fa
    INNER JOIN #agentFile af ON (af.agentFileID = fa.agentFileID)
	WHERE fa.tranDate >= CAST(@startDate AS DATE)
      AND fa.tranDate <= CAST(@endDate AS DATE)
	  GROUP BY af.agentfileid,af.agentID,af.fileID,af.fileNumber

	UPDATE #fileAR
	   SET #fileAR.tranDate = af.tranDate
	FROM #agentFile af
	WHERE af.agentfileid = #fileAR.agentfileID
	  AND af.agentID     = #fileAR.agentID
	  AND af.fileID      = #fileAR.fileID

    UPDATE #fileAR
       SET #fileAR.propertyInfo =
       TRIM(', ' FROM
        (CASE WHEN fileproperty.addr1 > '' THEN fileproperty.addr1 ELSE '' END) +
        (CASE WHEN fileproperty.addr2 > ''
              THEN CASE WHEN fileproperty.addr1 > '' THEN ', ' ELSE '' END + fileproperty.addr2 ELSE '' END) +
        (CASE WHEN fileproperty.addr3 > ''
              THEN CASE WHEN fileproperty.addr1 > '' OR fileproperty.addr2 > '' THEN ', ' ELSE '' END + fileproperty.addr3 ELSE '' END) +
        (CASE WHEN fileproperty.addr4 > ''
              THEN CASE WHEN fileproperty.addr1 > '' OR fileproperty.addr2 > '' OR fileproperty.addr3 > '' THEN ', ' ELSE '' END + fileproperty.addr4 ELSE '' END) +
        (CASE WHEN fileproperty.city > ''
              THEN CASE WHEN fileproperty.addr1 > '' OR fileproperty.addr2 > '' OR fileproperty.addr3 > '' OR fileproperty.addr4 > '' THEN ', ' ELSE '' END + fileproperty.city ELSE '' END) +
        (CASE WHEN fileProperty.countyID > ''
              AND EXISTS(SELECT * FROM county WHERE county.countyID = fileProperty.countyID AND county.stateID = fileproperty.stateID)
              THEN CASE WHEN fileproperty.addr1 > '' OR fileproperty.addr2 > '' OR fileproperty.addr3 > '' OR fileproperty.addr4 > '' OR fileproperty.city > '' THEN ', ' ELSE '' END +
                  (SELECT county.description FROM county WHERE county.countyID = fileProperty.countyID AND county.stateID = fileproperty.stateID)
             ELSE ''
         END) +
        (CASE WHEN fileproperty.stateID > ''
              THEN CASE WHEN fileproperty.addr1 > '' OR fileproperty.addr2 > '' OR fileproperty.addr3 > '' OR fileproperty.addr4 > '' OR fileproperty.city > '' OR
                               (fileProperty.countyID > '' AND EXISTS(SELECT * FROM county WHERE county.countyID = fileProperty.countyID AND county.stateID = fileproperty.stateID))
                    THEN ', ' ELSE '' END + fileproperty.stateID
              ELSE ''
         END) +
        (CASE WHEN fileproperty.zip > ''
              THEN CASE WHEN fileproperty.addr1 > '' OR fileproperty.addr2 > '' OR fileproperty.addr3 > '' OR fileproperty.addr4 > '' OR fileproperty.city > '' OR
                               fileproperty.stateID > '' OR
                               (fileProperty.countyID > '' AND EXISTS(SELECT * FROM county WHERE county.countyID = fileProperty.countyID AND county.stateID = fileproperty.stateID))
              THEN ', ' ELSE '' END + fileproperty.zip
              ELSE ''
         END))
    FROM fileProperty
    INNER JOIN #fileAR fa ON fileproperty.agentFileID = fa.agentfileid
	
	/* @excludeZeroBalanceFiles if true delete filear records who does have zero balance esle take */
	IF @excludeZeroBalanceFiles = 1
	BEGIN
	   Delete from #filear where not exists( select * from filear where #filear.agentfileID = filear.agentFileID and #filear.agentID = filear.agentID
                                                                and #filear.fileID = filear.fileID and filear.seq = 0)
	END

	SELECT * FROM #fileAR

END
