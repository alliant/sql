GO
/****** Object:  StoredProcedure [dbo].[spGetDistributionContacts]    Script Date: 7/5/2024 10:19:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetDistributionContacts]
	-- Add the parameters for the stored procedure here
	@name VARCHAR(100) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  DECLARE @MaxLength INT = 175

  SELECT  p.[personID],
          ISNULL(STRING_AGG(pa.[agentID], ', '), '') AS [agentID],
          ISNULL(SUBSTRING([dbo].[RemoveDuplicates](STRING_AGG(a.[name], ', '), ', '), 1, @MaxLength), '') AS [agentName],
          LEN([dbo].[RemoveDuplicates](STRING_AGG(a.[name], ', '), ', ')) AS [agentNameLength],
          CASE 
            WHEN SUM(CASE WHEN a.[stat] = 'A' THEN 1 ELSE 0 END) > 0 THEN 'A'
            WHEN SUM(CASE WHEN a.[stat] = 'P' THEN 1 ELSE 0 END) > 0 THEN 'P'
            WHEN SUM(CASE WHEN a.[stat] = 'X' THEN 1 ELSE 0 END) > 0 THEN 'X'
            WHEN SUM(CASE WHEN a.[stat] = 'C' THEN 1 ELSE 0 END) > 0 THEN 'C'
            WHEN SUM(CASE WHEN a.[stat] = 'W' THEN 1 ELSE 0 END) > 0 THEN 'W'
            ELSE 'N'
          END AS [agentStat],
          ISNULL(STRING_AGG(a.[stateID], ' '), 'N') AS [agentStateID],
          SUBSTRING(ISNULL(STRING_AGG(a.[stateID], ' '), ''), 1, 2) AS [startState]
  INTO    #agentTable
  FROM    [dbo].[person] p LEFT OUTER JOIN
          [dbo].[personagent] pa
  ON      p.[personID] = pa.[personID] LEFT OUTER JOIN
          [dbo].[agent] a
  ON      pa.[agentID] = a.[agentID]
  GROUP BY p.[personID]

  UPDATE  #agentTable
  SET     [agentName] = 
          CASE WHEN [agentNameLength] > @MaxLength
            THEN SUBSTRING([agentName], 1, LEN([agentName]) - CHARINDEX(' ', REVERSE([agentName]))) + '...'
            ELSE [agentName]
          END
  WHERE   [agentName] <> ''

  UPDATE  #agentTable
  SET     [agentStateID] = CASE WHEN LEN(TRIM(REPLACE([agentStateID], [startState], ''))) > 0 THEN 'N' ELSE [startState] END
  WHERE   [agentStateID] <> 'N'

  SELECT  d.[name] AS [distName],
          d.[description] AS [description],
          pc.[personContactID] AS [personContactID],
          p.[personID] AS [personID],
          p.[dispName] AS [contactName],
          [dbo].[FormatAddress] (p.[address1], p.[address2], '', '', p.[city], p.[state]) + ' ' + p.[zip] AS [address],
          CASE WHEN COALESCE(p.[doNotCall], 0) = 0 THEN CASE WHEN pc.[expirationDate] IS NOT NULL THEN 'yes' ELSE 'no'END ELSE 'yes' END AS [doNotCall],
          pc.[contactID] AS [email],
          CASE WHEN pc.[expirationDate] IS NULL THEN 'yes' ELSE 'no' END AS [contactActive],
          a.[agentID],
          a.[agentName],
          a.[agentStat],
		      (SELECT [objValue] FROM [sysprop] where [appCode] = 'AMD' AND [objAction] = 'Agent' AND [objID] = a.[agentStat]) AS [agentStatDesc],
          a.[agentStateID],
          (SELECT [description] FROM [dbo].[state] WHERE [stateID] = a.[agentStateID]) AS [agentStateDesc]
  INTO    #contact
  FROM    [dbo].[distribution] d INNER JOIN
          [dbo].[distributioncontact] dc
  ON      d.[name] = dc.[name] INNER JOIN
          [dbo].[personcontact] pc
  ON      dc.[personContactID] = pc.[personContactID] INNER JOIN
          [dbo].[person] p
  ON      pc.[personID] = p.[personID] LEFT OUTER JOIN
          #agentTable a
  ON      p.[personID] = a.[personID]
  WHERE   d.[name] = @name
 
  SELECT  [distName],
          [description],
          [personContactID],
          [personID],
          [contactName],
          [doNotCall],
          [email],
          CASE
            WHEN [address] LIKE ', ,%' THEN TRIM(SUBSTRING([address], 4, LEN([address])))
            WHEN [address] LIKE ',%' THEN TRIM(SUBSTRING([address], 2, LEN([address])))
            ELSE [address]
          END AS [address],
          [contactActive],
          [agentID],
          [agentName],
          [agentStat],
          [agentStatDesc],
          [agentStateID],
          [agentStateDesc]
  FROM    #contact 

  DROP TABLE #contact
  DROP TABLE #agentTable
END
