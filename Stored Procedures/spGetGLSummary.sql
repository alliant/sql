-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- Object:  StoredProcedure [dbo].[spGetGLSummary]
-- Script Date: 1/27/2020 12:35:53 PM
-- ================================================
USE [Compass]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Rahul Sharma>
-- Create date: <16-01-2020>
-- Description:	<Get GL Summary Report>
-- =============================================
ALTER PROCEDURE [dbo].[spGetGLSummary] 
	-- Add the parameters for the stored procedure here
    @batchid     integer  = 0,
	@posted      bit      = 0,
    @startDate   datetime = NULL,          
    @endDate     datetime = NULL    
AS
IF @batchid = 0 and @startDate IS NULL  and @endDate IS NULL 
   BEGIN    
       RETURN
   END  
ELSE 
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

    -- Temporary table definition
    create table #GLperiodprocessinggl (batchid             integer,
                                        batchpostingdate    datetime,
                                        glDesc              varchar(500),
				                        glRef               varchar(500),
		                                debit               [decimal](18, 2) default 0,
                 		    		    credit              [decimal](18, 2) default 0
     	 		                        )	
	
    create table #periodprocessing  (batchid             integer,                                     		    					     
                                     revenueType         varchar(500),                                     
		                             grossdeltaamt       [decimal](18, 2) default 0,
                                     retaineddeltaamt    [decimal](18, 2) default 0,
                                     netdeltaamt         [decimal](18, 2) default 0                 		    		 
     	 		                     )
    	
    -- Insert statements for procedure here	
    insert into #periodprocessing (batchid, revenueType, grossdeltaamt, retaineddeltaamt, netdeltaamt)
    select  t1.batchID, t1.revenueType, isnull(t2.grossdeltatotal,0), isnull(t3.retentionDeltatotal,0), isnull(t4.netDeltatotal,0)  
     from
         (select distinct bf.batchID, bf.revenueType
            from batchform bf
            where bf.batchID in (select b.batchid from batch b 
						          where b.batchID     = (case when @batchid = 0 then b.batchID else @batchid end)
								    and ((@posted = 1 and b.posted = @posted) or (@posted = 0 and b.stat = 'C'))
							        and b.invoiceDate >= (case when @startDate is null then b.invoiceDate else @startDate end)
								    and b.invoiceDate <= (case when @endDate is null then b.invoiceDate else @endDate end)
                                )
         ) t1
	 join 
         (select sum(grossDelta) as grossdeltatotal, bf.batchID, bf.revenueType 
            from batchform bf  
            where bf.batchID in (select b.batchid from batch b 
			    			      where b.batchID     = (case when @batchid = 0 then b.batchID else @batchid end)
								    and ((@posted = 1 and b.posted = @posted) or (@posted = 0 and b.stat = 'C'))
							        and b.invoiceDate >= (case when @startDate is null then b.invoiceDate else @startDate end) 
								    and b.invoiceDate <= (case when @endDate is null then b.invoiceDate else @endDate end)
                                )
		    group by bf.batchID, bf.revenueType 
         ) t2
	 on t1.batchID = t2.batchID and t1.revenueType = t2.revenueType
	 left outer join 
         (select sum(retentionDelta) as retentionDeltatotal, bf.batchID, bf.revenueType
            from batchform bf  
            where (bf.retentionDelta is not null and bf.retentionDelta <> 0)
			  and bf.batchID in (select b.batchid from batch b 
			 			          where b.batchID     = (case when @batchid = 0 then b.batchID else @batchid end)
								    and ((@posted = 1 and b.posted = @posted) or (@posted = 0 and b.stat = 'C'))
							        and b.invoiceDate >= (case when @startDate is null then b.invoiceDate else @startDate end) 
								    and b.invoiceDate <= (case when @endDate is null then b.invoiceDate else @endDate end)
                                )
		    group by bf.batchID, bf.revenueType
         ) t3
	 on t1.batchID = t3.batchID and t1.revenueType = t3.revenueType 
	 join 
         (select sum(netDelta) as netDeltatotal, bf.batchID, bf.revenueType
            from batchform bf  
            where bf.batchID in (select b.batchid from batch b 
			   			          where b.batchID     = (case when @batchid = 0 then b.batchID else @batchid end) 
								    and ((@posted = 1 and b.posted = @posted) or (@posted = 0 and b.stat = 'C'))
							        and b.invoiceDate >= (case when @startDate is null then b.invoiceDate else @startDate end) 
								    and b.invoiceDate <= (case when @endDate is null then b.invoiceDate else @endDate end)
                                )
		    group by  bf.batchID, bf.revenueType
         ) t4
	 on t1.batchID = t4.batchID and t1.revenueType = t4.revenueType
	
	/*-------grossGLRef with Credit Balance-----------*/
	insert into #GLperiodprocessinggl (batchid, credit, glDesc, glRef)
	select pp.batchid,           
		   pp.grossdeltaamt,
		   ar.grossGLDesc,
		   ar.grossGLRef
      from #periodprocessing pp inner join arrevenue ar on pp.revenueType = ar.revenueType
      where pp.grossdeltaamt >= 0
		   	
	/*-------grossGLRef with debit Balance-----------*/
	insert into #GLperiodprocessinggl (batchid, debit, glDesc, glRef)
	select pp.batchid,           
		   pp.grossdeltaamt,
		   ar.grossGLDesc,
		   ar.grossGLRef
      from #periodprocessing pp inner join arrevenue ar on pp.revenueType = ar.revenueType
      where pp.grossdeltaamt < 0
		   	
	/*------retainedGLRef with Debit Balance---------------*/
    insert into #GLperiodprocessinggl (batchid, debit, glDesc, glRef)
	select pp.batchid,
		   pp.retaineddeltaamt,
		   ar.retainedGLDesc,
		   ar.retainedGLRef
      from #periodprocessing pp inner join arrevenue ar on pp.revenueType = ar.revenueType
      where pp.retaineddeltaamt >= 0

    /*------retainedGLRef with credit Balance---------------*/
    insert into #GLperiodprocessinggl (batchid, credit, glDesc, glRef)
	select pp.batchid,           
		   pp.retaineddeltaamt,
		   ar.retainedGLDesc,
		   ar.retainedGLRef
      from #periodprocessing pp inner join arrevenue ar on pp.revenueType = ar.revenueType
      where pp.retaineddeltaamt < 0      
	  		   	
	/*------arGLRef with Debit Balance---------------*/
	insert into #GLperiodprocessinggl (batchid, debit, glDesc, glRef)
	select pp.batchid,        
		   pp.netdeltaamt,
		   agent.arGLDesc,
		   agent.arGLRef
      from #periodprocessing pp inner join batch 
	  on pp.batchID = batch.batchID
	  inner join agent
	  on batch.agentID = agent.agentID
      where pp.netdeltaamt >= 0
		   	
	/*------arGLRef with credit Balance---------------*/
	insert into #GLperiodprocessinggl (batchid, credit, glDesc, glRef)
	select pp.batchid,           
		   pp.netdeltaamt,
		   agent.arGLDesc,
		   agent.arGLRef
      from #periodprocessing pp inner join batch 
	  on pp.batchID = batch.batchID
	  inner join agent
	  on batch.agentID = agent.agentID
      where pp.netdeltaamt < 0    
		
    update #GLperiodprocessinggl set batchpostingdate = (select batch.invoiceDate from batch where batch.batchid = #GLperiodprocessinggl.batchid),
                                               credit = ISNULL(ABS(credit),0),
									           debit  = ISNULL(ABS(debit),0)

    select * from #GLperiodprocessinggl order by batchid, glref
END
