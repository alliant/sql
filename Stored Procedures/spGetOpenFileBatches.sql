USE [compass]
GO
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Shubham>
-- Create date: <11-02-2020>
-- Description:	<Get Open File Batches>
-- =============================================
CREATE PROCEDURE [dbo].[spGetOpenFileBatches] 
	-- Add the parameters for the stored procedure here
    @agentid     varchar(10)  = 0   
AS
IF @agentid = 0  
   BEGIN    
       RETURN
   END  
ELSE 
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  select * 
   from batch b
   where b.agentID = @agentID
	 and posted = 1
	 and exists (select * 
	              from artran at 
				  where at.entityID     = @agentID
	                and at.entity       = 'A'
				    and (at.type        = 'I' or at.type = 'R')
					and at.transtype    = 'F'
					and sourcetype      = 'B'
                    and at.sourceID     = cast(b.batchID as varchar(10))
					and exists (select atr.fileID from artran atr
					                    where atr.entity       = at.entity 
	                                      and atr.entityID     = at.entityID
                                          and atr.fileID       = at.fileID
					                      and atr.type         = 'F'
					                      and atr.transtype    = 'F'
										  and atr.seq          = 0
                                          and atr.remainingAmt > 0 )
                    )
    order by batchID

END
