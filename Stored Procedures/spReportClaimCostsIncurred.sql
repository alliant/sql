USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spReportClaimsCostsIncurred]    Script Date: 11/16/2016 8:49:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportClaimCostsIncurred]
	-- Add the parameters for the stored procedure here
	@startDate DATETIME = NULL,
  @endDate DATETIME = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  -- Decide the dates
  IF (@startDate IS NULL)
    SET @startDate = '2005-01-01'

  IF (@endDate IS NULL)
    SET @endDate = GETDATE()
    
  -- The end date should be the last second of the day
  SET @endDate = DATEADD(s,-1,DATEADD(d,1,@endDate))

  DECLARE @insertClaimBalanceDelta [dbo].[ClaimBalanceDelta]
  SELECT * INTO #insertClaimBalanceDelta FROM @insertClaimBalanceDelta

  -- Get the claim balances delta
  EXEC [dbo].[spReportClaimBalancesDelta] @startDate = @startDate, @endDate = @endDate

  DECLARE @insertClaimBalance [dbo].[ClaimBalance]
  SELECT * INTO #insertClaimBalance FROM @insertClaimBalance

  -- Get the claim balances delta
  EXEC [dbo].[spReportClaimBalances] @asOfDate = @endDate

  SELECT  c.[claimID],
          c.[assignedTo],
          CASE c.[stat]
            WHEN 'C' THEN YEAR(ISNULL([dateClosed],GETDATE()))
            ELSE 0
          END AS [yearClosed],
          c.[stat] AS [claimStatus],
          CASE
            WHEN property.[residential] > 0 THEN 'TRUE'
            ELSE 'FALSE'
          END AS [residential],
          c.[altaResponsibility],
          c.[altaRisk],
          CONVERT(VARCHAR(MAX),ISNULL(claimcode.[category],'')) AS [category],
          CONVERT(VARCHAR(MAX),ISNULL(claimcode2.[category],'')) AS [cause],
          cc.[policyID],
          cc.[formID] AS [policyFormID],
          cc.[issuedEffDate] AS [policyEffectiveDate],
          cc.[liabilityAmount] AS [policyLiability],
          a.[agentID],
          a.[name] AS [agentName],
          a.[stat] AS [agentStatus],
          a.[cancelDate] AS [agentCancelDate],
          a.[stateID],
          d.[laeCompletedInvoiceAmount] AS [laePosted],
          d.[lossCompletedInvoiceAmount] AS [lossPosted],
          d.[paidRecoveries] AS [recoveries],
          d.[laeReserveBalance] AS [laeBalance],
          d.[lossReserveBalance] AS [lossBalance],
          d.[laeCompletedInvoiceAmount] + 
          d.[lossCompletedInvoiceAmount] +
          d.[laeReserveBalance] + 
          d.[lossReserveBalance] -
          d.[paidRecoveries] AS [costsIncurred],
          b.[laeReserve],
          b.[lossReserve],
          b.[laeReserveWithOpenPayments],
          b.[lossReserveWithOpenPayments]
  FROM    [claim] c LEFT OUTER JOIN
          (
          SELECT  [claimID],
                  MAX([stateID]) AS [stateID],
                  SUM(CONVERT(INTEGER,[residential])) AS [residential]
          FROM    [claimproperty]
          GROUP BY [claimID]
          ) property
  ON      c.[claimID] = property.[claimID] LEFT OUTER JOIN
          (
          SELECT  * 
          FROM    (
                  SELECT  cc.[claimID],
                          cc.[seq],
                          COALESCE(CONVERT(VARCHAR, p.[policyID]), cc.[coverageID]) AS [policyID],
                          COALESCE(p.[issuedEffDate], cc.[effDate]) AS [issuedEffDate],
                          COALESCE(p.[liabilityAmount], cc.[origLiabilityAmount]) AS [liabilityAmount],
                          COALESCE(p.[formID], '') AS [formID]
                  FROM    [dbo].[claimcoverage] cc LEFT OUTER JOIN
                          [dbo].[policy] p
                  ON      cc.[coverageID] = CONVERT(VARCHAR, p.[policyID])
                  ) cc
          WHERE   cc.[seq] = 1
          ) cc
  ON      c.[claimID] = cc.[claimID] INNER JOIN
          #insertClaimBalanceDelta d
  ON      c.[claimID] = d.[claimID] INNER JOIN
          (
          SELECT  [claimID],
                  SUM(CASE WHEN [refCategory] = 'E' THEN [reserveBalance] ELSE 0 END) AS [laeReserve],
                  SUM(CASE WHEN [refCategory] = 'L' THEN [reserveBalance] ELSE 0 END) AS [lossReserve],
                  SUM(CASE WHEN [refCategory] = 'E' THEN [reserveBalance] - [pendingInvoiceAmount] ELSE 0 END) AS [laeReserveWithOpenPayments],
                  SUM(CASE WHEN [refCategory] = 'L' THEN [reserveBalance] - [pendingInvoiceAmount] ELSE 0 END) AS [lossReserveWithOpenPayments]
          FROM    #insertClaimBalance
          GROUP BY [claimID]
          ) b
  ON      c.[claimID] = b.[claimID] LEFT OUTER JOIN
          (
          SELECT  cc2.[claimID],
                  SUBSTRING(
                  (
                    SELECT  ', ' + sc.[description] AS [text()]
                    FROM    [syscode] sc INNER JOIN
                            [claimcode] cc
                    ON      cc.[code] = sc.[code]
                    AND     cc.[codeType] = sc.[codeType]
                    WHERE   cc.[claimID] = cc2.[claimID]
                    AND     sc.[codeType] = 'ClaimDescription'
                    ORDER BY cc.[claimID]
                    FOR XML PATH('')
                  ), 3, 1000) AS [category]
          FROM    [claimcode] cc2
          GROUP BY cc2.[claimID]
          ) claimcode
  ON      C.[claimID] = claimcode.[claimID] LEFT OUTER JOIN
          (
          SELECT  cc2.[claimID],
                  SUBSTRING(
                  (
                    SELECT  ', ' + sc.[description] AS [text()]
                    FROM    [syscode] sc INNER JOIN
                            [claimcode] cc
                    ON      cc.[code] = sc.[code]
                    AND     cc.[codeType] = sc.[codeType]
                    WHERE   cc.[claimID] = cc2.[claimID]
                    AND     sc.[codeType] = 'ClaimCause'
                    ORDER BY cc.[claimID]
                    FOR XML PATH('')
                  ), 3, 1000) AS [category]
          FROM    [claimcode] cc2
          GROUP BY cc2.[claimID]
          ) claimcode2
  ON      C.[claimID] = claimcode2.[claimID] INNER JOIN
          [agent] a
  ON      c.[agentID] = a.[agentID]
  ORDER BY c.[claimID]
END
