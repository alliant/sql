USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetRegion]    Script Date: 7/23/2018 3:58:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetRegions]
  -- No parameters
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  CREATE TABLE #regionTable
  (
    [regionID] VARCHAR(10),
	[regionDesc] VARCHAR(50),
    [stateID] VARCHAR(10) DEFAULT NULL,
	[stateDesc] VARCHAR(50) DEFAULT NULL
  )
  
  INSERT INTO #regionTable (regionID,
                            regionDesc,
                            stateID,
						    stateDesc
                            )


  select syscode.code,
         syscode.description,
	     state.stateID,
	     state.description
	     from syscode 
	     full outer join state on state.active = 1  and syscode.code = state.region
	     where syscode.codeType = 'Region' 

 select * from #regionTable --order by #regionTable.regionID

--drop table #regionTable
END
