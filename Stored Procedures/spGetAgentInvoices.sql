-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
USE [COMPASS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author:		<Rahul Sharma>
-- Create date: <21-11-2019>
-- Description:	<Get batch records>
-- =============================================
ALTER PROCEDURE [dbo].[spGetAgentInvoices] 
	-- Add the parameters for the stored procedure here
	@piMonth    INTEGER   = 0,
	@piYear     INTEGER   = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    
	-- Insert statements for procedure here
    
	CREATE TABLE #arInvoice (artranID             INTEGER,
							 InvoiceID			  VARCHAR(20),
	                         reference            VARCHAR(30),
						     type				  VARCHAR(10),
                             stateID              VARCHAR(10),						   
	                         agentID              VARCHAR(20),
						     agentName            VARCHAR(200),
						     agentStat            VARCHAR(10),						   
						     tranDate             DATETIME, 
						     netPremiumDelta      [DECIMAL](18, 2) NULL,					  
						     agentManager         VARCHAR(100))   
			
	INSERT INTO #arInvoice (artranID,InvoiceID,reference,type, agentID, tranDate, netPremiumDelta)
						  
      SELECT a.artranID, a.tranID, a.reference, a.type, a.entityid, a.tranDate, a.tranamt 	
        FROM artran a
        WHERE year(a.tranDate)  = (CASE WHEN @piYear = 0  THEN year(a.tranDate)  ELSE @piYear   END)  AND 
	          month(a.tranDate) =(CASE WHEN @piMonth = 0  THEN month(a.tranDate) ELSE @piMonth  END)  AND
	          a.type = 'I' AND a.transtype = ' ' AND a.seq = 0 and a.postdate is not null
	
    UPDATE #arInvoice
    SET #arInvoice.agentName = agent.name,
	    #arInvoice.agentStat = agent.stat,
		#arInvoice.stateID  = agent.stateID
	  FROM agent 
	  WHERE #arInvoice.agentID = agent.agentID
	
    UPDATE #arInvoice
    SET #arInvoice.agentManager = agentManager.UID
	  FROM agentManager 
	  WHERE #arInvoice.agentID     = agentManager.agentID	
	    AND agentManager.stat      = 'A'
        AND agentManager.isPrimary = 1
	
    SELECT * FROM #arInvoice ORDER BY #arInvoice.InvoiceID	
		
END
GO
