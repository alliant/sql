/****** Object:  StoredProcedure [dbo].[spGetQueueCategories]    Script Date: 5/4/2023 6:54:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spGetQueueCategories]
  @queuecategory VARCHAR(100) = '',
  @active        VARCHAR(1)   = ''
AS
BEGIN
CREATE TABLE #tempQueueCategory
	(
      [category]    VARCHAR(100),
	  [description] VARCHAR(500),
      [active]      BIT,
	  [isSecure]    BIT
	)

    IF (@active = '')
	 SET @active = 'B'
	 
	IF (@active = 'A')    /* (A)ctive */
	BEGIN
	  INSERT INTO #tempQueueCategory([category], [description], [active],[isSecure])
	  SELECT [category],
		     [description],
             [active],
			 [isSecure]
		     FROM queuecategory
      WHERE queuecategory.category = CASE WHEN @queuecategory != '' THEN @queuecategory ELSE queuecategory.category END
	    AND queuecategory.active   = 1 
	END		
	ELSE IF (@active = 'I')      /* (I)nactive */    
	BEGIN
	  INSERT INTO #tempQueueCategory([category], [description], [active],[isSecure])
	  SELECT [category],
		     [description],
             [active],
			 [isSecure]
		     FROM queuecategory
      WHERE queuecategory.category = CASE WHEN @queuecategory != '' THEN @queuecategory ELSE queuecategory.category END
	    AND queuecategory.active   = 0
	END	
	ELSE    /* (B)oth Active and Inactive */
	BEGIN      
	  INSERT INTO #tempQueueCategory([category], [description], [active],[isSecure])
	  SELECT [category],
		     [description],
             [active],
			 [isSecure]
		     FROM queuecategory
      WHERE queuecategory.category = CASE WHEN @queuecategory != '' THEN @queuecategory ELSE queuecategory.category END
	END	
			
	SELECT * FROM #tempQueueCategory

	DROP TABLE #tempQueueCategory
END

