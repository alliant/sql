USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spAgentReviewReceivable]    Script Date: 4/9/2018 3:26:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spAgentReviewReceivable]
	-- Add the parameters for the stored procedure here
  @agentID VARCHAR(MAX) = 'ALL',
  @UID varchar(100)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SET @agentID = [dbo].[StandardizeAgentID](@agentID)
  
  SELECT  *
  FROM    (
          SELECT  a.[agentID],
                  b.[stat],
                  b.[due],
                  b.[amount]
          FROM    [dbo].[agent] a INNER JOIN
                  (
                  SELECT  RTRIM(LTRIM(CM.[CUSTNMBR])) AS [agentID],
                          CASE WHEN RM.[POSTDATE] IS NOT NULL AND RM.[POSTDATE] < GETDATE() THEN 'C' ELSE 'O' END AS [stat],
                          RM.[DUEDATE] AS [due],
                          SUM(CASE WHEN RM.[RMDTYPAL] < 7 THEN RM.[CURTRXAM] ELSE 0 END) AS [amount]
                  FROM    [ANTIC].[dbo].[RM20101] RM INNER JOIN 
                          [ANTIC].[dbo].[RM00101] CM
                  ON      RM.[CUSTNMBR] = CM.[CUSTNMBR] LEFT OUTER JOIN
                          [ANTIC].[dbo].[RM00103] S
                  ON      RM.[CUSTNMBR] = S.[CUSTNMBR]
                  WHERE   RM.[VOIDSTTS] = 0 
                  AND     RM.[CURTRXAM] <> 0
                  AND     RM.[DUEDATE] > '2005-01-01'
                  GROUP BY CM.[CUSTNMBR],RM.[POSTDATE],RM.[DUEDATE]
                  ) b
          ON      a.[agentID] = b.[agentID]
          ) a
  WHERE (a.[agentID] = (CASE  WHEN @agentID != 'ALL' THEN @agentID ELSE a.[agentID] END)) 
  AND   (dbo.CanAccessAgent(@UID ,a.[agentID]) = 1)   
  
END
