USE [COMPASS_DVLP]
GO
/****** Object:  StoredProcedure [dbo].[spGetQualiaOrders]    Script Date: 3/1/2021 4:01:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportQualiaOrders]
	-- Add the parameters for the stored procedure here
  @stat VARCHAR(50) = 'ALL',
  @searcher VARCHAR(50) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  IF @stat = 'ALL'
    SET @stat = 'D,X,C'

  CREATE TABLE #status ([stat] VARCHAR(10))
  INSERT INTO #status ([stat])
  SELECT value FROM STRING_SPLIT(@stat, ',')

  -- Insert statements for procedure here
  SELECT  [qualiaID],
          [customer_id],
          [customer_deployment],
          [orderID],
          q.[agentID],
          ISNULL(a.[stateID], '') AS [stateID],
          [ccOrderID],
          [uid],
          q.[stat],
          [orderDate],
          [completeDate],
          q.[cancelDate],
          [dueDate],
          [fileNumber],
          [fileID],
          (SELECT [description] FROM [qualiaactivity] WHERE [qualiaID] = q.[qualiaID] AND [seq] = 2) AS [description],
          ISNULL(a.[name], '') AS [agentName],
          ISNULL((SELECT [name] FROM [sysuser] WHERE [uid] = q.[uid]), '') AS [uidDesc],
          CASE q.[stat]
            WHEN 'N' THEN 'New'
            WHEN 'X' THEN 'Cancelled'
            WHEN 'C' THEN 'Closed'
            WHEN 'P' THEN 'In Process'
            WHEN 'D' THEN 'Denied'
          END AS [statDesc],
          ISNULL((SELECT [description] FROM [state] WHERE [stateID] = a.[stateID]), '') AS [stateName]
  FROM    [qualia] q LEFT OUTER JOIN
          [agent] a
  ON      q.[agentID] = a.[agentID]
  WHERE   q.[uid] = CASE WHEN @searcher = '' THEN q.[uid] ELSE @searcher END
  AND     q.[stat] IN (SELECT [stat] FROM #status)
END
