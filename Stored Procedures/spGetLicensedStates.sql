/****** Object:  StoredProcedure [dbo].[spGetLicensedStates]    Script Date: 4/4/2023 3:54:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetLicensedStates]
	-- Add the parameters for the stored procedure here
	@UID VARCHAR(100) = ''  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [stateID],
           [description]
    FROM   [dbo].[state]
	WHERE  [dbo].[state].[active] = 1 
	AND    [dbo].[canAccessState](@UID, [dbo].[state].[stateID]) = 1

END
