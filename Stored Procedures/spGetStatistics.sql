-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
USE [COMPASS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Rahul>
-- Create date: <11/13/2018>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetStatistics]
	-- Add the parameters for the stored procedure here
	@ipExceptActions VARCHAR(MAX) = 'ALL',
	@ipStartDate     DATETIME,
	@ipEndDate       DATETIME 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Temp variable declaration
	 DECLARE @iNoCalls INTEGER          = 0,
	         @iNoActions INTEGER        = 0,
             @inoError INTEGER          = 0,
             @iuniqeaction INTEGER      = 0,
		     @icount INTEGER            = 0,
			 @iNoUser INTEGER           = 0,
			 @iActive INTEGER           = 0,
			 @cStr NVARCHAR(MAX)        = 0,
			 @iPos INTEGER              = 0,
			 @NextString VARCHAR(100),          
             @Delimiter NCHAR(1)        = ','

    -- Temporary table definition
	CREATE TABLE #action (action      VARCHAR(50),
	                      calls       INTEGER,
                          error       INTEGER,
                          avgDuration INTEGER)

	CREATE TABLE #actionerr (action VARCHAR(50),
	                         error  INTEGER)

	CREATE TABLE #user (UID    VARCHAR(100),
	                    uName  VARCHAR(50),
                        roles  VARCHAR(200),
	                    calls  INTEGER,
                        error  INTEGER)

	CREATE TABLE #usererr (UID   VARCHAR(100),
	                       error INTEGER)
    
    CREATE TABLE #ttstatistics (type        VARCHAR(20),
	                          action      VARCHAR(50),
                              uName       VARCHAR(50),
                              UID         VARCHAR(100),
	                          calls       INTEGER,
                              error       INTEGER,
                              avgDuration INTEGER,
                              noAction    INTEGER,
                              roles       VARCHAR(200),
                              description VARCHAR(100),
                              active      INTEGER,
                              inactive    INTEGER)

    CREATE TABLE #exceptAction ([action] VARCHAR(50))

	-- Extract value from  @ipExceptActions and make records in #exceptAction 
    SET @cStr = @ipExceptActions + @Delimiter
    SET @iPos = CHARINDEX(@Delimiter, @cStr)

    WHILE (@iPos <> 0)
	  BEGIN
		  SET @NextString = SUBSTRING(@cStr,1,@iPos-1)
      INSERT INTO #exceptAction
      SELECT @NextString
		  SET @cStr = SUBSTRING(@cStr,@iPos+1,LEN(@cStr))
		  SET @iPos = CHARINDEX(@Delimiter, @cStr)
	END

	 
    -- Find total number of server calls as per search criteria and store in @iNoCalls
    SELECT  @iNoCalls = count(*) 
	FROM syslog 
	WHERE syslog.createdate >= @ipStartDate 
	  AND syslog.createdate <= @ipendDate

    -- Find total number of sysaction records as per search criteria and store in @iNoActions
	SELECT  @iNoActions   = count(*)
	FROM sysaction

	-- Find total number of sysaction called as per search criteria and store in @iuniqeaction
	SELECT  @iuniqeaction = count(DISTINCT syslog.action) 
	FROM syslog inner join sysaction on syslog.action = sysaction.action
	WHERE syslog.createdate >= @ipStartDate 
	  AND syslog.createdate <= @ipendDate

    --Find total number of sysaction that returns error as per search criteria and store in @inoError
	SELECT  @inoError     = count(*) 
	FROM syslog 
	WHERE syslog.iserror     = 'true' 
	  AND syslog.createdate >= @ipStartDate 
	  AND syslog.createdate <= @ipendDate

    SELECT @iActive = count( distinct syslog.UID) 
	  FROM syslog 
	  WHERE syslog.iserror    = 'false'
	    AND syslog.createdate >= @ipStartDate 
		AND syslog.createdate <= @ipendDate

    SELECT @iNoUser = count(*) FROM sysuser where isActive = 1


    --Extract unique action,count of action and average of time duration in #action table
    INSERT INTO #action([Action],[calls],[avgDuration]) 
	  SELECT syslog.action,count(syslog.action),avg(syslog.duration) 
	  FROM syslog 
	  WHERE syslog.createdate >= @ipStartDate 
	    AND syslog.createdate <= @ipendDate group by syslog.action 

    DELETE #action FROM #action where #action.action  in (select * from #exceptAction)

    --Extract unique action and count of action that result error in #action table
    INSERT INTO #actionerr([Action],[error]) 
	  SELECT syslog.action,count(syslog.iserror) 
	  FROM syslog 
	  WHERE syslog.iserror = 'true' 
	    AND syslog.createdate >= @ipStartDate 
		AND syslog.createdate <= @ipendDate group by syslog.action
    
	--Update error field in #action table from #actionerr
    UPDATE #action SET #action.error = #actionerr.error 
	FROM #actionerr 
	WHERE #action.action = #actionerr.action
    
	--Insert top 5 maximum called #action record in #ttstatistics table with type 'Action'
	INSERT INTO #ttstatistics([TYPE],[Action],[calls],[avgDuration],[error]) 
	  SELECT top 5 'Action',#action.action,#action.calls,#action.avgDuration,#action.error 
	  from #action order by #action.calls desc

    --Update description field of #ttstatistics table from sysaction
	update #ttstatistics set #ttstatistics.description = sysaction.description 
	from sysaction 
	where #ttstatistics.action = sysaction.action
    
	--Insert record in #ttstatistics for tatal sysAction, Errors and inactive users with type 'ActionType' 
	INSERT INTO #ttstatistics([TYPE],[noAction],[calls],[error],[inactive]) 
	  values('ActionType',@iuniqeaction,@iNoCalls,@inoError,@iNoActions - @iuniqeaction)

    -- Extract data for user in #user table
	INSERT INTO #user([UID],[calls]) 
	  SELECT syslog.UID,count(syslog.action) 
	  FROM syslog 
        WHERE syslog.createdate >= @ipStartDate 
		  AND syslog.createdate <= @ipendDate group by syslog.UID

	INSERT INTO #usererr([UID],[error]) 
	  SELECT syslog.UID,count(syslog.iserror) 
	  FROM syslog 
	  WHERE syslog.iserror = 'true' 
		AND syslog.createdate >= @ipStartDate 
		AND syslog.createdate <= @ipendDate group by syslog.UID

	UPDATE #user SET #user.error = #usererr.error 
	FROM #usererr 
	WHERE #user.UID = #usererr.UID

	UPDATE #user SET #user.uname = sysuser.name, #user.roles = sysuser.role 
	from sysuser 
	where #user.UID = sysuser.UID

	INSERT INTO #ttstatistics([TYPE],[UID],[uName],[calls],[error],[Roles]) 
	SELECT top 5 'User',#user.UID,#user.uName,#user.calls,#user.error,#user.roles 
	from #user order by #user.calls desc

	--Insert record in #ttstatistics for tatal sysAction, Errors and inactive users with type 'ActionType' 
	INSERT INTO #ttstatistics([TYPE],[active],[inactive]) 
	  values('UserType',@iActive,@iNoUser - @iActive)

	SELECT * FROM #ttstatistics
    -- Insert statements for procedure here
END
GO
