
/****** Object:  StoredProcedure [dbo].[spGetWriteoffs]    Script Date: 6/14/2021 10:24:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Shefali>
-- Create date: <06/03/2021>
-- Description:	<Get Posted Write-off Report>
-- =============================================
ALTER PROCEDURE [dbo].[spGetWriteoffs] 
	-- Add the parameters for the stored procedure here
	@entityID         varchar(20)  = '',
	@fromPostDate     datetime     = null,
	@toPostDate       datetime     = null,
	@searchString     varchar(MAX) = '',
    @includeVoid      bit	       = 0,
	@includeArchived  bit	       = 0
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  -- Temporary table definition
  create table #arwriteoff (arwriteoffID      integer,
                            artranID          integer,
                            entity            varchar(50),
                            entityID          varchar(50),
							agentName         varchar(200),
							postBy            varchar(50),
							postByName        varchar(50),
                            postDate          datetime,
							tranDate          datetime,
                            tranamount        [decimal](18, 2) NULL,
                            void              bit,
                            archived          bit,							
  							ledgerID          integer,
                            createdDate       datetime,
			                createdBy         varchar(50),
							createdByName     varchar(50),
							voidLedgerID      integer,
							voidBy            varchar(50),
							voidByName        varchar(50),
							voidDate          datetime,
						    source            varchar(50),
							type              varchar(50),
							notes             varchar(50),
						   )
					  
  if @searchString = ''
  begin
    -- Insert statements for procedure here
    insert into #arwriteoff (arwriteoffID,artranID,entity,entityID,agentName,postBy,postDate,tranDate,tranamount,void,ledgerID,createdDate,createdBy,voidLedgerID,voidBy,voidDate,source,type,notes)
    select aw.arwriteoffID,at.artranID,aw.entity,aw.entityID,a.name,aw.postBy,aw.postDate,aw.tranDate,aw.tranamount,aw.void,aw.ledgerID,aw.createdDate,aw.createdBy,aw.voidLedgerID,
    aw.voidBy,aw.voidDate,aw.source,aw.type,aw.notes  
	  from arwriteoff aw 
	  inner join agent a 
		  on  aw.EntityID = a.agentID
      inner join artran at 
		  on  aw.arwriteoffID = at.tranID and at.type = 'W' and seq = 0
	  WHERE aw.Entity   = 'A' and
	        aw.EntityID = (CASE WHEN @entityID = 'ALL' THEN aw.entityID  ELSE @entityID END) AND
		    aw.trandate >= (CASE WHEN @fromPostDate IS NULL THEN aw.tranDate  ELSE @fromPostDate END) AND 
			aw.trandate <= (CASE WHEN @toPostDate   IS NULL THEN aw.tranDate  ELSE @toPostDate   END) AND 
		    aw.void = (CASE WHEN @includeVoid  = 1 THEN aw.void ELSE 0 END)
	
	/* Archived */
	if @includeArchived = 1
	begin
		-- Insert statements for procedure here
		insert into #arwriteoff (arwriteoffID,artranID,entity,entityID,agentName,postBy,postDate,tranDate,tranamount,void,archived,ledgerID,createdDate,createdBy,voidLedgerID,voidBy,voidDate,source,type,notes)
		select aw.arwriteoffID,at.artranID,aw.entity,aw.entityID,a.name,aw.postBy,aw.postDate,aw.tranDate,aw.tranamount,aw.void,aw.archived,aw.ledgerID,aw.createdDate,aw.createdBy,aw.voidLedgerID,
		aw.voidBy,aw.voidDate,aw.source,aw.type,aw.notes  
		from arwriteoff aw 
		inner join agent a 
			on  aw.EntityID = a.agentID
		inner join artranh at 
			on  aw.arwriteoffID = at.tranID and at.type = 'W' and seq = 0
		WHERE aw.Entity   = 'A' and
				aw.EntityID = (CASE WHEN @entityID = 'ALL' THEN aw.entityID  ELSE @entityID END) AND
				aw.trandate >= (CASE WHEN @fromPostDate IS NULL THEN aw.tranDate  ELSE @fromPostDate END) AND 
				aw.trandate <= (CASE WHEN @toPostDate   IS NULL THEN aw.tranDate  ELSE @toPostDate   END) AND 
				aw.void = (CASE WHEN @includeVoid  = 1 THEN aw.void ELSE 0 END)
	
	end
  end
  else
  begin
    -- Insert statements for procedure here
    insert into #arwriteoff (arwriteoffID,artranID,entity,entityID,agentName,postBy,postDate,tranDate,tranamount,void,ledgerID,createdDate,createdBy,voidLedgerID,voidBy,voidDate,source,type,notes)
    select aw.arwriteoffID,at.artranID,aw.entity,aw.entityID,a.name,aw.postBy,aw.postDate,aw.tranDate,aw.tranamount,aw.void,aw.ledgerID,aw.createdDate,aw.createdBy,aw.voidLedgerID,
    aw.voidBy,aw.voidDate,aw.source,aw.type,aw.notes  
	  from arwriteoff aw 
	  inner join agent a 
		  on  aw.EntityID = a.agentID
      inner join artran at 
		  on  aw.arwriteoffID = at.tranID and at.type = 'W' and seq = 0
	  WHERE aw.Entity   = 'A' and
	        aw.EntityID = (CASE WHEN @entityID = 'ALL' THEN aw.entityID  ELSE @entityID END) AND
		    (cast(aw.arWriteoffID as varchar) LIKE '%' + @searchString + '%' ) and
		    aw.trandate >= (CASE WHEN @fromPostDate IS NULL THEN aw.tranDate  ELSE @fromPostDate END) AND 
			aw.trandate <= (CASE WHEN @toPostDate   IS NULL THEN aw.tranDate  ELSE @toPostDate   END) AND
		    aw.void = (CASE WHEN @includeVoid  = 1 THEN aw.void ELSE 0 END)
			
	/* Archived */
	if @includeArchived = 1
	begin
		-- Insert statements for procedure here
		insert into #arwriteoff (arwriteoffID,artranID,entity,entityID,agentName,postBy,postDate,tranDate,tranamount,void,archived,ledgerID,createdDate,createdBy,voidLedgerID,voidBy,voidDate,source,type,notes)
		select aw.arwriteoffID,at.artranID,aw.entity,aw.entityID,a.name,aw.postBy,aw.postDate,aw.tranDate,aw.tranamount,aw.void,aw.archived,aw.ledgerID,aw.createdDate,aw.createdBy,aw.voidLedgerID,
		aw.voidBy,aw.voidDate,aw.source,aw.type,aw.notes  
		from arwriteoff aw 
		inner join agent a 
			on  aw.EntityID = a.agentID
		inner join artranh at 
			on  aw.arwriteoffID = at.tranID and at.type = 'W' and seq = 0
		WHERE aw.Entity   = 'A' and
				aw.EntityID = (CASE WHEN @entityID = 'ALL' THEN aw.entityID  ELSE @entityID END) AND
				(cast(aw.arWriteoffID as varchar) LIKE '%' + @searchString + '%' ) and
				aw.trandate >= (CASE WHEN @fromPostDate IS NULL THEN aw.tranDate  ELSE @fromPostDate END) AND 
				aw.trandate <= (CASE WHEN @toPostDate   IS NULL THEN aw.tranDate  ELSE @toPostDate   END) AND
				aw.void = (CASE WHEN @includeVoid  = 1 THEN aw.void ELSE 0 END)	
	end
  end
        
  UPDATE #arwriteoff
    SET
        #arwriteoff.postByName = (select sysuser.name from sysuser where sysuser.UID = #arwriteoff.postby),
	    #arwriteoff.createdByName = (select sysuser.name from sysuser where sysuser.UID = #arwriteoff.createdby),
	    #arwriteoff.voidByName = (select sysuser.name from sysuser where sysuser.UID = #arwriteoff.voidby)
  select * from #arwriteoff order by entityID
 end
