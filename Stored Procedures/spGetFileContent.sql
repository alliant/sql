/****** Object:  StoredProcedure [dbo].[spGetFileContent]    Script Date: 3/15/2023 6:05:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetFileContent]
	-- Add the parameters for the stored procedure here
	@agentFileID            INTEGER           =  0,
	@contentType            VARCHAR(1)        = 'A'
	
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	CREATE TABLE #fileContent
	(
	  agentFileID  integer, 
      seq          integer,   
      content      varchar(8000), 
      indent       integer,   
      isNote       bit,   
      appliesTo    varchar(100), 
      type         varchar(1)
	)
	
    IF @contentType  = 'E' /* get Exception fileContent data*/
	BEGIN
      INSERT INTO #fileContent ([agentFileID],[seq],[content],[indent],[isNote],[appliesTo],[type])
	  SELECT fc.agentFileID,
             fc.seq,
             fc.content,
             fc.indent,
             fc.isNote,
             fc.appliesTo,
             fc.type			 
      FROM [dbo].[filecontent] fc
      WHERE fc.agentFileID = @agentFileID and
            fc.type        = 'E'
    END
    ELSE IF @contentType  = 'I' /* get Instrument fileContent data*/
	BEGIN
      INSERT INTO #fileContent ([agentFileID],[seq],[content],[indent],[isNote],[appliesTo],[type])
	  SELECT fc.agentFileID,
             fc.seq,
             fc.content,
             fc.indent,
             fc.isNote,
             fc.appliesTo,
             fc.type			 
      FROM [dbo].[filecontent] fc
      WHERE fc.agentFileID = @agentFileID and
            fc.type        = 'I'
    END
	ELSE IF @contentType  = 'R' /* get Requirement fileContent data*/
	BEGIN
      INSERT INTO #fileContent ([agentFileID],[seq],[content],[indent],[isNote],[appliesTo],[type])
	  SELECT fc.agentFileID,
             fc.seq,
             fc.content,
             fc.indent,
             fc.isNote,
             fc.appliesTo,
             fc.type			 
      FROM [dbo].[filecontent] fc
      WHERE fc.agentFileID = @agentFileID and
            fc.type        = 'R'
    END
    ELSE /* get ALL fileContent data */
	BEGIN
      INSERT INTO #fileContent ([agentFileID],[seq],[content],[indent],[isNote],[appliesTo],[type])
	  SELECT fc.agentFileID,
             fc.seq,
             fc.content,
             fc.indent,
             fc.isNote,
             fc.appliesTo,
             fc.type			 
      FROM [dbo].[filecontent] fc
      WHERE fc.agentFileID = @agentFileID
    END
    
	SELECT * FROM #fileContent 
	     
	DROP TABLE #fileContent
END
