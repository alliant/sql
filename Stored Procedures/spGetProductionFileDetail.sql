/****** Object:  StoredProcedure [dbo].[spGetProductionFileDetail]    Script Date: 7/16/2024 12:48:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetProductionFileDetail]
  @agentFileID INTEGER    = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

   CREATE TABLE #fileInvArTrxs(agentfileid  VARCHAR(80),
							   fileInvID    INTEGER,
						       amount     [DECIMAL](17, 2) NULL,
							   trxDetail    VARCHAR(50),
							   description  VARCHAR(100),
							   artranID     INTEGER,
							   type         VARCHAR(2),
							   checkNumber  VARCHAR(100),
							   appliedBy    VARCHAR(100),
							   appliedDate  DATE,
						       trandate     DATE  
						        )						     

   /* get fileInv records within the reporting period */
    INSERT INTO #fileInvArTrxs ([agentfileid],[fileInvID],[amount],[trxDetail],[trandate])
   	/*Now pick the file Invoice Items from fileInv and fileInvItem table */
	SELECT fi.agentfileid,
	       fi.fileinvid,
		   fii.amount,
		   fii.description,
		   fi.invoiceDate
	FROM  fileInv fi
	INNER JOIN fileInvitem fii ON (fii.fileinvid = fi.fileinvid)
	WHERE fi.agentfileid = @agentFileID
	  AND fi.posted = 1
	  AND fi.invoiceDate IS NOT NULL

	/* fetch fileAr transactions W or A */
	INSERT INTO #fileInvArTrxs ([agentfileid],[artranID],[amount],[description],[type],[trxDetail],[trandate])
	SELECT fa.agentFileID,
	       fa.arTranID,
		   fa.amount,
		   fa.cancelreason,
		   fa.type,
		   (CASE WHEN fa.type = 'A' then 'Payment Applied' else 'Write-off' END),
		   fa.tranDate
	FROM  fileAR fa
    WHERE fa.agentFileID  = @agentFileID
	  AND (fa.type = 'W' or fa.type = 'A')
	  AND  fa.tranDate IS NOT NULL

	  /* check number update  */
     UPDATE #fileInvArTrxs
	    SET #fileInvArTrxs.checkNumber = at.reference,
		    #fileInvArTrxs.appliedBy = (select name from sysuser where sysuser.uid = at.createdBy),
		    #fileInvArTrxs.appliedDate = at.createdDate
	 FROM artran at 
	  WHERE (at.artranID = #fileInvArTrxs.artranID)
        AND #fileInvArTrxs.type = 'A'

	/* updates the check number */
	SELECT * FROM #fileInvArTrxs
END
