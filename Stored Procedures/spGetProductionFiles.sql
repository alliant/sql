
/****** Object:  StoredProcedure [dbo].[spGetProductionFiles]    Script Date: 6/19/2024 10:09:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author:		<Rahul Sharma>
-- Create date: <16-11-2020>
-- Description:	<Get artran records>
-- =============================================
CREATE PROCEDURE [dbo].[spGetProductionFiles] 
	-- Add the parameters for the stored procedure here
	@pcEntityID                   VARCHAR(50) = 'ALL',	
	/*------------applicable for file-----------*/
	@plIncludeFullyPaidFile       BIT         = 0,	
    /*---------------date range-----------------*/	
    @pFromPostDate          DATETIME          = NULL,
	@pToPostDate            DATETIME          = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- Insert statements for procedure here
	CREATE TABLE #prodFile (fileARID        INTEGER,
	                        agentFileID     INTEGER,
							agentID         VARCHAR(50),
						    agentName       VARCHAR(200),
						    fileID          VARCHAR(50),
							fileNumber      VARCHAR(50),
	                        invoiceDate     DATETIME,
							invoicedAmount  [DECIMAL](18, 2) NULL,
							cancelledAmount	[DECIMAL](18, 2) NULL,
                            appliedAmount   [DECIMAL](18, 2) NULL,
						     ) 

    CREATE TABLE #AgentFile (agentfileid integer, agentID varchar(50), fileID VARCHAR(50))  
	
	/* If date range is provided, then select the files having any transaction in the date range. */
	IF @pFromPostDate IS NOT NULL and @pToPostDate IS NOT NULL
	BEGIN 
		if @plIncludeFullyPaidFile = 1 
		begin
			INSERT INTO #AgentFile(agentfileid, agentid, fileID) 
			(SELECT distinct(agentfileid), agentid, fileID FROM fileAR at 
				WHERE at.agentID  = (CASE WHEN @pcEntityID = 'ALL' THEN at.agentID ELSE @pcEntityID END)
				--AND at.type = 'I'
				AND (at.tranDate >= @pFromPostDate AND at.tranDate <= @pToPostDate)
			)
		end 
		else
		begin
			INSERT INTO #AgentFile(agentfileid, agentid, fileID) 
			(SELECT distinct(agentfileid), agentid, fileID FROM fileAR at 
				WHERE at.agentID  = (CASE WHEN @pcEntityID = 'ALL' THEN at.agentID ELSE @pcEntityID END)
				--AND at.type = 'I'
				AND (at.tranDate >= @pFromPostDate AND at.tranDate <= @pToPostDate)
				and exists (select 1 from fileAR FA where FA.agentfileid =  at.agentfileID 
				            and FA.type = 'F' and FA.seq = 0) )
			
        end 
	END
	else
	begin
		/* Select the open files, with non zero balance. */
		INSERT INTO #AgentFile(agentfileid, agentid, fileID) 
			(SELECT agentfileid, agentid, fileID FROM fileAR at WHERE at.agentID  = (CASE WHEN @pcEntityID = 'ALL' THEN at.agentID ELSE @pcEntityID END)
											AND at.type = 'F'
											AND at.seq = 0)

	
		/* Select the files with zero balance if user wants to see fully paid files */
		if @plIncludeFullyPaidFile = 1
		begin
		INSERT INTO #AgentFile(agentfileid, agentid, fileID)
			(select distinct(agentfileid), agentid, fileid from fileAR at
				where at.agentID = (CASE WHEN @pcEntityID = 'ALL' THEN at.agentID ELSE @pcEntityID END)
				and at.type = 'I'
				and not exists (select fileid from fileAR PF1 
								where PF1.agentfileid = at.agentfileid
								and PF1.type = 'F'
								and PF1.seq = 0 ) 
				and ((@pFromPostDate IS NULL OR @pToPostDate IS NULL)
					  OR 
					  at.agentfileID IN (SELECT DISTINCT agentfileID FROM #AgentFile)
					)
				)
		end
	end

	INSERT INTO #prodFile (agentfileid,fileARID, agentID, fileid, fileNumber, invoiceDate, invoicedAmount , appliedAmount, cancelledAmount,agentName)
	select 
	distinct(AF.agentfileid),
	(select min (FA.filearID) from fileAR FA where FA.agentfileID = fileAR.agentfileid and FA.type = 'I') as fileARID,
	AF.agentid,
	AF.fileID,
	agentfile.fileNumber as fileNumber,
	(select min (FA.tranDate) from fileAR FA where FA.agentfileID = fileAR.agentfileid and FA.type = 'I') as invoiceDate,
	(select sum(FA.amount) from fileAR FA where FA.agentfileID = fileAR.agentfileid and FA.type = 'I') as invoicedAmount,
	(select sum(FA.amount) from fileAR FA where FA.agentfileID = fileAR.agentfileid and FA.type = 'A') as appliedAmount,
	(select sum(FA.amount) from fileAR FA where FA.agentfileID = fileAR.agentfileid and FA.type = 'W') as cancelledAmount,
	agent.name as agentName 
	from fileAR
	inner join #AgentFile AF on AF.agentfileid = fileAR.agentFileID
	left outer join agent on agent.agentID = AF.agentID 
	left outer join agentfile on agentfile.agentFileID = af.agentfileid
	
	select * from #prodFile
	drop table #prodFile 

end