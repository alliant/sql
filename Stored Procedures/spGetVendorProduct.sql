/****** Object:  StoredProcedure [dbo].[spGetVendorProduct]    Script Date: 12/2/2022 4:47:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetVendorProduct] 
	-- Add the parameters for the stored procedure here
	  @vendorID VARCHAR(50) = '',
	  @productID INTEGER = 0
AS
BEGIN
	CREATE TABLE #tempVendorProduct
	(
		[vendorID]    VARCHAR(50),
		[productID]   INTEGER,
		[productDesc] VARCHAR(80),
		[price]       DECIMAL(18,2),
		[vendorName]  VARCHAR(80)
	)


	INSERT INTO #tempVendorProduct([vendorID], [productID],[productDesc],[price],[vendorName])
	SELECT a.[vendorID],
		   a.[productID],
		   b.[description],
		   a.[price],
		   c.[vendorname]
	FROM [dbo].[vendorproduct] a INNER JOIN 
	     [dbo].[product] b
	ON a.[productid] = b.[productid]
	  and a.[vendorID]  =  (CASE WHEN @vendorID != '' THEN @vendorID ELSE a.vendorid END)	
	  and a.[productid] =  (CASE WHEN @productID != 0 THEN @productID ELSE  a.productid END)
	INNER JOIN [dbo].[vendor] c
	ON a.[vendorid] = c.[vendorid]

	SELECT * FROM #tempVendorProduct

	DROP TABLE #tempVendorProduct	
END