USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spCPLCountDiscrepancyForRepFiles]    Script Date: 8/8/2019 2:45:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spCPLCountDiscrepancyForRepFiles]
	-- Add the parameters for the stored procedure here  
  @agentID VARCHAR(MAX) = '',
  @fileNumber VARCHAR(MAX) = '',
  @StartDate DATE = NULL,
  @EndDate DATE = NULL  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	
  -- Insert statements for procedure here
  if (@agentID <> '')
  BEGIN
    SELECT bf.fileNumber,
	       bf.formID,	       
		   bf.noOfCPLReported,
		   bf.revReported,
		   csf.noOfCPLIssued,
		   csf.revExpected		   
		FROM 
		
		(SELECT fileNumber, fileNumberID, formID, Sum(formCount) AS noOfCPLReported, SUM(netPremium) AS revReported FROM batchForm WHERE batchID IN
		 (SELECT batchID FROM batch WHERE batch.agentID = @agentID
		 							  AND batch.createDate >= @Startdate)
		 AND formType = 'C'
         GROUP BY fileNumber, fileNumberID, formID) AS bf
		 LEFT JOIN
		(SELECT fileNumber, c.formID, fileNumberID, COUNT(*) AS noOfCPLIssued, SUM(minGross) AS revExpected FROM 
		  (SELECT fileNumber, fileNumberID, stateID, formID FROM cpl WHERE cpl.agentID = @agentID
			 								        AND  cpl.issueDate BETWEEN @StartDate AND @EndDate
			 									    AND  (cpl.fileNumber LIKE '@fileNumber%' or
														  cpl.fileNumberID LIKE '@fileNumber%')) AS c
          LEFT JOIN
		  (SELECT stateID, formID, minGross FROM stateform where formType = 'C') AS sf		 
		  ON c.stateID = sf.stateID AND c.formID = sf.formID
		 GROUP BY fileNumber, fileNumberID, c.formID) AS csf 
		 ON bf.fileNumberID = csf.fileNumberID AND bf.formID = csf.formID AND bf.noOfCPLReported <> csf.noOfCPLIssued 
  END 
END