USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spCorrectBatch]    Script Date: 1/12/2017 8:36:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spCorrectBatch]
	-- Add the parameters for the stored procedure here
	@oldFile VARCHAR(200) = NULL,
  @newFile VARCHAR(200) = NULL,
  @batch INTEGER = 0,
  @policy INTEGER = 0,
  @GPPolicy VARCHAR(30) = '',
  @numSpaces INTEGER = 0
AS
BEGIN
  --make sure that the old file parameter is passed in
  IF @oldFile IS NULL OR @oldFile = ''
  BEGIN
    RAISERROR(N'Please enter an old file name',10,1)
    RETURN
  END

  --make sure that the new file parameter is passed in
  IF @newFile IS NULL OR @newFile = ''
  BEGIN
    RAISERROR(N'Please enter a new file name',10,1)
    RETURN
  END
  
  --make sure that the policy parameter is passed in
  IF @policy = 0
  BEGIN
    RAISERROR(N'Please enter a policy number',10,1)
    RETURN
  END

  PRINT @GPPolicy

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  --validation tables
  CREATE TABLE #validTable (DBName VARCHAR(20), validType VARCHAR(20),TableName VARCHAR(30),Policy VARCHAR(30),FileNumber VARCHAR(30))
  CREATE TABLE #gpTable (seq INTEGER,tbl VARCHAR(30),invCol VARCHAR(30),fileCol VARCHAR(30))

  DECLARE @tbl VARCHAR(30),
          @invCol VARCHAR(30),
          @fileCol VARCHAR(30),
          @seq INTEGER,
          @sql NVARCHAR(max);

  INSERT INTO #gpTable (seq,tbl,invCol,fileCol) VALUES (1,'RM20101','DOCNUMBR','CSPORNBR')
  INSERT INTO #gpTable (seq,tbl,invCol,fileCol) VALUES (2,'RM30101','DOCNUMBR','CSPORNBR')
  INSERT INTO #gpTable (seq,tbl,invCol,fileCol) VALUES (3,'SOP30200','SOPNUMBE','CSTPONBR')
  SELECT @seq = min(seq) from #gpTable

  WHILE @seq IS NOT NULL
  BEGIN
    SELECT @tbl = [tbl], @invCol = [invCol], @fileCol = [fileCol] FROM #gpTable WHERE [seq] = @seq
    SET @sql = N'SELECT ''ANTIC'',''Pre-Validation'',''' + @tbl + ''',[' + @invCol + '],[' + @fileCol + '] FROM ANTIC.dbo.[' + @tbl + '] WHERE [' + @invCol + '] = ''' + @GPPolicy + ''' AND RTRIM([' + @fileCol + ']) = ''' + @oldFile + ''' AND [BACHNUMB] = ''' + CONVERT(VARCHAR,@batch) + ''''
    INSERT INTO #validTable
    EXEC sp_executesql @sql

    IF (@@ROWCOUNT > 0)
    BEGIN
      --update
      SET @sql = N'UPDATE ANTIC.dbo.[' + @tbl + '] SET [' + @fileCol + '] = ''' + @newFile + REPLICATE(' ',@numSpaces) + ''' WHERE [' + @invCol + '] = ''' + @GPPolicy + ''' AND RTRIM([' + @fileCol + ']) = ''' + @oldFile + ''' AND [BACHNUMB] = ''' + CONVERT(VARCHAR,@batch) + ''''
      EXEC sp_executesql @sql

      --validation
      SET @sql = N'SELECT ''ANTIC'',''Post-Validation'',''' + @tbl + ''',[' + @invCol + '],[' + @fileCol + '] FROM ANTIC.dbo.[' + @tbl + '] WHERE [' + @invCol + '] = ''' + @GPPolicy + ''' AND RTRIM([' + @fileCol + ']) = ''' + @newFile + ''' AND [BACHNUMB] = ''' + CONVERT(VARCHAR,@batch) + ''''
      INSERT INTO #validTable
      EXEC sp_executesql @sql
    END

    SELECT @seq = min([seq]) FROM #gpTable WHERE [seq] > @seq
  END

  --update COMPASS batchform
  INSERT INTO #validTable
  SELECT 'COMPASS','Pre-Validation','batchform',[policyID],[fileNumber] FROM [COMPASS].[dbo].[batchform] WHERE [policyID] = @policy AND [fileNumber] = @oldFile AND [batchID] = @batch

  IF @@ROWCOUNT > 0
  BEGIN
    --update
    UPDATE [dbo].[batchform] SET [fileNumber] = @newFile WHERE [policyID] = @policy AND [fileNumber] = @oldFile AND [batchID] = @batch

    --validation
    INSERT INTO #validTable
    SELECT 'COMPASS','Post-Validation','batchform',[policyID],[fileNumber] FROM [COMPASS].[dbo].[batchform] where [policyID] = @policy AND [fileNumber] = @newFile AND [batchID] = @batch
  END
  
  INSERT INTO #validTable
  SELECT 'COMPASS','Pre-Validation','policy',[policyID],[fileNumber] FROM [dbo].[policy] WHERE [policyID] = @policy AND [fileNumber] = @oldFile
  
  --update COMPASS policy
  IF @@ROWCOUNT > 0
  BEGIN
    --update
    UPDATE [dbo].[policy] SET [fileNumber] = @newFile WHERE [policyID] = @policy AND [fileNumber] = @oldFile

    --validation
    INSERT INTO #validTable
    SELECT 'COMPASS','Post-Validation','policy',[policyID],[fileNumber] FROM [dbo].[policy] WHERE [policyID] = @policy AND [fileNumber] = @newFile
  END

  SELECT DISTINCT * FROM #validTable ORDER BY [validType] DESC,[DBName],[TableName]
END