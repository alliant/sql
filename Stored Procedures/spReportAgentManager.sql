USE [COMPASS]
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportAgentManager]
	-- Add the parameters for the stored procedure here
	@stateID VARCHAR(200) = 'ALL',
    @periodID INTEGER = 0,
    @UID varchar(100)
AS
BEGIN
   DECLARE @agentID VARCHAR(MAX) = 'ALL'

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    SET @agentID = [dbo].[StandardizeAgentID](@agentID);
  CREATE TABLE #stateTable ([stateID] VARCHAR(2))
  INSERT INTO #stateTable ([stateID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('State', @stateID)

  IF (@periodID = 0)
    SELECT @periodID = MAX([periodID]) FROM [dbo].[period] WHERE [active] = 0

  SELECT  a.[agentID],
          a.[stateID],
          a.[name],
          a.[stat],
          a.[uid] AS [manager],
          a.[effDate],
          a.[managerType],
          a.[managerStat],
          ISNULL(b.[numForms],0) AS [numForms],
          ISNULL(b.[numPolicies],0) AS [numPolicies],
          ISNULL(b.[grossPremium],0) AS [grossPremium],
          ISNULL(b.[netPremium],0) AS [netPremium]
  FROM    (
          SELECT  a.[agentID],
                  a.[stateID],
                  a.[name],
                  a.[stat],
                  am.[uid],
                  am.[effDate],
                  am.[stat] AS [managerStat],
                  CASE WHEN am.[isPrimary] = 1 THEN '1' ELSE '0' END AS [managerType]
          FROM    [dbo].[agent] a INNER JOIN
                  (
                  SELECT  am.[agentID],
                          am.[uid],
                          am.[isPrimary],
                          am.[effDate],
                          am.[stat],
                          ISNULL((SELECT [periodID] FROM [dbo].[period] WHERE am.[effDate] BETWEEN [startDate] AND [endDate]),0) AS [effPeriod],
                          am.[expDate],
                          ISNULL((SELECT [periodID] FROM [dbo].[period] WHERE am.[expDate] BETWEEN [startDate] AND [endDate]),999999) AS [expPeriod]
                  FROM    [dbo].[agentmanager] am
                  ) am
          ON      a.[agentID] = am.[agentID] INNER JOIN
                  #stateTable st
          ON      a.[stateID] = st.[stateID]
          WHERE   @periodID BETWEEN [effPeriod] AND [expPeriod]
		  AND     ((a.[agentID] = (CASE  WHEN @agentID != 'ALL' THEN @agentID ELSE a.[agentID] END)) 
          AND     (dbo.CanAccessAgent(@UID ,a.[agentID]) = 1))
          ) a LEFT OUTER JOIN
          (
          SELECT  b.[agentID],
                  SUM(CASE WHEN bf.[formType] = 'E' THEN 1 ELSE 0 END) AS [numForms],
                  SUM(CASE WHEN bf.[formType] = 'P' THEN 1 ELSE 0 END) AS [numPolicies],
                  SUM(bf.[grossDelta]) AS [grossPremium],
                  SUM(bf.[netDelta]) AS [netPremium]
          FROM    [dbo].[batch] b INNER JOIN
                  [dbo].[batchform] bf
          ON      b.[batchID] = bf.[batchID]
          WHERE   b.[periodID] = @periodID
          AND     b.[stat] = 'C'
          GROUP BY b.[agentID]
          ) b
  ON      b.[agentID] = a.[agentID]
  ORDER BY a.[agentID],a.[managerType]
END
GO
