GO
/****** Object:  StoredProcedure [dbo].[spAlertCPLVolume]    Script Date: 7/5/2018 4:24:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spAlertPolicyVolume]
	-- Add the parameters for the stored procedure here
  @agentID VARCHAR(MAX) = '',
  @user VARCHAR(100) = 'compass@alliantnational.com',
  @preview BIT = 0,
  @unremmited DECIMAL(18,2) = 0.0,
  @threeMonthAverage DECIMAL(18,2) = 0.0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;
  
  CREATE TABLE #agentTable ([agentID] VARCHAR(30))
  INSERT INTO #agentTable ([agentID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)

  DECLARE  -- Used for the preview
          @source VARCHAR(30) = 'OPS',
          @processCode VARCHAR(30) = 'OPS04',
          -- Calculate the three month average and the score
          @period1 INTEGER = 0,
          @period2 INTEGER = 0,
          @period3 INTEGER = 0,
          @date DATETIME = NULL,
          @score DECIMAL(18,2) = 0,
          @count INTEGER = 0,
          -- Notes for the alert note
          @note VARCHAR(MAX) = ''

  CREATE TABLE #batchTable ([agentID] VARCHAR(30), [total] DECIMAL(18,2), [score] DECIMAL(18,2))

  -- Get the period
  SELECT  @period1=MAX([periodID])
  FROM    [dbo].[period]
  WHERE   [active] = 0

  IF (@period1 IS NULL)
    RETURN 0

  SELECT  @period2=MAX([periodID])
  FROM    [dbo].[period]
  WHERE   [active] = 0
  AND     [periodID] <> @period1

  IF (@period2 IS NULL)
    SET @period2 = 0

  SELECT  @period3=MAX([periodID])
  FROM    [dbo].[period]
  WHERE   [active] = 0
  AND     [periodID] <> @period1
  AND     [periodID] <> @period2

  IF (@period3 IS NULL)
    SET @period3 = 0

  SELECT  @date=[endDate]
  FROM    [dbo].[period]
  WHERE   [periodID] = @period1

  -- Set the score
  INSERT INTO #batchTable ([agentID],[total],[score])
  SELECT  a.[agentID],
          a.[cnt] + ISNULL(b.[cnt],0) + ISNULL(c.[cnt],0),
          999
  FROM    (
          SELECT  a.[agentID],
                  COUNT(DISTINCT bf.[policyID]) AS [cnt]
          FROM    [dbo].[agent] a LEFT OUTER JOIN
                  [dbo].[batch] b
          ON      b.[agentID] = a.[agentID] LEFT OUTER JOIN
                  [dbo].[batchform] bf
          ON      b.[batchID] = bf.[batchID]
          WHERE   b.[periodID] = @period1
          AND     b.[stat] = 'C'
          GROUP BY a.[agentID]
          ) a LEFT OUTER JOIN
          (
          SELECT  a.[agentID],
                  COUNT(DISTINCT bf.[policyID]) AS [cnt]
          FROM    [dbo].[agent] a LEFT OUTER JOIN
                  [dbo].[batch] b
          ON      b.[agentID] = a.[agentID] LEFT OUTER JOIN
                  [dbo].[batchform] bf
          ON      b.[batchID] = bf.[batchID]
          WHERE   b.[periodID] = @period2
          AND     b.[stat] = 'C'
          GROUP BY a.[agentID]
          ) b
  ON      a.[agentID] = b.[agentID] LEFT OUTER JOIN
          (
          SELECT  DISTINCT
                  a.[agentID],
                  COUNT(DISTINCT bf.[policyID]) AS [cnt]
          FROM    [dbo].[agent] a LEFT OUTER JOIN
                  [dbo].[batch] b
          ON      b.[agentID] = a.[agentID] LEFT OUTER JOIN
                  [dbo].[batchform] bf
          ON      b.[batchID] = bf.[batchID]
          WHERE   b.[periodID] = @period3
          AND     b.[stat] = 'C'
          GROUP BY a.[agentID]
          ) c
  ON      b.[agentID] = c.[agentID]
  WHERE   a.[agentID] IN (SELECT [agentID] FROM #agentTable)

  SELECT @count = COUNT(*) FROM #batchTable
  IF (@count = 0)
    RETURN 0

  UPDATE  #batchTable
  SET     [score] = @unremmited / [total]
  WHERE   [total] > 0

  -- Insert alert or preview
  SELECT  @agentID = [agentID],
          @score = [score],
          @note = 'The calculation is Unremitted Count (' + [dbo].[FormatNumber] (@unremmited, 0) + ') / Last Three Month Remittance Total (' + [dbo].[FormatNumber] ([total], 0) + ')'
  FROM    #batchTable

  IF (@preview = 0)
    EXEC [dbo].[spInsertAlert] @source = @source, 
                               @processCode = @processCode, 
                               @user = @user, 
                               @agentID = @agentID, 
                               @score = @score,
                               @effDate = @date,
                               @note = @note
                               
  SELECT  @agentID AS [agentID],
          @source AS [source],
          @processCode AS [processCode],
          [dbo].[GetAlertThreshold] (@processCode, @score) AS [threshold],
          [dbo].[GetAlertThresholdRange] (@processCode, @score) AS [thresholdRange],
          [dbo].[GetAlertSeverity] (@processCode, @score) AS [severity],
          @score AS [score],
          [dbo].[GetAlertScoreFormat] (@processCode, @score) AS [scoreDesc],
          @date AS [effDate],
          [dbo].[GetAlertOwner] (@processCode) AS [owner],
          @note AS [note]

  DROP TABLE #batchTable
  DROP TABLE #agentTable
END
