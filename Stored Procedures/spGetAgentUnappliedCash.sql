USE [compass]
GO
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	<Rahul Sharma>
-- Create date: <22-12-2020>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetAgentUnappliedCash]
	-- Add the parameters for the stored procedure here
    @stateID     VARCHAR(MAX) = 'ALL',
	@agentID     VARCHAR(MAX) = 'ALL'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	CREATE TABLE #agentTable ([agentID] VARCHAR(30))
    INSERT INTO #agentTable ([agentID])
    SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)

    CREATE TABLE #stateTable ([stateID] VARCHAR(2))
    INSERT INTO #stateTable ([stateID])
    SELECT [field] FROM [dbo].[GetEntityFilter] ('State', @stateID)

	CREATE TABLE #agentunappliedCash (stateID      VARCHAR(2),
	     				              agentID      VARCHAR(80),
									  stat         VARCHAR(50),
						              name         VARCHAR(200),
						              manager      VARCHAR(80),
						              fileNumber   VARCHAR(50),
									  tranID       VARCHAR(50),
									  artranID     INTEGER,
								      checknum     VARCHAR(50),
                                      checkAmt     [decimal](17, 2) NULL,
						              remainingAmt [decimal](17, 2) NULL,
                                      appliedAmt   [decimal](17, 2) NULL,
						              refundedAmt  [decimal](17, 2) NULL,
								      checkDate    DATETIME,
								      receiptDate  DATETIME,
								      postDate     DATETIME,
								      depositRef   VARCHAR(50),
								      arCashglRef  VARCHAR(50),
								      arglref      VARCHAR(50))
						           

    INSERT INTO #agentunappliedCash ([stateID],[agentID],[stat],[name],[fileNumber],[tranID],[artranID],[checkAmt],[checkDate],[receiptDate],[checknum],[remainingAmt],[postDate],[arCashglRef],[arglref])
      SELECT a.stateID, 
	         at.entityID, 
			(SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'AMD' AND [objAction] = 'Agent' AND [objProperty] = 'Status' AND [objID] = a.[stat]) AS [stat],
			 a.name,
	         at.fileNumber, 
			 at.tranID,
			 at.arTranID,
	         at.tranAmt,
			 ap.checkDate,
			 ap.receiptDate,
			 ap.checknum,
			 at.remainingAmt,
			 at.postDate,
			 a.arCashglRef,
			 a.arglref
	  FROM [dbo].[artran] at 
		 INNER JOIN agent a
      ON a.agentID = at.entityID
	     INNER JOIN arpmt ap
      ON at.tranID = ap.arPmtID
      WHERE at.entity = 'A'
	    AND at.[entityID] IN (SELECT [agentID] FROM #agentTable)
		AND a.[stateID]   IN (SELECT [stateID] FROM #stateTable) 
	    AND at.type      = 'P'	    	    
		AND at.seq       = 0
		AND (at.void     = 0 or at.void = null)
		AND ap.void      = 0
		AND ap.archived  = 0

    UPDATE #agentunappliedCash
	  SET #agentunappliedCash.appliedAmt = (SELECT SUM(atn.tranAmt) FROM artran atn WHERE atn.entity     = 'A'
	                                                                                  and atn.entityID   = #agentunappliedCash.agentID
	                                                                                  and atn.sourceType = 'P'
		                                                                              and atn.sourceID   = CAST(#agentunappliedCash.arTranID as varchar)
											                                          and atn.type       = 'A'
		                                                                              GROUP BY atn.entityID,atn.sourceID),
         #agentunappliedCash.refundedAmt = (select sum(atn.tranAmt) from artran atn where atn.entity    = 'A'
	                                                                                  and atn.entityID  = #agentunappliedCash.agentID
		                                                                              and atn.tranID    = #agentunappliedCash.tranID
										                                              and atn.type      = 'P'
										                                              and atn.seq       > 0
		                                                                              GROUP BY atn.entityID,atn.tranID)          
	   

    UPDATE #agentunappliedCash
      SET #agentunappliedCash.remainingAmt = #agentunappliedCash.checkAmt + ISNULL(#agentunappliedCash.appliedAmt,0)  + ISNULL(#agentunappliedCash.refundedAmt,0)
    
	DELETE #agentunappliedCash WHERE #agentunappliedCash.remainingAmt <= 0

    UPDATE #agentunappliedCash
	  SET depositRef  = ad.depositRef
	  FROM arpmt ap
	  INNER JOIN
	  ardeposit ad
	  ON ad.depositID = ap.depositID
	  WHERE tranID = ap.arPmtID	  
	  
    UPDATE #agentunappliedCash
      SET manager = am.uid
	  FROM [dbo].[agentmanager] am
      WHERE #agentunappliedCash.agentID = am.[agentID]
        AND   am.[isPrimary]            = 1
        AND   am.[stat]                 = 'A'
	
    SELECT * FROM #agentunappliedCash
END
