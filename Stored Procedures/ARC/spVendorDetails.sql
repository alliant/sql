USE [alliant]
--USE [alliant_test]
--USE [dev_alliant]
GO
/****** Object:  StoredProcedure [dbo].[spGetAPIDetails]    Script Date: 5/24/2016 12:06:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spVendorDetails]
	-- Add the parameters for the stored procedure here
	@vendorID INT = 0,
  @year INT = 0,
  @month INT = 0,
  @type INT = 0,
  @status VARCHAR(20) = '',
  @state VARCHAR(2) = '',
  @agentID VARCHAR(20) = '',
  @fileNumber VARCHAR(200) = '',
  @user VARCHAR(50) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  DECLARE @typeName VARCHAR(50) = [dbo].GetAPIVendorType(@type)
  DECLARE @sql NVARCHAR(MAX)

  SET @sql = '
  SELECT  [TransactionLogID] AS ''Log ID''
         ,[TimeStamp] AS ''Timestamp''
         ,[TransactionServiceType] AS ''Status''
         ,[cintid] AS ''Agent ID''
         ,[Agency] AS ''Agency''
         ,[State]
         ,[Product]
         ,[ProductID] AS ''Product ID''
         ,[FileNumber] AS ''File Number''
         ,[ProductCode] AS ''Product Code''
         ,[ProductType] AS ''Product Type''
         ,[Signatory]
         ,[AttorneyID] AS ''Attorney ID''
         ,[Actor] AS ''Vendor''
         ,[UserID] AS ''User''
  FROM    (
          SELECT  t.[TransactionLogID],
                  t.[TimeStamp],
                  t.[TransactionServiceType],
                  b.[cintid],
                  b.[Agency],
                  ''CPL'' AS ''Product'',
                  t.[CPLNumber] AS ''ProductID'',
                  t.[GFNumber] AS ''FileNumber'',
                  c.[Code] AS ''ProductCode'',
                  l.[CPLType] AS ''ProductType'',
                  c.[AttorneyID],
                  t.[Actor],
                  t.[UserID],
                  s.[state_abbr] AS ''State'',
                  ''N/A'' AS ''Signatory''
          FROM    [dbo].[TransactionLog] t INNER JOIN
                  [dbo].[t_icl] c
          ON      t.[CPLNumber] = c.[iclid] INNER JOIN
                  [dbo].[t_ClosingLetter] l
          ON      l.[ClosingLetterID] = c.[ClosingLetterID] INNER JOIN
                  [dbo].[t_company] b
          ON      c.[EscrowID] = b.[cid] INNER JOIN
                  [dbo].[t_states] s
          ON      c.[StateName] = s.[state_name]
          WHERE   t.[Actor] IN (' + [dbo].GetAPIVendorName(@vendorID) + ')' +
          [dbo].GetAPIVendorFilter(@typeName,@year,@month) + '
          UNION
          SELECT  t.[TransactionLogID],
                  t.[TimeStamp],
                  t.[TransactionServiceType],
                  b.[cintid],
                  b.[Agency],
                  ''Policy Jacket'',
                  t.[PolicyNumber],
                  p.[filenumber],
                  ''N/A'',
                  p.[policytype],
                  0,
                  t.[Actor],
                  t.[UserID],
                  ISNULL(pp.[State],''N/A''),
                  s.[Name]
          FROM    [dbo].[TransactionLog] t INNER JOIN
                  [dbo].[t_policies] p
          ON      t.[PolicyNumber] = p.[policyid] INNER JOIN
                  [dbo].[t_company] b
          ON      p.[cid] = b.[cid] LEFT OUTER JOIN
                  [dbo].[Signatory] s
          ON      p.[SignatoryID] = s.[Id] INNER JOIN
                  [dbo].[PolicyProperties] pp
          ON      p.[policyid] = pp.[policyid]
          WHERE   t.[Actor] IN (' + [dbo].GetAPIVendorName(@vendorID) + ')' +
          [dbo].GetAPIVendorFilter(@typeName,@year,@month) + '
          ) a
  WHERE   1 = 1'
  -- add the status filter
  IF (@status != '')
  BEGIN
    SET @sql = @sql + '
    AND   [TransactionServiceType] = ''' + @status + ''''
  END
  -- add the state filter
  IF (@state != '')
  BEGIN
    SET @sql = @sql + '
    AND   [State] = ''' + @state + ''''
  END
  -- add the file number filter
  IF (@fileNumber != '')
  BEGIN
    SET @sql = @sql + '
    AND   [FileNumber] = ''' + @fileNumber + ''''
  END
  -- add the agent filter
  IF (@agentID != '')
  BEGIN
    SET @sql = @sql + '
    AND   [cintid] = ''' + @agentID + ''''
  END
  -- add the user filter
  IF (@user != '')
  BEGIN
    SET @sql = @sql + '
    AND   [UserID] = ''' + @user + ''''
  END
  SET @sql = @sql + '
    ORDER BY [TimeStamp] DESC'
  
  PRINT @sql
  EXEC sp_executesql @sql

END