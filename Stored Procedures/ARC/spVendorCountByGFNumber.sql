USE [alliant]
--USE [alliant_test]
--USE [dev_alliant]
GO
/****** Object:  StoredProcedure [dbo].[spVendorCountByGFNumber]    Script Date: 5/24/2016 1:34:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spVendorCountByGFNumber]
	-- Add the parameters for the stored procedure here
	@vendorID INT = 0,
  @year INT = 0,
  @month INT = 0,
  @type INT = 0,
  @fileNumber VARCHAR(200) = ''

AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  DECLARE @typeName VARCHAR(50) = [dbo].GetAPIVendorType(@type)
  DECLARE @sql NVARCHAR(MAX)

  CREATE TABLE #fileTable (fileNumber VARCHAR(50),actor VARCHAR(50),stat VARCHAR(50),yr INT,mth INT,cnt INT)
  
  IF (@type = 0 OR @typeName = 'cplIssue') -- All or CPL
  BEGIN
    -- CPL query
    SET @sql = '
    SELECT  c.[GFNumber]
            ,t.[Actor]
            ,t.[TransactionServiceType]
            ,YEAR(t.[TimeStamp])
            ,MONTH(t.[TimeStamp]) 
            ,COUNT(c.[GFNumber])
    FROM      [dbo].[TransactionLog] t INNER JOIN
              [dbo].[t_icl] c
    ON      t.[CPLNumber] = c.[iclid]
    WHERE   t.[Actor] IN (' + [dbo].GetAPIVendorName(@vendorID) + ')' +
    [dbo].GetAPIVendorFilter(@typeName,@year,@month) + '
    AND     t.[TransactionServiceType] <> ''cplVoid'''
  END

  -- add the file numer filter
  IF (@fileNumber != '')
  BEGIN
    SET @sql = @sql + '
    AND     c.[GFNumber] = ''' + @fileNumber + ''''
  END
  SET @sql = @sql + '
    GROUP BY c.[GFNumber],t.[Actor],t.[TransactionServiceType],YEAR(t.[TimeStamp]),MONTH(t.[TimeStamp])
    ORDER BY c.[GFNumber],YEAR(t.[TimeStamp]),MONTH(t.[TimeStamp]),t.[TransactionServiceType]'

  INSERT INTO #fileTable (fileNumber,actor,stat,yr,mth,cnt)
  EXEC sp_executesql @sql
  
  IF (@type = 0 OR @typeName = 'policyIsue') -- All or Policy
  BEGIN
    -- policy jacket query
    SET @sql = '
    SELECT  p.[filenumber],
            t.[Actor],
            t.[TransactionServiceType],
            YEAR(t.[TimeStamp]),
            MONTH(t.[TimeStamp]),
            COUNT(p.[filenumber])
    FROM    [dbo].[TransactionLog] t INNER JOIN
            [dbo].[t_policies] p
    ON      t.[PolicyNumber] = p.[policyid]
    WHERE   t.[Actor] IN (' + [dbo].GetAPIVendorName(@vendorID) + ')' +
    [dbo].GetAPIVendorFilter(@typeName,@year,@month)
  END

  -- add the file numer filter
  IF (@fileNumber != '')
  BEGIN
    SET @sql = @sql + '
    AND     p.[filenumber] = ''' + @fileNumber + ''''
  END
  SET @sql = @sql + '
    GROUP BY p.[filenumber],t.[Actor],t.[TransactionServiceType],YEAR(t.[TimeStamp]),MONTH(t.[TimeStamp])
    ORDER BY p.[filenumber],YEAR(t.[TimeStamp]),MONTH(t.[TimeStamp]),t.[TransactionServiceType]'

  PRINT @sql
  INSERT INTO #fileTable (fileNumber,actor,stat,yr,mth,cnt)
  EXEC sp_executesql @sql

  SELECT  [fileNumber] AS 'File Number',
          [actor] AS 'Vendor',
          [dbo].GetAPIVendorStatus([stat]) AS 'Status',
          [yr] AS 'Year',
          [mth] AS 'Month',
          [cnt] AS 'Total Processed'
  FROM    #fileTable
  ORDER BY [fileNumber],[vendor],[yr],[mth],[stat]
	SET NOCOUNT ON;
END
