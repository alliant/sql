USE [alliant]
--USE [alliant_test]
--USE [dev_alliant]
GO
/****** Object:  StoredProcedure [dbo].[spVendorDetailsByAgent]    Script Date: 5/24/2016 1:01:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spVendorCountByAgent]
	-- Add the parameters for the stored procedure here
	@vendorID INT = 0,
  @year INT = 0,
  @month INT = 0,
  @type INT = 0,
  @agentID VARCHAR(20) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  DECLARE @typeName VARCHAR(50) = [dbo].GetAPIVendorType(@type)
  DECLARE @sql NVARCHAR(MAX)

  CREATE TABLE #agentTable (officeID INT,agentID VARCHAR(50), agency VARCHAR(500),actor VARCHAR(50),stat VARCHAR(50),yr INT,mth INT,cnt INT)
  
  IF (@type = 0 OR @type = 1) -- All or CPL
  BEGIN
    -- cpl query
    SET @sql = '
    SELECT  b.[cid],
            b.[cintid],
            b.[Agency],
            t.[Actor],
            t.[TransactionServiceType],
            YEAR(t.[TimeStamp]),
            MONTH(t.[TimeStamp]),
            COUNT(b.[cid])
    FROM    [dbo].[TransactionLog] t INNER JOIN
            [dbo].[t_icl] c
    ON      t.[CPLNumber] = c.[iclid] INNER JOIN
            [dbo].[t_company] b
    ON      c.[EscrowID] = b.[cid]
    WHERE   t.[Actor] IN (' + [dbo].GetAPIVendorName(@vendorID) + ')' +
    [dbo].GetAPIVendorFilter(@typeName,@year,@month) + '
    AND     t.[TransactionServiceType] <> ''cplVoid'''
  END

  -- add the agent filter
  IF (@agentID != '')
  BEGIN
    SET @sql = @sql + '
    AND     b.[cintid] = ''' + @agentID + ''''
  END
  SET @sql = @sql + '
    GROUP BY b.[cid],b.[cintid],b.[Agency],t.[Actor],t.[TransactionServiceType],YEAR(t.[TimeStamp]),MONTH(t.[TimeStamp])
    ORDER BY b.[cintid],YEAR(t.[TimeStamp]),MONTH(t.[TimeStamp]),t.[TransactionServiceType]'

  INSERT INTO #agentTable (officeID,agentID,agency,actor,stat,yr,mth,cnt)
  EXEC sp_executesql @sql
  
  IF (@type = 0 OR @type = 2) -- All or PJ
  BEGIN
    -- policy jacket query
    SET @sql = '
    SELECT  b.[cid],
            b.[cintid],
            b.[Agency],
            t.[Actor],
            t.[TransactionServiceType],
            YEAR(t.[TimeStamp]),
            MONTH(t.[TimeStamp]),
            COUNT(b.[cid])
    FROM    [dbo].[TransactionLog] t INNER JOIN
            [dbo].[t_policies] p
    ON      t.[PolicyNumber] = p.[policyid] INNER JOIN
            [dbo].[t_company] b
    ON      p.[cid] = b.[cid]
    WHERE   t.[Actor] IN (' + [dbo].GetAPIVendorName(@vendorID) + ')' +
    [dbo].GetAPIVendorFilter(@typeName,@year,@month)
  END

  -- add the agent filter
  IF (@agentID != '')
  BEGIN
    SET @sql = @sql + '
    AND     b.[cintid] = ''' + @agentID + ''''
  END
  SET @sql = @sql + '
    GROUP BY b.[cid],b.[cintid],b.[Agency],t.[Actor],t.[TransactionServiceType],YEAR(t.[TimeStamp]),MONTH(t.[TimeStamp])
    ORDER BY b.[cintid],YEAR(t.[TimeStamp]),MONTH(t.[TimeStamp]),t.[TransactionServiceType]'

  INSERT INTO #agentTable (officeID,agentID,agency,actor,stat,yr,mth,cnt)
  EXEC sp_executesql @sql

  SELECT  [officeID] AS 'Office ID',
          [agentID] as 'Agent ID',
          [agency] as 'Agency',
          [actor] AS 'Vendor',
          [dbo].GetAPIVendorStatus([stat]) AS 'Status',
          [yr] AS 'Year',
          [mth] AS 'Month',
          [cnt] AS 'Total Processed'
  FROM    #agentTable
  ORDER BY [officeID],[agency],[vendor], [yr], [mth], [stat]
END
