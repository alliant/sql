USE [dev_alliant]
USE [alliant_test]
USE [alliant]
GO
/****** Object:  StoredProcedure [dbo].[spVendorCount]    Script Date: 6/8/2017 3:53:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spVendorCountByDate]
	-- Add the parameters for the stored procedure here
	@vendorID INT = 0,
  @startDate DATETIME = NULL,
  @endDate DATETIME = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  IF (@endDate IS NULL)
    SET @endDate = GETDATE()
  SET @endDate = DATEADD(d,1,@endDate)

  IF (@startDate IS NULL)
    SET @startDate = DATEADD(m,-1,@endDate)

  DECLARE @sql NVARCHAR(MAX)

  CREATE TABLE #finalTable (vendor VARCHAR(50), startDate DATETIME, endDate DATETIME, stat VARCHAR(50), total INT, gfTotal INT)
  
  SET @sql = '
  SELECT  [Source] AS [Actor],
          ''' + CONVERT(VARCHAR,@startDate,120) + ''' as [startDate],
          ''' + CONVERT(VARCHAR,@endDate,120) + ''' as [endDate],
          ''cplIssue'',
          COUNT(*) AS [cnt]
  FROM    [dbo].[t_ICL]
  WHERE   [Source] IN (' + [dbo].GetAPIVendorName(@vendorID) + ')
  AND     [ICLDate] BETWEEN ''' + CONVERT(VARCHAR,@startDate,120) + ''' AND ''' + CONVERT(VARCHAR,@endDate,120) + '''
  GROUP BY [Source]
  UNION ALL
  SELECT  [Source] AS [Actor],
          ''' + CONVERT(VARCHAR,@startDate,120) + ''' as [startDate],
          ''' + CONVERT(VARCHAR,@endDate,120) + ''' as [endDate],
          ''policyIssue'',
          COUNT(*) AS [cnt]
  FROM    [dbo].[t_policies]
  WHERE   [Source] IN (' + [dbo].GetAPIVendorName(@vendorID) + ')
  AND     [used] BETWEEN ''' + CONVERT(VARCHAR,@startDate,120) + ''' AND ''' + CONVERT(VARCHAR,@endDate,120) + '''
  GROUP BY [Source]'

  PRINT @sql
  INSERT INTO #finalTable (vendor,startDate,endDate,stat,total)
  EXEC sp_executesql @sql

  UPDATE  final
  SET     gfTotal = b.[cnt]
  FROM    (
          SELECT  a.[Actor],
                  a.[stat],
                  COUNT(a.[cnt]) AS [cnt]
          FROM    (
                  SELECT  [Source] AS [Actor],
                          'cplIssue' AS [stat],
                          COUNT(*) AS [cnt]
                  FROM    [dbo].[t_ICL] c
                  WHERE   c.[ICLDate] BETWEEN CONVERT(VARCHAR,@startDate,120) AND CONVERT(VARCHAR,@endDate,120)
                  GROUP BY c.[GFNumber],c.[Source]
                  ) a
          GROUP BY a.[Actor],a.[stat]
          ) b INNER JOIN
          #finalTable AS final
          ON  final.[vendor] = b.[Actor]
          AND final.[stat] = b.[stat]

  UPDATE  final
  SET     gfTotal = b.[cnt]
  FROM    (
          SELECT  a.[Actor]
                  ,a.[stat]
                  ,COUNT(a.[cnt]) AS cnt
          FROM    (
                  SELECT  [Source] AS [Actor],
                          'policyIssue' AS [stat],
                          COUNT(*) AS [cnt]
                  FROM    [dbo].[t_policies]
                  WHERE   [used] BETWEEN CONVERT(VARCHAR,@startDate,120) AND CONVERT(VARCHAR,@endDate,120)
                  GROUP BY [filenumber],[Source]
                  ) a
          GROUP BY a.[Actor],a.[stat]
          ) b INNER JOIN
          #finalTable AS final
          ON  final.[vendor] = b.Actor
          AND final.[stat] = b.[stat]

  SELECT  [vendor] AS 'Vendor',
          [dbo].GetAPIVendorStatus([stat]) AS 'Status',
          [total] AS 'Total Processed',
          [gfTotal] AS 'Total by File Number'
  FROM    #finalTable
  ORDER BY [vendor], [stat]
END
