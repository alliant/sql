USE [alliant]
--USE [alliant_test]
--USE [dev_alliant]
GO
/****** Object:  StoredProcedure [dbo].[spVendorCount]    Script Date: 7/25/2016 8:50:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spVendorCount]
	-- Add the parameters for the stored procedure here
	@vendorID INT = 0,
  @year INT = 0,
  @month INT = 0,
  @type INT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  DECLARE @typeName VARCHAR(50) = [dbo].GetAPIVendorType(@type)
  DECLARE @sql NVARCHAR(MAX)

  CREATE TABLE #finalTable (vendor VARCHAR(50), yr INT, mth INT, stat VARCHAR(50), total INT, gfTotal INT)
  
  SET @sql = '
  SELECT  [Source] AS [Actor],
          YEAR([ICLDate]) AS [yr],
          MONTH([ICLDate]) AS [mth],
          ''cplIssue'' AS [stat],
          COUNT(*) AS [cnt]
  FROM    [dbo].[t_ICL]
  WHERE   YEAR([ICLDate]) = ' + CONVERT(VARCHAR,@year) + '
  AND     MONTH([ICLDate]) = ' + CONVERT(VARCHAR,@month) + '
  AND     [Source] IN (' + [dbo].GetAPIVendorName(@vendorID) + ')
  GROUP BY [Source],YEAR([ICLDate]),MONTH([ICLDate])
  UNION ALL
  SELECT  [Source] AS [Actor],
          YEAR([used]) AS [yr],
          MONTH([used]) AS [mth],
          ''policyIssue'' AS [stat],
          COUNT(*) as [cnt]
  FROM    [dbo].[t_policies]
  WHERE   YEAR([used]) = ' + CONVERT(VARCHAR,@year) + '
  AND     MONTH([used]) = ' + CONVERT(VARCHAR,@month) + '
  AND     [Source] IN (' + [dbo].GetAPIVendorName(@vendorID) + ')
  GROUP BY [Source],YEAR([used]),MONTH([used])'

  INSERT INTO #finalTable (vendor,yr,mth,stat,total)
  EXEC sp_executesql @sql

  IF (@type = 0 OR @typeName = 'cpl') -- All or CPL
  BEGIN
    UPDATE  final
    SET     gfTotal = b.[cnt]
    FROM    (
            SELECT  a.[Actor]
                   ,a.[yr]
                   ,a.[mth]
                   ,a.[stat]
                   ,COUNT(a.[cnt]) AS cnt
            FROM    (
                    SELECT  [Source] AS [Actor],
                            YEAR([ICLDate]) AS [yr],
                            MONTH([ICLDate]) AS [mth],
                            'cplIssue' AS [stat],
                            COUNT(*) AS [cnt]
                    FROM    [dbo].[t_ICL]
                    GROUP BY [GFNumber],[Source],YEAR([ICLDate]),MONTH([ICLDate])
                    ) a
            GROUP BY a.[Actor],a.[yr],a.[mth],a.[stat]
            ) b INNER JOIN
            #finalTable AS final
            ON  final.[vendor] = b.Actor
            AND final.[mth] = b.[mth]
            AND final.[yr] = b.[yr]
            AND final.[stat] = b.[stat]
  END
  IF (@type = 0 OR @typeName = 'policy') -- All or Policy Jacket
  BEGIN
    UPDATE  final
    SET     gfTotal = b.[cnt]
    FROM    (
            SELECT  a.[Actor]
                   ,a.[yr]
                   ,a.[mth]
                   ,a.[stat]
                   ,COUNT(a.[cnt]) AS cnt
            FROM    (
                    SELECT  [Source] AS [Actor],
                            YEAR([used]) AS [yr],
                            MONTH([used]) AS [mth],
                            'policyIssue' AS [stat],
                            COUNT(*) AS [cnt]
                    FROM    [dbo].[t_policies]
                    GROUP BY [filenumber],[Source],YEAR([used]),MONTH([used])
                    ) a
            GROUP BY a.[Actor],a.[yr],a.[mth],a.[stat]
            ) b INNER JOIN
            #finalTable AS final
            ON  final.[vendor] = b.Actor
            AND final.[mth] = b.[mth]
            AND final.[yr] = b.[yr]
            AND final.[stat] = b.[stat]
  END

  SELECT  [vendor] AS 'Vendor',
          [yr] AS 'Year',
          [mth] AS 'Month',
          [dbo].GetAPIVendorStatus([stat]) AS 'Status',
          [total] AS 'Total Processed',
          [gfTotal] AS 'Total by File Number'
  FROM    #finalTable
  ORDER BY [vendor], [yr], [mth], [stat]
END
