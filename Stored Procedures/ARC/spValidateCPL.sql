GO
/****** Object:  StoredProcedure [dbo].[spValidateCPL]    Script Date: 7/21/2022 9:00:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spValidateCPL]
	-- Add the parameters for the stored procedure here
	@type		VARCHAR(100) = NULL,
	@state      VARCHAR(2)   = NULL
	
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  DECLARE @ID INT,
          @active BIT,
          @letter VARCHAR(50),
          @sig VARCHAR(100),
          @formID VARCHAR(20),
          @index INT,
          @maxIndex INT,
          @count INT,
          @dbName VARCHAR(30),
          @dbEnv VARCHAR(4),
          @sql NVARCHAR(MAX),
          @parm NVARCHAR(MAX),
          @tempType VARCHAR(20),
          @pos INT,
          @lastPos INT,
          @strLen INT

  -- the table for validation
  SELECT f.* INTO #tempValidate FROM [dbo].[t_ClosingLetter] f LEFT JOIN [dbo].[t_ClosingLetter] ON 0 = 1
  SELECT CONVERT(VARCHAR(30), '') AS [db], * INTO #validate FROM #tempValidate WHERE 0 = 1

  --we need to get the full string length to decide the length of the last segment
  SET @strLen = LEN(@type)
  SET @pos = 1
  SET @lastPos = 1
  WHILE @pos <> 0
  BEGIN
    SET @pos = CHARINDEX(',',@type,@lastPos)
    IF (@pos > 0)
    BEGIN
      SET @tempType = SUBSTRING(@type,@lastPos,@pos-@lastPos)
      SET @lastPos = @pos + 1
    END
    ELSE
    BEGIN
      SET @tempType = SUBSTRING(@type,@lastPos,@strLen-@lastPos+1)
    END

    -- get active record
    INSERT INTO #validate
    SELECT DB_NAME(), * FROM [dbo].[t_ClosingLetter] WHERE [ClosingLetterID] = (SELECT max([ClosingLetterID]) FROM [dbo].[t_ClosingLetter] WHERE [CPLType]=@tempType AND [StateInit]=@state AND [Active]=1)
    
    -- get inactive record
    INSERT INTO #validate
    SELECT DB_NAME(), * FROM [dbo].[t_ClosingLetter] WHERE [ClosingLetterID] = (SELECT max([ClosingLetterID]) FROM [dbo].[t_ClosingLetter] WHERE [CPLType]=@tempType AND [StateInit]=@state AND [Active]=0)
  END

  SELECT * FROM #validate ORDER BY [ClosingLetterID] DESC
END
