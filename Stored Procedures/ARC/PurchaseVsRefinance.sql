USE [dev_alliant]
USE [alliant_test]
USE [alliant]
GO
/****** Object:  StoredProcedure [compass].[PurchasingVsRefinance]    Script Date: 3/19/2019 2:25:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [compass].[PurchaseVsRefinance]
	-- Add the parameters for the stored procedure here
	@month INTEGER = 0,
  @year INTEGER = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  IF (@month = 0)
    SET @month = MONTH(DATEADD(MONTH, -1, GETDATE()))
  IF (@year = 0)
    SET @year = YEAR(DATEADD(MONTH, -1, GETDATE()))
  
  SELECT  [AgentID],
          [Purchase],
          [Refinance]
  FROM    (
          SELECT  [cintid] AS [AgentID],
                  CASE WHEN [TransactionType] IS NULL THEN 'Purchase' ELSE [TransactionType] END AS [TransactionType]
          FROM    [dbo].[t_company] o LEFT OUTER JOIN
                  [dbo].[t_ICL]  c 
          ON      o.[cid] = c.[EscrowID]
          WHERE   MONTH([ICLDate]) = @month
          AND     YEAR([ICLDate]) = @year
          ) s
          PIVOT 
          (
          COUNT([TransactionType])
          FOR [TransactionType] IN ([Purchase],[Refinance])
          ) p
END
