USE [dev_alliant]
USE [alliant_test]
USE [alliant]
GO
/****** Object:  StoredProcedure [compass].[PolicyVolume]    Script Date: 4/20/2018 2:10:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jeff
-- Create date: 7/21/2017
-- Description:	Compass report on policy volume
-- =============================================
ALTER PROCEDURE [compass].[MonthlyDetails]
    @agentId VARCHAR(MAX) = '',
    @month INTEGER = 0,
    @year INTEGER = 0
AS
BEGIN
  SET NOCOUNT ON;
  DECLARE @agentIdTemp VARCHAR(MAX) = '',
          @monthTemp INTEGER = 0,
          @yearTemp INTEGER = 0

  SET @agentIdTemp = @agentID

  SET @yearTemp = @year
  IF (@yearTemp = 0)
    SET @yearTemp = YEAR(GETDATE())
    
  SET @monthTemp = @month
  IF (@monthTemp = 0)
    SET @monthTemp = MONTH(GETDATE())

  CREATE TABLE #exportTable
  (
    [agentID] VARCHAR(20),
    [year] INTEGER,
    [category] VARCHAR(20),
    [type] VARCHAR(20),
    [month1] INTEGER,
    [month2] INTEGER,
    [month3] INTEGER,
    [month4] INTEGER,
    [month5] INTEGER,
    [month6] INTEGER,
    [month7] INTEGER,
    [month8] INTEGER,
    [month9] INTEGER,
    [month10] INTEGER,
    [month11] INTEGER,
    [month12] INTEGER
  )
  
  INSERT INTO #exportTable ([agentID],[year],[category],[type],[month1],[month2],[month3],[month4],[month5],[month6],[month7],[month8],[month9],[month10],[month11],[month12])
  --SELECT  a.[cintid] AS [agentID],
  --        @yearTemp AS [year],
  --        'E' AS [category],
  --        'P' AS [type],
  --        SUM(ISNULL(p.[1],0)) AS [month1],
  --        SUM(ISNULL(p.[2],0)) AS [month2],
  --        SUM(ISNULL(p.[3],0)) AS [month3],
  --        SUM(ISNULL(p.[4],0)) AS [month4],
  --        SUM(ISNULL(p.[5],0)) AS [month5],
  --        SUM(ISNULL(p.[6],0)) AS [month6],
  --        SUM(ISNULL(p.[7],0)) AS [month7],
  --        SUM(ISNULL(p.[8],0)) AS [month8],
  --        SUM(ISNULL(p.[9],0)) AS [month9],
  --        SUM(ISNULL(p.[10],0)) AS [month10],
  --        SUM(ISNULL(p.[11],0)) AS [month11],
  --        SUM(ISNULL(p.[12],0)) AS [month12]
  --FROM    [dbo].[t_company] a LEFT OUTER JOIN
  --        (
  --        SELECT  p.[cid],
  --                p.[policyId],
  --                MONTH(p.[used]) AS [month]
  --        FROM    [dbo].[t_policies] p INNER JOIN
  --                [dbo].[PolicyEndorsements] e
  --        ON      p.[policyID] = e.[policyID]
  --        WHERE   YEAR(p.[used]) = @yearTemp
  --        AND     MONTH(p.[used]) = @monthTemp
  --        AND     p.[void] = 0
  --        ) src PIVOT
  --        (
  --        COUNT([policyId])
  --        FOR [month] IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])
  --        ) p
  --ON      a.[cid] = p.[cid]
  --WHERE   a.[cintid] != ''
  --GROUP BY a.[cintid]
  --UNION ALL
  SELECT  a.[cintid] AS [agentID],
          @yearTemp AS [year],
          'P' AS [category],
          'I' AS [type],
          SUM(ISNULL(p.[1],0)) AS [month1],
          SUM(ISNULL(p.[2],0)) AS [month2],
          SUM(ISNULL(p.[3],0)) AS [month3],
          SUM(ISNULL(p.[4],0)) AS [month4],
          SUM(ISNULL(p.[5],0)) AS [month5],
          SUM(ISNULL(p.[6],0)) AS [month6],
          SUM(ISNULL(p.[7],0)) AS [month7],
          SUM(ISNULL(p.[8],0)) AS [month8],
          SUM(ISNULL(p.[9],0)) AS [month9],
          SUM(ISNULL(p.[10],0)) AS [month10],
          SUM(ISNULL(p.[11],0)) AS [month11],
          SUM(ISNULL(p.[12],0)) AS [month12]
  FROM    [dbo].[t_company] a LEFT OUTER JOIN
          (
          SELECT  [cid],
                  [policyId],
                  MONTH([used]) AS [month]
          FROM    [dbo].[t_policies]
          WHERE   YEAR([used]) = @yearTemp
          AND     MONTH([used]) = @monthTemp
          AND     [void] = 0
          ) src PIVOT
          (
          COUNT([policyId])
          FOR [month] IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])
          ) p
  ON      a.[cid] = p.[cid]
  WHERE   a.[cintid] = CASE WHEN @agentID = '' THEN a.[cintID] ELSE @agentID END
  AND     a.[cintid] != ''
  GROUP BY a.[cintid]
  UNION ALL
  SELECT  a.[cintid] AS [agentID],
          @yearTemp AS [year],
          'W' AS [category],
          'A' AS [type],
          SUM(ISNULL(c.[1],0)) AS [month1],
          SUM(ISNULL(c.[2],0)) AS [month2],
          SUM(ISNULL(c.[3],0)) AS [month3],
          SUM(ISNULL(c.[4],0)) AS [month4],
          SUM(ISNULL(c.[5],0)) AS [month5],
          SUM(ISNULL(c.[6],0)) AS [month6],
          SUM(ISNULL(c.[7],0)) AS [month7],
          SUM(ISNULL(c.[8],0)) AS [month8],
          SUM(ISNULL(c.[9],0)) AS [month9],
          SUM(ISNULL(c.[10],0)) AS [month10],
          SUM(ISNULL(c.[11],0)) AS [month11],
          SUM(ISNULL(c.[12],0)) AS [month12]
  FROM    [dbo].[t_company] a LEFT OUTER JOIN
          (
          SELECT  [EscrowID],
                  [ICLID],
                  MONTH([ICLDate]) AS [month]
          FROM    [dbo].[t_ICL]
          WHERE   YEAR([ICLDate]) = @yearTemp
          AND     MONTH([ICLDate]) = @monthTemp
          AND     [LenderName] LIKE '%Wells Fargo%'
          ) src PIVOT
          (
          COUNT([ICLID])
          FOR [month] IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])
          ) c
  ON      a.[cid] = c.[EscrowID]
  WHERE   a.[cintid] = CASE WHEN @agentID = '' THEN a.[cintID] ELSE @agentID END
  AND     a.[cintid] != ''
  GROUP BY a.[cintid]
  UNION ALL
  SELECT  a.[cintid] AS [agentID],
          @yearTemp AS [year],
          'I' AS [category],
          'A' AS [type],
          SUM(ISNULL(c.[1],0)) AS [month1],
          SUM(ISNULL(c.[2],0)) AS [month2],
          SUM(ISNULL(c.[3],0)) AS [month3],
          SUM(ISNULL(c.[4],0)) AS [month4],
          SUM(ISNULL(c.[5],0)) AS [month5],
          SUM(ISNULL(c.[6],0)) AS [month6],
          SUM(ISNULL(c.[7],0)) AS [month7],
          SUM(ISNULL(c.[8],0)) AS [month8],
          SUM(ISNULL(c.[9],0)) AS [month9],
          SUM(ISNULL(c.[10],0)) AS [month10],
          SUM(ISNULL(c.[11],0)) AS [month11],
          SUM(ISNULL(c.[12],0)) AS [month12]
  FROM    [dbo].[t_company] a LEFT OUTER JOIN
          (
          SELECT  [EscrowID],
                  [ICLID],
                  MONTH([ICLDate]) AS [month]
          FROM    [dbo].[t_ICL]
          WHERE   YEAR([ICLDate]) = @yearTemp
          AND     MONTH([ICLDate]) = @monthTemp
          ) src PIVOT
          (
          COUNT([ICLID])
          FOR [month] IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])
          ) c
  ON      a.[cid] = c.[EscrowID]
  WHERE   a.[cintid] = CASE WHEN @agentID = '' THEN a.[cintID] ELSE @agentID END
  AND     a.[cintid] != ''
  GROUP BY a.[cintid]
  UNION ALL
  SELECT  a.[cintid] AS [agentID],
          @yearTemp AS [year],
          'V' AS [category],
          'A' AS [type],
          SUM(ISNULL(c.[1],0)) AS [month1],
          SUM(ISNULL(c.[2],0)) AS [month2],
          SUM(ISNULL(c.[3],0)) AS [month3],
          SUM(ISNULL(c.[4],0)) AS [month4],
          SUM(ISNULL(c.[5],0)) AS [month5],
          SUM(ISNULL(c.[6],0)) AS [month6],
          SUM(ISNULL(c.[7],0)) AS [month7],
          SUM(ISNULL(c.[8],0)) AS [month8],
          SUM(ISNULL(c.[9],0)) AS [month9],
          SUM(ISNULL(c.[10],0)) AS [month10],
          SUM(ISNULL(c.[11],0)) AS [month11],
          SUM(ISNULL(c.[12],0)) AS [month12]
  FROM    [dbo].[t_company] a LEFT OUTER JOIN
          (
          SELECT  [EscrowID],
                  [ICLID],
                  MONTH([StatusChangeDate]) AS [month]
          FROM    [dbo].[t_ICL]
          WHERE   YEAR([StatusChangeDate]) = @yearTemp
          AND     MONTH([StatusChangeDate]) = @monthTemp
          AND     [Status] = 'Voided'
          ) src PIVOT
          (
          COUNT([ICLID])
          FOR [month] IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])
          ) c
  ON      a.[cid] = c.[EscrowID]
  WHERE   a.[cintid] = CASE WHEN @agentID = '' THEN a.[cintID] ELSE @agentID END
  AND     a.[cintid] != ''
  GROUP BY a.[cintid]
  UNION ALL
  SELECT  a.[cintid] AS [agentID],
          @yearTemp AS [year],
          'R' AS [category],
          'A' AS [type],
          SUM(ISNULL(c.[1],0)) AS [month1],
          SUM(ISNULL(c.[2],0)) AS [month2],
          SUM(ISNULL(c.[3],0)) AS [month3],
          SUM(ISNULL(c.[4],0)) AS [month4],
          SUM(ISNULL(c.[5],0)) AS [month5],
          SUM(ISNULL(c.[6],0)) AS [month6],
          SUM(ISNULL(c.[7],0)) AS [month7],
          SUM(ISNULL(c.[8],0)) AS [month8],
          SUM(ISNULL(c.[9],0)) AS [month9],
          SUM(ISNULL(c.[10],0)) AS [month10],
          SUM(ISNULL(c.[11],0)) AS [month11],
          SUM(ISNULL(c.[12],0)) AS [month12]
  FROM    [dbo].[t_company] a LEFT OUTER JOIN
          (
          SELECT  [EscrowID],
                  [ICLID],
                  MONTH([StatusChangeDate]) AS [month]
          FROM    [dbo].[t_ICL]
          WHERE   YEAR([StatusChangeDate]) = @yearTemp
          AND     MONTH([StatusChangeDate]) = @monthTemp
          AND     [Status] = 'Revised'
          ) src PIVOT
          (
          COUNT([ICLID])
          FOR [month] IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])
          ) c
  ON      a.[cid] = c.[EscrowID]
  WHERE   a.[cintid] = CASE WHEN @agentID = '' THEN a.[cintID] ELSE @agentID END
  AND     a.[cintid] != ''
  GROUP BY a.[cintid]
  UNION ALL
  SELECT  a.[cintid] AS [agentID],
          @yearTemp AS [year],
          'X' AS [category],
          'A' AS [type],
          SUM(ISNULL(c.[1],0)) AS [month1],
          SUM(ISNULL(c.[2],0)) AS [month2],
          SUM(ISNULL(c.[3],0)) AS [month3],
          SUM(ISNULL(c.[4],0)) AS [month4],
          SUM(ISNULL(c.[5],0)) AS [month5],
          SUM(ISNULL(c.[6],0)) AS [month6],
          SUM(ISNULL(c.[7],0)) AS [month7],
          SUM(ISNULL(c.[8],0)) AS [month8],
          SUM(ISNULL(c.[9],0)) AS [month9],
          SUM(ISNULL(c.[10],0)) AS [month10],
          SUM(ISNULL(c.[11],0)) AS [month11],
          SUM(ISNULL(c.[12],0)) AS [month12]
  FROM    [dbo].[t_company] a LEFT OUTER JOIN
          (
          SELECT  [EscrowID],
                  [ICLID],
                  MONTH([StatusChangeDate]) AS [month]
          FROM    [dbo].[t_ICL]
          WHERE   YEAR([StatusChangeDate]) = @yearTemp
          AND     MONTH([StatusChangeDate]) = @monthTemp
          AND     [Status] = 'Expired'
          ) src PIVOT
          (
          COUNT([ICLID])
          FOR [month] IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])
          ) c
  ON      a.[cid] = c.[EscrowID]
  WHERE   a.[cintid] = CASE WHEN @agentID = '' THEN a.[cintID] ELSE @agentID END
  AND     a.[cintid] != ''
  GROUP BY a.[cintid]

  SELECT  *
  FROM    #exportTable
  ORDER BY [agentID],[category],[type] DESC
END

