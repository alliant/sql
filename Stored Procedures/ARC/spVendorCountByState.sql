USE [alliant]
--USE [alliant_test]
--USE [dev_alliant]
GO
/****** Object:  StoredProcedure [dbo].[spVendorCountByState]    Script Date: 5/24/2016 1:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spVendorCountByState]
	-- Add the parameters for the stored procedure here
	@vendorID INT = 0,
  @year INT = 0,
  @month INT = 0,
  @type INT = 0,
  @state VARCHAR(2)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  DECLARE @typeName VARCHAR(50) = [dbo].GetAPIVendorType(@type)
  DECLARE @sql NVARCHAR(MAX)

  CREATE TABLE #stateTable (state_abbr VARCHAR(2),state_name VARCHAR(50),actor VARCHAR(50),stat VARCHAR(50),yr INT,mth INT,cnt INT)
  
  IF (@type = 0 OR @typeName = 'cplIssue') -- All or CPL
  BEGIN
    SET @sql = '
    SELECT  b.[state_abbr],
            b.[state_name],
            t.[Actor],
            t.[TransactionServiceType] as stat,
            YEAR(t.[TimeStamp]) as yr,
            MONTH(t.[TimeStamp]) as mth,
            COUNT(b.[state_abbr])
    FROM    [dbo].[TransactionLog] t INNER JOIN
            [dbo].[t_icl] c
    ON      t.[CPLNumber] = c.[iclid] INNER JOIN
            [dbo].[t_states] b
    ON      c.[StateName] = b.[state_name]
    WHERE   t.[Actor] IN (' + [dbo].GetAPIVendorName(@vendorID) + ')' + 
    [dbo].GetAPIVendorFilter(@typeName,@year,@month) + '
    AND     t.[TransactionServiceType] <> ''cplVoid'''
  END

  -- add the state filter
  IF (@state != '')
  BEGIN
    SET @sql = @sql + '
    AND     [state_abbr] = ''' + @state + ''''
  END
  SET @sql = @sql + '
    GROUP BY b.[state_abbr],b.[state_name],t.[Actor],t.[TransactionServiceType],YEAR(t.[TimeStamp]),MONTH(t.[TimeStamp])
    ORDER BY b.[state_abbr],YEAR(t.[TimeStamp]),MONTH(t.[TimeStamp]),t.[TransactionServiceType]'
  
  PRINT @sql
  INSERT INTO #stateTable (state_abbr,state_name,actor,stat,yr,mth,cnt)
  EXEC sp_executesql @sql
  
  IF (@type = 0 OR @typeName = 'policyIssue') -- All or PJ
  BEGIN
    SET @sql = '
    SELECT  b.[state_abbr],
            b.[state_name],
            t.[Actor],
            t.[TransactionServiceType] as stat,
            YEAR(t.[TimeStamp]) as yr,
            MONTH(t.[TimeStamp]) as mth,
            COUNT(b.[state_abbr])
    FROM    [dbo].[TransactionLog] t INNER JOIN
            [dbo].[t_policies] p
    ON      t.[PolicyNumber] = p.[policyid] INNER JOIN
            [dbo].[PolicyProperties] pp
    ON      p.[policyid] = pp.[policyid] INNER JOIN
            [dbo].[t_states] b
    ON      pp.[State] = b.[state_abbr]
    WHERE   t.[Actor] IN (' + [dbo].GetAPIVendorName(@vendorID) + ')' + 
    [dbo].GetAPIVendorFilter(@typeName,@year,@month)
  END

  -- add the state filter
  IF (@state != '')
  BEGIN
    SET @sql = @sql + '
    AND     [state_abbr] = ''' + @state + ''''
  END
  SET @sql = @sql + '
    GROUP BY b.[state_abbr],b.[state_name],t.[Actor],t.[TransactionServiceType],YEAR(t.[TimeStamp]),MONTH(t.[TimeStamp])
    ORDER BY b.[state_abbr],YEAR(t.[TimeStamp]),MONTH(t.[TimeStamp]),t.[TransactionServiceType]'
  
  PRINT @sql
  INSERT INTO #stateTable (state_abbr,state_name,actor,stat,yr,mth,cnt)
  EXEC sp_executesql @sql

  SELECT  [state_abbr] AS 'State Abbr.',
          [state_name] AS 'State Name',
          [actor] AS 'Vendor',
          [dbo].GetAPIVendorStatus([stat]) AS 'Status',
          [yr]'Year',
          [mth] AS 'Month',
          [cnt] AS 'Total Processed'
  FROM    #stateTable
  ORDER BY [state_abbr],[vendor], [yr], [mth], [stat]
END
