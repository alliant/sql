USE [dev_alliant]
USE [alliant_test]
USE [alliant]
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [compass].[ProcessingExport]
	-- Add the parameters for the stored procedure here
	@agentID VARCHAR(30) = '',
  @startDate DATETIME = NULL,
  @endDate DATETIME = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  IF (@startDate IS NULL)
    SET @startDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) - 1, 0)
  IF (@endDate IS NULL)
    SET @endDate = DATEADD(SECOND, -1, DATEADD(MONTH, DATEDIFF(MONTH, 0, @startDate) + 1, 0))

  PRINT @startDate
  PRINT @endDate

  CREATE TABLE #exportTable ([filenumber] VARCHAR(100), [residential] BIT, [formtype] VARCHAR(30), [policyid] INTEGER, [ratecode] VARCHAR(1), [statcode] VARCHAR(1), [formid] VARCHAR(30), [effdate] VARCHAR(30), [liabilityamount] MONEY, [countycode] VARCHAR(30), [grosspremium] MONEY, [netPremium] MONEY, [formname] VARCHAR(500))

  INSERT INTO #exportTable ([filenumber], [residential], [formtype], [policyid], [ratecode], [statcode], [formid], [effdate], [liabilityamount], [countycode], [grosspremium], [netPremium], [formname])
	SELECT  p.[filenumber],
          p.[residential],
          'P' AS [formtype],
          p.[policyid],
          '' AS [ratecode],
          '' AS [statcode],
          CASE
           WHEN pf.[FormName] LIKE '%Owner%' THEN 'O'
           WHEN pf.[FormName] LIKE '%Loan%' THEN 'L'
           ELSE ''
          END AS [formid],
          CONVERT(VARCHAR(10),p.[used],101) AS [effdate],
          p.[liabilityamount],
          ISNULL(px.[CountyCode],'') AS [countycode],
          ISNULL(p.[grosspremium],0) AS [grosspremium],
          ISNULL(px.[netpremium],0) AS [netPremium],
          pf.[FormName] AS [formname]
  FROM    [dbo].[t_policies] p INNER JOIN
          [dbo].[t_company] c
  ON      p.[cid] = c.[cid] INNER JOIN
          [dbo].[t_policyforms] pf
  ON      p.[pformid] = pf.[pformid] LEFT OUTER JOIN
          [dbo].[PolicyExtra] px
  ON      p.[policyid] = pX.[policyid]
  WHERE   c.[cintid] = @agentID
  AND     p.[paid] = 0
  AND     p.[used] BETWEEN @startDate AND @endDate
  UNION ALL
  SELECT  p.[filenumber],
          p.[residential],
          'E',
          p.[policyid],
          '',
          '',
          '',
          CONVERT(VARCHAR(10),p.[used],101),
          p.[liabilityamount],
          px.[CountyCode],
          pe.[GrossPremium],
          ISNULL(pe.[NetPremium],0),
          pe.[Description]
  FROM    [dbo].[t_policies] p INNER JOIN
          [dbo].[t_company] c
  ON      p.[cid] = c.[cid] INNER JOIN
          [dbo].[PolicyEndorsements] pe
  ON      p.[policyid] = pe.[policyid] INNER JOIN
          [dbo].[PolicyExtra] px
  ON      p.[policyid] = pX.[policyid]
  WHERE   c.[cintid] = @agentID
  AND     p.[paid] = 0
  AND     p.[used] BETWEEN @startDate AND @endDate

  SELECT  *
  FROM    #exportTable
  ORDER BY [policyid], [formtype] DESC
END
GO
