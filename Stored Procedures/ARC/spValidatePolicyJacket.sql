GO
/****** Object:  StoredProcedure [dbo].[spValidatePolicyJacket]    Script Date: 7/21/2022 9:00:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spValidatePolicyJacket]
	-- Add the parameters for the stored procedure here
	@formName		    VARCHAR(200) = NULL,
	@formState      VARCHAR(2)   = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  DECLARE @ID INT,
          @formActive BIT,
          @formFileActive VARCHAR(200),
          @formFileInactive VARCHAR(200),
          @formSignOffset INT,
          @formID VARCHAR(20),
          @index INT,
          @maxIndex INT,
          @count INT,
          @dbName VARCHAR(30),
          @dbEnv VARCHAR(4),
          @sql NVARCHAR(MAX),
          @parm NVARCHAR(MAX)

  -- the table for validation
  SELECT f.* INTO #tempValidate FROM [dbo].[t_policyforms] f LEFT JOIN [dbo].[t_policyforms] ON 0 = 1
  SELECT CONVERT(VARCHAR(30), '') AS [db], * INTO #validate FROM #tempValidate WHERE 0 = 1
  
  -- make sure only one active record
  SELECT @count=count([PFormID]) FROM [dbo].[t_policyforms] WHERE [FormName]=@formname AND [State]=@formstate AND active=1
  IF (@count = 0 OR @count = 1)
  BEGIN
    -- get active record
    INSERT INTO #validate
    SELECT DB_NAME(), * FROM [dbo].[t_policyforms] WHERE [PFormID] = (SELECT max([PFormID]) FROM [dbo].[t_policyforms] WHERE [FormName]=@formname AND [State]=@formstate AND [Active]=1)

    -- get the last unactive record as it is either the previous active form or the new form
    INSERT INTO #validate
    SELECT DB_NAME(), * FROM [dbo].[t_policyforms] WHERE [PFormID] = (SELECT max([PFormID]) FROM [dbo].[t_policyforms] WHERE [FormName]=@formname AND [State]=@formstate AND [Active]=0)
  END
  ELSE
  BEGIN
    -- show all the active records
    INSERT INTO #validate
    SELECT * FROM [dbo].[t_policyforms] WHERE [FormName]=@formname AND [State]=@formstate AND [Active]=1
  END

  SELECT * FROM #validate ORDER BY [PFormID] DESC
END
