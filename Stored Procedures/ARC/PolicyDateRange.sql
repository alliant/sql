USE [dev_alliant]
USE [alliant_test]
USE [alliant]
GO
/****** Object:  StoredProcedure [compass].[PolicyDateRange]    Script Date: 5/20/2020 9:52:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [compass].[PolicyDateRange]
	-- Add the parameters for the stored procedure here
	@startDate DATETIME = NULL,
  @endDate DATETIME = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  -- Insert statements for procedure here
	IF @startDate IS NULL
  BEGIN
    SET @startDate = DATEADD(DAY, DATEDIFF(DAY, 1, GETDATE()) ,0)
    SET @endDate = DATEADD(SECOND, -1, DATEADD(DAY, 1, @startDate))
  END

  IF @endDate IS NULL
    SET @endDate = GETDATE()

  SELECT  [policyID] AS [PolicyID],
          CASE
            WHEN [paid] = 0 AND [void] = 0 THEN 'I'
            WHEN [paid] = 1 AND [void] = 0 THEN 'P'
            WHEN [void] = 1                THEN 'V'
          END AS [Stat]
  FROM    [t_policies]
  WHERE   [used] BETWEEN @startDate AND @endDate
END
