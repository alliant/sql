USE [alliant]
USE [alliant_test]
--USE [dev_alliant]
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [compass].[MonthlyCPLReport]
	@agentID VARCHAR(MAX) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  DECLARE @NextString NVARCHAR(100),
          @Pos INTEGER = 0,
          @Str NVARCHAR(MAX),
          @Delimiter NCHAR(1) = ',',
          @month INTEGER = 0,
          @year INTEGER = 0

  --Separate the agent list
  CREATE TABLE #agentTable ([agentID] VARCHAR(10))
	IF (@agentID != '')
  BEGIN
    SET @Str = @agentID + @Delimiter
    SET @Pos = CHARINDEX(@Delimiter, @Str)
    WHILE (@Pos <> 0)
	  BEGIN
		  SET @NextString = SUBSTRING(@Str,1,@Pos-1)
      INSERT INTO #agentTable ([agentID])
      SELECT @NextString
		  SET @Str = SUBSTRING(@Str,@Pos+1,LEN(@Str))
		  SET @Pos = CHARINDEX(@Delimiter, @Str)
	  END
  END
  ELSE
  BEGIN
    INSERT INTO #agentTable ([agentID])
    SELECT [cintid] FROM [dbo].[t_company] GROUP BY [cintid]
  END

  --Set the year
  IF (@year = 0)
    SET @year=YEAR(DATEADD(m,-7,GETDATE()))
  
  --Set the month
  IF (@month = 0)
    SET @month=MONTH(DATEADD(m,-7,GETDATE()))

  SELECT  t.[cintid] AS [agentID],
          c.[TransactionType] AS [cplType],
          @month AS [month],
          @year AS [year],
          SUM(CASE WHEN c.[LenderName] LIKE '%Wells Fargo%' THEN 1 ELSE 0 END) AS [wellsFargoCount],
          COUNT(c.[iclid]) AS [cplCount]
  FROM    [dbo].[t_ICL] c INNER JOIN
          (
          SELECT  [cintid],
                  [cid]
          FROM    [dbo].[t_company] c INNER JOIN
                  #agentTable a
          ON      c.[cintid] = a.[agentID]
          ) t
  ON      c.[EscrowID] = t.[cid]
  WHERE   YEAR(c.[ICLDate]) = @year
  AND     MONTH(c.[ICLDate]) = @month
  AND     c.[Status] = 'Issued'
  GROUP BY t.[cintid],c.[TransactionType]
  ORDER BY COUNT(c.[iclid]) DESC
END
GO
