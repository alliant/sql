USE [dev_alliant]
GO
/****** Object:  StoredProcedure [dbo].[spDeleteCPL]    Script Date: 4/20/2016 4:18:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spDeleteCPL]
	-- Add the parameters for the stored procedure here
	@state        VARCHAR(2)   = NULL,
  @type         VARCHAR(100) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  DECLARE @formID INT,
          @deleteID INT,
          @active BIT,
          @tempType VARCHAR(20),
          @pos INT = 1,
          @lastPos INT = 1,
          @strLen INT
  
  --we need to get the full string length to decide the length of the last segment
  SET @strLen = LEN(@type)
  
  WHILE @pos <> 0
  BEGIN
    SET @pos = CHARINDEX(',',@type,@lastPos)
    IF (@pos > 0)
    BEGIN
      SET @tempType = SUBSTRING(@type,@lastPos,@pos-@lastPos)
      SET @lastPos = @pos + 1
    END
    ELSE
    BEGIN
      SET @tempType = SUBSTRING(@type,@lastPos,@strLen-@lastPos+1)
    END

    --validate that the ID is good
    SELECT @formID=max([ClosingLetterID]) FROM [dbo].[t_ClosingLetter] WHERE [StateInit]=@state AND [CPLType]=@tempType
    IF (@formID IS NULL)
    BEGIN
      PRINT 'The CPL where type is ' + @tempType + ' is not found in the database. Please check that the CPL type and state is correct'
      RETURN 1
    END
    ELSE
    BEGIN
      --get the latest inactive ID
      SELECT @deleteID=max([ClosingLetterID]) FROM [dbo].[t_ClosingLetter] WHERE [StateInit]=@state AND [CPLType]=@tempType

      --check if the ID is active
      SELECT @active=[Active] FROM [dbo].[t_ClosingLetter] WHERE [ClosingLetterID]=@deleteID
      IF (@active = 1)
      BEGIN
        EXEC [dbo].[spActivateCPL] @state=@state, @type=@tempType
      END
        
      BEGIN TRY
        PRINT 'Deleting ' + @tempType + ' CPL with ID ' + CONVERT(VARCHAR,@deleteID)
        DELETE FROM [dbo].[t_ClosingLetter] WHERE [ClosingLetterID]=@deleteID
      END TRY
      BEGIN CATCH
        EXECUTE spDisplayError
        RETURN ERROR_NUMBER()
      END CATCH
      PRINT ''
    END
  END
END
