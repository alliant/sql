USE [dev_alliant]
USE [alliant_test]
USE [alliant]
GO
/****** Object:  StoredProcedure [compass].[CPLDailyCount]    Script Date: 2/25/2019 9:01:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [compass].[DailyCount]
	-- Add the parameters for the stored procedure here
	@date DATETIME = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  IF (@date IS NULL) 
    SELECT @date = dbo.DateOnly(DATEADD(DAY, -1, GETDATE()))
  ELSE
    SELECT @date = dbo.DateOnly(@date)

  -- Insert statements for procedure here
  SELECT  [cintid] AS [agentID],
          SUM(ISNULL(c.[count],0)) AS [CPLCount],
          SUM(ISNULL(p.[count],0)) AS [PolicyCount]
  FROM    [dbo].[t_company] o LEFT OUTER JOIN
          (
          SELECT  [EscrowID],
                  COUNT(*) AS [count]
          FROM    [dbo].[t_ICL]
          WHERE   CONVERT(DATE, [ICLDate], 101) = @date
          GROUP BY [EscrowID]
          ) c 
  ON      o.[cid] = c.[EscrowID] LEFT OUTER JOIN
          (
          SELECT  [cid],
                  COUNT(*) AS [count]
          FROM    [dbo].[t_policies]
          WHERE   CONVERT(DATE, [used], 101) = @date
          GROUP BY [cid]
          ) p
  ON      o.[cid] = p.[cid]
  WHERE   [cintid] != ''
  GROUP BY [cintid]
  ORDER BY [cintid]
END
