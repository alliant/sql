USE [dev_alliant]
GO
/****** Object:  StoredProcedure [dbo].[spActivateCPL]    Script Date: 4/20/2016 4:21:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spActivateCPL]
	-- Add the parameters for the stored procedure here
	@state        VARCHAR(2)   = NULL,
  @type         VARCHAR(100) = NULL
AS
BEGIN
	BEGIN TRANSACTION
  -- SET NOCOUNT ON added to prevent extra result SETs from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  DECLARE @formID INT,
          @maxInactiveID INT,
          @tempType VARCHAR(20),
          @pos INT = 1,
          @lastPos INT = 1,
          @strLen INT,
          @stateName VARCHAR(50)

  SELECT @stateName=[state_name] FROM dbo.t_states WHERE [state_abbr]=@state
  
  --we need to get the full string length to decide the length of the last segment
  SET @strLen = LEN(@type)
  
  WHILE @pos <> 0
  BEGIN
    SET @pos = CHARINDEX(',',@type,@lastPos)
    IF (@pos > 0)
    BEGIN
      SET @tempType = SUBSTRING(@type,@lastPos,@pos-@lastPos)
      SET @lastPos = @pos + 1
    END
    ELSE
    BEGIN
      SET @tempType = SUBSTRING(@type,@lastPos,@strLen-@lastPos+1)
    END
    SELECT @formID=max([ClosingLetterID]) FROM [dbo].[t_ClosingLetter] WHERE [StateInit]=@state AND [CPLType]=@tempType
    IF (@formID IS NULL)
    BEGIN
      PRINT 'The CPL where type is ' + @tempType + ' is not found in the database. Please check that the CPL type and state is correct'
      ROLLBACK TRANSACTION
      RETURN 1
    END
    ELSE
    BEGIN
      --get the latest inactive ID
      SELECT @maxInactiveID=max([ClosingLetterID]) FROM [dbo].[t_ClosingLetter] WHERE [StateInit]=@state AND [CPLType]=@tempType and [Active]=0
        
      BEGIN TRY
        --deactivate all CPLs of the type
        PRINT 'Deactivating all CPLs of type ' + @tempType + ' for ' + @stateName
        UPDATE [dbo].[t_ClosingLetter] SET [Active]=0 WHERE [StateInit]=@state AND [CPLType]=@tempType

        --update the previous form to be active
        IF (@maxInactiveID IS NOT NULL)
        BEGIN
          PRINT 'Activating CPL [' + CONVERT(VARCHAR(20),@maxInactiveID) + '] ' + @tempType + ' CPL for ' + @stateName
          UPDATE [dbo].[t_ClosingLetter] SET [Active]=1 where [ClosingLetterID]=@maxInactiveID
        END
      END TRY
      BEGIN CATCH
        EXECUTE spDisplayError
        ROLLBACK TRANSACTION
        RETURN ERROR_NUMBER()
      END CATCH
      PRINT ''
    END
  END
  COMMIT TRANSACTION
END
