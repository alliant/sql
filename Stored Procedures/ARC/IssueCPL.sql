/****** Object:  StoredProcedure [arc].[IssueCpl]    Script Date: 1/25/2023 10:43:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [arc].[IssueCpl] 
	-- Add the parameters for the stored procedure here
	@Username VARCHAR(200),	-- = username
	@LetterReference VARCHAR(2000),
	@CPLTemplateID INT,
	@OfficeId INT,
	@ExtraOfficeId INT = NULL,
	@PolicyID INT = NULL,
	@Status VARCHAR(10),
	@GFNumber VARCHAR(200),
	@GFNumberClean VARCHAR(200),
	@ISAOA BIT,
	@ATIMA BIT,
	@VA BIT,
	@HUD BIT,
	@ShowBranches INT,
	@TransactionType VARCHAR(50),
	@AttorneyID INT = NULL,
	@LenderExtra VARCHAR(500) = NULL,
	@EstimatedClosingDate DATETIME = NULL,
	@LiabilityAmount MONEY = NULL,
	@Impersonator VARCHAR(500),
	@PropertyState VARCHAR(20),
	@Buyer VARCHAR(2000),
	@Source VARCHAR(200),
	@LenderId INT = NULL,
	@SellerId INT = NULL,
	@LenderName VARCHAR(2000),
	@SellerName VARCHAR(2000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE
		@ID INT,
		@CPLType VARCHAR(20),
		@AgentID VARCHAR(20),
		@Code CHAR(10),
		@date DATETIME,
		@Version INT = NULL,
		@stateinit VARCHAR(2),
		@TemplateStateInit VARCHAR(2),
		@OfficeAddress varchar(600) = NULL,
		@ExtraOfficeAddress varchar(600) = NULL,
		@AttorneyAddress varchar(600) = NULL,
		@OfficeName VARCHAR(150),
		@Beneficiary VARCHAR(2000),
		@Errors NVARCHAR(2048)



	SET @date = GETDATE()
	SET @Errors=''

	SELECT @stateinit = state_abbr FROM dbo.t_states WHERE state_name=@PropertyState

    -- VALIDATIONS
	IF NOT EXISTS (
		SELECT IsApproved, IsLockedOut, R.RoleName FROM dbo.aspnet_Membership M
			JOIN dbo.aspnet_Users U ON M.UserId=U.UserId
			JOIN dbo.aspnet_UsersInRoles UR ON UR.UserId = U.UserId
			JOIN dbo.aspnet_Roles R ON UR.RoleId = R.RoleId
			 WHERE UserName=@Username AND (RoleName = 'issuperadmin' OR RoleName = 'CplApi' OR RoleName='Closings') AND M.IsApproved=1
			 AND m.IsLockedOut=0
	) 
	BEGIN 
		-- VALIDATION ERRORS
		IF NOT EXISTS (
			SELECT IsApproved, IsLockedOut FROM dbo.aspnet_Membership M
				JOIN dbo.aspnet_Users U ON M.UserId=U.UserId
				WHERE UserName=@Username AND M.IsApproved=1 AND m.IsLockedOut=0
			)
		SELECT @Errors = CONCAT(@Errors, 'User account is not approved or active. ')

		IF NOT EXISTS (
			SELECT R.RoleName FROM dbo.aspnet_Membership M
				JOIN dbo.aspnet_Users U ON M.UserId=U.UserId
				JOIN dbo.aspnet_UsersInRoles UR ON UR.UserId = U.UserId
				JOIN dbo.aspnet_Roles R ON UR.RoleId = R.RoleId
				 WHERE UserName=@Username AND (RoleName = 'issuperadmin' OR RoleName = 'CplApi' OR RoleName='Closings')
			)
		SELECT @Errors = CONCAT(@Errors, 'The user does not have permission to create a CPL. ')
	END

	-- all licensed states support show branches
	IF NOT EXISTS (
		SELECT U.deleted, C.deleted, CS.StateInit FROM dbo.t_user U
			JOIN dbo.t_usercompany UC ON U.username=uc.username
			JOIN dbo.t_company C ON UC.cid=C.cid
			JOIN dbo.t_CompanyState CS ON C.cid=CS.CID
			 WHERE U.username=@Username AND U.deleted=0 AND C.deleted=0 AND CS.StateInit=@stateinit AND C.cid=@OfficeId
	)
	BEGIN
		-- licensed state errors
		IF NOT EXISTS (
			SELECT U.deleted, C.deleted FROM dbo.t_user U
				JOIN dbo.t_usercompany UC ON U.username=uc.username
				JOIN dbo.t_company C ON UC.cid=C.cid
				 WHERE U.username=@Username AND C.deleted=0 AND C.cid=@OfficeId
		)
		SELECT @Errors = CONCAT(@Errors, 'The user is not associated with office ', @OfficeId, '. ')

		IF NOT EXISTS (
			SELECT CS.StateInit FROM dbo.t_company C
				JOIN dbo.t_CompanyState CS ON C.cid=CS.CID
				 WHERE CS.StateInit=@stateinit AND C.cid=@OfficeId
		)
		SELECT @Errors = CONCAT(@Errors, 'Office ', @OfficeId, ' is not associated with property state ', @stateinit, '. ')
	END

  -- check for issue CPL on office
	IF NOT EXISTS (
		SELECT C.cid FROM dbo.t_company C
				WHERE C.cid=@OfficeId and C.IssuesCpls = 1
	)
  SELECT @Errors = CONCAT(@Errors, 'Office ', @OfficeId, ' is not configured to issue CPLs. ')
	
	-- LOOKUPS
	SELECT @OfficeName=agency, @OfficeAddress=dbo.PdfAddress(agency, NULL, address1, address2, city, state, zipcode), @AgentID=cintid FROM dbo.t_company WHERE cid=@OfficeId

	IF @ExtraOfficeId IS NOT NULL
		SELECT @ExtraOfficeAddress=dbo.PdfAddress(agency, NULL, address1, address2, city, state, zipcode) FROM dbo.t_company WHERE cid=@ExtraOfficeId

	IF @AttorneyID IS NOT NULL
		SELECT @AttorneyAddress=dbo.PdfAddress(FullName, FullName2, address1, address2, city, state, zipcode) FROM dbo.Attorneys WHERE AttorneyID=@AttorneyID

	SELECT @Version = version, @CPLType = CPLType, @TemplateStateInit = StateInit FROM dbo.t_ClosingLetter WHERE ClosingLetterID=@CPLTemplateID

	IF @Version IS NULL
	BEGIN 
		SELECT @Errors = CONCAT(@Errors, 'CPL type not found: ', @CPLTemplateID , '. ')
	END

	IF @stateinit <> @TemplateStateInit
	BEGIN 
		SELECT @Errors = CONCAT(@Errors, 'CPL type ', @CPLTemplateID ,' does not match property state ', @stateinit ,'. ')
	END

	--IF @CPLType='Lender' AND @LenderId IS NULL AND ISNULL(@LenderName, '') = ''
	--	BEGIN 
	--		SELECT @Errors = CONCAT(@Errors, 'Lender information is missing. ')
	--	END
	--IF @CPLType='Seller' AND @SellerId IS NULL AND ISNULL(@SellerName, '') = ''
	--	BEGIN 
	--		SELECT @Errors = CONCAT(@Errors, 'Seller information is missing. ')
	--	END
	--IF @CPLType='Buyer' AND ISNULL(@Buyer, '') = ''
	--	BEGIN 
	--		SELECT @Errors = CONCAT(@Errors, 'Buyer information is missing. ')
	--	END

	IF @CPLType='Lender'
	BEGIN
		IF @LenderId IS NULL
			SELECT @Beneficiary = @LenderName
		ELSE
			BEGIN
				SELECT @Beneficiary = lender_comp_name FROM dbo.t_lenders WHERE lender_id=@LenderId

				IF @Beneficiary IS NULL
					SELECT @Errors = CONCAT(@Errors, 'Lender not found: ', @LenderId, '. ')
			END
	END

	IF @CPLType='Buyer'
		SELECT @Beneficiary = @Buyer

	IF @CPLType = 'Seller'
	BEGIN
		IF @SellerId IS NULL
			SELECT @Beneficiary = @SellerName
		ELSE
			BEGIN
				SELECT @Beneficiary = lender_comp_name FROM dbo.t_lenders WHERE lender_id=@SellerId

				IF @Beneficiary IS NULL
					SELECT @Errors = CONCAT(@Errors, 'Seller not found: ', @SellerId, '. ')
			END
	END

	-- RETURN THE ERRORS
	SELECT @Errors = TRIM(@Errors)
	IF LEN(@Errors) > 0
	BEGIN
		; THROW 50001, @Errors, 1
		RETURN 0
	END

	-- CREATE CPL
	INSERT INTO dbo.t_ICL(
		FileNumber, Agent, LenderName, StateName, 
		ISAOA, ATIMA, VA, HUD, EscrowID, ICLDate, GFNumber,  
		Version, ClosingLetterID, ShowBranches, Borrower, 
		Status, PolicyID, TransactionType, AttorneyID, 
		LenderExtra, LiabilityAmount, EstimatedClosingDate, Impersonator, 
		GFNumberClean, ExtraOfficeId, Source, OfficeAddress, ExtraOfficeAddress, AttorneyAddress)
	VALUES(
	@LetterReference, -- FileNumber - varchar(2000)
	@Username  , -- Agent - varchar(50)
	@Beneficiary  , -- LenderName - varchar(300)
	@PropertyState  , -- StateName - varchar(20)
	@ISAOA, -- ISAOA - bit
	@ATIMA, -- ATIMA - bit
	@VA, -- VA - bit
	@HUD, -- HUD - bit
	@OfficeId   , -- EscrowID - int
	@date, -- ICLDate - datetime
	@GFNumber  , -- GFNumber - varchar(200)
	@Version   , -- Version - int
	@CPLTemplateID   , -- ClosingLetterID - int
	@ShowBranches, -- ShowBranches - bit
	@Buyer  , -- Borrower - varchar(2000)
	@Status  , -- Status - varchar(10)
	@PolicyID   , -- PolicyID - int
	@TransactionType  , -- TransactionType - varchar(50)
	@AttorneyID   , -- AttorneyID - int
	@LenderExtra  , -- LenderExtra - varchar(500)
	@LiabilityAmount, -- LiabilityAmount - money
	@EstimatedClosingDate, -- EstimatedClosingDate - datetime
	@Impersonator  , -- Impersonator - varchar(500)
	@GFNumberClean  , -- GFNumberClean - varchar(200)
	@ExtraOfficeId   , -- ExtraOfficeId - int
	@Source  -- Source - varchar(200)
	,@OfficeAddress, @ExtraOfficeAddress, @AttorneyAddress
	    )

	SELECT @id=SCOPE_IDENTITY() 
	SELECT @code = dbo.CalculateCPLCode(@id, @date)

	-- UPDATES TO CPL
	UPDATE dbo.t_ICL SET
		Code = @Code
		WHERE ICLID=@id

	SELECT @id AS Id, @Code AS Code, @AgentID AS AgentId, @OfficeName AS OfficeName
END