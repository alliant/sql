GO
/****** Object:  StoredProcedure [dbo].[spGetOrderProperty]    Script Date: 8/21/2019 10:38:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetOrderProperty]
	-- Add the parameters for the stored procedure here
	@orderID VARCHAR(30) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  SELECT  [orderID],
          [seq],
          [orderSeq],
          [propType],
          (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'TPS' AND [objAction] = 'Order' AND [objProperty] = 'Type' AND [objID] = o.[propType]) AS [propTypeDesc],
          [parcelID],
          [address],
          [city],
          [state],
          [zip],
          [address] + ' ' + [city] + ', ' + [state] + ' ' + [zip] AS [fullAddress],
          [mapSection],
          [mapTownship],
          [mapTownshipDir],
          CASE [mapTownshipDir]
            WHEN '' THEN [mapTownship]
            ELSE [mapTownship] + ' ' + (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'TPS' AND [objAction] = 'Order' AND [objProperty] = 'Type' AND [objID] = o.[mapTownshipDir])
          END AS [mapTownshipDesc],
          [mapRange],
          [mapRangeDir],
          CASE [mapRangeDir]
            WHEN '' THEN [mapRange]
            ELSE [mapRange] + ' ' + (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'TPS' AND [objAction] = 'OrderProperty' AND [objProperty] = 'Direction' AND [objID] = o.[mapRangeDir])
          END AS [mapRangeDesc],
          [platName],
          [platLot],
          [platBlock],
          [platBook],
          [platPage],
          [landName],
          [orderID] + '-' + CONVERT(VARCHAR, [seq]) AS [orderIDSeq],
          'false' AS [isNew],
          'false' AS [isModify],
          'false' AS [isDelete]
  FROM    [dbo].[orderproperty] o
  WHERE   [orderID] = CASE WHEN @orderID = '' THEN [orderID] ELSE @orderID END
END
