GO
/****** Object:  StoredProcedure [dbo].[spArAgentStatementBalance]    Script Date: 3/2/2023 1:45:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spArAgentStatementBalance]
	-- Add the parameters for the stored procedure here	
	@agentID         VARCHAR(MAX) = 'ALL',
	@startDate       DATE         = NULL,
	@endDate         DATE         = NULL,
	@StatementDate   DATE         = NULL,
	@TranType        VARCHAR(10)  = 'F'

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	CREATE TABLE #agentTable ([agentID] VARCHAR(30))
    INSERT INTO #agentTable ([agentID])
    SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)


	CREATE TABLE #agentFile (agentID      VARCHAR(80),
	                         invoiceID    VARCHAR(50),
	                         fileNumber   VARCHAR(50),
							 fileID       VARCHAR(50),
							 arAmount     [DECIMAL](17, 2) NULL,
							 periodCount  INTEGER,
							 arCountOld   INTEGER,
							 tranDate     DATETIME)

	CREATE TABLE #arAgentStatement (stateID       VARCHAR(2),
                                    agentID       VARCHAR(80),
                                    fileNumber    VARCHAR(50),
									fileID        VARCHAR(50),
									invoiceID     VARCHAR(50),
                                    policyDate    DATETIME,
                                    policyNumber  VARCHAR(100),
									batchID       INTEGER,
                                    tranCode      VARCHAR(15), 
									TranID        VARCHAR(50),
                                    adj           BIT,
                                    liability     [DECIMAL](17, 2) NULL,
                                    policyPremium [DECIMAL](17, 2) NULL,
                                    remittanceAmt [DECIMAL](17, 2) NULL,
                                    paymentAmt    [DECIMAL](17, 2) NULL,
									refundAmt     [DECIMAL](17, 2) NULL,
                                    receviedDate  DATETIME,                                   
                                    checkNumber   VARCHAR(100),
									checkAmt      [DECIMAL](17, 2) NULL,
                                    difference    [DECIMAL](17, 2) NULL,
									paymentType   VARCHAR(8),
									type          VARCHAR(2),
									sourceID      VARCHAR(50),
									sourceType    VARCHAR(8))



    IF @TranType = 'F' OR @TranType = 'B'
	BEGIN
		/* Get all file and its remaining amount as of given date from artran */
        INSERT INTO #agentFile ([agentID],[fileID],[arAmount],[periodCount],[arCountOld])
        SELECT at.entityID,
               at.fileID, 
               SUM(CASE WHEN at.tranDate >= CAST(@startDate AS DATE)
			             AND at.tranDate <= CAST(@endDate AS DATE)
			             AND at.transType = 'F'
				    	 AND at.Type      <> 'P'
                         AND at.Type      <> 'C'			         						   
                         THEN at.tranamt ELSE 0 END),
			   COUNT(CASE WHEN at.tranDate >= CAST(@startDate AS DATE)
                           AND at.tranDate <= CAST(@endDate AS DATE)						   
                           THEN 1 ELSE NULL END),
               COUNT(CASE WHEN at.tranDate <= CAST(@startDate AS DATE)                             
						  AND (at.type = 'I' OR at.type = 'R')						     				 						    
                          THEN 1 ELSE NULL END)		   
          FROM [dbo].[artran] at 
          INNER JOIN 
          [dbo].[agent] a 
          ON a.agentID = at.entityID
          WHERE at.entity = 'A'
	        AND at.[entityID] IN (SELECT [agentID] FROM #agentTable)
		    AND at.transType = 'F'
		    AND (at.type = 'I' OR at.type = 'R' OR at.type = 'A' OR at.type = 'P' OR at.type = 'C')		    	    
	      GROUP BY at.entityID,at.fileID
	
	
        /* Get all file and its remaining amount as of given date from artranh */
	    INSERT INTO #agentFile ([agentID],[fileID],[arAmount],[periodCount],[arCountOld])
        SELECT at.entityID,
               at.fileID, 
               SUM(CASE WHEN at.tranDate >= CAST(@startDate AS DATE)
			             AND at.tranDate <= CAST(@endDate AS DATE)
			             AND at.transType = 'F'
				    	 AND at.Type      <> 'P'
                         AND at.Type      <> 'C'			         						   
                         THEN at.tranamt ELSE 0 END),
			   COUNT(CASE WHEN at.tranDate >= CAST(@startDate AS DATE)
                          AND  at.tranDate <= CAST(@endDate AS DATE)					      
                          THEN 1 ELSE NULL END),
               COUNT(CASE WHEN at.tranDate <= CAST(@startDate AS DATE)                             
						  AND (at.type = 'I' OR at.type = 'R')						     				 						    
                          THEN 1 ELSE NULL END)		   
          FROM [dbo].[artranh] at 
          INNER JOIN 
          [dbo].[agent] a 
          ON a.agentID = at.entityID
          WHERE at.entity = 'A'
	        AND at.[entityID] IN (SELECT [agentID] FROM #agentTable)
		    AND at.transType = 'F'
		    AND (at.type = 'I' OR at.type = 'R' OR at.type = 'A' OR at.type = 'P' OR at.type = 'C')		    	    
	      GROUP BY at.entityID,at.fileID
    
	    /* Delete all #agent file records when no batchform/activity posted in period or 
		   batchform posted as of period start date and has no balance as of date */
	    DELETE #agentFile
	    WHERE  #agentFile.periodCount = 0 
		  AND (#agentFile.arCountOld = 0 OR (#agentFile.arCountOld <> 0 AND #agentFile.arAmount = 0))		 
		
	    /* Get policy information from policy table */
        INSERT INTO #arAgentStatement ([agentID],[fileID],[batchID],[policyNumber],[remittanceAmt],paymentType) 
	    SELECT af.entityID,
	           af.fileID,
			   af.sourceID,
			   af.reference,
			   af.tranAmt,
			   'Amt'
	      FROM (SELECT atn.entityID,
	                   atn.fileID,
				       MAX(atn.sourceID) AS sourceID,
				       atn.reference,
				       SUM (atn.tranAmt) AS tranAmt
		  FROM #agentFile a 
	        INNER JOIN artran atn
	        ON a.agentID = atn.entityID
		    AND a.fileID = atn.fileID
		  WHERE atn.entity   = 'A'
		    AND atn.transType = 'F'
			AND atn.tranDate >= CAST(@startDate AS DATE)
		    AND atn.tranDate <= CAST(@endDate AS DATE)	
		    AND (atn.type = 'I' OR atn.type = 'R')	    	    
	      GROUP BY atn.entityID, atn.fileID,atn.reference) af

        INSERT INTO #arAgentStatement ([agentID],[fileID],[batchID],[policyNumber],[remittanceAmt],paymentType) 
	    SELECT af.entityID,
	           af.fileID,
			   af.sourceID,
			   af.reference,
			   af.tranAmt,
			   'Amt'
	      FROM (SELECT atn.entityID,
	                   atn.fileID,
				       MAX(atn.sourceID) AS sourceID,
				       atn.reference,
				       SUM (atn.tranAmt) AS tranAmt
		  FROM #agentFile a 
	        INNER JOIN artranh atn
			ON a.agentID = atn.entityID
			AND a.fileID = atn.fileID
		  WHERE  atn.entity   = 'A'
		    AND atn.transType = 'F'
			AND atn.tranDate >= CAST(@startDate AS DATE)
			AND atn.tranDate <= CAST(@endDate AS DATE)	
		    AND (atn.type = 'I' OR atn.type = 'R')	    	    
	      GROUP BY atn.entityID, atn.fileID,atn.reference) af
	 														  
	    UPDATE #arAgentStatement
	      SET  #arAgentStatement.policyDate    = bf.effDate,
	           #arAgentStatement.tranCode      = bf.statCode,
	           #arAgentStatement.liability     = s.liability,
		       #arAgentStatement.policyPremium = s.gross
	      FROM #arAgentStatement a
	      INNER JOIN (SELECT bf.batchID,bf.policyID,SUM(bf.liabilityAmount) AS liability,SUM(bf.grossPremium) AS gross FROM batchform bf
	      GROUP BY bf.batchID,bf.policyID ) s
	      ON  a.batchID      = s.batchID
	      AND a.policyNumber = s.policyID
	      INNER JOIN 
	      batchform bf
	      ON bf.policyID = s.policyID 
	      WHERE ISNUMERIC(a.policyNumber) = 1

	    UPDATE #arAgentStatement
	      SET #arAgentStatement.policyPremium  = b.gross,
	          #arAgentStatement.tranCode       = b.statCode
	      FROM #arAgentStatement af INNER JOIN (SELECT a.fileID, a.statCode, a.formType, SUM(a.grossPremium) AS gross 
	                                              FROM (SELECT s.fileID,s.formType,s.statCode, s.batchID,bf.grossPremium FROM batchform bf INNER JOIN
                                               (SELECT fileID,formType,statCode, MAX(batchID) AS batchID,formID FROM batchform
                                                  GROUP BY fileID,formType,statCode,formID) s
                                                  ON bf.fileID    = s.fileID
                                                  AND bf.batchID  = s.batchID
                                                  AND bf.formID   = s.formID
                                                  INNER JOIN artran ar
                                                  ON ar.fileID      = s.fileID
                                                  AND ar.sourceID   = s.batchID 
											      WHERE ar.entityID = @agentID
											      AND ar.reference  = 'ALTA Closing Protection Letter'														
											      AND s.formType  = 'C') a 
												  GROUP BY a.fileID, a.statCode, a.formType) b
  	 	  ON b.fileID           = af.fileID
	      WHERE af.policyNumber = 'ALTA Closing Protection Letter'	

	    UPDATE #arAgentStatement
	      SET #arAgentStatement.policyPremium  = b.gross,
	          #arAgentStatement.tranCode       = b.statCode
	      FROM #arAgentStatement af INNER JOIN (SELECT a.fileID, a.statCode, a.formType, SUM(a.grossPremium) AS gross 
	                                              FROM (SELECT s.fileID,s.formType,s.statCode, s.batchID,bf.grossPremium FROM batchform bf INNER JOIN
                                               (SELECT fileID,formType,statCode, MAX(batchID) AS batchID,formID FROM batchform
                                                  GROUP BY fileID,formType,statCode,formID) s
                                                  ON bf.fileID    = s.fileID
                                                  AND bf.batchID  = s.batchID
                                                  AND bf.formID   = s.formID
                                                  INNER JOIN artranh ar
                                                  ON ar.fileID      = s.fileID
                                                  AND ar.sourceID   = s.batchID 
											      WHERE ar.entityID = @agentID
											      AND ar.reference  = 'ALTA Closing Protection Letter'														
											      AND s.formType  = 'C') a 
												  GROUP BY a.fileID, a.statCode, a.formType) b
  	 	  ON b.fileID           = af.fileID
	      WHERE af.policyNumber = 'ALTA Closing Protection Letter'			  

	    /* Set adj field for R(Reprocess) record */
	    UPDATE #arAgentStatement
	      SET adj = 1
	      FROM #arAgentStatement a
	      INNER JOIN
	      artran at
	      ON at.entityID    = a.agentID
	      AND at.fileID     = a.fileID
	      AND at.reference  = a.policyNumber	   
	      WHERE at.type     = 'R'

	    UPDATE #arAgentStatement
	      SET adj = 1
	      FROM #arAgentStatement a
	      INNER JOIN
	      artranh at
	      ON at.entityID   = a.agentID
	      AND at.fileID    = a.fileID
	      AND at.reference = a.policyNumber	   
	      WHERE at.type    = 'R'
		  
	END
	
	/* Get misc invoices from artran table */
	IF @TranType = 'I' OR @TranType = 'B'
	BEGIN
	    DELETE FROM #agentFile
			
		/* Get all invoice and its remaining amount as of given date from artran */
        INSERT INTO #agentFile ([agentID],[invoiceID],arCountOld,arAmount)
          SELECT at.entityID,
                 at.tranID,
                 COUNT(CASE WHEN at.tranDate <= CAST(@startDate AS DATE)                             
						     AND at.type = 'I'
                             THEN 1 ELSE NULL END),				 
                 SUM(CASE WHEN at.tranDate >= CAST(@startDate AS DATE)
				           AND at.tranDate <= CAST(@endDate AS DATE)
			               AND at.transType = ' '
				    	   AND (at.type = 'I' OR at.type = 'A')		         						   
                          THEN at.tranamt ELSE 0 END)
		   
          FROM [dbo].[artran] at 
          INNER JOIN 
          [dbo].[agent] a 
          ON a.agentID = at.entityID
          WHERE at.entity = 'A'
		    AND (at.void = 0 or at.voidDate > CAST(@endDate AS DATE))
	        AND at.[entityID] IN (SELECT [agentID] FROM #agentTable)
		    AND at.transType = ' '
		    AND (at.type = 'I' OR at.type = 'A')		    	    
	      GROUP BY at.entityID,at.tranID
				
	    /* Get all invoice and its remaining amount as of given date from artran */
        INSERT INTO #agentFile ([agentID],[invoiceID],arCountOld,arAmount)
          SELECT at.entityID,
                 at.tranID,
                 COUNT(CASE WHEN at.tranDate <= CAST(@startDate AS DATE)                             
						     AND at.type = 'I'
                             THEN 1 ELSE NULL END),				 
                 SUM(CASE WHEN at.tranDate >= CAST(@startDate AS DATE)
				           AND at.tranDate <= CAST(@endDate AS DATE)
			               AND at.transType = ' '
				    	   AND (at.type = 'I' OR at.type = 'A')		         						   
                          THEN at.tranamt ELSE 0 END)
		   
          FROM [dbo].[artranh] at 
          INNER JOIN 
          [dbo].[agent] a 
          ON a.agentID = at.entityID
          WHERE at.entity = 'A'
		    AND (at.void = 0 or at.voidDate > CAST(@endDate AS DATE))
	        AND at.[entityID] IN (SELECT [agentID] FROM #agentTable)
		    AND at.transType = ' '
		    AND (at.type = 'I' OR at.type = 'A')		    	    
	      GROUP BY at.entityID,at.tranID
			
		/* Delete all #agent file records where amount is zero and no activity in given time range */
	    DELETE #agentFile
	    WHERE #agentFile.arCountOld  = 0 
		  OR (#agentFile.arCountOld  > 0 AND #agentFile.arAmount = 0)
		
        INSERT INTO #arAgentStatement ([agentID],[invoiceID],[fileID],[policyDate],[policyNumber],[remittanceAmt],[type],paymentType) 
	    SELECT atn.entityID,
	           atn.tranID,
			   atn.fileID,
			   atn.trandate,
			   atn.reference,
			   atn.tranAmt,
			   'I',
			   'Amt'
	      FROM artran atn
		  WHERE atn.entity    = 'A'
		    AND atn.entityID IN (SELECT [agentID] FROM #agentTable)
			AND (atn.void = 0 or atn.voidDate > CAST(@endDate AS DATE))
		    AND atn.transType = ' '
			AND ((atn.tranDate >= CAST(@startDate AS DATE) AND atn.tranDate <= CAST(@endDate AS DATE)) 
			 OR (atn.tranID IN (SELECT [invoiceID] FROM #agentFile)))  	
		    AND atn.type      = 'I'
            AND atn.seq       = 0
	    
		/*void*/
        INSERT INTO #arAgentStatement ([agentID],[invoiceID],[fileID],[policyDate],[policyNumber],[remittanceAmt],[type],paymentType) 
	    SELECT atn.entityID,
	           atn.tranID,
			   atn.fileID,
			   atn.trandate,
			   atn.reference,
			   -atn.tranAmt,
			   'I',
			   'Amt-MI'
	      FROM artran atn
		  WHERE atn.entity    = 'A'
		    AND atn.entityID IN (SELECT [agentID] FROM #agentTable)
		    AND (atn.tranDate < CAST(@startDate AS DATE) )
		    AND (atn.void = 1 and atn.voidDate >= CAST(@startDate AS DATE) and atn.voidDate <= CAST(@endDate AS DATE))
	            AND atn.transType = ' '
		    AND atn.type      = 'I'
                    AND atn.seq       = 0

        INSERT INTO #arAgentStatement ([agentID],[invoiceID],[fileID],[policyDate],[policyNumber],[remittanceAmt],[type],paymentType) 
	    SELECT atn.entityID,
	           atn.tranID,
			   atn.fileID,
			   atn.trandate,
			   atn.reference,
			   atn.tranAmt,
			   'I',
			   'Amt'
	      FROM artranh atn
		  WHERE atn.entity    = 'A'
		    AND atn.entityID IN (SELECT [agentID] FROM #agentTable)
			AND (atn.void = 0 or atn.voidDate > CAST(@endDate AS DATE))
		    AND atn.transType = ' '
			AND ((atn.tranDate >= CAST(@startDate AS DATE) AND atn.tranDate <= CAST(@endDate AS DATE)) 
			 OR (atn.tranID IN (SELECT [invoiceID] FROM #agentFile)))	
		    AND atn.type      = 'I'
            AND atn.seq       = 0		
						  			
	END
	
    INSERT INTO #arAgentStatement ([agentID],[TranID],[type],[paymentAmt],[refundAmt],[paymentType]) 
	SELECT at.entityID,
	       at.TranID,
		   at.type,
		   SUM(CASE WHEN ((at.void = 0 OR (at.voidDate > @endDate))
			         AND   at.tranDate >= @startDate
					 AND   at.tranDate <= @endDate)
			         THEN  at.tranAmt ELSE 0 END),
		   SUM(CASE WHEN (at.type = 'P' and at.seq > 0 and at.tranDate <= @endDate)
			        THEN at.tranAmt ELSE 0 END),		 
		   'Amt-At'
	       FROM artran at
	       WHERE (at.type     = 'P')
	       AND at.[entityID] IN (SELECT [agentID] FROM #agentTable) 
	       GROUP BY at.entityID,at.TranID,at.type

    /* Void payment */
	INSERT INTO #arAgentStatement ([agentID],[TranID],[type],[paymentAmt],[refundAmt],[paymentType]) 
	SELECT at.entityID,
	       at.TranID,
		   at.type,
		   SUM(CASE WHEN ((at.void = 1 OR (at.voidDate > @endDate))
			         AND   at.voidDate >= @startDate
			         AND   at.voidDate <= @StatementDate)
			         THEN  -at.tranAmt ELSE 0 END),
		   SUM(CASE WHEN (at.type = 'P' and at.seq > 0 and at.voidDate <= @StatementDate)
			        THEN at.tranAmt ELSE 0 END),		 
		   'Amt-At'
	       FROM artran at
	       WHERE (at.type     = 'P') 
		   AND at.tranDate < CAST(@startDate AS DATE)
		   AND (at.void = 1 and (at.voidDate >= CAST(@startDate AS DATE) and at.voidDate <= CAST(@StatementDate AS DATE)) )
           AND at.[entityID] IN (SELECT [agentID] FROM #agentTable) 
	       GROUP BY at.entityID,at.TranID,at.type

    INSERT INTO #arAgentStatement ([agentID],[TranID],[type],[paymentAmt],[refundAmt],[paymentType]) 
	SELECT at.entityID,
	       at.TranID,
		   at.type,
		   SUM(CASE WHEN ((at.void = 0 OR (at.voidDate > @endDate))
			         AND   at.tranDate >= @startDate
					 AND   at.tranDate <= @endDate)
			         THEN  at.tranAmt ELSE 0 END),
		   SUM(CASE WHEN (at.type = 'C' and at.seq > 0 and at.tranDate <= @endDate)
			        THEN at.tranAmt ELSE 0 END),		 
		   'Amt-At'
	       FROM artran at
	       WHERE (at.type     = 'C')
	       AND at.[entityID] IN (SELECT [agentID] FROM #agentTable) 
	       GROUP BY at.entityID,at.TranID,at.type
		   
    INSERT INTO #arAgentStatement ([agentID],[sourceID],[sourceType],[paymentAmt],[paymentType]) 
	SELECT at.entityID,
	       at.sourceID,
		   at.sourceType,
		   at.tranAmt,
		   'Amt-Adj'
	       FROM artran at
	       WHERE (at.type = 'W')
		   AND  at.seq = 0
		   AND (at.void = 0 OR (at.voidDate > @endDate))
		   AND  (at.tranDate >= @startDate AND  at.tranDate <= @endDate)
	       AND at.[entityID] IN (SELECT [agentID] FROM #agentTable)

    INSERT INTO #arAgentStatement ([agentID],[TranID],[type],[paymentAmt],[refundAmt],[paymentType]) 
	SELECT at.entityID,
	        at.TranID,
			at.type,
		    SUM(CASE WHEN ((at.void = 0 OR (at.voidDate > @endDate))
			          AND   at.tranDate >= @startDate
			          AND   at.tranDate <= @endDate)
			          THEN  at.tranAmt ELSE 0 END),
		    SUM(CASE WHEN (at.type = 'P' AND at.seq > 0 AND at.tranDate <= @endDate)
			           THEN at.tranAmt ELSE 0 END),			 
			'Amt-At'
	  FROM artranh at
	  WHERE (at.type   = 'P')
	    AND at.[entityID] IN (SELECT [agentID] FROM #agentTable) 
	    GROUP BY at.entityID,at.TranID,at.type

    INSERT INTO #arAgentStatement ([agentID],[TranID],[type],[paymentAmt],[refundAmt],[paymentType]) 
	SELECT at.entityID,
	        at.TranID,
			at.type,
		    SUM(CASE WHEN ((at.void = 0 OR (at.voidDate > @endDate))
			          AND   at.tranDate >= @startDate
			          AND   at.tranDate <= @endDate)
			          THEN  at.tranAmt ELSE 0 END),
		    SUM(CASE WHEN (at.type = 'C' AND at.seq > 0 AND at.tranDate <= @endDate)
			           THEN at.tranAmt ELSE 0 END),			 
			'Amt-At'
	  FROM artranh at
	  WHERE (at.type   = 'C')
	    AND at.[entityID] IN (SELECT [agentID] FROM #agentTable) 
	    GROUP BY at.entityID,at.TranID,at.type
		
    INSERT INTO #arAgentStatement ([agentID],[sourceID],[sourceType],[paymentAmt],[paymentType]) 
	SELECT at.entityID,
	       at.sourceID,
		   at.sourceType,
		   at.tranAmt,
		   'Amt-Adj'
	       FROM artranh at
	       WHERE (at.type = 'W')
		   AND  at.seq = 0
		   AND (at.void = 0 OR (at.voidDate > @endDate))
		   AND  (at.tranDate >= @startDate AND  at.tranDate <= @endDate)
	       AND at.[entityID] IN (SELECT [agentID] FROM #agentTable)

    UPDATE #arAgentStatement	
      SET  #arAgentStatement.fileID       = at.fileID,
	       #arAgentStatement.receviedDate = at.tranDate,
	       #arAgentStatement.checkAmt     = at.tranAmt,
		   #arAgentStatement.checkNumber  = at.reference 
	  FROM artran at
      WHERE at.entityID              = #arAgentStatement.agentID
        AND at.tranID                = #arAgentStatement.tranID
		AND at.type                  = #arAgentStatement.type
	    AND at.seq                   = 0
        AND (#arAgentStatement.type = 'C' or #arAgentStatement.type = 'P')	 
    
    UPDATE #arAgentStatement	
      SET  #arAgentStatement.fileID       = at.fileID,
	       #arAgentStatement.receviedDate = at.tranDate,
	       #arAgentStatement.checkAmt     = at.tranAmt,
		   #arAgentStatement.checkNumber  = at.reference 
	  FROM artranh at
      WHERE at.entityID              = #arAgentStatement.agentID
        AND at.tranID                = #arAgentStatement.tranID
		AND at.type                  = #arAgentStatement.type
	    AND at.seq                   = 0
        AND (#arAgentStatement.type = 'C' or #arAgentStatement.type = 'P')
		
    UPDATE #arAgentStatement	
      SET  #arAgentStatement.fileID       = at.fileID,
	       #arAgentStatement.receviedDate = at.tranDate,
	       #arAgentStatement.checkAmt     = at.tranAmt,
		   #arAgentStatement.checkNumber  = at.reference 
	  FROM artran at
      WHERE at.entityID              = #arAgentStatement.agentID
        AND at.artranID              = #arAgentStatement.sourceID
		AND at.type                  = #arAgentStatement.sourcetype
	    AND at.seq                   = 0
        AND (#arAgentStatement.sourcetype = 'W')
		
    UPDATE #arAgentStatement	
      SET  #arAgentStatement.fileID       = at.fileID,
	       #arAgentStatement.receviedDate = at.tranDate,
	       #arAgentStatement.checkAmt     = at.tranAmt,
		   #arAgentStatement.checkNumber  = at.reference 
	  FROM artranh at
      WHERE at.entityID              = #arAgentStatement.agentID
        AND at.artranID              = #arAgentStatement.sourceID
		AND at.type                  = #arAgentStatement.sourcetype
	    AND at.seq                   = 0
        AND (#arAgentStatement.sourcetype = 'W')
	
	DELETE #arAgentStatement WHERE paymentType = 'Amt-At' AND (paymentAmt  = 0 OR receviedDate > CAST(@endDate AS DATE))

    UPDATE #arAgentStatement
	  SET #arAgentStatement.fileNumber = at.filenumber
	  FROM artran at
	  WHERE  at.entityID = #arAgentStatement.agentID
	    AND  at.fileID   = #arAgentStatement.fileID
		AND  at.seq      = 0
		/* AND  at.type     = 'F' */
		
    UPDATE #arAgentStatement
	  SET #arAgentStatement.fileNumber = at.filenumber
	  FROM artranh at
	  WHERE  at.entityID = #arAgentStatement.agentID
	    AND  at.fileID   = #arAgentStatement.fileID
		AND  at.seq      = 0

    SELECT * FROM #arAgentStatement ORDER BY agentID,fileID
END
