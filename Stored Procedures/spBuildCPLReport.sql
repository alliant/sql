USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spCalculateReportGap]    Script Date: 5/16/2016 10:07:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		  John Oliver
-- Create date: 5/16/2016
-- Description:	Calculates the Report and Policy
--              Distance Gaps
-- =============================================
ALTER PROCEDURE [dbo].[spBuildCPLReport]
	-- Add the parameters for the stored procedure here
	@batchID INT = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  -- Build the report
  SELECT  cnt.batchID,
          cnt.fileNumber,
          cnt.totalCount,
          cnt.policyCount
  FROM    batch b INNER JOIN
          batchform bf
       ON b.batchID = bf.batchID INNER JOIN
          (
          SELECT  a.batchID,
                  a.fileNumber,
                  a.cnt AS totalCount,
                  b.cnt AS policyCount
          FROM    (
                  -- Get the file count
                  SELECT  bf.batchID,bf.fileNumber,COUNT(*) AS cnt
                  FROM    batchform bf
                  WHERE   bf.batchID = @batchID
                  GROUP BY batchID,fileNumber
                  ) a INNER JOIN
                  (
                  -- Get the file count
                  SELECT  bf.batchID,bf.fileNumber,COUNT(*) AS cnt
                  FROM    batchform bf
                  WHERE   bf.batchID = @batchID
                  AND     bf.formType = 'P'
                  GROUP BY batchID,fileNumber
                  ) b
               ON a.fileNumber = b.fileNumber
              AND a.batchID = b.batchID
          ) cnt
       ON bf.batchID = cnt.batchID
      AND bf.fileNumber = cnt.fileNumber
  GROUP BY cnt.batchID,
           cnt.fileNumber,
           cnt.totalCount,
           cnt.policyCount
END
