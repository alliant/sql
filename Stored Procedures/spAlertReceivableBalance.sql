GO
/****** Object:  StoredProcedure [dbo].[spAlertClaimsRatio]    Script Date: 7/5/2017 12:13:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spAlertReceivableBalance]
  @agentID VARCHAR(MAX) = '',
  @user VARCHAR(100) = 'compass@alliantnational.com',
  @preview BIT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  IF (@agentID = '')
    SET @agentID = 'ALL'
  
  CREATE TABLE #agentTable ([agentID] VARCHAR(30))
  INSERT INTO #agentTable ([agentID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)

  DECLARE -- Used for the preview
          @source VARCHAR(30) = 'AP',
          @processCode VARCHAR(30) = 'AP02',
          @score DECIMAL(18,2) = 0,
          @date DATETIME = NULL,
          -- Notes for the alert note
          @note VARCHAR(MAX) = ''

  CREATE TABLE #receivableTable
  (
    [agentID] VARCHAR(30),
    [date] DATETIME,
    [daysOver] INTEGER,
    [amountDue] DECIMAL(18,2)
  )
  
  INSERT INTO #receivableTable ([agentID],[date],[daysOver],[amountDue])
  SELECT  a.[agentID],
          MIN(b.[DUEDATE]) AS [date],
          [dbo].[GetMaximum](MAX(ISNULL(b.[daysOver],0)),0) AS [daysOver],
          SUM(ISNULL(b.[amountDue],0)) AS [amountDue]
  FROM    [dbo].[agent] a LEFT OUTER JOIN
          (
          SELECT  RTRIM(LTRIM(CM.[CUSTNMBR])) AS [agentID],
                  RM.[DUEDATE],
                  DATEDIFF(d, RM.[DUEDATE], GETDATE()) AS [daysOver],
                  CASE
                   WHEN RM.[RMDTYPAL] < 7 THEN RM.[CURTRXAM]
                   ELSE 0
                  END AS [amountDue]
          FROM    [ANTIC].[dbo].[RM20101] RM INNER JOIN 
                  [ANTIC].[dbo].[RM00101] CM
          ON      RM.[CUSTNMBR] = CM.[CUSTNMBR] LEFT OUTER JOIN
                  [ANTIC].[dbo].[RM00103] S
          ON      RM.[CUSTNMBR] = S.[CUSTNMBR]
          WHERE   RM.[VOIDSTTS] = 0 
          AND     RM.[CURTRXAM] <> 0
          AND     RM.[DUEDATE] > '2005-01-01'
          ) b
  ON      a.[agentID] = b.[agentID]
  WHERE   a.[agentID] IN (SELECT [agentID] FROM #agentTable)
  AND     [daysOver] > 60
  GROUP BY a.[agentID]

  IF (@preview = 0)
  BEGIN
    -- Actual Costs Ratio
    DECLARE cur CURSOR LOCAL FOR
    SELECT  [agentID],
            [amountDue],
            [date],
            'The receivable of $' + [dbo].[FormatNumber] ([amountDue], 0) + ' is ' + [dbo].[FormatNumber] ([daysOver], 0) + ' days over due'
    FROM    #receivableTable

    OPEN cur
    FETCH NEXT FROM cur INTO @agentID, @score, @date, @note
    WHILE @@FETCH_STATUS = 0 
    BEGIN
      EXEC [dbo].[spInsertAlert] @source = @source, 
                                 @processCode = @processCode, 
                                 @user = @user, 
                                 @agentID = @agentID, 
                                 @score = @score,
                                 @note = @note,
                                 @effDate = @date
      FETCH NEXT FROM cur INTO @agentID, @score, @date, @note
    END
    CLOSE cur
    DEALLOCATE cur
  END

  SELECT  [agentID] AS [agentID],
          @source AS [source],
          @processCode AS [processCode],
          [dbo].[GetAlertThreshold] (@processCode, [amountDue]) AS [threshold],
          [dbo].[GetAlertThresholdRange] (@processCode, [amountDue]) AS [thresholdRange],
          [dbo].[GetAlertSeverity] (@processCode, [amountDue]) AS [severity],
          [amountDue] AS [score],
          [dbo].[GetAlertScoreFormat] (@processCode, [amountDue]) AS [scoreDesc],
          [date] AS [effDate],
          [dbo].[GetAlertOwner] (@processCode) AS [owner],
          'The receivable of $' + [dbo].[FormatNumber] ([amountDue], 0) + ' is ' + [dbo].[FormatNumber] ([daysOver], 0) + ' days over due' AS [note]
  FROM    #receivableTable

  DROP TABLE #agentTable
  dROP TABLE #receivableTable
END
