USE [COMPASS_DVLP]
GO
/****** Object:  StoredProcedure [dbo].[spInsertAgentFile]    Script Date: 3/20/2020 9:12:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spInsertAgentFile]
	-- Add the parameters for the stored procedure here
	@agentID VARCHAR(20),
  @fileNumber VARCHAR(1000),
  @addr1 VARCHAR(100),
  @addr2 VARCHAR(100),
  @addr3 VARCHAR(100),
  @addr4 VARCHAR(100),
  @city VARCHAR(100),
  @countyID VARCHAR(50),
  @state VARCHAR(20),
  @zip VARCHAR(20),
  @stage VARCHAR(20),
  @transactionType VARCHAR(20),
  @insuredType VARCHAR(20),
  @reportingDate DATETIME,
  @liability DECIMAL(18,2),
  @notes VARCHAR(MAX),
  @agentFileID INTEGER OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  DECLARE @fileID VARCHAR(100)

  --normalize the file number
  SET @fileID = [dbo].[NormalizeFileID] (@fileNumber)

  IF @fileID IS NULL
    RETURN

  -- Insert statements for procedure here
  SELECT @agentFileID = [agentFileID] FROM [dbo].[agentfile] WHERE [agentID] = @agentID AND [fileID] = @fileID
  IF @agentFileID <> 0
  BEGIN
    UPDATE  [agentfile]
    SET     [addr1] = (CASE WHEN (@addr1 <> '' AND @addr1 IS NOT NULL) THEN @addr1 ELSE [addr1] END),
            [addr2] = (CASE WHEN (@addr2 <> '' AND @addr2 IS NOT NULL) THEN @addr2 ELSE [addr2] END),
            [addr3] = (CASE WHEN (@addr3 <> '' AND @addr3 IS NOT NULL) THEN @addr3 ELSE [addr3] END),
            [addr4] = (CASE WHEN (@addr4 <> '' AND @addr4 IS NOT NULL) THEN @addr4 ELSE [addr4] END),
            [city] =  (CASE WHEN (@city <> '' AND @city IS NOT NULL) THEN @city  ELSE [city]  END),
            [countyID] = (CASE WHEN (@countyID <> '' AND @countyID IS NOT NULL) THEN @countyID ELSE [countyID] END),
            [state] = (CASE WHEN (@state <> '' AND @state IS NOT NULL) THEN @state ELSE [state] END),
            [zip] = (CASE WHEN (@zip <> '' AND @zip IS NOT NULL) THEN @zip ELSE [zip] END),
            [stage] = @stage,
            [transactionType] = (CASE WHEN (@transactionType <> '' AND @transactionType IS NOT NULL) THEN @transactionType ELSE [transactionType] END),
	          [reportingDate] = (CASE WHEN (@reportingDate IS NOT NULL AND [reportingDate] > @reportingDate) THEN @reportingDate ELSE [reportingDate] END),
	          [liabilityAmt]  = (CASE WHEN (@liability IS NOT NULL AND @liability <> 0 AND @liability > [liabilityAmt]) THEN @liability ELSE [liabilityAmt] END),
            [insuredType] = (CASE WHEN (@insuredType <> '' AND @insuredType IS NOT NULL) THEN @insuredType ELSE [insuredType] END),
            [notes] = @notes
    WHERE   [agentID] = @agentID 
    AND     [fileID] = @fileID
  END
  ELSE
  BEGIN
    EXEC [dbo].[spGetNextSequence] 'agentFile', @agentFileID OUTPUT
    INSERT INTO [dbo].[agentfile] ([agentFileID],[agentID],[fileNumber],[fileID],[stat],[addr1],[addr2],[addr3],[addr4],[city],[countyID],[state],[zip],[stage],[liabilityAmt],[transactionType],[reportingDate],[insuredType],[notes]) VALUES
    (
      @agentFileID,
      @agentID,
      @fileNumber,
      @fileID,
      (CASE WHEN EXISTS(SELECT artranID FROM artran 
	                     WHERE arTran.entity = 'A'       AND 
						 arTran.entityID     = @agentID  AND 
					     arTran.fileid       = @fileID   AND 
						 arTran.transType    = 'F') 
		    THEN 'O' ELSE 'C' END),
      @addr1,
      @addr2,
      @addr3,
      @addr4,
      @city,
      @countyID,
      @state,
      @zip,
      @stage,
      @liability,
      @transactionType,
      @reportingDate,
      @insuredType,
      @notes)
  END
END
