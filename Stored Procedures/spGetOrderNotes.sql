GO
/****** Object:  StoredProcedure [dbo].[spGetOrderNotes]    Script Date: 8/28/2019 12:44:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetOrderNotes] 
	-- Add the parameters for the stored procedure here
	@orderID VARCHAR(30) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  SELECT  [orderID],
          [seq],
          [noteDate],
          [uid],
          (SELECT [name] FROM [dbo].[sysuser] WHERE [uid] = o.[uid]) AS [uidDesc],
          [category],
          (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'TPS' AND [objAction] = 'OrderNote' AND [objProperty] = 'Category' AND [objID] = o.[category]) AS [categoryDesc],
          [subject],
          [noteType],
          (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'TPS' AND [objAction] = 'OrderNote' AND [objProperty] = 'Type' AND [objID] = o.[noteType]) AS [typeDesc],
          [notes]  
  FROM    [dbo].[ordernote] o
  WHERE   [orderID] = CASE WHEN @orderID = '' THEN [orderID] ELSE @orderID END
END
