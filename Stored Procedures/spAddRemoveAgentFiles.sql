SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spAddRemoveAgentFiles]
	@pcActionID VARCHAR(100)    = '',
	@pcClientID VARCHAR(100)    = '',
	@isError    BIT = 0      OUTPUT,
	@errMsg     VARCHAR(500) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @transCount BIT = 0

	SET @isError = 0
	SET @errMsg  = ''
  
	BEGIN TRY
	IF(@@TRANCOUNT = 0)
	BEGIN
		BEGIN TRANSACTION 
		SET @transCount = 1
	END
	   
	/* Delete agentFiles with no child records */
	EXEC [dbo].[spDeleteAFWithNoChild]
       
	/* Insert agentFiles for BatchForm */
	EXEC [dbo].[spCreateAFBatchForm] @pcActionID, @pcClientID, @isError OUTPUT, @errMsg OUTPUT
	IF(@isError = 1)
		RAISERROR(@errMsg, 16, 1)
     
	/* Insert agentFiles for Policy */
	EXEC [dbo].[spCreateAFPolicy] @pcActionID, @pcClientID, @isError OUTPUT, @errMsg OUTPUT
	IF(@isError = 1)
		RAISERROR(@errMsg, 16, 1)

	/* Insert agentFiles for CPL */
	EXEC [dbo].[spCreateAFCPL] @pcActionID, @pcClientID, @isError OUTPUT, @errMsg OUTPUT
	IF(@isError = 1)
		RAISERROR(@errMsg, 16, 1)

	/* Insert agentFiles for ArTran */
	EXEC [dbo].[spCreateAFArTran] @pcActionID, @pcClientID, @isError OUTPUT, @errMsg OUTPUT
	IF(@isError = 1)
		RAISERROR(@errMsg, 16, 1)

	IF(@transCount = 1)
		COMMIT TRANSACTION 

	END TRY
	BEGIN CATCH
		SET @isError = ERROR_STATE()
		SET @errMsg  = ERROR_MESSAGE()
		IF(@transCount = 1)
			ROLLBACK TRANSACTION 
		RETURN	
	END CATCH

END
