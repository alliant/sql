USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spReportAgentGrpActivities]    Script Date: 07/08/2021 4:16:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportAgentGrpActivities]
	-- Add the parameters for the stored procedure here
    @category VARCHAR(50) = '',
    @year INTEGER = 0,
    @UID varchar(100) = '', 
    @StateID VARCHAR(MAX) = 'ALL',
	@YearOfSigning integer = 0,
	@Software VARCHAR(MAX) = '',
	@Company VARCHAR(MAX) = '',
	@Organization VARCHAR(MAX) = '',
	@Manager VARCHAR(MAX) = '',
	@TagList VARCHAR(MAX) = '',
	@AffiliationList VARCHAR(MAX) = ''
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
  SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  CREATE TABLE #stateTable ([stateID] VARCHAR(2))
  INSERT INTO #stateTable ([stateID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('State', @stateID) 

  CREATE TABLE #agentGrpActivities (
    [activityID] [int] ,
	[agentID] [varchar](50) ,
	[name] [varchar](200),
	[stateID] [varchar](50) ,
	[year] [int] ,
    [type] [varchar](50) ,
	[stat] [varchar](50) ,
	[category] [varchar](50) ,
    [corporationID] [varchar](200) ,
	[month1] [decimal](18, 2) ,
	[month2] [decimal](18, 2) ,
	[month3] [decimal](18, 2) ,
	[month4] [decimal](18, 2) ,
	[month5] [decimal](18, 2) ,
	[month6] [decimal](18, 2) ,
	[month7] [decimal](18, 2) ,
	[month8] [decimal](18, 2) ,
	[month9] [decimal](18, 2) ,
	[month10] [decimal](18, 2) ,
	[month11] [decimal](18, 2) ,
	[month12] [decimal](18, 2) 
)

CREATE TABLE #agentGrpActivitiesFiltered (
    [activityID] [int] ,
	[agentID] [varchar](50) ,
	[name] [varchar](200),
	[stateID] [varchar](50) ,
	[year] [int] ,
    [type] [varchar](50) ,
	[stat] [varchar](50) ,
	[category] [varchar](50) ,
    [corporationID] [varchar](200) ,
	[month1] [decimal](18, 2) ,
	[month2] [decimal](18, 2) ,
	[month3] [decimal](18, 2) ,
	[month4] [decimal](18, 2) ,
	[month5] [decimal](18, 2) ,
	[month6] [decimal](18, 2) ,
	[month7] [decimal](18, 2) ,
	[month8] [decimal](18, 2) ,
	[month9] [decimal](18, 2) ,
	[month10] [decimal](18, 2) ,
	[month11] [decimal](18, 2) ,
	[month12] [decimal](18, 2) 
)

  CREATE TABLE #agentsGroup
(
   [agentID] VARCHAR(20),
   [corporationID] VARCHAR(MAX),  
   [stateID] VARCHAR(2),
   [stateName] VARCHAR(20), 
   [stat] VARCHAR(1),
   [name] VARCHAR(MAX),
   [contractDate] datetime,
   [swVendor] VARCHAR(100),
   [manager] VARCHAR(MAX),
   [orgID] VARCHAR(50),
   [orgRoleID] integer, 
   [orgName] VARCHAR(Max)
)

INSERT INTO #agentsGroup ([agentID] , [corporationID]  , [stateID] , [stateName] , [stat], [name], [contractDate] , [swVendor], [manager], [orgID] , [orgRoleID] , [orgName]  )
Exec SpGetConsolidatedAgents @StateID ,
	@YearOfSigning ,
	@Software,
	@Company  ,
	@Organization ,
    @Manager  ,
	@TagList ,
	@AffiliationList  ,
	@UID 

--Get activities of each agent
  INSERT INTO #agentGrpActivities ([activityID],[agentID],[name],[stateID],[year],[type],[stat],[category],[corporationID],[month1],[month2],[month3],[month4],[month5],[month6],[month7],[month8],[month9],[month10],[month11],[month12])
    EXEC [dbo].[spReportAgentActivities] @AgentID = 'ALL', @StateID = 'ALL', @category = 'ALL', @Year = 0, @UID = @UID
  
  INSERT INTO #agentGrpActivitiesFiltered  select * from  #agentGrpActivities ag  where 
         (ag.[agentID]  IN (SELECT [agentID] FROM #agentsGroup)) 
  AND     ag.[stateID] IN (SELECT [stateID] FROM #stateTable) 
  AND     ag.[stateID] = (SELECT [stateID] FROM #agentsGroup where #agentsGroup.[agentID] = ag.[agentID])    
  AND     ag.[category] = CASE WHEN @category = 'ALL' THEN ag.[category] ELSE @category END
  AND     ag.[year] = CASE WHEN @year = 0 THEN ag.[year] ELSE @year END 
 
  SELECT * FROM #agentGrpActivitiesFiltered
  DROP TABLE #agentGrpActivities
  DROP TABLE #agentGrpActivitiesFiltered
  DROP TABLE #agentsGroup
  DROP TABLE #stateTable
END
