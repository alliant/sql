-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Rahul Sharma>
-- Create date: <02-27-2019>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE spGetSysIndexIds
  -- Add the parameters for the stored procedure here
  @pcObjType VARCHAR(50) = 'ALL'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
  SELECT DISTINCT(s.[objID]) 
    FROM [dbo].[sysindex] s WHERE s.[objType] = (CASE WHEN @pcObjType = 'ALL' THEN s.[objType] ELSE @pcObjType END)
	
END
GO
