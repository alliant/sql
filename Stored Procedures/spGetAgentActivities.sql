USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetAgentActiviies]    Script Date: 2/14/2018 4:16:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetAgentActivities]
	-- Add the parameters for the stored procedure here
  @activityID INT = 0,
  @name VARCHAR(200) = '',
  @agentID VARCHAR(MAX) = 'ALL',
  @decide INTEGER = 0, /* 0=ALL,1=Agents,2=Targets */
  @UID varchar(100)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  SET @agentID = [dbo].[StandardizeAgentID](@agentID)
 
  IF (@decide = 1)
    SELECT  [activityID],
            aa.[agentID],
            a.[name],
            a.[stateID],
            aa.[year],
            aa.[type],
            aa.[stat],
            aa.[category],
            CASE WHEN a.[corporationID] = '' THEN a.[name] ELSE a.[corporationID] END AS [org],
            aa.[month1],
            aa.[month2],
            aa.[month3],
            aa.[month4],
            aa.[month5],
            aa.[month6],
            aa.[month7],
            aa.[month8],
            aa.[month9],
            aa.[month10],
            aa.[month11],
            aa.[month12]
    FROM    [dbo].[agentactivity] aa LEFT OUTER JOIN
            [dbo].[agent] a
    ON      aa.[agentID] = a.[agentID]
    WHERE   [activityID] = CASE WHEN @activityID = 0 THEN [activityID] ELSE @activityID END
    AND     aa.[agentID] = (CASE  WHEN @agentID != 'ALL' THEN @agentID ELSE aa.[agentID] END) 
    AND     [dbo].[CanAccessAgent](@UID, aa.[agentID]) = 1 
	  AND     aa.[agentID] != ''
    AND     aa.[category] IN (SELECT [objID] FROM [dbo].[sysprop] WHERE [appCode] = 'AMD' AND [objAction] = 'Activity' AND [objProperty] = 'Category')
    ORDER BY aa.[agentID],aa.[year],aa.[category],aa.[type]
  ELSE IF (@decide = 2)
    SELECT  [activityID],
            [agentID],
            [name],
            [stateID],
            [year],
            [type],
            [stat],
            [category],
            [name] AS [org],
            [month1],
            [month2],
            [month3],
            [month4],
            [month5],
            [month6],
            [month7],
            [month8],
            [month9],
            [month10],
            [month11],
            [month12]
    FROM    [dbo].[agentactivity]
    WHERE   [activityID] = CASE WHEN @activityID = 0 THEN [activityID] ELSE @activityID END
    AND     [name] LIKE '%' + @name + '%'
    AND     [agentID] = ''
    AND     [category] IN (SELECT [objID] FROM [dbo].[sysprop] WHERE [appCode] = 'AMD' AND [objAction] = 'Activity' AND [objProperty] = 'Category')
    ORDER BY [name],[year],[category],[type]
  ELSE
    SELECT  [activityID],
            aa.[agentID],
            CASE WHEN aa.[agentID] = '' THEN aa.[name] ELSE a.[name] END AS [name],
            CASE WHEN aa.[agentID] = '' THEN aa.[stateID] ELSE a.[stateID] END AS [stateID],
            aa.[year],
            aa.[type],
            aa.[stat],
            aa.[category],
            CASE WHEN aa.[agentID] = '' THEN aa.[name] ELSE CASE WHEN a.[corporationID] = '' THEN a.[name] ELSE a.[corporationID] END END AS [org],
            aa.[month1],
            aa.[month2],
            aa.[month3],
            aa.[month4],
            aa.[month5],
            aa.[month6],
            aa.[month7],
            aa.[month8],
            aa.[month9],
            aa.[month10],
            aa.[month11],
            aa.[month12]
    FROM    [dbo].[agentactivity] aa LEFT OUTER JOIN
            [dbo].[agent] a
    ON      aa.[agentID] = a.[agentID]
    WHERE   [activityID] = CASE WHEN @activityID = 0 THEN [activityID] ELSE @activityID END
    AND     aa.[category] IN (SELECT [objID] FROM [dbo].[sysprop] WHERE [appCode] = 'AMD' AND [objAction] = 'Activity' AND [objProperty] = 'Category')
    AND    (aa.[agentID] = '' OR [dbo].[CanAccessAgent](@UID, aa.[agentID]) = 1)
    ORDER BY aa.[agentID],aa.[year],aa.[category],aa.[type]

END
