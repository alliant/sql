
USE [COMPASS]
GO
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE  [dbo].[spGetMiscInvoiceStatement]
	-- Add the parameters for the stored procedure here
	@piInvoiceID INTEGER = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

CREATE TABLE #miscinvstatement (artranID          INTEGER,
								invoiceID         VARCHAR(50),
								reference         VARCHAR(50),
               	                fileNumber        VARCHAR(50),
								tranAmt           [decimal](18, 2) NULL,
							    SourceType        VARCHAR(50),
								SourceNumber      VARCHAR(50),
    							tranDate          datetime NULL,	
                                duedate           datetime NULL,								
                                agentid           VARCHAR(50),
								notes             VARCHAR(MAX) 
     	 					   )
							 
INSERT INTO #miscinvstatement ( [artranID], [invoiceID], [reference], [fileNumber], [tranAmt], [SourceType], [SourceNumber],[tranDate], [duedate], [agentid])    
   
select a.artranID, a.tranID, a.reference, a.filenumber , a.tranAmt , am.SourceType ,am.SourceID , a.tranDate , a.duedate ,a.entityID
	  from artran a left outer join arMisc am on a.tranid = am.armiscID 
	  where a.tranID = @piInvoiceID   AND
	        a.type   = 'I'            AND
			a.seq    = 0
	
    SELECT * FROM #miscinvstatement
END
GO
