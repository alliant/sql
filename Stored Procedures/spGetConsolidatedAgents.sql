USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetConsolidatedAgents]    Script Date: 22-09-2020 17:09:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sachin Chaturvedi
-- Create date: 22-09-2020
-- Description:	Gets a set of agents qualifying given criteria
-- Modification: 
-- =============================================
CREATE PROCEDURE [dbo].[spGetConsolidatedAgents]
	-- Add the parameters for the stored procedure here
	@StateID VARCHAR(MAX) = '',
	@YearOfSigning integer = 0,
	@Software VARCHAR(MAX) = '',
	@Company VARCHAR(MAX) = '',
	@Organization VARCHAR(MAX) = '',
	@Manager VARCHAR(MAX) = '',
	@TagList VARCHAR(MAX) = '',
	@AffiliationList VARCHAR(MAX) = '',
	@UID VARCHAR(100) = ''
	  
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;
     
  SELECT  a.[agentID] ,
          a.[corporationID], 
          a.[stateID],
          (SELECT [description] FROM [dbo].[state] WHERE [stateID] = a.[stateID]) AS [stateName],
          a.[stat],
          a.[name],
          a.[contractDate],
          a.[swVendor],
         am.[uid] AS [manager],
         orgrole.[orgID],
		 orgrole.[orgRoleID], 
		 org.[Name] AS orgName
	FROM    [dbo].[agent] a LEFT OUTER JOIN
          [dbo].[agentmanager] am
  ON      a.[agentID] = am.[agentID]
  AND     am.[isPrimary] = 1 
  AND     am.[stat] = 'A' LEFT OUTER JOIN
          (
          SELECT  [orgRoleID],
                  [orgID],
                  [source],
                  [sourceID],
                  ROW_NUMBER() OVER(PARTITION BY [sourceID] ORDER BY [sourceID] DESC) AS [rowID]
          FROM    [dbo].[orgrole]
          ) orgrole
  ON      a.[agentID] = orgrole.[sourceID]
  AND     orgrole.[source] = 'Agent'
  AND     orgrole.[rowID] = 1 LEFT OUTER JOIN
          [Organization] org
  ON      org.orgID = orgrole.orgID	
  
   WHERE   a.[stateID]          = CASE WHEN (@StateID != '' and @StateID != 'ALL') THEN 
					               @StateID 
					               ELSE
					               a.[stateID]
								  END 
																		  AND
        a.[corporationID]       = CASE WHEN @Company != '' THEN 
					               @Company 
					               ELSE
					               a.[corporationID]
							      END 
																		  AND
		a.[swVendor]            = CASE WHEN  @Software != '' THEN 
					               @Software 
					               ELSE
					               a.[swVendor]
								  END 
																		  AND

        ((@Manager != ''  and  am.uid is not null) or @Manager = '')      AND    
		 
        ( a.[manager]           = (CASE WHEN  @Manager != '' THEN 
					                @Manager 
					                ELSE
					                a.[manager]
								   END)           
								   OR 
		EXISTS(SELECT am2.[managerID] from [dbo].[agentmanager] am2
         WHERE        am2.[agentID] = a.[agentID]  AND     
                      am2.[stat] = 'A'             AND
					  am2.uid = @manager))    								
																		AND
        ( (a.[contractDate] IS NULL AND @YearOfSigning =0)  OR 
		year(a.[contractDate]) = CASE WHEN  @YearOfSigning  != 0 THEN 
					              @YearOfSigning 
					              ELSE
					              year(a.[contractDate])
								 END)   
																		AND
		
		 
       ((orgrole.[orgID] IS NULL AND @Organization = '')    OR 
		orgrole.[orgID]         = CASE WHEN  @Organization != '' THEN 
					               @Organization 
					               ELSE
					               orgrole.[orgID] 
								  END )								    AND 
						
	    ( @TagList = ''			   OR			
		 EXISTS(SELECT  t.[tagID] 
				FROM [dbo].[tag] t
				WHERE t.[name]   = @taglist    AND
					  t.[entity] = 'A'         AND
					  t.[entityID] = a.[agentID]) ) 
					   
																		AND
		(@AffiliationList = ''     OR 												 
		 EXISTS(SELECT  af.[affiliationID] 
				FROM [dbo].[affiliation] af
				WHERE af.[partnerBType] = 'P'             AND
				      af.[partnerB] IN (@AffiliationList) AND
					  af.[partnerA] IN(SELECT ogr.[orgID]        FROM 
										     [dbo].[orgrole] ogr WHERE
											 ogr.[stateID]  = CASE WHEN (@StateID != '' and @StateID != 'ALL') THEN 
																@StateID 
																ELSE
																a.[stateID]
															  END        AND
												ogr.[source]   = 'Agent' AND
												ogr.[sourceID] = a.[agentID]
										)		
			  ) 
		)																 AND 
  (dbo.CanAccessAgent(@UID , a.[agentID]) = 1) 
  ORDER BY a.[agentID]
END
