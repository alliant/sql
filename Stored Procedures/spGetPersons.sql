
/****** Object:  StoredProcedure [dbo].[spGetPersons]    Script Date: 11/7/2022 1:09:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetPersons]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    SET ANSI_WARNINGS OFF;
    
    CREATE TABLE #personTable
    (
      [personID]           VARCHAR(50),
      [NPN]                VARCHAR(50),
      [altaUID]            VARCHAR(200),
      [title_]             VARCHAR(50),
      [firstName]          VARCHAR(50),
      [middleName]         VARCHAR(50),
      [lastName]           VARCHAR(50),
      [dispName]           VARCHAR(50),
      [address1]           VARCHAR(80),
      [address2]           VARCHAR(80),
      [city]               VARCHAR(50),
      [state]              VARCHAR(20),
      [county]             VARCHAR(50),
      [zip]                VARCHAR(20),
      [contactEmail]       VARCHAR(100),
      [contactPhone]       VARCHAR(40),
      [contactMobile]      VARCHAR(40),
      [active]             BIT,
      [notes]              VARCHAR(8000), 
      [internal]           BIT,
      [doNotCall]          BIT,
      [linkedToAgent]      BIT,
      [linkedToCompliance] BIT
    )
    
    INSERT INTO #personTable ([personID],[NPN],[altaUID],[title_],[firstName],[middleName],[lastName],[dispName],[address1],[address2],[city],[state],[county],[zip],[contactEmail],[contactPhone],[contactMobile],[active],[notes],[internal],[doNotCall],[linkedToAgent],[linkedToCompliance])
    SELECT  p.[personID], 
            p.[NPN],      
            p.[altaUID],  
            p.[title] as [title_],   
            p.[firstName],
            p.[middleName],
            p.[lastName], 
            p.[dispName], 
            p.[address1], 
            p.[address2], 
            p.[city],     
            p.[state],    
            p.[county],   
            p.[zip],      
            ISNULL((CASE WHEN EXISTS(SELECT 1 FROM [dbo].[personcontact] pc WHERE pc.personid = p.personid AND pc.[contactType] = 'E' AND pc.[contactSubType] = 'Work' AND pc.[expirationDate] IS NULL)
					THEN (SELECT TOP 1 pc.[contactID] FROM [dbo].[personcontact] pc WHERE pc.personid = p.personid AND pc.[contactType] = 'E' AND pc.[contactSubType] = 'Work' AND pc.[expirationDate] IS NULL)
					ELSE (Select TOP 1 pc.[contactID] FROM [dbo].[personcontact] pc WHERE pc.personid = p.personid AND pc.[contactType] = 'E' AND pc.[expirationDate] IS NULL) END),'') AS [contactEmail] , 
            ISNULL((CASE WHEN EXISTS(SELECT 1 FROM [dbo].[personcontact] pc WHERE pc.personid = p.personid AND pc.[contactType] = 'P' AND pc.[contactSubType] = 'Work' AND pc.[expirationDate] IS NULL)
					THEN (SELECT TOP 1 pc.[contactID] FROM [dbo].[personcontact] pc WHERE pc.personid = p.personid AND pc.[contactType] = 'P' AND pc.[contactSubType] = 'Work' AND pc.[expirationDate] IS NULL)
					ELSE (Select TOP 1 pc.[contactID] FROM [dbo].[personcontact] pc WHERE pc.personid = p.personid AND pc.[contactType] = 'P' AND pc.[contactSubType] <> 'Mobile' AND pc.[expirationDate] IS NULL) END), '') AS [contactPhone],				  
            (CASE WHEN EXISTS(SELECT 1 FROM [dbo].[personcontact] pc WHERE pc.personid = p.personid AND pc.[contactType] = 'P' AND pc.[contactSubType] = 'Mobile' AND pc.[expirationDate] IS NULL)
			 THEN (SELECT TOP 1 pc.[contactID] FROM [dbo].[personcontact] pc WHERE pc.personid = p.personid AND pc.[contactType] = 'P' AND pc.[contactSubType] = 'Mobile' AND pc.[expirationDate] IS NULL)
			 ELSE '' END) AS [contactMobile],   
            p.[active],   
            p.[notes],    
            p.[internal], 
            p.[doNotCall],
    		(CASE WHEN EXISTS(SELECT 1 FROM [dbo].[personagent] pa  WHERE pa.[personid]  = p.[personid] AND  
                                         ((pa.[effectivedate] <= GETDATE() OR pa.[effectivedate] IS NULL) AND 
                                         (pa.[expirationdate] > GETDATE() OR pa.[expirationdate] IS NULL)))							
    
    			  THEN 1  ELSE 0 END) AS [linkedToAgent], 
    	  
    	    (CASE WHEN EXISTS(SELECT 1 FROM [dbo].[affiliation] a WHERE a.[partnerB] = p.[personid] AND a.[partnerBType] = 'P') OR
                       EXISTS(SELECT 1 FROM [dbo].[personrole] pr WHERE pr.[personid] = p.[personid] AND pr.[active] = 1) 
    			  THEN 1 ELSE 0 END) AS [linkedToCompliance]
    
    FROM    [dbo].[person] p
    
    SELECT * FROM #personTable
    
    DROP TABLE #personTable	
	
END
