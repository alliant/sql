USE [COMPASS]
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportAgentLTD]
	-- Add the parameters for the stored procedure here
	@stateID VARCHAR(MAX) = 'ALL',
    @manager VARCHAR(MAX) = 'ALL',
    @UID varchar(100)

AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;
  
  DECLARE @startDate DATETIME = '2005-01-01',
          @endDate DATETIME = DATEADD(d,-1,DATEADD(m, DATEDIFF(m,0,GETDATE()),0)),
		  @agentID varchar(MAX) = 'ALL'

  SET @agentID = [dbo].[StandardizeAgentID](@agentID);

  CREATE TABLE #stateTable ([stateID] VARCHAR(2))
  INSERT INTO #stateTable ([stateID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('State', @stateID)

  CREATE TABLE #managerTable ([manager] VARCHAR(50))
  INSERT INTO #managerTable ([manager])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('AgentManager', @manager)

  CREATE TABLE #ratioTable
  (
    [agentID] VARCHAR(20),
    [laePaidDelta] DECIMAL(18,2),
    [lossPaidDelta]  DECIMAL(18,2),
    [laeReserveDelta]  DECIMAL(18,2),
    [lossReserveDelta]  DECIMAL(18,2),
    [recoveriesDelta] DECIMAL(18,2),
    [pendingRecoveries] DECIMAL(18,2),
    [netPremium] DECIMAL(18,2)
  )
  
  -- Insert statements for procedure here
  INSERT INTO #ratioTable ([agentID],[laePaidDelta],[lossPaidDelta],[laeReserveDelta],[lossReserveDelta],[recoveriesDelta],[pendingRecoveries],[netPremium])
  EXEC [dbo].[spReportClaimsRatio] @startDate = @startDate, @endDate = @endDate

  SELECT  a.[agentID],
          a.[corporationID],
          a.[stateID],
          a.[name],
          a.[stat],
          a.[activeDate] AS [agentDate],
          a.[uid] AS [manager],
          ISNULL(c.[count],0) AS [numClaims],
          ISNULL(b.[numForms],0) AS [numForms],
          ISNULL(b.[numPolicies],0) AS [numPolicies],
          ISNULL(b.[netPremium],0) AS [netPremium],
          ISNULL(r.[laePaidDelta] + r.[lossPaidDelta] - r.[recoveriesDelta],0) AS [costsIncurred],
          ISNULL(b.[netPremium],0) - ISNULL(r.[laePaidDelta] + r.[lossPaidDelta] - r.[recoveriesDelta],0) AS [agentLTD]
  FROM    (
          SELECT  a.[agentID],
                  CASE WHEN a.[corporationID] = '' THEN a.[agentID] ELSE a.[corporationID] END AS [corporationID],
                  a.[stateID],
                  a.[name],
                  a.[stat],
                  a.[activeDate],
                  ISNULL(am.[uid],'') AS [uid],
                  a.[contractDate],
                  a.[closedDate]
          FROM    [dbo].[agent] a LEFT OUTER JOIN
                  [dbo].[agentmanager] am
          ON      a.[agentID] = am.[agentID]
          AND     am.[stat] = 'A'
          AND     am.[isPrimary] = 1 INNER JOIN
                  #stateTable st
          ON      a.[stateID] = st.[stateID] LEFT OUTER JOIN
                  #managerTable mt
          ON      am.[uid] = mt.[manager]
		  WHERE (a.[agentID] = (CASE  WHEN @agentID != 'ALL' THEN @agentID ELSE a.[agentID] END)) 
          AND     (dbo.CanAccessAgent(@UID ,a.[agentID]) = 1)
          ) a LEFT OUTER JOIN
          (
          SELECT  b.[agentID],
                  SUM(CASE WHEN bf.[formType] = 'E' THEN 1 ELSE 0 END) AS [numForms],
                  SUM(CASE WHEN bf.[formType] = 'P' THEN 1 ELSE 0 END) AS [numPolicies],
                  SUM(bf.[netDelta]) AS [netPremium]
          FROM    [dbo].[batch] b INNER JOIN
                  [dbo].[batchform] bf
          ON      b.[batchID] = bf.[batchID]
          GROUP BY b.[agentID]
          ) b
  ON      b.[agentID] = a.[agentID] LEFT OUTER JOIN
          (
          SELECT  a.[agentID],
                  COUNT(c.[claimID]) AS [count]
          FROM    [dbo].[agent] a INNER JOIN
                  [dbo].[claim] c
          ON      a.[agentID] = c.[agentID]
          GROUP BY a.[agentID]
          ) c
  ON      a.[agentID] = c.[agentID] LEFT OUTER JOIN
          #ratioTable r
  ON      a.[agentID] = r.[agentID]
  ORDER BY a.[agentID]


  DROP TABLE #ratioTable
  DROP TABLE #managerTable
  DROP TABLE #stateTable
END
GO
