USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetOffices]    Script Date: 2/12/2020 3:04:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetOffices]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  [officeID],
          [name] AS [officeName],
          [agentID],
          (SELECT [name] FROM [dbo].[agent] WHERE [agentID] = o.[agentID]) AS [agentName],
          [stat],
          (SELECT [stateID] FROM [dbo].[agent] WHERE [agentID] = o.[agentID]) AS [stateID],
          (SELECT [description] FROM [dbo].[state] WHERE [stateID] = (SELECT [stateID] FROM [dbo].[agent] WHERE [agentID] = o.[agentID])) AS [stateDesc],
          [addr1],
          [addr2],
          [addr1] + CASE WHEN [addr2] <> '' THEN ' ' + [addr2] ELSE '' END AS [fullAddr],
          [city],
          [state],
          [zipcode],
          [phone],
          [email],
          [website],
          [notes]
  FROM    [dbo].[office] o
END
