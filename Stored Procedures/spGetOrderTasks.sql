USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetOrderTasks]    Script Date: 8/22/2019 9:37:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetOrderTasks]
	-- Add the parameters for the stored procedure here
	@uid VARCHAR(100) = 'ALL',
  @orderID VARCHAR(100) = 'ALL'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  IF @uid = '' OR @uid IS NULL
    SET @uid = 'ALL'

  IF @orderID = '' OR @orderID IS NULL
    SET @orderID = 'ALL'

  SELECT  [orderID],
          [taskID],
          [seq],
          [stat],
          [uid],
          [notes],
          [completeDate],
          [completedBy],
          (SELECT [agentID] FROM [dbo].[order] WHERE [orderID] = t.[orderID]) AS [agentID]
  FROM    [dbo].[ordertask] t
  WHERE   [uid] = CASE WHEN @uid = 'ALL' THEN [uid] ELSE @uid END
  AND     [orderID] = CASE WHEN @orderID = 'ALL' THEN [orderID] ELSE @orderID END
END
