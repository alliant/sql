GO
/****** Object:  StoredProcedure [dbo].[spSearchPersonBeginName]    Script Date: 7/5/2024 1:17:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spSearchContacts]
	-- Add the parameters for the stored procedure here
  @type VARCHAR(100) = '',
  @search VARCHAR(100) = '',
  @state VARCHAR(50) = ''
AS
BEGIN
  CREATE TABLE #contacts ([personContactID] INT)

  IF @type = 'First'
    INSERT INTO #contacts
    SELECT  pc.[personContactID]
    FROM    [personcontact] pc INNER JOIN
            [person] p
    ON      pc.[personID] = p.[personID]
    WHERE   p.[dispName] LIKE @search + '%'
    AND     p.[state] = CASE WHEN @state = 'ALL' THEN p.[state] ELSE @state END
  ELSE IF @type = 'Last'
    INSERT INTO #contacts
    SELECT  pc.[personContactID]
    FROM    [personcontact] pc INNER JOIN
            [person] p
    ON      pc.[personID] = p.[personID]
    WHERE   REVERSE(LEFT(REVERSE(p.[dispName]), CHARINDEX(' ', REVERSE(p.[dispName])) -1)) LIKE @search + '%'
    AND     CHARINDEX(' ', [dispName]) > 0
    AND     p.[state] = CASE WHEN @state = 'ALL' THEN p.[state] ELSE @state END
  ELSE IF @search <> ''
    INSERT INTO #contacts
    SELECT  pc.[personContactID]
    FROM    [sysindex] s INNER JOIN
            [person] p
    ON      s.[objID] = p.[personID] INNER JOIN
            [personcontact] pc
    ON      p.[personID] = pc.[personID]
    WHERE   s.[objType] = 'Person'
    AND     s.[objAttribute] = ''
    AND     s.[objKey] IN (SELECT * FROM STRING_SPLIT(@search, ' '))
    AND     p.[state] = CASE WHEN @state = 'ALL' THEN p.[state] ELSE @state END
  ELSE
    INSERT INTO #contacts
    SELECT  pc.[personContactID]
    FROM    [personcontact] pc INNER JOIN
            [person] p
    ON      pc.[personID] = p.[personID]

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
  SELECT  pc.[personContactID] AS [personContactID],
          p.[dispName] AS [contactName],
          CASE WHEN COALESCE(p.[doNotCall], 0) = 0 THEN 'no' ELSE 'yes' END AS [doNotCall],
          pc.[contactID] AS [email],
          COALESCE(STRING_AGG(pa.[agentID], ', '), '') AS [agentID],
          COALESCE(STRING_AGG(a.[name], ', '), '') AS [agentName]
  FROM    [dbo].[personcontact] pc INNER JOIN
          [dbo].[person] p
  ON      pc.[personID] = p.[personID] LEFT OUTER JOIN
          [dbo].[personagent] pa
  ON      p.[personID] = pa.[personID] LEFT OUTER JOIN
          [dbo].[agent] a
  ON      pa.[agentID] = a.[agentID]
  WHERE   pc.[contactType] = 'E'
  AND     p.[active] = 1
  AND     pc.[personContactID] IN (SELECT [personContactID] FROM #contacts)
  GROUP BY pc.[personContactID],p.[dispName],p.[doNotCall],pc.[contactID]
  ORDER BY p.[dispName]

	SET NOCOUNT ON;
END
