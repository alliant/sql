USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spCPLValueDiscrepancyForRepFiles]    Script Date: 8/9/2019 10:54:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCPLValueDiscrepancyForRepFiles]
	-- Add the parameters for the stored procedure here  
  @agentID VARCHAR(MAX) = '',
  @fileNumber VARCHAR(MAX) = '',
  @StartDate DATE = NULL,
  @EndDate DATE = NULL  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	
  -- Insert statements for procedure here
  if (@agentID <> '')
  BEGIN
    SELECT bfs.fileNumber,
	       bfs.formID,	       
		  (bfs.revReported / bfs.noOfCPLReported) AS rateReportedPerCPL,
		  sf.minGross AS rateExpectedPerCPL
	FROM 
		
  	  (SELECT stateID, fileNumber, formID, noOfCPLReported, revReported FROM

		(SELECT fileNumber, fileNumberID, formID, sum(formCount) AS noOfCPLReported, SUM(netPremium) AS revReported FROM batchForm WHERE batchID IN
		 (SELECT batchID FROM batch WHERE batch.agentID = @agentID
		 							  AND batch.createDate >= @Startdate)
		  AND formType = 'C' 
		  GROUP BY fileNumber, fileNumberID, formID) AS bf

		LEFT JOIN
		 
		(SELECT stateID FROM Agent WHERE Agent.agentID = @agentID) AS s 
		ON 1 = 1) AS bfs    

      LEFT JOIN		 				  

	  (SELECT stateID, formID, minGross FROM stateform where formType = 'C') AS sf		 
	  ON bfs.stateID = sf.stateID AND bfs.formID = sf.formID
	  WHERE (bfs.revReported / bfs.noOfCPLReported) <> sf.minGross		
  END 
END