USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spAgentReviewScore]    Script Date: 4/3/2018 1:14:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spAgentReviewRemittance]
	-- Add the parameters for the stored procedure here
  @agentID VARCHAR(MAX) = 'ALL',
  @UID varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SET @agentID = [dbo].[StandardizeAgentID](@agentID)
  
  -- Insert statements for procedure here
  SELECT  b.[agentID],
          b.[periodYear],
          ROW_NUMBER() OVER(PARTITION BY b.[agentID] ORDER BY b.[periodYear] DESC) AS [seq],
          SUM(bf.[netDelta]) AS [netPremium],
          SUM(CASE WHEN bf.[formType] = 'P' OR bf.[formType] = 'E' THEN bf.[grossDelta] ELSE 0 END) AS [grossPremium],
          SUM(CASE WHEN bf.[formType] = 'C' THEN bf.[grossDelta] ELSE 0 END) AS [cplPremium]
  FROM    [dbo].[batch] b INNER JOIN
          [dbo].[batchform] bf
  ON      b.[batchID] = bf.[batchID]
  WHERE   (b.[agentID] = (CASE  WHEN @agentID != 'ALL' THEN @agentID ELSE b.[agentID] END)) 
  AND     (dbo.CanAccessAgent(@UID ,b.[agentID]) = 1)   

  GROUP BY b.[agentID], b.[periodYear]

END
