-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Rahul Sharma>
-- Create date: <02-19-2018>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE spGetAgentGrpQualifications
	-- Add the parameters for the stored procedure here
    @UID VARCHAR(100) = '',
    @stateID     VARCHAR(MAX) = '',
	@YearOfSigning integer = 0,
	@Software VARCHAR(MAX) = '',
	@Company VARCHAR(MAX) = '',
	@Organization VARCHAR(MAX) = '',
	@Manager VARCHAR(MAX) = '', 
	@TagList VARCHAR(MAX) = '',
	@AffiliationList VARCHAR(MAX) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
  DECLARE @pcEntityID VARCHAR(50)    = 'ALL' 

  CREATE TABLE #Qual 
  (
  [qualificationID] INTEGER,
  [stateID]  VARCHAR(3),
  [entity]  VARCHAR(MAX),  
  [entityId]  VARCHAR(MAX),
  [qualification] VARCHAR(MAX),
  [qualificationNumber] VARCHAR(MAX),
  [active] BIT,
  [stat]  VARCHAR(1), 
  [authorizedBy] VARCHAR(100),
  [effectiveDate] DATETIME, 
  [expirationDate] DATETIME, 
  [notes] VARCHAR(MAX),
  [dateCreated] DATETIME, 
  [userCreated] VARCHAR(MAX),
  [dateInactive] DATETIME, 
  [userInactive] VARCHAR(MAX),
  [coverageAmt] DECIMAL,
  [deductibleAmt] DECIMAL,
  [retrodate] DATETIME,
  [provider] VARCHAR(MAX),
  [insuranceBroker] VARCHAR(MAX),
  [aggregateAmt] DECIMAL 
  )
  
  CREATE TABLE #QualFiltered 
  (
  [qualificationID] INTEGER,
  [stateID]  VARCHAR(3),
  [entity]  VARCHAR(MAX),  
  [entityId]  VARCHAR(MAX),
  [qualification] VARCHAR(MAX),
  [qualificationNumber] VARCHAR(MAX),
  [active] BIT,
  [stat]  VARCHAR(1), 
  [authorizedBy] VARCHAR(100),
  [effectiveDate] DATETIME, 
  [expirationDate] DATETIME, 
  [notes] VARCHAR(MAX),
  [dateCreated] DATETIME, 
  [userCreated] VARCHAR(MAX),
  [dateInactive] DATETIME, 
  [userInactive] VARCHAR(MAX),
  [coverageAmt] DECIMAL,
  [deductibleAmt] DECIMAL,
  [retrodate] DATETIME,
  [provider] VARCHAR(MAX),
  [insuranceBroker] VARCHAR(MAX),
  [aggregateAmt] DECIMAL  
  )

  CREATE TABLE #agentsGroup
   (
   [agentID] VARCHAR(20),
   [corporationID] VARCHAR(MAX),  
   [stateID] VARCHAR(2),
   [stateName] VARCHAR(20), 
   [stat] VARCHAR(1),
   [name] VARCHAR(MAX),
   [contractDate] datetime,
   [swVendor] VARCHAR(100),
   [manager] VARCHAR(MAX),
   [orgID] VARCHAR(50),
   [orgRoleID] integer, 
   [orgName] VARCHAR(Max)
   )	

INSERT INTO #agentsGroup ([agentID] , [corporationID]  , [stateID] , [stateName] , [stat], [name], [contractDate] , [swVendor], [manager], [orgID] , [orgRoleID] , [orgName]  )
Exec SpGetConsolidatedAgents @StateID ,
	@YearOfSigning ,
	@Software,
	@Company  ,
	@Organization ,
    @Manager  ,
	@TagList ,
	@AffiliationList  ,
	@UID 

INSERT INTO #Qual ( [qualificationID],
  [stateID],
  [entity], 
  [entityId],
  [qualification] ,
  [qualificationNumber],
  [active] ,
  [stat], 
  [authorizedBy],
  [effectiveDate],
  [expirationDate],
  [notes],
  [dateCreated], 
  [userCreated],
  [dateInactive],
  [userInactive],
  [coverageAmt] ,
  [deductibleAmt],
  [retrodate],
  [provider],
  [insuranceBroker],
  [aggregateAmt])
    EXEC [dbo].[spGetQualifications] @pcStateID = 'ALL', @pcEntity = 'O', @pcEntityID = 'ALL', @piQualificationID = 0

INSERT INTO #QualFiltered  select * from  #Qual ql  WHERE
		 ql.[entityID] IN(SELECT orgID FROM orgRole WHERE orgRole.source = 'Agent' and 
														  orgRole.sourceID IN (SELECT [agentID] FROM #agentsGroup)) AND
		 ql.[entityID] <> '' AND 
        (ql.[StateID]  IN (SELECT distinct[StateID] FROM #agentsGroup))																
														  
 
  SELECT * FROM #QualFiltered 
  DROP TABLE #agentsGroup
  DROP TABLE #Qual
  DROP TABLE #QualFiltered
END
