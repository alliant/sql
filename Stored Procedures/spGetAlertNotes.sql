USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetAlertNotes]    Script Date: 10/9/2017 3:45:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetAlertNotes]
	-- Add the parameters for the stored procedure here
	@alertID INTEGER = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  DECLARE @agentID VARCHAR(20) = '',
          @code VARCHAR(20) = ''

  IF (@alertID = 0)
    RETURN 0

  SELECT  @agentID = [agentID],
          @code = [processCode]
  FROM    [dbo].[alert]
  WHERE   [alertID] = @alertID

  SELECT  @alertID = [alertID]
  FROM    [dbo].[alert]
  WHERE   [agentID] = @agentID
  AND     [processCode] = @code
  AND     [active] = 1

  SELECT  [alertID],
          [seq],
          [dateCreated],
          [createdBy],
          [category],
          [description]
  FROM    [dbo].[alertnote]
  WHERE   [alertID] = @alertID
END
