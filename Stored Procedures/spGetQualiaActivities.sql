USE [COMPASS_DVLP]
GO
/****** Object:  StoredProcedure [dbo].[spGetQualiaActivities]    Script Date: 3/5/2021 11:12:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetQualiaActivities]
	-- Add the parameters for the stored procedure here
	@qualiaID VARCHAR(50) = '',
  @stat VARCHAR(50) = 'ALL'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  IF @stat = 'ALL'
    SET @stat = 'N,P,D,X,C,S'

  CREATE TABLE #status ([stat] VARCHAR(10))
  INSERT INTO #status ([stat])
  SELECT value FROM STRING_SPLIT(@stat, ',')

  -- Insert statements for procedure here
  SELECT  qa.[qualiaID],
          qa.[createDate],
          qa.[seq],
          qa.[description],
          qa.[responseBody]
  FROM    [qualia] q INNER JOIN
          [qualiaactivity] qa
  ON      q.[qualiaID] = qa.[qualiaID]
  WHERE   q.[qualiaID] = CASE WHEN @qualiaID = '' THEN q.[qualiaID] ELSE @qualiaID END
  AND     q.[stat] IN (SELECT [stat] FROM #status)
  AND     qa.[responseBody] IS NOT NULL
END
