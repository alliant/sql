USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetStateForms]    Script Date: 8/6/2018 11:10:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetStateForms]
	-- Add the parameters for the stored procedure here
	@stateID VARCHAR(20) = 'ALL',
  @formID VARCHAR(100) = '',
  @formType VARCHAR(20) = '',
  @insuredType VARCHAR(100) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  -- Insert statements for procedure here
	SELECT  sf.[stateID],
          sf.[formID],
          sf.[active],
          sf.[effdate],
          sf.[filedate],
          sf.[mingross],
          sf.[maxgross],
          sf.[formType],
          sf.[rateCheck],
          sf.[rateMin],
          sf.[rateMax],
          sf.[description],
          sf.[formCode],
          sf.[insuredType],
          sf.[orgName],
          sf.[orgRev],
          sf.[orgRelDate],
          sf.[orgEffDate],
          sf.[revenueType]
  FROM    [dbo].[stateform] sf
  WHERE   sf.[stateID] = CASE WHEN @stateID = 'ALL' THEN sf.[stateID] ELSE @stateID END
  AND     sf.[formID] = CASE WHEN @formID = '' THEN sf.[formID] ELSE @formID END
  AND     sf.[formType] = CASE WHEN @formType = '' THEN sf.[formType] ELSE @formType END
  AND     sf.[insuredType] = CASE WHEN @insuredType = '' THEN sf.[insuredType] ELSE @insuredType END
END
