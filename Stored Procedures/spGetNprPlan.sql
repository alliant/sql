USE [COMPASS_ALFA]
GO
/****** Object:  StoredProcedure [dbo].[spGetNprPlan]    Script Date: 6/23/2022 8:27:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetNprPlan]
	-- Add the parameters for the stored procedure here
	@piAgentID VARCHAR(MAX) = 'ALL',
	@piYear    integer = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	CREATE TABLE #NprPlan (AgentID   VARCHAR(10),
					   Date      DATE, 
					   NprPlan   VARCHAR(50) 
					   )

INSERT INTO #NprPlan (AgentID,Date,NprPlan)
select aa.agentID,
       DATEFROMPARTS(aa.year,01,1),
	   aa.month1
       from  [dbo].[agentactivity] aa
	   where aa.agentID = (CASE  WHEN @piAgentID != 'ALL' THEN @piAgentID ELSE aa.[agentID] END)
	    and aa.stat = 'O' and aa.category = 'N' and aa.type = 'P' 
		and aa.year = (CASE  WHEN @piYear != 0 THEN @piYear ELSE aa.year END) 


INSERT INTO #NprPlan (AgentID,Date,NPRplan)
select ag.agentID,
       DATEFROMPARTS(aa.year,02,1),
	   aa.month2
       from agent ag 
	   LEFT OUTER JOIN
       [dbo].[agentactivity] aa
	   ON      ag.[agentID] = aa.[agentID]
	     where ag.agentID = (CASE  WHEN @piAgentID != 'ALL' THEN @piAgentID ELSE ag.[agentID] END)
	    and aa.stat = 'O' and aa.category = 'N' and aa.type = 'P' 
		and aa.year = (CASE  WHEN @piYear != 0 THEN @piYear ELSE aa.year END) 
		
INSERT INTO #NprPlan (AgentID,Date,NPRplan)
select ag.agentID,
       DATEFROMPARTS(aa.year,03,1),
	   aa.month3
       from agent ag 
	   LEFT OUTER JOIN
       [dbo].[agentactivity] aa
	   ON      ag.[agentID] = aa.[agentID]
	     where ag.agentID = (CASE  WHEN @piAgentID != 'ALL' THEN @piAgentID ELSE ag.[agentID] END)
	    and aa.stat = 'O' and aa.category = 'N' and aa.type = 'P' 
		and aa.year = (CASE  WHEN @piYear != 0 THEN @piYear ELSE aa.year END) 
		
INSERT INTO #NprPlan (AgentID,Date,NPRplan)
select ag.agentID,
       DATEFROMPARTS(aa.year,04,1),
	   aa.month4
       from agent ag 
	   LEFT OUTER JOIN
       [dbo].[agentactivity] aa
	   ON      ag.[agentID] = aa.[agentID]
	     where ag.agentID = (CASE  WHEN @piAgentID != 'ALL' THEN @piAgentID ELSE ag.[agentID] END)
	    and aa.stat = 'O' and aa.category = 'N' and aa.type = 'P' 
		and aa.year = (CASE  WHEN @piYear != 0 THEN @piYear ELSE aa.year END) 
		
INSERT INTO #NprPlan (AgentID,Date,NPRplan)
select ag.agentID,
       DATEFROMPARTS(aa.year,05,1),
	   aa.month5
       from agent ag 
	   LEFT OUTER JOIN
       [dbo].[agentactivity] aa
	   ON      ag.[agentID] = aa.[agentID]
	     where ag.agentID = (CASE  WHEN @piAgentID != 'ALL' THEN @piAgentID ELSE ag.[agentID] END)
	    and aa.stat = 'O' and aa.category = 'N' and aa.type = 'P' 
		and aa.year = (CASE  WHEN @piYear != 0 THEN @piYear ELSE aa.year END) 
		
INSERT INTO #NprPlan (AgentID,Date,NPRplan)
select ag.agentID,
       DATEFROMPARTS(aa.year,06,1),
	   aa.month6
       from agent ag 
	   LEFT OUTER JOIN
       [dbo].[agentactivity] aa
	   ON      ag.[agentID] = aa.[agentID]
	     where ag.agentID = (CASE  WHEN @piAgentID != 'ALL' THEN @piAgentID ELSE ag.[agentID] END)
	    and aa.stat = 'O' and aa.category = 'N' and aa.type = 'P' 
		and aa.year = (CASE  WHEN @piYear != 0 THEN @piYear ELSE aa.year END) 
		
INSERT INTO #NprPlan (AgentID,Date,NPRplan)
select ag.agentID,
       DATEFROMPARTS(aa.year,07,1),
	   aa.month7
       from agent ag 
	   LEFT OUTER JOIN
       [dbo].[agentactivity] aa
	   ON      ag.[agentID] = aa.[agentID]
	     where ag.agentID = (CASE  WHEN @piAgentID != 'ALL' THEN @piAgentID ELSE ag.[agentID] END)
	    and aa.stat = 'O' and aa.category = 'N' and aa.type = 'P' 
		and aa.year = (CASE  WHEN @piYear != 0 THEN @piYear ELSE aa.year END) 
		
INSERT INTO #NprPlan (AgentID,Date,NPRplan)
select ag.agentID,
       DATEFROMPARTS(aa.year,08,1),
	   aa.month8
       from agent ag 
	   LEFT OUTER JOIN
       [dbo].[agentactivity] aa
	   ON      ag.[agentID] = aa.[agentID]
	     where ag.agentID = (CASE  WHEN @piAgentID != 'ALL' THEN @piAgentID ELSE ag.[agentID] END)
	    and aa.stat = 'O' and aa.category = 'N' and aa.type = 'P' 
		and aa.year = (CASE  WHEN @piYear != 0 THEN @piYear ELSE aa.year END) 
		

INSERT INTO #NprPlan (AgentID,Date,NPRplan)
select ag.agentID,
       DATEFROMPARTS(aa.year,09,1),
	   aa.month9
       from agent ag 
	   LEFT OUTER JOIN
       [dbo].[agentactivity] aa
	   ON      ag.[agentID] = aa.[agentID]
	     where ag.agentID = (CASE  WHEN @piAgentID != 'ALL' THEN @piAgentID ELSE ag.[agentID] END)
	    and aa.stat = 'O' and aa.category = 'N' and aa.type = 'P' 
		and aa.year = (CASE  WHEN @piYear != 0 THEN @piYear ELSE aa.year END) 
		

INSERT INTO #NprPlan (AgentID,Date,NPRplan)
select ag.agentID,
       DATEFROMPARTS(aa.year,10,1),
	   aa.month10
       from agent ag 
	   LEFT OUTER JOIN
       [dbo].[agentactivity] aa
	   ON      ag.[agentID] = aa.[agentID]
	     where ag.agentID = (CASE  WHEN @piAgentID != 'ALL' THEN @piAgentID ELSE ag.[agentID] END)
	    and aa.stat = 'O' and aa.category = 'N' and aa.type = 'P' 
		and aa.year = (CASE  WHEN @piYear != 0 THEN @piYear ELSE aa.year END) 
		

INSERT INTO #NprPlan (AgentID,Date,NPRplan)
select ag.agentID,
       DATEFROMPARTS(aa.year,11,1),
	   aa.month11
       from agent ag 
	   LEFT OUTER JOIN
       [dbo].[agentactivity] aa
	   ON      ag.[agentID] = aa.[agentID]
	     where ag.agentID = (CASE  WHEN @piAgentID != 'ALL' THEN @piAgentID ELSE ag.[agentID] END)
	    and aa.stat = 'O' and aa.category = 'N' and aa.type = 'P' 
		and aa.year = (CASE  WHEN @piYear != 0 THEN @piYear ELSE aa.year END) 
		
INSERT INTO #NprPlan (AgentID,Date,NPRplan)
select ag.agentID,
       DATEFROMPARTS(aa.year,12,1),
	   aa.month12
       from agent ag 
	   LEFT OUTER JOIN
       [dbo].[agentactivity] aa
	   ON      ag.[agentID] = aa.[agentID]
	     where ag.agentID = (CASE  WHEN @piAgentID != 'ALL' THEN @piAgentID ELSE ag.[agentID] END)
	    and aa.stat = 'O' and aa.category = 'N' and aa.type = 'P' 
		and aa.year = (CASE  WHEN @piYear != 0 THEN @piYear ELSE aa.year END) 

/*update #NprPlan
set date = DATEFROMPARTS(aa.year,01,1),
    nprPlan = month1 
	from agentactivity aa where aa.agentID = #NprPlan.agentID and aa.stat = 'O' and aa.category = 'N' and aa.type = 'P'
*/

delete from  #NprPlan where agentID  = ''

select * from #NprPlan ORDER BY agentID,Date
DROP TABLE #NprPlan
END
--SELECT DATEFROMPARTS(2021,01,2)