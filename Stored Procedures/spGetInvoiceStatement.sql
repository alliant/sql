
USE [COMPASS]
GO
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE  [dbo].[spGetInvoiceStatement]
	-- Add the parameters for the stored procedure here
	@piBatchID INTEGER = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
CREATE TABLE #invoicestatement (batchid           INTEGER,
                                batchState        Varchar(max),
               	                fileNumber        VARCHAR(500),
				                fileID            VARCHAR(500),
								policyid          INTEGER,
     				            liabilityamount   [decimal](18, 2) NULL,
					            grosspremium      [decimal](18, 2) NULL,
					            retentionpremium  [decimal](18, 2) NULL,
					            netdelta          [decimal](18, 2) NULL,
							    formDesc          VARCHAR(500),
								formType          VARCHAR(50),
								typeSeq           INTEGER,
                                formID            VARCHAR(50),
                                statCode          VARCHAR(50),
    							batchrecivedate   datetime NULL,	
                                duedate           datetime NULL,								
                                agentid            VARCHAR(50),
                                agentAddr1         VARCHAR(500),
	    						agentAddr2         VARCHAR(500),
								agentCity          VARCHAR(500),
								agentState          VARCHAR(500),
								agentZip          VARCHAR(500),
								agentName          VARCHAR(500),
								terms              Varchar(100)
     	 					   )
							 
INSERT INTO #invoicestatement ( [batchid], [batchState], [agentid], [batchrecivedate], [fileNumber], [fileID], [policyid],[liabilityamount], [grosspremium], [retentionpremium],[netdelta], [formType], [formID], [statCode])    
   
select b.batchid, b.stateid, b.agentid  ,b.receivedDate,  bf.filenumber , bf.fileid , bf.policyid ,bf.liabilityamount , bf.grossdelta , bf.retentiondelta ,bf.netdelta , bf.formType , bf.formID , bf.statCode
	  from batch b inner join batchform bf on b.batchid = bf.batchid 
	  where b.batchid = @piBatchID 

 /*
    update #invoicestatement set #invoicestatement.[agentAddr1] =  (select agent.addr1 from agent where agent.agentid = #invoicestatement.agentid),
                                 #invoicestatement.[agentAddr2] =  (select agent.addr2 from agent where agent.agentid = #invoicestatement.agentid),
                                 #invoicestatement.[agentCity]  =  (select agent.city from agent where agent.agentid = #invoicestatement.agentid),
                                 #invoicestatement.[agentState] =  (select agent.state from agent where agent.agentid = #invoicestatement.agentid),
                                 #invoicestatement.[agentZip]   =  (select agent.zip from agent where agent.agentid = #invoicestatement.agentid) ,
                                 #invoicestatement.[agentName]  =  (select agent.name from agent where agent.agentid = #invoicestatement.agentid)
	*/	 
	
    update #invoicestatement set  #invoicestatement.formDesc        =  (select stateform.description from stateform where stateform.stateid = #invoicestatement.batchState and stateform.formID  = #invoicestatement.formID )	
	
    update #invoicestatement set  #invoicestatement.liabilityamount =  (select policy.liabilityAmount from policy where policy.policyID = #invoicestatement.policyid)
	
    update #invoicestatement set  #invoicestatement.typeSeq = 1 where #invoicestatement.formType == 'P'
    
    update #invoicestatement set  #invoicestatement.typeSeq = 2 where #invoicestatement.formType == 'E'
	
    update #invoicestatement set  #invoicestatement.typeSeq  = 3 where #invoicestatement.formType == 'C'
	update #invoicestatement set  #invoicestatement.policyid = 999999999 where #invoicestatement.formType == 'C'
	 
    update #invoicestatement set  #invoicestatement.typeSeq  = 4 where #invoicestatement.formType == 'S'
	update #invoicestatement set  #invoicestatement.policyid = 999999999 where #invoicestatement.formType == 'S'
	
    update #invoicestatement set  #invoicestatement.typeSeq  = 5 where #invoicestatement.formType == 'T'
	update #invoicestatement set  #invoicestatement.policyid = 999999999 where #invoicestatement.formType == 'T'
	
	update #invoicestatement set  #invoicestatement.typeSeq  = 6 where #invoicestatement.formType <> 'P' and #invoicestatement.formType <> 'E' and #invoicestatement.formType <> 'C' and #invoicestatement.formType <> 'S'and #invoicestatement.formType <> 'T' 
	update #invoicestatement set  #invoicestatement.policyid = 999999999 where #invoicestatement.formType <> 'P' and #invoicestatement.formType <> 'E' and #invoicestatement.formType <> 'C' and #invoicestatement.formType <> 'S'and #invoicestatement.formType <> 'T' 
	
    SELECT * FROM #invoicestatement
END
GO
