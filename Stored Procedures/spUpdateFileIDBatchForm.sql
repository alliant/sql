USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateFileIDBatchForm]    Script Date: 3/20/2020 8:16:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spUpdateFileIDBatchForm]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  DECLARE @agentID VARCHAR(20),
          @fileNumber VARCHAR(1000),
          @addr1 VARCHAR(100) = '',
          @addr2 VARCHAR(100) = '',
          @addr3 VARCHAR(100) = '',
          @addr4 VARCHAR(100) = '',
          @city VARCHAR(100) = '',
          @countyID VARCHAR(50) = '',
          @state VARCHAR(2) = '',
          @zip VARCHAR(20) = '',
          @stage VARCHAR(20),
          @transactionType VARCHAR(20) = '',
          @insuredType VARCHAR(20) = '',
          @reportingDate DATETIME = NULL,
          @liability DECIMAL(18,2) = 0,
          @notes VARCHAR(MAX),
          @agentFileID INTEGER

  -- Insert statements for procedure here
	CREATE TABLE #fileID
  (
    [agentID] VARCHAR(20),
    [batchID] INTEGER,
    [seq] INTEGER,
    [stat] VARCHAR(20),
    [stateID] VARCHAR(20),
    [revenueType] VARCHAR(50),
    [invoiceDate] DATETIME,
    [fileNumber] VARCHAR(1000),
    [fileID] VARCHAR(1000)
  )

  INSERT INTO #fileID ([agentID],[batchID],[seq],[stat],[stateID],[revenueType],[invoiceDate],[fileNumber],[fileID])
  SELECT  b.[agentID],
          b.[batchID],
          bf.[seq],
          b.[stat],
          b.[stateID],
          (SELECT [revenueType] FROM [dbo].[stateform] WHERE [stateID] = b.[stateID] AND [formID] = bf.[formID]) AS [revenueType],
          b.[invoiceDate],
          bf.[fileNumber],
          [dbo].[NormalizeFileID] (bf.[fileNumber])
  FROM    [dbo].[batch] b INNER JOIN
          [dbo].[batchform] bf
  ON      b.[batchID] = bf.[batchID]
  WHERE  ([fileID] = '' OR [fileID] IS NULL)
  AND    ([fileNumber] <> '' AND [fileNumber] IS NOT NULL)
  AND     [agentID] IS NOT NULL

  UPDATE  [dbo].[batchform]
  SET     [fileID] = f.[fileID],
          [revenueType] = f.[revenueType]
  FROM    #fileID f INNER JOIN
          [dbo].[batchform] bf
  ON      f.[batchID] = bf.[batchID]
  AND     f.[seq] = bf.[seq]

  --Insert into agentfile and sysnote
  DECLARE cur CURSOR LOCAL FOR
  SELECT  f.[agentID],
          f.[fileNumber],
          CASE WHEN f.[stat] = 'C' THEN 'batchInvoice' ELSE 'batchFormCreate' END AS [stage],
          CASE WHEN bf.[residential] = 1 THEN 'Residential' ELSE 'Commercial' END AS [transactionType],
          bf.[insuredType],
          CASE WHEN f.[stat] = 'C' THEN f.[invoiceDate] ELSE NULL END AS [reportingDate],
          CASE WHEN f.[stat] = 'C' THEN 'DataFix:batchInvoice,OPS,Processed Agent File when Batch: ' + CONVERT(VARCHAR,bf.[batchID]) + ' is completed.' ELSE 'DataFix:batchFormCreate,OPS,Processed Agent File when batchform created with form ID ' + bf.formID + ' for policy ID ' + CONVERT(VARCHAR,bf.policyID) + ' and batch ' + CONVERT(VARCHAR,f.[batchID]) END AS [notes]
  FROM    [dbo].[batchform] bf INNER JOIN
          #fileID f
  ON      bf.[batchID] = f.[batchID]
  AND     bf.[seq] = f.[seq]

  OPEN cur
  FETCH NEXT FROM cur INTO @agentID, @fileNumber, @stage, @transactionType, @insuredType, @reportingDate, @notes
  WHILE @@FETCH_STATUS = 0 
  BEGIN
    SET @agentFileID = 0
    EXEC [dbo].[spInsertAgentFile] @agentID, @fileNumber, @addr1, @addr2, @addr3, @addr4, @city, @countyID, @state, @zip, @stage, @transactionType ,@insuredType, @reportingDate, @liability, @notes, @agentFileID OUTPUT
    EXEC [dbo].[spInsertSysNote] 'AF', @agentFileID, @notes
    FETCH NEXT FROM cur INTO @agentID, @fileNumber, @stage, @transactionType, @insuredType, @reportingDate, @notes
  END
  CLOSE cur
  DEALLOCATE cur

  DROP TABLE #fileID
END
