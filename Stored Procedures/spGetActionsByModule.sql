-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	<Rahul Sharma>
-- Create date: <04-05-2019>
-- Description:	<Get syslog records by module>
-- =============================================
CREATE PROCEDURE [dbo].[spGetActionsByModule] 
    -- Add the parameters for the stored procedure here
    @pcModule    VARCHAR(50) = 'ALL',
    @pdStartDate DATE,
    @pdEndDate   DATE
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from
  -- interfering with SELECT statements.
  SET NOCOUNT ON;

  -- Temporary table definition
  CREATE TABLE #templog (action         VARCHAR(40),			            
                         applicationID  VARCHAR(50))
  

  CREATE TABLE #syslog (action         VARCHAR(40),
			            totalcount     INTEGER,
                        applicationID  VARCHAR(50))
    
  -- Insert statements for procedure here
  IF @pcModule = 'ALL'
  BEGIN
    INSERT INTO #templog([action],[applicationID]) 	     	
	  SELECT syslog.action, syslog.applicationID
      FROM syslog
      WHERE CAST(syslog.createdate AS DATE) >= @pdStartDate
        AND CAST(syslog.createdate AS DATE) <= @pdEndDate
	
    UPDATE #templog 
      SET #templog.applicationID = (CASE WHEN #templog.applicationID NOT IN (SELECT syscode.code FROM syscode) 
	                                                                     OR (#templog.applicationID is NULL) THEN 'Unknown'
									     ELSE #templog.applicationID END)								    
	  
    INSERT INTO #syslog([action],[applicationID],[totalcount]) 	     	
      SELECT #templog.action, #templog.applicationID, COUNT(#templog.action)
      FROM #templog 
	  GROUP BY #templog.applicationID, #templog.action
    
    SELECT #syslog.applicationID, sysaction.action, #syslog.totalcount, sysaction.progexec
      FROM sysaction 
	    LEFT JOIN #syslog ON sysaction.action = #syslog.action

  END
  ELSE IF @pcModule = 'Unknown'
  BEGIN
    -- @pcModule = 'Unknown' means all those syslog records whose applicationID is other than syscode
    INSERT INTO #syslog([action],[totalcount])  
	  SELECT syslog.action, COUNT(syslog.action)
      FROM syslog
	  WHERE CAST(syslog.createdate AS DATE) >= @pdStartDate 
        AND CAST(syslog.createdate AS DATE) <= @pdEndDate
	    AND (syslog.applicationID NOT IN (SELECT syscode.code FROM syscode) 
	     OR (syslog.applicationID is NULL)) 
      GROUP BY syslog.action
    	 
    UPDATE #syslog SET #syslog.applicationID ='Unknown'
    
	SELECT #syslog.applicationID, #syslog.action, #syslog.totalcount, sysaction.progexec
      FROM sysaction 
	    INNER JOIN #syslog ON sysaction.action = #syslog.action
  END
  ELSE 
  BEGIN
    INSERT INTO #syslog([action],[applicationID],[totalcount]) 
	  SELECT syslog.action, syslog.applicationID, COUNT(syslog.action)
      FROM syslog
      WHERE CAST(syslog.createdate AS DATE) >= @pdStartDate
        AND CAST(syslog.createdate AS DATE) <= @pdEndDate 
        AND syslog.applicationID = @pcModule
      GROUP BY syslog.applicationID, syslog.action
	
    SELECT #syslog.applicationID, #syslog.action, #syslog.totalcount, sysaction.progexec
      FROM sysaction 
	    INNER JOIN #syslog ON sysaction.action = #syslog.action	
  END 
END
GO
