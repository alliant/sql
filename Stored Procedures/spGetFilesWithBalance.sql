/****** Object:  StoredProcedure [dbo].[spGetFilesWithBalance]    Script Date: 8/2/2024 10:26:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetFilesWithBalance] 
	-- Add the parameters for the stored procedure here
	@agentID     VARCHAR(MAX)  = 'ALL'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
    CREATE TABLE #fileAR0 (agentfileID	   INTEGER,
	                       agentID         VARCHAR(50),
						   agent           VARCHAR(250),
						   agentName       VARCHAR(200),
						   fileID          VARCHAR(50),
						   unpostedAmount  [decimal](18,2) NULL,
						   amount          [decimal](18, 2) NULL,
						   invoicedAmount  [decimal](18, 2) NULL,
     				       cancelledAmount [decimal](18, 2) NULL,
                           paidAmount      [decimal](18, 2) NULL
     	 		           )
    CREATE TABLE #fileAR1 (agentfileID	   INTEGER,
	                       agentID         VARCHAR(50),
						   agent           VARCHAR(250),
						   agentName       VARCHAR(200),
						   fileID          VARCHAR(50),
						   unpostedAmount  [decimal](18,2) NULL,
						   amount          [decimal](18, 2) NULL,
						   invoicedAmount  [decimal](18, 2) NULL,
     				       cancelledAmount [decimal](18, 2) NULL,
                           paidAmount      [decimal](18, 2) NULL
     	 		           )

	CREATE TABLE #fileARFinal ( agentfileID	   INTEGER,
	                            agentID         VARCHAR(50),
						        agent           VARCHAR(250),
						        agentName       VARCHAR(200),
						        fileID          VARCHAR(50),
						        unpostedAmount  [decimal](18,2) NULL,
						        amount          [decimal](18, 2) NULL,
						        invoicedAmount  [decimal](18, 2) NULL,
     				            cancelledAmount [decimal](18, 2) NULL,
                                paidAmount      [decimal](18, 2) NULL
     	 		              )

	/* fetch the files which have balance fileAR.seq = 0 record*/
	insert into #fileAR0( agentfileID, agentID, agent, agentName, fileID, amount, unpostedAmount, invoicedAmount)
	select fa.agentfileID, 
		   fa.agentID, 
		   ag.name + ' (' + fa.agentID + ')', 
		   ag.name as [agentName], 
		   fa.fileID,
		   fa.amount,
		   (select sum(invoiceAmount) from fileInv where fileInv.agentFileID = fa.agentfileID and fileInv.posted = 0) AS unpostedAmount, 
	       (select sum(invoiceAmount) from fileInv where fileInv.agentFileID = fa.agentfileID) AS [invoicedAmount]
	from filear fa
	  INNER JOIN agent ag ON (ag.agentID = fa.agentID)
	 where fa.agentID = (CASE WHEN @agentID  = 'ALL' THEN fa.agentID  ELSE @agentID  END)
	 and   fa.seq = 0

    /* fetch the files which have non-zero seq fileAr records */
	insert into #fileAR1( agentfileID, agentID, agent, agentName, fileID, unpostedAmount, invoicedAmount)
	select af.agentfileid,
	       af.agentID,
	       (select name from agent where  agent.agentID = af.agentID) + ' (' + af.agentID + ')',
		   (select name from agent where  agent.agentID = af.agentID) as [agentName],
		   af.fileID,
		   (select sum(invoiceAmount) from fileInv where fileInv.agentFileID = af.agentfileID and fileInv.posted = 0) AS [unpostedAmount], 
	       (select sum(invoiceAmount) from fileInv where fileInv.agentFileID = af.agentfileID) AS [invoicedAmount]
	from fileinv fi
	Inner Join agentfile af 
	ON (af.agentfileid = fi.agentfileid)
	Where fi.posted = 0 
	AND af.agentID = (CASE WHEN @agentID  = 'ALL' THEN af.agentID  ELSE @agentID  END)
	AND af.agentfileid not in (select agentFileID from fileAR where filear.seq = 0) 

	/* fetch the files which don't have fileAr record and not persent in #fileAr1 */
	insert into #fileAR1( agentfileID, agentID, agent, agentName, fileID, unpostedAmount, invoicedAmount)
    select af.agentfileid,
	       af.agentID,
	       (select name from agent where  agent.agentID = af.agentID) + ' (' + af.agentID + ')',
		   (select name from agent where  agent.agentID = af.agentID) as [agentName],
		   af.fileID,
		   (select sum(invoiceAmount) from fileInv where fileInv.agentFileID = af.agentfileID and fileInv.posted = 0) AS [unpostedAmount], 
	       (select sum(invoiceAmount) from fileInv where fileInv.agentFileID = af.agentfileID) AS [invoicedAmount]
	from fileinv fi
	Inner Join agentfile af 
	ON (af.agentfileid = fi.agentfileid)
	Where fi.posted = 0 
	AND af.agentID = (CASE WHEN @agentID  = 'ALL' THEN af.agentID  ELSE @agentID  END)
	AND af.agentfileid not in (select agentFileID from fileAR)
	AND af.agentfileid not in (select agentfileID from #fileAR1)

	insert into #fileARFinal
	select * from #fileAR0 where #fileAR0.agentfileID NOT in (select agentfileID from #fileARFinal)

	insert into #fileARFinal
	select * from #fileAR1 where #fileAR1.agentfileID NOT in (select agentfileID from #fileARFinal)
	

	UPDATE #fileARFinal
		SET #fileARFinal.cancelledAmount = (select sum(fa.amount) from filear fa where fa.agentFileID = #fileARFinal.agentfileID
		                                                                        and fa.type = 'W'
																				and fa.seq > 0),

			#fileARFinal.paidAmount = (select sum(fa.amount) from filear fa where fa.agentFileID = #fileARFinal.agentfileID
		                                                                        and fa.type = 'A'
																				and fa.seq > 0) 

	select * from #fileARFinal
END
