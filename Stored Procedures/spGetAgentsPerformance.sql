-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetAgentsPerformance]
	-- Add the parameters for the stored procedure here
	@ipPageNo    integer,
	@ipNoOfRows  integer,
    @agentID     VARCHAR(MAX) = 'ALL',
    @stateID     VARCHAR(200) = 'ALL',
	@UID         VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @iNoCalls  INTEGER     = 0,
	        @iCount    INTEGER     = 0,
			@currMonth INTEGER     = 0
    
	CREATE TABLE #ttAgentPremium (entityId       VARCHAR(100),
							      name           VARCHAR(200),
								  city           VARCHAR(50),
								  state        VARCHAR(2),
								  favorite       BIT,
								  sysfavoriteID  INTEGER,
								  zip            VARCHAR(20),  
							      stat           VARCHAR(1),
								  email          VARCHAR(200),
	                              nprPlanned     DECIMAL(18,2),
								  nprActual      DECIMAL(18,2),
								  nprPercent     DECIMAL(18,2),
								  nprPlannedYTD  DECIMAL(18,2),
								  nprPercentYTD  DECIMAL(18,2),
								  noOfPeople     INTEGER)

    CREATE TABLE #ttAllAgents (rowNum         INTEGER,
	                           entityId       VARCHAR(100),
							   name           VARCHAR(200),
							   city           VARCHAR(50),
							   state        VARCHAR(2),
							   favorite       BIT,
							   sysfavoriteID  INTEGER,
						       zip            VARCHAR(20),  
							   stat           VARCHAR(1),
							   email          VARCHAR(200),
	                           nprPlanned     DECIMAL(18,2),
							   nprActual      DECIMAL(18,2),
							   nprPercent     DECIMAL(18,2),
							   nprPlannedYTD  DECIMAL(18,2),
							   nprPercentYTD  DECIMAL(18,2),
							   noOfPeople     INTEGER)


    CREATE TABLE #agentPerformance (type           VARCHAR(10),
	                                rowNum         INTEGER,
	                                entityId       VARCHAR(100),
								    state        VARCHAR(2),
									city           VARCHAR(50),
						            favorite       BIT,
									sysfavoriteID  INTEGER,
								    zip            VARCHAR(20),  
							        name           VARCHAR(200),
							        stat           VARCHAR(1),
									email          VARCHAR(200),
	                                nprPlanned     DECIMAL(18,2),
							        nprActual      DECIMAL(18,2),
							        nprPercent     DECIMAL(18,2),
							        nprPlannedYTD  DECIMAL(18,2),
							        nprPercentYTD  DECIMAL(18,2),
									noOfPeople     INTEGER)

	      CREATE TABLE #totalCount (type           VARCHAR(10),
	                                rowNum         INTEGER,
	                                entityId       VARCHAR(100),
								    state          VARCHAR(2),
									city           VARCHAR(50),
						            favorite       BIT,
									sysfavoriteID  INTEGER,
								    zip            VARCHAR(20),  
							        name           VARCHAR(200),
							        stat           VARCHAR(1),
									email          VARCHAR(200),
	                                nprPlanned     DECIMAL(18,2),
							        nprActual      DECIMAL(18,2),
							        nprPercent     DECIMAL(18,2),
							        nprPlannedYTD  DECIMAL(18,2),
							        nprPercentYTD  DECIMAL(18,2),
									noOfPeople     INTEGER)

    CREATE TABLE #agentactivity ([activityID]    INTEGER NOT NULL,
                                 [agentID]       VARCHAR(50) NULL,
                                 [name]          VARCHAR(200) NULL,
                                 [stateID]       VARCHAR(50) NULL,
                                 [year]          INTEGER NULL,
                                 [type]          VARCHAR(50) NULL,
                                 [stat]          VARCHAR(1) NULL,
                                 [category]      VARCHAR(50) NULL,
                                 [corporationID] VARCHAR(100) NULL,
                                 [month1]        DECIMAL(18, 2) NULL,
                                 [month2]        DECIMAL(18, 2) NULL,
                                 [month3]        DECIMAL(18, 2) NULL,
                                 [month4]        DECIMAL(18, 2) NULL,
                                 [month5]        DECIMAL(18, 2) NULL,
                                 [month6]        DECIMAL(18, 2) NULL,
                                 [month7]        DECIMAL(18, 2) NULL,
                                 [month8]        DECIMAL(18, 2) NULL,
                                 [month9]        DECIMAL(18, 2) NULL,
                                 [month10]       DECIMAL(18, 2) NULL,
                                 [month11]       DECIMAL(18, 2) NULL,
                                 [month12]       DECIMAL(18, 2) NULL
                                 ) ON [PRIMARY]


    -- Insert statements for procedure here

    INSERT INTO #agentactivity
    EXEC [spReportAgentActivities] @year = 2019, @agentID = @agentID, @category = 'N' , @UID = @UID

    INSERT INTO #agentactivity
    EXEC [spReportAgentActivities] @year = 2019, @agentID = @agentID, @category = 'I' , @UID = @UID

	INSERT INTO #ttAgentPremium([entityId],[name],[stat],[email],[state],[city],[zip]
	)
	  SELECT distinct aa.agentID,
	           a.name,
	           a.stat,
			   a.email,
			   a.state,
			   a.city,
			   a.zip
      FROM #agentactivity aa inner join agent a
	    ON aa.agentId = a.agentID

  
    UPDATE #ttAgentPremium
     SET #ttAgentPremium.nprActual = p.nprActual
     FROM #ttAgentPremium
     CROSS APPLY (SELECT (p.month1 +
	                      p.month2 +
						  p.month3 +
						  p.month4 +
						  p.month5 +
						  p.month6 +
						  p.month7 +
						  p.month8 +
						  p.month9 +
						  p.month10 +
						  p.month11 +
						  p.month12) as nprActual
                    FROM #agentActivity p
                     WHERE #ttAgentPremium.entityId = p.agentID
					   and p.category = 'N'
					   and p.type     = 'A') as p

    UPDATE #ttAgentPremium
     SET #ttAgentPremium.nprPlanned = p.nprPlanned
     FROM #ttAgentPremium
     CROSS APPLY (SELECT (p.month1 +
	                      p.month2 +
						  p.month3 +
						  p.month4 +
						  p.month5 +
						  p.month6 +
						  p.month7 +
						  p.month8 +
						  p.month9 +
						  p.month10 +
						  p.month11 +
						  p.month12) as nprPlanned
                    FROM #agentActivity p
                     WHERE #ttAgentPremium.entityId = p.agentID
					    and p.category = 'N'
					    and p.type     = 'P') as p

   select @currMonth = datepart(month, getdate())

   UPDATE #ttAgentPremium
     SET #ttAgentPremium.nprPlannedYTD = p.nprPlannedYTD
     FROM #ttAgentPremium
     CROSS APPLY (SELECT (case @currMonth 
                           when 1  then p.month1
						   when 2  then p.month1 + p.month2
						   when 3  then p.month1 + p.month2 + p.month3
						   when 4  then p.month1 + p.month2 + p.month3 + p.month4
						   when 5  then p.month1 + p.month2 + p.month3 + p.month4 + p.month5
						   when 6  then p.month1 + p.month2 + p.month3 + p.month4 + p.month5 + p.month6
						   when 7  then p.month1 + p.month2 + p.month3 + p.month4 + p.month5 + p.month6 + p.month7
						   when 8  then p.month1 + p.month2 + p.month3 + p.month4 + p.month5 + p.month6 + p.month7 + p.month8
						   when 9  then p.month1 + p.month2 + p.month3 + p.month4 + p.month5 + p.month6 + p.month7 + p.month8 + p.month9
						   when 10 then p.month1 + p.month2 + p.month3 + p.month4 + p.month5 + p.month6 + p.month7 + p.month8 + p.month9 + p.month10
						   when 11 then p.month1 + p.month2 + p.month3 + p.month4 + p.month5 + p.month6 + p.month7 + p.month8 + p.month9 + p.month10 + p.month11
						   when 12 then p.month1 + p.month2 + p.month3 + p.month4 + p.month5 + p.month6 + p.month7 + p.month8 + p.month9 + p.month10 + p.month11 + p.month12
                         End) as nprPlannedYTD
                    FROM #agentActivity p
                     WHERE #ttAgentPremium.entityId = p.agentID
					    and p.category = 'N'
					    and p.type     = 'P') as p

    INSERT INTO #ttAllAgents([entityId],[name],[stat],[email],[city],[state],[zip],[nprPlanned],[nprActual],[nprPercent],[nprPlannedYTD],[nprPercentYTD]) 
	  SELECT #ttAgentPremium.entityId,
	         #ttAgentPremium.name,
	         #ttAgentPremium.stat,
			 #ttAgentPremium.email,
			 #ttAgentPremium.city,
			 #ttAgentPremium.state,
			 #ttAgentPremium.zip,
			 ISNULL(#ttAgentPremium.[nprPlanned],0)      AS [nprPlanned],
			 ISNULL(#ttAgentPremium.[nprActual],0)       AS [nprActual],			 
			 CASE 
			  WHEN #ttAgentPremium. nprPlanned = 0 
			   THEN 0 
			  ELSE ISNULL(((#ttAgentPremium.nprActual / #ttAgentPremium. nprPlanned) * 100),0) 
			 END AS [nprPercent],
	         ISNULL(#ttAgentPremium.[nprPlannedYTD],0)   AS [nprPlannedYTD], 
			 CASE 
			  WHEN #ttAgentPremium.[nprPlannedYTD] = 0 
			   THEN 0 
			  ELSE ISNULL(((#ttAgentPremium.nprActual / #ttAgentPremium.[nprPlannedYTD]) * 100),0) 
			 END                                         AS [nprPercentYTD]
	    FROM #ttAgentPremium 

	UPDATE #ttAllAgents SET @iNoCalls = #ttAllAgents.rowNum = @iNoCalls + 1

    UPDATE #ttAllAgents SET #ttAllAgents.favorite      = 'true',
	                        #ttAllAgents.sysfavoriteID = sysfavorite.sysfavoriteID
	  FROM #ttAllAgents join sysfavorite on sysfavorite.entity   = 'A'
										     AND #ttAllAgents.entityId = sysfavorite.entityID 
   
    UPDATE #ttAllAgents set #ttAllAgents.noOfPeople = (select COUNT(*) from personagent where personagent.agentID = #ttAllAgents.entityID);

	/* count total Agents */
    SELECT @iCount = COUNT(*) FROM #ttAllAgents

	INSERT INTO #agentPerformance([type],[entityId],[name],[city],[stat],[email],[state],[zip],[favorite],[sysfavoriteID],[nprPlanned],[nprActual],[nprPercent],[nprPlannedYTD],[nprPercentYTD],[noOfPeople])
    SELECT 
	      'A',
		  #ttAllAgents.entityId,
		  #ttAllAgents.name,
		  #ttAllAgents.city,
		  #ttAllAgents.stat,
		  #ttAllAgents.email,
		  #ttAllAgents.state,
		  #ttAllAgents.zip,
		  #ttAllAgents.favorite,
		  #ttAllAgents.sysfavoriteID,
		  ISNULL(#ttAllAgents.nprPlanned,0)    AS nprPlanned,
		  ISNULL(#ttAllAgents.nprActual,0)     AS nprActual,
		  ISNULL(#ttAllAgents.nprPercent,0)    AS nprPercent,
		  ISNULL(#ttAllAgents.nprPlannedYTD,0) AS nprPlannedYTD,
		  ISNULL(#ttAllAgents.nprPercentYTD,0) AS nprPercentYTD,
		  #ttAllAgents.noOfPeople
	 FROM #ttAllAgents -- Insert statements for procedure here	
	 
	 /* Last record count */
	 INSERT INTO #totalCount(#ttTopAgents.TYPE,#ttTopAgents.rowNum) VALUES('AgentCount',@iCount)

	/********************Top 10 rows sorted according to netPermium*********************/
	
	SELECT * FROM
	(
	SELECT TOP(@ipNoOfRows) * 
	  FROM ( SELECT  type,          
	                 ROW_NUMBER ( ) OVER (ORDER BY #agentperformance.nprActual DESC) AS rowNum ,
                     entityId,
			         state,
				     city ,
				     favorite ,
				     sysfavoriteID ,
			         zip ,  
			         name ,
			         stat ,
				     email  ,
	                 nprPlanned    ,
			         nprActual     ,
				     nprPercent    ,
				     nprPlannedYTD ,
				     nprPercentYTD ,      /* giving row number sequentially acc to netPermium*/
					 noOfPeople
	         from #agentPerformance) q      /* all fields data */
	WHERE rowNum >= (@ipNoOfRows * (@ipPageNo - 1) + 1)     /* pagination */
	UNION
	SELECT * 
	FROM #totalCount ) t
	
END
							    
GO
