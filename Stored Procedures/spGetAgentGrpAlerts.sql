USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetAgentGrpAlerts]    Script Date: 10/9/2017 2:53:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetAgentGrpAlerts] 
    @alertID INT = 0,
    @UID varchar(100) = '',
    @stateID     VARCHAR(3) = '',
	@YearOfSigning integer = 0,
	@Software VARCHAR(MAX) = '',
	@Company VARCHAR(MAX) = '',
	@Organization VARCHAR(MAX) = '',
	@Manager VARCHAR(MAX) = '', 
	@TagList VARCHAR(MAX) = '',
	@AffiliationList VARCHAR(MAX) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
  DECLARE 
  @agentID VARCHAR(MAX) = 'ALL'
  
  SET @agentID = [dbo].[StandardizeAgentID](@agentID)

  CREATE TABLE #stateTable ([stateID] VARCHAR(2))
  INSERT INTO #stateTable ([stateID])
  SELECT [field] FROM [dbo].[GetEntityFilter] ('State', @stateID)
  
  CREATE TABLE #Alerts 
  (
  [alertID] INTEGER,
  [agentID]  VARCHAR(20),
  [agentName]  VARCHAR(MAX),  
  [stateID]  VARCHAR(2),
  [source] VARCHAR(MAX),
  [processCode] VARCHAR(50),
  [threshold] DECIMAL,
  [thresholdRange]  VARCHAR(20), 
  [score] DECIMAL,
  [scoreDesc] VARCHAR(MAX), 
  [severity] VARCHAR(50),
  [description] VARCHAR(MAX),
  [dateCreated] DATETIME,
  [createdBy] VARCHAR(MAX),
  [dateClosed] DATETIME,
  [effDate] DATETIME,
  [closedBy] VARCHAR(MAX),
  [stat] VARCHAR(50),
  [active] BIT,
  [owner] VARCHAR(MAX),
  [lastNoteDate] DATETIME  
  )
  
  CREATE TABLE #AlertsFiltered 
  (
  [alertID] INTEGER,
  [agentID]  VARCHAR(20),
  [agentName]  VARCHAR(MAX),  
  [stateID]  VARCHAR(2),
  [source] VARCHAR(MAX),
  [processCode] VARCHAR(50),
  [threshold] DECIMAL,
  [thresholdRange]  VARCHAR(20), 
  [score] DECIMAL,
  [scoreDesc] VARCHAR(MAX), 
  [severity] VARCHAR(50),
  [description] VARCHAR(MAX),
  [dateCreated] DATETIME,
  [createdBy] VARCHAR(MAX),
  [dateClosed] DATETIME,
  [effDate] DATETIME,
  [closedBy] VARCHAR(MAX),
  [stat] VARCHAR(50),
  [active] BIT,
  [owner] VARCHAR(MAX),
  [lastNoteDate] DATETIME    
  )
  
  CREATE TABLE #agentsGroup
   (
   [agentID] VARCHAR(20),
   [corporationID] VARCHAR(MAX),  
   [stateID] VARCHAR(2),
   [stateName] VARCHAR(20), 
   [stat] VARCHAR(1),
   [name] VARCHAR(MAX),
   [contractDate] datetime,
   [swVendor] VARCHAR(100),
   [manager] VARCHAR(MAX),
   [orgID] VARCHAR(50),
   [orgRoleID] integer, 
   [orgName] VARCHAR(Max)
   )	

INSERT INTO #agentsGroup ([agentID] , [corporationID]  , [stateID] , [stateName] , [stat], [name], [contractDate] , [swVendor], [manager], [orgID] , [orgRoleID] , [orgName]  )
Exec SpGetConsolidatedAgents @StateID ,
	@YearOfSigning ,
	@Software,
	@Company  ,
	@Organization ,
    @Manager  ,
	@TagList ,
	@AffiliationList  ,
	@UID 
	
INSERT INTO #Alerts ( [alertID] ,
  [agentID],
  [agentName] ,
  [stateID] ,
  [source],
  [processCode],
  [threshold] ,
  [thresholdRange],
  [score] ,
  [scoreDesc] , 
  [severity],
  [description] ,
  [dateCreated],
  [createdBy] ,
  [dateClosed] ,
  [effDate] ,
  [closedBy] ,
  [stat] ,
  [active] ,
  [owner] ,
  [lastNoteDate])
    EXEC [dbo].[spGetAlerts] @alertID = 0, @agentID = 'ALL', @stateID = 'ALL', @UID = @UID	
   
  INSERT INTO #AlertsFiltered  select * from  #Alerts al  where 
         (al.[agentID]  IN (SELECT [agentID] FROM #agentsGroup)) 																
														  
 
  SELECT * FROM #AlertsFiltered
  DROP TABLE #agentsGroup
  DROP TABLE #Alerts
  DROP TABLE #AlertsFiltered
  DROP TABLE #stateTable
END