USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spInsertClaimPayable]    Script Date: 11/16/2016 9:49:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spInsertClaimPayable]
	-- Add the parameters for the stored procedure here
	@claimID integer = 0,
  @category varchar(1) = '',
  @vendorID varchar(100) = '',
  @account varchar(20) = '',
  @acctName varchar(500) = '',
  @invoiceDate datetime,
  @amount decimal(18,2) = 0.00
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  DECLARE @vendorName varchar(100) = NULL,
          @apinvID integer = 0,
          @aptrxID integer = 0,
          @good integer = 0

  -- Get the vendor name
  SELECT  @good=COUNT(*)
  FROM    [ANTIC].dbo.PM00200
  WHERE   [VENDORID] = @vendorID

  IF (@good = 1)
  BEGIN
    -- Set the vendor name
    SELECT  @vendorName=LTRIM([VENDNAME])
    FROM    [ANTIC].dbo.PM00200
    WHERE   [VENDORID] = @vendorID
  END
  ELSE
  BEGIN
    -- If we couldn't find the vendor name, set the name as the vendor ID
    SET @vendorName = @vendorID
    SET @vendorID = ''
  END

  -- Get the account name
  IF (@account <> '' AND @acctName = '')
  BEGIN
    SELECT  @acctName=LTRIM(AM.[ACTDESCR])
    FROM    [ANTIC].dbo.GL00100 AM INNER JOIN
            [ANTIC].dbo.GL00105 A
         ON A.[ACTINDX] = AM.[ACTINDX]
    WHERE   AM.[ACTIVE] = 1
    AND     A.[ACTNUMST] = @account
  END

  -- Get the next syskey
  SELECT  @apinvID=[seq] + 1
  FROM    syskey
  WHERE   [type] = 'apInvoice'

  -- Update the key in the table
  UPDATE  syskey
  SET     [seq] = @apinvID
  WHERE   [type] = 'apInvoice'

  -- Get the next syskey
  SELECT  @aptrxID=[seq] + 1
  FROM    syskey
  WHERE   [type] = 'apTransaction'

  -- Update the key in the table
  UPDATE  syskey
  SET     [seq] = @aptrxID
  WHERE   [type] = 'apTransaction'

  PRINT '@claimID=' + CONVERT(VARCHAR,@claimID)
  PRINT '@category=' + @category
  PRINT '@vendorID=' + @vendorID
  PRINT '@vendorName=' + @vendorName
  PRINT '@account=' + @account
  PRINT '@acctName=' + @acctName
  PRINT '@invoiceDate=' + CONVERT(VARCHAR,@invoiceDate)
  PRINT '@amount=' + CONVERT(VARCHAR,@amount)
  PRINT '@apinvID=' + CONVERT(VARCHAR,@apinvID)

  -- Insert into apinv
  INSERT INTO apinv 
          ([apinvID],
           [refType],
           [refID],
           [refSeq],
           [refCategory],
           [vendorID],
           [vendorName],
           [vendorAddress],
           [invoiceDate],
           [amount],
           [stat],
           [uid],
           [invoiceNumber],
           [PONumber],
           [deptID],
           [notes])
  SELECT @apinvID,'C',CONVERT(VARCHAR,@claimID),0,@category,@vendorID,@vendorName,'',@invoiceDate,@amount,'C','Converted','0','0','',''

  -- Insert into apinva
  INSERT INTO apinva 
          ([apinvID],
           [seq],
           [role],
           [uid],
           [dateActed],
           [stat],
           [amount],
           [notes])
  SELECT @apinvID,1,'Approver','Converted',@invoiceDate,'A',@amount,'Auto Approved'

  -- Insert into apinvd
  INSERT INTO apinvd 
          ([apinvID],
           [seq],
           [acct],
           [acctName],
           [amount])
  SELECT @apinvID,1,@account,@acctName,@amount

  -- Insert into aptrx
  INSERT INTO aptrx 
          ([aptrxID],
           [transType],
           [transAmount],
           [transDate],
           [apinvID],
           [refType],
           [refID],
           [refSeq],
           [refCategory],
           [report],
           [uid],
           [completeDate],
           [notes])
  SELECT @aptrxID,'C',@amount,@invoiceDate,@apinvID,'C',CONVERT(VARCHAR,@claimID),0,@category,1,'Converted',@invoiceDate,'Auto Completed'
END
