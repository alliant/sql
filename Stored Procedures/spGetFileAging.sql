USE [COMPASS]
GO
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetFileAging]
	-- Add the parameters for the stored procedure here
	@stateID     VARCHAR(MAX) = 'ALL',
	@agentID     VARCHAR(MAX) = 'ALL',
	@asOfDate    DATETIME     = NULL ,
	@reportType  VARCHAR(1),
	@reportData  VARCHAR(10),
	@allagents   BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	CREATE TABLE #agentTable ([agentID] VARCHAR(30))
    INSERT INTO #agentTable ([agentID])
    SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)

    CREATE TABLE #stateTable ([stateID] VARCHAR(2))
    INSERT INTO #stateTable ([stateID])
    SELECT [field] FROM [dbo].[GetEntityFilter] ('State', @stateID)


	CREATE TABLE #agentFile (agentID      VARCHAR(80),
	                         fileID   VARCHAR(50),
							 arAmount     [decimal](17, 2) NULL,
							 postedDate   DATETIME,
							 invoiced     [decimal](17, 2) NULL,
							 tranDate     datetime,
							 artranID     Integer,
							 tranID       VARCHAR(50),
							 type         VARCHAR(1),
							 fileNumber   VARCHAR(50))


	CREATE TABLE #arAging (stateID      VARCHAR(2),
	     			       agentID      VARCHAR(80),
						   name         VARCHAR(200),
						   manager      VARCHAR(200),
						   region       VARCHAR(80),
						   fileNumber   VARCHAR(50),
						   fileID       VARCHAR(50),
						   tranDate     datetime,
                           receiptDate  datetime,
						   postedDate   DATETIME,
						   invoiced     [decimal](17, 2) NULL, 
						   artranID     Integer,
						   stat         VARCHAR(30),
						   type         VARCHAR(1),
						   tranID       VARCHAR(50),
                           due0_30      [decimal](17, 2) NULL,
						   due31_60     [decimal](17, 2) NULL,
						   due61_90     [decimal](17, 2) NULL,
						   due91_180    [decimal](17, 2) NULL,
						   due181_365   [decimal](17, 2) NULL,
						   due366_730   [decimal](17, 2) NULL,
						   due730_      [decimal](17, 2) NULL,
						   balance      [decimal](17, 2) NULL)

    IF @reportData = 'F' OR @reportData = 'B' 
	 BEGIN 
		/* Get all file and its remaining amount as of given date from artran */
		INSERT INTO #agentFile ([agentID],[fileID],arAmount,invoiced,type)
		SELECT at.entityID,
				at.fileID, 
				SUM(CASE When at.tranDate <= @asOfDate			         
						Then at.tranamt Else 0 End ), 
				SUM(CASE When (at.tranDate <= @asOfDate)
						AND (at.type <> 'A')			         
						Then at.tranamt Else 0 End ),
						'A' 
		FROM [dbo].[artran] at 
		inner join 
		[dbo].[agent] a 
		ON a.agentID = at.entityID
		WHERE (at.type = 'I' OR at.type = 'R' OR at.type = 'A')
			AND at.transType = 'F'
			AND at.[entityID] IN (SELECT [agentID] FROM #agentTable)
			and a.[stateID]   IN (SELECT [stateID] FROM #stateTable)
		GROUP BY at.entityID,at.fileID

		/* Get all file and its remaining amount as of given date from artranh */
		/*INSERT INTO #agentFile ([agentID],[fileID],arAmount,invoiced,type)
		SELECT at.entityID,
				at.fileID, 
				SUM(CASE When at.tranDate <= @asOfDate			         
						Then at.tranamt Else 0 End ),
				SUM(CASE When (at.tranDate <= @asOfDate)
						AND (at.type <> 'A')			         
						Then at.tranamt Else 0 End ), 	
				'A'		  				   
		FROM [dbo].[artranh] at inner join 
		agent a 
		on a.agentID = at.entityID
		WHERE (at.type = 'I' OR at.type = 'R' OR at.type = 'A')
			AND at.transType = 'F'
			AND at.[entityID] IN (SELECT [agentID] FROM #agentTable)
			and a.[stateID]   IN (SELECT [stateID] FROM #stateTable)
		GROUP BY at.entityID, at.fileID */
		
	    /* Set agent ID and tranDate for artran type 'F' */
		Update #agentFile SET tranDate   = at.tranDate,
							artranID   = at.arTranID,
							postedDate = at.postDate 
		FROM #agentFile
		INNER JOIN
		[dbo].[artran] at
		on  at.entityID   = #agentfile.agentID
		and at.fileID = #agentfile.fileID
		WHERE  at.type = 'F'
			and  at.seq  = 0
			and  #agentFile.type = 'A'
			
		/* Set agent ID and tranDate for artranh type 'F' */
		/*Update #agentFile set #agentFile.tranDate   = at.tranDate,
							#agentFile.artranID   = at.arTranhID,
							#agentFile.postedDate = at.postDate   
		from #agentFile
		INNER JOIN
		[dbo].[artranh] at
		on  at.entityID   = #agentfile.agentID
		and at.fileID = #agentfile.fileID
		WHERE at.type         = 'F'
			and at.seq          =  0
			and #agentFile.type = 'A'*/
		
	 END 
	
    /* Misc Invoices */	
	IF @reportData = 'M' OR @reportData = 'B' 
	 BEGIN
	  INSERT INTO #agentFile ([agentID],[tranID],arAmount,[type],[fileNumber])
       SELECT at.entityID,
	          at.tranID, 
	          SUM(CASE When at.tranDate <= @asOfDate			         
		               Then at.tranamt Else 0 End ),
			  'I',
			  'Misc. - ' + at.reference
	   FROM [dbo].[artran] at 
	   inner join 
	   [dbo].[agent] a 
	   ON a.agentID = at.entityID
       WHERE at.type = 'I'
	     AND at.transType = ''
		 AND (at.void = 0 or at.voidDate > @asOfDate)
	     AND at.[entityID] IN (SELECT [agentID] FROM #agentTable)
		 and a.[stateID]   IN (SELECT [stateID] FROM #stateTable)
	   GROUP BY at.entityID,at.tranID,at.reference
	  
	  /* Get all file and its remaining amount as of given date from artranh */
	  /*INSERT INTO #agentFile ([agentID],[tranID],arAmount,[type],[fileNumber])
       SELECT at.entityID,
	          at.tranID, 
	          SUM(CASE When at.tranDate <= @asOfDate			         
		               Then at.tranamt Else 0 End ),
			  'I',
              'Misc. - ' + at.reference			 
	   FROM [dbo].[artranh] at inner join 
	   agent a 
	   on a.agentID = at.entityID
       WHERE at.type = 'I'
	     AND at.transType = ' '
		 AND (at.void = 0 or at.voidDate > @asOfDate)
	     AND at.[entityID] IN (SELECT [agentID] FROM #agentTable)
		 and a.[stateID]   IN (SELECT [stateID] FROM #stateTable)
	   GROUP BY at.entityID,at.tranID,at.reference */
		
      update #agentFile
	    set #agentFile.artranID   =  at.artranID,
	        #agentFile.tranDate   = at.tranDate,
		    #agentFile.postedDate = at.postDate, 
		    #agentFile.fileID = at.reference
	    from artran at
	    where #agentFile.agentID = at.entityID
	      and #agentFile.tranID  = at.tranID
	      and #agentFile.type    = 'I'
		  and at.type = 'I'
		  and at.seq             = 0
		
      /*update #agentFile
	    set #agentFile.artranID   =  at.artranhID,
	        #agentFile.tranDate   = at.tranDate,
		    #agentFile.postedDate = at.postDate ,
		    #agentFile.fileID = at.reference
	    from artranh at
	    where #agentFile.agentID = at.entityID
	      and #agentFile.tranID  = at.tranID
	      and #agentFile.type    = 'I'
		  and at.type = 'I'
		  and at.seq             =  0*/
	  
      update #agentFile
		set #agentFile.arAmount = #agentFile.arAmount + s.tranamt
		from #agentFile
			INNER JOIN (select at.tranID, sum (at.tranAmt) as tranAmt from artran at where at.type = 'A' and at.tranDate <= @asOfDate group by at.tranID ) s	 
		ON #agentFile.tranID = s.tranID
		where #agentFile.type = 'I'
	
	  /*update #agentFile
        set #agentFile.arAmount = #agentFile.arAmount + s.tranamt
		from #agentFile
			INNER JOIN (select at.tranID, sum (at.tranAmt) as tranAmt from artranh at where at.type = 'A' and at.tranDate <= @asOfDate group by at.tranID ) s	 
		ON #agentFile.tranID = s.tranID
		where #agentFile.type = 'I'*/
	END
	
	/* Payments */	
	INSERT INTO #agentFile ([agentID],[tranID],arAmount,[type],[fileNumber])
	SELECT at.entityID,
			at.tranID, 
			SUM(CASE When at.tranDate <= @asOfDate			         
					Then at.tranamt Else 0 End ),
			'P',
			'Payment - ' + at.reference
	  FROM [dbo].[artran] at 
	  inner join 
	  [dbo].[agent] a 
	  ON a.agentID = at.entityID
	  WHERE at.type = 'P'
	    AND at.seq  = 0
		AND (at.void = 0 or at.voidDate > @asOfDate)
		AND at.[entityID] IN (SELECT [agentID] FROM #agentTable)
		and a.[stateID]   IN (SELECT [stateID] FROM #stateTable)
	  GROUP BY at.entityID,at.tranID,at.type,at.reference
	
	/*INSERT INTO #agentFile ([agentID],[tranID],arAmount,[type],[fileNumber])
	SELECT at.entityID, at.tranID, 
			SUM(CASE When at.tranDate <= @asOfDate			         
					Then at.tranamt Else 0 End ),
			'P',
            'Payment - ' + at.reference	
	  FROM [dbo].[artranh] at 
	  inner join 
	  [dbo].[agent] a 
	  ON a.agentID = at.entityID
	  WHERE at.type = 'P'
	    AND at.seq  = 0
		AND (at.void = 0 or at.voidDate > @asOfDate)
		AND at.[entityID] IN (SELECT [agentID] FROM #agentTable)
		and a.[stateID]   IN (SELECT [stateID] FROM #stateTable)
	  GROUP BY at.entityID,at.tranID,at.type,at.reference*/
	
	update #agentFile
	set #agentFile.artranID   =  at.artranID,
		#agentFile.tranDate   = at.tranDate,
		#agentFile.postedDate = at.postDate, 
		#agentFile.fileID = at.reference
	  from artran at
	  where #agentFile.agentID = at.entityID
		and #agentFile.tranID  = at.tranID
		and #agentFile.type    = 'P'
		and at.type = 'P'
		and at.seq             = 0
		
	/*update #agentFile
	set #agentFile.artranID   =  at.artranhID,
		#agentFile.tranDate   = at.tranDate,
		#agentFile.postedDate = at.postDate ,
		#agentFile.fileID = at.reference
	  from artranh at
	  where #agentFile.agentID = at.entityID
		and #agentFile.tranID  = at.tranID
		and #agentFile.type    = 'P'
		and at.type = 'P'
		and at.seq             =  0*/
		
	update #agentFile
	set #agentFile.arAmount = #agentFile.arAmount + s.tranamt
	  from #agentFile
		INNER JOIN (select at.sourceID, sum (at.tranAmt) as tranAmt from artran at where at.type = 'A' and at.tranDate <= @asOfDate group by at.sourceID ) s	 
	  ON #agentFile.artranID = s.sourceID
	  where #agentFile.type = 'P'
	
	/*update #agentFile
	set #agentFile.arAmount = #agentFile.arAmount + s.tranamt
	  from #agentFile
		INNER JOIN (select at.sourceID, sum (at.tranAmt) as tranAmt from artranh at where at.type = 'A' and at.tranDate <= @asOfDate group by at.sourceID ) s	 
	  ON #agentFile.artranID = s.sourceID
	  where #agentFile.type = 'P'*/
	  
    /* considering refunds */	
    update #agentFile
	  set #agentFile.arAmount = #agentFile.arAmount + s.tranamt
	  from #agentFile
	    INNER JOIN (select at.tranID, sum (at.tranAmt) as tranAmt from artran at where at.type = 'P' and at.seq > 0 and at.tranDate <= @asOfDate group by at.tranID) s	 
      ON #agentFile.tranID = s.tranID
	  where #agentFile.type = 'P'
	  
    /*update #agentFile
	  set #agentFile.arAmount = #agentFile.arAmount + s.tranamt
	  from #agentFile
	    INNER JOIN (select at.tranID, sum (at.tranAmt) as tranAmt from artranh at where at.type = 'P' and at.seq > 0 and at.tranDate <= @asOfDate group by at.tranID) s	 
      ON #agentFile.tranID = s.tranID
	  where #agentFile.type = 'P'*/
	  
	  
	/* Credits */	
	INSERT INTO #agentFile ([agentID],[tranID],arAmount,[type],[fileNumber])
	SELECT at.entityID,
			at.tranID, 
			SUM(CASE When at.tranDate <= @asOfDate			         
					Then at.tranamt Else 0 End ),
			'C',
			'Credit - ' + at.reference
	  FROM [dbo].[artran] at 
	  inner join 
	  [dbo].[agent] a 
	  ON a.agentID = at.entityID
	  WHERE at.type = 'C'
	    AND at.seq = 0
		AND (at.void = 0 or at.voidDate > @asOfDate)
		AND at.[entityID] IN (SELECT [agentID] FROM #agentTable)
		and a.[stateID]   IN (SELECT [stateID] FROM #stateTable)
	  GROUP BY at.entityID,at.tranID,at.type,at.reference
	
	/*INSERT INTO #agentFile ([agentID],[tranID],arAmount,[type],[fileNumber])
	SELECT at.entityID, at.tranID, 
			SUM(CASE When at.tranDate <= @asOfDate			         
					Then at.tranamt Else 0 End ),
			'C',
            'Credit - ' + at.reference			
	  FROM [dbo].[artranh] at 
	  inner join 
	  [dbo].[agent] a 
	  ON a.agentID = at.entityID
	  WHERE at.type = 'C'
	    AND at.seq = 0 
		AND (at.void = 0 or at.voidDate > @asOfDate)
		AND at.[entityID] IN (SELECT [agentID] FROM #agentTable)
		and a.[stateID]   IN (SELECT [stateID] FROM #stateTable)
	  GROUP BY at.entityID,at.tranID,at.type,at.reference*/
	
	update #agentFile
	set #agentFile.artranID   =  at.artranID,
		#agentFile.tranDate   = at.tranDate,
		#agentFile.postedDate = at.postDate, 
		#agentFile.fileID = at.reference
	  from artran at
	  where #agentFile.agentID = at.entityID
		and #agentFile.tranID  = at.tranID
		and #agentFile.type    = 'C'
		and at.type = 'C'
		and at.seq             = 0
		
	/*update #agentFile
	set #agentFile.artranID   =  at.artranhID,
		#agentFile.tranDate   = at.tranDate,
		#agentFile.postedDate = at.postDate ,
		#agentFile.fileID = at.reference
	  from artranh at
	  where #agentFile.agentID = at.entityID
		and #agentFile.tranID  = at.tranID
		and #agentFile.type    = 'C'
		and at.type = 'C'
		and at.seq             =  0*/
		
	update #agentFile
	set #agentFile.arAmount = #agentFile.arAmount + s.tranamt
	  from #agentFile
		INNER JOIN (select at.sourceID, sum (at.tranAmt) as tranAmt from artran at where at.type = 'A' and at.tranDate <= @asOfDate group by at.sourceID ) s	 
	  ON #agentFile.artranID = s.sourceID
	  where #agentFile.type = 'C'
	
	/*update #agentFile
	set #agentFile.arAmount = #agentFile.arAmount + s.tranamt
	  from #agentFile
		INNER JOIN (select at.sourceID, sum (at.tranAmt) as tranAmt from artranh at where at.type = 'A' and at.tranDate <= @asOfDate group by at.sourceID ) s	 
	  ON #agentFile.artranID = s.sourceID
	  where #agentFile.type = 'C'*/
	  
    /* considering refunds */
    update #agentFile
	  set #agentFile.arAmount = #agentFile.arAmount + s.tranamt
	  from #agentFile
	    INNER JOIN (select at.tranID, sum (at.tranAmt) as tranAmt from artran at where at.type = 'C' and at.seq > 0 and at.tranDate <= @asOfDate group by at.tranID) s	 
      ON #agentFile.tranID = s.tranID
	  where #agentFile.type = 'C'
	
    /*update #agentFile
	  set #agentFile.arAmount = #agentFile.arAmount + s.tranamt
	  from #agentFile
	    INNER JOIN (select at.tranID, sum (at.tranAmt) as tranAmt from artranh at where at.type = 'C' and at.seq > 0 and at.tranDate <= @asOfDate group by at.tranID) s	 
      ON #agentFile.tranID = s.tranID
	  where #agentFile.type = 'C'*/

    Delete #agentFile where arAmount = 0

    IF @reportType = 'S'
    BEGIN
	  INSERT INTO #arAging([agentID],[due0_30],[due31_60],[due61_90],[due91_180],[due181_365],[due366_730],[due730_])
	  SELECT af.agentID,
	        sum(Case when (@asOfDate -  af.tranDate) >= 0
	               and (@asOfDate -  af.tranDate) <= 30 
				   and af.type                     = 'A'   
				  Then af.arAmount else 0 END) +
            sum(Case when (@asOfDate -  af.tranDate) >= 0
	               and (@asOfDate -  af.tranDate) <= 30 
				   and af.type                     = 'I'   
				  Then af.arAmount else 0 END) -
		    sum(Case when (@asOfDate -  af.tranDate) >= 0
	               and (@asOfDate -  af.tranDate) <= 30 
				   and af.type                     = 'P'   
				  Then af.arAmount else 0 END) -
		    sum(Case when (@asOfDate -  af.tranDate) >= 0
	               and (@asOfDate -  af.tranDate) <= 30 
				   and af.type                     = 'C'   
				  Then af.arAmount else 0 END) as [due0_30], 

	        sum(Case when (@asOfDate -  af.tranDate) > 30
	               and (@asOfDate -  af.tranDate) <= 60 
				   and af.type                     = 'A'   
				  Then af.arAmount else 0 END) +
	        sum(Case when (@asOfDate -  af.tranDate) > 30
	               and (@asOfDate -  af.tranDate) <= 60 
				   and af.type                     = 'I'   
				  Then af.arAmount else 0 END) -
		    sum(Case when (@asOfDate -  af.tranDate) > 30
	               and (@asOfDate -  af.tranDate) <= 60 
				   and af.type                     = 'P'   
				  Then af.arAmount else 0 END) -
		    sum(Case when (@asOfDate -  af.tranDate) > 30
	               and (@asOfDate -  af.tranDate) <= 60 
				   and af.type                     = 'C'   
				  Then af.arAmount else 0 END) as [due31_60], 

	        sum(Case when (@asOfDate -  af.tranDate) > 60
	               and (@asOfDate -  af.tranDate) <= 90 
				   and af.type                     = 'A'   
				  Then af.arAmount else 0 END) +
		    sum(Case when (@asOfDate -  af.tranDate) > 60
	               and (@asOfDate -  af.tranDate) <= 90 
				   and af.type                     = 'I'   
				  Then af.arAmount else 0 END) -
		    sum(Case when (@asOfDate -  af.tranDate) > 60
	               and (@asOfDate -  af.tranDate) <= 90 
				   and af.type                     = 'P'   
				  Then af.arAmount else 0 END) -
		    sum(Case when (@asOfDate -  af.tranDate) > 60
	               and (@asOfDate -  af.tranDate) <= 90 
				   and af.type                     = 'C'   
				  Then af.arAmount else 0 END) as [due61_90],	

	        sum(Case when (@asOfDate -  af.tranDate) > 90
				    and (@asOfDate -  af.tranDate) <= 180 			
			          and af.type                    = 'A' 
			      Then af.arAmount else 0 END) +
		    sum(Case when (@asOfDate -  af.tranDate) > 90
               and (@asOfDate -  af.tranDate) <= 180 			
			          and af.type                    = 'I' 
			      Then af.arAmount else 0 END) -
		    sum(Case when (@asOfDate -  af.tranDate) > 90
                       and (@asOfDate -  af.tranDate) <= 180 			
			          and af.type                    = 'P' 
			      Then af.arAmount else 0 END) -
            sum(Case when (@asOfDate -  af.tranDate) > 90
                  and (@asOfDate -  af.tranDate) <= 180 			
			          and af.type                    = 'C' 
			      Then af.arAmount else 0 END)as [due91_180],
				  
		    sum(Case when (@asOfDate -  af.tranDate) >= 181
	               and (@asOfDate -  af.tranDate) <= 365 
				   and af.type                     = 'A'   
				  Then af.arAmount else 0 END) +
            sum(Case when (@asOfDate -  af.tranDate) >= 181
	               and (@asOfDate -  af.tranDate) <= 365 
				   and af.type                     = 'I'   
				  Then af.arAmount else 0 END) -
		    sum(Case when (@asOfDate -  af.tranDate) >= 181
	               and (@asOfDate -  af.tranDate) <= 365 
				   and af.type                     = 'P'   
				  Then af.arAmount else 0 END) -
		    sum(Case when (@asOfDate -  af.tranDate) >= 181
	               and (@asOfDate -  af.tranDate) <= 365 
				   and af.type                     = 'C'   
				  Then af.arAmount else 0 END) as [due181_365], 
				  
			sum(Case when (@asOfDate -  af.tranDate) >= 366
	               and (@asOfDate -  af.tranDate) <= 730
				   and af.type                     = 'A'   
				  Then af.arAmount else 0 END) +
            sum(Case when (@asOfDate -  af.tranDate) >= 366
	               and (@asOfDate -  af.tranDate) <= 730 
				   and af.type                     = 'I'   
				  Then af.arAmount else 0 END) -
		    sum(Case when (@asOfDate -  af.tranDate) >= 366
	               and (@asOfDate -  af.tranDate) <= 730 
				   and af.type                     = 'P'   
				  Then af.arAmount else 0 END) -
		    sum(Case when (@asOfDate -  af.tranDate) >= 366
	               and (@asOfDate -  af.tranDate) <= 730 
				   and af.type                     = 'C'   
				  Then af.arAmount else 0 END) as [due366_730], 

            sum(Case when (@asOfDate -  af.tranDate) >= 730
	              
				   and af.type                     = 'A'   
				  Then af.arAmount else 0 END) +
            sum(Case when (@asOfDate -  af.tranDate) >= 730
	              
				   and af.type                     = 'I'   
				  Then af.arAmount else 0 END) -
		    sum(Case when (@asOfDate -  af.tranDate) >= 730
	            
				   and af.type                     = 'P'   
				  Then af.arAmount else 0 END) -
		    sum(Case when (@asOfDate -  af.tranDate) >= 730
	              
				   and af.type                     = 'C'   
				  Then af.arAmount else 0 END) as [due730_] 				  
		
       FROM #agentFile af
	   WHERE af.arAmount <> 0
	   GROUP BY af.agentID		  

    END
	ELSE
	BEGIN
	  INSERT INTO #arAging( [agentID],[fileID],[tranDate],[due0_30],[due31_60],[due61_90],[due91_180],[due181_365],[due366_730],[due730_],[type])
	   SELECT af.agentID,
	        af.fileID,
                af.tranDate,
	        (Case when (@asOfDate -  af.tranDate) >= 0
	               and (@asOfDate -  af.tranDate) <= 30    
				  Then af.arAmount else 0 END) as [due0_30], 

	        (Case when (@asOfDate -  af.tranDate) > 30
		           and (@asOfDate -  af.tranDate) <= 60    
				  Then af.arAmount else 0 END) as [duu31_60], 

	        (Case when (@asOfDate -  af.tranDate) > 60
			       and (@asOfDate -  af.tranDate) <= 90    
		    	  Then af.arAmount else 0 END) as [duu61_90],	

	        (Case when (@asOfDate -  af.tranDate) > 90	
                   and (@asOfDate -  af.tranDate) <= 180 			
			      Then af.arAmount else 0 END) as [due91_180],
				  
			(Case when (@asOfDate -  af.tranDate) > 180
		           and (@asOfDate -  af.tranDate) <= 365    
				  Then af.arAmount else 0 END) as [due181_365],
				  
		    (Case when (@asOfDate -  af.tranDate) > 365
		           and (@asOfDate -  af.tranDate) <= 730
				  Then af.arAmount else 0 END) as [due366_730],
				  
			(Case when (@asOfDate -  af.tranDate) > 730 
				  Then af.arAmount else 0 END) as [due730_],
			
            'A'
       FROM #agentFile af
	   WHERE af.arAmount <> 0
	     and af.type = 'A'

	Update #arAging set   #arAging.artranID   = af.arTranID, 
	                      #arAging.postedDate = af.postedDate,
						  #arAging.invoiced   = af.invoiced 
	  from #arAging
	  INNER JOIN
	  #agentFile af
	   on af.agentID    = #arAging.agentID
	  and af.fileID = #arAging.fileID

	Update #arAging set receiptDate = af.reportingDate
	  from #arAging
	  INNER JOIN
	  agentFile af
	   on af.agentID    = #arAging.agentID
	  and af.fileID = #arAging.fileID

	  INSERT INTO #arAging( [agentID],[fileID],[tranDate],[tranID],[due0_30],[due31_60],[due61_90],[due91_180],[due181_365],[due366_730],[due730_],[type])
	   SELECT af.agentID,
	        af.fileID,
                af.tranDate,
	        af.tranID,			
	        (Case when (@asOfDate -  af.tranDate) >= 0
	               and (@asOfDate -  af.tranDate) <= 30    
				  Then af.arAmount else 0 END) as [due0_30], 

	        (Case when (@asOfDate -  af.tranDate) > 30
		           and (@asOfDate -  af.tranDate) <= 60    
				  Then af.arAmount else 0 END) as [duu31_60], 

	        (Case when (@asOfDate -  af.tranDate) > 60
			       and (@asOfDate -  af.tranDate) <= 90    
		    	  Then af.arAmount else 0 END) as [duu61_90],	

	        (Case when (@asOfDate -  af.tranDate) > 90
					and (@asOfDate -  af.tranDate) <= 180			
			      Then af.arAmount else 0 END) as [due91_180],
				  
			(Case when (@asOfDate -  af.tranDate) > 180	
				   and (@asOfDate -  af.tranDate) <= 365
			      Then af.arAmount else 0 END) as [due181_365],

            (Case when (@asOfDate -  af.tranDate) > 365	
				   and (@asOfDate -  af.tranDate) <= 730
			      Then af.arAmount else 0 END) as [due366_730],		

            (Case when (@asOfDate -  af.tranDate) > 730	
			      Then af.arAmount else 0 END) as [due730_],						  
				  
            'P'
       FROM #agentFile af
	   WHERE af.arAmount <> 0
	     and af.type = 'P'

	Update #arAging set   #arAging.artranID   = af.arTranID, 
	                      #arAging.postedDate = af.postedDate,
						  #arAging.invoiced   = af.invoiced, 
						  #arAging.fileID     = af.fileID,
						  #arAging.fileNumber = af.fileNumber
	  from #arAging
	  INNER JOIN
	  #agentFile af
	   on af.agentID    = #arAging.agentID
	  and af.tranID    = #arAging.tranID
	  and af.type = 'P'
	  
	INSERT INTO #arAging( [agentID],[fileID],[tranDate],[tranID],[due0_30],[due31_60],[due61_90],[due91_180],[due181_365],[due366_730],[due730_],[type])
	 SELECT af.agentID,
	      af.fileID,
              af.tranDate,
	      af.tranID,
	      (Case when (@asOfDate -  af.tranDate) >= 0
	             and (@asOfDate -  af.tranDate) <= 30    
			  Then af.arAmount else 0 END) as [due0_30], 

	      (Case when (@asOfDate -  af.tranDate) > 30
	           and (@asOfDate -  af.tranDate) <= 60    
			  Then af.arAmount else 0 END) as [duu31_60], 

	      (Case when (@asOfDate -  af.tranDate) > 60
		       and (@asOfDate -  af.tranDate) <= 90    
	    	  Then af.arAmount else 0 END) as [duu61_90],	

			(Case when (@asOfDate -  af.tranDate) > 90
					and (@asOfDate -  af.tranDate) <= 180			
			      Then af.arAmount else 0 END) as [due91_180],
				  
			(Case when (@asOfDate -  af.tranDate) > 180	
				   and (@asOfDate -  af.tranDate) <= 365
			      Then af.arAmount else 0 END) as [due181_365],

            (Case when (@asOfDate -  af.tranDate) > 365	
				   and (@asOfDate -  af.tranDate) <= 730
			      Then af.arAmount else 0 END) as [due366_730],		

            (Case when (@asOfDate -  af.tranDate) > 730	
			      Then af.arAmount else 0 END) as [due730_],
				  
         'C'
    FROM #agentFile af
	 WHERE af.arAmount <> 0
	   and af.type = 'C'

	Update #arAging set   #arAging.artranID   = af.arTranID, 
	                      #arAging.postedDate = af.postedDate,
						  #arAging.invoiced   = af.invoiced, 
						  #arAging.fileID     = af.fileID,
						  #arAging.fileNumber = af.fileNumber
	  from #arAging
	  INNER JOIN
	  #agentFile af
	   on af.agentID    = #arAging.agentID
	  and af.tranID    = #arAging.tranID
	  and af.type = 'C'
	  
	
	INSERT INTO #arAging( [agentID],[fileID],[tranDate],[tranID],[due0_30],[due31_60],[due61_90],[due91_180],[due181_365],[due366_730],[due730_],[type])
	   SELECT af.agentID,
	        af.fileID,
                af.tranDate,
	        af.tranID,		
	        (Case when (@asOfDate -  af.tranDate) >= 0
	               and (@asOfDate -  af.tranDate) <= 30    
				  Then af.arAmount else 0 END) as [due0_30], 

	        (Case when (@asOfDate -  af.tranDate) > 30
		           and (@asOfDate -  af.tranDate) <= 60    
				  Then af.arAmount else 0 END) as [duu31_60], 

	        (Case when (@asOfDate -  af.tranDate) > 60
			       and (@asOfDate -  af.tranDate) <= 90    
		    	  Then af.arAmount else 0 END) as [duu61_90],	

	        (Case when (@asOfDate -  af.tranDate) > 90
					and (@asOfDate -  af.tranDate) <= 180			
			      Then af.arAmount else 0 END) as [due91_180],
				  
			(Case when (@asOfDate -  af.tranDate) > 180	
				   and (@asOfDate -  af.tranDate) <= 365
			      Then af.arAmount else 0 END) as [due181_365],

            (Case when (@asOfDate -  af.tranDate) > 365	
				   and (@asOfDate -  af.tranDate) <= 730
			      Then af.arAmount else 0 END) as [due366_730],		

            (Case when (@asOfDate -  af.tranDate) > 730	
			      Then af.arAmount else 0 END) as [due730_],
				  
            'I'
       FROM #agentFile af
	   WHERE af.arAmount <> 0
	     and af.type = 'I'

	Update #arAging set   #arAging.artranID   = af.arTranID, 
	                      #arAging.postedDate = af.postedDate,
						  #arAging.invoiced   = af.invoiced, 
						  #arAging.fileID     = af.fileID,
						  #arAging.fileNumber = af.fileNumber
	  from #arAging
	  INNER JOIN
	  #agentFile af
	   on af.agentID    = #arAging.agentID
	  and af.tranID    = #arAging.tranID
	  and af.type = 'I'
	END
	
	IF (@allagents = 1)
    BEGIN
      INSERT INTO #arAging ([agentID])
	  SELECT a.agentID FROM agent a	
	  WHERE a.agentID IN (SELECT [agentID] FROM #agentTable)
	    AND a.agentID NOT IN (SELECT #arAging.agentID FROM #arAging)
		AND a.stateID IN (SELECT [stateID] FROM #stateTable)
    END

    UPDATE #arAging
      SET #arAging.stateID    = agent.stateID,
	      #arAging.region     = agent.region,
          #arAging.name       = agent.name,
		  #araging.stat       = (SELECT [objValue] FROM [dbo].[sysprop] WHERE [appCode] = 'AMD' AND [objAction] = 'Agent' AND [objProperty] = 'Status' AND [objID] = agent.[stat]),
		  #arAging.balance    = #arAging.due0_30 + #arAging.due31_60 + #arAging.due61_90 + #arAging.due91_180
		                        + #arAging.due181_365 + #arAging.due366_730 + #arAging.due730_
	      FROM agent 
	      WHERE #arAging.agentID  = agent.agentID

    IF (@allagents = 0)
    BEGIN
	  Delete #arAging where balance = 0 
    END

    UPDATE #arAging
      SET #arAging.manager    = am.uid	
	  from [dbo].[agentmanager] am
        where am.[isPrimary]      = 1
          AND   am.[stat]         = 'A'
	      AND   #arAging.agentID  = am.agentID

    update #arAging
	  set #arAging.fileNumber = at.filenumber
	  from artran at
	  where  at.entityID   = #arAging.agentID 
	    and  at.fileID     = #arAging.fileID
		and  at.type       = 'F'
		and  #arAging.type = 'A'

    Delete #arAging where #arAging.balance = 0
	
    SELECT * FROM #arAging order by #ArAging.stateID,#ArAging.agentID,#arAging.fileNumber
END
