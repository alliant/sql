USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spCPLCountDiscrepancyForRepFiles]    Script Date: 7/26/2019 2:51:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spCPLCountDiscrepancyForRepFiles]
	-- Add the parameters for the stored procedure here  
  @agentID VARCHAR(MAX) = '',
  @fileNumber VARCHAR(MAX) = '',
  @StartDate DATE = NULL,
  @EndDate DATE = NULL  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	
  -- Insert statements for procedure here
  if (@agentID <> '')
  BEGIN
    SELECT bf.fileNumber,	       
		   bf.cplReported,
		   bf.revReported,
		   csf.noOfCPLIssued,
		   csf.revExpacted,
		   ABS(revExpacted - revReported) AS discrepancy
		FROM 
		
		(SELECT fileNumber, COUNT(*) AS cplReported, SUM(netPremium) AS revReported FROM batchForm WHERE batchID IN
		 (SELECT batchID FROM batch WHERE batch.agentID = @agentID
		 							  AND batch.createDate >= @Startdate)
		 AND formType = 'C'
         GROUP BY fileNumber) AS bf
		 INNER JOIN
		(SELECT fileNumber, COUNT(*) AS noOfCPLIssued, SUM(minGross) AS revExpacted FROM 
		  (SELECT fileNumber, stateID, formID FROM cpl WHERE cpl.agentID = @agentID
			 								        AND  cpl.issueDate BETWEEN @StartDate AND @EndDate
			 									    AND  cpl.fileNumber LIKE '@fileNumber%') AS c
          LEFT JOIN
		  (SELECT stateID, formID, minGross FROM stateform where formType = 'C') AS sf		 
		  ON c.stateID = sf.stateID AND c.formID = sf.formID
		 GROUP BY fileNumber) AS csf 
		 ON bf.fileNumber = csf.fileNumber		 
  END 
END