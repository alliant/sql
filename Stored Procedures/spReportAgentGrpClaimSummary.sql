USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spReportAgentGrpClaimSummary]    Script Date: 3/22/2017 12:52:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spReportAgentGrpClaimSummary]
    @status VARCHAR(5) = '',
    @assignedTo VARCHAR(50) = '',
    @dormantDays INTEGER = 0,
    @startDate DATETIME = NULL,
    @endDate DATETIME = NULL,
    @UID varchar(100) = '',
    @StateID VARCHAR(MAX) = 'ALL',
	@YearOfSigning integer = 0,
	@Software VARCHAR(MAX) = '',
	@Company VARCHAR(MAX) = '',
	@Organization VARCHAR(MAX) = '',
	@Manager VARCHAR(MAX) = '',
	@TagList VARCHAR(MAX) = '',
	@AffiliationList VARCHAR(MAX) = ''
AS
BEGIN
	declare @agentID VARCHAR(MAX) = 'ALL'

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  SET @agentID = [dbo].[StandardizeAgentID](@agentID)

  CREATE TABLE #agentsGroup
(
   [agentID] VARCHAR(20),
   [corporationID] VARCHAR(MAX),  
   [stateID] VARCHAR(2),
   [stateName] VARCHAR(20), 
   [stat] VARCHAR(1),
   [name] VARCHAR(MAX),
   [contractDate] datetime,
   [swVendor] VARCHAR(100),
   [manager] VARCHAR(MAX),
   [orgID] VARCHAR(50),
   [orgRoleID] integer, 
   [orgName] VARCHAR(Max)
)
CREATE TABLE #claimssummary
(
[claimID] integer, 
[description] VARCHAR(MAX),
[stage] VARCHAR(20),
[stageDesc] VARCHAR(MAX),
[type] VARCHAR(10),
[typeDesc] VARCHAR(50),
[action] VARCHAR(20),
[actionDesc] VARCHAR(MAX),
[difficulty] integer,
[urgency] integer,
[stateID] VARCHAR(2),
[agentID] VARCHAR(10),
[agentName] VARCHAR(50),
[agentError] BIT,
[agentErrorDesc] VARCHAR(MAX),
[stat] VARCHAR(20),
[statDesc] VARCHAR(10),
[fileNumber] VARCHAR(20),
[altaRiskCode] VARCHAR(10),
[altaRiskDesc] VARCHAR(100),
[altaResponsibilityCode] VARCHAR(10),
[altaResponsibilityDesc] VARCHAR(MAX),
[causeCode] VARCHAR(10),
[causeDesc] VARCHAR(max),
[descriptionCode] VARCHAR(10),
[descriptionDesc] VARCHAR(MAX),
[dateOpened] DATETIME,
[assignedTo] VARCHAR(50),
[laeOpen] DECIMAL,
[laeApproved] DECIMAL,
[laeLTD] DECIMAL,
[laeReserve] DECIMAL,
[laeAdjOpen] DECIMAL,
[laeAdjLTD] DECIMAL,
[lossOpen] DECIMAL,
[lossApproved] DECIMAL,
[lossLTD] DECIMAL,
[lossReserve] DECIMAL,
[lossAdjOpen] DECIMAL,
[lossAdjLTD] DECIMAL,
[recoveries] DECIMAL,
[pendingRecoveries] DECIMAL,
[pendingDeductibleRecoveries] DECIMAL,
[deductibleRecoveries] DECIMAL,
[deductibleInvoice] VARCHAR(20),
[policyID] VARCHAR(10),
[policyDate] DATETIME,
[policyType] VARCHAR(50),
[insuredName] VARCHAR(50),
[insuredAddr] VARCHAR(50),
[lastNote] DATETIME,
[NoteCnt] INTEGER,
[assignedToDesc] VARCHAR(50),
[costsPaid] DECIMAL
)

CREATE TABLE #claimssummaryFiltered
(
[claimID] integer, 
[description] VARCHAR(MAX),
[stage] VARCHAR(20),
[stageDesc] VARCHAR(MAX),
[type] VARCHAR(10),
[typeDesc] VARCHAR(50),
[action] VARCHAR(20),
[actionDesc] VARCHAR(MAX),
[difficulty] integer,
[urgency] integer,
[stateID] VARCHAR(2),
[agentID] VARCHAR(10),
[agentName] VARCHAR(50),
[agentError] BIT,
[agentErrorDesc] VARCHAR(MAX),
[stat] VARCHAR(20),
[statDesc] VARCHAR(10),
[fileNumber] VARCHAR(20),
[altaRiskCode] VARCHAR(10),
[altaRiskDesc] VARCHAR(100),
[altaResponsibilityCode] VARCHAR(10),
[altaResponsibilityDesc] VARCHAR(MAX),
[causeCode] VARCHAR(10),
[causeDesc] VARCHAR(max),
[descriptionCode] VARCHAR(10),
[descriptionDesc] VARCHAR(MAX),
[dateOpened] DATETIME,
[assignedTo] VARCHAR(50),
[laeOpen] DECIMAL,
[laeApproved] DECIMAL,
[laeLTD] DECIMAL,
[laeReserve] DECIMAL,
[laeAdjOpen] DECIMAL,
[laeAdjLTD] DECIMAL,
[lossOpen] DECIMAL,
[lossApproved] DECIMAL,
[lossLTD] DECIMAL,
[lossReserve] DECIMAL,
[lossAdjOpen] DECIMAL,
[lossAdjLTD] DECIMAL,
[recoveries] DECIMAL,
[pendingRecoveries] DECIMAL,
[pendingDeductibleRecoveries] DECIMAL,
[deductibleRecoveries] DECIMAL,
[deductibleInvoice] VARCHAR(20),
[policyID] VARCHAR(10),
[policyDate] DATETIME,
[policyType] VARCHAR(50),
[insuredName] VARCHAR(50),
[insuredAddr] VARCHAR(50),
[lastNote] DATETIME,
[NoteCnt] INTEGER,
[assignedToDesc] VARCHAR(50),
[costsPaid] DECIMAL
)

INSERT INTO #agentsGroup ([agentID] , [corporationID]  , [stateID] , [stateName] , [stat], [name], [contractDate] , [swVendor], [manager], [orgID] , [orgRoleID] , [orgName]  )
Exec SpGetConsolidatedAgents @StateID ,
	@YearOfSigning ,
	@Software,
	@Company  ,
	@Organization ,
    @Manager  ,
	@TagList ,
	@AffiliationList  ,
	@UID 

  INSERT INTO #claimssummary ([claimID],[stat],[statDesc], [description], [stage], [stageDesc], [type], [typeDesc], [action],[actiondesc],[difficulty],[urgency],[stateID],[agentID],[agentName],[dateOpened],[agentError],[agentErrorDesc],[fileNumber],[assignedTo],[lastNote],  [NoteCnt],[descriptionCode], [descriptionDesc],[causeCode],[causeDesc],[altaResponsibilityCode],
  [altaResponsibilityDesc],[altaRiskCode], [altaRiskDesc],   [laeOpen],  [laeApproved], [laeLTD], [laeAdjOpen], [laeAdjLTD], [laeReserve],  [lossOpen], [lossApproved], [lossLTD], [lossAdjOpen],[lossAdjLTD],[lossReserve], [recoveries],[pendingRecoveries], [pendingDeductibleRecoveries], [deductibleRecoveries], [deductibleInvoice], [policyID], [policyDate], [policyType], [insuredName], [insuredAddr]
  )
    EXEC [dbo].[spReportClaimSummary] @AgentID = 'ALL', @StateID = 'ALL',  @status = '', @assignedTo = '', @dormantDays = 0, @startDate = Null, @endDate = Null, @UID = @UID 
	
  INSERT INTO #claimssummaryFiltered  select * from  #claimssummary cs  where 
         (cs.[agentID]  IN (SELECT [agentID] FROM #agentsGroup)) 
 
  SELECT * FROM #claimssummaryFiltered
  DROP TABLE #claimssummary
  DROP TABLE #claimssummaryFiltered
  DROP TABLE #agentsGroup	
END
