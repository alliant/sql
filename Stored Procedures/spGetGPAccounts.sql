USE [COMPASS]
GO
/****** Object:  StoredProcedure [dbo].[spGetBatchCounts]    Script Date: 8/9/2016 2:38:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		  John Oliver
-- Create date: 5/16/2016
-- Description:	Gets the vendors from GP
-- =============================================
ALTER PROCEDURE [dbo].[spGetGPAccounts]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  -- Build the report
  SELECT  rtrim(a.ACTNUMST) as 'acct',
          rtrim(am.ACTDESCR) as 'acctname',
          isnull(s.stateID,'U') as 'state',
          isnull(s.description,'Unknown') as 'stateName'
  FROM    ANTIC.dbo.GL00100 am INNER JOIN
          ANTIC.dbo.GL00105 a
     ON a.ACTINDX = am.ACTINDX LEFT OUTER JOIN
          dbo.state s
     ON s.seq = convert(integer,substring(rtrim(a.ACTNUMST),len(rtrim(a.ACTNUMST))-1,2))
    AND s.active = 1
  ORDER BY a.ACTNUMST
END