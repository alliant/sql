USE [COMPASS_DVLP]
GO
/****** Object:  StoredProcedure [dbo].[spCreateFileIDCPL]    Script Date: 04/07/2021 8:16:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spCreateFileIDCPL]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  SET ANSI_WARNINGS OFF;

  DECLARE @agentID VARCHAR(20),
          @fileNumber VARCHAR(1000),
          @addr1 VARCHAR(100) = '',
          @addr2 VARCHAR(100) = '',
          @addr3 VARCHAR(100) = '',
          @addr4 VARCHAR(100) = '',
          @city VARCHAR(100) = '',
          @countyID VARCHAR(50) = '',
          @state VARCHAR(2) = '',
          @zip VARCHAR(20) = '',
          @stage VARCHAR(20),
          @transactionType VARCHAR(20) = '',
          @insuredType VARCHAR(20) = '',
          @reportingDate DATETIME = NULL,
          @liability DECIMAL(18,2) = 0,
          @notes VARCHAR(MAX),
          @agentFileID INTEGER

  -- Insert statements for procedure here
	CREATE TABLE #fileID
  (
    [agentID] VARCHAR(20),
    [cplID] VARCHAR(20),
	  [addr1] VARCHAR(100) NULL,
	  [addr2] VARCHAR(100) NULL,
	  [addr3] VARCHAR(100) NULL,
	  [addr4] VARCHAR(100) NULL,
	  [city] VARCHAR(50) NULL,
    [county] VARCHAR(50) NULL,
	  [state] VARCHAR(20) NULL,
	  [zip] VARCHAR(20) NULL,
    [fileNumber] VARCHAR(1000),
    [fileID] VARCHAR(1000)
  )

  INSERT INTO #fileID ([agentID],[cplID],[addr1],[addr2],[addr3],[addr4],[city],[county],[state],[zip],[fileNumber],[fileID])
  SELECT  [agentID],
          [cplID],
          [addr1],
          [addr2],
          [addr3],
          [addr4],
          [city],
          [county],
          [state],
          [zip],
          [fileNumber],
          [dbo].[NormalizeFileID] ([fileNumber])
  FROM    [dbo].[cpl] c
  where c.fileid not in(select fileid from agentfile af where af.agentid = c.agentid)
	and c.fileid is not null
	and c.fileid <> ''
	and c.agentid is not null
	and c.agentid <> ''
  --Insert into agentfile
  DECLARE cur CURSOR LOCAL FOR
  SELECT  [agentID],
          [fileNumber],
          [addr1],
          [addr2],
          [addr3],
          [addr4],
          [city],
          [county],
          [state],
          [zip],
          'CPLIssued' AS [stage],
          'DataFix:CPLIssued,ARC,Processed Agent File when CPL ' + CONVERT(VARCHAR,[cplID]) + ' is issued' AS [notes]
  FROM    #fileID f

  OPEN cur
  FETCH NEXT FROM cur INTO @agentID, @fileNumber, @addr1, @addr2, @addr3, @addr4, @city, @countyID, @state, @zip, @stage, @notes
  WHILE @@FETCH_STATUS = 0 
  BEGIN
    SET @agentFileID = 0
    EXEC [dbo].[spInsertAgentFile] @agentID, @fileNumber, @addr1, @addr2, @addr3, @addr4, @city, @countyID, @state, @zip, @stage, @transactionType, @insuredType, @reportingDate, @liability, @notes, @agentFileID OUTPUT
	
    FETCH NEXT FROM cur INTO @agentID, @fileNumber, @addr1, @addr2, @addr3, @addr4, @city, @countyID, @state, @zip, @stage, @notes
  END
  CLOSE cur
  DEALLOCATE cur

  DROP TABLE #fileID
END
