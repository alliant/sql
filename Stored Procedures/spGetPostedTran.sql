-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
USE [COMPASS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author:		<Rahul Sharma>
-- Create date: <21-11-2019>
-- Description:	<Get artran records>
-- =============================================
CREATE PROCEDURE [dbo].[spGetPostedTran] 
	-- Add the parameters for the stored procedure here
    @piArTranID    INTEGER      = 0,
	@pcEntity      VARCHAR(50)  = 'ALL',
	@pcEntityID    VARCHAR(50)  = 'ALL',
    @pcType        VARCHAR(50)  = 'ALL',	
    @pFromPostDate DATETIME     = NULL,
	@pToPostDate   DATETIME     = NULL,
	@plIncludeAll  BIT          = 0,
	@searchString  VARCHAR(MAX) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    
	-- Insert statements for procedure here
    CREATE TABLE #artran (artranID         INTEGER,
	                      revenueType      VARCHAR(50),
						  tranID           VARCHAR(50),
						  seq              INTEGER,
						  type             VARCHAR(50),
	                      entity           VARCHAR(50),
	                      entityID         VARCHAR(50),
						  fileNumber       VARCHAR(50),
	                      fileID           VARCHAR(50),
						  tranAmt          [DECIMAL](18, 2) NULL,
						  remainingAmt     [DECIMAL](18, 2) NULL,
					      tranDate         DATETIME,
                          void             BIT,
                          voidDate	       DATETIME,
                          voidBy	       VARCHAR(100),
                          notes            VARCHAR(MAX),
                          createdBy        VARCHAR(100),
                          createdDate      DATETIME,
                          fullyPaid        BIT,
                          reference        VARCHAR(100),
                          appliedAmt       [DECIMAL](18, 2) NULL,
                          transType        VARCHAR(20),
                          postBy           VARCHAR(100), 						  
						  postDate         DATETIME,
						  ledgerID         INTEGER,
						  voidledgerID     INTEGER)
	
    INSERT INTO #artran (artranID, revenueType, tranID, seq, type, entity, entityID, fileNumber, fileID, tranAmt, remainingAmt, tranDate, void, voidDate, 
	                     voidBy, notes, createdBy, createdDate, fullyPaid, reference, appliedAmt, transType, postBy, postDate, ledgerID,  voidledgerID) 
						 
    SELECT artran.artranID, 
	       artran.revenueType, 
		   artran.tranID, 
		   artran.seq,
		   artran.type, 
		   artran.entity,
	       artran.entityID, 
		   artran.fileNumber, 
		   artran.fileID, 
		   artran.tranAmt, 
		   artran.remainingAmt, 
		   artran.tranDate, 
		   artran.void, 
		   artran.voidDate,
		   artran.voidBy,
		   artran.notes, 
		   artran.createdBy, 
		   artran.createdDate, 
		   artran.fullyPaid, 
		   artran.reference, 
		   artran.appliedAmt,
		   artran.transType, 
		   artran.postBy, 
		   artran.postDate, 
		   artran.ledgerID, 
		   artran.voidledgerID
      FROM artran
      WHERE (@piArTranID    <> 0 AND artran.artranID = @piArTranID)
	     OR (@piArTranID     = 0 
		AND  artran.entity   =  (CASE WHEN @pcEntity   = 'ALL'    THEN artran.entity    ELSE @pcEntity      END) 
		AND  artran.entityID =  (CASE WHEN @pcEntityID = 'ALL'    THEN artran.entityID  ELSE @pcEntityID    END)
        AND  artran.type     =  (CASE WHEN @pcType     = 'ALL'    THEN artran.type      ELSE @pcType        END)
		AND ((@pcType = 'F' AND artran.transtype = 'F') OR (@pcType = 'I' AND artran.transtype = ' ') OR (@pcType = @pcType))
        AND  artran.seq      = 0
        AND (artran.void     = 0 OR artran.void IS NULL)		
		AND ((@plIncludeAll  = 0 AND artran.remainingAmt > 0) OR @plIncludeAll = 1)
		AND ((@searchString <> '' AND ((@pcType = 'F' AND artran.fileID LIKE @searchString + '%') OR (@pcType = 'I' AND artran.tranID like '%' + @searchString + '%'))) OR @searchString = '')
	    AND  artran.tranDate >= (case WHEN @pFromPostDate IS NULL THEN artran.tranDate  ELSE @pFromPostDate END) 
		AND  artran.tranDate <= (case WHEN @pToPostDate   IS NULL THEN artran.tranDate  ELSE @pToPostDate   END))
	
	
	/* Calculate total amount of file type records */
	UPDATE #artran
	      SET #artran.tranAmt = s.tranAmt
	      FROM #artran
	      INNER JOIN (SELECT at.entityID, at.fileID, SUM (at.tranAmt) AS tranAmt FROM artran at WHERE  at.entity    = 'A'
		                                                                                          AND  at.entityID  = (CASE WHEN @pcEntityID = 'ALL' THEN at.entityID ELSE @pcEntityID END)		  
		                                                                                          AND (at.type = 'I' OR at.type  = 'R') 
		                                                                                          AND  at.transtype = 'F' 		    													                     
			    																	              GROUP BY at.entityID, at.fileID) s	 
          ON #artran.entityID = s.entityID AND (#artran.fileID = s.fileID OR ISNULL(#artran.fileID, s.fileID) IS NULL)	  
	      WHERE #artran.type      = 'F'
	        AND #artran.transtype = 'F'
	        AND #artran.seq       =  0
     	
    SELECT * FROM #artran ORDER BY #artran.artranID DESC		
END
GO
