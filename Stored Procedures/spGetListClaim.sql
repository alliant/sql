GO
/****** Object:  StoredProcedure [dbo].[spGetListClaim]    Script Date: 2/17/2017 7:04:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetListClaim] 
	-- Add the parameters for the stored procedure here
	@fieldList VARCHAR(MAX) = '',
  @filterList VARCHAR(MAX) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets FROM
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  DECLARE @sql NVARCHAR(MAX) = ''

  IF (@fieldList <> '')
  BEGIN
    SET @fieldList = ',' + @fieldList
  END

  SET @sql = N'
  SELECT  DISTINCT claim.[claimID] AS keyfield' + @fieldList + N' 
  FROM    ( 
          SELECT  c.*, 
                  a.[name], 
                  piv.[CA8] AS [significant], 
                  piv.[CA9] AS [litigation], 
                  cc.[claimDescription], 
                  cc.[claimCause],
                  trx.[lossPaymentFilter]
          FROM    [claim] c LEFT OUTER JOIN 
                  ( 
                  SELECT  [claimID], 
                          [attrCode], 
                          [attrValue] 
                  FROM    [claimattr]
                  ) a 
                  PIVOT 
                  ( 
                  max([attrValue]) FOR [attrCode] IN ([CA8], [CA9])
                  ) piv
          ON      c.[claimID] = piv.[claimID] LEFT OUTER JOIN
                  (
                  SELECT  CONVERT(integer, trx.[refID]) AS [claimID], 
                          trx.[transDate] AS [lossPaymentFilter] 
                  FROM    [aptrx] trx
                  WHERE   trx.[refCategory] = ''L''
                  GROUP BY trx.[refID], trx.[transDate]
                  ) trx
          ON      c.[claimID] = trx.[claimID] LEFT OUTER JOIN 
                  ( 
                  SELECT  cc2.[claimID], 
                          CONVERT(VARCHAR(1000), SUBSTRING((SELECT '', '' + sc.[description] + '' ('' + sc.[code] + '')'' AS [text()] FROM [syscode] sc INNER JOIN [claimcode] cc ON cc.[code] = sc.[code] AND cc.[codeType] = sc.[codeType] WHERE cc.[claimID] = cc2.[claimID] AND sc.[codeType] = ''claimDescription'' ORDER BY cc.[claimID] FOR xml path('''') ), 3, 1000)) AS [claimDescription], 
                          CONVERT(VARCHAR(1000), SUBSTRING((SELECT '', '' + sc.[description] + '' ('' + sc.[code] + '')'' AS [text()] FROM [syscode] sc INNER JOIN [claimcode] cc ON cc.[code] = sc.[code] AND cc.[codeType] = sc.[codeType] WHERE cc.[claimID] = cc2.[claimID] AND sc.[codeType] = ''claimCause'' ORDER BY cc.[claimID] FOR xml path('''') ), 3, 1000)) AS [claimCause]
                  FROM    [claimcode] cc2
                  GROUP BY cc2.[claimID] 
                  ) cc 
          ON      c.[claimID] = cc.[claimID] INNER JOIN 
                  [agent] a 
          ON      c.[agentID] = a.[agentID]
          ) claim LEFT OUTER JOIN 
          ( 
          SELECT  CONVERT(integer, inv.[refID]) AS [claimID], 
                  CONVERT(VARCHAR(1000), SUBSTRING((SELECT '', '' + CONVERT(VARCHAR,trx.[transDate],101) AS [text()] FROM [aptrx] trx WHERE trx.[refID] = inv.[refID] AND trx.[refCategory] = ''L'' ORDER BY trx.[transDate] FOR xml path('''')), 3, 1000)) AS [lossPaymentDate] 
          FROM    [apinv] inv 
          GROUP BY inv.[refID] 
          ) aptrx 
  ON      claim.[claimID] = aptrx.[claimID] LEFT OUTER JOIN 
          ( 
          SELECT  CONVERT(integer, inv.[refID]) AS [claimID], 
                  CONVERT(VARCHAR(1000), SUBSTRING((SELECT '', '' + CONVERT(VARCHAR,trx.[transDate],101) AS [text()] FROM [artrx] trx WHERE trx.[refID] = inv.[refID] ORDER BY trx.[transDate] FOR xml path('''')), 3, 1000)) AS [recoveryDate]
          FROM    [arinv] inv 
          GROUP BY inv.[refID] 
          ) artrx 
  ON      claim.[claimID] = artrx.[claimID] LEFT OUTER JOIN 
          ( 
          SELECT  * 
          FROM    ( 
                  SELECT  *, 
                          RANK() OVER(ORDER BY isPrimary DESC) AS [rn]
                  FROM    [claimcoverage] 
                  ) claimcoverage 
          WHERE   claimcoverage.[rn] = 1 
          ) claimcoverage 
  ON      claim.[claimID] = claimcoverage.[claimID] LEFT OUTER JOIN 
          ( 
          SELECT  n.* 
          FROM    [claimnote] n INNER JOIN 
                  ( 
                  SELECT  [claimID], 
                          max([seq]) AS [seq] 
                  FROM    [claimnote]
                  GROUP BY [claimID] 
                  ) n2 
          ON      n.[claimID] = n2.[claimID] 
          AND     n.[seq] = n2.[seq]
          ) claimnote 
  ON      claim.[claimID] = claimnote.[claimID] LEFT OUTER JOIN 
          ( 
          SELECT  * 
          FROM    ( 
                  SELECT   *, 
                           RANK() OVER( ORDER BY seq) AS [rn]
                  FROM     [claimproperty] 
                  ) claimproperty 
          WHERE  claimproperty.[rn] = 1
          ) claimproperty 
  ON      claim.[claimID] = claimproperty.[claimID] LEFT OUTER JOIN 
          ( 
          SELECT  cc.[claimID], 
                  CONVERT(VARCHAR(1000), SUBSTRING((SELECT '', '' + cc2.[fullName] AS [text()] FROM [claimcontact] cc2 WHERE cc2.[claimID] = cc.[claimID] AND cc2.[role] = ''8'' ORDER BY cc2.[contactID] FOR xml path('''')), 3, 1000)) AS [outsideCounselOwner], 
                  CONVERT(VARCHAR(1000), SUBSTRING((SELECT '', '' + cc2.[fullName] AS [text()] FROM [claimcontact] cc2 WHERE cc2.[claimID] = cc.[claimID] AND cc2.[role] = ''9'' ORDER BY cc2.[contactID] FOR xml path('''')), 3, 1000)) AS [outsideCounselLender], 
                  CONVERT(VARCHAR(1000), SUBSTRING((SELECT '', '' + cc2.[fullName] AS [text()] FROM [claimcontact] cc2 WHERE cc2.[claimID] = cc.[claimID] AND cc2.[role] = ''3'' AND cc2.[fullName] <> '''' ORDER BY cc2.[contactID] FOR xml path('''')), 3, 1000)) AS [claimant],
		              CONVERT(VARCHAR(1000), SUBSTRING((SELECT '', '' + cc2.[fullName] AS [text()] FROM [claimcontact] cc2 WHERE cc2.[claimID] = cc.[claimID] AND cc2.[role] = ''12'' ORDER BY cc2.[contactID] FOR xml path('''')), 3, 1000)) AS [outsideCounselCompany] 
          FROM    [claimcontact] cc 
          GROUP BY cc.[claimID]
          ) claimcontact 
  ON      claim.[claimID] = claimcontact.[claimID] LEFT OUTER JOIN 
          ( 
          SELECT  cl.[claimID], 
                  CONVERT(VARCHAR(1000), SUBSTRING((SELECT '', '' + cl2.[defendant] AS [text()] FROM [claimlitigation] cl2 WHERE cl2.[claimID] = cl.[claimID] ORDER BY cl2.[litigationID] FOR xml path('''')), 3, 1000)) AS [defendant], 
                  CONVERT(VARCHAR(1000), SUBSTRING((SELECT '', '' + cl2.[plaintiff] AS [text()] FROM [claimlitigation] cl2 WHERE cl2.[claimID] = cl.[claimID] ORDER BY cl2.[litigationID] FOR xml path('''')), 3, 1000)) AS [plaintiff]
          FROM    [claimlitigation] cl 
          GROUP BY cl.[claimID]
          ) claimlitigation 
  ON      claim.[claimID] = claimlitigation.[claimID]
  '

  IF (@filterList <> '')
    SET @sql = @sql + @filterList

  EXECUTE sp_executesql @sql
END
