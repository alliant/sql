USE [COMPASS]
GO
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetunderPaidInvoices]
	-- Add the parameters for the stored procedure here
    @stateID         VARCHAR(MAX) = 'ALL',
	@agentID         VARCHAR(MAX) = 'ALL',
	@descrepancyAmt  DECIMAL(17,2) = 0.0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	CREATE TABLE #agentTable ([agentID] VARCHAR(30))
    INSERT INTO #agentTable ([agentID])
    SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)

    CREATE TABLE #stateTable ([stateID] VARCHAR(2))
    INSERT INTO #stateTable ([stateID])
    SELECT [field] FROM [dbo].[GetEntityFilter] ('State', @stateID)

	CREATE TABLE #underPaidFileMisc (stateID          VARCHAR(2),
	     				           agentID          VARCHAR(80),
						           name             VARCHAR(200),
						           manager          VARCHAR(80),
						           fileNumber       VARCHAR(50),
								   fileID           VARCHAR(50),
								   tranID           VARCHAR(50),
								   notes            VARCHAR(max),
								   artranID         INTEGER,
						           stat             VARCHAR(1),
                                   appliedAmt       [decimal](17, 2) NULL,
						           tranAmt          [decimal](17, 2) NULL,
						           outstandingAmt   [decimal](17, 2) NULL)

    insert into #underPaidFileMisc ([agentID],[fileID],[tranID],[appliedAmt],[tranAmt],[outstandingAmt])
      SELECT at.entityID,
			 at.fileID,
	         at.tranID, 
	         SUM(CASE When at.type = 'A'
		              Then at.tranamt Else 0 End ), 
	         SUM(CASE When at.type = 'I'
		              Then at.tranamt Else 0 End ),
	         SUM(at.tranamt)
	  FROM [dbo].[artran] at 
	  inner join 
	  [dbo].[agent] a 
	  ON a.agentID = at.entityID
      WHERE at.entity = 'A'
	    AND at.[entityID] IN (SELECT [agentID] FROM #agentTable)
		AND a.[stateID]   IN (SELECT [stateID] FROM #stateTable)
		AND at.transType = ' '
	    AND (at.type = 'I' OR at.type = 'A')		
	  GROUP BY at.entityID,at.tranID,at.fileID	  		

	delete #underPaidFileMisc where #underPaidFileMisc.appliedAmt = 0   		

    UPDATE #underPaidFileMisc
	SET stateID = agent.stateID,
	    name    = agent.name,
		stat    = agent.stat,
		manager = am.uid
	 FROM agent INNER JOIN
       [dbo].[agentmanager] am
       ON   agent.[agentID] = am.[agentID]
       AND  am.[isPrimary]  = 1
       AND  am.[stat]       = 'A'
     where #underPaidFileMisc.agentID = agent.agentID
   
	Update #underPaidFileMisc 
	  SET artranID   = at.arTranID
	  FROM [dbo].[artran] at
	  WHERE at.tranID = #underPaidFileMisc.tranID
	    and at.seq        = 0
	    and at.type       = 'I'
	    and at.transtype  = ' '


    delete #underPaidFileMisc
	    where #underPaidFileMisc.outstandingAmt <= @descrepancyAmt			
		

    update #underPaidFileMisc
	  set #underPaidFileMisc.fileNumber = at.filenumber
	  from artran at
	  where at.entityID    = #underPaidFileMisc.agentID
	    and at.tranID     = #underPaidFileMisc.tranID
		and (at.type       = 'I' or at.type       = 'A')
		and at.transtype  = ' '

    select * from #underPaidFileMisc
    drop table #underPaidFileMisc
	drop table #agentTable
	drop table #stateTable

END
