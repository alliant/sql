USE [COMPASS]
GO
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetunderPaidFile]
	-- Add the parameters for the stored procedure here
    @stateID         VARCHAR(MAX) = 'ALL',
	@agentID         VARCHAR(MAX) = 'ALL',
	@checkNotes      BIT = 0,
	@descrepancyAmt  DECIMAL(17,2) = 0.0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	CREATE TABLE #agentTable ([agentID] VARCHAR(30))
    INSERT INTO #agentTable ([agentID])
    SELECT [field] FROM [dbo].[GetEntityFilter] ('Agent', @agentID)

    CREATE TABLE #stateTable ([stateID] VARCHAR(2))
    INSERT INTO #stateTable ([stateID])
    SELECT [field] FROM [dbo].[GetEntityFilter] ('State', @stateID)

	CREATE TABLE #underPaidFile (stateID          VARCHAR(2),
	     				           agentID          VARCHAR(80),
						           name             VARCHAR(200),
						           manager          VARCHAR(80),
						           fileNumber       VARCHAR(50),
								   fileID           VARCHAR(50),
								   notes            VARCHAR(max),
								   artranID         INTEGER,
						           stat             VARCHAR(1),
                                   appliedAmt       [decimal](17, 2) NULL,
						           tranAmt          [decimal](17, 2) NULL,
						           outstandingAmt   [decimal](17, 2) NULL)

    insert into #underPaidFile ([agentID],[fileID],[appliedAmt],[tranAmt],[outstandingAmt])
      SELECT at.entityID, 
	         at.fileID, 
	         SUM(CASE When at.type = 'A'
		              Then at.tranamt Else 0 End ), 
	         SUM(CASE When at.type = 'I' or at.type = 'R'
		              Then at.tranamt Else 0 End ),
	         SUM(at.tranamt)
	  FROM [dbo].[artran] at 
	  inner join 
	  [dbo].[agent] a 
	  ON a.agentID = at.entityID
      WHERE at.entity = 'A'
	    AND at.[entityID] IN (SELECT [agentID] FROM #agentTable)
		and a.[stateID]   IN (SELECT [stateID] FROM #stateTable)
		AND at.transType = 'F'
	    AND (at.type = 'I' OR at.type = 'R' OR at.type = 'A')	    	    
	  GROUP BY at.entityID,at.fileID	  		

	delete #underPaidFile where #underPaidFile.appliedAmt = 0   		

    UPDATE #underPaidFile
	SET stateID = agent.stateID,
	    name    = agent.name,
		stat    = agent.stat,
		manager = am.uid
	 FROM agent INNER JOIN
       [dbo].[agentmanager] am
       ON   agent.[agentID] = am.[agentID]
       AND  am.[isPrimary]  = 1
       AND  am.[stat]       = 'A'
     where #underPaidFile.agentID = agent.agentID
   
	Update #underPaidFile 
	  SET artranID   = at.arTranID
	  FROM [dbo].[artran] at
	  WHERE at.fileID = #underPaidFile.fileID
	    and at.type       = 'F'
	    and at.seq        = 0

	Update #underPaidFile 
	  SET notes      = af.arnotes
	  FROM [dbo].[agentfile] af
	  WHERE af.agentID    = #underPaidFile.agentID
	    and af.fileID     = #underPaidFile.fileID
		
   IF (@checkNotes = 1 )
      delete #underPaidFile
	    where #underPaidFile.notes is null 
		   or  #underPaidFile.notes = '' 
	       or  #underPaidFile.outstandingAmt <= @descrepancyAmt 
    else
      delete #underPaidFile
	    where #underPaidFile.outstandingAmt <= @descrepancyAmt 
		

    update #underPaidFile
	  set #underPaidFile.fileNumber = at.filenumber
	  from artran at
	  where #underPaidFile.agentID = at.entityID
	    and #underPaidFile.fileID  = at.fileID

    select * from #underPaidFile

END
